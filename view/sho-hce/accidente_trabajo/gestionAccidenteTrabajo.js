
//--------------------------FUNCIONES DE NAVEGACIÓN ------------------------------//

function atSp4NuevoAccidenteTrabajo(){
  
  modeAccidenteTrabajo = 'create'
  actualPageSystemSoma = "view/sho-hce/accidente_trabajo/nuevoAccidenteTrabajo.html";
	handlerUrlhtml('contentGlobal', actualPageSystemSoma , 'Nuevo accidente de trabajo');
}

function atSp4VerDetalleAccidenteTrabajo(Id_at){
  
  ControlLateralAccidenteTrabajo = true;
  navHistoriaAT = true
  modeAccidenteTrabajo = 'show'
  IdAT = Id_at
  
  actualPageSystemSoma = "view/sho-hce/accidente_trabajo/detalleAccidenteTrabajo.html";
  handlerUrlhtml('contentGlobal', actualPageSystemSoma , 'Ver detalle de registro accidente de trabajo');
}

function atSp4VerDetalleAccidenteTrabajoBandeja(Id_at){
  
  navHistoriaAT = false
  modeAccidenteTrabajo = 'show'
  IdAT = Id_at
  
  actualPageSystemSoma = "view/sho-hce/accidente_trabajo/detalleAccidenteTrabajo.html";
  handlerUrlhtml('contentGlobal', actualPageSystemSoma , 'Ver detalle de registro accidente de trabajo');
}

function atSp4SeguimientoAccidenteTrabajo(){

  modeAccidenteTrabajo = 'show'
  actualPageSystemSoma = "view/sho-hce/accidente_trabajo/seguimientoAccidenteTrabajo.html";
  handlerUrlhtml('contentGlobal', actualPageSystemSoma ,'Seguimientos de accidentes de trabajo');
}

function atsp4ShowModal(option) {
    
    $('#titleModalAccidenteTrabajo').text('')
    $('#bodymodalAccidenteTrabajo').html('')

    $('#modalAccidenteTrabajo').modal()
    let title = null
    let page = null
    
    switch(option){
        case 'nuevo-descanso':
            title = "Registrar descanso médico"
            page = "view/sho-hce/descansos_medicos/nuevoDescanzoMedico_HC.html"
            $('#bodymodalAccidenteTrabajo').load(page,_init_atSp4NuevoDescansoMedico)
        break;
        case 'nuevo-seguimiento':
            title = "Registrar seguimiento"
            page = "view/sho-hce/accidente_trabajo/nuevoSeguimiento.html"
            $('#bodymodalAccidenteTrabajo').load(page)
        break;
        case 'nueva-transferencia':
            title = "Registrar transferencia"
            page = "view/sho-hce/interconsultas_transferencias/formularioTransferenciaEditar.html"
            $('#bodymodalAccidenteTrabajo').load(page,_init_atSp4NuevaTransferencia)
        break;
        case 'nueva-alta-especialidad':
            title = "Creación de alta"
            page = "view/sho-hce/accidente_trabajo/nuevaAltaEspecialidades.html"
            $('#bodymodalAccidenteTrabajo').load(page)
        break;
        case 'nuevo-informe':
            title = "Nuevo informe"
            page = "view/sho-hce/accidente_trabajo/nuevoInformeMedico.html"
            $('#bodymodalAccidenteTrabajo').load(page)
        break;
        case 'nuevo-examen-auxiliar':
            title = "Registrar exámenes auxiliares"
            page = "view/sho-hce/accidente_trabajo/nuevoExamenAuxiliar.html"
            $('#bodymodalAccidenteTrabajo').load(page)
        break;
        case 'nueva-evaluacion':
            title = "Registrar evaluación médica"
            page = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html"
            $('#bodymodalAccidenteTrabajo').load(page,_init_atSp4NuevaEvaluacion)
        break;
    }

    if( title){
      $('#titleModalAccidenteTrabajo').text(title)
    }
}

function setControlLateralAT(){
  ControlLateralAccidenteTrabajo = true;
  console.log('setControlLateralAT',ControlLateralAccidenteTrabajo )
}

//--------------------------FUNCIONES DE INICIALIZACION ------------------------------//

function _init_tabs() {
  $(document).ready(()=>{
    $('.tabs-item').click((e)=>{
      const id = $(e.currentTarget).attr('id')
      atSp4SelectTab(id)
    })
  })  
}

function atSp4SelectTab(id){
	$('.tab-section').hide()
	$(`#section-${id}`).show()
	$('.tabs-item').removeClass('active')
	$(`#${id}`).addClass('active')
}

async function _init_atSp4NuevoAccidenteTrabajo() {

  showLoading();

  await atSp4CargarFormAccidenteTrabajo()
  await atSp4CrearAccidenteTrabajoVacio()
  await atSp4CargarDatosCombo()
  await atSp4cargarDatosHistoriaAT()
  await atSp4CargaSedesAreasGerencias()
  await atSp4CargarAptitud()
  await atSp4CargarSistemaAfectado()
  await atSp4CargarSeccionAfectada()
  atSpRenderSelectsSistemaAfectado('input_at_sistema_afectado','input_at_seccion_afectada')
  atSp4CargarDatosTrabajador()
  atSp4CargarInputsDatosTrabajor()
  
  $('#input_at_fecha_registro').val(moment(new Date()).format('YYYY-MM-DD'))
  _init_tabs()

  hideLoading();
}

async function _init_atSp4DetalleAccidenteTrabajo() {

  showLoading();

  await atSp4CargarFormAccidenteTrabajo()

  //Anexo Javier Galindo
  await atSp4ObtenerAdjuntosAccidenteTrabajo();
  
  //Eliminar elementos
  //$('.hide-detail-page').remove()
  
  await atSp4CargarDatosCombo()
  await atSp4ObtenerDatosAccidenteTrabajo()
  await atSp4cargarDatosHistoriaAT()
  await atSp4CargaSedesAreasGerencias()
  await atSp4CargarAptitud()

  await atSp4CargarEvaluacionOtrosAccidenteTrabajo()
  await atSp4CargarDiagnosticoCie10AccidenteTrabajo()
  await atSp4CargarDsoAccidenteTrabajo()
  await atSp4CargarListasAccidenteTrabajo()
  await atSp4CargarAtencionCentroAccidenteTrabajo()
  await atSp4CargaSeguimientoAccidenteTrabajo()
  await atSp4CargarAltas()
  await atSp4CargarInformesMedicos()
  await atSp4CargarExamenesAuxiliares()
  atSp4CargarInputsDatosTrabajor()
  
  _init_tabs()

  if( modeAccidenteTrabajo == 'show' ){

    //Eliminar elementos
    $('.hide-detail-page').remove();

    //Inhabilitar inputs y selects
    $('input').attr('disabled',true);
    $('select').attr('disabled',true);

  }


  if( modeAccidenteTrabajo == 'edit' ){

    $('#input_at_fecha_registro').attr('disabled',true);
    $('#input_at_estado').attr('disabled',true);
    await atSp4CargarSistemaAfectado()
    await atSp4CargarSeccionAfectada()
    atSpRenderSelectsSistemaAfectado('input_at_sistema_afectado','input_at_seccion_afectada')

  }


  hideLoading();
}

async function _init_atSp4NuevoDescansoMedico(){
  showLoading();

  navDescansoAT = true
  await atSp4CrearDescansoMedicoVacio()
  $('.hide-at').remove()
  //Deshabilitar inputs
  $('.disable-at').attr('disabled','disabled')

  //Llenar inputs
  let obj = paObj_at['HistoriaClin'];
  
  $('#dat_des_fi_v').val(paObj_at.IdDescansoMedico); 
  $('#dat_des_dni_trabajador_v').val(obj.NroDocumento_Trabajador_H);
  $('#dat_des_nombres_trabajador_v').val(obj.Nombres_Trabajador_H);
  $('#dat_des_apellidos_trabajador_v').val(obj.Apellidos_Trabajador_H);
  $('#dat_des_fr_v').val(obj.FechaIngresoTasa_H.split('T')[0]);
  $('#dat_des_id_atencion_med_v').val(obj.PuestoTrabajo_Empresa_H);
  $('#dat_des_origen_v').html(`<option value="6" style="font-weight: bold;" selected >Accidente de Trabajo</option>`)

  at_SedeAreaGerencia.Gerencias.forEach((e) => {
    if (e.Id == obj.GerenciaId_Empresa_H) {
      $('#dat_des_gerencia_v').html(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })

  at_SedeAreaGerencia.Sedes.forEach((e) => {
    if (e.Id == obj.PlantaId_Empresa_H) {
      $('#dat_des_planta_v').html(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })

  at_SedeAreaGerencia.Areas.forEach((e) => {
    if (e.Id == obj.AreaId_Empresa_H) {
      $('#dat_des_area_v').html(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })
  
  await atSp4CargarSistemaAfectado()
  await atSp4CargarSeccionAfectada()
  atSpRenderSelectsSistemaAfectado('dat_des_sis_afectado_v','dat_des_sec_afectada_v')
  
  hideLoading();
}

function atSp4CargarFormAccidenteTrabajo(){
  return new Promise((resolve, reject)=>{
    $('#form_at_AccidenteTrabajo').load('view/sho-hce/accidente_trabajo/_formAccidenteTrabajo.html',()=>{
      resolve()
    })
  })
}

function atSp4CargarDatosTrabajador(){  

  //Datos generales
  let obj = paObj_hc[idHC].a;
  
  at_SedeAreaGerencia.Sedes.forEach((e) => {
    if (e.Id == obj.PlantaId_Empresa_H) {
      $('#dat_at_planta_trabajador').text(e.Description);
    }
  });

  at_SedeAreaGerencia.Areas.forEach((e) => {
    if (e.Id == obj.AreaId_Empresa_H) {
      $('#dat_at_area_trabajador').text(e.Description);
    }
  });

  at_SedeAreaGerencia.Gerencias.forEach((e) => {
    if (e.Id == obj.GerenciaId_Empresa_H) {
      $('#dat_at_gerencia_trabajador').text(e.Description);
    }
  });

  $('#dat_at_dni_trabajador').text(obj.NroDocumento_Trabajador_H);
  $('span[name=dat_at_nombres]').text(obj.Nombres_Trabajador_H+" "+obj.Apellidos_Trabajador_H);
  $('#dat_at_edad_trabajador').text(obj.Edad_Trabajador_H);
  $('#dat_at_sexo_trabajador').text(obj.Sexo_Trabajador_H == 1 ? 'Masculino' : 'Femenino');
  $('#dat_at_puesto_trabajador').text(obj.PuestoTrabajo_Empresa_H);

  if(obj.FotoPerfil.length){
    $("img[name=img_file_perfil]").attr("src", obj.FotoPerfil[0].FotoPacienteBase64);
  }else{
      $("img[name=img_file_perfil]").attr("src", 'images/sho/profile.png')
  }
}

function atSp4CargarInputsDatosTrabajor() {
  let obj = paObj_at['HistoriaClin'];

  $('#dat_at_dni_trabajador').val(obj.NroDocumento_Trabajador_H);
  $('#dat_at_nombres_trabajador').val(obj.Nombres_Trabajador_H);
  $('#dat_at_apellidos_trabajador').val(obj.Apellidos_Trabajador_H);
  $('#dat_at_edad_trabajador').val(obj.Edad_Trabajador_H);
  $('#dat_at_fecha_ingreso').val(obj.FechaIngresoTasa_H.split('T')[0]);
  $('#dat_am_fecha_trabajador').val('2021-03-21'); //dato faltante
  $('#dat_at_fecha_retiro').val('2021-03-21'); //dato faltante

  at_SedeAreaGerencia.Gerencias.forEach((e) => {
    if (e.Id == obj.GerenciaId_Empresa_H) {
      $('#dat_at_gerencia_trabajador').append(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })

  at_SedeAreaGerencia.Sedes.forEach((e) => {
    if (e.Id == obj.PlantaId_Empresa_H) {
      $('#dat_at_planta_trabajador').append(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })

  at_SedeAreaGerencia.Areas.forEach((e) => {
    if (e.Id == obj.AreaId_Empresa_H) {
      $('#dat_at_area_trabajador').append(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })

  $('#dat_at_puesto_trabajo_trabajador').val(obj.PuestoTrabajo_Empresa_H);
}

function atSp4CargaSedesAreasGerencias() {

  var url = apiUrlssoma + "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0"
  
  var headers = {
    "apikey": constantes.apiKey
  }

  var settings = {
    "url": url,
    "method": "GET",
    "timeout": 0,
    "crossDomain": true,
    "dataType": "json",
    "headers": headers,
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done(function (response) {

    at_SedeAreaGerencia["Areas"] = response.Area;

    at_SedeAreaGerencia["Gerencias"] = response.Gerencia;
    at_SedeAreaGerencia["Sedes"] = response.Sedes;

  });
}

function atSp4CargarSistemaAfectado() {

 let url = apiUrlsho+`/api/hce_Get_034_sistema_afectado?code=qxkQ9ZXSLatmcpaCr044BU0xgBGcMRX899c8sjilL/dCTIgHf4HyVg==`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {

    pluck_at['SistemaAfectado'] = response.SistemaAfectado;

  });
}

function atSp4CargarSeccionAfectada() {

  let url = apiUrlsho+`/api/hce_Get_035_seccion_afectada?code=7waSycbZnJkX12509pMvTJINkreJkBBQCrsMZNYeY6R5AayCauB5nQ==`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {
    pluck_at['SeccionAfectada'] = response.SeccionAfectada;
  });
}

function atSp4CargarDatosCombo() {
  let url = apiUrlsho+`/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=ListaAT_Combos_All`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {
    pluck_at['TipoEvento'] = response.TipoEvento;
    pluck_at['TipoAccidenteTrabajo'] = response.TipoAccidenteTrabajo;
    pluck_at['Especialidad'] = response.Especialidad;
    pluck_at['TipoExamen'] = response.TipoExamen;
  });
}

function atSpRenderSelectsSistemaAfectado(SistemaAfectado, SeccionAfectada){

  //Sistemas afectados
  $(`#${SistemaAfectado}`).html('');
  $(`#${SistemaAfectado}`).append('<option value="" selected></option>')
  
  pluck_at['SistemaAfectado'].map((Item) => {
    $(`#${SistemaAfectado}`).append(`<option value="${Item.Id}">${Item.Descripcion}</option>`)
  });

  //Seccion afectada
  $(`#${SeccionAfectada}`).html('');
  $(`#${SeccionAfectada}`).append('<option value="" selected></option>')
  
  pluck_at['SeccionAfectada'].map((Item) => {
    $(`#${SeccionAfectada}`).append(`<option value="${Item.Id}">${Item.Descripcion}</option>`)
  });
}

function atSp4CargarTipoExamen() {

  let url = apiUrlsho+`/api/hce_Get_039_tipo_examen?code=fyAYixF97aLnaHqhcOdCMbvgb0SH7Qlz2EYrfCrk1uZxhmTPmZuKjg==`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {
    pluck_at['TipoExamen'] = response.TipoExamen;
  });
}

function atSp4CargarAptitud() {
  let url = apiUrlsho+`/api/hce_Get_040_aptitud?code=naSFtYAO8RHbK08dOfi93Vja7Cc9iCHjz6o7ldn9EW0dzFu3aU3jZg==`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {
    paObj_at.Aptitud = response.Aptitud;
  })
}


function atSp4cargarDatosHistoriaAT() {
  

  //Variable a consultar en la BD. Se consulta con el ID de la Historia Clínica
  idHC = paObj_at.data_Accidente_Trabajo ? paObj_at.data_Accidente_Trabajo.IdHC : idHC
  
  //Se especifican los parametros para la consulta
  let url = `https://5h0-dev-salud.azurewebsites.net/api/hce_Get_003_historia_clinica_datos_generales?code=EGqLSsRdXVPm7TSRIZ6/MGieka3md/u1gwzEwWcGhOjMZqDNK1jjRw==`;
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }
  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    "data": { "IdHC": idHC }
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done((response) => {

    paObj_at['HistoriaClin'] = response.HistoriaClin[0];

  });

}

//--------------------------FUNCIONES ACCIDENTE DE TRABAJO ------------------------------//

function atSp4ConfirmGuardarAccidenteTrabajo() {

  if (validationForm('submit').length > 0) {
    return;
  }

  let nombres = $("#dat_at_nombres_trabajador").val();
  let apellidos = $("#dat_at_apellidos_trabajador").val();
  
  Swal.fire({
    title: "Guardar accidente de trabajo.",
    html: `
       <p>Está por guardar el accidente de trabajo de</p>
       <p>${nombres} ${apellidos}</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4GuardarAccidenteTrabajo()
    }
  });
}

function atSp4ConfirmCancelarAccidenteTrabajo() {

  if( modeAccidenteTrabajo == 'edit' ){

     atSp4CancelarAccidenteTrabajo();

     return;

  }

  let nombres = $("#dat_at_nombres_trabajador").val();
  let apellidos = $("#dat_at_apellidos_trabajador").val();
  
  Swal.fire({
    title: "Cancelar accidente de trabajo.",
    html: `
       <p>Está por cancelar el accidente de trabajo de</p>
       <p>${nombres} ${apellidos}</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4CancelarAccidenteTrabajo()
    }
  });
}

function atSp4ConfirmCerrarAccidenteTrabajo() {

  if (validationForm('submit').length > 0) {
    return;
  }

  let nombres = $("#dat_at_nombres_trabajador").val();
  let apellidos = $("#dat_at_apellidos_trabajador").val();
  
  Swal.fire({
    title: "Cerrar accidente de trabajo.",
    html: `
       <p>Está por cerr el accidente de trabajo de</p>
       <p>${nombres} ${apellidos}</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4CerrarAccidenteTrabajo()
    }
  });
}

async function atSp4GuardarAccidenteTrabajo() {

  if (validationForm('submit').length > 0) {
    return;
  }

  let data = await atSp4SetDataAccidenteTrabajo();
  data.Id1 = IdAT
  data.accion = 1

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_post?code=OsV1/z3yiGoDvAzVKQwUk/dF28djRGKG4ZDUrSdQK01rZoPPCXPOTg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    Swal.fire({
      title: 'Accidente de trabajo registrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
    
    actualPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    handlerUrlhtml('contentGlobal', actualPageSystemSoma , 'Historia clínica electrónica');    
  })
}

function atSp4CancelarAccidenteTrabajo(){

  /*let data = await atSp4SetDataAccidenteTrabajo();
  data.Id1 = IdAT
  data.accion = 1
  
  let url = `${apiUrlsho}/api/hce_accidente_trabajo_post?code=OsV1/z3yiGoDvAzVKQwUk/dF28djRGKG4ZDUrSdQK01rZoPPCXPOTg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    actualPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    handlerUrlhtml('contentGlobal', actualPageSystemSoma , 'Historia clínica electrónica');
  })*/
  actualPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
  handlerUrlhtml('contentGlobal', actualPageSystemSoma , 'Historia clínica electrónica');
}

async function atSp4CerrarAccidenteTrabajo() {

  let data = await atSp4SetDataAccidenteTrabajo();
  data.Id1 = IdAT
  data.accion = 1
  data.A_Estado_Accidente = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_post?code=OsV1/z3yiGoDvAzVKQwUk/dF28djRGKG4ZDUrSdQK01rZoPPCXPOTg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Accidente de trabajo cerrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    actualPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    handlerUrlhtml('contentGlobal', actualPageSystemSoma , 'Historia clínica electrónica');
  })
}

function atSp4SetDataAccidenteTrabajo() {
  return new Promise((resolve,reject) => {
    let data = {};

    data.IdHC = idHC
    data.A_Accidente_TrabajoId = $('#input_at_accidente_trabajo').val()
    data.A_Fecha_Registro = $('#input_at_fecha_registro').val()
    data.A_Fecha_Accidente = $('#input_at_fecha_accidente').val()
    data.A_Hora_Accidente = $('#input_at_hora_accidente').val()
    data.A_Lugar_Accidente = $('#input_at_lugar_accidente').val()
    data.A_Estado_Accidente = $('#input_at_estado').val()
    data.A_Jefe = $('#input_at_jefe').val()
    data.A_Telefono_Jefe = $('#input_at_jefe_telefono').val()
    data.A_HashJefe = ''
    data.A_Anamesis = $('#input_at_anamnesis').val()
    data.B_SV_Presion_Arterial  = parseFloat($('#input_at_presion_arterial_sv').val())
    data.B_SV_Presion_Arterial2 = parseFloat($('#input_at_presion_arterial2_sv').val())
    data.B_SV_Frecuencia_Cardiaca = parseFloat($('#input_at_frecuencia_cardiaca_sv').val())
    data.B_SV_Frecuencia_Respiratoria = parseFloat($('#input_at_frecuencia_respiratoria_sv').val())
    data.B_SV_Saturacion = parseFloat($('#input_at_saturacion_sv').val())
    data.B_SV_Temperatura = parseFloat($('#input_at_temperatura_sv').val())
    data.C_Estado_Conciencia = parseInt($('#input_at_conciencia_examen').val())
    data.C_Estado_Nutricion = parseInt($('#input_at_nutricion_examen').val())
    data.C_Estado_General = parseInt($('#input_at_general_examen').val())
    data.C_Estado_Hidratacion = parseInt($('#input_at_hidratacion_examen').val())
    data.C_Orofaringe = $('#input_at_orofaringe_examen').val()
    data.C_Torax = $('#input_at_torax_examen').val()
    data.C_Cardio = $('#input_at_cardio_examen').val()
    data.C_SOMA = $('#input_at_soma_examen').val()
    data.CreadoPor = 'SHO'
    data.ModificadoPor = 'SHO'
    data.CreadoFecha = ''
    resolve(data)
  })
}

async function atSp4CrearAccidenteTrabajoVacio() {

  let data = await atSp4SetDataAccidenteTrabajo();

  data.Id1 = 0
  data.accion = 0
  data.A_Estado_Accidente = 1
  data.A_Accidente_TrabajoId = 0
  data.A_Fecha_Registro = moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS')
  data.A_Fecha_Accidente = moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS')
  data.A_Hora_Accidente = '00:00:00.0000000'
  data.B_SV_Presion_Arterial = 0
  data.B_SV_Presion_Arterial2 = 0
  data.B_SV_Frecuencia_Cardiaca = 0
  data.B_SV_Frecuencia_Respiratoria = 0
  data.B_SV_Saturacion = 0
  data.B_SV_Temperatura = 0
  data.C_Estado_Conciencia = 0
  data.C_Estado_Nutricion = 0
  data.C_Estado_General = 0
  data.C_Estado_Hidratacion = 0
  data.CreadoFecha =  moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS')

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_post?code=OsV1/z3yiGoDvAzVKQwUk/dF28djRGKG4ZDUrSdQK01rZoPPCXPOTg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    IdAT = response.Id
  })
}

function atSp4ObtenerDatosAccidenteTrabajo() {


  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Data_Accidente_Trabajo`;

  let data = {
    idHC : IdAT // id de accidente de trabajo
  }

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {

    paObj_at['data_Accidente_Trabajo'] = response.data_Accidente_Trabajo.length ? response.data_Accidente_Trabajo[0] : null

    atSp4CargarDatosAccidenteTrabajo()
console.log("response",response);
    if(paObj_at.data_Accidente_Trabajo) {

      const adjunto1 = paObj_at.data_Accidente_Trabajo.Adjunto1 ? {
        NombreArchivo: paObj_at.data_Accidente_Trabajo.NombreAdjunto1,
        CreadoFecha: paObj_at.data_Accidente_Trabajo.FechaSubidaAdjunto1
      } : null

      const adjunto2 = paObj_at.data_Accidente_Trabajo.Adjunto2 ? {
        NombreArchivo: paObj_at.data_Accidente_Trabajo.NombreAdjunto2,
        CreadoFecha: paObj_at.data_Accidente_Trabajo.FechaSubidaAdjunto2
      } : null

      //atSp4RenderAdjuntoLista(adjunto1, 1)
      //atSp4RenderAdjuntoLista(adjunto2, 2)
    }

  })
}

//Anexo Javier Galindo 31-01-2022

function atSp4ObtenerAdjuntosAccidenteTrabajo() {


  let url = `${apiUrlsho}/api/hce_Get_029_1_accidente_trabajo_adjunto?code=PoFFnsJxNh6MUJp4xB3EG8AlvV21d9TyK4fs8HWntZHytGUaBB3KvA==`;

  let data = {
    AccidenteTrabajoId : IdAT // id de accidente de trabajo
  }

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {

    console.log("adjuntos AT 2", response);

    $('#content_sp4_AT_table_iat_img').html('')
    $('#content_sp4_AT_table_iat').html('')
    $('#content_sp4_AT_table_adjunto_2').html('')

    $('#secion_at_adjuntar_iat').show()
    $('#secion_at_adjuntar_2').show();

    response.AdjuntosAccidenteTrabajo.forEach((ItemAdjunto) => {

      if( ItemAdjunto.TipoAdjunto_1_2 == 1  ){

        $('#secion_at_adjuntar_iat').hide();        

        $('#dat_at_cant_iat').text('01 archivos')        
        $('#content_sp4_AT_table_iat').append(`
          <tr>
            <td>${ItemAdjunto.NombreArchivo}</td>
            <td>${moment(new Date(ItemAdjunto.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>
              <div class="d-flex justify-content-center align-items-center">
                <a class="btn btn-link shadow-none" href="${ItemAdjunto.ArchivoBase64}" download="${ItemAdjunto.NombreArchivo}" style="text-decoration:none; text-decoration: none;color: #000; display: contents;">
                  <img src="./images/sho/eyeIcon.svg">
                </a>
                <button onclick="atSp4ConfirmarEliminarAdjunto(${ItemAdjunto.Id},1)" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                  <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                </button>
              </div>
            </td>
          </tr>
        `);

      }


      if( ItemAdjunto.TipoAdjunto_1_2 == 2  ){

        $('#secion_at_adjuntar_2').hide()        

        $('#dat_at_cant_adjunto_2').text('01 archivos')        
        $('#content_sp4_AT_table_adjunto_2').append(`
          <tr>
            <td>${ItemAdjunto.NombreArchivo}</td>
            <td>${moment(new Date(ItemAdjunto.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>
              <div class="d-flex justify-content-center align-items-center">
                <a class="btn btn-link shadow-none" href="${ItemAdjunto.ArchivoBase64}" download="${ItemAdjunto.NombreArchivo}" style="text-decoration:none; text-decoration: none;color: #000; display: contents;">
                  <img src="./images/sho/eyeIcon.svg">
                </a>
                <button onclick="atSp4ConfirmarEliminarAdjunto(${ItemAdjunto.Id},2)" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                  <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                </button>
              </div>
            </td>
          </tr>
        `);

      }


      if( ItemAdjunto.TipoAdjunto_1_2 == 3  ){

        //$('#secion_at_adjuntar_iat_img').hide();
        //$('#secion_at_iat_list_img').show();
        
        $('#content_sp4_AT_table_iat_img').append(`
          <tr>
            <td>${ItemAdjunto.NombreArchivo}</td>
            <td>${moment(new Date(ItemAdjunto.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>
              <div class="d-flex justify-content-center align-items-center">
                <a class="btn btn-link shadow-none" href="${ItemAdjunto.ArchivoBase64}" download="${ItemAdjunto.NombreArchivo}" style="text-decoration:none; text-decoration: none;color: #000; display: contents;">
                  <img src="./images/sho/eyeIcon.svg">
                </a>
                <button onclick="atSp4ConfirmarEliminarAdjunto(${ItemAdjunto.Id},3)" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                  <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                </button>
              </div>
            </td>
          </tr>
        `);

      }

    });

    
    $('#dat_at_cant_iat_img').text('0'+$('#content_sp4_AT_table_iat_img tr').length+' archivos')
    
    if($('#content_sp4_AT_table_iat_img tr').length == 2){

      $('#secion_at_adjuntar_iat_img').hide();        

    }
    else{

      $('#secion_at_adjuntar_iat_img').show();  

    }

    if($('#content_sp4_AT_table_iat_img tr').length == 0){

      $('#secion_at_iat_list_img').hide();      

    }
    else{

      $('#secion_at_iat_list_img').show();  

    }




    if($('#content_sp4_AT_table_iat tr').length == 0){

      $('#secion_at_iat_list').hide();      

    }
    else{

      $('#secion_at_iat_list').show();   

    }

    if($('#content_sp4_AT_table_adjunto_2 tr').length == 0){

      $('#secion_at_adjunto_2').hide();      

    }
    else{

      $('#secion_at_adjunto_2').show();   

    }

  });


}

function atSp4CargarDatosAccidenteTrabajo() {

  if(!paObj_at.data_Accidente_Trabajo) return

  const data = paObj_at.data_Accidente_Trabajo
  
  $('#input_at_accidente_trabajo').val(data.A_Accidente_TrabajoId)
  $('#input_at_fecha_registro').val(moment(new Date(data.A_Fecha_Registro)).format('YYYY-MM-DD'))
  $('#input_at_fecha_accidente').val(moment(new Date(data.A_Fecha_Accidente)).format('YYYY-MM-DD'))
  $('#input_at_hora_accidente').val(data.A_Hora_Accidente.split('.')[0])
  $('#input_at_lugar_accidente').val(data.A_Lugar_Accidente)
  $('#input_at_estado').val(data.A_Estado_Accidente)
  $('#input_at_jefe').val(data.A_Jefe)
  $('#input_at_anamnesis').val(data.A_Anamesis ? data.A_Anamesis : '')
  $('#input_at_jefe_telefono').val(data.A_Telefono_Jefe)
  $('#input_at_presion_arterial_sv').val(data.B_SV_Presion_Arterial)
  $('#input_at_presion_arterial2_sv').val(data.B_SV_Presion_Arterial2)
  $('#input_at_frecuencia_cardiaca_sv').val(data.B_SV_Frecuencia_Cardiaca)
  $('#input_at_frecuencia_respiratoria_sv').val(data.B_SV_Frecuencia_Respiratoria)
  $('#input_at_saturacion_sv').val(data.B_SV_Saturacion)
  $('#input_at_temperatura_sv').val(data.B_SV_Temperatura)
  $('#input_at_conciencia_examen').val(data.C_Estado_Conciencia)
  $('#input_at_nutricion_examen').val(data.C_Estado_Nutricion)
  $('#input_at_general_examen').val(data.C_Estado_General)
  $('#input_at_hidratacion_examen').val(data.C_Estado_Hidratacion)
  $('#input_at_orofaringe_examen').val(data.C_Orofaringe)
  $('#input_at_torax_examen').val(data.C_Torax)
  $('#input_at_cardio_examen').val(data.C_Cardio)
  $('#input_at_soma_examen').val(data.C_SOMA)

}

//--------------------------FUNCIONES ARCHIVOS ADJUNTOS ------------------------------//

function atSp4CapturarAdjunto(element,type, tipoAdjunto) {
  console.log("atSp4CapturarAdjunto");
  var file = element.files[0];
  var reader = new FileReader();

  reader.onloadend = function () {
console.log("onloadend");
    let data = {};
    data.Id1 = 0
    data.accion = 0
    data.NombreArchivo = element.files[0].name
    data.ArchivoBase64 = reader.result
    data.CreadoPor = 'SHO' 
    data.ModificadoPor = 'SHO' 

    switch(type){
      case 'IAT':console.log("switch");
        data.TipoAdjunto_1_2 = tipoAdjunto
        data.Accidente_TrabajoId = IdAT
        atSp4GuardarAdjuntoIAT(data)
      break;
      case 'alta-especialidad':
        data.AltaEspecialidadId = paObj_at.IdAlta
        atSp4GuardarAdjuntoAlta(data)
      break;
      case 'informe':
        atSp4GuardarAdjuntoInforme(data)
      break;
      case 'examenes-auxiliares':
        paObj_at['adjuntoExamenAuxiliar'] = data
        atSp4RenderAdjuntoListaExamenAuxiliar()
      break;
    }
  }

  reader.readAsDataURL(file);
}

function atSp4GuardarAdjuntoIAT(data) { 
  

  let url = apiUrlsho+`/api/hce_accidente_trabajo_adjunto_post?code=EI20Mo83UPBp602vppBnVkq5Y6BhfwCk9WvKk8mU3BrEecoHPqwdBg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    "data": JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    Swal.fire({
      title: 'Archivo adjunto anexado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
    
    
    atSp4ObtenerAdjuntosAccidenteTrabajo();
    
      //atSp4RenderAdjuntoLista(response, response.TipoAdjunto_1_2)

    

  });
}

function atSp4RenderAdjuntoLista(data, tipo){

  if(tipo == 1){

    $('#secion_at_adjuntar_iat').hide()
    $('#secion_at_iat_list').show()


    if(!data) return

    $('#dat_at_cant_iat').text('01 archivos')
    $('#content_sp4_AT_table_iat').html('')
    $('#content_sp4_AT_table_iat').append(`
      <tr>
        <td>${data.NombreArchivo}</td>
        <td>${moment(new Date(data.CreadoFecha)).format('DD/MM/YYYY')}</td>
        <td>
          <div class="d-flex justify-content-center align-items-center">
            <a class="btn btn-link shadow-none" href="${data.ArchivoBase64}" download="${data.NombreArchivo}" style="text-decoration:none; text-decoration: none;color: #000; display: contents;">
              <img src="./images/sho/eyeIcon.svg">
            </a>
            <button onclick="atSp4ConfirmarEliminarAdjunto(${data.Id},1)" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
              <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
            </button>
          </div>
        </td>
      </tr>
    `)

  }else if( tipo == 2){

    $('#secion_at_adjuntar_2').hide()
    $('#secion_at_adjunto_2').show()

    if(!data) return

    $('#dat_at_cant_adjunto_2').text('01 archivos')
    $('#content_sp4_AT_table_adjunto_2').html('')
    $('#content_sp4_AT_table_adjunto_2').append(`
      <tr>
        <td>${data.NombreArchivo}</td>
        <td>${moment(new Date(data.CreadoFecha)).format('DD/MM/YYYY')}</td>
        <td>
          <div class="d-flex justify-content-center align-items-center">
            <a class="btn btn-link shadow-none" href="${data.ArchivoBase64}" download="${data.NombreArchivo}" style="text-decoration:none; text-decoration: none;color: #000; display: contents;">
              <img src="./images/sho/eyeIcon.svg">
            </a>
            <button onclick="atSp4ConfirmarEliminarAdjunto(${data.Id},2)" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
              <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
            </button>
          </div>
        </td>
      </tr>
    `)
  }
}

function atSp4ConfirmarEliminarAdjunto(Id,tipo){

    Swal.fire({
    title: "Eliminar archivo adjunto.",
    html: `
       <p>Está por eliminar un archivo adjunto.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarAdjunto(Id,tipo)
    }
  });
}

function atSp4EliminarAdjunto(Id,tipo){
  
  let data = {};
  data.Id1 = Id
  data.accion = 2
  data.NombreArchivo = 'Archivo eliminado'
  data.ArchivoBase64 = 'Archivo eliminado'
  data.CreadoPor = 'SHO' 
  data.ModificadoPor = 'SHO'
  data.TipoAdjunto_1_2 = tipo
  data.Accidente_TrabajoId = IdAT
console.log("data eliminar--->",data);
  let url = apiUrlsho+`/api/hce_accidente_trabajo_adjunto_post?code=EI20Mo83UPBp602vppBnVkq5Y6BhfwCk9WvKk8mU3BrEecoHPqwdBg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    "data": JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    Swal.fire({
      title: 'Archivo adjunto eliminado correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    if(response.Id == 0) return

    /*if(tipo == 1){

      $('#secion_at_adjuntar_iat').show()
      $('#secion_at_iat_list').hide()

    }

    if(tipo == 2){

      $('#secion_at_adjuntar_2').show()
      $('#secion_at_adjunto_2').hide()

    }

    if(tipo == 3){

      $('#secion_at_adjuntar_iat_img').show()
      $('#secion_at_iat_list_img').hide()

    }*/
    atSp4ObtenerAdjuntosAccidenteTrabajo();

  });
}

//--------------------------FUNCIONES OTROS ------------------------------//

function atSp4GuardarEvaluacionOtrosAccidenteTrabajo() {
  
  if (validationForm('check-otros').length > 0) {
    return;
  }

  let data = {};
  data.Id1 = 0
  data.Accidente_TrabajoId = IdAT
  data.Tipo_EvaluacionId = parseInt($('#input_at_tipo_evaluacion_otro').val())
  data.Detalle_Evaluacion = $('#input_at_detalle_evaluacion_otro').val()
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_tipo_evaluacion_post?code=uhfNS1ptjLupdjzSq0QZWGZ5ylxTo1rGbgMfgjd5paC5ciUy3XlryQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Registro anexado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarEvaluacionOtrosAccidenteTrabajo()

    //Limpiar formulario otro
    $('#input_at_tipo_evaluacion_otro').val('')
    $('#input_at_detalle_evaluacion_otro').val('')
  })
}

function atSp4CargarEvaluacionOtrosAccidenteTrabajo() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Evaluacion_Accidente`;

  let data = {
    idHC : IdAT // id de accidente de trabajo
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['TipoEvaluacionAccidente'] = response.TipoEvaluacionAccidente
    atSp4RenderEvaluacionOtrosAccidenteTrabajo()
  })
}

function atSp4RenderEvaluacionOtrosAccidenteTrabajo(){

    if(paObj_at.TipoEvaluacionAccidente.length) {

      $('#dat_at_cant_evaluacion_otro').text(`${paObj_at.TipoEvaluacionAccidente.length == 1 ? '01' : paObj_at.TipoEvaluacionAccidente.length } registros`)
      $('#content_sp4_AT_table_evaluacion_otro').html('')
      
      paObj_at.TipoEvaluacionAccidente.map(at_eval => {
        $('#content_sp4_AT_table_evaluacion_otro').append(`
          <tr>
            <td>${at_eval.Descripcion}</td>
            <td>${at_eval.Detalle_Evaluacion}</td>
            <td>
              <button id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page" onclick="atSp4ConfirmarEliminarOtrosAccidenteTrabajo(${at_eval.Id})">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else{
      $('#dat_at_cant_evaluacion_otro').text(`0 registros`)
      $('#content_sp4_AT_table_evaluacion_otro').html(`
        <tr>
          <td colspan="3" class="text-center">No se encontraron resultados.</td>
        </tr>
      `)
    }
}

function atSp4ConfirmarEliminarOtrosAccidenteTrabajo(Id_eval){


    const at_eval = paObj_at['TipoEvaluacionAccidente'].find( item => item.Id = Id_eval)

    if(!at_eval) return

    Swal.fire({
    title: "Eliminar evalauación.",
    html: `
       <p>Está por eliminar la evaluación registrada.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarEvaluacionOtrosAccidenteTrabajo(at_eval)
    }
  });
}

function atSp4EliminarEvaluacionOtrosAccidenteTrabajo(data) {
  
  data.Id1 = data.Id
  data.accion = 2
  data.Accidente_TrabajoId = IdAT
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_tipo_evaluacion_post?code=uhfNS1ptjLupdjzSq0QZWGZ5ylxTo1rGbgMfgjd5paC5ciUy3XlryQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Registro eliminado correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarEvaluacionOtrosAccidenteTrabajo()
  })
}

//--------------------------FUNCIONES DIAGNOSTICO CIE10 ------------------------------//

function atSp4GuardarDiagnosticoCie10AccidenteTrabajo() {
  
  if (validationForm('check-diagnosticocie10').length > 0) {
    return;
  }

  let data = {};
  data.Id1 = 0
  data.AccidenteTrabajoId = IdAT
  data.Diagnostico = $('#sp4_at_diagCIE10_diag').val()
  data.CIE10 = $('#dat_amT_cie_diagnostico1').val()
  data.SistemaAfectado = $('#input_at_sistema_afectado').val() ? $('#input_at_sistema_afectado').val() : 15
  data.SeccionAfectada = $('#input_at_seccion_afectada').val() ? $('#input_at_seccion_afectada').val() : 11
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_cei10_post?code=c5KSdibrY/yJd/a8B3NuDghnq4BFLf6HI1ubqQcaA/66q/8kbLxvoQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Diágnostico CIE10 anexado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarDiagnosticoCie10AccidenteTrabajo()
    
    //Limpiar formulario 
    $('#sp4_at_diagCIE10_diag').val('')
    $('#sp4_at_diagCIE10_cie10').val('')
    $('#dat_amT_cie_diagnostico1').val('')
    $('#input_at_sistema_afectado').val('')
    $('#input_at_seccion_afectada').val('')  

  })
}

function atSp4CargarDiagnosticoCie10AccidenteTrabajo() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Accidente_CIE10`;

  let data = {
    idHC : IdAT // id de accidente de trabajo
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['listaCIE10_Accidente'] = response.listaCIE10_Accidente
    atSp4RenderDiagnosticoCie10AccidenteTrabajo()
  })
}

function atSp4RenderDiagnosticoCie10AccidenteTrabajo(){

    if(paObj_at.listaCIE10_Accidente.length) {

      $('#dat_at_cant_diagCIE10').text(`${paObj_at.listaCIE10_Accidente.length == 1 ? '01' : paObj_at.listaCIE10_Accidente.length } registros`)
      $('#content_sp4_AT_table_diagCIE10').html('')
      
      paObj_at.listaCIE10_Accidente.map(at_cie10 => {
        $('#content_sp4_AT_table_diagCIE10').append(`
          <tr>
            <td>${at_cie10.Diagnostico}</td>
            <td>${at_cie10.Code_D}</td>
            <td>${ moment(new Date(at_cie10.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>${at_cie10.Especilidades}</td>
            <td>${at_cie10.SistemaAfectado_D}</td>
            <td>${at_cie10.SeccionAfectada_D}</td>
            <td>
              <button id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page" onclick="atSp4ConfirmarEliminarDiagnosticoCie10AccidenteTrabajo(${at_cie10.Id})">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else{

      $('#dat_at_cant_diagCIE10').text(`0 registros`)
      $('#content_sp4_AT_table_diagCIE10').html(`
        <tr>
          <td colspan="7" class="text-center">No se encontraron resultados.</td>
        </tr>
      `)
    }
}

function atSp4ConfirmarEliminarDiagnosticoCie10AccidenteTrabajo(Id){
  
  const data = paObj_at['listaCIE10_Accidente'].find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar diagnóstico.",
    html: `
       <p>Está por eliminar el diagnóstico CIE 10 registrado.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarDiagnosticoCie10AccidenteTrabajo(data)
    }
  });
}

function atSp4EliminarDiagnosticoCie10AccidenteTrabajo(data){

  data.Id1 = data.Id
  data.AccidenteTrabajoId = IdAT
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 2

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_cei10_post?code=c5KSdibrY/yJd/a8B3NuDghnq4BFLf6HI1ubqQcaA/66q/8kbLxvoQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Diágnostico CIE10 eliminado correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarDiagnosticoCie10AccidenteTrabajo()
  })
}

//--------------------------FUNCIONES ATENCION EN DSO ------------------------------//

function atSp4GuardarDsoAccidenteTrabajo(){

  if (validationForm('check-dso').length > 0) {
    return;
  }

  let data = {};
  data.Id1 = 0
  data.AccidenteTrabajoId = IdAT
  data.Registro_Atencion = $('#input_at_registro_atencion_dso').val()
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_dso_post?code=qElJjqryaTAVY7JZ32ycFcApveFCbGlW4Q26K5VP3LORfKQzij5MeQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Atención en DSO registrada con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarDsoAccidenteTrabajo()
    //Limpiar formulario
    $('#input_at_registro_atencion_dso').val('')
  })
}

function atSp4CargarDsoAccidenteTrabajo() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Accidente_DSO`;

  let data = {
    idHC : IdAT // id de accidente de trabajo
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['listaDSO_Accidente'] = response.listaDSO_Accidente
    atSp4RenderDsoAccidenteTrabajo()
  })
}

function atSp4RenderDsoAccidenteTrabajo() {

    if(paObj_at.listaDSO_Accidente.length) {

      $('#dat_at_cant_atencion_dso').text(`${paObj_at.listaDSO_Accidente.length == 1 ? '01' : paObj_at.listaDSO_Accidente.length } registros`)
      $('#content_sp4_AT_table_atencion_dso').html('')
      
      paObj_at.listaDSO_Accidente.map(at_dso => {
        $('#content_sp4_AT_table_atencion_dso').append(`
          <tr>
            <td>${at_dso.Detalle}</td>
            <td>${ moment(new Date(at_dso.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>${ moment(at_dso.Hora,'HH:mm:ss').format('HH:mm')}</td> 
            <td>
              <button id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page" onclick="atSp4ConfirmarEliminarDsoAccidenteTrabajo(${at_dso.Id})">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else{
      $('#dat_at_cant_atencion_dso').text(`0 registros`)
      $('#content_sp4_AT_table_atencion_dso').html(`
        <tr>
          <td colspan="4" class="text-center">No se encontraron resultados.</td>
        </tr>
      `) 
    }
}

function atSp4ConfirmarEliminarDsoAccidenteTrabajo(Id){

    const data = paObj_at['listaDSO_Accidente'].find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar atención DSO.",
    html: `
       <p>Está por eliminar atención en DSO registrada.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarEvaluacionDsoAccidenteTrabajo(data)
    }
  });
}

function atSp4EliminarEvaluacionDsoAccidenteTrabajo(data) {
  
  data.Id1 = data.Id
  data.AccidenteTrabajoId = IdAT
  data.Registro_Atencion = data.Detalle
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 2

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_dso_post?code=qElJjqryaTAVY7JZ32ycFcApveFCbGlW4Q26K5VP3LORfKQzij5MeQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Atención en DSO eliminada correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarDsoAccidenteTrabajo()
  })
}

//--------------------------FUNCIONES ATENCIÓN CENTRO DE SALUD ------------------------------//

function atSp4GuardarAtencionCentroAccidenteTrabajo() {

  if (validationForm('check-centro-salud').length > 0) {
    return;
  }

  let data = {};
  data.Id1 = 0
  data.Accidente_TrabajoId = IdAT
  data.Detalle = $('#input_at_detalle_atencion_centro_salud').val()
  data.Centro_Salud = $('#input_at_centro_atencion_centro_salud').val()
  data.CreadoPor = "SHO"
  data.accion = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_atencion_centro_salud_post?code=j1fl8Y9xFGzbQ5c6WSzHqRxdPWonaKnannniCzs7s2xrah5mSdfdbQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Atención en centro de salud anexada con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarAtencionCentroAccidenteTrabajo()

    //Limpiar formulario
    $('#input_at_detalle_atencion_centro_salud').val('')
    $('#input_at_centro_atencion_centro_salud').val('')
  })
}

function atSp4CargarAtencionCentroAccidenteTrabajo() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Accidente_Centro_Salud`;

  let data = {
    idHC : IdAT // id de accidente de trabajo
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['listaCentro_Salud_Accidente'] = response.listaCentro_Salud_Accidente
    atSp4RenderAtencionCentroAccidenteTrabajo()
  })
}

function atSp4RenderAtencionCentroAccidenteTrabajo() {

    if(paObj_at.listaCentro_Salud_Accidente.length) {

      $('#dat_at_cant_atencion_centro_salud').text(`${paObj_at.listaCentro_Salud_Accidente.length == 1 ? '01' : paObj_at.listaCentro_Salud_Accidente.length } registros`)
      $('#content_sp4_AT_table_atencion_centro_salud').html('')
      
      paObj_at.listaCentro_Salud_Accidente.map(at_centro => {
        $('#content_sp4_AT_table_atencion_centro_salud').append(`
          <tr>
            <td>${at_centro.Detalle}</td>
            <td>${ moment(new Date(at_centro.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>${ moment(at_centro.Hora,'HH:mm:ss').format('HH:mm')}</td> 
            <td>
              <button id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page" onclick="atSp4ConfirmarEliminarAtencionCentroAccidenteTrabajo(${at_centro.Id})">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else{
      $('#dat_at_cant_atencion_centro_salud').text(`0 registros`)
      $('#content_sp4_AT_table_atencion_centro_salud').html(`
        <tr>
          <td colspan="4" class="text-center">No se encontraron resultados.</td>
        </tr>
      `)

    }
}

function atSp4ConfirmarEliminarAtencionCentroAccidenteTrabajo(Id){

  const data = paObj_at['listaCentro_Salud_Accidente'].find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar atención en centro.",
    html: `
       <p>Está por eliminar la atención en centro.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarAtencionCentroAccidenteTrabajo(data)
    }
  });
}

function atSp4EliminarAtencionCentroAccidenteTrabajo(data){

  data.Id1 = data.Id
  data.accion = 2
  data.Accidente_TrabajoId = IdAT
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_atencion_centro_salud_post?code=j1fl8Y9xFGzbQ5c6WSzHqRxdPWonaKnannniCzs7s2xrah5mSdfdbQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Atención en centro de salud eliminada correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarAtencionCentroAccidenteTrabajo()
  })
}

//--------------------------FUNCIONES DESCANSO ------------------------------//

async function atSp4CrearDescansoMedicoVacio() {

  let data = {}

  data.IdHC = idHC
  data.IdDescM = 0
  data.OrigenId = IdAT
  data.TipoOrigen = 6
  data.A_IdAtencionMedica = 0
  data.A_DniTrabajador = ""
  data.A_DniTrabajador = ""
  data.A_NombreTrabjador = ""
  data.A_ApellidosTrabajador = ""
  data.A_Empresa = ""
  data.A_Origen = 0

  data.A_Gerencia = 0
  data.A_Planta =  0
  data.A_Area = 0,
  data.A_PuestoTrabajo = ""
  data.B_PersonalSolicitud = ""
  data.B_PersonalIdHash =  ""
  data.B_TipoContingencia = 0
  data.B_DescansoPorEnfermedad = 0

  data.B_CMP =  "",
  data.B_CantidadDias = 0
  data.B_DiasAcumulados = 0
  data.B_FechaIni = moment(new Date()).format('YYYY-MM-DD')

  data.B_FechaFin = moment(new Date()).format('YYYY-MM-DD')
  data.B_HuboAltaMedica = 1
  data.B_EstableceDescanso = 1
  data.B_Particular = ""

  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'


  let url = `${apiUrlsho}/api/hce_Post_023_descanso_medico?code=ZMMXyYQzh2QaROc92eMe55BMlkkckJVD9r9mxqTmHFpCUXm75EvSsQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    paObj_at['IdDescansoMedico'] = response.Id
    id_DM = response.Id
  })
}

async function atSp4GuardarDescansoMedico() {

  let data = {}

  data.IdHC = idHC
  data.IdDescM = paObj_at.IdDescansoMedico
  data.OrigenId = IdAT
  data.TipoOrigen = 6
  data.A_IdAtencionMedica = 0
  
  data.A_DniTrabajador = ""
  data.A_NombreTrabjador = ""
  data.A_ApellidosTrabajador = ""
  data.A_Empresa = ""
  data.A_Origen = 0
  data.A_Gerencia = 0
  data.A_Planta =  0
  data.A_Area = 0
  data.A_PuestoTrabajo = ""

  data.B_PersonalSolicitud = $('#dat_des_personal_salud_v').val()
  data.B_PersonalIdHash =  ""
  data.B_TipoContingencia = $('#dat_des_tipo_contingencia_v').val()
  data.B_DescansoPorEnfermedad = $('#dat_des_descanso_enfermedad_v').val()

  data.B_CMP = $('#dat_des_cmp_v').val(),
  data.B_CantidadDias = $('#dat_des_cant_dias_v').val()
  data.B_DiasAcumulados = $('#dat_des_dias_ac_v').val()
  data.B_FechaIni = $('#dat_des_fi_v2').val()
  data.B_FechaFin = $('#dat_des_ff_v').val()
  data.B_HuboAltaMedica = $('#dat_des_alta_med_v').val()
  data.B_EstableceDescanso = $('#dat_des_establecimiento_emisión_v').val()
  data.B_Particular = $('#dat_des_particular_v').val()

  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'

  let url = `${apiUrlsho}/api/hce_Post_023_descanso_medico?code=ZMMXyYQzh2QaROc92eMe55BMlkkckJVD9r9mxqTmHFpCUXm75EvSsQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    atSp4CargarListasAccidenteTrabajo()
    
    $('#modalAccidenteTrabajo').modal('hide')

    Swal.fire({
      title: 'Descanso médico registrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
    
  })
}

function atSp4CargarListasAccidenteTrabajo() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_AT_Accidente_Trabajo`;

  let data = {
    idHC : IdAT // id del accidente de trabajo
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['AccidentesT'] = response.AccidentesT[0]
    atSp4RenderDecansosMedicos()
    atSp4RenderTransferencias()
    atSp4RenderEvaluaciones()
    atSp4RenderSeguimientoAccidenteTrabajo()
  })
}

function atSp4RenderDecansosMedicos(){

    if(paObj_at.AccidentesT.DescansoAT.length) {

      $('#at_nuevo_descanso').hide()
      $('#at_lista_descanso').show()

      $('#dat_at_cant_descanso').text(`${paObj_at.AccidentesT.DescansoAT.length == 1 ? '01' : paObj_at.AccidentesT.DescansoAT.length } registros`)
      $('#content_sp4_AT_table_descanso').html('')
      
      paObj_at.AccidentesT.DescansoAT.map(at_descanso => {
        $('#content_sp4_AT_table_descanso').append(`
          <tr>
            <td>${ moment(new Date(at_descanso.B_FechaIni)).format('DD/MM/YYYY')}</td>
            <td>${ moment(new Date(at_descanso.B_FechaFin)).format('DD/MM/YYYY')}</td>
            <td>${at_descanso.B_CantidadDias}</td>
            <td>${at_descanso.B_DiasAcumulados}</td>
            <td>${at_descanso.DiasRestantes}</td>
            <td>${at_descanso.Diagnostico ? at_descanso.Diagnostico : 'No disponible'}</td>
            <td>${at_descanso.TipoContingenciaName}</td>
            <td>${at_descanso.Establecimiento}</td>
            <td>
              <button onclick="atSp4ConfirmarEliminarDescanso(${at_descanso.Id})" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else {

      if(modeAccidenteTrabajo == 'create'){
        
        $('#at_nuevo_descanso').show()
        $('#at_lista_descanso').hide()
      }
    }
}

function atSp4ConfirmarEliminarDescanso(Id) {

  const data = paObj_at.AccidentesT.DescansoAT.find( item => item.Id = Id)

  if(!data) return

  Swal.fire({
    title: "Eliminar descanso médico.",
    html: `
       <p>Está por eliminar un descanso médico registrado.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarDescanso(data)
    }
  }); 
}

function atSp4EliminarDescanso(descanso){
  
  let data = {}

  data.IdHC = idHC
  data.IdDescM = descanso.Id

  let url = `${apiUrlsho}/api/hce_Post_030_descanso_medico_eliminadoLogico?code=SJylajP1NKUiZTWmwwJe6BLaBixuD/xsbaFSHQ07P9ksOYkfFxxkTw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    atSp4CargarListasAccidenteTrabajo()
    
    $('#modalAccidenteTrabajo').modal('hide')

    Swal.fire({
      title: 'Descanso médico eliminado correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
    
  })
}


//--------------------------FUNCIONES TRANSFERENCIA ------------------------------//

async function _init_atSp4NuevaTransferencia(){
  
  showLoading();

  navTransferenciaAT = true
  paObj_at['IdTransferencia'] = 0
  
  $('#dat_int_tran_fe_transferencia').val(moment(new Date()).format('YYYY-MM-DD'))
  $('.hide-at').remove()
  
  await atSp4CrearTransferenciaVacia()

  // Cargar inputs
  let obj = paObj_at['HistoriaClin'];
  
  $('#dat_int_tran_codigo').val(obj.NroDocumento_Trabajador_H);
  $('#dat_int_tran_nombre').val(obj.Nombres_Trabajador_H);
  $('#dat_int_tran_apellido').val(obj.Apellidos_Trabajador_H);
  $('#dat_int_tran_puesto').val(obj.PuestoTrabajo_Empresa_H);
  $('#dat_int_tran_edad').val(obj.Edad_Trabajador_H);

  at_SedeAreaGerencia.Gerencias.forEach((e) => {
    if (e.Id == obj.GerenciaId_Empresa_H) {
      $('#dat_int_tran_gerencia').html(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })

  at_SedeAreaGerencia.Sedes.forEach((e) => {
    if (e.Id == obj.PlantaId_Empresa_H) {
      $('#dat_int_tran_planta').html(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })

  at_SedeAreaGerencia.Areas.forEach((e) => {
    if (e.Id == obj.AreaId_Empresa_H) {
      $('#dat_int_tran_area').html(`
        <option value="${e.Id}" selected>${e.Description}</option>
      `)
    }
  })
  
  await atSp4CargarSistemaAfectado()
  await atSp4CargarSeccionAfectada()
  atSpRenderSelectsSistemaAfectado('dat_int_tran_sis_af','dat_int_tran_seccion_af')
  atSpRenderSelectsSistemaAfectado('dat_int_tran_sist_af_2','dat_int_tran_seccion_af_2')
  
  hideLoading();
}

function atSp4CrearTransferenciaVacia() {

  let data = {}

  data.IdHC = idHC
  data.IdHashUser = ""
  data.C_IdHashRecibido = ""
  data.IdTransf = 0
  data.IdOrigen = IdAT
  data.IdTipoOrigen = 6

  data.A_Empresa= ""
  data.A_DireccionTrabajador = ""
  data.A_Telefono = ""
  data.A_Anamnesis = ""
  data.B_Tratamiento = ""
  data.B_MotivoTrasferencia = ""
  data.B_HoraAtencionDSO = "00:00"
  data.B_HoraEvaluacion = "00:00"

  data.C_Tratamiento = ""
  data.C_RecibidoPor = ""
  data.C_IdHashRecibidodata = ""
  data.C_Lugar = ""
  data.C_Fecha =  moment(new Date()).format('YYYY-MM-DD')
  data.CodigoOrigen = ""

  data.IdSV = 0 //Id de signos vitales
  data.PresionArterial_SV= 0
  data.FrecuenciaCardiaca_SV= 0
  data.FrecuenciaRespiratoria_SV= 0
  data.Temperatura_SV= 0
  data.PesoKg_SV= 0
  data.Talla_SV= 0
  data.Saturacion_SV= 0
  data.IndiceMasaCorporal_SV= 0
  data.PerimetroAbdominal_SV= 0

  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'


  let url = `${apiUrlsho}/api/hce_Post_017_transferencias?code=x69HnqaaZgWZQTcc61KfkEokA8TYLE6bRUfIxQIwrhOkrrFiYcDVbQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    paObj_at['IdTransferencia'] = response.Id
    istAud = response.Id
  })
}

async function atSp4GuardarTrasferencia(){

  let data = {}

  data.IdHC = idHC
  data.IdHashUser = ""
  data.C_IdHashRecibido = ""
  data.IdTransf = paObj_at.IdTransferencia
  data.IdOrigen = IdAT
  data.IdTipoOrigen = 6

  data.A_Empresa= $('#dat_int_tran_empresa').val()
  data.A_DireccionTrabajador = $('#dat_int_tran_direcciont').val()
  data.A_Telefono = $('#dat_int_tran_telefono').val()
  data.A_Anamnesis = $('#dat_int_tran_anamesis').val()
  data.B_Tratamiento = $('#dat_int_tran_trata').val()
  data.B_MotivoTrasferencia = $('#dat_int_tran_motivo_trans').val()
  data.B_HoraAtencionDSO = $('#dat_int_tran_h_dso').val()
  data.B_HoraEvaluacion = $('#dat_int_tran_h_evol').val()

  data.C_Tratamiento = $('#dat_int_tran_trata2').val()
  data.C_RecibidoPor = $('#dat_int_tran_h_dso2').val()
  data.C_IdHashRecibidodata = getCookie("vtas_id_hash"+sessionStorage.tabVisitasa)
  data.C_Lugar = $('#dat_int_tran_lugard2').val()
  data.C_Fecha =  $('#dat_int_tran_fe_transferencia').val()
  data.CodigoOrigen = "Registro a Demanda"

  data.IdSV = 0 //Id de signos vitales
  data.PresionArterial_SV=  $('#dat_int_tran_sv1').val()
  data.FrecuenciaCardiaca_SV=  $('#dat_int_tran_sv2').val()
  data.FrecuenciaRespiratoria_SV = $('#dat_int_tran_sv3').val()
  data.Temperatura_SV= $('#dat_int_tran_sv4').val()
  data.PesoKg_SV=  $('#dat_int_tran_sv5').val()
  data.Talla_SV= $('#dat_int_tran_sv6').val()
  data.Saturacion_SV= $('#dat_int_tran_sv7').val()
  data.IndiceMasaCorporal_SV=  $('#dat_int_tran_sv8').val()
  data.PerimetroAbdominal_SV= $('#dat_int_tran_sv9').val()

  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'


  let url = `${apiUrlsho}/api/hce_Post_017_transferencias?code=x69HnqaaZgWZQTcc61KfkEokA8TYLE6bRUfIxQIwrhOkrrFiYcDVbQ==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    atSp4CargarListasAccidenteTrabajo()
    
    $('#modalAccidenteTrabajo').modal('hide')

    Swal.fire({
      title: 'Transferencia registrada con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
  })
}

function atSp4RenderTransferencias(){

    if(paObj_at.AccidentesT.transferenciaAT.length) {

      $('#at_nueva_transferencia').hide()
      $('#at_lista_transferencia').show()

      $('#dat_at_cant_transferencia').text(`${paObj_at.AccidentesT.transferenciaAT.length == 1 ? '01' : paObj_at.AccidentesT.transferenciaAT.length } registros`)
      $('#content_sp4_AT_table_transferencia').html('')
      
      paObj_at.AccidentesT.transferenciaAT.map(at_transf => {
        $('#content_sp4_AT_table_transferencia').append(`
          <tr>
            <td>${at_transf.Informe}</td>
            <td>${at_transf.Establecimento}</td>
            <td>${ moment(new Date(at_transf.A_FechaTransferencia)).format('DD/MM/YYYY')}</td>
            <td>${at_transf.Diagnostico ? at_transf.Diagnostico : 'No disponible' }</td>
            <td>
              <button onclick="atSp4ConfirmarEliminarTransferencia(${at_transf.Id})" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else {

      if(modeAccidenteTrabajo == 'create'){
        
        $('#at_nueva_transferencia').show()
        $('#at_lista_transferencia').hide()
      }
    }
}

function atSp4ConfirmarEliminarTransferencia(Id){

  const data = paObj_at.AccidentesT.transferenciaAT.find( item => item.Id = Id)

  if(!data) return

  Swal.fire({
    title: "Eliminar transferencia.",
    html: `
       <p>Está por eliminar una transferencia registrada.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarTransferencia(data)
    }
  }); 
}

function atSp4EliminarTransferencia(transferencia){

  let data = {}

  data.IdHC = idHC
  data.IdTransf = transferencia.Id

  let url = `${apiUrlsho}/api/hce_Post_022_transferencias_eliminado?code=esni77EOqjKGGAhgSs2Rs5QABYhvILXpjvRvRSWkbTv/ddZV8qmaYw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    atSp4CargarListasAccidenteTrabajo()
    
    $('#modalAccidenteTrabajo').modal('hide')

    Swal.fire({
      title: 'Trasnferencia eliminada correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
    
  })
}

//--------------------------FUNCIONES SEGUIMIENTO ------------------------------//

function atSp4ConfirmGuardarSeguimiento() {

  if (validationForm('check-seguimiento').length > 0) {
    return;
  }
  
  Swal.fire({
    title: "Guardar registro de seguimiento.",
    html: `
       <p>Está por guardar el registro de seguimiento.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4GuardarSeguimientoAccidenteTrabajo()
    }
  });
}

function atSp4ConfirmCancelarSeguimiento() {

  let nombres = $("#dat_at_nombres_trabajador").val();
  let apellidos = $("#dat_at_apellidos_trabajador").val();
  
  Swal.fire({
    title: "Cancelar registro de seguimiento.",
    html: `
       <p>Está por cancelar el registro de seguimiento de</p>
       <p>${nombres} ${apellidos}</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      $('#modalAccidenteTrabajo').modal('hide')
    }
  });
}

function atSp4GuardarSeguimientoAccidenteTrabajo() {

  let data = {};
  data.Id1 = 0
  data.Accidente_TrabajoId = IdAT
  data.CreadoFecha = $('#input_at_fecha_seguimiento').val()
  data.Hora = $('#input_at_hora_seguimiento').val()
  data.Comentario = $('#input_at_comentario_seguimiento').val()
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_atencion_centro_reg_seguimiento_post?code=RMGs541Mv7PEU97tpddeuyGj2A6WVhz6zXZoULfzWG2xT9dplnQ2Hg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    $('#modalAccidenteTrabajo').modal('hide')

    Swal.fire({
      title: 'Seguimiento registrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
    atSp4CargarListasAccidenteTrabajo()
  })
}

function atSp4CargaSeguimientoAccidenteTrabajo() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Seguimiento_Accidente`;

  let data = {
    idHC : IdAT // id de accidente de trabajo
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['lista_Seguimiento'] = response.lista_Seguimiento
    atSp4RenderSeguimientoAccidenteTrabajo()
  })
}

function atSp4RenderSeguimientoAccidenteTrabajo() {

    if(paObj_at.AccidentesT.SeguimientoAT.length){
      
      $('#at_nuevo_seguimiento').hide()
      $('#at_lista_seguimiento').show()

      $('#dat_at_cant_seguimiento').text(`${paObj_at.AccidentesT.SeguimientoAT.length == 1 ? '01' : paObj_at.AccidentesT.SeguimientoAT.length } registros`)
      $('#content_sp4_AT_table_seguimiento').html('')
      
      paObj_at.AccidentesT.SeguimientoAT.map(at_seg => {
        $('#content_sp4_AT_table_seguimiento').append(`
          <tr>
            <td>${ moment(new Date(at_seg.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>${ moment(at_seg.Hora,'HH:mm:ss').format('HH:mm')}</td> 
            <td>${ at_seg.Comentario }</td> 
            <td>
                <div class="dropdown">
                  <button class=" btn-link dropdown textVertical" style='outline:none !important;' id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <label class='textVertical'>...</label>
                  </button>
                  <div class="dropdown-menu" style="padding: 8px 0px;">  
                      <!--<a class="dropdown-item d-flex align-items-center">
                        <img src="./images/sho/edit.svg" alt="" />&nbsp;&nbsp; Editar
                      </a>-->
                      <a class="dropdown-item d-flex align-items-center hide-detail-page" onclick="atSp4ConfirmarEliminarSeguimiento(${at_seg.Id})">
                        <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                        &nbsp;&nbsp; Eliminar
                      </a>
                  </div>
                </div>
            </td>
          </tr>
        `)
      })

    }else{
      if(modeAccidenteTrabajo == 'create'){
        
        $('#at_nuevo_seguimiento').show()
        $('#at_lista_seguimiento').hide()
      }
    }
}

function atSp4ConfirmarEliminarSeguimiento(Id){
  
  const data = paObj_at.AccidentesT.SeguimientoAT.find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar seguimiento.",
    html: `
       <p>Está por eliminar seguimiento registrado.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarSeguimiento(data)
    }
  });
}

function atSp4EliminarSeguimiento(data){

  data.Id1 = data.Id
  data.Accidente_TrabajoId = IdAT
  data.CreadoPor = "SHO"
  data.ModificadoPor = 'SHO'
  data.FechaModificacion = moment(new Date).format("YYYY-MM-DD")
  data.CreadoFecha = moment(new Date).format("YYYY-MM-DD")
  data.accion = 2

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_atencion_centro_reg_seguimiento_post?code=RMGs541Mv7PEU97tpddeuyGj2A6WVhz6zXZoULfzWG2xT9dplnQ2Hg==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    $('#modalAccidenteTrabajo').modal('hide')

    Swal.fire({
      title: 'Seguimiento eliminado correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarListasAccidenteTrabajo() 
  })
}

//--------------------------FUNCIONES ALTA ESPECIALIDADES ------------------------------//

async function _init_atSp4NuevaAltaAccidenteTrabajo() {

  showLoading();

  await atSp4CrearAltaVacia()
  atSpRenderSelectsSistemaAfectado('input_at_sistema_afectado_alta','input_at_seccion_afectada_alta')

  hideLoading();
}

async function atSp4CrearAltaVacia() {

  let data = {}

  data.Id1 = 0
  data.accion = 0
  data.Accidente_TrabajoId = IdAT
  data.Rne_Especialista = ''
  data.Nombre_Especialista = ''
  data.Cmp_Especialista = ''
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'
  data.Fecha_Alta =  moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS')

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_alta_especialidades_post?code=H5/sGe2oVbTkogbTya5zYhyMVqcuqL1bmh/mABXDU5YNkoENGZRiHw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    paObj_at['IdAlta'] = response.Id
  })
}

function atSp4ConfirmGuardarAlta() {

  if (validationForm('check-alta').length > 0) {
    return;
  }
  
  Swal.fire({
    title: "Guardar alta de especialidades.",
    html: `
       <p>Está por guardar el alta de especialidades.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4GuardarAlta()
    }
  });
}

function atSp4GuardarAlta() {

  let data = {}

  data.Id1 = paObj_at.IdAlta
  data.accion = 1
  data.Accidente_TrabajoId = IdAT
  data.Fecha_Alta =  $('#input_at_fecha_alta').val()
  data.Rne_Especialista = $('#input_at_rne_especialista_alta').val()
  data.Nombre_Especialista = $('#input_at_nombre_especialista_alta').val()
  data.Cmp_Especialista = $('#input_at_cmp_especialista_alta').val()
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_alta_especialidades_post?code=H5/sGe2oVbTkogbTya5zYhyMVqcuqL1bmh/mABXDU5YNkoENGZRiHw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    atSp4CargarAltas()
    $('#modalAccidenteTrabajo').modal('hide')
    
    Swal.fire({
      title: 'Alta de especialidaes registrada con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

  })
}

function atSp4CargarAltas() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Alta_Especialidad_Accidente`;

  let data = {
    idHC : IdAT // id del accidente de trabajo
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['alta_Especialidad_Accidente'] = response.alta_Especialidad_Accidente
    atSp4RenderAltas()
  })
}

function atSp4RenderAltas(){

    if(paObj_at.alta_Especialidad_Accidente.length) {

      $('#at_nuevo_alta').hide()
      $('#at_lista_alta').show()

      $('#dat_at_cant_alta').text(`${paObj_at.alta_Especialidad_Accidente.length == 1 ? '01' : paObj_at.alta_Especialidad_Accidente.length } registros`)
      $('#content_sp4_AT_table_alta').html('')
      
      paObj_at.alta_Especialidad_Accidente.map(at_alta => {
        $('#content_sp4_AT_table_alta').append(`
          <tr>
            <td>${at_alta.Diagnostico}</td>
            <td>${at_alta.documento_escaneado}</td>
            <td>${ moment(new Date(at_alta.Fecha_Alta)).format('DD/MM/YYYY')}</td>
            <td>${at_alta.Rne_Especialista}</td>
            <td>${at_alta.Nombre_Especialista}</td>
            <td>
              <button onclick="atSp4ConfirmarEliminarAlta(${at_alta.Id})" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else {

      if(modeAccidenteTrabajo == 'create'){
        
        $('#at_nuevo_alta').show()
        $('#at_lista_alta').hide()
      }
    }
}

function atSp4ConfirmCancelarAlta() {

  let nombres = $("#dat_at_nombres_trabajador").val();
  let apellidos = $("#dat_at_apellidos_trabajador").val();
  
  Swal.fire({
    title: "Cancelar alta de especialidades.",
    html: `
       <p>Está por cancelar el alta de especialidades</p>
       <p>${nombres} ${apellidos}</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      $('#modalAccidenteTrabajo').modal('hide')
    }
  });
}

function atSp4ConfirmarEliminarAlta(Id){
  
  const data = paObj_at['alta_Especialidad_Accidente'].find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar alta de especialidades.",
    html: `
       <p>Está por eliminar el alta de especialidades.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarAlta(data)
    }
  });
}

function atSp4EliminarAlta(data) {

  data.Id1 = paObj_at.IdAlta
  data.accion = 2
  data.Accidente_TrabajoId = IdAT
  data.Cmp_Especialista = 'cmp'
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_alta_especialidades_post?code=H5/sGe2oVbTkogbTya5zYhyMVqcuqL1bmh/mABXDU5YNkoENGZRiHw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    atSp4CargarAltas()
    
    Swal.fire({
      title: 'Alta de especialidaes eliminada correctamente.',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

  })
}


function atSp4GuardarDiagnosticoCie10Alta() {
  
  if (validationForm('check-diagnosticocie10-alta').length > 0) {
    return;
  }

  let data = {};
  data.Id1 = 0
  data.AltaEspecialistaId = paObj_at.IdAlta
  data.Diagnostico = $('#sp4_at_diagCIE10_diag_alta').val()
  data.CIE10 = $('#dat_at_cie10_alta').val()
  data.SistemaAfectado = $('#input_at_sistema_afectado_alta').val() ? $('#input_at_sistema_afectado_alta').val() : 15
  data.SeccionAfectada = $('#input_at_seccion_afectada_alta').val() ? $('#input_at_seccion_afectada_alta').val() : 11
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_alta_especialidades_cei10_post?code=t9kTZd97vIaY3VIVkpmGTWS6Ki4RfiXpyDKV3OsiRo8qEeFi5to0NA==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Diágnostico CIE10 registrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarDiagnosticoCie10Alta()

    //Limpiar formulario 
    $('#sp4_at_diagCIE10_diag_alta').val('')
    $('#dat_at_cie10_alta').val('')
    $('#sp4_at_diagCIE10_cie10_alta').val('')
    $('#input_at_sistema_afectado_alta').val('') 
     $('#input_at_seccion_afectada_alta').val('') 
  })
}

function atSp4CargarDiagnosticoCie10Alta() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Alta_medica_con_CIE10`;

  let data = {
    idHC : paObj_at.IdAlta // id del alta especialidades
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['CE10_Esp_Medicas'] = response.CE10_Esp_Medicas
    atSp4RenderDiagnosticoCie10Alta()
  })
}

function atSp4RenderDiagnosticoCie10Alta(){

    if(paObj_at.CE10_Esp_Medicas.length) {

      $('#dat_at_cant_diagCIE10_alta').text(`${paObj_at.CE10_Esp_Medicas.length == 1 ? '01' : paObj_at.CE10_Esp_Medicas.length } registros`)
      $('#content_sp4_AT_table_diagCIE10_alta').html('')
      
      paObj_at.CE10_Esp_Medicas.map(at_cie10_alta => {
        $('#content_sp4_AT_table_diagCIE10_alta').append(`
          <tr>
            <td>${at_cie10_alta.Diagnostico}</td>
            <td>${at_cie10_alta.Code_D}</td>
            <td>${ moment(new Date(at_cie10_alta.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>${at_cie10_alta.Especilidades}</td>
            <td>${at_cie10_alta.SistemaAfectado_D}</td>
            <td>${at_cie10_alta.SeccionAfectada_D}</td>
            <td>
              <button onclick="atSp4ConfirmarEliminarDiagnosticoCie10Alta(${at_cie10_alta.Id})" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else{
      $('#dat_at_cant_diagCIE10_alta').text(`0 registros`)
      $('#content_sp4_AT_table_diagCIE10_alta').html(`
        <tr>
          <td colspan="7" class="text-center">No se encontraron resultados.</td>
        </tr>
      `)
    }
}

function atSp4GuardarAdjuntoAlta(data) {
  let url = apiUrlsho+`/api/hce_accidente_trabajo_alta_especialidades_adjunto_post?code=9RPbe1I5ohqGr8nYVPWwlvo0MrLGWZookfKDFYyhUgkeqBmMv8aa3g==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    "data": JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    Swal.fire({
      title: 'Archivo adjunto anexado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    const adjunto = response

    $('#secion_at_adjuntar_alta').hide()
    $('#secion_at_adjunto_alta').show()

    $('#content_sp4_AT_table_adjunto_alta').html('')
    $('#content_sp4_AT_table_adjunto_alta').append(`
      <tr>
        <td>${adjunto.NombreArchivo}</td>
        <td>${moment(new Date(adjunto.CreadoFecha)).format('DD/MM/YYYY')}</td>
        <td>
          <a class="btn btn-link shadow-none float-right" download="${adjunto.NombreArchivo}" href="${adjunto.ArchivoBase64}">
            <img src="./images/sho/eyeIcon.svg">
          </a>
        </td>
      </tr>
    `)
  });
}

function atSp4ConfirmarEliminarDiagnosticoCie10Alta(Id){
  
  const data = paObj_at['CE10_Esp_Medicas'].find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar diagnóstico.",
    html: `
       <p>Está por eliminar el diagnóstico CIE 10 registrado.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarDiagnosticoCie10Alta(data)
    }
  });
}

function atSp4EliminarDiagnosticoCie10Alta(data) {
  

  data.Id1 = data.Id
  data.AltaEspecialistaId = paObj_at.IdAlta
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 2

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_alta_especialidades_cei10_post?code=t9kTZd97vIaY3VIVkpmGTWS6Ki4RfiXpyDKV3OsiRo8qEeFi5to0NA==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Diágnostico CIE10 eliminado correctamente',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarDiagnosticoCie10Alta()

  })
}

//--------------------------FUNCIONES EXAMENES AUXILIARES ------------------------------//

async function _init_atSp4NuevoExamenesAuxiliares() {

  showLoading();

  if(!pluck_at.TipoExamen.length){
    await atSp4CargarDatosCombo()
  }

  paObj_at['adjuntoExamenAuxiliar'] = null
  $(`#input_at_tipo_examenes_auxiliares`).html('');
  $(`#input_at_tipo_examenes_auxiliares`).append('<option value="" selected></option>')

  pluck_at['TipoExamen'].map((Item) => {
    $(`#input_at_tipo_examenes_auxiliares`).append(`<option value="${Item.Id}">${Item.Descripcion}</option>`)
  });

  hideLoading();
}

function atSp4ConfirmGuardarExamenesAuxiliares() {

  if (validationForm('check-examenes-auxiliares').length > 0) {
    return;
  }

  Swal.fire({
    title: "Guardar examenes auxiliares.",
    html: `
       <p>Está por guardar los examenes auxiliares.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4GuardarExamenesAuxiliares()
    }
  });
}


function atSp4GuardarExamenesAuxiliares(){

  let data = {}

  data.Id1 = 0
  data.accion = 0
  data.Accidente_TrabajoId= IdAT
  data.TipoExamenId = $('#input_at_tipo_examenes_auxiliares').val()
  data.Conclusion = $('#input_at_conclusion_examenes_auxiliares').val()
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'
  data.NombreArchivo = ""
  data.ArchivoBase64 = ""
  data.FechaModificacion = $('#input_at_fecha_examenes_auxiliares').val()

  if(paObj_at.hasOwnProperty('adjuntoExamenAuxiliar') && paObj_at.adjuntoExamenAuxiliar){
      data.NombreArchivo = paObj_at.adjuntoExamenAuxiliar.NombreArchivo
      data.ArchivoBase64 = paObj_at.adjuntoExamenAuxiliar.ArchivoBase64
  }


  let url = `${apiUrlsho}/api/hce_accidente_trabajo_examenes_auxiliares_post?code=e0k2JcleRZnuG4g1ZJ/syHlska0sKyd1VqavOmt6cVkPqTh/5IgJKw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    atSp4CargarExamenesAuxiliares()

    Swal.fire({
      title: 'Examen auxiliar registrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    $('#input_at_fecha_examenes_auxiliares').val('')
    $('#input_at_conclusion_examenes_auxiliares').val('')
    $('#input_at_tipo_examenes_auxiliares').val('')
  })
}

function atSp4CargarExamenesAuxiliares() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_ExamenAuxiliar`;

  let data = {
    idHC : IdAT// id del accidente
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['iExamen_Auxiliar'] = response.iExamen_Auxiliar
    atSp4RenderExamenesAuxiliares()
    atSp4RenderExamenesAuxiliaresModal()
  })
}

function atSp4RenderExamenesAuxiliares() {

    if(paObj_at.iExamen_Auxiliar.length){

      $('#at_nuevo_examenes_auxiliares').hide()
      $('#at_lista_examenes_auxiliares').show()

      $('#dat_am_cant_examenes-auxiliares').text(`${paObj_at.iExamen_Auxiliar.length == 1 ? '01' : paObj_at.iExamen_Auxiliar.length } registros`)
      $('#content_sp4_AT_table_examenes-auxiliares').html('')
      
      paObj_at.iExamen_Auxiliar.map(at_examen => {

        const tipoExamen = pluck_at.TipoExamen.find(item => item.Id == at_examen.TipoExamenId )

        $('#content_sp4_AT_table_examenes-auxiliares').append(`
          <tr>
            <td>${ moment(new Date(at_examen.FechaExamenes)).format('DD/MM/YYYY')}</td>
            <td>${ tipoExamen ? tipoExamen.Descripcion : 'No disponible'}</td>
            <td>${ at_examen.Conclusion }</td>
            <td>${ at_examen.NombreArchivo}</td>
            <td>
                <div class="dropdown">
                  <button class=" btn-link dropdown textVertical" style='outline:none !important;' id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <label class='textVertical'>...</label>
                  </button>
                  <div class="dropdown-menu" style="padding: 8px 0px;">  
                      <a class="dropdown-item d-flex align-items-center hide-detail-page" onclick="atSp4ConfirmarEliminarExamenesAuxiliares(${at_examen.Id})">
                        <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                        <span>Eliminar</span>
                      </a>
                      ${ at_examen.ArchivoBase64 !== '' ? `<a class="dropdown-item d-flex align-items-center" download="${at_examen.NombreArchivo}" href="${at_examen.ArchivoBase64}">
                        <img src="./images/sho/download.svg"> <span>Descargar documento</span>
                        </a>` : '' }
                      <div class="dropdown-item d-flex align-items-center">
                        <label for="am_file_upload_examen_${at_examen.Id}" style="display: contents; cursor: pointer">
                          <img src="./images/sho/upload2.svg" fill="#8fbb02"/>
                          <span>${(!at_examen.ArchivoBase64) ? 'Adjuntar documento' : 'Actualizar documento'}</span>
                        </label>          
                        <input type="file" id="am_file_upload_examen_${at_examen.Id}" accept="application/pdf, .pdf, .doc, docx, .odf" onchange="atSp4CargarAdjuntoExamen(this,${at_examen.Id})" style="display: none">
                      </div>
                  </div>
                </div>
            </td>
          </tr>
        `)
      })

    }else{

      if(modeAccidenteTrabajo == 'create'){
        
        $('#at_nuevo_examenes_auxiliares').show()
        $('#at_lista_examenes_auxiliares').hide()
      }
    }
}

function atSp4RenderExamenesAuxiliaresModal() {

    if(paObj_at.iExamen_Auxiliar.length){

      $('#dat_at_examenes-auxiliares-modulo').text(`${paObj_at.iExamen_Auxiliar.length == 1 ? '01' : paObj_at.iExamen_Auxiliar.length } registros`)
      $('#content_sp4_AT_table_examenes-auxiliares-modulo').html('')
      
      paObj_at.iExamen_Auxiliar.map(at_examen => {

        const tipoExamen = pluck_at.TipoExamen.find(item => item.Id == at_examen.TipoExamenId )

        $('#content_sp4_AT_table_examenes-auxiliares-modulo').append(`
          <tr>
            <td>${ moment(new Date(at_examen.FechaExamenes)).format('DD/MM/YYYY')}</td>
            <td>${ tipoExamen ? tipoExamen.Descripcion : 'No disponible'}</td>
            <td>${ moment(new Date(at_examen.FechaModificacion)).format('DD/MM/YYYY')}</td>
            <td>${ at_examen.Conclusion }</td>
            <td>${ at_examen.NombreArchivo}</td>
            <td>
                <div class="dropdown">
                  <button class=" btn-link dropdown textVertical" style='outline:none !important;' id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <label class='textVertical'>...</label>
                  </button>
                  <div class="dropdown-menu" style="padding: 8px 0px;">  
                      <a class="dropdown-item d-flex align-items-center hide-detail-page" onclick="atSp4ConfirmarEliminarExamenesAuxiliares(${at_examen.Id})">
                        <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                        <span>Eliminar</span>
                      </a>
                      ${ at_examen.ArchivoBase64 !== '' ? `<a class="dropdown-item d-flex align-items-center" download="${at_examen.NombreArchivo}" href="${at_examen.ArchivoBase64}">
                        <img src="./images/sho/download.svg"> <span>Descargar documento</span>
                        </a>` : '' }
                      <div class="dropdown-item d-flex align-items-center">
                        <label for="am_file_upload_examen_${at_examen.Id}" style="display: contents; cursor: pointer">
                          <img src="./images/sho/upload2.svg" fill="#8fbb02"/>
                          <span>${(!at_examen.ArchivoBase64) ? 'Adjuntar documento' : 'Actualizar documento'}</span>
                        </label>          
                        <input type="file" id="am_file_upload_examen_${at_examen.Id}" accept="application/pdf, .pdf, .doc, docx, .odf" onchange="atSp4CargarAdjuntoExamen(this,${at_examen.Id})" style="display: none">
                      </div>
                  </div>
                </div>
            </td>
          </tr>
        `)
      })

    }else{
      $('#dat_at_examenes-auxiliares-modulo').text(`0 registros`)
      $('#content_sp4_AT_table_examenes-auxiliares-modulo').html(`
        <tr>
          <td colspan="6" class="text-center">No se encontraron resultados.</td>
        </tr>
      `)
    }
}

function atSp4ConfirmarEliminarExamenesAuxiliares(Id){
  
  const data = paObj_at['iExamen_Auxiliar'].find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar examen.",
    html: `
       <p>Está por eliminar el examen registrado.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4EliminarExamenesAuxiliares(data)
    }
  });
}

function atSp4EliminarExamenesAuxiliares(data){

  data.Id1 = data.Id
  data.accion = 2
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'
  data.NombreArchivo = ""
  data.ArchivoBase64 = ""
  data.CreadoFecha = data.FechaExamenes
  data.FechaModificacion = moment(new Date()).format('YYYY-MM-DD')

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_examenes_auxiliares_post?code=e0k2JcleRZnuG4g1ZJ/syHlska0sKyd1VqavOmt6cVkPqTh/5IgJKw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
      atSp4CargarExamenesAuxiliares()
      Swal.fire({
        title: 'Examen auxiliar eliminado correctamente',
        iconColor: "#8fbb02",
        iconHtml: '<img src="./images/sho/check.svg" width="28px">',
        showConfirmButton: false,
        padding: "3em 3em 6em 3em ",
        timer: 1500,
      })
  })
}

function atSp4ConfirmCancelarExamenesAuxiliares() {

  let nombres = $("#dat_at_nombres_trabajador").val();
  let apellidos = $("#dat_at_apellidos_trabajador").val();
  
  Swal.fire({
    title: "Cancelar examenes auxiliares.",
    html: `
       <p>Está por cancelar el examen auxiliar</p>
       <p>${nombres} ${apellidos}</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      $('#modalAccidenteTrabajo').modal('hide')
    }
  });
}

function atSp4CargarAdjuntoExamen(element, id) {

  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function () {
    // $("#file_upload").attr("data-file64", reader.result);
    let data = {};
    data.NombreArchivo = element.files[0].name;
    data.ArchivoBase64 = reader.result;
    atSp4GuardarAdjuntoExamenAuxiliar(id, data);
  }
  reader.readAsDataURL(file);
}

function atSp4GuardarAdjuntoExamenAuxiliar(Id, adjunto){

  let data = paObj_at.iExamen_Auxiliar.find(item => item.Id == Id)

  if (!data) return;

  data.Id1 = data.Id
  data.accion = 1
  data.NombreArchivo = adjunto.NombreArchivo
  data.ArchivoBase64 = adjunto.ArchivoBase64
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'
  data.FechaModificacion = moment(new Date(data.FechaExamenes)).format('YYYY-MM-DD')


  let url = `${apiUrlsho}/api/hce_accidente_trabajo_examenes_auxiliares_post?code=e0k2JcleRZnuG4g1ZJ/syHlska0sKyd1VqavOmt6cVkPqTh/5IgJKw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    atSp4CargarExamenesAuxiliares()

    Swal.fire({
      title: 'Archivo adjunto anexado correctamente.',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
  })

}

//--------------------------FUNCIONES INFORMES MEDICOS ------------------------------//

async function _init_atSp4NuevoInformeAccidenteTrabajo() {

  showLoading();

  await atSp4CrearInformeVacio()
  atSpRenderSelectsSistemaAfectado('input_at_sistema_afectado_informe','input_at_seccion_afectada_informe')

  if(pluck_at.Especialidad.length){
    
    $(`#input_at_especialidad_informe`).html('');
    $(`#input_at_especialidad_informe`).append('<option value="" selected></option>')
  
    pluck_at['Especialidad'].map((Item) => {
      $(`#input_at_especialidad_informe`).append(`<option value="${Item.Id}">${Item.Descripcion}</option>`)
    });
  }

  hideLoading();
}

async function atSp4CrearInformeVacio() {

  let data = {}

  data.Id1 = 0
  data.accion = 0
  data.Accidente_TrabajoId = IdAT
  data.EspecialidadId = 0
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'
  data.FechaInforme =  moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS')

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_informe_medico_post?code=JqCgGzKltScFtO3PkfN0pg1VQ1A18GKShkpsEsnTaOCLyIlQjAvuog==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    paObj_at['IdInforme'] = response.Id
  })
}

function atSp4ConfirmGuardarInforme() {

  if (validationForm('check-informe').length > 0) {
    return;
  }
  
  Swal.fire({
    title: "Guardar informe médico.",
    html: `
       <p>Está por guardar el informe medico.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      atSp4GuardarInformeMedico()
    }
  });
}

function atSp4GuardarInformeMedico() {

  let data = {}

  data.Id1 = paObj_at.IdInforme
  data.accion = 1
  data.Accidente_TrabajoId = IdAT
  data.EspecialidadId = parseInt($('#input_at_especialidad_informe').val())
  data.FechaInforme =  $('#input_at_fecha_informe').val()
  data.CreadoPor = 'SHO'
  data.ModificadoPor = 'SHO'

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_informe_medico_post?code=JqCgGzKltScFtO3PkfN0pg1VQ1A18GKShkpsEsnTaOCLyIlQjAvuog==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    $('#modalAccidenteTrabajo').modal('hide')
    
    Swal.fire({
      title: 'Informe médico registrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarInformesMedicos()
  })
}

function atSp4ConfirmCancelarInforme() {

  let nombres = $("#dat_at_nombres_trabajador").val();
  let apellidos = $("#dat_at_apellidos_trabajador").val();
  
  Swal.fire({
    title: "Cancelar informe médico.",
    html: `
       <p>Está por cancelar el informe medico de</p>
       <p>${nombres} ${apellidos}</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {
      $('#modalAccidenteTrabajo').modal('hide')
    }
  });
}

function atSp4GuardarDiagnosticoCie10Informe() {
  
  if (validationForm('check-diagnosticocie10-informe').length > 0) {
    return;
  }

  let data = {};
  data.Id1 = 0
  data.InformeMedicoId = paObj_at.IdInforme
  data.Diagnostico = $('#sp4_at_diagCIE10_diag_informe').val()
  data.CIE10 = $('#dat_at_cie10_informe').val()
  data.SistemaAfectado = $('#input_at_sistema_afectado_informe').val() ? $('#input_at_sistema_afectado_informe').val() : 15
  data.SeccionAfectada = $('#input_at_seccion_afectada_informe').val() ? $('#input_at_seccion_afectada_informe').val() : 11
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 0

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_informe_medico_cei10_post?code=88CHCes9gGrtxaAIsykNCuqvNSVRMIPHixaldNVu8P1dIgZYHb4jJw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Diágnostico CIE10 registrado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarDiagnosticoCie10Informe()
    
    //Limpiar formulario 
    $('#sp4_at_diagCIE10_diag_informe').val('')
    $('#sp4_at_diagCIE10_cie10_informe').val('')
    $('#dat_at_cie10_informe').val('')
    $('#input_at_sistema_afectado_informe').val('')
    $('#input_at_seccion_afectada_informe').val('')
  })
}

function atSp4CargarDiagnosticoCie10Informe() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Informe_medico_con_CIE10`;

  let data = {
    idHC : paObj_at.IdInforme // id del informe
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['CE10_informe_medico'] = response.CE10_informe_medico
    atSp4RenderDiagnosticoCie10Informe()
  })
}

function atSp4RenderDiagnosticoCie10Informe(){

    if(paObj_at.CE10_informe_medico.length) {

      $('#at_nuevo_seguimiento').hide()
      $('#at_lista_seguimiento').show()

      $('#dat_at_cant_diagCIE10_informe').text(`${paObj_at.CE10_informe_medico.length == 1 ? '01' : paObj_at.CE10_informe_medico.length } registros`)
      $('#content_sp4_AT_table_diagCIE10_informe').html('')
      
      paObj_at.CE10_informe_medico.map(at_cie10_informe => {
        $('#content_sp4_AT_table_diagCIE10_informe').append(`
          <tr>
            <td>${at_cie10_informe.Diagnostico}</td>
            <td>${at_cie10_informe.Code_D}</td>
            <td>${ moment(new Date(at_cie10_informe.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>${at_cie10_informe.Especilidades}</td>
            <td>${at_cie10_informe.SistemaAfectado_D}</td>
            <td>${at_cie10_informe.SeccionAfectada_D}</td>
            <td>
              <button onclick="atSp4ConfirmarEliminarDiagnosticoCie10Informe(${at_cie10_informe.Id})" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else{
      $('#dat_at_cant_diagCIE10_informe').text(`0 registros`)
      $('#content_sp4_AT_table_diagCIE10_informe').html(`
        <tr>
          <td colspan="7" class="text-center">No se encontraron resultados.</td>
        </tr>
      `)
    }
}

function atSp4ConfirmarEliminarDiagnosticoCie10Informe(Id){
  
  const data = paObj_at['CE10_informe_medico'].find( item => item.Id = Id)

    if(!data) return

    Swal.fire({
    title: "Eliminar diagnóstico.",
    html: `
       <p>Está por eliminar el diagnóstico CIE 10 registrado.</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then(async (result) => {
    if (result.isConfirmed) {

      atSp4EliminarDiagnosticoCie10Informe(data)
    }
  });
}

function atSp4EliminarDiagnosticoCie10Informe(data) {
  
  data.Id1 = data.Id
  data.InformeMedicoId = paObj_at.IdInforme
  data.CreadoPor = "SHO"
  data.ModificadoPor = "SHO"
  data.accion = 2

  let url = `${apiUrlsho}/api/hce_accidente_trabajo_informe_medico_cei10_post?code=88CHCes9gGrtxaAIsykNCuqvNSVRMIPHixaldNVu8P1dIgZYHb4jJw==&httpmethod=post`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {
    
    Swal.fire({
      title: 'Diágnostico CIE10 eliminado correctamente.',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })
    atSp4CargarDiagnosticoCie10Informe()
  })
}



function atSp4GuardarAdjuntoInforme(data) {

  let url = apiUrlsho+`/api/hce_accidente_trabajo_informe_medico_adjunto_post?code=MOaRCxawifp0wBaVfKk400PmUTvsNaefzSpuSGwZ1saSSBGjy0cKMw==&httpmethod=post`;

  data.InformeMedicoId = paObj_at.IdInforme

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "post",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    "data": JSON.stringify(data)
  };

  return $.ajax(settings).done((response) => {

    Swal.fire({
      title: 'Archivo adjunto anexado con éxito',
      iconColor: "#8fbb02",
      iconHtml: '<img src="./images/sho/check.svg" width="28px">',
      showConfirmButton: false,
      padding: "3em 3em 6em 3em ",
      timer: 1500,
    })

    atSp4CargarAdjuntoInforme()
  });
}

function atSp4CargarAdjuntoInforme() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Informe_medico_Adjuntos`;

  let data = {
    idHC : paObj_at.IdInforme // id del informe
  }
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {
    paObj_at['adjuntosInformeMedico'] = response.adjuntosInformeMedico
    atSp4RenderAdjuntoInforme()
  })

}

function atSp4RenderAdjuntoInforme() {

    if(paObj_at.adjuntosInformeMedico.length) {

      $('#secion_at_adjuntar_informe').hide()
      $('#secion_at_adjunto_list_informe').show()

      $('#dat_at_cant_adjunto_informe').text(`${paObj_at.adjuntosInformeMedico.length == 1 ? '01' : paObj_at.adjuntosInformeMedico.length } archivos`)
      $('#content_sp4_AT_table_adjunto_informe').html('')
      
      paObj_at.adjuntosInformeMedico.map(at_adj_informe => {
        $('#content_sp4_AT_table_adjunto_informe').append(`
          <tr>
            <td>${at_adj_informe.NombreArchivo}</td>
            <td>${moment(new Date(at_adj_informe.FechaSubida)).format('DD/MM/YYYY')}</td>
            <td>
              <a download="${at_adj_informe.NombreArchivo}" href="${at_adj_informe.ArchivoBase64}" class="btn btn-link shadow-none float-right">
                <img src="./images/sho/eyeIcon.svg">
              </a>
            </td>
          </tr>
        `)
      })
    }else{

      $('#secion_at_adjuntar_informe').show()
      $('#secion_at_adjunto_list_informe').hide()
      
      $('dat_at_cant_adjunto_informe').text(`0 registros`)
      $('#content_sp4_AT_table_adjunto_informe').html(`
        <tr>
          <td colspan="7" class="text-center">No se encontraron resultados.</td>
        </tr>
      `)
    }
}

function atSp4CargarInformesMedicos() {

  let url = `${apiUrlsho}/api/hce_enfermedades_cronicas?code=noDt5LQMV9CC2oM2x9LajYZaZw6Bvss3KqqWzg7wr4JQSgfh0RAPTg==&AccionBackend=Lista_Informes_Accidente_Trabajo`;

  let data = {
    idHC : IdAT // id del accidente
  }

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    data
  };

  return $.ajax(settings).done((response) => {

    paObj_at['informes_Accidente_Trabajo'] = response.informes_Accidente_Trabajo
    atSp4RenderInformesMedicos()
  });  
}

function atSp4RenderInformesMedicos() {

    if(paObj_at.informes_Accidente_Trabajo.length){

      $('#at_nuevo_informes').hide()
      $('#at_lista_informes').show()

      $('#dat_am_cant_informes').text(`${paObj_at.informes_Accidente_Trabajo.length == 1 ? '01' : paObj_at.informes_Accidente_Trabajo.length } registros`)
      $('#content_sp4_AT_table_informes').html('')
      
      paObj_at.informes_Accidente_Trabajo.map(at_informe => {

        $('#content_sp4_AT_table_informes').append(`
          <tr>
            <td>${ moment(new Date(at_informe.FechaInforme)).format('DD/MM/YYYY')}</td>
            <td>${ at_informe.Especilidades }</td>
            <td>${ at_informe.Diagnostico }</td>
            <td>${ at_informe.documento_escaneado }</td>
            <td>
            </td>
          </tr>
        `)
      })

    }else{

      if(modeAccidenteTrabajo == 'create'){
        
        $('#at_nuevo_informes').show()
        $('#at_lista_informes').hide()
      }
    }
}
//--------------------------FUNCIONES EVALUACIÓN ------------------------------//

async function _init_atSp4NuevaEvaluacion() {
  
  showLoading();

  navEvaluacionAT = true
  paObj_at['IdEvaluacion'] = 0
  
  hideLoading();
}

function atSp4RenderEvaluaciones() {

    if(paObj_at.AccidentesT.postAltaMedicaAT.length) {

      $('#at_nueva_evaluaciones').hide()
      $('#at_lista_evaluaciones').show()

      $('#dat_at_cant_evaluaciones').text(`${paObj_at.AccidentesT.postAltaMedicaAT.length == 1 ? '01' : paObj_at.AccidentesT.postAltaMedicaAT.length } registros`)
      $('#content_sp4_AT_table_evaluaciones').html('')
      
      paObj_at.AccidentesT.postAltaMedicaAT.map(at_eval => {
        $('#content_sp4_AT_table_evaluaciones').append(`
          <tr>
            <td>${ moment(new Date(at_eval.CreadoFecha)).format('DD/MM/YYYY')}</td>
            <td>${at_eval.C_Anamnesis}</td>
            <td>${at_eval.F_Tratamiento}</td>
            <td>${at_eval.G_Aptitud}</td>
            <td>
              <button onclick="" id="" type="button" class="btn btn-link shadow-none float-right hide-detail-page">
                <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
              </button>
            </td>
          </tr>
        `)
      })
    }else {

      if(modeAccidenteTrabajo == 'create'){
        
        $('#at_nueva_evaluaciones').show()
        $('#at_lista_evaluaciones').hide()
      }
    }
}

//--------------------------FUNCION VALIDAR FORMULARIO------------------------------//

function validationForm(class_selector) {
  let error = [];
  let inputs = $('input.'+class_selector);
  let selects = $('select.'+class_selector);

  inputs.each((e) => {
    if (!inputs.eq(e).val()) {
      inputs.eq(e).addClass('is-invalid');
      inputs.eq(e).parent().find('.invalid-feedback').remove();
      inputs.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
      error.push(inputs.eq(e).val())
    }
    if (inputs.eq(e).val()) {
      inputs.eq(e).removeClass('is-invalid');
    }
  })

  selects.each((e) => {
    if (!selects.eq(e).val()) {
      selects.eq(e).addClass('is-invalid');
      selects.eq(e).parent().find('.invalid-feedback').remove();
      selects.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
      error.push(selects.eq(e).val())
  }
    if (selects.eq(e).val()) {
      selects.eq(e).removeClass('is-invalid');
    }
  })

  return error;
}













/////////////////////ESTO ES NUEVO


function intSp3ConfirmGuardarDescanzoMedico() 
{
  
  // Desde el modulo Accidentes de trabajo
  if(navDescansoAT) {

    if (validationForm('check-descanso').length > 0) {
      return;
    }
  }

  let nombres = $("#dat_des_nombres_trabajador_v").val();
  let apellidos = $("#dat_des_apellidos_trabajador_v").val();
  Swal.fire({
    title: "Guardar descanso medico.",
    html: `
    <p>Está por guardar el descanso medico de</p>
    <p>${nombres} ${apellidos}</p>
    <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img class="inject-svg" src="./images/sho/confirm.svg" alt="" fill="#fff" width="18px">`,
    cancelButtonText: `Cancelar <img class="inject-svg" src="./images/sho/cancelar.svg" alt="" fill="#fff" width="18px">`,
  }).then((result) => {
    if (result.isConfirmed) {
      
      // Desde el modulo Accidentes de trabajo
      if(navDescansoAT){
        return atSp4GuardarDescansoMedico()
      }

      fnSp3ModificarNuevaDescansoMedico();
    }
  });
  SVGInject($(".inject-svg"));
}










function fnSp3ModificarNuevaDescansoMedico()
{//-------------------------------------------------------------------------------------------------------------------------
        
        // if(isNow == 1)
        // {
        //   istAudT = istAud;
        // }

         if(newD == 1)
         {
             
         }

     //alert("la trasnferencia es la = "+istAud+" y los signos vitales Id = "+IdSV);
         showLoading();

         var f1g =  $('#dat_des_fi_v2').val();
         var pru = f1g.split('/');
         var f1g = pru[2]+'-'+pru[1]+'-'+pru[0];
         let jpru = f1g.split('undefined-undefined-');f1g = jpru[1];

         var f2g =  $('#dat_des_ff_v').val();
         var prux = f2g.split('/');
         var f2g = prux[2]+'-'+prux[1]+'-'+prux[0];
         let jpru2 = f2g.split('undefined-undefined-');f2g = jpru2[1];
    
        var body = {
                    "IdHC": idHC, //Id Historia clinica
                    "IdDescM": id_DM,
                    "A_DniTrabajador":  $('#dat_des_dni_trabajador_v').val(),
                    "A_NombreTrabjador": $('#dat_des_nombres_trabajador_v').val(),
                    "A_ApellidosTrabajador":  $('#dat_des_apellidos_trabajador_v').val(),
                    "A_Empresa": $('#dat_des_empresa_v').val(),
                    "A_Origen": $('#dat_des_origen_v').val(),
                    "A_IdAtencionMedica":  0,//preguntar como se vincula???
                    
                    "A_Gerencia": $('#dat_des_gerencia_v').val(),
                    "A_Planta":  $('#dat_des_planta_v').val(),
                    "A_Area":  $('#dat_des_area_v').val(),
                    "A_PuestoTrabajo":  $('#dat_des_id_atencion_med_v').val(),
                    "B_PersonalSolicitud": $('#dat_des_personal_salud_v').val(),
                    "B_PersonalIdHash": "",
                    "B_TipoContingencia": $('#dat_des_tipo_contingencia_v').val(),
                    "B_DescansoPorEnfermedad":  $('#dat_des_descanso_enfermedad_v').val(),
                  
                    "B_CMP": $('#dat_des_cmp_v').val(),
                    "B_CantidadDias": $('#dat_des_cant_dias_v').val(),
                    "B_DiasAcumulados": $('#dat_des_dias_ac_v').val(),
                    "B_FechaIni":f1g,

                    "B_FechaFin":f2g,
                    "B_HuboAltaMedica": $('#dat_des_alta_med_v').val(),
                    "B_EstableceDescanso": $('#dat_des_establecimiento_emisión_v').val(),
                    "B_Particular": $('#dat_des_particular_v').val(),
                    "OrigenId": 0,
                    "TipoOrigen": $('#dat_des_origen_v').val()
                    }

            var url = apiUrlsho+"/api/hce_Post_023_descanso_medico?code=ZMMXyYQzh2QaROc92eMe55BMlkkckJVD9r9mxqTmHFpCUXm75EvSsQ==&httpmethod=post";

            console.log('urlr:', url)
            console.log('body new descanso:', body)

            console.log('urlr:', url)

                   var headers ={
                   "apikey":constantes.apiKey,
                   "Content-Type": "application/json",
                   }

                   $.ajax({
                       method: 'POST',
                       url:  url,
                       headers:headers,
                       data: JSON.stringify(body),
                       crossDomain: true,
                       dataType: "json",
                   }).done(function (data)
                   {

                       console.log('despues crear',data);
                       if(data.Id > 0)
                       {
                           //alert('el reguistro de la transferencia tiene el Id = '+data.Id );
                           hideLoading();
                            Swal.fire({
                                        title: "Se terminó con éxito el registro",
                                        iconColor: "#8fbb02",
                                        iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                                        showConfirmButton: false,
                                        padding: "3em 3em 6em 3em ",
                                        timer: 1500,
                                      }).then(() => {
                                        //handlerUrlhtml('contentGlobal', 'view/sho-hce/historia_clinica/gestionHistoriaClinica.html', 'Historia clínica electrónica');
                                        handlerUrlhtml('contentGlobal', 'view/sho-hce/descansos_medicos/gestionDescansoMedicoBandeja.html', 'Registro de Descansos Médicos');
                                      })

                        }
                        else 
                        {
                            Swal.fire({
                                      icon: 'error',
                                      title: 'Descanso Medico',
                                      text: 'Error al crear el registro de la Descanso Medico',
                                      footer: '<a href="">Why do I have this issue?</a>'
                                    })
                        }

                   })
                   .fail(function( jqXHR, textStatus, errorThrown ) {

                     
                          Swal.fire({
                                      icon: 'error',
                                      title: 'Descanso Medico',
                                      text: 'Error al crear el registro de la Descanso Medico',
                                      footer: '<a href="">Why do I have this issue?</a>'
                                    })
                   })
                   .always(function( jqXHR, textStatus, errorThrown ) {

                   });




}//-------------------------------------------------------------------------------------------------------------------------



function hcSp3ConfirmCancelarDescansoMedico() 
{
  Swal.fire({
    title: "Cancelar los Datos del Descanso Medico",
    html: `
       <p>Está por cancelar los datos del descanso medico</p>
       <p class="mt-5">¿Desea confirmar la acción?</p>`,
    icon: "info",
    showCancelButton: true,
    reverseButtons: true,
    cancelButtonColor: "#ff3636",
    confirmButtonColor: "#8fbb02",
    confirmButtonText: `Confirmar <img src="./images/sho/confirm.svg">`,
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  }).then((result) => {
    if (result.isConfirmed) {
      fnCancelarNuevoDscansoMedico();
    }
  });
}//-----------------------------------------------------------------------------------------








function agregarAdjunto_1_dm(element, id) {
  //$(`#int_spin_guardar_adjunto_${id}`).show();
   //lb_bt_adjuntarEditar
  $('#lb_bt_adjuntarEditar').html( ` <div class="spinner-border spinner-border-sm" id="int_spin_guardar_adjunto_0" role="status" style="display: block;">
              <span class="sr-only">Adjuntando..</span>
</div `);

  let file = element.files[0];
  let reader = new FileReader();
  var nameFile = '';
  reader.onloadend = function() {

    nameFile = element.files[0].name;
    let data = {};
    data.IdDescansoMedico = id_DM;
    data.IdDescMedAdj = 0;
    data.NombreArchivo = element.files[0].name;
    data.IdHashUser = getCookie("vtas_id_hash"+sessionStorage.tabVisitasa);
    data.ArchivoBase64 = reader.result;

    let url = apiUrlsho+`/api/hce_Post_026_descanso_medico_adjunto?code=Yw42GGECBezciqikQakRh/yqFSMnMO7lehs6hIf80A2XzlspDWtkew==&httpmethod=post`;

    let headers = {
      "apikey": constantes.apiKey,
      "Content-Type": "application/json"
    }

    let settings = {
      "url": url,
      "method": "post",
      "dataType": 'json',
      "headers": headers,
      "data": JSON.stringify(data)
    };

    $.ajax(settings).done(async (response) => {
      console.log("Se guardaria el adjunto............", response);
    
      $(`#int_spin_guardar_adjunto_${id}`).hide();
       

       nameFile = "Adjuntado: "+response.NombreArchivo;
      $(`#lb_bt_adjuntarEditar`).html(nameFile+` 
                        <img class="inject-svg" src="./images/sho/upload.svg" fill="#fff" style="width:14px !important" />`);

      

      //aqui volvemos a car gar desde ajax los adjuntos
    })
  }
  reader.readAsDataURL(file);
}



function agregarAdjunto_2_dm(element, id) {
  //$(`#int_spin_guardar_adjunto_${id}`).show();
   //lb_bt_adjuntarEditar
  $('#lb_bt_adjuntarEditar2').html( ` <div class="spinner-border spinner-border-sm" id="int_spin_guardar_adjunto_0" role="status" style="display: block;">
              <span class="sr-only">Adjuntando..</span>
</div `);

  let file = element.files[0];
  let reader = new FileReader();
  var nameFile = '';
  reader.onloadend = function() {

    nameFile = element.files[0].name;
    let data = {};
    data.IdDescansoMedico = id_DM;
    data.IdDescMedAdj = 0;
    data.NombreArchivo = element.files[0].name;
    data.IdHashUser = getCookie("vtas_id_hash"+sessionStorage.tabVisitasa);
    data.ArchivoBase64 = reader.result;

    let url = apiUrlsho+`/api/hce_Post_028_descanso_medico_adjunto_detalles?code=YC1grBN6HgHSibcvm3IVlATt8DQ7OKgzfW0iUT07yUaIKVAG97EEMA==&httpmethod=post`;

    let headers = {
      "apikey": constantes.apiKey,
      "Content-Type": "application/json"
    }

    let settings = {
      "url": url,
      "method": "post",
      "dataType": 'json',
      "headers": headers,
      "data": JSON.stringify(data)
    };

    $.ajax(settings).done(async (response) => {
      console.log("Se guardaria el adjunto............", response);
    
      $(`#int_spin_guardar_adjunto_${id}`).hide();
       

       nameFile = "Adjuntado: "+response.NombreArchivo;
      $(`#lb_bt_adjuntarEditar2`).html(nameFile+` 
                        <img class="inject-svg" src="./images/sho/upload.svg" fill="#fff" style="width:14px !important" />`);

      

      //aqui volvemos a car gar desde ajax los adjuntos
    })
  }
  reader.readAsDataURL(file);
}







function agregarDignosticoCIE10_DM()
  {//--------------------------------------------------------------------------------------------------------------

    if(navDescansoAT) {

      if (validationForm('check-diagnostico-dm').length > 0) {
        return;
      }
    }

      $('#lb_bt1').html(' Agregando ......');
      $('#btn_int_diagnostico_agregar').attr("disabled",true);

      let a = $('#dat_des_diagnostico_v').val();
      let b = $('#dat_am_cie_diagnostico1').val(); // let b = $('#dat_des_cie10_v').val();
      
      let c = $('#dat_des_sis_afectado_v').val() ? $('#dat_des_sis_afectado_v').val() : 15
      let d = $('#dat_des_sec_afectada_v').val() ? $('#dat_des_sis_afectado_v').val() : 11


     let body =  {
        "IdDescansoMedico":id_DM,
        "TransferenciaId2":0,

        "Diagnostico":a,
        "CIE10":b,
        "IdHashUser":getCookie("vtas_id_hash"+sessionStorage.tabVisitasa),
        "SistemaAfectado":c,
        "SeccionAfectada":d
    }

    var url = apiUrlsho+"/api/hce_Post_024_descanso_medico_diagnosticoCIE10?code=aN5mlWKDseFSRycTtVpr2bh6tWq4qzDff0qeQvcPmPImyOvd9PcxGA==&httpmethod=post";

        console.log('urlr:', url)

               var headers ={
               "apikey":constantes.apiKey,
               "Content-Type": "application/json",
               }

               $.ajax({
                   method: 'POST',
                   url:  url,
                   headers:headers,
                   data: JSON.stringify(body),
                   crossDomain: true,
                   dataType: "json",
               }).done(function (data)
               {

                   console.log('despues crear lista cie10',data);
                   if(data.Id > 0)
                   {
                       //alert('el reguistro de la descanso medico tiene el Id = '+data.Id );
                      
                       sp3CargaLosCie10_1_DM(id_DM);

                        hideLoading();

                   }

               })
               .fail(function( jqXHR, textStatus, errorThrown ) {

                   ///verModalError('Al Cargar la', 'Intente Nuevamente');
                    Swal.fire({
                                      icon: 'error',
                                      title: 'Error',
                                      text: 'Error al al cargar, intente nuevamente',
                                      footer: '<a href="">Why do I have this issue?</a>'
                                    })
                 
               })
               .always(function( jqXHR, textStatus, errorThrown ) {

               });

 }//--------------------------------------------------------------------------------------------------------------





var diag_DN = ' ';



  function sp3CargaLosCie10_1_DM(IdTransf)
    {

  if(newD == 1)
  {
    diag_DN = ''; //alert('entramosss aqui');
  }
   
      var  ikk = 0;


      var headers = { "apiKey": constantes.apiKey, "Content-Type": "application/json" }
      showLoading();

      var url = apiUrlsho+"/api/hce_Get_014_descanso_medico_diagnosticoCIE10?code=/J44W6Q4iAAgb6cxLiqZKMLhKa6QAOFNoNsOlVU0KHWXrK3n053ELQ==&IdDescansoMedico="+IdTransf;
      var tabla;
      var settings = {
        "url": url,
        "method": "GET",
        "timeout": 0,
        "crossDomain": true,
        "dataType": "json",
        "headers": headers,
        // "data": { "NroDocumento_Trabajador_H": a, "AreaId_Empresa_H": c, "SedeId_Empresa_H": b, "Buscador": d } //JSON.stringify(body)
      };



        $.ajax(settings).done(function(response)
        {

          console.log("** Buscando........................925 cie10...*", response);

          $('#table_diagn_cie110_1').html(" ");
          $('#lb_diag_1_int_t').html(""+ikk+" registros");
          response.DiagnosticoCIE.map(function(Item)
          {

            if(Item.DescansoMedicoId > 0)
            {//----------------------------------------------------------------------------------

              //alert('si es desde aqui men');
              var f1a = date_AAAA_MM_DD_T_HH_MM_S_to_DD_MM_AAAA(Item.CreadoFecha);

              $('#dat_des_cie10_v').val(Item.CIE10);                          let b = Item.Code; //$('#dat_des_cie10_v option:selected').text();
              $('#dat_des_sis_afectado_v').val(Item.SistemaAfectado);         let c = Item.SistemaAfectado_D; //$('#dat_des_sis_afectado_v option:selected').text();
              $('#dat_des_sec_afectada_v').val(Item.SeccionAfectada);         let d = Item.SeccionAfectada_D; //$('#dat_des_sec_afectada_v option:selected').text();
                                                                               //let e =  $('#dat_des_cie10_v').attr('title');
                                                                              let e = Item.Especilidades; //$('#dat_des_cie10_v option:selected').attr('title');


                         // var abc = ' '+Item.Diagnostico+' '+b+', ';
                         // var re = /`${abc}`/g;
                         // var diag_DN = diag_DN.replace(re, '');

                          diag_DN = diag_DN +' '+Item.Diagnostico //+' '+b+', ';

                          tabla += `
                            <tr>
                              <td>${Item.Diagnostico != null ? Item.Diagnostico : "---"}</td>
                              <td>${b != null ? b : "---"}</td>
                              <td>${f1a != null ? f1a : "---"}</td>
                              <td>${e != null ? e : "---"}</td>
                              <td>${c!= null ? c: "---"}</td>
                              <td>${d != null ? d : "---"}</td>

                              <td>
                                <button type="button" class="btn btn-link shadow-none float-right" onclick = "sp3FnBorrarDiagnosticoTrnasferencia_DM(${Item.Id})" >
                                  <img class="inject-svg" src="./images/sho/delete.svg" alt=""  fill="#ff3636" width="16px">
                                </button>
                              </td>

                            </tr>

                       `;

                       $('#table_diagn_cie110_1').append(tabla);
                   ikk = ikk+1;
                   $('#lb_diag_1_int_t').html(""+ikk+" registros");
                   tabla = '';

                   $('#dat_des_diagnostico_v').val('');
                   $('#dat_des_cie10_v').val('');
                   $('#dat_des_sis_afectado_v').val(15);
                   $('#dat_des_sec_afectada_v').val(11);
            }//---------------------------------------------------------------------------------

          });

          hideLoading();
          $('#lb_bt1').html('Agregar a lista');
          $('#btn_int_diagnostico_agregar').attr("disabled",false);

        });






        // SVGInject($(".inject-svg"));


    }






function sp3FnBorrarDiagnosticoTrnasferencia_DM(IdDiag)
    {
        //alert('vamos a eliminar el diagnostico:::' +IdDiag );
       showLoading();
       $('#lb_bt1').html(' Eliminando ......');
      var body = {
                 "IdDescansoMedico":id_DM,
                 "IdDiagnosticoCIE10":IdDiag
                 }


        //var url = apiUrlssoma+ "/api/Post-LeccionAprendida?code=3SZLZvJm1g4qEdObbrPAWiDaiGa6EFKE9RFyIN1Q5osMz6ozxWX/xQ==&httpmethod=post";
        var url = apiUrlsho+"/api/hce_Post_025_descanso_medico_diagnosticoCIE10_eliminadoLogico?code=5zvYewTmqPmUD4OaV8u9ToJU83SCj9PnyabaoQv1aqHERQoI6AYKUA==&httpmethod=post";

        console.log('urlr:', url)

               var headers ={
               "apikey":constantes.apiKey,
               "Content-Type": "application/json",
               }

               $.ajax({
                   method: 'POST',
                   url:  url,
                   headers:headers,
                   data: JSON.stringify(body),
                   crossDomain: true,
                   dataType: "json",
               }).done(function (data)
               {

                   console.log('despues crear',data);
                   if(data.Id > 0)
                   {
                       //alert('el reguistro de la transferencia tiene el Id = '+data.Id );
                       hideLoading();
                      $('#lb_bt1').html('Agregar a lista');

                      
                       sp3CargaLosCie10_1_DM(id_DM);


                       //istAud = data.Id;

                   }

               })
               .fail(function( jqXHR, textStatus, errorThrown ) {

                   //verModalError('Al Eliminar el registro de Transferencia', 'Intente Nuevamente');
                 Swal.fire({
                                      icon: 'error',
                                      title: 'Error',
                                      text: 'Al Eliminar el registro de Transferencia, intente nuevamente',
                                      footer: '<a href="">Why do I have this issue?</a>'
                                    })
               })
               .always(function( jqXHR, textStatus, errorThrown ) {

               });


    }



