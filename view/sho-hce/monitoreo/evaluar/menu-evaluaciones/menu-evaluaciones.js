//#region  Variables
SVGInject($(".inject-svg"));

btnToggle = $("#sidebar");
sidebar = $("#sidebar");
tituloMenu = $("#sidebar .titulo").text();
tituloMenuCorto = tituloMenu.split(" ").map((e) => e[0]).join("");

//#endregion


//#region Inicializar
$(document).ready(function () {
  sp3HoMenuGenaralIzquierdoEventos(ControlNavHigiene);
});

setMenu();
//#endregion


//#region Acciones y eventos de los menu lateral 
function sp3HoMenuGenaralIzquierdoEventos(idAccion) {

  //201: Gestion de Agentes | 200: Gestion de GES | 202:Programacion de Monitoreo
  //Cambiar de menu (inactivar anterior, activar actual)
  switch (idAccion) {

    //Inspección de Instalaciones
    case 200:
      $('#' + menuAnt).removeClass('active');
      $('#menu_lateral200').addClass('active');
      menuAnt = 'menu_lateral200';

      $('#page-content-sidebar').load('view/sho-hce/higiene_evaluar/indexInspeccion.html');
      $('#page-content-sidebar').css('display', 'block');

      break;

    //Incidencias
    case 201:
      $('#' + menuAnt).removeClass('active');
      $('#menu_lateral201').addClass('active');
      menuAnt = 'menu_lateral201';

      $('#page-content-sidebar').load('view/sho-hce/higiene_evaluar/indexIncidencia.html');
      $('#page-content-sidebar').css('display', 'block');

      break;

    //Monitoreo Ocupacional
    case 202:
      $('#' + menuAnt).removeClass('active');
      $('#menu_lateral202').addClass('active');
      menuAnt = 'menu_lateral202';

      $('#page-content-sidebar').load('view/sho-hce/monitoreo/evaluar/lista-monitoreo/lista-monitoreo.html');
      $('#page-content-sidebar').css('display', 'block');
      break;
      
  }

}
//#endregion


//#region Accion de Click Boton Menu
btnToggle.on("mouseover mouseout", function () {
  sidebar.toggleClass("active");
  $("#toggle-btn svg").toggleClass("active");
  $("#page-content-sidebar").toggleClass("active");
  setMenu(); // Seteando el contenido del menu
});
//#endregion


//#region Setear Menu
function setMenu() {
  if (!sidebar.attr("class")) {
    $("#sidebar .titulo").text(tituloMenuCorto);
    $(".item-list span").css("display", "none");
  } else {
    $("#sidebar .titulo").text(tituloMenu);
    $(".item-list span").css("display", "flex");
  }
}
//#endregion


$('#ocultar_datos_dg').click(() => {
  $('#datos_hc_general_trabajador').children().hide();
  $('.img-perfil').css('height', '75px');
  $('.img-perfil').css('width', '75px');
  $('#content_img_datos_generales').removeClass('col-md-2');
  $('#botones_datos_generales').removeClass('col-md-3');
  $('#content_img_datos_generales').addClass('col-md-1');
  $('#botones_datos_generales').addClass('col-md-4');
  $('.btn_interacciones_datos_generales').removeClass('col-md-12');
  $('.btn_interacciones_datos_generales').addClass('col-md-6');
  $('#botones_datos_generales .row').addClass('flex-row-reverse')
  $('#ocultar_datos_dg').hide();
  $('#mostrar_datos_dg').show();
})
$('#mostrar_datos_dg').click(() => {
  $('#datos_hc_general_trabajador').children().show();
  $('.img-perfil').css('height', '148px');
  $('.img-perfil').css('width', '148px');
  $('#content_img_datos_generales').removeClass('col-md-1');
  $('#botones_datos_generales').removeClass('col-md-4');
  $('#content_img_datos_generales').addClass('col-md-2');
  $('#botones_datos_generales').addClass('col-md-3');
  $('.btn_interacciones_datos_generales').removeClass('col-md-6');
  $('.btn_interacciones_datos_generales').addClass('col-md-12');
  $('#botones_datos_generales .row').removeClass('flex-row-reverse')
  $('#ocultar_datos_dg').show();
  $('#mostrar_datos_dg').hide();
})

