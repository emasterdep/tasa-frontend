//#region Variables
var ListaEvaluacion;
var ListaTabla;
var ListaDetalleILU = [];

var listaLugares = []
var listaNivelPromedio = []
var listaNivelRecomendado = []
var listaCumplimientoNorm = []
var listaCumplimientoPorc = []
var listaEstado = []
var listaMotivo = []

//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    fnOcultarMostrarMotivo();
    fnOcultarMostrarBotones();

    onInit();

});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnCargarSedesAreasGerenciasMonitoreo(),
        fnCargarListaPuestos(),
        fnCargarPlucksMonitoreo(),
        fnGetSelectsForm(IdTipoEvaluacion)
    ]);

    fnSetSelectsMonitoreo();
    fnSetSelectsForm();

    fnCargarDatosGeneral();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Cargar Datos en General
async function fnCargarDatosGeneral() {
    //1: Abierto, 2:Cerrado, 3: Anulado, 4:Programado, 5:En Progreso
    if (EstadoEvaluacion == 1) {
        fnFormDisabled();
        fnCargarDatosMonitoreo();
    }
    else if (EstadoEvaluacion == 4) {
        fnCargarDatosMonitoreo();
    }
    else {
        fnCargarDatosEvaluacion();
        fnCargarLineasDetalle();
    }

    //Solo Ver
    if (!bModoEditar || EstadoEvaluacion == 2) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = true;
    }
    //Anulado
    else if (EstadoEvaluacion == 3) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = false;
    }
}
//#endregion


//#region GET Cargar Datos de Evaluacion para Editar
function fnCargarDatosEvaluacion() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_form?code=Yl68m4mOaMYua7CjjPCFOWT5E6fNotQtWa63t0GOpKiamhBNMUpfjQ==&AccionBackEnd=DatosEvaluacionILU&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaEvaluacion = response.lista_DatosEvaluacion

        fnSetDatosEvaluacion(ListaEvaluacion);

    });
}
//#endregion


//#region SET Asignar Datos de Evaluacion para Editar
async function fnSetDatosEvaluacion(lista) {

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_zona option[value='" + lista[0].IdZona + "']").prop("selected", true);
    $("#dat_etapa option[value='" + lista[0].IdEtapa + "']").prop("selected", true);
    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(lista[0].CodigoTareaMonitoreo);

    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaEjecucion != "") { $('#dFechaRegistro').val(lista[0].FechaEjecucion); }

    $('#sCondicionMonitoreo').val(lista[0].CondicionMonitoreo);

    $('#sMarcaMedicion').val(lista[0].MarcaMedicion);
    $('#sModeloMedicion').val(lista[0].ModeloMedicion);
    $('#sNumSerie').val(lista[0].Serie);

    if (EstadoEvaluacion == 3) {
        fnSetMotivos();
        fnSetDatosMotivosAnulacion(lista);
    }

}
//#endregion


//#region CRUD: Programar | Guardar | Cerrar | Anular
function fnCRUDEvaluacionMonitoreo(CodAccion, IdEstado) {

    if (!fnValidarLineas()) {
        return;
    }
    else {
        fnConfirmarLineas()
    }

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //Tipo de Evaluacion
    data.TipoEvaluacion = IdTipoEvaluacion;

    //CodAccion => 0: Registrar | 1: Modificar | 2:Cerrar | 3: Anular | 4: Programar 
    if (CodAccion == 3 || CodAccion == 4) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = "1900-01-01";

        data.CondicionMonitoreo = "";

        data.MarcaMedicion = "";
        data.ModeloMedicion = "";
        data.Serie = "";

        data.listaLugares = ""
        data.listaNivelPromedio = ""
        data.listaNivelRecomendado = ""
        data.listaCumplimientoNorm = ""
        data.listaCumplimientoPorc = ""
        data.listaEstado = ""
        data.listaMotivo = ""


        data.MotivoSeleccionado = MotivoAnulacionSeleccionado;
        data.MotivoEscribir = MotivoAnulacionEscribir

        data.accion = CodAccion;

    }

    if (CodAccion == 0 || CodAccion == 1 || CodAccion == 2) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = $("#dFechaRegistro").val();

        data.CondicionMonitoreo = $("#sCondicionMonitoreo").val();

        data.MarcaMedicion = $("#sMarcaMedicion").val();
        data.ModeloMedicion = $("#sModeloMedicion").val();
        data.Serie = $("#sNumSerie").val();

        data.listaLugares = listaLugares.join();
        data.listaNivelPromedio = listaNivelPromedio.join();
        data.listaNivelRecomendado = listaNivelRecomendado.join();
        data.listaCumplimientoNorm = listaCumplimientoNorm.join();
        data.listaCumplimientoPorc = listaCumplimientoPorc.join('|');
        data.listaEstado = listaEstado.join();
        data.listaMotivo = listaMotivo.join();

        data.MotivoSeleccionado = 0;
        data.MotivoEscribir = '';

        data.accion = CodAccion;
    }

    showLoading();

    let url = apiUrlsho + `/api/ho_Post_monitoreo_evaluar?code=FhnsxxYBMqr1RirUftBaVPsevFlBysozPo2DzN3CJ/UieNTCcE4Cfw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    hideLoading();

    return $.ajax(settings).done((response) => {


        EjecutarModal = (CodAccion >= 0 && response.Id > 0) ? true : false;

        switch (CodAccion) {
            case 0:
                TituloModal = 'Formulario guardado con éxito';
                break;
            case 1:
                TituloModal = 'Se actualizó la evaluación de monitoreo con éxito';
                break;
            case 2:
                TituloModal = 'Se cerró la evaluación de monitoreo con éxito';
                break;
            case 3:
                TituloModal = 'Se anuló la tarea de monitoreo con éxito';
                break;
            case 4:
                TituloModal = 'Se programó la evaluación de monitoreo con éxito';
                break;
            default:
                break;
        }

        if (EjecutarModal) {

            Swal.fire({
                title: TituloModal,
                iconColor: "#8fbb02",
                iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                showConfirmButton: false,
                padding: "3em 3em 6em 3em ",
                timer: 1500,
            }).then(() => {
                fnVolverIndexMonitoreoEva();
            });

        }

    });

}
//#endregion


//#region AGREGAR a Lista Detalle
function fnAgregarDetalle() {
    this.ListaDetalleILU.push
        ({
            nNro: this.ListaDetalleILU.length + 1,
            sNombreLugar: $("#sLugar").val(),
            sNivelPromedio: "",
            sNivelRecomendado: "",
            nCumplimientoNormativa: "",
            nCumplimientoPorc: "",
            nIdEstado: 1,
            sEstado: "Abierto",
            sMotivo: "",
            bActivo: 0
        });

}
//#endregion


//#region AGREGAR Fila
function fnAgregarFila() {

    let sLugar = $("#sLugar").val();
    //Validar
    if (sLugar == "" || sLugar == undefined || sLugar == null) {
        return Swal.fire(
            'Advertencia',
            'Debe colocar el nombre del lugar antes de agregar a lista.',
            'warning'
        )
    }

    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();
    opcCumplNorm = OpcionesCumplimientoNormativa()
    var table = $('#table_form_ilu').DataTable();

    fnAgregarDetalle();

    var nFilas = this.ListaDetalleILU.length // Tamaño de Filas Totales
    var n = this.ListaDetalleILU.length - 1//Fila Actual
    var lista = ListaDetalleILU[n]//Objeto Actual

    html_tbody = OpcionesTable(n);


    table.row.add([
        `<label id="nNro_${n}">         ${nFilas}   </label>`,
        `<label id="sNombreLugar_${n}"> ${sLugar}   </label>`,
        `<input type="number" id="sNivelPromedio_${n}" min="0"/>`,
        `<input type="number" id="sNivelRecomendado_${n}" min="0"/>`,
        `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcCumplNorm}</select></div>`,
        `<input type="number" id="nCumplimientoPorc_${n}" step="0.01"/>`,
        `<label id="sEstado_${n}"/>${lista.sEstado}</label>`,
        `<input type="text" id="sMotivo_${n}" value="Sin Motivo"/>`,
        html_tbody,

    ]).draw(false);

    document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
    document.getElementById(`sMotivo_${n}`).readOnly = true;

    $('#sLugar').val("");

    $('#table_form_ilu-count-row').text(table.rows().count());
}
//#endregion


//#region Confirmar Lineas Detalles
function fnConfirmarLineas() {

    for (let i = 0; i < this.ListaDetalleILU.length; i++) {
        this.listaLugares.push(this.ListaDetalleILU[i].sNombreLugar)
        this.listaNivelPromedio.push(this.ListaDetalleILU[i].sNivelPromedio)
        this.listaNivelRecomendado.push(this.ListaDetalleILU[i].sNivelRecomendado)
        this.listaCumplimientoNorm.push(this.ListaDetalleILU[i].nCumplimientoNormativa)
        this.listaCumplimientoPorc.push(this.ListaDetalleILU[i].nCumplimientoPorc)
        this.listaEstado.push(this.ListaDetalleILU[i].nIdEstado)
        this.listaMotivo.push(this.ListaDetalleILU[i].sMotivo)
    }
}
//#endregion


//#region Validar Lineas Detalles
function fnValidarLineas() {

    var bValido = true;

    for (let n = 0; n < this.ListaDetalleILU.length; n++) {

        ListaDetalleILU[n].sNivelPromedio = $(`#sNivelPromedio_${n}`).val();
        ListaDetalleILU[n].sNivelRecomendado = $(`#sNivelRecomendado_${n}`).val();
        ListaDetalleILU[n].nCumplimientoNormativa = $(`#nCumplimientoNormativa_${n} option:selected`).val();
        ListaDetalleILU[n].nCumplimientoPorc = $(`#nCumplimientoPorc_${n}`).val();
        ListaDetalleILU[n].sMotivo = $(`#sMotivo_${n}`).val();

        if (
            ListaDetalleILU[n].sNombreLugar == "" ||
            ListaDetalleILU[n].sNivelPromedio == "" ||
            ListaDetalleILU[n].sNivelRecomendado == "" ||
            ListaDetalleILU[n].nCumplimientoNormativa == "" ||
            ListaDetalleILU[n].nCumplimientoPorc == "" ||
            ListaDetalleILU[n].nIdEstado == 0
        ) {
            bValido = false
            Swal.fire(
                'Advertencia',
                `Debe completar los datos del lugar Nro. ${n + 1} de la tabla de Datos de monitoreo.`,
                'warning'
            )
        }

        //Filas Anuladas
        if (ListaDetalleILU[n].nIdEstado == 3 && ListaDetalleILU[n].sMotivo == "") {
            bValido = false
            Swal.fire(
                'Advertencia',
                `El Lugar Nro. ${n + 1} tiene estado Anulado, debe completar el Motivo.`,
                'warning'
            )
        }

    }

    return bValido

}
//#endregion


//#region GET Cargar Lineas Detalles
function fnCargarLineasDetalle() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_table?code=60Gs5EVzToGI4HE97MLaoGyLDDlHeh3qxh3Y0PJl95KECrDYlsrF9g==&AccionBackEnd=TablaEvaluacionILU&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaTabla = response.lista_DatosEvaluacion

        if (ListaTabla.length > 0) {
            fnSetLineasDetalle(ListaTabla);
        }

    });
}
//#endregion


//#region SET Asignar Lineas Detalles 
function fnSetLineasDetalle(lista) {
    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();

    var table = $('#table_form_ilu').DataTable({
        paging: false,
        ordering: false,
        bInfo: false
    });;

    opcCumplNorm = OpcionesCumplimientoNormativa();

    for (let n = 0; n < lista.length; n++) {

        this.ListaDetalleILU.push
            ({
                nNro: n + 1,
                sNombreLugar: lista[n].Lugar,
                sNivelPromedio: lista[n].NivelPromedio,
                sNivelRecomendado: lista[n].NivelRecomendado,
                nCumplimientoNormativa: lista[n].CumplimientoNormativa,
                nCumplimientoPorc: lista[n].CumplimientoPorc,
                nIdEstado: lista[n].Estado,
                sEstado: lista[n].NombreEstado,
                sMotivo: lista[n].Motivo
            });

        html_tbody = OpcionesTable(n);

        table.row.add([
            `<label id="nNro_${n}">         ${n + 1}   </label>`,
            `<label id="sNombreLugar_${n}"> ${lista[n].Lugar}   </label>`,
            `<input type="number" id="sNivelPromedio_${n}" value="${lista[n].NivelPromedio}" min="0"/>`,
            `<input type="number" id="sNivelRecomendado_${n}" value="${lista[n].NivelRecomendado}" min="0"/>`,
            `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcCumplNorm}</select></div>`,
            `<input type="number" id="nCumplimientoPorc_${n}" value="${lista[n].CumplimientoPorc}" step="0.01"/>`,
            `<label id="sEstado_${n}"/>${lista[n].NombreEstado}</label>`,
            `<input type="text" id="sMotivo_${n}" value="${lista[n].Motivo}" readOnly/>`,
            html_tbody,

        ]).draw(false);

        $(`#nCumplimientoNormativa_${n} option[value='` + lista[n].CumplimientoNormativa + "']").prop("selected", true);

        if (lista[n].Estado == 1) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
        }
        else if (lista[n].Estado == 2) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";
        }
        else if (lista[n].Estado == 3) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";
            document.getElementById(`sMotivo_${n}`).readOnly = false;
        }
    }

    $('#table_form_ilu-count-row').text(table.rows().count());

}
//#endregion


//#region CERRAR Fila
function fnCerrarFila(n) {
    ListaDetalleILU[n].nIdEstado = 2
    ListaDetalleILU[n].sEstado = "Cerrado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Cerrado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = true;
    //document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion


//#region ANULAR Fila
function fnAnularFila(n) {

    ListaDetalleILU[n].nIdEstado = 3
    ListaDetalleILU[n].sEstado = "Anulado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Anulado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = false;
}
//#endregion

