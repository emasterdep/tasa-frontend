//#region Variables
var ListaEvaluacion;

var ListaTabla;
var ListaDetalleHumosMet = [];

var listaCodigoFiltro = []
var listaParaAnalisis = []
var listaResultadoLab = []
var listaTiempoExposicion = []
var listaConcentracionPuesto = []
var listaLMP = []
var listaLMPCorregido = []
var listaIndiceExposicion = []
var listaNivelRiesgo = []
var listaCumplimientoNormativa = []
var listaEstado = []
var listaMotivo = []
//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    fnOcultarMostrarMotivo();
    fnOcultarMostrarBotones();

    onInit();

    $("#sNumeroDoc").on('change', onChangeNroDocumento);
    $("#dat_EPP").on('change', onChangeEPP);


});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnCargarSedesAreasGerenciasMonitoreo(),
        fnCargarListaPuestos(),
        fnCargarPlucksMonitoreo(),
        fnGetSelectsForm(IdTipoEvaluacion)
    ]);

    fnSetSelectsMonitoreo();
    fnSetSelectsForm();

    fnCargarDatosGeneral();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Cargar Datos en General
async function fnCargarDatosGeneral() {
    //1: Abierto, 2:Cerrado, 3: Anulado, 4:Programado, 5:En Progreso
    if (EstadoEvaluacion == 1) {
        fnFormDisabled();
        fnCargarDatosMonitoreo();
    }
    else if (EstadoEvaluacion == 4) {
        fnCargarDatosMonitoreo();
    }
    else {
        fnCargarDatosEvaluacion();
        fnCargarLineasDetalle();
    }

    //Solo Ver
    if (!bModoEditar || EstadoEvaluacion == 2) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = true;
    }
    //Anulado
    else if (EstadoEvaluacion == 3) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = false;
    }
}
//#endregion


//#region GET Cargar Datos de Evaluacion para Editar
function fnCargarDatosEvaluacion() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_form?code=Yl68m4mOaMYua7CjjPCFOWT5E6fNotQtWa63t0GOpKiamhBNMUpfjQ==&AccionBackEnd=DatosEvaluacionHumosMetalicos&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaEvaluacion = response.lista_DatosEvaluacion

        fnSetDatosEvaluacion(ListaEvaluacion);

    });
}
//#endregion


//#region SET Asignar Datos de Evaluacion para Editar
function fnSetDatosEvaluacion(lista) {

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $("#dat_puesto option[value='" + lista[0].IdPuestoCargo + "']").prop("selected", true);
    $("#dat_zona option[value='" + lista[0].IdZona + "']").prop("selected", true);
    $("#dat_etapa option[value='" + lista[0].IdEtapa + "']").prop("selected", true);

    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(lista[0].CodigoTareaMonitoreo);

    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaEjecucion != "") { $('#dFechaRegistro').val(lista[0].FechaEjecucion); }

    $('#sDescripcionZona').val(lista[0].DescripcionZona);
    $('#sFuente').val(lista[0].Fuente);

    if (lista[0].NroDocumento > 0) {
        $('#sNumeroDoc').val(lista[0].NroDocumento);
    }
    $('#sNombres').val(lista[0].Nombres);
    $('#sApellidos').val(lista[0].Apellidos);
    $('#nRegimen').val(lista[0].Regimen);
    $('#nTiempoPuesto').val(lista[0].TiempoPuesto);
    $('#nTiempoAlmuerzo').val(lista[0].TiempoAlmuerzo);
    $('#sActivGeneral').val(lista[0].ActividadesGenerales);

    $("#dat_Ingenieria option[value='" + lista[0].Ingenieria + "']").prop("selected", true);
    $('#sDetallar').val(lista[0].Detallar);
    $("#dat_EPP option[value='" + lista[0].EPP + "']").prop("selected", true);
    $('#sAdministrativo').val(lista[0].Administrativo);
    $('#sMarcaControl').val(lista[0].MarcaControl);
    $('#sModeloControl').val(lista[0].ModeloControl);
    $('#sNRREquipo').val(lista[0].NRREquipo);

    if (lista[0].EPP == 3) {
        $('#divDobleProteccion').show();
        $('#sMarcaControl_2').val(lista[0].MarcaControl_2);
        $('#sModeloControl_2').val(lista[0].ModeloControl_2);
        $('#sNRREquipo_2').val(lista[0].NRREquipo_2);
    }

    $('#sMarcaMedicion').val(lista[0].MarcaMedicion);
    $('#sModeloMedicion').val(lista[0].ModeloMedicion);
    $('#nResultadosPreVerif').val(lista[0].ResultadosPreVerif);
    $('#nResultadosPostVerif').val(lista[0].ResultadosPostVerif);
    $('#nFlujoPromedio').val(lista[0].FlujoPromedio);
    $('#sCodigoFiltro').val(lista[0].CodigoFiltro);

    if (EstadoEvaluacion == 3) {
        fnSetMotivos();
        fnSetDatosMotivosAnulacion(lista);
    }
}
//#endregion


//#region CRUD: Programar | Guardar | Cerrar | Anular
function fnCRUDEvaluacionMonitoreo(CodAccion, IdEstado) {

    if (!fnValidarLineas()) {
        return;
    }
    else {
        fnConfirmarLineas()
    }

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //Tipo de Evaluacion
    data.TipoEvaluacion = IdTipoEvaluacion;

    //CodAccion => 0: Registrar | 1: Modificar | 2:Cerrar | 3: Anular | 4: Programar 
    if (CodAccion == 3 || CodAccion == 4) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = "1900-01-01";

        data.DescripcionZona = "";
        data.Fuente = "";

        data.NroDocumento = 0;
        data.Regimen = 0;
        data.ActividadesGenerales = "";
        data.TiempoAlmuerzo = 0;

        data.Ingenieria = 0;
        data.Detallar = "";
        data.Administrativo = "";
        data.EPP = 0;
        data.MarcaControl = "";
        data.ModeloControl = "";
        data.NRREquipo = "";
        data.MarcaControl_2 = "";
        data.ModeloControl_2 = "";
        data.NRREquipo_2 = "";

        data.MarcaMedicion = "";
        data.ModeloMedicion = "";
        data.Serie = "";
        data.ResultadosPreVerif = 0;
        data.ResultadosPostVerif = 0;
        data.FlujoPromedio = 0;
        data.CodigoFiltro = "";

        data.listaCodigoFiltro = ""
        data.listaParaAnalisis = ""
        data.listaResultadoLab = ""
        data.listaTiempoExposicion = ""
        data.listaConcentracionPuesto = ""
        data.listaLMP = ""
        data.listaLMPCorregido = ""
        data.listaIndiceExposicion = ""
        data.listaNivelRiesgo = ""
        data.listaCumplimientoNormativa = ""
        data.listaEstado = ""
        data.listaMotivo = ""

        data.MotivoSeleccionado = MotivoAnulacionSeleccionado;
        data.MotivoEscribir = MotivoAnulacionEscribir

        data.accion = CodAccion;

    }

    if (CodAccion == 0 || CodAccion == 1 || CodAccion == 2) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = $("#dFechaRegistro").val();

        data.DescripcionZona = $("#sDescripcionZona").val();
        data.Fuente = $("#sFuente").val();

        data.NroDocumento = $("#sNumeroDoc").val();
        data.Regimen = $("#nRegimen").val();
        data.ActividadesGenerales = $("#sActivGeneral").val();
        data.TiempoAlmuerzo = $("#nTiempoAlmuerzo").val();

        data.Ingenieria = $("#dat_Ingenieria option:selected").val();
        data.Detallar = $("#sDetallar").val();
        data.Administrativo = $("#sAdministrativo").val();
        data.EPP = $("#dat_EPP option:selected").val();
        data.MarcaControl = $("#sMarcaControl").val();
        data.ModeloControl = $("#sModeloControl").val();
        data.NRREquipo = $("#sNRREquipo").val();
        data.MarcaControl_2 = $("#sMarcaControl_2").val();
        data.ModeloControl_2 = $("#sModeloControl_2").val();
        data.NRREquipo_2 = $("#sNRREquipo_2").val();

        data.MarcaMedicion = $("#sMarcaMedicion").val();
        data.ModeloMedicion = $("#sModeloMedicion").val();
        data.Serie = $("#sNumSerie").val();
        data.ResultadosPreVerif = $("#nResultadosPreVerif").val();
        data.ResultadosPostVerif = $("#nResultadosPostVerif").val();
        data.FlujoPromedio = $("#nFlujoPromedio").val();
        data.CodigoFiltro = $("#sCodigoFiltro").val();

        data.listaCodigoFiltro = listaCodigoFiltro.join();
        data.listaParaAnalisis = listaParaAnalisis.join();

        data.listaResultadoLab = listaResultadoLab.join('|');
        data.listaTiempoExposicion = listaTiempoExposicion.join('|');
        data.listaConcentracionPuesto = listaConcentracionPuesto.join('|');
        data.listaLMP = listaLMP.join('|');
        data.listaLMPCorregido = listaLMPCorregido.join('|');
        data.listaIndiceExposicion = listaIndiceExposicion.join('|');

        data.listaNivelRiesgo = listaNivelRiesgo.join();
        data.listaCumplimientoNormativa = listaCumplimientoNormativa.join();
        data.listaEstado = listaEstado.join();
        data.listaMotivo = listaMotivo.join();

        data.MotivoSeleccionado = 0;
        data.MotivoEscribir = '';

        data.accion = CodAccion;
    }

    showLoading();

    let url = apiUrlsho + `/api/ho_Post_monitoreo_evaluar?code=FhnsxxYBMqr1RirUftBaVPsevFlBysozPo2DzN3CJ/UieNTCcE4Cfw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    hideLoading();

    return $.ajax(settings).done((response) => {

        EjecutarModal = (CodAccion >= 0 && response.Id > 0) ? true : false;

        switch (CodAccion) {
            case 0:
                TituloModal = 'Formulario guardado con éxito';
                break;
            case 1:
                TituloModal = 'Se actualizó la evaluación de monitoreo con éxito';
                break;
            case 2:
                TituloModal = 'Se cerró la evaluación de monitoreo con éxito';
                break;
            case 3:
                TituloModal = 'Se anuló la tarea de monitoreo con éxito';
                break;
            case 4:
                TituloModal = 'Se programó la evaluación de monitoreo con éxito';
                break;
            default:
                break;
        }


        if (EjecutarModal) {
            Swal.fire({
                title: TituloModal,
                iconColor: "#8fbb02",
                iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                showConfirmButton: false,
                padding: "3em 3em 6em 3em ",
                timer: 1500,
            }).then(() => {
                fnVolverIndexMonitoreoEva();
            });

        }

    });

}
//#endregion


//#region SET Opciones Para Analisis
function OpcionesParaAnalisis() {

    contenido = ''
    contenido += `<option value=""></option>`;

    objSelect["ParaAnalisis"].forEach((Item) => {
        contenido += `<option value="${Item.Id}">${Item.ParaAnalisis}</option>`
    });

    return contenido;
}
//#endregion


//#region AGREGAR a Lista Detalle
function fnAgregarDetalle() {
    this.ListaDetalleHumosMet.push
        ({
            nNro: this.ListaDetalleHumosMet.length + 1,
            sNombreCodigoFiltro: $("#sLugar").val(),
            nParaAnalisis: "",

            nResultadoLab: "",
            nTiempoExposicion: "",
            nConcentracionPuesto: "",
            nLMP: "",
            nLMPCorregido: "",
            nIndiceExposicion: "",

            nNivelRiesgo: "",
            nCumplimientoNormativa: "",
            nIdEstado: 1,
            sEstado: "Abierto",
            sMotivo: ""
        });

}
//#endregion


//#region AGREGAR Fila
function fnAgregarFila() {

    let sCodigoFiltro = $("#sLugar").val();
    //Validar
    if (sCodigoFiltro == "" || sCodigoFiltro == undefined || sCodigoFiltro == null) {
        return Swal.fire(
            'Advertencia',
            'Debe colocar el nombre del lugar antes de agregar a lista.',
            'warning'
        )
    }

    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();

    var table = $('#table_form_hum_met').DataTable({
        paging: false,
        ordering: false,
        bInfo: false
    });;

    fnAgregarDetalle();

    var nFilas = this.ListaDetalleHumosMet.length // Tamaño de Filas Totales
    var n = this.ListaDetalleHumosMet.length - 1//Fila Actual
    var lista = ListaDetalleHumosMet[n]//Objeto Actual

    opcParaAnalisis = OpcionesParaAnalisis();
    opcCumplNorm = OpcionesCumplimientoNormativa();
    opcNivelRiesgo = OpcionesNivelRiesgo();

    html_tbody = OpcionesTable(n);

    table.row.add([
        `<label id="nNro_${n}">         ${nFilas}   </label>`,
        `<label id="sNombreCodigoFiltro_${n}"> ${sCodigoFiltro}   </label>`,

        `<input type="number" id="nResultadoLab_${n}" step="0.01"/>`,
        `<select id="nParaAnalisis_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcParaAnalisis}</select></div>`,

        `<input type="number" id="nTiempoExposicion_${n}" step="0.01"/>`,
        `<input type="number" id="nConcentracionPuesto_${n}" step="0.01" />`,
        `<input type="number" id="nLMP_${n}" step="0.01" />`,
        `<input type="number" id="nLMPCorregido_${n}" step="0.01"/>`,
        `<input type="number" id="nIndiceExposicion_${n}" step="0.01"/>`,

        `<select id="nNivelRiesgo_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcNivelRiesgo}</select></div>`,
        `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcCumplNorm}</select></div>`,
        `<label id="sEstado_${n}"/>${lista.sEstado}</label>`,
        `<input type="text" id="sMotivo_${n}" value="Sin Motivo"/>`,
        html_tbody,

    ]).draw(false);

    document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
    document.getElementById(`sMotivo_${n}`).readOnly = true;

    $('#sLugar').val("");

    $('#table_form_hum_met-count-row').text(table.rows().count());
}
//#endregion


//#region Confirmar Lineas Detalles
function fnConfirmarLineas() {

    for (let i = 0; i < this.ListaDetalleHumosMet.length; i++) {
        this.listaCodigoFiltro.push(this.ListaDetalleHumosMet[i].sNombreCodigoFiltro)

        this.listaParaAnalisis.push(this.ListaDetalleHumosMet[i].nParaAnalisis)
        this.listaResultadoLab.push(this.ListaDetalleHumosMet[i].nResultadoLab)
        this.listaTiempoExposicion.push(this.ListaDetalleHumosMet[i].nTiempoExposicion)
        this.listaConcentracionPuesto.push(this.ListaDetalleHumosMet[i].nConcentracionPuesto)
        this.listaLMP.push(this.ListaDetalleHumosMet[i].nLMP)
        this.listaLMPCorregido.push(this.ListaDetalleHumosMet[i].nLMPCorregido)
        this.listaIndiceExposicion.push(this.ListaDetalleHumosMet[i].nIndiceExposicion)
        this.listaNivelRiesgo.push(this.ListaDetalleHumosMet[i].nNivelRiesgo)

        this.listaCumplimientoNormativa.push(this.ListaDetalleHumosMet[i].nCumplimientoNormativa)
        this.listaEstado.push(this.ListaDetalleHumosMet[i].nIdEstado)
        this.listaMotivo.push(this.ListaDetalleHumosMet[i].sMotivo)
    }
}
//#endregion


//#region Validar Lineas Detalles
function fnValidarLineas() {

    var bValido = true;

    for (let n = 0; n < this.ListaDetalleHumosMet.length; n++) {

        ListaDetalleHumosMet[n].nParaAnalisis = $(`#nParaAnalisis_${n}`).val();
        ListaDetalleHumosMet[n].nResultadoLab = $(`#nResultadoLab_${n}`).val();
        ListaDetalleHumosMet[n].nTiempoExposicion = $(`#nTiempoExposicion_${n}`).val();
        ListaDetalleHumosMet[n].nConcentracionPuesto = $(`#nConcentracionPuesto_${n}`).val();
        ListaDetalleHumosMet[n].nLMP = $(`#nLMP_${n}`).val();
        ListaDetalleHumosMet[n].nLMPCorregido = $(`#nLMPCorregido_${n}`).val();
        ListaDetalleHumosMet[n].nIndiceExposicion = $(`#nIndiceExposicion_${n}`).val();
        ListaDetalleHumosMet[n].nNivelRiesgo = $(`#nNivelRiesgo_${n}`).val();

        ListaDetalleHumosMet[n].nCumplimientoNormativa = $(`#nCumplimientoNormativa_${n} option:selected`).val();
        ListaDetalleHumosMet[n].sMotivo = $(`#sMotivo_${n}`).val();


        if (

            ListaDetalleHumosMet[n].sNombreCodigoFiltro == "" ||
            ListaDetalleHumosMet[n].nParaAnalisis == "" ||
            ListaDetalleHumosMet[n].nResultadoLab == "" ||
            ListaDetalleHumosMet[n].nTiempoExposicion == "" ||
            ListaDetalleHumosMet[n].nConcentracionPuesto == "" ||
            ListaDetalleHumosMet[n].nLMP == "" ||

            ListaDetalleHumosMet[n].nLMPCorregido == "" ||
            ListaDetalleHumosMet[n].nIndiceExposicion == "" ||
            ListaDetalleHumosMet[n].nNivelRiesgo == "" ||

            ListaDetalleHumosMet[n].nCumplimientoNormativa == "" ||
            ListaDetalleHumosMet[n].nIdEstado == 0
        ) {
            bValido = false
            Swal.fire(
                'Advertencia',
                `Debe completar los datos del agente Nro. ${n + 1} de la tabla de Datos de monitoreo.`,
                'warning'
            )
        }

        //Filas Anuladas
        if (ListaDetalleHumosMet[n].nIdEstado == 3 && ListaDetalleHumosMet[n].sMotivo == "") {
            bValido = false
            Swal.fire(
                'Advertencia',
                `El Agente Nro. ${n + 1} tiene estado Anulado, debe completar el Motivo.`,
                'warning'
            )
        }

    }

    return bValido

}
//#endregion


//#region GET Cargar Lineas Detalles
function fnCargarLineasDetalle() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_table?code=60Gs5EVzToGI4HE97MLaoGyLDDlHeh3qxh3Y0PJl95KECrDYlsrF9g==&AccionBackEnd=TablaEvaluacionHumosMetalicos&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaTabla = response.lista_DatosEvaluacion

        if (ListaTabla.length > 0) {
            fnSetLineasDetalle(ListaTabla);
        }

    });
}
//#endregion


//#region SET Asignar Lineas Detalles 
function fnSetLineasDetalle(lista) {
    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();

    var table = $('#table_form_hum_met').DataTable({
        paging: false,
        ordering: false,
        bInfo: false
    });;

    opcCumplNorm = OpcionesCumplimientoNormativa();
    opcParaAnalisis = OpcionesParaAnalisis();
    opcNivelRiesgo = OpcionesNivelRiesgo();

    for (let n = 0; n < lista.length; n++) {

        this.ListaDetalleHumosMet.push
            ({
                nNro: n + 1,
                sNombreCodigoFiltro: lista[n].CodigoFiltro,

                nResultadoLab: lista[n].ResultadoLab,
                nParaAnalisis: lista[n].ParaAnalisis,

                nTiempoExposicion: lista[n].TiempoExposicion,
                nConcentracionPuesto: lista[n].ConcentracionPuesto,
                nLMP: lista[n].LMP,
                nLMPCorregido: lista[n].LMPCorregido,
                nIndiceExposicion: lista[n].IndiceExposicion,

                nNivelRiesgo: lista[n].NivelRiesgo,
                nCumplimientoNormativa: lista[n].CumplimientoNormativa,

                nIdEstado: lista[n].Estado,
                sEstado: lista[n].NombreEstado,
                sMotivo: lista[n].Motivo
            });

        html_tbody = OpcionesTable(n);

        table.row.add([
            `<label id="nNro_${n}">         ${n + 1}   </label>`,
            `<label id="sNombreCodigoFiltro_${n}"> ${lista[n].CodigoFiltro}   </label>`,

            `<input type="number" id="nResultadoLab_${n}" value="${lista[n].ResultadoLab}" step="0.01"/>`,
            `<select id="nParaAnalisis_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcParaAnalisis}</select></div>`,

            `<input type="number" id="nTiempoExposicion_${n}" value="${lista[n].TiempoExposicion}" step="0.01"/>`,
            `<input type="number" id="nConcentracionPuesto_${n}" value="${lista[n].ConcentracionPuesto}" step="0.01"/>`,
            `<input type="number" id="nLMP_${n}" value="${lista[n].LMP}" step="0.01"/>`,
            `<input type="number" id="nLMPCorregido_${n}" value="${lista[n].LMPCorregido}" step="0.01"/>`,
            `<input type="number" id="nIndiceExposicion_${n}" value="${lista[n].IndiceExposicion}" step="0.01"/>`,

            `<select id="nNivelRiesgo_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcNivelRiesgo}</select></div>`,
            `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcCumplNorm}</select></div>`,
            `<label id="sEstado_${n}"/>${lista[n].NombreEstado}</label>`,
            `<input type="text" id="sMotivo_${n}" value="${lista[n].Motivo}" readOnly/>`,
            html_tbody,

        ]).draw(false);

        $(`#nCumplimientoNormativa_${n} option[value='` + lista[n].CumplimientoNormativa + "']").prop("selected", true);
        $(`#nNivelRiesgo_${n} option[value='` + lista[n].NivelRiesgo + "']").prop("selected", true);
        $(`#nParaAnalisis_${n} option[value='` + lista[n].ParaAnalisis + "']").prop("selected", true);


        if (lista[n].Estado == 1) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
        }
        else if (lista[n].Estado == 2) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";
        }
        else if (lista[n].Estado == 3) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";
            document.getElementById(`sMotivo_${n}`).readOnly = false;
        }
    }

    $('#table_form_hum_met-count-row').text(table.rows().count());

}
//#endregion


//#region CERRAR Fila
function fnCerrarFila(n) {
    ListaDetalleHumosMet[n].nIdEstado = 2
    ListaDetalleHumosMet[n].sEstado = "Cerrado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Cerrado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = true;
    //document.getElementById(`sMotivo_${n}`).readOnly = false;


}
//#endregion


//#region ANULAR Fila
function fnAnularFila(n) {

    ListaDetalleHumosMet[n].nIdEstado = 3
    ListaDetalleHumosMet[n].sEstado = "Anulado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Anulado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";


    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion

