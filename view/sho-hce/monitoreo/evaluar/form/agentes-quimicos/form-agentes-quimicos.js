//#region Variables
var ListaEvaluacion;
//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    fnOcultarMostrarMotivo();
    fnOcultarMostrarBotones();

    onInit();

    $("#sNumeroDoc").on('change', onChangeNroDocumento);
    $("#dat_EPP").on('change', onChangeEPP);

});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnCargarSedesAreasGerenciasMonitoreo(),
        fnCargarListaPuestos(),
        fnCargarPlucksMonitoreo(),
        fnGetSelectsForm(IdTipoEvaluacion)
    ]);

    fnSetSelectsMonitoreo();
    fnSetSelectsForm();

    fnCargarDatosGeneral();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Cargar Datos en General
async function fnCargarDatosGeneral() {
    //1: Abierto, 2:Cerrado, 3: Anulado, 4:Programado, 5:En Progreso
    if (EstadoEvaluacion == 1) {
        fnFormDisabled();
        fnCargarDatosMonitoreo();
    }
    else if (EstadoEvaluacion == 4) {
        fnCargarDatosMonitoreo();
    }
    else {
        fnCargarDatosEvaluacion();
    }

    //Solo Ver
    if (!bModoEditar || EstadoEvaluacion == 2) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = true;
    }
    //Anulado
    else if (EstadoEvaluacion == 3) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = false;
    }
}
//#endregion


//#region GET Cargar Datos de Evaluacion para Editar
function fnCargarDatosEvaluacion() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_form?code=Yl68m4mOaMYua7CjjPCFOWT5E6fNotQtWa63t0GOpKiamhBNMUpfjQ==&AccionBackEnd=DatosEvaluacionAgentesQuimicos&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaEvaluacion = response.lista_DatosEvaluacion

        fnSetDatosEvaluacion(ListaEvaluacion);

    });
}
//#endregion


//#region SET Asignar Datos de Evaluacion para Editar
function fnSetDatosEvaluacion(lista) {

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $("#dat_puesto option[value='" + lista[0].IdPuestoCargo + "']").prop("selected", true);
    $("#dat_zona option[value='" + lista[0].IdZona + "']").prop("selected", true);
    $("#dat_etapa option[value='" + lista[0].IdEtapa + "']").prop("selected", true);

    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(lista[0].CodigoTareaMonitoreo);

    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaEjecucion != "") { $('#dFechaRegistro').val(lista[0].FechaEjecucion); }

    $('#sDescripcionZona').val(lista[0].DescripcionZona);
    $('#sFuente').val(lista[0].Fuente);

    if (lista[0].NroDocumento > 0) {
        $('#sNumeroDoc').val(lista[0].NroDocumento);
    }
    $('#sNombres').val(lista[0].Nombres);
    $('#sApellidos').val(lista[0].Apellidos);
    $('#nRegimen').val(lista[0].Regimen);
    $('#nTiempoPuesto').val(lista[0].TiempoPuesto);
    $('#nTiempoAlmuerzo').val(lista[0].TiempoAlmuerzo);
    $('#sActivGeneral').val(lista[0].ActividadesGenerales);

    $("#dat_Ingenieria option[value='" + lista[0].Ingenieria + "']").prop("selected", true);
    $('#sDetallar').val(lista[0].Detallar);
    $("#dat_EPP option[value='" + lista[0].EPP + "']").prop("selected", true);
    $('#sAdministrativo').val(lista[0].Administrativo);
    $('#sMarcaControl').val(lista[0].MarcaControl);
    $('#sModeloControl').val(lista[0].ModeloControl);
    $('#sNRREquipo').val(lista[0].NRREquipo);

    if (lista[0].EPP == 3) {
        $('#divDobleProteccion').show();
        $('#sMarcaControl_2').val(lista[0].MarcaControl_2);
        $('#sModeloControl_2').val(lista[0].ModeloControl_2);
        $('#sNRREquipo_2').val(lista[0].NRREquipo_2);
    }

    $('#sMarcaMedicion').val(lista[0].MarcaMedicion);
    $('#sModeloMedicion').val(lista[0].ModeloMedicion);
    $('#sNumSerie').val(lista[0].Serie);

    $('#nHoraMedicion').val(lista[0].HoraMedicion);
    $('#sAgenteQuimico').val(lista[0].AgenteQuimico);
    $('#sCAS').val(lista[0].CAS);
    $('#nTLVTWA').val(lista[0].TLVTWA);
    $('#nTLVTWACorregido').val(lista[0].TLVTWACorregido);

    $("#dat_NivelRiesgo option[value='" + lista[0].NivelRiesgo + "']").prop("selected", true);
    $("#dat_CumplimientoNormativa option[value='" + lista[0].CumplimientoNormativa + "']").prop("selected", true);

    if (EstadoEvaluacion == 3) {
        fnSetMotivos();
        fnSetDatosMotivosAnulacion(lista);
    }
}
//#endregion


//#region CRUD: Programar | Guardar | Cerrar | Anular
function fnCRUDEvaluacionMonitoreo(CodAccion, IdEstado) {

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //Tipo de Evaluacion
    data.TipoEvaluacion = IdTipoEvaluacion;

    //CodAccion => 0: Registrar | 1: Modificar | 2:Cerrar | 3: Anular | 4: Programar 
    if (CodAccion == 3 || CodAccion == 4) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = "1900-01-01";

        data.DescripcionZona = "";
        data.Fuente = "";

        data.NroDocumento = 0;
        data.Regimen = 0;
        data.ActividadesGenerales = "";
        data.TiempoAlmuerzo = 0;

        data.Ingenieria = 0;
        data.Detallar = "";
        data.Administrativo = "";
        data.EPP = 0;
        data.MarcaControl = "";
        data.ModeloControl = "";
        data.NRREquipo = "";
        data.MarcaControl_2 = "";
        data.ModeloControl_2 = "";
        data.NRREquipo_2 = "";

        data.MarcaMedicion = "";
        data.ModeloMedicion = "";
        data.Serie = "";

        data.HoraMedicion = "";
        data.AgenteQuimico = 0;
        data.CAS = "";
        data.TLVTWA = 0;
        data.TLVTWACorregido = 0;

        data.NivelRiesgo = 0;
        data.CumplimientoNormativa = 0;

        data.MotivoSeleccionado = MotivoAnulacionSeleccionado;
        data.MotivoEscribir = MotivoAnulacionEscribir

        data.accion = CodAccion;

    }

    if (CodAccion == 0 || CodAccion == 1 || CodAccion == 2) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = $("#dFechaRegistro").val();

        data.DescripcionZona = $("#sDescripcionZona").val();
        data.Fuente = $("#sFuente").val();

        data.NroDocumento = $("#sNumeroDoc").val();
        data.Regimen = $("#nRegimen").val();
        data.ActividadesGenerales = $("#sActivGeneral").val();
        data.TiempoAlmuerzo = $("#nTiempoAlmuerzo").val();

        data.Ingenieria = $("#dat_Ingenieria option:selected").val();
        data.Detallar = $("#sDetallar").val();
        data.Administrativo = $("#sAdministrativo").val();
        data.EPP = $("#dat_EPP option:selected").val();
        data.MarcaControl = $("#sMarcaControl").val();
        data.ModeloControl = $("#sModeloControl").val();
        data.NRREquipo = $("#sNRREquipo").val();
        data.MarcaControl_2 = $("#sMarcaControl_2").val();
        data.ModeloControl_2 = $("#sModeloControl_2").val();
        data.NRREquipo_2 = $("#sNRREquipo_2").val();

        data.MarcaMedicion = $("#sMarcaMedicion").val();
        data.ModeloMedicion = $("#sModeloMedicion").val();
        data.Serie = $("#sNumSerie").val();

        data.HoraMedicion = $("#nHoraMedicion").val();
        data.AgenteQuimico = $("#sAgenteQuimico").val();
        data.CAS = $("#sCAS").val();
        data.TLVTWA = $("#nTLVTWA").val();
        data.TLVTWACorregido = $("#nTLVTWACorregido").val();

        data.NivelRiesgo = $("#dat_NivelRiesgo option:selected").val();
        data.CumplimientoNormativa = $("#dat_CumplimientoNormativa option:selected").val();

        data.MotivoSeleccionado = 0;
        data.MotivoEscribir = '';

        data.accion = CodAccion;
    }

    showLoading();

    let url = apiUrlsho + `/api/ho_Post_monitoreo_evaluar?code=FhnsxxYBMqr1RirUftBaVPsevFlBysozPo2DzN3CJ/UieNTCcE4Cfw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        EjecutarModal = (CodAccion >= 0 && response.Id > 0) ? true : false;

        switch (CodAccion) {
            case 0:
                TituloModal = 'Formulario guardado con éxito';
                break;
            case 1:
                TituloModal = 'Se actualizó la evaluación de monitoreo con éxito';
                break;
            case 2:
                TituloModal = 'Se cerró la evaluación de monitoreo con éxito';
                break;
            case 3:
                TituloModal = 'Se anuló la tarea de monitoreo con éxito';
                break;
            case 4:
                TituloModal = 'Se programó la evaluación de monitoreo con éxito';
                break;
            default:
                break;
        }

        if (EjecutarModal) {

            hideLoading();

            Swal.fire({
                title: TituloModal,
                iconColor: "#8fbb02",
                iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                showConfirmButton: false,
                padding: "3em 3em 6em 3em ",
                timer: 1500,
            }).then(() => {
                fnVolverIndexMonitoreoEva();
            });

        }

    });

}
//#endregion

