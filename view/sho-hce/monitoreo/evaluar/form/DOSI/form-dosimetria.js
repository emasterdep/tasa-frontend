//#region Variables
var ListaEvaluacion;
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    fnOcultarMostrarMotivo();
    fnOcultarMostrarBotones();

    onInit();

    $("#sNumeroDoc").on('change', onChangeDocument);
    $("#dat_EPP").on('change', onChangeEPP);

    $("#nCalibracionInicial").on('change', onChangeCalibracion);
    $("#nCalibracionFinal").on('change', onChangeCalibracion);

});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnCargarSedesAreasGerenciasMonitoreo(),
        fnCargarListaPuestos(),
        fnCargarPlucksMonitoreo(),
        fnGetSelectsForm(IdTipoEvaluacion)
    ]);

    fnSetSelectsMonitoreo();
    fnSetSelectsForm();

    fnCargarDatosGeneral();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
    $('#divCalibracionCorrecta').hide();
    $('#divNoCalibrado').hide();
});
//#endregion


//#region CRUD: Programar | Guardar | Cerrar | Anular
function fnCRUDEvaluacionMonitoreo(CodAccion, IdEstado) {

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //Tipo de Evaluacion
    data.TipoEvaluacion = IdTipoEvaluacion;

    //CodAccion => 0: Registrar | 1: Modificar | 2:Cerrar | 3: Anular | 4: Programar 

    if (CodAccion == 3 || CodAccion == 4) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;
        data.NroDocumento = 0;
        data.Regimen = 0;
        data.ActividadesGenerales = "";
        data.TiempoAlmuerzo = 0;
        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = "1900-01-01";
        data.DescripcionRuido = "";
        data.CondicionMonitoreo = "";
        data.DescripcionZona = "";
        data.Fuente = "";
        data.Ingenieria = 0;
        data.Detallar = "";
        data.Administrativo = "";
        data.EPP = 0;
        data.MarcaControl = "";
        data.ModeloControl = "";
        data.NRREquipo = "";
        data.MarcaControl_2 = "";
        data.ModeloControl_2 = "";
        data.NRREquipo_2 = "";
        data.MarcaMedicion = "";
        data.ModeloMedicion = "";
        data.Serie = "";
        data.CalibracionInicial = 0;
        data.CalibracionFinal = 0;
        data.TiempoMedicion = 0;
        data.NivelRuido = 0;
        data.TiempoPermitido = 0;
        data.DosisProyectada = 0;
        data.Leq = 0;
        data.Tref = 0;
        data.DosisCalculada = 0;
        data.DosisLimite = 0;
        data.IntensidadAgenteGlobal = 0;
        data.NivelRiesgo = 0;
        data.CumplimientoNormativa = 0;
        data.accion = CodAccion;
        data.MotivoSeleccionado = MotivoAnulacionSeleccionado
        data.MotivoEscribir = MotivoAnulacionEscribir
    }

    if (CodAccion == 0 || CodAccion == 1 || CodAccion == 2) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;
        data.NroDocumento = $("#sNumeroDoc").val();
        data.Regimen = $("#nRegimen").val();
        data.ActividadesGenerales = $("#sActivGeneral").val();
        data.TiempoAlmuerzo = $("#nTiempoAlmuerzo").val();
        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = $("#dFechaRegistro").val();
        data.DescripcionRuido = $("#sFuente").val();
        data.CondicionMonitoreo = "";
        data.DescripcionZona = "";
        data.Fuente = "";
        data.Ingenieria = $("#dat_Ingenieria option:selected").val();
        data.Detallar = $("#sDetallar").val();
        data.Administrativo = $("#sAdministrativo").val();
        data.EPP = $("#dat_EPP option:selected").val();
        data.MarcaControl = $("#sMarcaControl").val();
        data.ModeloControl = $("#sModeloControl").val();
        data.NRREquipo = $("#sNRREquipo").val();
        data.MarcaControl_2 = $("#sMarcaControl_2").val();
        data.ModeloControl_2 = $("#sModeloControl_2").val();
        data.NRREquipo_2 = $("#sNRREquipo_2").val();
        data.MarcaMedicion = $("#sMarcaMedicion").val();
        data.ModeloMedicion = $("#sModeloMedicion").val();
        data.Serie = $("#sNumSerie").val();
        data.CalibracionInicial = $("#nCalibracionInicial").val();
        data.CalibracionFinal = $("#nCalibracionFinal").val();
        data.TiempoMedicion = $("#nTiempoMedicion").val();
        data.NivelRuido = $("#nNivelRuido").val();
        data.TiempoPermitido = $("#nTiempoPermitido").val();
        data.DosisProyectada = $("#nDosisProyectada").val();
        data.Leq = $("#nLeq").val();
        data.Tref = $("#nTref").val();
        data.DosisCalculada = $("#nDosisCalculada").val();
        data.DosisLimite = $("#nDosisLimite").val();
        data.IntensidadAgenteGlobal = $("#nIntensidadAgenteGlobal").val();
        data.NivelRiesgo = $("#dat_NivelRiesgo option:selected").val();
        data.CumplimientoNormativa = $("#dat_CumplimientoNormativa option:selected").val();
        data.accion = CodAccion;
        data.MotivoSeleccionado = 0;
        data.MotivoEscribir = '';
    }

    showLoading();

    let url = apiUrlsho + `/api/ho_Post_monitoreo_evaluar?code=FhnsxxYBMqr1RirUftBaVPsevFlBysozPo2DzN3CJ/UieNTCcE4Cfw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        hideLoading();

        if (response.Id > 0) {

            if (CodAccion == 4) {
                EjecutarModal = true;
                TituloModal = 'Se programó la evaluación de monitoreo con éxito';
            }

            if (CodAccion == 0) {
                EjecutarModal = true;
                TituloModal = 'Formulario guardado con éxito';
            }

            if (CodAccion == 1) {
                EjecutarModal = true;
                TituloModal = 'Se actualizó la evaluación de monitoreo con éxito';
            }

            if (CodAccion == 2) {
                EjecutarModal = true;
                TituloModal = 'Se cerró la evaluación de monitoreo con éxito';
            }

            if (CodAccion == 3) {
                EjecutarModal = true;
                TituloModal = 'Se anuló la tarea de monitoreo con éxito';
            }

            if (EjecutarModal) {

                Swal.fire({
                    title: TituloModal,
                    iconColor: "#8fbb02",
                    iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                    showConfirmButton: false,
                    padding: "3em 3em 6em 3em ",
                    timer: 1500,
                }).then(() => {

                    fnVolverIndexMonitoreoEva();

                });

            }

        }

    });

}
//#endregion


//#region Deshabilitar Controles (Solo Ver)
function fnFormDisabledDosi() {

    document.getElementById('fieldset1').disabled = true;
    document.getElementById('fieldset3').disabled = true;

    document.getElementById('dFechaRegistro').disabled = true;
    document.getElementById('sCodEvaluacion').disabled = true;
    document.getElementById('sCodTareaMonitoreo').disabled = true;
}
//#endregion


//#region Cargar Datos en General
async function fnCargarDatosGeneral() {
    //1: Abierto, 2:Cerrado, 3: Anulado, 4:Programado, 5:En Progreso
    if (EstadoEvaluacion == 1) {
        await fnFormDisabledDosi();
        fnCargarDatosMonitoreo();
    }
    else if (EstadoEvaluacion == 4) {
        fnCargarDatosMonitoreo();
    }
    else {
        fnCargarDatosEvaluacion();
    }

    //Solo Ver
    if (!bModoEditar || EstadoEvaluacion == 2) {
        fnFormDisabledDosi();
        document.getElementById("dFechaProgramacion").disabled = true;
    }
    //Anulado
    else if (EstadoEvaluacion == 3) {
        fnFormDisabledDosi();
        document.getElementById("dFechaProgramacion").disabled = false;
    }
}
//#endregion


//#region CHANGE Cambiar Numero de Documento
function onChangeDocument() {
    if ($(this).val() != 0) {
        fnCargarDatosPaciente($("#sNumeroDoc").val())
    }
}
//#endregion


//#region GET Cargar Datos de Evaluado
async function fnCargarDatosPaciente(NumeroDoc) {

    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar?code=954FWrzAYNSsRlSx8D6aCbN8amsaVW9OGkGvzsaPvSmz1Ce9tQCkCg==&AccionBackEnd=ListaDatosEvaluado&NumeroDoc=` + NumeroDoc;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        var DatosEvaluado = response.lista_DatosEvaluado

        fnSetDatosPaciente(DatosEvaluado);

    });

}
//#endregion


//#region SET Asignar Datos de Evaluado
function fnSetDatosPaciente(lista) {

    $('#sNombres').val(lista[0].Nombres);
    $('#sApellidos').val(lista[0].Apellidos);
    $('#dFechaNac').val(lista[0].FechaNacimiento);
    $('#nEdad').val(lista[0].Edad);
    $('#nTalla').val(lista[0].Talla);
    $('#nPeso').val(lista[0].Peso);

}
//#endregion


//#region GET Cargar Datos de Evaluacion para Editar
function fnCargarDatosEvaluacion() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_form?code=Yl68m4mOaMYua7CjjPCFOWT5E6fNotQtWa63t0GOpKiamhBNMUpfjQ==&AccionBackEnd=DatosEvaluacionDOSI&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaEvaluacion = response.lista_DatosEvaluacion

        fnSetDatosEvaluacion(ListaEvaluacion);

    });
}
//#endregion


//#region SET Asignar Datos de Evaluacion para Editar
function fnSetDatosEvaluacion(lista) {

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $("#dat_puesto option[value='" + lista[0].IdPuestoCargo + "']").prop("selected", true);
    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(lista[0].CodigoTareaMonitoreo);
    $("#dat_etapa option[value='" + lista[0].IdEtapa + "']").prop("selected", true);

    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaEjecucion != "") { $('#dFechaRegistro').val(lista[0].FechaEjecucion); }

    if (lista[0].NroDocumento > 0) {
        $('#sNumeroDoc').val(lista[0].NroDocumento);
        $('#sNombres').val(lista[0].Nombres);
        $('#sApellidos').val(lista[0].Apellidos);
        $('#dFechaNac').val(lista[0].FechaNacimiento);
        $('#nEdad').val(lista[0].Edad);
        $('#nTalla').val(lista[0].Talla);
        $('#nPeso').val(lista[0].Peso);
    }

    $('#sFuente').val(lista[0].DescripcionRuido);

    $('#nRegimen').val(lista[0].Regimen);
    $('#sActivGeneral').val(lista[0].ActividadesGenerales);
    $('#nTiempoAlmuerzo').val(lista[0].TiempoAlmuerzo);

    $("#dat_Ingenieria option[value='" + lista[0].Ingenieria + "']").prop("selected", true);
    $('#sDetallar').val(lista[0].Detallar);
    $("#dat_EPP option[value='" + lista[0].EPP + "']").prop("selected", true);
    $('#sAdministrativo').val(lista[0].Administrativo);
    $('#sMarcaControl').val(lista[0].MarcaControl);
    $('#sModeloControl').val(lista[0].ModeloControl);
    $('#sNRREquipo').val(lista[0].NRREquipo);

    if (lista[0].EPP == 3) {
        $('#divDobleProteccion').show();
        $('#sMarcaControl_2').val(lista[0].MarcaControl_2);
        $('#sModeloControl_2').val(lista[0].ModeloControl_2);
        $('#sNRREquipo_2').val(lista[0].NRREquipo_2);
    }

    $('#sMarcaMedicion').val(lista[0].MarcaMedicion);
    $('#sModeloMedicion').val(lista[0].ModeloMedicion);
    $('#sNumSerie').val(lista[0].Serie);
    $('#nCalibracionInicial').val(lista[0].CalibracionInicial);
    $('#nCalibracionFinal').val(lista[0].CalibracionFinal);

    $('#nTiempoMedicion').val(lista[0].TiempoMedicion);
    $('#nNivelRuido').val(lista[0].NivelRuido);
    $('#nTiempoPermitido').val(lista[0].TiempoPermitido);
    $('#nDosisProyectada').val(lista[0].DosisProyectada);

    $('#nLeq').val(lista[0].Leq);
    $('#nTref').val(lista[0].Tref);
    $('#nDosisCalculada').val(lista[0].DosisCalculada);

    $('#nDosisLimite').val(lista[0].DosisLimite);
    $('#nIntensidadAgenteGlobal').val(lista[0].IntensidadAgenteGlobal);
    $("#dat_NivelRiesgo option[value='" + lista[0].NivelRiesgo + "']").prop("selected", true);
    $("#dat_CumplimientoNormativa option[value='" + lista[0].CumplimientoNormativa + "']").prop("selected", true);

    if (EstadoEvaluacion == 3) {
        fnSetMotivos();
        fnSetDatosMotivosAnulacion(lista);
    }

    onChangeCalibracion();

}
//#endregion


//#region CHANGE Calibracion
function onChangeCalibracion() {

    var CalibracionInicial = $("#nCalibracionInicial").val();
    var CalibracionFinal = $("#nCalibracionFinal").val();

    if (CalibracionInicial != null && CalibracionFinal != null &&
        (CalibracionInicial == CalibracionFinal)) {
        $('#divCalibracionCorrecta').show();
        $('#divNoCalibrado').hide();
    }
    else if (CalibracionInicial != CalibracionFinal) {
        $('#divCalibracionCorrecta').hide();
        $('#divNoCalibrado').show();

    }
}
//#endregion

