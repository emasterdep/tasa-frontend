//#region Variables
var ListaEvaluacion;
var ListaTabla;
var ListaDetalleSONO = [];

var listaLugares = []
var listaTiempoMedicion = []

var listaLeq = []
var listaLeqProm = []
var listaTrefHoras = []
var listaTref = []
var listaLmax = []
var listaLpico = []

var listaCumplimientoNormativa = []
var listaEstado = []
var listaMotivo = []
//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    fnOcultarMostrarMotivo();
    fnOcultarMostrarBotones();

    onInit();

    $("#dat_EPP").on('change', onChangeEPP);


});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnCargarSedesAreasGerenciasMonitoreo(),
        fnCargarListaPuestos(),
        fnCargarPlucksMonitoreo(),
        fnGetSelectsForm(IdTipoEvaluacion)
    ]);

    fnSetSelectsMonitoreo();
    fnSetSelectsForm();

    fnCargarDatosGeneral();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Limpiar Listas
function fnLimpiarListas() {
    this.listaLugares = []
    this.listaTiempoMedicion = []
    this.listaLeq = []
    this.listaLeqProm = []
    this.listaTrefHoras = []
    this.listaTref = []
    this.listaLmax = []
    this.listaLpico = []
    this.listaCumplimientoNormativa = []
    this.listaEstado = []
    this.listaMotivo = []
}
//#endregion


//#region Cargar Datos en General
async function fnCargarDatosGeneral() {
    //1: Abierto, 2:Cerrado, 3: Anulado, 4:Programado, 5:En Progreso
    if (EstadoEvaluacion == 1) {
        fnFormDisabled();
        fnCargarDatosMonitoreo();
    }
    else if (EstadoEvaluacion == 4) {
        fnCargarDatosMonitoreo();
    }
    else {
        fnCargarDatosEvaluacion();
        fnCargarLineasDetalle();
    }

    //Solo Ver
    if (!bModoEditar || EstadoEvaluacion == 2) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = true;
    }
    //Anulado
    else if (EstadoEvaluacion == 3) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = false;
    }
}
//#endregion


//#region GET Cargar Datos de Evaluacion para Editar
function fnCargarDatosEvaluacion() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_form?code=Yl68m4mOaMYua7CjjPCFOWT5E6fNotQtWa63t0GOpKiamhBNMUpfjQ==&AccionBackEnd=DatosEvaluacionSONO&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaEvaluacion = response.lista_DatosEvaluacion

        fnSetDatosEvaluacion(ListaEvaluacion);

    });
}
//#endregion


//#region SET Asignar Datos de Evaluacion para Editar
function fnSetDatosEvaluacion(lista) {

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_zona option[value='" + lista[0].IdZona + "']").prop("selected", true);
    $("#dat_etapa option[value='" + lista[0].IdEtapa + "']").prop("selected", true);

    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(lista[0].CodigoTareaMonitoreo);

    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaEjecucion != "") { $('#dFechaRegistro').val(lista[0].FechaEjecucion); }

    $("#dat_Ingenieria option[value='" + lista[0].Ingenieria + "']").prop("selected", true);
    $('#sDetallar').val(lista[0].Detallar);
    $("#dat_EPP option[value='" + lista[0].EPP + "']").prop("selected", true);
    $('#sAdministrativo').val(lista[0].Administrativo);
    $('#sMarcaControl').val(lista[0].MarcaControl);
    $('#sModeloControl').val(lista[0].ModeloControl);
    $('#sNRREquipo').val(lista[0].NRREquipo);

    if (lista[0].EPP == 3) {
        $('#divDobleProteccion').show();
        $('#sMarcaControl_2').val(lista[0].MarcaControl_2);
        $('#sModeloControl_2').val(lista[0].ModeloControl_2);
        $('#sNRREquipo_2').val(lista[0].NRREquipo_2);
    }

    $('#sMarcaMedicion').val(lista[0].MarcaMedicion);
    $('#sModeloMedicion').val(lista[0].ModeloMedicion);
    $('#sNumSerie').val(lista[0].Serie);

    if (EstadoEvaluacion == 3) {
        fnSetMotivos();
        fnSetDatosMotivosAnulacion(lista);
    }
}
//#endregion


//#region CRUD: Programar | Guardar | Cerrar | Anular
function fnCRUDEvaluacionMonitoreo(CodAccion, IdEstado) {

    if (!fnValidarLineas()) {
        return;
    }
    else {
        fnConfirmarLineas()
    }

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //Tipo de Evaluacion
    data.TipoEvaluacion = IdTipoEvaluacion;

    //CodAccion => 0: Registrar | 1: Modificar | 2:Cerrar | 3: Anular | 4: Programar 
    if (CodAccion == 3 || CodAccion == 4) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = "1900-01-01";

        data.Ingenieria = 0;
        data.Detallar = "";
        data.Administrativo = "";
        data.EPP = 0;
        data.MarcaControl = "";
        data.ModeloControl = "";
        data.NRREquipo = "";
        data.MarcaControl_2 = "";
        data.ModeloControl_2 = "";
        data.NRREquipo_2 = "";

        data.MarcaMedicion = "";
        data.ModeloMedicion = "";
        data.Serie = "";

        data.listaLugares = ""
        data.listaTiempoMedicion = ""
        data.listaLeq = ""
        data.listaLeqProm = ""
        data.listaTrefHoras = ""
        data.listaTref = ""
        data.listaLmax = ""
        data.listaLpico = ""
        data.listaCumplimientoNormativa = ""
        data.listaEstado = ""
        data.listaMotivo = ""

        data.MotivoSeleccionado = MotivoAnulacionSeleccionado;
        data.MotivoEscribir = MotivoAnulacionEscribir

        data.accion = CodAccion;

    }

    if (CodAccion == 0 || CodAccion == 1 || CodAccion == 2) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = $("#dFechaRegistro").val();

        data.Ingenieria = $("#dat_Ingenieria option:selected").val();
        data.Detallar = $("#sDetallar").val();
        data.Administrativo = $("#sAdministrativo").val();
        data.EPP = $("#dat_EPP option:selected").val();
        data.MarcaControl = $("#sMarcaControl").val();
        data.ModeloControl = $("#sModeloControl").val();
        data.NRREquipo = $("#sNRREquipo").val();
        data.MarcaControl_2 = $("#sMarcaControl_2").val();
        data.ModeloControl_2 = $("#sModeloControl_2").val();
        data.NRREquipo_2 = $("#sNRREquipo_2").val();

        data.MarcaMedicion = $("#sMarcaMedicion").val();
        data.ModeloMedicion = $("#sModeloMedicion").val();
        data.Serie = $("#sNumSerie").val();

        data.listaLugares = listaLugares.join();
        data.listaTiempoMedicion = listaTiempoMedicion.join();
        data.listaLeq = listaLeq.join('|');
        data.listaLeqProm = listaLeqProm.join('|');
        data.listaTrefHoras = listaTrefHoras.join('|');
        data.listaTref = listaTref.join('|');
        data.listaLmax = listaLmax.join('|');
        data.listaLpico = listaLpico.join('|');
        data.listaCumplimientoNormativa = listaCumplimientoNormativa.join();
        data.listaEstado = listaEstado.join();
        data.listaMotivo = listaMotivo.join();

        data.MotivoSeleccionado = 0;
        data.MotivoEscribir = "";

        data.accion = CodAccion;
    }


    showLoading();

    let url = apiUrlsho + `/api/ho_Post_monitoreo_evaluar?code=FhnsxxYBMqr1RirUftBaVPsevFlBysozPo2DzN3CJ/UieNTCcE4Cfw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        hideLoading();

        EjecutarModal = (CodAccion >= 0 && response.Id > 0) ? true : false;

        switch (CodAccion) {
            case 0:
                TituloModal = 'Formulario guardado con éxito';
                break;
            case 1:
                TituloModal = 'Se actualizó la evaluación de monitoreo con éxito';
                break;
            case 2:
                TituloModal = 'Se cerró la evaluación de monitoreo con éxito';
                break;
            case 3:
                TituloModal = 'Se anuló la tarea de monitoreo con éxito';
                break;
            case 4:
                TituloModal = 'Se programó la evaluación de monitoreo con éxito';
                break;
            default:
                break;
        }


        if (EjecutarModal) {
            Swal.fire({
                title: TituloModal,
                iconColor: "#8fbb02",
                iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                showConfirmButton: false,
                padding: "3em 3em 6em 3em ",
                timer: 1500,
            }).then(() => {
                fnVolverIndexMonitoreoEva();
            });
        }
        else {
            fnLimpiarListas();
        }

    });


}
//#endregion


//#region AGREGAR a Lista Detalle
function fnAgregarDetalle() {
    this.ListaDetalleSONO.push
        ({
            nNro: this.ListaDetalleSONO.length + 1,
            sNombreLugar: $("#sLugar").val(),
            nTiempoMedicion: "",
            nLeq: "",
            nLeqProm: "",
            nTrefHoras: "",
            nTref: "",
            nLmax: "",
            nLpico: "",
            nCumplimientoNormativa: "",
            nIdEstado: 1,
            sEstado: "Abierto",
            sMotivo: "",
            bActivo: 0
        });

}
//#endregion


//#region AGREGAR Fila
function fnAgregarFila() {

    let sLugar = $("#sLugar").val();
    //Validar
    if (sLugar == "" || sLugar == undefined || sLugar == null) {
        return Swal.fire(
            'Advertencia',
            'Debe colocar el nombre del lugar antes de agregar a lista.',
            'warning'
        )
    }

    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();

    var table = $('#table_form_sono').DataTable({
        paging: false,
        ordering: false,
        bInfo: false
    });;

    fnAgregarDetalle();

    var nFilas = this.ListaDetalleSONO.length // Tamaño de Filas Totales
    var n = this.ListaDetalleSONO.length - 1//Fila Actual
    var lista = ListaDetalleSONO[n]//Objeto Actual

    opcCumplNorm = OpcionesCumplimientoNormativa()
    html_tbody = OpcionesTable(n);


    table.row.add([
        `<label id="nNro_${n}">         ${nFilas}   </label>`,
        `<label id="sNombreLugar_${n}"> ${sLugar}   </label>`,
        `<input type="number" id="nTiempoMedicion_${n}" />`,

        `<input type="number" id="nLeq_${n}" step="0.01"/>`,
        `<input type="number" id="nLeqProm_${n}" step="0.01"/>`,
        `<input type="number" id="nTrefHoras_${n}" step="0.01"/>`,
        `<input type="number" id="nTref_${n}" step="0.01"/>`,
        `<input type="number" id="nLmax_${n}" step="0.01" />`,
        `<input type="number" id="nLpico_${n}" step="0.01"/>`,

        `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcCumplNorm}</select></div>`,
        `<label id="sEstado_${n}"/>${lista.sEstado}</label>`,
        `<input type="text" id="sMotivo_${n}" value="Sin Motivo" readOnly/>`,
        html_tbody,

    ]).draw(false);

    document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";

    $('#sLugar').val("");

    $('#table_form_sono-count-row').text(table.rows().count());
}
//#endregion


//#region Confirmar Lineas Detalles
function fnConfirmarLineas() {

    for (let i = 0; i < this.ListaDetalleSONO.length; i++) {
        this.listaLugares.push(this.ListaDetalleSONO[i].sNombreLugar)
        this.listaTiempoMedicion.push(this.ListaDetalleSONO[i].nTiempoMedicion)

        this.listaLeq.push(this.ListaDetalleSONO[i].nLeq)
        this.listaLeqProm.push(this.ListaDetalleSONO[i].nLeqProm)
        this.listaTrefHoras.push(this.ListaDetalleSONO[i].nTrefHoras)
        this.listaTref.push(this.ListaDetalleSONO[i].nTref)
        this.listaLmax.push(this.ListaDetalleSONO[i].nLmax)
        this.listaLpico.push(this.ListaDetalleSONO[i].nLpico)

        this.listaCumplimientoNormativa.push(this.ListaDetalleSONO[i].nCumplimientoNormativa)
        this.listaEstado.push(this.ListaDetalleSONO[i].nIdEstado)
        this.listaMotivo.push(this.ListaDetalleSONO[i].sMotivo)
    }
}
//#endregion


//#region Validar Lineas Detalles
function fnValidarLineas() {

    var bValido = true;

    for (let n = 0; n < this.ListaDetalleSONO.length; n++) {

        ListaDetalleSONO[n].nTiempoMedicion = $(`#nTiempoMedicion_${n}`).val();

        ListaDetalleSONO[n].nLeq = $(`#nLeq_${n}`).val();
        ListaDetalleSONO[n].nLeqProm = $(`#nLeqProm_${n}`).val();
        ListaDetalleSONO[n].nTrefHoras = $(`#nTrefHoras_${n}`).val();
        ListaDetalleSONO[n].nTref = $(`#nTref_${n}`).val();
        ListaDetalleSONO[n].nLmax = $(`#nLmax_${n}`).val();
        ListaDetalleSONO[n].nLpico = $(`#nLpico_${n}`).val();

        ListaDetalleSONO[n].nCumplimientoNormativa = $(`#nCumplimientoNormativa_${n} option:selected`).val();
        ListaDetalleSONO[n].sMotivo = $(`#sMotivo_${n}`).val();

        if (
            ListaDetalleSONO[n].sNombreLugar == "" ||
            ListaDetalleSONO[n].nTiempoMedicion == "" ||

            ListaDetalleSONO[n].nLeq == "" ||
            ListaDetalleSONO[n].nLeqProm == "" ||
            ListaDetalleSONO[n].nTrefHoras == "" ||
            ListaDetalleSONO[n].nTref == "" ||
            ListaDetalleSONO[n].nLmax == "" ||
            ListaDetalleSONO[n].nLpico == "" ||

            ListaDetalleSONO[n].nCumplimientoNormativa == "" ||
            ListaDetalleSONO[n].nCumplimientoPorc == "" ||
            ListaDetalleSONO[n].nIdEstado == 0
        ) {
            bValido = false
            Swal.fire(
                'Advertencia',
                `Debe completar los datos del lugar Nro. ${n + 1} de la tabla de Datos de monitoreo.`,
                'warning'
            )
        }

        //Filas Anuladas
        if (ListaDetalleSONO[n].nIdEstado == 3 && ListaDetalleSONO[n].sMotivo == "") {
            bValido = false
            Swal.fire(
                'Advertencia',
                `El Lugar Nro. ${n + 1} tiene estado Anulado, debe completar el Motivo.`,
                'warning'
            )
        }

    }

    return bValido

}
//#endregion


//#region GET Cargar Lineas Detalles
function fnCargarLineasDetalle() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_table?code=60Gs5EVzToGI4HE97MLaoGyLDDlHeh3qxh3Y0PJl95KECrDYlsrF9g==&AccionBackEnd=TablaEvaluacionSONO&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaTabla = response.lista_DatosEvaluacion

        if (ListaTabla.length > 0) {
            fnSetLineasDetalle(ListaTabla);
        }

    });
}
//#endregion


//#region SET Asignar Lineas Detalles 
function fnSetLineasDetalle(lista) {
    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();

    var table = $('#table_form_sono').DataTable({
        paging: false,
        ordering: false,
        bInfo: false
    });;

    opcCumplNorm = OpcionesCumplimientoNormativa();

    for (let n = 0; n < lista.length; n++) {

        this.ListaDetalleSONO.push
            ({
                nNro: n + 1,
                sNombreLugar: lista[n].Lugar,
                nTiempoMedicion: lista[n].TiempoMedicion,
                nLeq: lista[n].Leq,
                nLeqProm: lista[n].LeqProm,
                nTrefHoras: lista[n].TrefHoras,
                nTref: lista[n].Tref,
                nLmax: lista[n].Lmax,
                nLpico: lista[n].Lpico,
                nCumplimientoNormativa: lista[n].CumplimientoNormativa,
                nIdEstado: lista[n].Estado,
                sEstado: lista[n].NombreEstado,
                sMotivo: lista[n].Motivo
            });

        html_tbody = OpcionesTable(n);

        table.row.add([
            `<label id="nNro_${n}">         ${n + 1}   </label>`,
            `<label id="sNombreLugar_${n}"> ${lista[n].Lugar}   </label>`,
            `<input type="number" id="nTiempoMedicion_${n}" value="${lista[n].TiempoMedicion}" />`,

            `<input type="number" id="nLeq_${n}" value="${lista[n].Leq}" step="0.01"/>`,
            `<input type="number" id="nLeqProm_${n}" value="${lista[n].LeqProm}" step="0.01"/>`,
            `<input type="number" id="nTrefHoras_${n}" value="${lista[n].TrefHoras}" step="0.01"/>`,
            `<input type="number" id="nTref_${n}" value="${lista[n].Tref}" step="0.01"/>`,
            `<input type="number" id="nLmax_${n}" value="${lista[n].Lmax}" step="0.01"/>`,
            `<input type="number" id="nLpico_${n}" value="${lista[n].Lpico}" step="0.01"/>`,

            `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcCumplNorm}</select></div>`,
            `<label id="sEstado_${n}"/>${lista[n].NombreEstado}</label>`,
            `<input type="text" id="sMotivo_${n}" value="${lista[n].Motivo}" readOnly/>`,
            html_tbody,

        ]).draw(false);

        $(`#nCumplimientoNormativa_${n} option[value='` + lista[n].CumplimientoNormativa + "']").prop("selected", true);

        if (lista[n].Estado == 1) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
        }
        else if (lista[n].Estado == 2) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";
        }
        else if (lista[n].Estado == 3) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";
            document.getElementById(`sMotivo_${n}`).readOnly = false;
        }
    }

    $('#table_form_sono-count-row').text(table.rows().count());

}
//#endregion


//#region CERRAR Fila
function fnCerrarFila(n) {
    ListaDetalleSONO[n].nIdEstado = 2
    ListaDetalleSONO[n].sEstado = "Cerrado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Cerrado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = true;
    //document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion


//#region ANULAR Fila
function fnAnularFila(n) {

    ListaDetalleSONO[n].nIdEstado = 3
    ListaDetalleSONO[n].sEstado = "Anulado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Anulado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion

