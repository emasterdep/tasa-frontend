//#region Variables
var ListaEvaluacion;

var ListaTabla;
var ListaDetalleMatPart = [];

var listaCodigoFiltro = []
var listaHoraInicio = []
var listaHoraFin = []
var listaTiempoFiltro = []
var listaPesajeInicial = []
var listaPesajeFinal = []
var listaPesoMuestra = []
var listaVolumen = []
var listaConcentracionFiltro = []
var listaEstado = []
var listaMotivo = []

//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    fnOcultarMostrarMotivo();
    fnOcultarMostrarBotones();

    onInit();

    $("#sNumeroDoc").on('change', onChangeNroDocumento);
    $("#dat_EPP").on('change', onChangeEPP);


});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnCargarSedesAreasGerenciasMonitoreo(),
        fnCargarListaPuestos(),
        fnCargarPlucksMonitoreo(),
        fnGetSelectsForm(IdTipoEvaluacion)
    ]);

    fnSetSelectsMonitoreo();
    fnSetSelectsForm();

    fnCargarDatosGeneral();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Cargar Datos en General
async function fnCargarDatosGeneral() {
    //1: Abierto, 2:Cerrado, 3: Anulado, 4:Programado, 5:En Progreso
    if (EstadoEvaluacion == 1) {
        fnFormDisabled();
        fnCargarDatosMonitoreo();
    }
    else if (EstadoEvaluacion == 4) {
        fnCargarDatosMonitoreo();
    }
    else {
        fnCargarDatosEvaluacion();
        fnCargarLineasDetalle();
    }

    //Solo Ver
    if (!bModoEditar || EstadoEvaluacion == 2) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = true;
    }
    //Anulado
    else if (EstadoEvaluacion == 3) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = false;
    }
}
//#endregion


//#region GET Cargar Datos de Evaluacion para Editar
function fnCargarDatosEvaluacion() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_form?code=Yl68m4mOaMYua7CjjPCFOWT5E6fNotQtWa63t0GOpKiamhBNMUpfjQ==&AccionBackEnd=DatosEvaluacionMaterialParticulado&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaEvaluacion = response.lista_DatosEvaluacion

        fnSetDatosEvaluacion(ListaEvaluacion);

    });
}
//#endregion


//#region SET Asignar Datos de Evaluacion para Editar
function fnSetDatosEvaluacion(lista) {

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $("#dat_puesto option[value='" + lista[0].IdPuestoCargo + "']").prop("selected", true);
    $("#dat_zona option[value='" + lista[0].IdZona + "']").prop("selected", true);
    $("#dat_etapa option[value='" + lista[0].IdEtapa + "']").prop("selected", true);

    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(lista[0].CodigoTareaMonitoreo);

    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaEjecucion != "") { $('#dFechaRegistro').val(lista[0].FechaEjecucion); }

    $('#sDescripcionZona').val(lista[0].DescripcionZona);
    $('#sFuente').val(lista[0].Fuente);

    $('#sNumeroDoc').val(lista[0].NroDocumento);
    $('#sNombres').val(lista[0].Nombres);
    $('#sApellidos').val(lista[0].Apellidos);
    $('#nRegimen').val(lista[0].Regimen);
    $('#nTiempoPuesto').val(lista[0].TiempoPuesto);
    $('#nTiempoAlmuerzo').val(lista[0].TiempoAlmuerzo);
    $('#sActivGeneral').val(lista[0].ActividadesGenerales);

    $("#dat_Ingenieria option[value='" + lista[0].Ingenieria + "']").prop("selected", true);
    $('#sDetallar').val(lista[0].Detallar);
    $("#dat_EPP option[value='" + lista[0].EPP + "']").prop("selected", true);
    $('#sAdministrativo').val(lista[0].Administrativo);
    $('#sMarcaControl').val(lista[0].MarcaControl);
    $('#sModeloControl').val(lista[0].ModeloControl);
    //$('#sNRREquipo').val(lista[0].NRREquipo);

    if (lista[0].EPP == 3) {
        $('#divDobleProteccion').show();
        $('#sMarcaControl_2').val(lista[0].MarcaControl_2);
        $('#sModeloControl_2').val(lista[0].ModeloControl_2);
        //$('#sNRREquipo_2').val(lista[0].NRREquipo_2);
    }

    $('#sMarcaMedicion').val(lista[0].MarcaMedicion);
    $('#sModeloMedicion').val(lista[0].ModeloMedicion);
    $('#sNumSerie').val(lista[0].Serie);

    $('#sDescripcionExposicion').val(lista[0].DescripcionExposicion);
    $('#nConcentracion').val(lista[0].ConcentracionPuesto);

    $('#nLMP').val(lista[0].LMP);
    $('#nLMPCorregido').val(lista[0].LMPCorregido);
    $('#nNivelAccion').val(lista[0].NivelAccion);
    $('#nIndiceExposicion').val(lista[0].IndiceExposicion);
    $("#dat_NivelRiesgo option[value='" + lista[0].NivelRiesgo + "']").prop("selected", true);
    $("#dat_CumplimientoNormativa option[value='" + lista[0].CumplimientoNormativa + "']").prop("selected", true);

    if (EstadoEvaluacion == 3) {
        fnSetMotivos();
        fnSetDatosMotivosAnulacion(lista);
    }
}
//#endregion


//#region CRUD: Programar | Guardar | Cerrar | Anular
function fnCRUDEvaluacionMonitoreo(CodAccion, IdEstado) {

    if (!fnValidarLineas()) {
        return;
    }
    else {
        fnConfirmarLineas()
    }

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //Tipo de Evaluacion
    data.TipoEvaluacion = IdTipoEvaluacion;

    //CodAccion => 0: Registrar | 1: Modificar | 2:Cerrar | 3: Anular | 4: Programar 
    if (CodAccion == 3 || CodAccion == 4) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = "1900-01-01";

        data.DescripcionZona = "";
        data.Fuente = "";

        data.NroDocumento = 0;
        data.Regimen = 0;
        data.TiempoAlmuerzo = 0;
        data.ActividadesGenerales = "";

        data.Ingenieria = 0;
        data.Detallar = "";
        data.Administrativo = "";
        data.EPP = 0;
        data.MarcaControl = "";
        data.ModeloControl = "";
        data.NRREquipo = "";
        data.MarcaControl_2 = "";
        data.ModeloControl_2 = "";
        data.NRREquipo_2 = "";

        data.MarcaMedicion = "";
        data.ModeloMedicion = "";
        data.Serie = "";

        data.DescripcionExposicion = "";
        data.ConcentracionPuesto = 0;

        data.LMP = 0;
        data.LMPCorregido = 0;
        data.NivelAccion = 0;
        data.IndiceExposicion = 0;
        data.NivelRiesgo = 0;
        data.CumplimientoNormativa = 0;

        data.listaCodigoFiltro = ""
        data.listaHoraInicio = ""
        data.listaHoraFin = ""
        data.listaTiempoFiltro = ""
        data.listaPesajeInicial = ""
        data.listaPesajeFinal = ""
        data.listaPesoMuestra = ""
        data.listaVolumen = ""
        data.listaConcentracionFiltro = ""
        data.listaEstado = ""
        data.listaMotivo = ""

        data.MotivoSeleccionado = MotivoAnulacionSeleccionado;
        data.MotivoEscribir = MotivoAnulacionEscribir

        data.accion = CodAccion;

    }

    if (CodAccion == 0 || CodAccion == 1 || CodAccion == 2) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = $("#dFechaRegistro").val();

        data.DescripcionZona = $("#sDescripcionZona").val();
        data.Fuente = $("#sFuente").val();

        data.NroDocumento = $("#sNumeroDoc").val();
        data.Regimen = $("#nRegimen").val();
        data.TiempoAlmuerzo = $("#nTiempoAlmuerzo").val();
        data.ActividadesGenerales = $("#sActivGeneral").val();

        data.Ingenieria = $("#dat_Ingenieria option:selected").val();
        data.Detallar = $("#sDetallar").val();
        data.Administrativo = $("#sAdministrativo").val();
        data.EPP = $("#dat_EPP option:selected").val();
        data.MarcaControl = $("#sMarcaControl").val();
        data.ModeloControl = $("#sModeloControl").val();
        data.NRREquipo = "";// $("#sNRREquipo").val();
        data.MarcaControl_2 = $("#sMarcaControl_2").val();
        data.ModeloControl_2 = $("#sModeloControl_2").val();
        data.NRREquipo_2 = "";

        data.MarcaMedicion = $("#sMarcaMedicion").val();
        data.ModeloMedicion = $("#sModeloMedicion").val();
        data.Serie = $("#sNumSerie").val();

        data.DescripcionExposicion = $("#sDescripcionExposicion").val();
        data.ConcentracionPuesto = $("#nConcentracion").val();

        data.LMP = $("#nLMP").val();
        data.LMPCorregido = $("#nLMPCorregido").val();
        data.NivelAccion = $("#nNivelAccion").val();
        data.IndiceExposicion = $("#nIndiceExposicion").val();
        data.NivelRiesgo = $("#dat_NivelRiesgo option:selected").val();
        data.CumplimientoNormativa = $("#dat_CumplimientoNormativa option:selected").val();

        data.listaCodigoFiltro = listaCodigoFiltro.join();
        data.listaHoraInicio = listaHoraInicio.join();
        data.listaHoraFin = listaHoraFin.join();
        data.listaTiempoFiltro = listaTiempoFiltro.join("|");
        data.listaPesajeInicial = listaPesajeInicial.join("|");
        data.listaPesajeFinal = listaPesajeFinal.join("|");
        data.listaPesoMuestra = listaPesoMuestra.join("|");
        data.listaVolumen = listaVolumen.join("|"),
            data.listaConcentracionFiltro = listaConcentracionFiltro.join("|");
        data.listaEstado = listaEstado.join();
        data.listaMotivo = listaMotivo.join();

        data.MotivoSeleccionado = 0;
        data.MotivoEscribir = "";

        data.accion = CodAccion;
    }

    showLoading();

    let url = apiUrlsho + `/api/ho_Post_monitoreo_evaluar?code=FhnsxxYBMqr1RirUftBaVPsevFlBysozPo2DzN3CJ/UieNTCcE4Cfw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    hideLoading();

    return $.ajax(settings).done((response) => {

        EjecutarModal = (CodAccion >= 0 && response.Id > 0) ? true : false;

        switch (CodAccion) {
            case 0:
                TituloModal = 'Formulario guardado con éxito';
                break;
            case 1:
                TituloModal = 'Se actualizó la evaluación de monitoreo con éxito';
                break;
            case 2:
                TituloModal = 'Se cerró la evaluación de monitoreo con éxito';
                break;
            case 3:
                TituloModal = 'Se anuló la tarea de monitoreo con éxito';
                break;
            case 4:
                TituloModal = 'Se programó la evaluación de monitoreo con éxito';
                break;
            default:
                break;
        }


        if (EjecutarModal) {

            Swal.fire({
                title: TituloModal,
                iconColor: "#8fbb02",
                iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                showConfirmButton: false,
                padding: "3em 3em 6em 3em ",
                timer: 1500,
            }).then(() => {
                fnVolverIndexMonitoreoEva();
            });

        }

    });

}
//#endregion


//#region AGREGAR a Lista Detalle
function fnAgregarDetalle() {
    this.ListaDetalleMatPart.push
        ({
            nNro: this.ListaDetalleMatPart.length + 1,
            sNombreCodigoFiltro: $("#sCodigoFiltro").val(),
            bFiltroBlanco: 0,
            HoraInicio: "",
            HoraFin: "",
            nTiempoFiltro: "",
            nPesajeInicial: "",
            nPesajeFinal: "",
            nPesoMuestra: "",
            nVolumen: "",
            nConcentracionFiltro: "",
            nIdEstado: 1,
            sEstado: "Abierto",
            sMotivo: ""
        });

}
//#endregion


//#region AGREGAR Fila
function fnAgregarFila() {

    let sCodigoFiltro = $("#sCodigoFiltro").val();
    //Validar
    /*   if (sCodigoFiltro == "" || sCodigoFiltro == undefined || sCodigoFiltro == null) {
          return Swal.fire(
              'Advertencia',
              'Debe colocar el nombre del lugar antes de agregar a lista.',
              'warning'
          )
      } */

    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();

    var table = $('#table_form_mat_part').DataTable();

    fnAgregarDetalle();

    var nFilas = this.ListaDetalleMatPart.length // Tamaño de Filas Totales
    var n = this.ListaDetalleMatPart.length - 1//Fila Actual
    var lista = ListaDetalleMatPart[n]//Objeto Actual

    html_tbody = OpcionesTable(n);


    table.row.add([
        `<label id="nNro_${n}">         ${nFilas}   </label>`,
        `<label id="sNombreCodigoFiltro_${n}"> ${sCodigoFiltro}   </label>`,
        `<input type="checkbox" id="bFiltroBlanco_${n}" />`,

        `<input type="time" id="HoraInicio_${n}" />`,
        `<input type="time" id="HoraFin_${n}" />`,
        `<input type="number" id="nTiempoFiltro_${n}" step="0.01"/>`,
        `<input type="number" id="nPesajeInicial_${n}" step="0.01"/>`,
        `<input type="number" id="nPesajeFinal_${n}" step="0.01" />`,
        `<input type="number" id="nPesoMuestra_${n}" step="0.01" />`,
        `<input type="number" id="nVolumen_${n}" step="0.01"/>`,
        `<input type="number" id="nConcentracionFiltro_${n}" step="0.01"/>`,

        `<label id="sEstado_${n}"/>${lista.sEstado}</label>`,
        `<input type="text" id="sMotivo_${n}" value="Sin Motivo"/>`,
        html_tbody,

    ]).draw(false);

    document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
    document.getElementById(`sMotivo_${n}`).readOnly = true;

    $('#sCodigoFiltro').val("");

    $('#table_form_mat_part-count-row').text(table.rows().count());
}
//#endregion


//#region Confirmar Lineas Detalles
function fnConfirmarLineas() {

    for (let i = 0; i < this.ListaDetalleMatPart.length; i++) {
        this.listaCodigoFiltro.push(this.ListaDetalleMatPart[i].sNombreCodigoFiltro)

        this.listaHoraInicio.push(this.ListaDetalleMatPart[i].HoraInicio)
        this.listaHoraFin.push(this.ListaDetalleMatPart[i].HoraFin)
        this.listaTiempoFiltro.push(this.ListaDetalleMatPart[i].nTiempoFiltro)
        this.listaPesajeInicial.push(this.ListaDetalleMatPart[i].nPesajeInicial)
        this.listaPesajeFinal.push(this.ListaDetalleMatPart[i].nPesajeFinal)
        this.listaPesoMuestra.push(this.ListaDetalleMatPart[i].nPesoMuestra)
        this.listaVolumen.push(this.ListaDetalleMatPart[i].nVolumen)
        this.listaConcentracionFiltro.push(this.ListaDetalleMatPart[i].nConcentracionFiltro)

        this.listaEstado.push(this.ListaDetalleMatPart[i].nIdEstado)
        this.listaMotivo.push(this.ListaDetalleMatPart[i].sMotivo)
    }
}
//#endregion


//#region Validar Lineas Detalles
function fnValidarLineas() {

    var bValido = true;

    for (let n = 0; n < this.ListaDetalleMatPart.length; n++) {

        ListaDetalleMatPart[n].HoraInicio = $(`#HoraInicio_${n}`).val();
        ListaDetalleMatPart[n].HoraFin = $(`#HoraFin_${n}`).val();
        ListaDetalleMatPart[n].nTiempoFiltro = $(`#nTiempoFiltro_${n}`).val();
        ListaDetalleMatPart[n].nPesajeInicial = $(`#nPesajeInicial_${n}`).val();
        ListaDetalleMatPart[n].nPesajeFinal = $(`#nPesajeFinal_${n}`).val();
        ListaDetalleMatPart[n].nPesoMuestra = $(`#nPesoMuestra_${n}`).val();
        ListaDetalleMatPart[n].nVolumen = $(`#nVolumen_${n}`).val();
        ListaDetalleMatPart[n].nConcentracionFiltro = $(`#nConcentracionFiltro_${n}`).val();

        ListaDetalleMatPart[n].sMotivo = $(`#sMotivo_${n}`).val();

        if (

            ListaDetalleMatPart[n].HoraInicio == "" ||
            ListaDetalleMatPart[n].HoraFin == "" ||
            ListaDetalleMatPart[n].nTiempoFiltro == "" ||
            ListaDetalleMatPart[n].nPesajeInicial == "" ||
            ListaDetalleMatPart[n].nPesajeFinal == "" ||
            ListaDetalleMatPart[n].nPesoMuestra == "" ||

            ListaDetalleMatPart[n].nVolumen == "" ||
            ListaDetalleMatPart[n].nConcentracionFiltro == "" ||

            ListaDetalleMatPart[n].nCumplimientoPorc == "" ||
            ListaDetalleMatPart[n].nIdEstado == 0
        ) {
            bValido = false
            Swal.fire(
                'Advertencia',
                `Debe completar los datos del lugar Nro. ${n + 1} de la tabla de Datos de monitoreo.`,
                'warning'
            )
        }

        //Filas Anuladas
        if (ListaDetalleMatPart[n].nIdEstado == 3 && ListaDetalleMatPart[n].sMotivo == "") {
            bValido = false
            Swal.fire(
                'Advertencia',
                `El CodigoFiltro Nro. ${n + 1} tiene estado Anulado, debe completar el Motivo.`,
                'warning'
            )
        }

    }

    return bValido

}
//#endregion


//#region GET Cargar Lineas Detalles
function fnCargarLineasDetalle() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_table?code=60Gs5EVzToGI4HE97MLaoGyLDDlHeh3qxh3Y0PJl95KECrDYlsrF9g==&AccionBackEnd=TablaEvaluacionMaterialParticulado&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaTabla = response.lista_DatosEvaluacion

        if (ListaTabla.length > 0) {
            fnSetLineasDetalle(ListaTabla);
        }

    });
}
//#endregion


//#region SET Asignar Lineas Detalles 
function fnSetLineasDetalle(lista) {
    //Limpiar Tabla
    $.fn.dataTable.ext.search.pop();

    var table = $('#table_form_mat_part').DataTable({
        paging: false,
        ordering: false,
        bInfo: false
    });;

    for (let n = 0; n < lista.length; n++) {

        this.ListaDetalleMatPart.push
            ({
                nNro: n + 1,
                sNombreCodigoFiltro: lista[n].CodigoFiltro,
                HoraInicio: lista[n].HoraInicio,
                HoraFin: lista[n].HoraFin,

                nTiempoFiltro: lista[n].TiempoFiltro,
                nPesajeInicial: lista[n].PesajeInicial,
                nPesajeFinal: lista[n].PesajeFinal,
                nPesoMuestra: lista[n].PesoMuestra,
                nVolumen: lista[n].Volumen,
                nConcentracionFiltro: lista[n].ConcentracionFiltro,

                nIdEstado: lista[n].Estado,
                sEstado: lista[n].NombreEstado,
                sMotivo: lista[n].Motivo
            });

        html_tbody = OpcionesTable(n);

        table.row.add([
            `<label id="nNro_${n}">         ${n + 1}   </label>`,
            `<label id="sNombreCodigoFiltro_${n}"> ${lista[n].CodigoFiltro}   </label>`,
            `<input type="checkbox" id="bFiltroBlanco_${n}" />`,

            `<input type="time" id="HoraInicio_${n}" value="${lista[n].HoraInicio}" />`,
            `<input type="time" id="HoraFin_${n}" value="${lista[n].HoraFin}" />`,

            `<input type="number" id="nTiempoFiltro_${n}" value="${lista[n].TiempoFiltro}" step="0.01"/>`,
            `<input type="number" id="nPesajeInicial_${n}" value="${lista[n].PesajeInicial}" step="0.01"/>`,
            `<input type="number" id="nPesajeFinal_${n}" value="${lista[n].PesajeFinal}" step="0.01"/>`,
            `<input type="number" id="nPesoMuestra_${n}" value="${lista[n].PesoMuestra}" step="0.01"/>`,
            `<input type="number" id="nVolumen_${n}" value="${lista[n].Volumen}" step="0.01"/>`,
            `<input type="number" id="nConcentracionFiltro_${n}" value="${lista[n].ConcentracionFiltro}" step="0.01"/>`,

            `<label id="sEstado_${n}"/>${lista[n].NombreEstado}</label>`,
            `<input type="text" id="sMotivo_${n}" value="${lista[n].Motivo}" readOnly/>`,
            html_tbody,

        ]).draw(false);

        if (lista[n].Estado == 1) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
        }
        else if (lista[n].Estado == 2) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";
        }
        else if (lista[n].Estado == 3) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";
            document.getElementById(`sMotivo_${n}`).readOnly = false;
        }
    }

    $('#table_form_mat_part-count-row').text(table.rows().count());

}
//#endregion


//#region CERRAR Fila
function fnCerrarFila(n) {
    ListaDetalleMatPart[n].nIdEstado = 2
    ListaDetalleMatPart[n].sEstado = "Cerrado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Cerrado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = true;
    //document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion


//#region ANULAR Fila
function fnAnularFila(n) {

    ListaDetalleMatPart[n].nIdEstado = 3
    ListaDetalleMatPart[n].sEstado = "Anulado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Anulado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion

