//#region Variables
var ListaEvaluacion;
var table_VIB_CC
var table_VIB_MB
var bTable = false;

var ListaTabla;
var ListaDetalleVIB = [];

var listaLugares = []
var listaTiempoExposicion = []
var listaTiempoMonitoreo = []

var listaAwx = []
var listaAwy = []
var listaAwz = []

var listaAwxProm = []
var listaAwyProm = []
var listaAwzProm = []

var listaAeqX = []
var listaAeqY = []
var listaAeqZ = []

var listaAMaxEjes = []
var listaAeqGlobal = []
var listaTiempoMaxPerm = []
var listaLMP = []
var listaNivelAccion = []
var listaIndiceExposicion = []
var listaNivelRiesgo = []

var listaCumplimientoNormativa = []
var listaEstado = []
var listaMotivo = []
//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    fnOcultarMostrarMotivo();
    fnOcultarMostrarBotones();

    onInit();

    $("#sNumeroDoc").on('change', onChangeNroDocumento);
    $("#dat_EPP").on('change', onChangeEPP);


    $.fn.dataTable.ext.search.pop();

    if (IdTipoEvaluacion == 4 || IdTipoEvaluacion == 5) {
        $('#divTableVIB_CC').show();
        $('#divTableVIB_MB').hide();
    }
    else if (IdTipoEvaluacion == 6) {
        $('#divTableVIB_CC').hide();
        $('#divTableVIB_MB').show();
    }

});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnCargarSedesAreasGerenciasMonitoreo(),
        fnCargarListaPuestos(),
        fnCargarPlucksMonitoreo(),
        fnGetSelectsForm(IdTipoEvaluacion)
    ]);

    fnSetSelectsMonitoreo();
    fnSetSelectsForm();

    fnCargarDatosGeneral();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Cargar Datos en General
async function fnCargarDatosGeneral() {
    //1: Abierto, 2:Cerrado, 3: Anulado, 4:Programado, 5:En Progreso
    if (EstadoEvaluacion == 1) {
        fnFormDisabled();
        fnCargarDatosMonitoreo();
    }
    else if (EstadoEvaluacion == 4) {
        fnCargarDatosMonitoreo();
    }
    else {
        fnCargarDatosEvaluacion();
        fnCargarLineasDetalle();
    }

    //Solo Ver
    if (!bModoEditar || EstadoEvaluacion == 2) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = true;
    }
    //Anulado
    else if (EstadoEvaluacion == 3) {
        fnFormDisabled();
        document.getElementById("dFechaProgramacion").disabled = false;
    }

    if (IdTipoEvaluacion == 5) {
        $('#divPromedios').hide();
        $('#divResultados').hide();
    }
}
//#endregion


//#region GET Cargar Datos de Evaluacion para Editar
function fnCargarDatosEvaluacion() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_form?code=Yl68m4mOaMYua7CjjPCFOWT5E6fNotQtWa63t0GOpKiamhBNMUpfjQ==&AccionBackEnd=DatosEvaluacionVIB&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaEvaluacion = response.lista_DatosEvaluacion

        fnSetDatosEvaluacion(ListaEvaluacion);

    });
}
//#endregion


//#region SET Asignar Datos de Evaluacion para Editar
function fnSetDatosEvaluacion(lista) {

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $("#dat_puesto option[value='" + lista[0].IdPuestoCargo + "']").prop("selected", true);
    $("#dat_zona option[value='" + lista[0].IdZona + "']").prop("selected", true);
    $("#dat_etapa option[value='" + lista[0].IdEtapa + "']").prop("selected", true);

    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(lista[0].CodigoTareaMonitoreo);

    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaEjecucion != "") { $('#dFechaRegistro').val(lista[0].FechaEjecucion); }

    $('#sDescripcionZona').val(lista[0].DescripcionZona);
    $('#sFuente').val(lista[0].Fuente);

    if (lista[0].NroDocumento > 0) {
        $('#sNumeroDoc').val(lista[0].NroDocumento);
        $('#sNombres').val(lista[0].Nombres);
        $('#sApellidos').val(lista[0].Apellidos);
    }

    $('#nRegimen').val(lista[0].Regimen);
    $('#nTiempoPuesto').val(lista[0].TiempoPuesto);
    $('#nTiempoAlmuerzo').val(lista[0].TiempoAlmuerzo);
    $('#sActivGeneral').val(lista[0].ActividadesGenerales);

    $("#dat_Ingenieria option[value='" + lista[0].Ingenieria + "']").prop("selected", true);
    $('#sDetallar').val(lista[0].Detallar);
    $("#dat_EPP option[value='" + lista[0].EPP + "']").prop("selected", true);
    $('#sAdministrativo').val(lista[0].Administrativo);
    $('#sMarcaControl').val(lista[0].MarcaControl);
    $('#sModeloControl').val(lista[0].ModeloControl);
    $('#sNRREquipo').val(lista[0].NRREquipo);

    if (lista[0].EPP == 3) {
        $('#divDobleProteccion').show();
        $('#sMarcaControl_2').val(lista[0].MarcaControl_2);
        $('#sModeloControl_2').val(lista[0].ModeloControl_2);
        $('#sNRREquipo_2').val(lista[0].NRREquipo_2);
    }

    $('#sMarcaMedicion').val(lista[0].MarcaMedicion);
    $('#sModeloMedicion').val(lista[0].ModeloMedicion);
    $('#sNumSerie').val(lista[0].Serie);

    if (IdTipoEvaluacion == 4 || IdTipoEvaluacion == 6) {
        $('#nAwxProm').val(lista[0].AwxProm);
        $('#nAwyProm').val(lista[0].AwyProm);
        $('#nAwzProm').val(lista[0].AwzProm);
        $('#nAeqX').val(lista[0].AeqX);
        $('#nAeqY').val(lista[0].AeqY);
        $('#nAeqZ').val(lista[0].AeqZ);
        $('#nAeqMaximo').val(lista[0].AeqMaximo);
        $('#nAeqGlobal').val(lista[0].AeqGlobal);

        $('#nLMP').val(lista[0].LMP);
        $('#nNivelAccion').val(lista[0].NivelAccion);
        $("#dat_NivelRiesgo option[value='" + lista[0].NivelRiesgo + "']").prop("selected", true);
        $("#dat_CumplimientoNormativa option[value='" + lista[0].CumplimientoNormativa + "']").prop("selected", true);
    }

    if (EstadoEvaluacion == 3) {
        fnSetMotivos();
        fnSetDatosMotivosAnulacion(lista);
    }

}
//#endregion


//#region LIMPIAR Listas
function fnLimpiarListas() {
    ListaDetalleVIB = []
    listaLugares = []
    listaTiempoExposicion = []
    listaTiempoMonitoreo = []
    listaAwx = []
    listaAwy = []
    listaAwz = []
    listaAwxProm = []
    listaAwyProm = []
    listaAwzProm = []
    listaAeqX = []
    listaAeqY = []
    listaAeqZ = []
    listaAMaxEjes = []
    listaAeqGlobal = []
    listaTiempoMaxPerm = []
    listaLMP = []
    listaNivelAccion = []
    listaIndiceExposicion = []
    listaNivelRiesgo = []
    listaCumplimientoNormativa = []
    listaEstado = []
    listaMotivo = []
}
//#endregion


//#region CRUD: Programar | Guardar | Cerrar | Anular
function fnCRUDEvaluacionMonitoreo(CodAccion, IdEstado) {

    if (IdTipoEvaluacion == 4 || IdTipoEvaluacion == 5) {
        if (!fnValidarLineasVIB_CC()) {
            return;
        }
    }

    else if (IdTipoEvaluacion == 6) {
        if (!fnValidarLineasVIB_MB()) {
            return;
        }
    }

    fnConfirmarLineas()

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //Tipo de Evaluacion
    data.TipoEvaluacion = IdTipoEvaluacion;

    //CodAccion => 0: Registrar | 1: Modificar | 2:Cerrar | 3: Anular | 4: Programar 
    if (CodAccion == 3 || CodAccion == 4) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = "1900-01-01";

        data.DescripcionZona = "";
        data.Fuente = "";

        data.NroDocumento = 0;
        data.Regimen = 0;
        data.TiempoAlmuerzo = 0;
        data.ActividadesGenerales = "";

        data.Ingenieria = 0;
        data.Detallar = "";
        data.Administrativo = "";
        data.EPP = 0;
        data.MarcaControl = "";
        data.ModeloControl = "";
        data.NRREquipo = "";
        data.MarcaControl_2 = "";
        data.ModeloControl_2 = "";
        data.NRREquipo_2 = "";

        data.MarcaMedicion = "";
        data.ModeloMedicion = "";
        data.Serie = "";

        data.listaLugares = "";
        data.listaTiempoExposicion = "";
        data.listaTiempoMonitoreo = "";
        data.listaAwx = "";
        data.listaAwy = "";
        data.listaAwz = "";

        data.listaAeqX = "";
        data.listaAeqY = "";
        data.listaAeqZ = "";

        data.listaAwxProm = "";
        data.listaAwyProm = "";
        data.listaAwzProm = "";

        data.listaAMaxEjes = "";
        data.listaAeqGlobal = "";
        data.listaTiempoMaxPerm = "";
        data.listaLMP = "";
        data.listaNivelAccion = "";
        data.listaIndiceExposicion = "";
        data.listaNivelRiesgo = "";

        data.listaCumplimientoNormativa = "";
        data.listaEstado = "";
        data.listaMotivo = "";

        data.AwxProm = "";
        data.AwyProm = "";
        data.AwzProm = "";
        data.AeqX = "";
        data.AeqY = "";
        data.AeqZ = "";
        data.AeqMaximo = "";
        data.AeqGlobal = "";

        data.LMP = 0;
        data.NivelAccion = 0;
        data.NivelRiesgo = 0;
        data.CumplimientoNormativa = 0;

        data.MotivoSeleccionado = MotivoAnulacionSeleccionado;
        data.MotivoEscribir = MotivoAnulacionEscribir

        data.accion = CodAccion;

    }

    if (CodAccion == 0 || CodAccion == 1 || CodAccion == 2) {

        data.IdEvaluacion = IdEvaluacion;
        data.Estado = IdEstado;

        data.FechaProgramacion = $("#dFechaProgramacion").val();
        data.FechaRegistro = $("#dFechaRegistro").val();

        data.DescripcionZona = $("#sDescripcionZona").val();
        data.Fuente = $("#sFuente").val();

        data.NroDocumento = $("#sNumeroDoc").val();
        data.Regimen = $("#nRegimen").val();
        data.TiempoAlmuerzo = $("#nTiempoAlmuerzo").val();
        data.ActividadesGenerales = $("#sActivGeneral").val();

        data.Ingenieria = $("#dat_Ingenieria option:selected").val();
        data.Detallar = $("#sDetallar").val();
        data.Administrativo = $("#sAdministrativo").val();
        data.EPP = $("#dat_EPP option:selected").val();
        data.MarcaControl = $("#sMarcaControl").val();
        data.ModeloControl = $("#sModeloControl").val();
        data.NRREquipo = $("#sNRREquipo").val();
        data.MarcaControl_2 = $("#sMarcaControl_2").val();
        data.ModeloControl_2 = $("#sModeloControl_2").val();
        data.NRREquipo_2 = $("#sNRREquipo_2").val();

        data.MarcaMedicion = $("#sMarcaMedicion").val();
        data.ModeloMedicion = $("#sModeloMedicion").val();
        data.Serie = $("#sNumSerie").val();

        data.listaLugares = listaLugares.join('|')
        data.listaTiempoExposicion = listaTiempoExposicion.join('|')
        data.listaTiempoMonitoreo = listaTiempoMonitoreo.join('|')
        data.listaAwx = listaAwx.join('|')
        data.listaAwy = listaAwy.join('|')
        data.listaAwz = listaAwz.join('|')

        data.listaAwxProm = listaAwxProm.join('|')
        data.listaAwyProm = listaAwyProm.join('|')
        data.listaAwzProm = listaAwzProm.join('|')

        data.listaAeqX = listaAeqX.join('|')
        data.listaAeqY = listaAeqY.join('|')
        data.listaAeqZ = listaAeqZ.join('|')

        data.listaAMaxEjes = listaAMaxEjes.join('|')
        data.listaAeqGlobal = listaAeqGlobal.join('|')
        data.listaTiempoMaxPerm = listaTiempoMaxPerm.join('|')
        data.listaLMP = listaLMP.join('|')
        data.listaNivelAccion = listaNivelAccion.join('|')
        data.listaIndiceExposicion = listaIndiceExposicion.join('|')
        data.listaNivelRiesgo = listaNivelRiesgo.join('|')

        data.listaCumplimientoNormativa = listaCumplimientoNormativa.join('|')
        data.listaEstado = listaEstado.join('|')
        data.listaMotivo = listaMotivo.join('|')

        data.AwxProm = $("#nAwxProm").val();
        data.AwyProm = $("#nAwyProm").val();
        data.AwzProm = $("#nAwzProm").val();
        data.AeqX = $("#nAeqX").val();
        data.AeqY = $("#nAeqY").val();
        data.AeqZ = $("#nAeqZ").val();
        data.AeqMaximo = $("#nAeqMaximo").val();
        data.AeqGlobal = $("#nAeqGlobal").val();

        data.LMP = $("#nLMP").val();
        data.NivelAccion = $("#nNivelAccion").val();
        data.NivelRiesgo = $("#dat_NivelRiesgo option:selected").val();
        data.CumplimientoNormativa = $("#dat_CumplimientoNormativa option:selected").val();

        data.MotivoSeleccionado = 0;
        data.MotivoEscribir = '';

        data.accion = CodAccion;
    }


    showLoading();

    let url = apiUrlsho + `/api/ho_Post_monitoreo_evaluar?code=FhnsxxYBMqr1RirUftBaVPsevFlBysozPo2DzN3CJ/UieNTCcE4Cfw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        EjecutarModal = (CodAccion >= 0 && response.Id > 0) ? true : false;

        switch (CodAccion) {
            case 0:
                TituloModal = 'Formulario guardado con éxito';
                break;
            case 1:
                TituloModal = 'Se actualizó la evaluación de monitoreo con éxito';
                break;
            case 2:
                TituloModal = 'Se cerró la evaluación de monitoreo con éxito';
                break;
            case 3:
                TituloModal = 'Se anuló la tarea de monitoreo con éxito';
                break;
            case 4:
                TituloModal = 'Se programó la evaluación de monitoreo con éxito';
                break;
            default:
                break;
        }


        if (EjecutarModal) {

            hideLoading();

            Swal.fire({
                title: TituloModal,
                iconColor: "#8fbb02",
                iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                showConfirmButton: false,
                padding: "3em 3em 6em 3em ",
                timer: 1500,
            }).then(() => {
                fnVolverIndexMonitoreoEva();
            });

        }
        else {
            fnLimpiarListas();
        }


    });

}
//#endregion


//#region AGREGAR a Lista Detalle
function fnAgregarDetalle() {
    this.ListaDetalleVIB.push
        ({
            nNro: this.ListaDetalleVIB.length + 1,
            sNombreLugar: $("#sLugar").val(),
            nTiempoExposicion: "",
            nTiempoMonitoreo: "",

            nAwx: "",
            nAwy: "",
            nAwz: "",

            nAeqX: "",
            nAeqY: "",
            nAeqZ: "",

            nAMaxEjes: "",
            nAeqGlobal: "",
            nTiempoMaxPerm: "",
            nLMP: "",
            nNivelAccion: "",
            nIndiceExposicion: "",
            nNivelRiesgo: "",

            nCumplimientoNormativa: "",
            nIdEstado: 1,
            sEstado: "Abierto",
            sMotivo: "",
            bActivo: 0
        });

}
//#endregion


//#region AGREGAR Fila
function fnAgregarFila() {

    if (IdTipoEvaluacion == 4 || IdTipoEvaluacion == 5) {
        if (!bTable) {
            table_VIB_CC = $('#table_form_vib').DataTable({
                searching: false,
                paging: false,
                ordering: false,
                bInfo: false
            });
        }

        bTable = true;
        fnAgregarFilaVIB_CC();
    }
    else if (IdTipoEvaluacion == 6) {
        if (!bTable) {
            table_VIB_MB = $('#table_form_vib_mb').DataTable({
                searching: false,
                paging: false,
                ordering: false,
                bInfo: false
            });
        }

        bTable = true;
        fnAgregarFilaVIB_MB();
    }
}
//#endregion


//#region CONFIRMAR Lineas
function fnConfirmarLineas() {

    if (IdTipoEvaluacion == 4 || IdTipoEvaluacion == 5) {

        fnConfirmarLineasVIB_CC();
    }
    else if (IdTipoEvaluacion == 6) {

        fnConfirmarLineasVIB_MB();
    }
}
//#endregion


//#region GET Cargar Lineas Detalles
function fnCargarLineasDetalle() {

    if (IdTipoEvaluacion == 4 || IdTipoEvaluacion == 5) {

        fnCargarLineasDetalleVIB_CC();
    }
    else if (IdTipoEvaluacion == 6) {

        fnCargarLineasDetalleVIB_MB();
    }
}
//#endregion


//#region AGREGAR Fila VIB CC
function fnAgregarFilaVIB_CC() {

    let sLugar = $("#sLugar").val();
    //Validar
    if (sLugar == "" || sLugar == undefined || sLugar == null) {
        return Swal.fire(
            'Advertencia',
            'Debe colocar el nombre del lugar antes de agregar a lista.',
            'warning'
        )
    }

    fnAgregarDetalle();

    var nFilas = this.ListaDetalleVIB.length // Tamaño de Filas Totales
    var n = this.ListaDetalleVIB.length - 1//Fila Actual
    var lista = ListaDetalleVIB[n]//Objeto Actual

    opcCumplNorm = OpcionesCumplimientoNormativa();
    opcNivelRiesgo = OpcionesNivelRiesgo();
    html_tbody = OpcionesTable(n);

    table_VIB_CC.row.add([
        `<label id="nNro_${n}">         ${nFilas}   </label>`,
        `<label id="sNombreLugar_${n}"> ${sLugar}   </label>`,
        `<input type="number" id="nTiempoExposicion_${n}" />`,
        `<input type="number" id="nTiempoMonitoreo_${n}" />`,

        `<input type="number" id="nAwx_${n}" step="0.01"/>`,
        `<input type="number" id="nAwy_${n}" step="0.01"/>`,
        `<input type="number" id="nAwz_${n}" step="0.01"/>`,

        `<input type="number" id="nAeqX_${n}" step="0.01"/>`,
        `<input type="number" id="nAeqY_${n}" step="0.01" />`,
        `<input type="number" id="nAeqZ_${n}" step="0.01"/>`,

        `<input type="number" id="nAMaxEjes_${n}" step="0.01"/>`,
        `<input type="number" id="nAeqGlobal_${n}" step="0.01" />`,
        `<input type="number" id="nTiempoMaxPerm_${n}" step="0.01"/>`,
        `<input type="number" id="nLMP_${n}" step="0.01"/>`,
        `<input type="number" id="nNivelAccion_${n}" />`,

        `<select id="nNivelRiesgo_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcNivelRiesgo}</select></div>`,
        `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcCumplNorm}</select></div>`,
        `<label id="sEstado_${n}"/>${lista.sEstado}</label>`,
        `<input type="text" id="sMotivo_${n}" value="Sin Motivo"/>`,
        html_tbody,

    ]).draw(false);

    document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
    document.getElementById(`sMotivo_${n}`).readOnly = true;

    $('#sLugar').val("");
    $('#nLugaresPlan').val(nFilas);
    $('#nLugaresMedidos').val(nFilas);

    $('#table_form_vib-count-row').text(table_VIB_CC.rows().count());
}
//#endregion


//#region Validar Lineas Detalles VIB CC
function fnValidarLineasVIB_CC() {

    var bValido = true;

    for (let n = 0; n < this.ListaDetalleVIB.length; n++) {

        ListaDetalleVIB[n].nTiempoExposicion = $(`#nTiempoExposicion_${n}`).val();
        ListaDetalleVIB[n].nTiempoMonitoreo = $(`#nTiempoMonitoreo_${n}`).val();

        ListaDetalleVIB[n].nAwx = $(`#nAwx_${n}`).val();
        ListaDetalleVIB[n].nAwy = $(`#nAwy_${n}`).val();
        ListaDetalleVIB[n].nAwz = $(`#nAwz_${n}`).val();

        ListaDetalleVIB[n].nAeqX = $(`#nAeqX_${n}`).val();
        ListaDetalleVIB[n].nAeqY = $(`#nAeqY_${n}`).val();
        ListaDetalleVIB[n].nAeqZ = $(`#nAeqZ_${n}`).val();

        ListaDetalleVIB[n].nAMaxEjes = $(`#nAMaxEjes_${n}`).val();
        ListaDetalleVIB[n].nAeqGlobal = $(`#nAeqGlobal_${n}`).val();
        ListaDetalleVIB[n].nTiempoMaxPerm = $(`#nTiempoMaxPerm_${n}`).val();
        ListaDetalleVIB[n].nLMP = $(`#nLMP_${n}`).val();
        ListaDetalleVIB[n].nNivelAccion = $(`#nNivelAccion_${n}`).val();

        ListaDetalleVIB[n].nNivelRiesgo = $(`#nNivelRiesgo_${n} option:selected`).val();
        ListaDetalleVIB[n].nCumplimientoNormativa = $(`#nCumplimientoNormativa_${n} option:selected`).val();
        ListaDetalleVIB[n].sMotivo = $(`#sMotivo_${n}`).val();

        if (
            ListaDetalleVIB[n].sNombreLugar == "" ||
            ListaDetalleVIB[n].nTiempoExposicion == "" ||
            ListaDetalleVIB[n].nTiempoMonitoreo == "" ||

            ListaDetalleVIB[n].nAwx == "" ||
            ListaDetalleVIB[n].nAwy == "" ||
            ListaDetalleVIB[n].nAwz == "" ||
            ListaDetalleVIB[n].nAeqX == "" ||
            ListaDetalleVIB[n].nAeqY == "" ||
            ListaDetalleVIB[n].nAeqZ == "" ||

            ListaDetalleVIB[n].nAMaxEjes == "" ||
            ListaDetalleVIB[n].nAeqGlobal == "" ||
            ListaDetalleVIB[n].nTiempoMaxPerm == "" ||
            ListaDetalleVIB[n].nLMP == "" ||
            ListaDetalleVIB[n].nNivelAccion == "" ||

            ListaDetalleVIB[n].nNivelRiesgo == "" ||
            ListaDetalleVIB[n].nCumplimientoNormativa == "" ||
            ListaDetalleVIB[n].nIdEstado == 0
        ) {
            bValido = false
            Swal.fire(
                'Advertencia',
                `Debe completar los datos del lugar Nro. ${n + 1} de la tabla de Datos de monitoreo.`,
                'warning'
            )
        }

        //Filas Anuladas
        if (ListaDetalleVIB[n].nIdEstado == 3 && ListaDetalleVIB[n].sMotivo == "") {
            bValido = false
            Swal.fire(
                'Advertencia',
                `El Lugar Nro. ${n + 1} tiene estado Anulado, debe completar el Motivo.`,
                'warning'
            )
        }

    }

    return bValido

}
//#endregion


//#region Confirmar Lineas Detalles VIB CC
function fnConfirmarLineasVIB_CC() {

    if (this.ListaDetalleVIB.length > 0) {
        for (let i = 0; i < this.ListaDetalleVIB.length; i++) {
            this.listaLugares.push(this.ListaDetalleVIB[i].sNombreLugar)
            this.listaTiempoExposicion.push(this.ListaDetalleVIB[i].nTiempoExposicion)
            this.listaTiempoMonitoreo.push(this.ListaDetalleVIB[i].nTiempoMonitoreo)

            this.listaAwx.push(this.ListaDetalleVIB[i].nAwx)
            this.listaAwy.push(this.ListaDetalleVIB[i].nAwy)
            this.listaAwz.push(this.ListaDetalleVIB[i].nAwz)

            this.listaAeqX.push(this.ListaDetalleVIB[i].nAeqX)
            this.listaAeqY.push(this.ListaDetalleVIB[i].nAeqY)
            this.listaAeqZ.push(this.ListaDetalleVIB[i].nAeqZ)

            this.listaAMaxEjes.push(this.ListaDetalleVIB[i].nAMaxEjes)
            this.listaAeqGlobal.push(this.ListaDetalleVIB[i].nAeqGlobal)
            this.listaTiempoMaxPerm.push(this.ListaDetalleVIB[i].nTiempoMaxPerm)
            this.listaLMP.push(this.ListaDetalleVIB[i].nLMP)
            this.listaNivelAccion.push(this.ListaDetalleVIB[i].nNivelAccion)
            this.listaIndiceExposicion.push(this.ListaDetalleVIB[i].nIndiceExposicion)
            this.listaNivelRiesgo.push(this.ListaDetalleVIB[i].nNivelRiesgo)

            this.listaCumplimientoNormativa.push(this.ListaDetalleVIB[i].nCumplimientoNormativa)
            this.listaEstado.push(this.ListaDetalleVIB[i].nIdEstado)
            this.listaMotivo.push(this.ListaDetalleVIB[i].sMotivo)

        }
    }

}
//#endregion


//#region GET Cargar Lineas Detalles
function fnCargarLineasDetalleVIB_CC() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_table?code=60Gs5EVzToGI4HE97MLaoGyLDDlHeh3qxh3Y0PJl95KECrDYlsrF9g==&AccionBackEnd=TablaEvaluacionVIB&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaTabla = response.lista_DatosEvaluacion

        if (ListaTabla.length > 0) {
            if (!bTable) {
                table_VIB_CC = $('#table_form_vib').DataTable({
                    searching: false,
                    paging: false,
                    ordering: false,
                    bInfo: false
                });
            }

            bTable = true;
            fnSetLineasDetalleVIB_CC(ListaTabla);
        }

    });
}
//#endregion


//#region SET Asignar Lineas Detalles VIB CC 
function fnSetLineasDetalleVIB_CC(lista) {

    opcCumplNorm = OpcionesCumplimientoNormativa();
    opcNivelRiesgo = OpcionesNivelRiesgo();

    for (let n = 0; n < lista.length; n++) {

        this.ListaDetalleVIB.push
            ({
                nNro: n + 1,
                sNombreLugar: lista[n].Lugar,
                nTiempoExposicion: lista[n].TiempoExposicion,
                nTiempoMonitoreo: lista[n].TiempoMonitoreo,

                nAwx: lista[n].Awx,
                nAwy: lista[n].Awy,
                nAwz: lista[n].Awz,

                nAeqX: lista[n].AeqX,
                nAeqY: lista[n].AeqY,
                nAeqZ: lista[n].AeqZ,

                nAMaxEjes: lista[n].AMaxEjes,
                nAeqGlobal: lista[n].AeqGlobal,
                nTiempoMaxPerm: lista[n].TiempoMaxPerm,
                nLMP: lista[n].LMP,
                nNivelAccion: lista[n].NivelAccion,

                nNivelRiesgo: lista[n].NivelRiesgo,
                nCumplimientoNormativa: lista[n].CumplimientoNormativa,

                nIdEstado: lista[n].Estado,
                sEstado: lista[n].NombreEstado,
                sMotivo: lista[n].Motivo
            });

        html_tbody = OpcionesTable(n);

        table_VIB_CC.row.add([
            `<label id="nNro_${n}">         ${n + 1}   </label>`,
            `<label id="sNombreLugar_${n}"> ${lista[n].Lugar}   </label>`,

            `<input type="number" id="nTiempoExposicion_${n}" value="${lista[n].TiempoExposicion}" />`,
            `<input type="number" id="nTiempoMonitoreo_${n}" value="${lista[n].TiempoMonitoreo}" />`,

            `<input type="number" id="nAwx_${n}" value="${lista[n].Awx}" step="0.01"/>`,
            `<input type="number" id="nAwy_${n}" value="${lista[n].Awy}" step="0.01"/>`,
            `<input type="number" id="nAwz_${n}" value="${lista[n].Awz}" step="0.01"/>`,
            `<input type="number" id="nAeqX_${n}" value="${lista[n].AeqX}" step="0.01"/>`,
            `<input type="number" id="nAeqY_${n}" value="${lista[n].AeqY}" step="0.01"/>`,
            `<input type="number" id="nAeqZ_${n}" value="${lista[n].AeqZ}" step="0.01"/>`,

            `<input type="number" id="nAMaxEjes_${n}" value="${lista[n].AMaxEjes}" step="0.01"/>`,
            `<input type="number" id="nAeqGlobal_${n}" value="${lista[n].AeqGlobal}" step="0.01"/>`,
            `<input type="number" id="nTiempoMaxPerm_${n}" value="${lista[n].TiempoMaxPerm}" step="0.01"/>`,
            `<input type="number" id="nLMP_${n}" value="${lista[n].LMP}" step="0.01"/>`,
            `<input type="number" id="nNivelAccion_${n}" value="${lista[n].NivelAccion}" step="0.01"/>`,

            `<select id="nNivelRiesgo_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcNivelRiesgo}</select></div>`,
            `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcCumplNorm}</select></div>`,
            `<label id="sEstado_${n}"/>${lista[n].NombreEstado}</label>`,
            `<input type="text" id="sMotivo_${n}" value="${lista[n].Motivo}" readOnly/>`,
            html_tbody,

        ]).draw(false);

        $(`#nCumplimientoNormativa_${n} option[value='` + lista[n].CumplimientoNormativa + "']").prop("selected", true);
        $(`#nNivelRiesgo_${n} option[value='` + lista[n].NivelRiesgo + "']").prop("selected", true);


        if (lista[n].Estado == 1) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
        }
        else if (lista[n].Estado == 2) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";
        }
        else if (lista[n].Estado == 3) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";
            document.getElementById(`sMotivo_${n}`).readOnly = false;
        }
    }

    $('#table_form_vib-count-row').text(table_VIB_CC.rows().count());

    $('#nLugaresPlan').val(lista.length);
    $('#nLugaresMedidos').val(lista.length);

}
//#endregion


//#region CERRAR Fila
function fnCerrarFila(n) {
    ListaDetalleVIB[n].nIdEstado = 2
    ListaDetalleVIB[n].sEstado = "Cerrado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Cerrado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = true;
    //document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion


//#region ANULAR Fila
function fnAnularFila(n) {

    ListaDetalleVIB[n].nIdEstado = 3
    ListaDetalleVIB[n].sEstado = "Anulado"

    document.getElementById(`sEstado_${n}`).innerHTML = "Anulado";
    document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";

    document.getElementById(`sMotivo_${n}`).value = "";
    document.getElementById(`sMotivo_${n}`).readOnly = false;

}
//#endregion

//********************* VIB MB *********************/

//#region AGREGAR Fila VIB MB
function fnAgregarFilaVIB_MB() {

    let sLugar = $("#sLugar").val();
    //Validar
    if (sLugar == "" || sLugar == undefined || sLugar == null) {
        return Swal.fire(
            'Advertencia',
            'Debe colocar el nombre del lugar antes de agregar a lista.',
            'warning'
        )
    }

    fnAgregarDetalle();

    var nFilas = this.ListaDetalleVIB.length // Tamaño de Filas Totales
    var n = this.ListaDetalleVIB.length - 1//Fila Actual
    var lista = ListaDetalleVIB[n]//Objeto Actual

    opcCumplNorm = OpcionesCumplimientoNormativa();
    opcNivelRiesgo = OpcionesNivelRiesgo();
    html_tbody = OpcionesTable(n);

    table_VIB_MB.row.add([

        `<label id="nNro_${n}">         ${nFilas}   </label> `,
        `<label id="sNombreLugar_${n}" > ${sLugar}   </label> `,

        `<div class="row mt-2"><input type="number" id="nTiempoExposicion_${n}_a" /> </div>
         <div class="row mt-3"><input type="number" id="nTiempoExposicion_${n}_b" /> </div>
         <div class="row mt-3"><input type="number" id="nTiempoExposicion_${n}_c" /> </div>`,

        `<div class="row mt-2"><input type="number" id="nTiempoMonitoreo_${n}_a" /> </div>
         <div class="row mt-3"><input type="number" id="nTiempoMonitoreo_${n}_b" /> </div>
         <div class="row mt-3"><input type="number" id="nTiempoMonitoreo_${n}_c" /> </div>`,

        `<div class="row mt-2" style="padding-right: 1rem"><input type="number" id="nAwx_${n}_a" step="0.01"/> </div>
         <div class="row mt-3"><input type="number" id="nAwx_${n}_b" step="0.01"/> </div>
         <div class="row mt-3"><input type="number" id="nAwx_${n}_c" step="0.01"/> </div>`,

        `<div class="row mt-2" style="padding-right: 1rem"><input type="number" id="nAwy_${n}_a" step="0.01"/> </div>
         <div class="row mt-3"><input type="number" id="nAwy_${n}_b" step="0.01"/> </div>
         <div class="row mt-3"><input type="number" id="nAwy_${n}_c" step="0.01"/> </div>`,

        `<div class="row mt-2" style="padding-right: 1rem"><input type="number" id="nAwz_${n}_a" step="0.01"/> </div>
         <div class="row mt-3"><input type="number" id="nAwz_${n}_b" step="0.01"/> </div>
         <div class="row mt-3"><input type="number" id="nAwz_${n}_c" step="0.01"/> </div>`,

        `<input type="number" id="nAwxProm_${n}" step="0.01"/> `,
        `<input type="number" id="nAwyProm_${n}" step="0.01"/> `,
        `<input type="number" id="nAwzProm_${n}" step="0.01"/> `,

        `<input type="number" id="nAeqX_${n}" step="0.01"/> `,
        `<input type="number" id="nAeqY_${n}" step="0.01" /> `,
        `<input type="number" id="nAeqZ_${n}" step="0.01"/> `,

        `<input type="number" id="nAMaxEjes_${n}" step="0.01"/> `,
        `<input type="number" id="nLMP_${n}" step="0.01"/> `,

        `<input type="number" id="nNivelAccion_${n}" step="0.01" /> `,
        `<input type="number" id="nIndiceExposicion_${n}" step="0.01"/> `,

        `<select id="nNivelRiesgo_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcNivelRiesgo}</select> `,
        `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
        ${opcCumplNorm}</select> `,
        `<label id="sEstado_${n}"/>${lista.sEstado}</label> `,
        `<input type="text" id="sMotivo_${n}" value="Sin Motivo"/> `,
        `${html_tbody} `,

    ],

    ).draw();

    document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
    document.getElementById(`sMotivo_${n}`).readOnly = true;

    $('#sLugar').val("");
    $('#nLugaresPlan').val(nFilas);
    $('#nLugaresMedidos').val(nFilas);

    $('#table_form_vib_mb-count-row').text(table_VIB_MB.rows().count());
}
//#endregion


//#region Validar Lineas Detalles VIB MB
function fnValidarLineasVIB_MB() {

    var bValido = true;

    for (let n = 0; n < this.ListaDetalleVIB.length; n++) {

        ListaDetalleVIB[n].nTiempoExposicion_a = $(`#nTiempoExposicion_${n}_a`).val();
        ListaDetalleVIB[n].nTiempoExposicion_b = $(`#nTiempoExposicion_${n}_b`).val();
        ListaDetalleVIB[n].nTiempoExposicion_c = $(`#nTiempoExposicion_${n}_c`).val();

        ListaDetalleVIB[n].nTiempoMonitoreo_a = $(`#nTiempoMonitoreo_${n}_a`).val();
        ListaDetalleVIB[n].nTiempoMonitoreo_b = $(`#nTiempoMonitoreo_${n}_b`).val();
        ListaDetalleVIB[n].nTiempoMonitoreo_c = $(`#nTiempoMonitoreo_${n}_c`).val();


        ListaDetalleVIB[n].nAwx_a = $(`#nAwx_${n}_a`).val();
        ListaDetalleVIB[n].nAwx_b = $(`#nAwx_${n}_b`).val();
        ListaDetalleVIB[n].nAwx_c = $(`#nAwx_${n}_c`).val();

        ListaDetalleVIB[n].nAwy_a = $(`#nAwy_${n}_a`).val();
        ListaDetalleVIB[n].nAwy_b = $(`#nAwy_${n}_b`).val();
        ListaDetalleVIB[n].nAwy_c = $(`#nAwy_${n}_c`).val();

        ListaDetalleVIB[n].nAwz_a = $(`#nAwz_${n}_a`).val();
        ListaDetalleVIB[n].nAwz_b = $(`#nAwz_${n}_b`).val();
        ListaDetalleVIB[n].nAwz_c = $(`#nAwz_${n}_c`).val();

        ListaDetalleVIB[n].nAwxProm = $(`#nAwxProm_${n}`).val();
        ListaDetalleVIB[n].nAwyProm = $(`#nAwyProm_${n}`).val();
        ListaDetalleVIB[n].nAwzProm = $(`#nAwzProm_${n}`).val();

        ListaDetalleVIB[n].nAeqX = $(`#nAeqX_${n}`).val();
        ListaDetalleVIB[n].nAeqY = $(`#nAeqY_${n}`).val();
        ListaDetalleVIB[n].nAeqZ = $(`#nAeqZ_${n}`).val();

        ListaDetalleVIB[n].nAMaxEjes = $(`#nAMaxEjes_${n}`).val();
        ListaDetalleVIB[n].nLMP = $(`#nLMP_${n}`).val();

        ListaDetalleVIB[n].nNivelAccion = $(`#nNivelAccion_${n}`).val();
        ListaDetalleVIB[n].nIndiceExposicion = $(`#nIndiceExposicion_${n}`).val();

        ListaDetalleVIB[n].nNivelRiesgo = $(`#nNivelRiesgo_${n} option:selected`).val();
        ListaDetalleVIB[n].nCumplimientoNormativa = $(`#nCumplimientoNormativa_${n} option:selected`).val();
        ListaDetalleVIB[n].sMotivo = $(`#sMotivo_${n}`).val();

        if (
            ListaDetalleVIB[n].sNombreLugar == "" ||
            ListaDetalleVIB[n].nTiempoExposicion_a == "" ||
            ListaDetalleVIB[n].nTiempoExposicion_b == "" ||
            ListaDetalleVIB[n].nTiempoExposicion_c == "" ||

            ListaDetalleVIB[n].nTiempoMonitoreo_a == "" ||
            ListaDetalleVIB[n].nTiempoMonitoreo_b == "" ||
            ListaDetalleVIB[n].nTiempoMonitoreo_c == "" ||

            ListaDetalleVIB[n].nAwx_a == "" ||
            ListaDetalleVIB[n].nAwx_b == "" ||
            ListaDetalleVIB[n].nAwx_c == "" ||

            ListaDetalleVIB[n].nAwy_a == "" ||
            ListaDetalleVIB[n].nAwy_b == "" ||
            ListaDetalleVIB[n].nAwy_c == "" ||

            ListaDetalleVIB[n].nAwz_a == "" ||
            ListaDetalleVIB[n].nAwz_b == "" ||
            ListaDetalleVIB[n].nAwz_c == "" ||

            ListaDetalleVIB[n].nAwxProm == "" ||
            ListaDetalleVIB[n].nAwyProm == "" ||
            ListaDetalleVIB[n].nAwzProm == "" ||

            ListaDetalleVIB[n].nAeqX == "" ||
            ListaDetalleVIB[n].nAeqY == "" ||
            ListaDetalleVIB[n].nAeqZ == "" ||

            ListaDetalleVIB[n].nAMaxEjes == "" ||
            ListaDetalleVIB[n].nLMP == "" ||

            ListaDetalleVIB[n].nNivelAccion == "" ||
            ListaDetalleVIB[n].nIndiceExposicion == "" ||

            ListaDetalleVIB[n].nNivelRiesgo == "" ||
            ListaDetalleVIB[n].nCumplimientoNormativa == "" ||
            ListaDetalleVIB[n].nIdEstado == 0
        ) {
            bValido = false
            Swal.fire(
                'Advertencia',
                `Debe completar los datos del lugar Nro. ${n + 1} de la tabla de Datos de monitoreo.`,
                'warning'
            )
        }

        //Filas Anuladas
        if (ListaDetalleVIB[n].nIdEstado == 3 && ListaDetalleVIB[n].sMotivo == "") {
            bValido = false
            Swal.fire(
                'Advertencia',
                `El Lugar Nro. ${n + 1} tiene estado Anulado, debe completar el Motivo.`,
                'warning'
            )
        }

    }

    return bValido

}
//#endregion


//#region Confirmar Lineas Detalles VIB MB
function fnConfirmarLineasVIB_MB() {

    if (this.ListaDetalleVIB.length > 0) {
        for (let i = 0; i < this.ListaDetalleVIB.length; i++) {

            this.listaTiempoExposicion.push(this.ListaDetalleVIB[i].nTiempoExposicion_a)
            this.listaTiempoExposicion.push(this.ListaDetalleVIB[i].nTiempoExposicion_b)
            this.listaTiempoExposicion.push(this.ListaDetalleVIB[i].nTiempoExposicion_c)

            this.listaTiempoMonitoreo.push(this.ListaDetalleVIB[i].nTiempoMonitoreo_a)
            this.listaTiempoMonitoreo.push(this.ListaDetalleVIB[i].nTiempoMonitoreo_b)
            this.listaTiempoMonitoreo.push(this.ListaDetalleVIB[i].nTiempoMonitoreo_c)

            this.listaAwx.push(this.ListaDetalleVIB[i].nAwx_a)
            this.listaAwx.push(this.ListaDetalleVIB[i].nAwx_b)
            this.listaAwx.push(this.ListaDetalleVIB[i].nAwx_c)

            this.listaAwy.push(this.ListaDetalleVIB[i].nAwy_a)
            this.listaAwy.push(this.ListaDetalleVIB[i].nAwy_b)
            this.listaAwy.push(this.ListaDetalleVIB[i].nAwy_c)

            this.listaAwz.push(this.ListaDetalleVIB[i].nAwz_a)
            this.listaAwz.push(this.ListaDetalleVIB[i].nAwz_b)
            this.listaAwz.push(this.ListaDetalleVIB[i].nAwz_c)

            for (let j = 0; j < 3; j++) {
                this.listaLugares.push(this.ListaDetalleVIB[i].sNombreLugar)

                this.listaAwxProm.push(this.ListaDetalleVIB[i].nAwxProm)
                this.listaAwyProm.push(this.ListaDetalleVIB[i].nAwyProm)
                this.listaAwzProm.push(this.ListaDetalleVIB[i].nAwzProm)

                this.listaAeqX.push(this.ListaDetalleVIB[i].nAeqX)
                this.listaAeqY.push(this.ListaDetalleVIB[i].nAeqY)
                this.listaAeqZ.push(this.ListaDetalleVIB[i].nAeqZ)

                this.listaAMaxEjes.push(this.ListaDetalleVIB[i].nAMaxEjes)
                this.listaLMP.push(this.ListaDetalleVIB[i].nLMP)

                this.listaNivelAccion.push(this.ListaDetalleVIB[i].nNivelAccion)
                this.listaIndiceExposicion.push(this.ListaDetalleVIB[i].nIndiceExposicion)

                this.listaNivelRiesgo.push(this.ListaDetalleVIB[i].nNivelRiesgo)
                this.listaCumplimientoNormativa.push(this.ListaDetalleVIB[i].nCumplimientoNormativa)
                this.listaEstado.push(this.ListaDetalleVIB[i].nIdEstado)
                this.listaMotivo.push(this.ListaDetalleVIB[i].sMotivo)

            }

        }
    }

}
//#endregion


//#region GET Cargar Lineas Detalles VIB MB
function fnCargarLineasDetalleVIB_MB() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar_table?code=60Gs5EVzToGI4HE97MLaoGyLDDlHeh3qxh3Y0PJl95KECrDYlsrF9g==&AccionBackEnd=TablaEvaluacionVIB_MB&IdEvaluacion=` + IdEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaTabla = response.lista_DatosEvaluacion

        if (ListaTabla.length > 0) {
            if (!bTable) {
                table_VIB_MB = $('#table_form_vib_mb').DataTable({
                    searching: false,
                    paging: false,
                    ordering: false,
                    bInfo: false
                });
            }
            bTable = true;
            fnSetLineasDetalleVIB_MB(ListaTabla);
        }

    });
}
//#endregion


//#region SET Asignar Lineas Detalles VIB MB
function fnSetLineasDetalleVIB_MB(lista) {

    opcCumplNorm = OpcionesCumplimientoNormativa();
    opcNivelRiesgo = OpcionesNivelRiesgo();

    for (let n = 0; n < lista.length; n++) {

        this.ListaDetalleVIB.push
            ({
                nNro: n + 1,
                sNombreLugar: lista[n].Lugar,
                nTiempoExposicion_a: lista[n].TiempoExposicion_a,
                nTiempoExposicion_b: lista[n].TiempoExposicion_b,
                nTiempoExposicion_c: lista[n].TiempoExposicion_c,

                nTiempoMonitoreo_a: lista[n].TiempoMonitoreo_a,
                nTiempoMonitoreo_b: lista[n].TiempoMonitoreo_b,
                nTiempoMonitoreo_c: lista[n].TiempoMonitoreo_c,

                nAwx_a: lista[n].Awx_a,
                nAwx_b: lista[n].Awx_b,
                nAwx_c: lista[n].Awx_c,

                nAwy_a: lista[n].Awy_a,
                nAwy_b: lista[n].Awy_b,
                nAwy_c: lista[n].Awy_c,

                nAwz_a: lista[n].Awz_a,
                nAwz_b: lista[n].Awz_b,
                nAwz_c: lista[n].Awz_c,

                nAwxProm: lista[n].AwxProm,
                nAwyProm: lista[n].AwyProm,
                nAwzProm: lista[n].AwzProm,

                nAeqX: lista[n].AeqX,
                nAeqY: lista[n].AeqY,
                nAeqZ: lista[n].AeqZ,

                nAMaxEjes: lista[n].AMaxEjes,
                nLMP: lista[n].LMP,

                nNivelAccion: lista[n].NivelAccion,
                nIndiceExposicion: lista[n].IndiceExposicion,

                nNivelRiesgo: lista[n].NivelRiesgo,
                nCumplimientoNormativa: lista[n].CumplimientoNormativa,

                nIdEstado: lista[n].Estado,
                sEstado: lista[n].NombreEstado,
                sMotivo: lista[n].Motivo
            });

        html_tbody = OpcionesTable(n);

        table_VIB_MB.row.add([
            `<label id="nNro_${n}">         ${n + 1}   </label>`,
            `<label id="sNombreLugar_${n}"> ${lista[n].Lugar}   </label>`,

            `<div class="row mt-2"><input type="number" id="nTiempoExposicion_${n}_a" value="${lista[n].TiempoExposicion_a}"/> </div>
            <div class="row mt-3"><input type="number" id="nTiempoExposicion_${n}_b" value="${lista[n].TiempoExposicion_b}"/> </div>
            <div class="row mt-3"><input type="number" id="nTiempoExposicion_${n}_c" value="${lista[n].TiempoExposicion_c}"/> </div>`,

            `<div class="row mt-2"><input type="number" id="nTiempoMonitoreo_${n}_a" value="${lista[n].TiempoMonitoreo_a}"/> </div>
            <div class="row mt-3"><input type="number" id="nTiempoMonitoreo_${n}_b"  value="${lista[n].TiempoMonitoreo_b}"/> </div>
            <div class="row mt-3"><input type="number" id="nTiempoMonitoreo_${n}_c"  value="${lista[n].TiempoMonitoreo_c}"/> </div>`,

            `<div class="row mt-2" style="padding-right: 1rem"><input type="number" id="nAwx_${n}_a" value="${lista[n].Awx_a}" step="0.01"/> </div>
            <div class="row mt-3"><input type="number" id="nAwx_${n}_b" value="${lista[n].Awx_b}" step="0.01"/> </div>
            <div class="row mt-3"><input type="number" id="nAwx_${n}_c" value="${lista[n].Awx_c}" step="0.01"/> </div>`,

            `<div class="row mt-2" style="padding-right: 1rem"><input type="number" id="nAwy_${n}_a" value="${lista[n].Awy_a}" step="0.01"/> </div>
            <div class="row mt-3"><input type="number" id="nAwy_${n}_b" value="${lista[n].Awy_b}" step="0.01"/> </div>
            <div class="row mt-3"><input type="number" id="nAwy_${n}_c" value="${lista[n].Awy_c}" step="0.01"/> </div>`,

            `<div class="row mt-2" style="padding-right: 1rem"><input type="number" id="nAwz_${n}_a" value="${lista[n].Awz_a}"  step="0.01"/> </div>
            <div class="row mt-3"><input type="number" id="nAwz_${n}_b" value="${lista[n].Awz_b}"  step="0.01"/> </div>
            <div class="row mt-3"><input type="number" id="nAwz_${n}_c" value="${lista[n].Awz_c}"  step="0.01"/> </div>`,

            `<input type="number" id="nAwxProm_${n}" value="${lista[n].AwxProm}" step="0.01"/> `,
            `<input type="number" id="nAwyProm_${n}" value="${lista[n].AwyProm}" step="0.01"/> `,
            `<input type="number" id="nAwzProm_${n}" value="${lista[n].AwzProm}" step="0.01"/> `,

            `<input type="number" id="nAeqX_${n}" value="${lista[n].AeqX}" step="0.01"/>`,
            `<input type="number" id="nAeqY_${n}" value="${lista[n].AeqY}" step="0.01"/>`,
            `<input type="number" id="nAeqZ_${n}" value="${lista[n].AeqZ}" step="0.01"/>`,

            `<input type="number" id="nAMaxEjes_${n}" value="${lista[n].AMaxEjes}" step="0.01"/>`,
            `<input type="number" id="nLMP_${n}" value="${lista[n].LMP}" step="0.01"/>`,

            `<input type="number" id="nNivelAccion_${n}" value="${lista[n].NivelAccion}" step="0.01"/>`,
            `<input type="number" id="nIndiceExposicion_${n}" value="${lista[n].IndiceExposicion}" step="0.01"/> `,

            `<select id="nNivelRiesgo_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcNivelRiesgo}</select></div>`,
            `<select id="nCumplimientoNormativa_${n}" class="form-control" style="border: 1px solid !important"/>
            ${opcCumplNorm}</select></div>`,
            `<label id="sEstado_${n}"/>${lista[n].NombreEstado}</label>`,
            `<input type="text" id="sMotivo_${n}" value="${lista[n].Motivo}" readOnly/>`,
            html_tbody,

        ]).draw(false);

        $(`#nCumplimientoNormativa_${n} option[value='` + lista[n].CumplimientoNormativa + "']").prop("selected", true);
        $(`#nNivelRiesgo_${n} option[value='` + lista[n].NivelRiesgo + "']").prop("selected", true);

        if (lista[n].Estado == 1) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAbierto";
        }
        else if (lista[n].Estado == 2) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleCerrado";
        }
        else if (lista[n].Estado == 3) {
            document.getElementById(`sEstado_${n}`).className = "clsDetalleAnulado";
            document.getElementById(`sMotivo_${n}`).readOnly = false;
        }
    }

    $('#table_form_vib_mb-count-row').text(table_VIB_MB.rows().count());

    $('#nLugaresPlan').val(lista.length);
    $('#nLugaresMedidos').val(lista.length);

}
//#endregion



