//#region Variables
var valor = 0;
var ObjTabla;
var rutaForm = "view/sho-hce/monitoreo/evaluar/form/"
var nTipoMonitoreo = 0
var ObjTablaMonitoreoEva;
var ObjListaBandejaMonitoreo
var ListaBandejaMonitoreoEva
var ObjSelectID = {};
//#endregion


//#region Constructor
$(document).ready(() => {
  SVGInject($(".inject-svg"));
});
//#endregion


//#region Al iniciar
async function onInit() {

  //Se inicia el Cargando
  showLoading();

  await Promise.all([
    fnCargarSelectID(),
    fnCargarSedesAreasGerenciasMonitoreo(),
    fnCargarPlucksMonitoreoGES(),
    fnCargarPlucksMonitoreo(), //Etapas
    fnCargarEstados()
  ]);

  await fnSetSelectMonitoreo();

  await fnCargarBandejaMonitoreoEva();
  await fnCargarTablaMonitoreoBandejaEva();



  $('.nav-link-ho').on('click', onClickSeccion);

  //Se apaga el Cargando
  hideLoading();

}
//#endregion


//#region Cambiar de Modulo
function onClickSeccion() {

  if (!$(this).hasClass("active")) {

    switch ($(this).attr("data-seccion")) {

      case "Identificar":

        Sp5controlNavegacionHigiene(100);
        handlerUrlhtml('contentGlobal', 'view/sho-hce/ges/datosGenerales.html', 'Identificar');
        break;

      case "Evaluar":

        Sp5controlNavegacionHigiene(200);
        handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_evaluar/datosGenerales.html', 'Evaluar');
        break;

      case "Controlar":

        Sp5controlNavegacionHigiene(300);
        handlerUrlhtml('contentGlobal', 'view/sho-hce/controlar/datosGenerales.html', 'Controlar');
        break;

    }

  }

}
//#endregion


//#region Inicializar
$(document).ready(function () {

  onInit();
  unSelect();

  $('.select2-container').css('width', '100%');

  fnSetChangeFilter();



});
//#endregion


//#region Cargar Fecha Actual (Hoy)
function cargarFechaHoy() {

  var fecha = new Date();
  var mes = fecha.getMonth() + 1;
  var dia = fecha.getDate();
  var anio = fecha.getFullYear();

  //Agrega cero si el dia es menor de 10
  if (dia < 10) {
    dia = '0' + dia;
  }

  //Agrega cero si el mes es menor de 10
  if (mes < 10) {
    mes = '0' + mes
  }

  return (anio + "-" + mes + "-" + dia);

}
//#endregion


//#region Ver Detalle
function Sp5verDetalleMonitoreo(IdEvaluacion, IdTipoEvaluacion, IdMonitoreo, IdEstadoEvaluacion) {

  IdEvaluacionGlobal = IdEvaluacion;
  IdMonitoreoGlobal = IdMonitoreo;
  IdTipoEvaluacionGlobal = IdTipoEvaluacion;
  IdEstadoEvaluacionGlobal = IdEstadoEvaluacion;
  bModoEditar = false;

  switch (IdTipoEvaluacion) {
    case 1:
      NombreFormulario = 'DOSI'
      handlerUrlhtml('contentGlobal', rutaForm + 'DOSI/form-dosimetria.html', 'Formulario DOSI');
      break;
    case 2:
      NombreFormulario = 'ILU'
      handlerUrlhtml('contentGlobal', rutaForm + 'ILU/form-iluminacion.html', 'Formulario ILU');
      break;
    case 3:
      NombreFormulario = 'SONO'
      handlerUrlhtml('contentGlobal', rutaForm + 'SONO/form-sonometria.html', 'Formulario SONO');
      break;
    case 4:
      NombreFormulario = 'VIB CC'
      handlerUrlhtml('contentGlobal', rutaForm + 'VIB/form-vib.html', 'Formulario VIB CC');
      break;
    case 5:
      NombreFormulario = 'VIB CC'
      handlerUrlhtml('contentGlobal', rutaForm + 'VIB/form-vib.html', 'Formulario VIB CC');
      break;
    case 6:
      NombreFormulario = 'VIB MB'
      handlerUrlhtml('contentGlobal', rutaForm + 'VIB/form-vib.html', 'Formulario VIB MB');
      break;
    case 7:
      NombreFormulario = 'Agentes Químicos'
      handlerUrlhtml('contentGlobal', rutaForm + 'agentes-quimicos/form-agentes-quimicos.html', 'Agentes Químicos');
      break;
    case 8:
      NombreFormulario = 'Material Particulado'
      handlerUrlhtml('contentGlobal', rutaForm + 'material-particulado/form-material-particulado.html', 'Formulario material particulado');
      break;
    case 9:
      NombreFormulario = 'Humos Metálicos'
      handlerUrlhtml('contentGlobal', rutaForm + 'humos-metalicos/form-humos-metalicos.html', 'Formulario humos metálicos');
      break;

    default:
      break;
  }

}
//#endregion


//#region Editar
function Sp5editarMonitoreo(IdEvaluacion, IdTipoEvaluacion, IdMonitoreo, IdEstadoEvaluacion) {

  // idDetalleMonitoreo = IdMonitoreo;

  IdEvaluacionGlobal = IdEvaluacion
  IdMonitoreoGlobal = IdMonitoreo
  IdTipoEvaluacionGlobal = IdTipoEvaluacion
  IdEstadoEvaluacionGlobal = IdEstadoEvaluacion
  bModoEditar = true

  switch (IdTipoEvaluacion) {
    case 1:
      NombreFormulario = 'DOSI'
      handlerUrlhtml('contentGlobal', rutaForm + 'DOSI/form-dosimetria.html', 'Formulario DOSI');
      break;
    case 2:
      NombreFormulario = 'ILU'
      handlerUrlhtml('contentGlobal', rutaForm + 'ILU/form-iluminacion.html', 'Formulario ILU');
      break;
    case 3:
      NombreFormulario = 'SONO'
      handlerUrlhtml('contentGlobal', rutaForm + 'SONO/form-sonometria.html', 'Formulario SONO');
      break;
    case 4:
      NombreFormulario = 'VIB CC'
      handlerUrlhtml('contentGlobal', rutaForm + 'VIB/form-vib.html', 'Formulario VIB CC');
      break;
    case 5:
      NombreFormulario = 'VIB CC'
      handlerUrlhtml('contentGlobal', rutaForm + 'VIB/form-vib.html', 'Formulario VIB CC');
      break;
    case 6:
      NombreFormulario = 'VIB MB'
      handlerUrlhtml('contentGlobal', rutaForm + 'VIB/form-vib.html', 'Formulario VIB MB');
      break;
    case 7:
      NombreFormulario = 'Agentes Químicos'
      handlerUrlhtml('contentGlobal', rutaForm + 'agentes-quimicos/form-agentes-quimicos.html', 'Agentes Químicos');
      break;
    case 8:
      NombreFormulario = 'Material Particulado'
      handlerUrlhtml('contentGlobal', rutaForm + 'material-particulado/form-material-particulado.html', 'Formulario material particulado');
      break;
    case 9:
      NombreFormulario = 'Humos Metálicos'
      handlerUrlhtml('contentGlobal', rutaForm + 'humos-metalicos/form-humos-metalicos.html', 'Formulario humos metálicos');
      break;

    default:
      break;
  }
}
//#endregion


//#region Navegacion
function Sp5controlNavegacionHigiene(CodControl) {
  ControlNavHigiene = CodControl;
}
//#endregion


//#region SET Asignar Datos de Selects
async function fnSetSelectMonitoreo() {

  let contenido = '';
  let ControlOptionSelect = false;


  //#region Cargando Select de CODIGO GES
  contenido = $('#dat_codigo_ges');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  ObjSelectID["GES"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.CodigoGES}</option>`)
  });
  //#endregion


  //#region Cargando Select de CODIGO Evaluacion
  contenido = $('#dat_codigo_evaluacion');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  ObjSelectID["Evaluacion"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.CodigoEvaluacion}</option>`)
  });
  //#endregion


  //#region Cargando Select de CODIGO Monitoreo
  contenido = $('#dat_codigo_monitoreo');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  ObjSelectID["Monitoreo"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.CodigoTareaMonitoreo}</option>`)
  });
  //#endregion

  
  //#region Cargando Select de Áreas
  contenido = $('#dat_area');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Areas"].forEach((Item) => {

    ControlOptionSelect = false;

    $('#dat_area option').each(function () {

      if ($.trim($(this).text()) == $.trim(Item.Description)) {
        ControlOptionSelect = true;
      }

    });

    if (!ControlOptionSelect) {

      contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`);

    }

  });
  //#endregion


  //#region Cargando Select de Gerencias
  contenido = $('#dat_gerencia');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Gerencias"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
  });
  //#endregion


  //#region Cargando Select de Sedes
  contenido = $('#dat_sede');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Sedes"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
  });
  //#endregion


  //#region Cargando Select de Zonas
  contenido = $('#dat_zona');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Zonas"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
  });
  //#endregion


  //#region Cargando Select de Puesto
  contenido = $('#dat_puesto');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Puestos"].forEach((Item) => {

    ControlOptionSelect = false;

    $('#dat_puesto option').each(function () {
      if ($.trim($(this).text()) == $.trim(Item.PuestoTrabajo)) {
        ControlOptionSelect = true;
      }
    });

    if (!ControlOptionSelect) {
      contenido.append(`<option value="${Item.Id}">${Item.PuestoTrabajo}</option>`);
    }

  });
  //#endregion


  //#region Cargando Select de Agentes
  contenido = $('#dat_agente');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Agentes"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.Agentes}</option>`)
  });
  //#endregion


  //#region Cargando Select de Estado
  contenido = $('#dat_estado');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Estados"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.Descripcion}</option>`)
  });
  //#endregion


  //#region Cargando Select de Etapas
  contenido = $('#dat_etapa');
  contenido.empty();
  contenido.append(`<option value=""></option>`);

  pluck_Monitoreo["Etapas"].forEach((Item) => {
    contenido.append(`<option value="${Item.Id}">${Item.Etapa}</option>`)
  });
  //#endregion

}
//#endregion


//#region GET Cargar ActividadMA, Agentes, Riesgos y Puestos
function fnCargarPlucksMonitoreoGES() {

  let url = apiUrlsho + `/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaCombosGestion_All`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {


    pluck_Monitoreo["ActividadMA"] = response.lista_Actividad_MA;
    pluck_Monitoreo["Agentes"] = response.lista_Agentes;
    pluck_Monitoreo["Riesgos"] = response.lista_Riesgos;
    pluck_Monitoreo["Puestos"] = response.lista_Cargos_Puestos_Trabajo;

  });
}
//#endregion


//#region GET Cargar Datos de la Tabla/Bandeja
async function fnCargarBandejaMonitoreoEva() {
  let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar?code=954FWrzAYNSsRlSx8D6aCbN8amsaVW9OGkGvzsaPvSmz1Ce9tQCkCg==&AccionBackEnd=BandejaMonitoreoEvaluacion`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {

    ListaBandejaMonitoreoEva = response.lista_BandejaMonitoreo

    fnSetValueStringsTable();

  });
}
//#endregion


//#region SET Asignar Valores en la tabla
async function fnSetValueStringsTable() {
  ListaBandejaMonitoreoEva.forEach((ItemMonitoreo) => {
    //Se recorre el listado Áreas y se compara con el AreaId_Empresa_H del paObj_ec y se añade la descripción del área al objeto paObj_ec
    pluck_Monitoreo["Areas"].forEach((Item) => {
      if (Item.Id == ItemMonitoreo.IdArea) {
        ItemMonitoreo.AreaDescripcion = Item.Description;
      }
    });

    //Se recorre el listado Gerencias 
    pluck_Monitoreo["Gerencias"].forEach((Item) => {
      if (Item.Id == ItemMonitoreo.IdGerencia) {
        ItemMonitoreo.GerenciaDescripcion = Item.Description;
      }
    });

    //Se recorre el listado Sedes
    pluck_Monitoreo["Sedes"].forEach((Item) => {
      if (Item.Id == ItemMonitoreo.IdUnidadSede) {
        ItemMonitoreo.SedeDescripcion = Item.Description;
      }
    });

    //Se recorre el listado Zona 
    pluck_Monitoreo["Zonas"].forEach((Item) => {
      if (Item.Id == ItemMonitoreo.IdZona) {
        ItemMonitoreo.ZonaDescripcion = Item.Description;
      }
    });

  });
}
//#endregion


//#region Cargar Datos de Tabla
function fnCargarTablaMonitoreoBandejaEva() {

  let contenido = '';
  let datoVacio = '';
  let html_tbody = '';
  let ContadorFila = 0;

  $.fn.dataTable.ext.search.pop();

  contenido = $('#content_monitoreo_evaluacion');
  contenido.empty();

  ListaBandejaMonitoreoEva.forEach((ItemMonitoreo) => {

    paObj_detalle_Monitoreo[ItemMonitoreo.Id] = ItemMonitoreo;

    html_tbody = "<tr>";
    html_tbody += "<td>" + ItemMonitoreo.GerenciaDescripcion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.SedeDescripcion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.AreaDescripcion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.PuestoCargoDescripcion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.AgenteDescripcion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.CodigoGES + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.ZonaDescripcion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.CodigoTareaMonitoreo + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.CodigoEvaluacion + "</td>";

    html_tbody += "<td>" + ItemMonitoreo.FechaProgramacion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.FechaEjecucion + "</td>";
    /* html_tbody += "<td>" + new Date(ItemMonitoreo.FechaProgramacion).toLocaleDateString('en-GB').split('T')[0] + "</td>";
    html_tbody += "<td>" + new Date(ItemMonitoreo.FechaEjecucion).toLocaleDateString('en-GB').split('T')[0] + "</td>"; */

    html_tbody += "<td>" + ItemMonitoreo.TipoEvaluacionDescripcion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.IdMedicion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.LugarMedicion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.IntensidadAgente + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.NivelRiesgo + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.NivelRiesgoGlobal + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.EstadoMedicion + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.IntensidadAgenteGlobal + "</td>";
    html_tbody += "<td>" + ItemMonitoreo.EstadoEvaluacion + "</td>";

    html_tbody += "<td>";
    html_tbody += "<div class='dropdown dropleft'>";
    html_tbody += "<a class='nav-link text-vertical' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
    html_tbody += "...";
    html_tbody += "</a>";
    html_tbody += "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";

    html_tbody += "<a class='dropdown-item' onclick='Sp5controlNavegacionHigiene(202);";
    html_tbody += "Sp5verDetalleMonitoreo(" + ItemMonitoreo.IdEvaluacion + "," + ItemMonitoreo.IdTipoEvaluacion + "," + ItemMonitoreo.IdMonitoreo + "," + ItemMonitoreo.IdEstadoEvaluacion + ");";
    html_tbody += "' style='cursor: pointer;'>";
    html_tbody += "<img src='./images/sho/eyeIcon.svg' alt='' fill='#01719d'>";
    html_tbody += "&nbsp;&nbsp; Ver registro";
    html_tbody += "</a>";

    if (ItemMonitoreo.IdEstadoEvaluacion != 2) {
      html_tbody += "<a class='dropdown-item' onclick='Sp5controlNavegacionHigiene(202);";
      html_tbody += "Sp5editarMonitoreo(" + ItemMonitoreo.IdEvaluacion + "," + ItemMonitoreo.IdTipoEvaluacion + "," + ItemMonitoreo.IdMonitoreo + "," + ItemMonitoreo.IdEstadoEvaluacion + ");";
      html_tbody += "' style='cursor: pointer; '>";
      html_tbody += "<img src='./images/sho/edit.svg' alt='' fill='#5daf57'>";
      html_tbody += "&nbsp;&nbsp; Editar";
      html_tbody += "</a>";
    }

    html_tbody += "</div>";
    html_tbody += "</div>";
    html_tbody += "</td>";

    html_tbody += "</tr>";

    contenido.append(html_tbody);
    ContadorFila++;

  });


  ObjTablaMonitoreoEva = $('#table_monitoreo_evaluacion').DataTable({
    "pageLength": 5,
    "ordering": false,
    "scrollX": true,
    "destroy": true,
    "language": {
      "paginate": {
        "next": "»",
        "previous": "«"
      },
      "loadingRecords": "Cargando registros...",
      "processing": "Procesando...",
      "infoPostFix": "",
      "zeroRecords": "No se encontraron registros"
    },
    "columnDefs": [
      {
        "targets": [14],
        "visible": true,
        "searchable": true
      },
    ],
    dom:
      /* "<'row btn-table'<B>>" + */
      "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
      "<'row row-records'<'col-sm-12'tr>>" +
      "<'row row-info'<'col-sm-12 col-md-12'p>>"
  });



  $('#table_monitoreo_evaluacion-count-row').text(ObjTablaMonitoreoEva.rows().count());
  $('.scrollbar').removeClass('dataTables_scrollBody');

  if ($.fn.DataTable.isDataTable('#table_monitoreo_evaluacion')) {
    $('#table_monitoreo_evaluacion').find('.nav-link').trigger('click');
  }

  //{ targets: [0, 1,2,3], visible: true, searchable: false},

  /* Inicializa la tabla cuando se hace uso del filtro fecha */
  $.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {

      var iFini = document.getElementById('dFechaInicio').value;
      var iFfin = document.getElementById('dFechaFin').value;
      var iStartDateCol = 7;
      var iEndDateCol = 7;

      iFini = iFini.replace(/-/g, "");
      iFfin = iFfin.replace(/-/g, "");

      var datofini = data[iStartDateCol].substring(6, 10) + data[iStartDateCol].substring(3, 5) + data[iStartDateCol].substring(0, 2);
      var datoffin = data[iEndDateCol].substring(6, 10) + data[iEndDateCol].substring(3, 5) + data[iEndDateCol].substring(0, 2);

      if (iFini === "" && iFfin === "") {
        return true;
      }
      else if (iFini <= datofini && iFfin === "") {
        return true;
      }
      else if (iFfin >= datoffin && iFini === "") {
        return true;
      }
      else if (iFini <= datofini && iFfin >= datoffin) {
        return true;
      }
      return false;

    }
  );

}
//#endregion


//#region GET Cargar ID GES
async function fnCargarSelectID() {
  let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar?code=954FWrzAYNSsRlSx8D6aCbN8amsaVW9OGkGvzsaPvSmz1Ce9tQCkCg==&AccionBackEnd=ListaComboId`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {


    ObjSelectID["GES"] = response.lista_GES;
    ObjSelectID["Evaluacion"] = response.lista_Evaluacion;
    ObjSelectID["Monitoreo"] = response.lista_Monitoreo;

    //fnSetValueSelectID();

  });
}
//#endregion


//#region Filtros de Tabla
function fnSetChangeFilter() {


  //#region Filtrar si es Select
  $('.data-filtro-select').on('change', function () {

    valor = ($(this).find('option[value="' + $(this).val().trim() + '"]').text()).trim();

    ObjTablaMonitoreoEva.column(parseInt($(this).attr('data-filtro-select'))).search(valor ? '^' + valor + '$' : '', true, false).draw();

    $('#table_monitoreo_evaluacion-count-row').text(ObjTablaMonitoreoEva.page.info().recordsDisplay);

  });
  //#endregion

  //#region Filtrar si es input text
  $('.data-filtro').on('keyup', function () {

    ObjTablaMonitoreoEva.column(parseInt($(this).attr('data-filtro'))).search($(this).val()).draw();
    $('#table_monitoreo_evaluacion-count-row').text(ObjTablaMonitoreoEva.page.info().recordsDisplay);

  });
  //#endregion

  //#region Filtrar Fechas
  $('#dFechaInicio').on('keyup change', function () {

    $('#dFechaFin').attr("dFechaInicio", $('#dFechaInicio').val());
    ObjTablaMonitoreoEva.draw();
    $('#table_monitoreo_evaluacion-count-row').text(ObjTablaMonitoreoEva.page.info().recordsDisplay);

  });

  $('#dFechaFin').on('keyup change', function () {

    $('#dFechaInicio').attr("dFechaFin", $('#dFechaFin').val());
    ObjTablaMonitoreoEva.draw();
    $('#table_monitoreo_evaluacion-count-row').text(ObjTablaMonitoreoEva.page.info().recordsDisplay);

  });

  $('#dFechaProgDesde').on('keyup change', function () {

    $('#dFechaProgHasta').attr("dFechaProgDesde", $('#dFechaProgDesde').val());
    ObjTablaMonitoreoEva.draw();
    $('#table_monitoreo_evaluacion-count-row').text(ObjTablaMonitoreoEva.page.info().recordsDisplay);

  });

  $('#dFechaProgHasta').on('keyup change', function () {

    $('#dFechaProgDesde').attr("dFechaProgHasta", $('#dFechaProgHasta').val());
    ObjTablaMonitoreoEva.draw();
    $('#table_monitoreo_evaluacion-count-row').text(ObjTablaMonitoreoEva.page.info().recordsDisplay);

  });
  //#endregion
}
//#endregion


//#region Limpiar Filtro Individual
function unSelect() {

  $("select").select2({
    allowClear: true
  }).on("select2:unselecting", function (e) {
    $(this).data('state', 'unselected');
  }).on("select2:open", function (e) {
    if ($(this).data('state') === 'unselected') {
      $(this).removeData('state');
      var self = $(this);
      setTimeout(function () {
        self.select2('close');
      }, 1);
    }
  });
}
//#endregion
