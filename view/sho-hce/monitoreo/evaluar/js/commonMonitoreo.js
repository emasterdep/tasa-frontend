//#region Variables
var objSelect = {}
var IdEvaluacion;
var EstadoEvaluacion = 0;
var IdTipoEvaluacion;
var MotivoAnulacionSeleccionado = 0;
var MotivoAnulacionEscribir = "";
var listaDatosMonitoreo;
//#endregion


//#region Asignar Variables Globales
async function fnSetVariablesGlobales() {
    //Variables Anteriores
    EstadoEvaluacion = IdEstadoEvaluacionGlobal;
    IdTipoEvaluacion = IdTipoEvaluacionGlobal;
    IdEvaluacion = IdEvaluacionGlobal;
}
//#endregion


//#region Cargar Fecha Actual (Hoy)
function cargarFechaHoy() {

    var fecha = new Date();
    var mes = fecha.getMonth() + 1;
    var dia = fecha.getDate();
    var anio = fecha.getFullYear();

    //Agrega cero si el dia es menor de 10
    if (dia < 10) {
        dia = '0' + dia;
    }

    //Agrega cero si el mes es menor de 10
    if (mes < 10) {
        mes = '0' + mes
    }
    return (anio + "-" + mes + "-" + dia);
}
//#endregion


//#region Ocultar Mostrar Botones
function fnOcultarMostrarBotones() {
    $('#divBtnInvisible').hide();
    $('#divDobleProteccion').hide();

    if (EstadoEvaluacion == 1) {
        $('#divBtnProgramar').show();
        $('#divBtnGuardar').hide();
    }
    else {
        $('#divBtnProgramar').hide();
        $('#divBtnGuardar').show();
    }

    //Solo leer
    if (!bModoEditar || EstadoEvaluacion == 1 || EstadoEvaluacion == 2 || EstadoEvaluacion == 3) {
        $('#divBtnCancelar').hide();
        $('#divBtnCerrar').hide();
        $('#divBtnAnular').hide();
        $('#divBtnGuardar').hide();
        $('#divBtnProgramar').hide();
    }

    //Abierto o Anulado
    if (EstadoEvaluacion == 1 || EstadoEvaluacion == 3) {
        $('#divBtnInvisible').show();
        $('#divBtnProgramar').show();
    }

}
//#endregion


//#region Ocultar Motivo Anulación
function fnOcultarMostrarMotivo(bool) {
    if (EstadoEvaluacion == 3) {
        $('#divMotivoAnulacion').show();
    }
    else {
        $('#divMotivoAnulacion').hide();
    }
}
//#endregion


//#region Funcionalidad Boton Cancelar
function fnVolverIndexMonitoreoEva() {
    handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_evaluar/datosGenerales.html', 'Evaluar');
}
//#endregion


//#region GET Areas, Gerencias, Sedes y Zonas
function fnCargarSedesAreasGerenciasMonitoreo() {

    //Se especifican los parametros para la consulta
    var url = apiUrlssoma + "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0"

    var headers = {
        "apikey": constantes.apiKey
    }

    var settings = {
        "url": url,
        "method": "GET",
        "timeout": 0,
        "crossDomain": true,
        "dataType": "json",
        "headers": headers,
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done(function (response) {

        pluck_Monitoreo["Areas"] = response.Area;
        pluck_Monitoreo["Gerencias"] = response.Gerencia;
        pluck_Monitoreo["Sedes"] = response.Sedes;
        pluck_Monitoreo["Zonas"] = response.Zonas;

    });

}
//#endregion


//#region GET Cargar Estados y Etapas
function fnCargarPlucksMonitoreo() {

    let url = apiUrlsho + `/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaCombosGestionMonitoreo_All`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        //pluck_Monitoreo["Estados"] = response.lista_EstadosInsp;
        pluck_Monitoreo["Etapas"] = response.lista_Etapas;

    });
}
//#endregion


//#region GET Cargar Estados
async function fnCargarEstados() {
    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar?code=954FWrzAYNSsRlSx8D6aCbN8amsaVW9OGkGvzsaPvSmz1Ce9tQCkCg==&AccionBackEnd=ListaEstados`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        pluck_Monitoreo["Estados"] = response.lista_Estados

    });
}
//#endregion


//#region GET Cargar Puestos
function fnCargarListaPuestos() {

    let url = apiUrlsho + `/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaCombosGestion_All`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {
        pluck_Monitoreo["Puestos"] = response.lista_Cargos_Puestos_Trabajo;
    });
}
//#endregion


//#region SET Asignar Selects Plucks Monitoreo
async function fnSetSelectsMonitoreo() {

    let contenido = '';
    let ControlOptionSelect = false;

    //#region Cargando Select de Gerencias
    contenido = $('#dat_gerencia');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Monitoreo["Gerencias"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
    });
    //#endregion


    //#region Cargando Select de Sedes
    contenido = $('#dat_sede');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Monitoreo["Sedes"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
    });
    //#endregion


    if (IdTipoEvaluacion != 2 && IdTipoEvaluacion != 3) {

        //#region Cargando Select de Áreas
        contenido = $('#dat_area');
        contenido.empty();
        contenido.append(`<option value=""></option>`);

        pluck_Monitoreo["Areas"].forEach((Item) => {

            ControlOptionSelect = false;

            $('#dat_area option').each(function () {
                if ($.trim($(this).text()) == $.trim(Item.Description)) {
                    ControlOptionSelect = true;
                }
            });

            if (!ControlOptionSelect) {
                contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`);
            }

        });

        //#endregion


        //#region Cargando Select de Puesto de Trabajo
        contenido = $('#dat_puesto');
        contenido.empty();
        contenido.append(`<option value=""></option>`);

        pluck_Monitoreo["Puestos"].forEach((Item) => {
            contenido.append(`<option value="${Item.Id}">${Item.PuestoTrabajo}</option>`)
        });
        //#endregion

    }


    //#region Cargando Select de Etapas
    contenido = $('#dat_etapa');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Monitoreo["Etapas"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Etapa}</option>`)
    });
    //#endregion


    //#region Cargando Select de Zonas
    if (IdTipoEvaluacion != 1) {
        contenido = $('#dat_zona');
        contenido.empty();
        contenido.append(`<option value=""></option>`);

        pluck_Monitoreo["Zonas"].forEach((Item) => {
            contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
        });
    }
    //#endregion

}
//#endregion


//#region GET Cargar Selects para los Formularios
function fnGetSelectsForm(IdTipoEvaluacion) {

    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar?code=954FWrzAYNSsRlSx8D6aCbN8amsaVW9OGkGvzsaPvSmz1Ce9tQCkCg==&AccionBackEnd=ListaCombosEvaluarMonitoreoForm&IdMonitoreoEvaForm=` + IdTipoEvaluacion;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {


        objSelect["Ingenierias"] = response.lista_Ingenieria;
        objSelect["EPPs"] = response.lista_EPP;
        objSelect["CumplimientoNormativas"] = response.lista_CumplimientoNormativa;
        objSelect["MotivosAnulacion"] = response.lista_MotivoAnulacion;
        if (IdTipoEvaluacion == 1 || IdTipoEvaluacion >= 4) {
            objSelect["NivelesRiesgo"] = response.lista_NivelRiesgo;
        }
        if (IdTipoEvaluacion == 9) {
            objSelect["ParaAnalisis"] = response.lista_ParaAnalisis;
        }

    });
}
//#endregion


//#region SET Asignar Selects para los Formularios
async function fnSetSelectsForm() {
    let contenido = '';

    //#region Cargando Select de Ingenierias
    contenido = $('#dat_Ingenieria');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    objSelect["Ingenierias"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Ingenieria}</option>`)
    });
    //#endregion


    //#region Cargando Select de EPP
    contenido = $('#dat_EPP');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    objSelect["EPPs"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Epp}</option>`)
    });
    //#endregion


    //#region Cargando Select de Cumplimiento de Normativa
    contenido = $('#dat_CumplimientoNormativa');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    objSelect["CumplimientoNormativas"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.CumplimientoNormativa}</option>`)
    });
    //#endregion


    //#region Cargando Select de Nivel de Riesgo
    if (IdTipoEvaluacion == 1 || IdTipoEvaluacion >= 4) {
        contenido = $('#dat_NivelRiesgo');
        contenido.empty();
        contenido.append(`<option value=""></option>`);

        objSelect["NivelesRiesgo"].forEach((Item) => {
            contenido.append(`<option value="${Item.Id}">${Item.NivelRiesgo}</option>`)
        });
    }
    //#endregion


    //#region Cargando Select de Para Analisis
    if (IdTipoEvaluacion == 9) {
        contenido = $('#dat_ParaAnalisis');
        contenido.empty();
        contenido.append(`<option value=""></option>`);

        objSelect["ParaAnalisis"].forEach((Item) => {
            contenido.append(`<option value="${Item.Id}">${Item.ParaAnalisis}</option>`)
        });
    }
    //#endregion

}
//#endregion


//#region Validar Formulario (Todos los Inputs y Selects)
function validationForm(class_selector) {
    let error = [];
    let inputs = $('input.' + class_selector);
    let selects = $('select.' + class_selector);

    inputs.each((e) => {

        if (!inputs.eq(e).val()) {
            inputs.eq(e).addClass('is-invalid');
            inputs.eq(e).parent().find('.invalid-feedback').remove();
            inputs.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
            error.push(inputs.eq(e).val())
        }
        if (inputs.eq(e).val()) {
            inputs.eq(e).removeClass('is-invalid');
        }
    })

    selects.each((e) => {
        if (!selects.eq(e).val()) {
            selects.eq(e).addClass('is-invalid');
            selects.eq(e).parent().find('.invalid-feedback').remove();
            selects.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
            error.push(selects.eq(e).val())
        }
        if (selects.eq(e).val()) {
            selects.eq(e).removeClass('is-invalid');
        }
    })

    return error;
}
//#endregion


//#region Validar Formulario (Todos los Inputs y Selects Required)
function fnValidationForm() {
    let error = [];
    let inputs = $('input');
    let selects = $('select');

    inputs.each((e) => {
        if (!inputs.eq(e).val()) {
            if (inputs[e].required) {
                inputs.eq(e).addClass('is-invalid');
                inputs.eq(e).parent().find('.invalid-feedback').remove();
                inputs.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
                error.push(inputs.eq(e).val())
            }
        }
        if (inputs.eq(e).val()) {
            inputs.eq(e).removeClass('is-invalid');
        }
    })

    selects.each((e) => {
        if (!selects.eq(e).val()) {
            if (selects[e].required) {
                selects.eq(e).addClass('is-invalid');
                selects.eq(e).parent().find('.invalid-feedback').remove();
                selects.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
                error.push(selects.eq(e).val())
            }
        }
        if (selects.eq(e).val()) {
            selects.eq(e).removeClass('is-invalid');
        }
    })

    return error;
}
//#endregion


//#region Funcionalidad Programar Evaluacion
function fnProgramarEvaluacion(CodAccion) {

    var EstadoDestino = 4;

    //Validar
    if ($("#dFechaProgramacion").val() == "") {
        return Swal.fire({
            icon: 'warning',
            title: 'Advertencia',
            text: 'Por favor elija una Fecha para programar.',
        });
    }

    Swal.fire({
        title: "Programar evaluación",
        html: `
        <p>Está por programar la evaluación de monitoreo</p>
        <p class="mt-5">¿Desea confirmar la acción?</p>`,
        icon: "info",
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonColor: "#ff3636",
        confirmButtonColor: "#8fbb02",
        confirmButtonText: `Confirmar <img class="inject-svg" src="./images/sho/confirm.svg" alt="" fill="#fff" width="18px">`,
        cancelButtonText: `Cancelar <img class="inject-svg" src="./images/sho/cancelar.svg" alt="" fill="#fff" width="18px">`,
    }).then((result) => {

        if (result.isConfirmed) {
            fnCRUDEvaluacionMonitoreo(CodAccion, EstadoDestino);
        }

    });

}
//#endregion


//#region Funcionalidad Cerrar Evaluacion
function fnCerrarEvaluacion(CodAccion) {

    var EstadoNuevo = 2;

    //Validar
    if ($("#dFechaProgramacion").val() == "") {
        return Swal.fire({
            icon: 'warning',
            title: 'Advertencia',
            text: 'Por favor elija una Fecha para programar.',
        });
    }

    if (fnValidationForm().length > 0) {
        return Swal.fire({
            icon: 'warning',
            title: 'Advertencia',
            text: 'Por favor complete todos los campos',
        });
    }

    Swal.fire({
        title: "Cerrar evaluación de monitoreo",
        html: `
      <p>Está por cerrar la evaluación de monitoreo</p>
      <p class="mt-5">¿Desea confirmar la acción?</p>`,
        icon: "info",
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonColor: "#ff3636",
        confirmButtonColor: "#8fbb02",
        confirmButtonText: `Confirmar <img class="inject-svg" src="./images/sho/confirm.svg" alt="" fill="#fff" width="18px">`,
        cancelButtonText: `Cancelar <img class="inject-svg" src="./images/sho/cancelar.svg" alt="" fill="#fff" width="18px">`,
    }).then((result) => {

        if (result.isConfirmed) {
            fnCRUDEvaluacionMonitoreo(CodAccion, EstadoNuevo);
        }
    });
}
//#endregion


//#region Funcionalidad Guardar Evaluacion
function fnGuardarEvaluacion() {

    if ($("#dFechaRegistro").val() == "") {
        return Swal.fire({
            icon: 'warning',
            title: 'Advertencia',
            text: 'Por favor elija una Fecha para registrar.',
        });
    }

    var CodAccion;
    var EstadoDestino = 5;

    CodAccion = EstadoEvaluacion == 5 ? 1 : 0

    EstadoNuevo = EstadoEvaluacion

    /* if (validationForm('submit').length > 0) {
        return;
    } */

    Swal.fire({
        title: "Guardar formulario",
        html: `
            <p>Está por guardar el formulario ${NombreFormulario}</p>
            <p class="mt-5">¿Desea confirmar la acción?</p>`,
        icon: "info",
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonColor: "#ff3636",
        confirmButtonColor: "#8fbb02",
        confirmButtonText: `Confirmar <img class="inject-svg" src="./images/sho/confirm.svg" alt="" fill="#fff" width="18px">`,
        cancelButtonText: `Cancelar <img class="inject-svg" src="./images/sho/cancelar.svg" alt="" fill="#fff" width="18px">`,
    }).then((result) => {

        if (result.isConfirmed) {
            fnCRUDEvaluacionMonitoreo(CodAccion, EstadoDestino);
        }

    });

}
//#endregion


//#region CHANGE Cambiar Numero de Documento
function onChangeNroDocumento() {
    if ($(this).val() != 0) {
        fnGetDatosEvaluado($("#sNumeroDoc").val())
    }
}
//#endregion


//#region GET Cargar Datos de Evaluado
async function fnGetDatosEvaluado(NumeroDoc) {

    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar?code=954FWrzAYNSsRlSx8D6aCbN8amsaVW9OGkGvzsaPvSmz1Ce9tQCkCg==&AccionBackEnd=ListaDatosEvaluado&NumeroDoc=` + NumeroDoc;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        var DatosEvaluado = response.lista_DatosEvaluado

        fnSetDatosEvaluado(DatosEvaluado);

    });

}
//#endregion


//#region SET Asignar Datos de Evaluado
function fnSetDatosEvaluado(lista) {

    $('#sNombres').val(lista[0].Nombres);
    $('#sApellidos').val(lista[0].Apellidos);
    //Tiempo en el puesto
    $('#nTiempoPuesto').val(lista[0].TiempoPuesto);

}
//#endregion


//#region GET Cargar Datos de Monitoreo
async function fnCargarDatosMonitoreo() {

    let url = apiUrlsho + `/api/ho_get_monitoreo_evaluar?code=954FWrzAYNSsRlSx8D6aCbN8amsaVW9OGkGvzsaPvSmz1Ce9tQCkCg==&AccionBackEnd=ListaDatosMonitoreo&IdMonitoreo=` + IdMonitoreoGlobal;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        listaDatosMonitoreo = response.lista_DatosMonitoreo
        fnSetDatosMonitoreo();

    });

}
//#endregion


//#region SET Asignar Datos de Monitoreo
function fnSetDatosMonitoreo() {

    $('#sCodEvaluacion').val(listaDatosMonitoreo[0].CodigoEvaluacion);
    $('#sCodTareaMonitoreo').val(listaDatosMonitoreo[0].CodigoTareaMonitoreo);
    $("#dat_gerencia option[value='" + listaDatosMonitoreo[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + listaDatosMonitoreo[0].IdUnidadSede + "']").prop("selected", true);
    $("#dat_etapa option[value='" + listaDatosMonitoreo[0].IdEtapa + "']").prop("selected", true);

    if (IdTipoEvaluacion != 2 && IdTipoEvaluacion != 3) {
        $("#dat_area option[value='" + listaDatosMonitoreo[0].IdArea + "']").prop("selected", true);
        $("#dat_puesto option[value='" + listaDatosMonitoreo[0].IdPuestoCargo + "']").prop("selected", true);
    }
    if (IdTipoEvaluacion != 1) {
        $("#dat_zona option[value='" + listaDatosMonitoreo[0].IdZona + "']").prop("selected", true);
    }

    if (listaDatosMonitoreo[0].FechaProgramacion != "") {
        $('#dFechaProgramacion').val(listaDatosMonitoreo[0].FechaProgramacion);
    }

}
//#endregion


//#region Deshabilitar Controles (Solo Ver)
function fnFormDisabled() {

    document.getElementById('fieldset2').disabled = true;

    document.getElementById('dFechaRegistro').disabled = true;
    document.getElementById('sCodEvaluacion').disabled = true;
    document.getElementById('sCodTareaMonitoreo').disabled = true;
}
//#endregion


//#region Funcionalidad Anular Evaluacion
async function fnAnularEvaluacion(CodAccion) {

    var EstadoDestino = 3;

    await Swal.fire({
        title: "Anular control",
        html: `
        <style>
            .swal2-html-container{
                overflow: hidden;
            }
        </style>
        <p>Está por anular el control, ingrese el motivo</p> 
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-12">
                <div class="input-box-secondary">
                    <span>Motivo de anulación</span>
                    <select class="submit" name="" id="dat_motivo" onchange="onChangeMotivoSelect" required>
                        <option value="0"></option>
                        <option value="1">Equipo descalibrado</option>
                        <option value="2">Resultado inexacto</option>
                        <option value="3">Interferencia del evaluado</option>
                        <option value="4">No se evaluó en fecha</option>
                        <option value="5">Otros</option>
                    </select>
                    <div class="invalid-feedback" id="divErrorMotivo" >Requerido!</div>
                </div>
                
            </div>
            <div class="col-md-2"></div>
        </div>
    
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <span>Escribir</span>
                <div class="input-box-secondary">                    
                    <textarea class="form-control" id="sMotivoEscribir" rows="3" required></textarea>                    
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
               
      <p class="mt-5">¿Desea confirmar la acción?</p>
           
      ` ,
        icon: "info",
        didRender: $('#divErrorMotivo').hide(),
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonColor: "#ff3636",
        confirmButtonColor: "#8fbb02",
        confirmButtonText: `Confirmar <img class="inject-svg" src="./images/sho/confirm.svg" alt="" fill="#fff" width="18px">`,
        cancelButtonText: `Cancelar <img class="inject-svg" src="./images/sho/cancelar.svg" alt="" fill="#fff" width="18px">`,
        preConfirm: () => {

            MotivoAnulacionSeleccionado = document.getElementById('dat_motivo').value
            MotivoAnulacionEscribir = document.getElementById('sMotivoEscribir').value

            if (MotivoAnulacionSeleccionado == 0) {
                $('#divErrorMotivo').show();
                $("#dat_motivo").on('change', onChangeMotivoSelect);
                return false;
            }

            else {
                $('#divErrorMotivo').hide();

                return [
                    document.getElementById('sMotivoEscribir').value,
                    document.getElementById('dat_motivo').value
                ]
            }
        }
    }).then((result) => {

        if (result.isConfirmed) {

            fnCRUDEvaluacionMonitoreo(CodAccion, EstadoDestino);

        }

    });


}
//#endregion


//#region SET Asignar Motivos de Anulacion
async function fnSetMotivos() {
    let contenido = '';

    //#region Cargando Select de Motivo
    contenido = $('#dat_motivo_select');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    objSelect["MotivosAnulacion"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.MotivoAnulacion}</option>`)
    });
    //#endregion
}
//#endregion


//#region CHANGE Cambiar Motivo de Anulacion
function onChangeMotivoSelect() {

    var localMotivo = document.getElementById('dat_motivo').value;

    if (localMotivo == 0) {
        $('#divErrorMotivo').show()
    }
    else {
        $('#divErrorMotivo').hide()
    }
}
//#endregion


//#region SET Asignar Datos Motivos de Anulacion
function fnSetDatosMotivosAnulacion(lista) {
    $('#sMotivoAnulacion').val(lista[0].Mensaje);
    $("#dat_motivo_select option[value='" + lista[0].MensajeSeleccionado + "']").prop("selected", true);

}
//#endregion


//#region SET Opciones de Fila
function OpcionesTable(nFila) {
    let html_tbody = '';

    html_tbody += "<div class='dropdown dropleft'>";
    html_tbody += "<a class='nav-link text-vertical' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
    html_tbody += "...";
    html_tbody += "</a>";
    html_tbody += "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";
    html_tbody += "<a class='dropdown-item' onclick='Sp5controlNavegacionHigiene(202);";
    html_tbody += "fnCerrarFila(" + nFila + ");";
    html_tbody += "' style='cursor: pointer;'>";
    html_tbody += "<img src='./images/sho/trazado1379.svg' alt='Cerrar' fill='#4798c7'>";
    html_tbody += "&nbsp;&nbsp; Cerrar";
    html_tbody += "</a>";
    html_tbody += "<a class='dropdown-item' onclick='Sp5controlNavegacionHigiene(202);";
    html_tbody += "fnAnularFila(" + nFila + ");";
    html_tbody += "' style='cursor: pointer; '>";
    html_tbody += "<img src='./images/sho/trazado1383.svg' alt='Anular' fill='#d8801f'>";
    html_tbody += "&nbsp;&nbsp; Anular";
    html_tbody += "</a>";
    html_tbody += "</div>";
    html_tbody += "</div>";

    return html_tbody;
}
//#endregion


//#region SET Opciones Cumplimiento de Normativa
function OpcionesCumplimientoNormativa() {

    contenido = ''
    contenido += `<option value=""></option>`;

    objSelect["CumplimientoNormativas"].forEach((Item) => {
        contenido += `<option value="${Item.Id}">${Item.CumplimientoNormativa}</option>`
    });

    return contenido;
}
//#endregion


//#region SET Opciones Nivel Riesgo
function OpcionesNivelRiesgo() {

    contenido = ''
    contenido += `<option value=""></option>`;

    objSelect["NivelesRiesgo"].forEach((Item) => {
        contenido += `<option value="${Item.Id}">${Item.NivelRiesgo}</option>`
    });

    return contenido;
}
//#endregion


//#region CHANGE Cambiar EPP
function onChangeEPP() {
    var IdEPP = $("#dat_EPP option:selected").val();

    if (IdEPP == 3) {
        $('#divDobleProteccion').show();
    }
    else {
        $('#divDobleProteccion').hide();
    }
}
//#endregion

