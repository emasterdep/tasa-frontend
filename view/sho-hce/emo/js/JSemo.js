function cargarFechaHoy() {

  var fecha = new Date(); //Fecha actual
  var mes = fecha.getMonth()+1; //obteniendo mes
  var dia = fecha.getDate(); //obteniendo dia
  var ano = fecha.getFullYear(); //obteniendo año
  
  if(dia<10){
    dia='0'+dia; //agrega cero si el menor de 10
  }
  
  if(mes<10){
    mes='0'+mes //agrega cero si el menor de 10
  }

  return (ano+"-"+mes+"-"+dia);

}

//--------------------------FUNCION CARGAR GRUPOS SANGUINEOS------------------------------//

function ecSp4CargarGruposSanguineoEMO() {
  
  //Se especifican los parametros para la consulta
  let url =  apiUrlsho+`/api/hce_Get_022_grupo_sanguineo?code=IhNvDEFrj7QvaNdVB0u1lg32UwyNstFuM75/VlxeAMn2HMJ2SZ9E7g==`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "dataType": 'json',
    "headers": headers,
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done((response) => {

    pluck_EMO["GrupoSanguineo"] = response.GrupoSanguineo;

  });

}

//--------------------------FUNCION CARGAR SEDES AREAS GERENCIAS------------------------------//

function ecSp4CargaSedesAreasGerenciasEMO() {

  //Se especifican los parametros para la consulta
  var url = apiUrlssoma + "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0"
  
  var headers = {
    "apikey": constantes.apiKey
  }

  var settings = {
    "url": url,
    "method": "GET",
    "timeout": 0,
    "crossDomain": true,
    "dataType": "json",
    "headers": headers,
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done(function (response) {

    pluck_EMO["Areas"] = response.Area;
    pluck_EMO["Gerencias"] = response.Gerencia;
    pluck_EMO["Sedes"] = response.Sedes;

  });

}

//--------------------------FUNCION CARGAR SECCION AFECTADA------------------------------//

function ecSp4cargarSeccionAfectadaEMO() {


  let url = apiUrlsho+`/api/hce_Get_035_seccion_afectada?code=7waSycbZnJkX12509pMvTJINkreJkBBQCrsMZNYeY6R5AayCauB5nQ==`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {

    pluck_EMO['SeccionAfectada'] = response.SeccionAfectada;

  });

}

//--------------------------FUNCION CARGAR SISTEMA AFECTADO------------------------------//

function ecSp4cargarSistemaAfectadoEMO() {


 let url = apiUrlsho+`/api/hce_Get_034_sistema_afectado?code=qxkQ9ZXSLatmcpaCr044BU0xgBGcMRX899c8sjilL/dCTIgHf4HyVg==`;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {

    pluck_EMO['SistemaAfectado'] = response.SistemaAfectado;

  });

}

///////////////////////////////////////////////////////////////////////////////////////////

function downloadBlob(blob, filename){

  if(window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveBlob(blob, filename);
  } else {
      var elem = window.document.createElement('a');
      elem.href = window.URL.createObjectURL(blob);
      elem.download = filename;
      document.body.appendChild(elem);
      elem.click();
      document.body.removeChild(elem);
  }

}

function fnSp4descargarAdjuntoEMOFicha(indexObj)
{
  
  url = paObj_descargarAdjunto_Ficha[indexObj].ArchivoBase64;

  fetch(url)
  .then(res => res.blob())
  .then(function(blob) {
    downloadBlob(blob, paObj_descargarAdjunto_Ficha[indexObj].NombreArchivo);
  });
    
}

function fnSp4descargarAdjuntoEMO(indexObj)
{
  
  url = paObj_descargarAdjunto_EMO[indexObj].ArchivoBase64;

  fetch(url)
  .then(res => res.blob())
  .then(function(blob) {
    downloadBlob(blob, paObj_descargarAdjunto_EMO[indexObj].NombreArchivo);
  });
    
}