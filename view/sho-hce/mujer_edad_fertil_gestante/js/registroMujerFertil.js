function _init(){
    cargarDatos()
    lisstaCombosGestionAll()
    localStorage.setItem('combosGestionAll','')
    localStorage.setItem('codigoGesSeleccionado','')
    localStorage.setItem('listaElegir','')
    getRiesgosAgente()
    setTimeout(function (){
        mujerFertilInit()
        cargarDatosEdicion()
    },1000)
}

function cargarDatosEdicion(){
    let datos =  JSON.parse(localStorage.getItem('mujerFertilEditar'))
    datos.GesConfirmados.forEach(confirmado => {
        DATA_MUJER_FERTIL.GesConfirmados.push(new GesConfirmados(confirmado.Id,confirmado.GES_ID,confirmado.idAgente,confirmado.idRiesgo,confirmado.Observaciones,confirmado.EfectoSalud,confirmado.CalculoRiesgo,confirmado.ClasificacionRiesgo))
    })
    getTablaRegistro()
}

function simpleTemplatingConfirmados(data){
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.EfectoSalud}</td>
                <td>${item.CalculoRiesgo}</td>
                <td>${item.ClasificacionRiesgo}</td>
                <td></td>
                <td><input type="text" value="${item.Observaciones}" class="form-control" id="observacion-${index}" style="border: 1px solid #ced4da !important;" onkeyup="observaciones(${index})"></td>
                <td>
                    <img src="images/iconos/trash.svg" style="width: 18px; margin-right: 12px" onclick="eliminarItemConfirmada(${index})">               
                </td>   
            </tr>
        `
        });
    }else{
        html+= `<tr>
                    <td colspan="6" class="text-center">No se ha encontrado registro</td>
                </tr>`
    }

    return html;
}

function lisstaCombosGestionAll(){
    var settings = {
        url: `${apiUrlsho}/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaCombosGestion_All`,
        method: "GET",
        dataType: 'json',
        headers: {
            "apikey": constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        response.lista_Riesgos.forEach(riesgos => {
            $('#riesgo_lista').append(`<option value="${riesgos.Id}">${riesgos.Riesgo}</option>`)
            $('#riesgo_lista_no_monitoreados').append(`<option value="${riesgos.Id}">${riesgos.Riesgo}</option>`)
        })
        localStorage.setItem('combosGestionAll',JSON.stringify(response))
    });
}

function getRiesgosAgente(){

    var settings = {
        url: `${apiUrlsho}/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=BandejaDetalle_Gestion`,
        method: "GET",
        dataType: 'json',
        headers: {
            "apikey": constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log('getRiesgp',response);
        localStorage.setItem('listaElegir',JSON.stringify(response.lista_GestionData))
        $('#pagination_table_elegir_riesgo_agente').pagination({
            dataSource: response.lista_GestionData,
            callback: function(data, pagination) {
                var html = simpleTemplatingElegir(data);
                $('#content_riesgo_agente').html(html);
            }
        })
    });
}

function simpleTemplatingElegir(data){
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.Riesgo}</td>
                <td>${item.AgenteDescripcion}</td>
                <td>${item.GES_ID}</td>
                <td>${item.TiempoExp}</td>
                <td>${item.Dosis}</td>
                <td>${item.ValoracionDosis}</td>
                <td>${item.EfectoSalud}</td>
                <td>${item.CalculoRiesgo}</td>
                <td>
                    <img src="images/iconos/plus_verde.svg" style="width: 18px; margin-right: 12px" onclick="agregarListaConfirmada(${item.Id})">               
                </td>
            </tr>
        `
        });
    }else{
        html +=` <tr>
            <td colSpan="9" className="text-center" style="text-align: center">No se ha encontrado registro</td>
        </tr>`
    }
    return html;
}

function buscarAgente(){
    $('#agente_lista').html('')
    let combosGestionAll = JSON.parse(localStorage.getItem('combosGestionAll'))
    let valorRiesgo = $('#riesgo_lista').val();

    const agentes = combosGestionAll.lista_Agentes.filter(agente => {
        return agente.idRiesgoPuestoTrabajo == valorRiesgo
    })
    $('#agente_lista').append(`<option disabled selected>seleccionar una opción</option>`)
    agentes.forEach(agente => {
        $('#agente_lista').append(`<option value="${agente.Id}">${agente.Agentes}</option>`)
    })

    let listaElegir = JSON.parse(localStorage.getItem('listaElegir'));

    let datos = listaElegir.filter(item => {
        return item.idRiesgo == valorRiesgo
    })

    $('#pagination_table_elegir_riesgo_agente').pagination({
        dataSource: datos,
        callback: function(data, pagination) {
            var html = simpleTemplatingElegir(data);
            $('#content_riesgo_agente').html(html);
        }
    })
}

function buscarRegistro(){
    let valorRiesgo = $('#riesgo_lista').val();
    let valorAgente = $('#agente_lista').val();

    let listaElegir = JSON.parse(localStorage.getItem('listaElegir'));

    let datos = listaElegir.filter(item => {
        return item.idRiesgo == valorRiesgo && item.idAgente == valorAgente
    })

    $('#pagination_table_elegir_riesgo_agente').pagination({
        dataSource: datos,
        callback: function(data, pagination) {
            var html = simpleTemplatingElegir(data);
            $('#content_riesgo_agente').html(html);
        }
    })
}

function buscarAgenteNoMonitoreados(){
    $('#agente_lista_no_monitoreados').html('')
    let combosGestionAll = JSON.parse(localStorage.getItem('combosGestionAll'))
    let valorRiesgo = $('#riesgo_lista_no_monitoreados').val();

    const agentes = combosGestionAll.lista_Agentes.filter(agente => {
        return agente.idRiesgoPuestoTrabajo == valorRiesgo
    })

    agentes.forEach(agente => {
        $('#agente_lista_no_monitoreados').append(`<option value="${agente.Id}">${agente.Agentes}</option>`)
    })
}


function buscarCodigoGes(){
    let codigo_ges = $('#codigo_ges').val()
    $('#lista-buscador').html('')
    var settings = {
        url: `${apiUrlsho}/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaGestionByPatron&IdGestion=${codigo_ges}`,
        method: "GET",
        dataType: 'json',
        headers: {
            "apikey": constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        response.Lista_gestion_data.forEach(codigos => {
            $('#lista-buscador').append(`<li onclick="codigoSeleccionado('${codigos.GES_ID}',${codigos.Id})" class="item-buscador">${codigos.GES_ID}</li>`)
        })
    });

    // BUSCAR LISTA POR CODIGO GES
    filtroGes()

}

function filtroGes(){
    let valorRiesgo = $('#riesgo_lista').val();
    let valorAgente = $('#agente_lista').val();
    let codigo_ges = $('#codigo_ges').val();

    let listaElegir = JSON.parse(localStorage.getItem('listaElegir'));

    let datos = []

    if(valorRiesgo == null){
        datos = listaElegir.filter(item => {
            return item.idRiesgo == valorRiesgo && item.GES_ID == codigo_ges
        })
    }else if(valorAgente == null && valorRiesgo != null){
        console.log('entrando.....')
        datos = listaElegir.filter(item => {
            const itemData = item.GES_ID ? item.GES_ID.toUpperCase() : ''.toUpperCase();
            const textData = codigo_ges.toUpperCase()
            return item.idRiesgo == valorRiesgo && itemData.indexOf(textData) > -1
        })
    }else if(valorRiesgo != null || valorAgente != null){
        console.log('entrando..... 2222')
        datos = listaElegir.filter(item => {
            const itemData = item.GES_ID ? item.GES_ID.toUpperCase() : ''.toUpperCase();
            const textData = codigo_ges.toUpperCase()
            return item.idRiesgo == valorRiesgo && item.idAgente == valorAgente && itemData.indexOf(textData) > -1
        })
    }

    console.log(datos)
    $('#pagination_table_elegir_riesgo_agente').pagination({
        dataSource: datos,
        callback: function(data, pagination) {
            var html = simpleTemplatingElegir(data);
            $('#content_riesgo_agente').html(html);
        }
    })
}

function codigoSeleccionado(codigo,id){
    $('#lista-buscador').html('')
    localStorage.setItem('codigoGesSeleccionado',id)
    $('#codigo_ges').val(codigo)
    filtroGes()
}

function agregarListaConfirmada(Id){
    let listaElegir = JSON.parse(localStorage.getItem('listaElegir'));
    let dato = listaElegir.filter(item => {
        return item.Id == Id
    })
    DATA_MUJER_FERTIL.GesConfirmados.push(new GesConfirmados(0,dato[0].GES_ID,dato[0].idAgente,dato[0].idRiesgo,'',dato[0].EfectoSalud,dato[0].CalculoRiesgo,dato[0].ClasificacionRiesgo))
    getTablaRegistro()
}

function getTablaRegistro(){
    $('#content_riesgo_agentecon_firmada').html('')
    $('#pagination_table_riesgo_agente_confirmada').pagination({
        dataSource: DATA_MUJER_FERTIL.GesConfirmados,
        callback: function(data, pagination) {
            var html = simpleTemplatingConfirmados(data);
            $('#content_riesgo_agentecon_firmada').html(html);
        }
    })
}

function mujerFertilInit(){
    DATA_MUJER_FERTIL = null
    let mujerFertilEditar = JSON.parse(localStorage.getItem('mujerFertilEditar'));
    console.log(mujerFertilEditar)
    DATA_MUJER_FERTIL = new mujerFertil(mujerFertilEditar.Id,mujerFertilEditar.HCId,mujerFertilEditar.CreadoPor,mujerFertilEditar.ModificadoPor,mujerFertilEditar.MedicoCargo)
}

function observaciones(index){
    let texto = $('#observacion-'+index).val()
    DATA_MUJER_FERTIL.GesConfirmados[index].Observaciones = texto
}

function eliminarItemConfirmada(index){
    DATA_MUJER_FERTIL.GesConfirmados.splice(index,1)
    getTablaRegistro()
}

function AdjuntarArchivo(id,tipo){
    let file = $(`#${id}`)[0].files[0];
    const date = new Date();
    let CreadoFecha = `${date.getDate()}/${((parseInt(date.getMonth()) + 1) < 10) ? '0'+(parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1}/${date.getFullYear()}`
    var reader = new FileReader();
    reader.onloadend = function () {
        console.log('render '+reader.result)
        DATA_MUJER_FERTIL.Adjuntos.push(new Adjuntos(file.name,reader.result,tipo,CreadoFecha))
        getTableAdjuntar()
    }
    reader.readAsDataURL(file);
}

function getTableAdjuntar(){
    if(DATA_MUJER_FERTIL.Adjuntos.length > 0){
        $('#content_table_adjuntos').removeClass('d-none')
    }else{
        $('#content_table_adjuntos').addClass('d-none')
    }
    $('#table_adjuntos-count-row').html(DATA_MUJER_FERTIL.Adjuntos.length)
    $('#content_adjuntos').html('')
    $('#pagination_table_riesgo_agente_confirmada').pagination({
        dataSource: DATA_MUJER_FERTIL.Adjuntos,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjuntos(data);
            $('#content_adjuntos').html(html);
        }
    })
}

function simpleTemplatingAdjuntos(data){
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.NombreArchivo}</td>
                <td>${item.CreadoFecha}</td>
                <td>
                    <button onclick="eliminarAdjunto(${index})" id="hc_btn_borrar_evidencia_31" type="button" class="btn btn-link shadow-none float-right">
                      <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                    </button>      
                    <a class="btn btn-link shadow-none float-right" href="${item.Archivo}" download="${item.NombreArchivo}">
                      <img class="inject-svg" src="./images/sho/download.svg" fill="#207345" style="width:16px !important">
                    </a>
                </td>
            </tr>
        `
        });
    }else{
        html +=` <tr>
            <td colSpan="9" className="text-center" style="text-align: center">No se ha encontrado registro</td>
        </tr>`
    }
    return html;
}

function eliminarAdjunto(index){
    DATA_MUJER_FERTIL.Adjuntos.splice(index,1)
    getTableAdjuntar()
}

function Adjuntos(NombreArchivo, Archivo, Tipo,CreadoFecha){
    this.Id = 0
    this.NombreArchivo = NombreArchivo
    this.Archivo = Archivo
    this.Tipo = Tipo
    this.CreadoFecha = CreadoFecha
    this.Activo = 1
}

function mujerFertil(Id,HCId,CreadoPor,ModificadoPor,MedicoCargo){
    this.Id = Id
    this.HCId = HCId
    this.CreadoPor = CreadoPor
    this.ModificadoPor = ModificadoPor
    this.MedicoCargo = MedicoCargo
    this.Activo = 1
    this.Adjuntos = []
    this.GesConfirmados = []
}

function GesConfirmados(Id,GesId,AgenteId,RiesgoId,Observaciones,EfectoSalud,CalculoRiesgo,ClasificacionRiesgo){
    this.Id = Id
    this.GesId = GesId
    this.AgenteId = AgenteId
    this.RiesgoId = RiesgoId
    this.Observaciones = Observaciones
    this.EfectoSalud = EfectoSalud
    this.CalculoRiesgo = CalculoRiesgo
    this.ClasificacionRiesgo = ClasificacionRiesgo
}

function guardarMujerFertil(){

    let datosMujerFertil = JSON.parse(localStorage.getItem('mujerFertilEditar'))
    let datosHc = paObj_hc[idHC].a
    var settings = {
        url: `${apiUrlsho}/api/hce_post_mfg?code=9HopqtVa5opysDxYNERBEeWmDBa/ZtdYnClUJbx1aWLhRzJxlakbzQ==&httpmethod=postmf&Id=${datosMujerFertil.Id}`,
        method: "POST",
        dataType: 'json',
        headers: {
            "apikey": constantes.apiKey,
            "Content-Type": "application/json"
        },
        data: JSON.stringify(DATA_MUJER_FERTIL)
    };

    Swal.fire({
        title: 'Guardar registro de mujeres fértiles',
        html: `<p>Está por guardar el registro de ${datosHc.Nombres_Trabajador_H} ${datosHc.Apellidos_Trabajador_H}</p> <p class="mt-4">¿Desea confirmar la acción?</p>`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#8fbb02',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar <img src="./images/sho/confirm.svg" />',
        cancelButtonText: 'Cancelar <img src="./images/sho/cancelar.svg" />',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax(setting).done(response => {
                Swal.fire({
                    title: 'Se guardó el registro con éxito',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 2500
                })

                setTimeout(function (){
                    atrasBack()
                },2600)

            })
        }
    })

}