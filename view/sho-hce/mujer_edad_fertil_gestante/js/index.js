$('#mostrar_datos_dg').on('click',onClickMostrarDatos);
$('#ocultar_datos_dg').on('click',onClickOcultarDatos);

function _init(){
    $('#datos_hc_general_trabajador').hide()
    localStorage.setItem('mujerFertilEditar',"")
    datosTab()
}

function onClickMostrarDatos(){
    $('#mostrar_datos_dg').hide()
    $('#ocultar_datos_dg').show()
    // $('.show-less').hide()
    $('#datos_hc_general_trabajador').show()
}

function onClickOcultarDatos(){
    $('#ocultar_datos_dg').hide()
    $('#mostrar_datos_dg').show()
    // $('.show-less').show()
    $('#datos_hc_general_trabajador').hide()
}


function cargarDatos(){
    let dni = paObj_hc[idHC].a.NroDocumento_Trabajador_H;
    var settings = {
        url: `${apiUrlsho}/api/hce_Get_001_historia_clinica?code=5lJWTsRDsoxqo9VoOxIzfQAPyCTxUTqLpvgY5tuHluCZlSodpQ/Y7w==&AreaId_Empresa_H=0&SedeId_Empresa_H=0&NroDocumento_Trabajador_H=${dni}&Buscador=`,
        method: "GET",
        dataType: "json",
        timeout: 0,
        headers: {
            "apikey": constantes.apiKey,
            "Content-Type": "application/json"
        },
    };

    $.ajax(settings).done(response => {
        let datos = response.HistoriaClin[0]

        console.log(datos)

        $('#dat_am_dni_trabajador').val(datos.NroDocumento_Trabajador_H);
        $('#dat_am_nombres_trabajador').val(datos.Nombres_Trabajador_H);
        $('#dat_am_apellidos_trabajador').val(datos.Apellidos_Trabajador_H);
        $('#dat_am_edad_trabajador').val(datos.Edad_Trabajador_H);
        $('#dat_am_puesto_trabajo_trabajador').val(datos.PuestoTrabajo_Empresa_H)
        $('#input_fecha_registro').val(datos.FechaRegistro_Trabajador_H.split("T")[0])

        sedeAreaGerencia.Sedes.forEach((e) => {
            if (e.Id == datos.PlantaId_Empresa_H) {
                $('#dat_am_planta_trabajador').append(`<option>${e.Description}</option>`);
            }
        });

        sedeAreaGerencia.Area.forEach((e) => {
            if (e.Id == datos.AreaId_Empresa_H) {
                $('#dat_am_area_trabajador').append(`<option>${e.Description}</option>`);
            }
        });

        sedeAreaGerencia.Gerencia.forEach((e) => {
            if (e.Id == datos.GerenciaId_Empresa_H) {
                $('#dat_am_gerencia_trabajador').append(`<option>${e.Description}</option>`);
            }
        });


    })
}

function VerDatosCompletoTrabajador() {
    //let datos = paObj_ec[IdHC].a;
    let url = apiUrlsho+`/api/hce_Get_600_historia_clinica_datos_generales_completo?code=x65sNwDvhbkdVQk0BvFAlVp7TVvemyvLlzYet9HXKgv5H2Fb4xXaUg==`;
    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }
    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": { "IdHC": idHC }
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done(response => {
        console.log(response)
        let datos = response.HistoriaClin[0]

        sedeAreaGerencia.Sedes.forEach((e) => {
            if (e.Id == datos.SedeId_Empresa_H) {
                datos.SedeDescripcion = e.Description
            }
        });

        sedeAreaGerencia.Area.forEach((e) => {
            if (e.Id == datos.AreaId_Empresa_H) {
                datos.AreaDescripcion = e.Description
            }
        });
        //Se carga el modal con los datos completos del trabajador
        // let datos = paObj_ec[idHC_EC];
        Swal.fire({
            title: "Datos del trabajador",
            html: `
      <div class="text-left">
        <div class="row my-3">
          <div class="col-12">
            <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos Principales</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Documento: </b></span>
            <span>${datos.NroDocumento_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Nombres: </b></span>
            <span>${datos.Nombres_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Apellidos: </b></span>
            <span>${datos.Apellidos_Trabajador_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>C.Colaborador: </b></span>
            <span>${datos.CodigoColaborador_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Telefono: </b></span>
            <span>${datos.Telefono_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Sexo: </b></span>
            <span>${(datos.Sexo_Trabajador_H == 1) ? 'Masculino' : 'Femenino'}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-12">
            <span style="color: #254373"><b>Dirección: </b></span>
            <span>${datos.Direccion_Trabajador_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>F.Nacimiento: </b></span>
            <span>${datos.fecha_nacimiento}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Edad: </b></span>
            <span>${datos.Edad_Trabajador_H}</span>
          </div>
        </div>
        <hr class="my-4">
        <div class="row my-3">
          <div class="col-12">
            <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos de la empresa</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Sede: </b></span>
            <span>${datos.SedeDescripcion}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Área: </b></span>
            <span>${datos.AreaDescripcion}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Cargo: </b></span>
            <span>${datos.CargoJefe_Empresa_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Jefe inmediato: </b></span>
            <span>${datos.JefeInmediato_Empresa_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Celular: </b></span>
            <span>${datos.Celular_Empresa_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Telefono: </b></span>
            <span>${datos.Telefono_Empresa_H}</span>
          </div>
        </div>
      </div>            
    `,
            iconHtml: '<img src="./images/sho/perfil.svg">',
            width: 800,
            showCancelButton: true,
            showConfirmButton: false,
            cancelButtonColor: "#ff3636",
            cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
        })
        $('.swal2-cancel').css('width', '200px');
        $('.swal2-html-container').css('overflow', 'visible');
    })

}

function datosTab(){
    var settings = {
        url:`${apiUrlsho}/api/hce_get_mfg?code=twHBW4XOISiNXPXs3grIF/hvnQC2GObfXVIsc/IF4PBPLflgRaX0Zw==&httpmethod=objectlist&HCId=${idHC}`,
        method: "GET",
        dataType: "json",
        headers: {
            "apikey": constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        $('#pagination-container').pagination({
            dataSource: response.MujerFertil,
            callback: function(data, pagination) {
                var html = simpleTemplating(data);
                $('#content_edad_fertil').html(html);
            }
        })
        if(response.MujerGestante.length == 0){

        }
    });
}

function simpleTemplating(data) {
    var html = '<tr>';
    $.each(data, function(index, item){
        html += `
            <td>${item.CreadoFecha}</td>
            <td>${item.CreadoFecha}</td>
            <td>${item.A_Edad}</td>
            <td>${item.ActivoDescripcion}</td>
            <td>
                <div class="dropdown float-right dropleft">
                    <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                        <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                    </div>
                    <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                        <ul>
                            <li onclick="editarEdadFertil(${item.Id})">
                                <img src="./images/sho/edit.svg" width="16" height="16">
                                <span>Editar</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </td>   
        `
    });
    html += '</tr>';
    return html;
}

function madreGestante(){
    $('#btn-registro').toggleClass('d-none')
}

function nuevoRegistroMadreGestante(){
    actualPageSystemSoma = "view/sho-hce/mujer_edad_fertil_gestante/registro_mujer_gestante.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/mujer_edad_fertil_gestante/registro_mujer_gestante.html', 'Registro de mujer en edad fértil');
    $("#logoCompany1 b").text('Registro de mujer en edad fértil');
}

function editarEdadFertil(id){
    MUJER_EDAD_FERTIL_GESTANTE = true
    actualPageSystemSoma = "view/sho-hce/mujer_edad_fertil_gestante/registroMujerFertil.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/mujer_edad_fertil_gestante/registroMujerFertil.html', 'Registro de mujer en edad fértil');
    $("#logoCompany1 b").text('Registro de mujer en edad fértil');

    var settings = {
        url: `${apiUrlsho}/api/hce_get_mfg?code=twHBW4XOISiNXPXs3grIF/hvnQC2GObfXVIsc/IF4PBPLflgRaX0Zw==&httpmethod=objectmf&MFId=${id}`,
        method: "GET",
        dataType: "json",
        headers: {
            "apikey": constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        localStorage.setItem('mujerFertilEditar',JSON.stringify(response))
    });
}

async function amSp3NuevaAtencionMedica(IdHC, id) {
    idHC = IdHC;
    idAM = 0;
    tipoVerAtencion = 1;
    MUJER_EDAD_FERTIL_GESTANTE = true

    if (id == 1) {
        actualPageSystemSoma =
            "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
        backPageSystemSoma =
            "view/sho-hce/historia_clinica/gestionHistoriaClinica.html";
        backTitle = "Historia clínica electrónica";
    }

    if (id == 2) {
        actualPageSystemSoma =
            "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
        backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
        backTitle = "Historia clínica";
    }

    handlerUrlhtml(
        "contentGlobal",
        "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html",
        "Nueva atención médica"
    );
    $("#regresar").show();
    $("#logoCompany1 b").text("Nueva atención médica");
}
