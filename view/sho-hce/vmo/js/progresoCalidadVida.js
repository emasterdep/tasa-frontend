function _initProgreso(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    cargarDatos()
    getProgreso()
}

function getProgreso(){
    let EmoId = localStorage.getItem('idVMO');
    var settings = {
        url: apiUrlsho+`/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=progresscv&EmoId=${EmoId}`,
        method: "GET",
        timeout: 0,
        dataType: "json",
        headers: {
            "apikey": constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log('log',response);
        response.forEach(item => {
            $('#table_progreso-count-row').html(response.length)
            $('#table_hospitalizacion thead tr').append(`<th>${item.CreadoFecha}</th>`)
            $('#precion-arterial').append(`<td>${item.PresionArterial} / ${item.PresionArterialDiastole} </td>`)
            $('#peso').append(`<td>${item.Peso} Kg</td>`)
            $('#glucosa').append(`<td>${item.Glucosa}</td>`)
            $('#dislipidemia').append(`<td>${(item.Dislipidemias == 1) ? 'Si' : 'No' }</td>`)
            $('#colesterol').append(`<td>${item.Colesterol}</td>`)
            $('#trigliceridos').append(`<td>${item.Trigliceridos}</td>`)
            $('#imc').append(`<td>${item.IMC.toFixed(2)}</td>`)
            $('#perimetro-abdominal').append(`<td>${item.PerimetroAbdominal} cm</td>`)
            $('#hdl').append(`<td>${item.HDL}</td>`)
            $('#ldl').append(`<td>${item.LDL}</td>`)
            $('#riesgo-cardiaco-vascular').append(`<td>${(item.RiesgoCardioVascularDescripcion == null) ? '' : item.RiesgoCardioVascularDescripcion}</td>`)
        })
    });
}