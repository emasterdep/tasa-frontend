function _init(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    cargarDatos()
    generarData()
    cei10()
}

function getDatosConsultado(){
    let id = localStorage.getItem('idRegistroVMO')

    var settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=objecttmea&TranstornoMEAId="+id,
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);

        if(response.ETCadmio == 1){
            $('#ETCadmio-si').addClass('active-si')
            $('#ETCadmio-no').removeClass('active-no')
        }

        if(response.ETMercurio == 1){
            $('#ETMercurio-si').addClass('active-si')
            $('#ETMercurio-no').removeClass('active-no')
        }

        if(response.ETManganeso == 1){
            $('#ETManganeso-si').addClass('active-si')
            $('#ETManganeso-no').removeClass('active-no')
        }

        if(response.EOManganeso == 1){
            $('#EOManganeso-si').addClass('active-si')
            $('#EOManganeso-no').removeClass('active-no')
        }

        if(response.EOEquilibrio == 1){
            $('#EOEquilibrio-si').addClass('active-si')
            $('#EOEquilibrio-no').removeClass('active-no')
        }

        if(response.EOManipulacion == 1){
            $('#EOManipulacion-si').addClass('active-si')
            $('#EOManipulacion-no').removeClass('active-no')
        }

        if(response.EFExige == 1){
            $('#EFExige-si').addClass('active-si')
            $('#EFExige-no').removeClass('active-no')
        }

        if(response.EFExiste == 1){
            $('#EFExiste-si').addClass('active-si')
            $('#EFExiste-no').removeClass('active-no')
        }

        if(response.EFCuerpo == 1){
            $('#EFCuerpo-si').addClass('active-si')
            $('#EFCuerpo-no').removeClass('active-no')
        }

        if(response.EFAlzar == 1){
            $('#EFAlzar-si').addClass('active-si')
            $('#EFAlzar-no').removeClass('active-no')
        }

        //Condiciones de ambiente de trabajo

        if(response.CAEspacio == 1){
            $('#CAEspacio-si').addClass('active-si')
            $('#CAEspacio-no').removeClass('active-no')
        }

        if(response.CASueloIrregular == 1){
            $('#CASueloIrregular-si').addClass('active-si')
            $('#CASueloIrregular-no').removeClass('active-no')
        }

        if(response.CAAltura == 1){
            $('#CAAltura-si').addClass('active-si')
            $('#CAAltura-no').removeClass('active-no')
        }

        if(response.CAPostulas == 1){
            $('#CAPostulas-si').addClass('active-si')
            $('#CAPostulas-no').removeClass('active-no')
        }

        if(response.CADesniveles == 1){
            $('#CADesniveles-si').addClass('active-si')
            $('#CADesniveles-no').removeClass('active-no')
        }

        if(response.CASuelo == 1){
            $('#CASuelo-si').addClass('active-si')
            $('#CASuelo-no').removeClass('active-no')
        }

        if(response.CATemperatura == 1){
            $('#CATemperatura-si').addClass('active-si')
            $('#CATemperatura-no').removeClass('active-no')
        }

        if(response.CAVibraciones == 1){
            $('#CAVibraciones-si').addClass('active-si')
            $('#CAVibraciones-no').removeClass('active-no')
        }

        //Exigencia de la actividad

        if(response.EAEsfuerzos == 1){
            $('#EAEsfuerzos-si').addClass('active-si')
            $('#EAEsfuerzos-no').removeClass('active-no')
        }

        if(response.EAPeriodo == 1){
            $('#EAPeriodo-si').addClass('active-si')
            $('#EAPeriodo-no').removeClass('active-no')
        }
        if(response.EADistancia == 1){
            $('#EADistancia-si').addClass('active-si')
            $('#EADistancia-no').removeClass('active-no')
        }

        if(response.EARitmo == 1){
            $('#EARitmo-si').addClass('active-si')
            $('#EARitmo-no').removeClass('active-no')
        }

        //Medidas de protección

        if(response.MPFaja == 1){
            $('#MPFaja-si').addClass('active-si')
            $('#MPFaja-no').removeClass('active-no')
        }

        if(response.MPMedios == 1){
            $('#MPMedios-si').addClass('active-si')
            $('#MPMedios-no').removeClass('active-no')
        }

        if(response.MPDiseño == 1){
            $('#MPDiseño-si').addClass('active-si')
            $('#MPDiseño-no').removeClass('active-no')
        }

        if(response.MPPausa == 1){
            $('#MPPausa-si').addClass('active-si')
            $('#MPPausa-no').removeClass('active-no')
        }

        if(response.SimetriaMMS == 1){
            $('#SimetriaMMS').append(`<option>Anormal</option>`)
        }else{
            $('#SimetriaMMS').append(`<option>Normal</option>`)
        }

        if(response.SimetriaMMLL == 0){
            $('#SimetriaMMLL').append(`<option>Normal</option>`)
        }else{
            $('#SimetriaMMLL').append(`<option>Anormal</option>`)
        }

        $('#ComentariosExamenFisico').val(response.ComentariosExamenFisico)
        $('#ObservacionSimetriaMMS').val(response.ObservacionSimetriaMMS)
        $('#ObservacionSimetriaMMLL').val(response.ObservacionSimetriaMMLL)


        $('#ComentariosCAED').val(response.ComentariosCAED)
        $('#ComentariosDAED').val(response.ComentariosDAED)
        $('#ComentariosLAED').val(response.ComentariosLAED)

        $('#ComentariosCCM').val(response.ComentariosCCM)
        $('#ComentariosDCM').val(response.ComentariosDCM)
        $('#ComentariosLCM').val(response.ComentariosLCM)

        $('#ComentariosCervical').val(response.ComentariosCervical)
        $('#ComentariosDorsal').val(response.ComentariosDorsal)
        $('#ComentariosLumbar').val(response.ComentariosLumbar)

        $('#ComentariosPalpacion').val(response.ComentariosPalpacion)

        setDataRiesgos(response.Riesgos)
        setDataAdjunto(response)
        setDataDiagnosticoTME(response.DiagnosticosCIE10)
        setDataCapacitacion(response.Capacitaciones)
        setSintomas(response.Sintomas)
        setTest(response.Tests)
    });
}

function setDataAdjunto(response){
    const adjunto_archivo = response.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 1
    })

    const adjunto_firma = response.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 2
    })

    const adjunto_ficha = response.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 3
    })

    if(adjunto_firma.length > 0){
        $('#lista-adjunto-firma').removeClass('d-none')
    }else{
        $('#adjuntarFirma').append(`<div class="null-program">
            No hay archivos adjuntos <img src="./images/exclamation-circle.svg" width="25px" height="25px" class="ml-2"/>
        </div>`)
    }

    if(adjunto_archivo.length > 0){
        $('#lista-adjunto-archivos').removeClass('d-none')
    }else{
        $('#adjuntarArchivos').append(`<div class="null-program">
            No hay archivos adjuntos <img src="./images/exclamation-circle.svg" width="25px" height="25px" class="ml-2"/>
        </div>`)
    }

    if(adjunto_ficha.length > 0){
        $('#lista-adjunto-fichas').removeClass('d-none')
    }else{
        $('#adjuntarFicha').append(`<div class="null-program">
            No hay archivos adjuntos <img src="./images/exclamation-circle.svg" width="25px" height="25px" class="ml-2"/>
        </div>`)
    }

    $('#table_adjunto_firma-count-row').html(adjunto_firma.length)
    $('#table_adjunto-archivos-count-row').html(adjunto_archivo.length)
    $('#table_adjunto_fichas-count-row').html(adjunto_ficha.length)

    $('#adjunto_firma').pagination({
        dataSource: adjunto_firma,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_firma').html(html);
        }
    })

    $('#pagination-adjunto-archivos').pagination({
        dataSource: adjunto_archivo,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto-archivos').html(html);
        }
    })

    $('#paginacion_adjunto_fichas').pagination({
        dataSource: adjunto_ficha,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_fichas').html(html);
        }
    })
}

function setDataRiesgos(riesgos){

    riesgos.forEach((riesgo, index) => {

        $('#listaRiesgos').append(`<div class="row mb-2 align-items-center">
                    <div class="col-md-1">
                        <p class="m-0">${riesgo.RiesgoDescripcion}</p>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA ${(riesgo.FlagActivo == 1) ? 'active-si' : 'disabled'} "
                                        id="FlagActivo-${index}-1">Si</label>
                            </div>
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA ml-2 ${(riesgo.FlagActivo == 0) ? 'active-no' : 'disabled'}"
                                        id="FlagActivo-${index}-0">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <p class="m-0">Repetitivo</p>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA ${(riesgo.Repetitivo == 1) ? 'active-si' : 'disabled'}"
                                        id="Repetitivo-${index}-1">Si</label>
                                <input class="d-none" type="radio" name="hta" value="Si" id="reg_cali_vida_datos_HTA_si">
                            </div>
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA ml-2 ${(riesgo.Repetitivo == 0) ? 'active-no' : 'disabled'}"
                                        id="Repetitivo-${index}-0">No</label>
                                <input class="d-none" type="radio" name="hta" value="Si" id="reg_cali_vida_datos_HTA_no">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control input_form_control" placeholder="Horas" id="Horas-${index}" value="${riesgo.Horas}" disabled>
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control input_form_control" placeholder="Días" id="Dias-${index}" value="${riesgo.Dias}" disabled>
                    </div>
                    <div class="col-md-4">
                        <select name="" id="EvaluacionId-${index}" class="w-100 select_form" disabled>
                            <option value="">${riesgo.EvaluacionDescripcion}</option>
                        </select>
                    </div>
                </div>`)
    })
}

function simpleTemplatingAdjunto(data){
    const date = new Date();
    let CreadoFecha = `${date.getDate()}/${((parseInt(date.getMonth()) + 1) < 10) ? '0'+(parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1}/${date.getFullYear()}`
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.NombreArchivo}</td>
                <td>${CreadoFecha}</td>
                <td>
                    <a class="btn btn-link shadow-none float-right" href="${item.Archivo}" download="${item.NombreArchivo}">
                      <img class="inject-svg" src="./images/sho/download.svg" fill="#207345" style="width:16px !important">
                    </a>
                </td>
            </tr> 
        `
        });
    }else{
        html += ` <tr>
            <td className="text-center" colSpan="5" style="text-align: center">No se encontro resultados</td>
        </tr>`
    }
    return html;
}

function setDataDiagnosticoTME(DiagnosticosCIE10){

    $('#content_diagnostico').html('')
    $('#table_diagnostico-count-row').html(DiagnosticosCIE10.length)
    DiagnosticosCIE10.forEach(item => {

        const diagnostico = datosCEI.filter(dato => {
            return dato.Code == item.CIE10
        })

        console.log("diagnostico => "+diagnostico.length)

        const general1 = general.SistemasAfectados.filter(itemGeneral => {
            return itemGeneral.Id == item.SistemaAfectado
        })

        const general2 = general.SeccionesAfectadas.filter(itemGeneral => {
            return itemGeneral.Id == item.SeccionAfectada
        })
        const date = new Date()
        if(diagnostico.length != 0){
            $('#content_diagnostico').append(`<tr>
                <td>${item.Diagnostico}</td>
                <td>${item.CIE10Code}</td>
                <td>${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}/${(parseInt(date.getMonth() + 1) < 10) ? '0'+parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1)}/${date.getFullYear()}</td>
                <td>${diagnostico[0].Especilidades}</td>
                <td>${general1[0].Descripcion}</td>
                <td>${general2[0].Descripcion}</td>
            </tr>`)
        }else{
            $('#content_diagnostico').append(`<tr>
                <td>${item.Diagnostico}</td>
                <td>${item.CIE10Code}</td>
                <td>${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}/${(parseInt(date.getMonth() + 1) < 10) ? '0'+parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1)}/${date.getFullYear()}</td>
                <td>--</td>
                <td>${general1[0].Descripcion}</td>
                <td>${general2[0].Descripcion}</td>
            </tr>`)
        }

    })
}

function setDataCapacitacion(Capacitaciones){
    $('#table_capacitacion-count-row').html(Capacitaciones.length)
    console.log(Capacitaciones.length)
    $('#content_capacitacion').html('')
    Capacitaciones.forEach((capacitacion, index) => {
        $('#content_capacitacion').append(`
            <tr>
                <td>${index + 1}</td>
                <td>${capacitacion.Tema}</td>
                <td>${capacitacion.Comentario}</td>
                <td>
                    
                </td>
            </tr>    
        `)
    })
}

function cei10(){
    var settings = {
        url: apiUrlsho+"/api/hce_Get_021_CIE10_busqueda?code=Kr7q88AoJqtcFZLAx3w8cS7kZ8ezNaxCr/YUbbfUMvEQH1zUvDsxjg==&IdCIE10=&Descripcion=",
        method: "GET",
        dataType: "json"
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        datosCEI = response.CEI10
        getDatosConsultado()
    });
}

function setSintomas(Sintomas){

    Sintomas.forEach(sintoma => {

        if(sintoma.Tipo == 1){
            if( sintoma.SeccionDescripcion == "Nuca/Cuello" ){
                if(sintoma.Problema12 == 1){
                    $('#Problema12-si').addClass('active-si')
                    $('#Problema12-no').removeClass('active-no')
                }

                if(sintoma.Incapacitado == 1){
                    $('#Incapacitado-si').addClass('active-si')
                    $('#Incapacitado-no').removeClass('active-no')
                }

                if(sintoma.Problema7 == 1){
                    $('#Problema7-si').addClass('active-si')
                    $('#Problema7-no').removeClass('active-no')
                }
            }

            if(sintoma.SeccionDescripcion == "Hombro"){
                if(sintoma.Area == 1){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-1-si').addClass('active-si')
                        $('#Problema12-1-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-1-si').addClass('active-si')
                        $('#Incapacitado-1-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-1-si').addClass('active-si')
                        $('#Problema7-1-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 2){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-2-si').addClass('active-si')
                        $('#Problema12-2-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-2-si').addClass('active-si')
                        $('#Incapacitado-2-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-2-si').addClass('active-si')
                        $('#Problema7-2-no').removeClass('active-no')
                    }
                }
            }

            if(sintoma.SeccionDescripcion == "Codo"){
                if(sintoma.Area == 1){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-3-si').addClass('active-si')
                        $('#Problema12-3-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-3-si').addClass('active-si')
                        $('#Incapacitado-3-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-3-si').addClass('active-si')
                        $('#Problema7-3-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 2){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-4-si').addClass('active-si')
                        $('#Problema12-4-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-4-si').addClass('active-si')
                        $('#Incapacitado-4-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-4-si').addClass('active-si')
                        $('#Problema7-4-no').removeClass('active-no')
                    }
                }
            }

            if(sintoma.SeccionDescripcion == "Mano"){
                if(sintoma.Area == 1){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-5-si').addClass('active-si')
                        $('#Problema12-5-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-5-si').addClass('active-si')
                        $('#Incapacitado-5-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-5-si').addClass('active-si')
                        $('#Problema7-5-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 2){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-6-si').addClass('active-si')
                        $('#Problema12-6-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-6-si').addClass('active-si')
                        $('#Incapacitado-6-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-6-si').addClass('active-si')
                        $('#Problema7-6-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 3){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-7-si').addClass('active-si')
                        $('#Problema12-7-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-7-si').addClass('active-si')
                        $('#Incapacitado-7-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-7-si').addClass('active-si')
                        $('#Problema7-7-no').removeClass('active-no')
                    }
                }
            }

            if(sintoma.SeccionDescripcion == "Columna"){
                if(sintoma.Area == 1){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-8-si').addClass('active-si')
                        $('#Problema12-8-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-8-si').addClass('active-si')
                        $('#Incapacitado-8-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-8-si').addClass('active-si')
                        $('#Problema7-8-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 2){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-9-si').addClass('active-si')
                        $('#Problema12-9-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-9-si').addClass('active-si')
                        $('#Incapacitado-9-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-9-si').addClass('active-si')
                        $('#Problema7-9-no').removeClass('active-no')
                    }
                }
            }

            if(sintoma.SeccionDescripcion == "Cadera"){
                if(sintoma.Area == 1){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-10-si').addClass('active-si')
                        $('#Problema12-10-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-10-si').addClass('active-si')
                        $('#Incapacitado-10-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-10-si').addClass('active-si')
                        $('#Problema7-10-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 2){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-11-si').addClass('active-si')
                        $('#Problema12-11-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-11-si').addClass('active-si')
                        $('#Incapacitado-11-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-11-si').addClass('active-si')
                        $('#Problema7-11-no').removeClass('active-no')
                    }
                }
            }

            if(sintoma.SeccionDescripcion == "Rodilla"){
                if(sintoma.Area == 1){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-12-si').addClass('active-si')
                        $('#Problema12-12-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-12-si').addClass('active-si')
                        $('#Incapacitado-12-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-12-si').addClass('active-si')
                        $('#Problema7-12-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 2){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-13-si').addClass('active-si')
                        $('#Problema12-13-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-13-si').addClass('active-si')
                        $('#Incapacitado-13-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-13-si').addClass('active-si')
                        $('#Problema7-13-no').removeClass('active-no')
                    }
                }
            }

            if(sintoma.SeccionDescripcion == "Tobillo"){
                if(sintoma.Area == 1){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-14-si').addClass('active-si')
                        $('#Problema12-14-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-14-si').addClass('active-si')
                        $('#Incapacitado-14-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-14-si').addClass('active-si')
                        $('#Problema7-14-no').removeClass('active-no')
                    }
                }
                if(sintoma.Area == 2){
                    if(sintoma.Problema12 == 1){
                        $('#Problema12-15-si').addClass('active-si')
                        $('#Problema12-15-no').removeClass('active-no')
                    }

                    if(sintoma.Incapacitado == 1){
                        $('#Incapacitado-15-si').addClass('active-si')
                        $('#Incapacitado-15-no').removeClass('active-no')
                    }

                    if(sintoma.Problema7 == 1){
                        $('#Problema7-15-si').addClass('active-si')
                        $('#Problema7-15-no').removeClass('active-no')
                    }
                }
            }
        }

        if(sintoma.Tipo == 2){
            if(sintoma.SeccionDescripcion == "Hombro"){
                if(sintoma.Area == 1){
                    $('#Abducc-16').val(sintoma.Abducc)
                    $('#Abduc-16').val(sintoma.Abduc)
                    $('#Rexion-16').val(sintoma.Rexion)
                    $('#Extens-16').val(sintoma.Extens)
                    $('#RotExt-16').val(sintoma.RotExt)
                    $('#RotInt-16').val(sintoma.RotInt)
                    $('#Dolor04-16').val(sintoma.Dolor04)
                    $('#Irrad-16').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-16').val(sintoma.AlternacionMasaMuscular)
                }
                if(sintoma.Area == 2){
                    $('#Abducc-17').val(sintoma.Abducc)
                    $('#Abduc-17').val(sintoma.Abduc)
                    $('#Rexion-17').val(sintoma.Rexion)
                    $('#Extens-17').val(sintoma.Extens)
                    $('#RotExt-17').val(sintoma.RotExt)
                    $('#RotInt-17').val(sintoma.RotInt)
                    $('#Dolor04-17').val(sintoma.Dolor04)
                    $('#Irrad-17').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-17').val(sintoma.AlternacionMasaMuscular)
                }
            }
            if(sintoma.SeccionDescripcion == "Codo"){
                if(sintoma.Area == 1){
                    $('#Abducc-18').val(sintoma.Abducc)
                    $('#Abduc-18').val(sintoma.Abduc)
                    $('#Rexion-18').val(sintoma.Rexion)
                    $('#Extens-18').val(sintoma.Extens)
                    $('#RotExt-18').val(sintoma.RotExt)
                    $('#RotInt-18').val(sintoma.RotInt)
                    $('#Dolor04-18').val(sintoma.Dolor04)
                    $('#Irrad-18').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-18').val(sintoma.AlternacionMasaMuscular)
                }
                if(sintoma.Area == 2){
                    $('#Abducc-19').val(sintoma.Abducc)
                    $('#Abduc-19').val(sintoma.Abduc)
                    $('#Rexion-19').val(sintoma.Rexion)
                    $('#Extens-19').val(sintoma.Extens)
                    $('#RotExt-19').val(sintoma.RotExt)
                    $('#RotInt-19').val(sintoma.RotInt)
                    $('#Dolor04-19').val(sintoma.Dolor04)
                    $('#Irrad-19').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-19').val(sintoma.AlternacionMasaMuscular)
                }
            }

            if(sintoma.SeccionDescripcion == ""){
                if(sintoma.Area == 1){
                    $('#Abducc-20').val(sintoma.Abducc)
                    $('#Abduc-20').val(sintoma.Abduc)
                    $('#Rexion-20').val(sintoma.Rexion)
                    $('#Extens-20').val(sintoma.Extens)
                    $('#RotExt-20').val(sintoma.RotExt)
                    $('#RotInt-20').val(sintoma.RotInt)
                    $('#Dolor04-20').val(sintoma.Dolor04)
                    $('#Irrad-20').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-20').val(sintoma.AlternacionMasaMuscular)
                }
                if(sintoma.Area == 2){
                    $('#Abducc-21').val(sintoma.Abducc)
                    $('#Abduc-21').val(sintoma.Abduc)
                    $('#Rexion-21').val(sintoma.Rexion)
                    $('#Extens-21').val(sintoma.Extens)
                    $('#RotExt-21').val(sintoma.RotExt)
                    $('#RotInt-21').val(sintoma.RotInt)
                    $('#Dolor04-21').val(sintoma.Dolor04)
                    $('#Irrad-21').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-21').val(sintoma.AlternacionMasaMuscular)
                }
            }

            if(sintoma.SeccionDescripcion == "Mano"){
                if(sintoma.Area == 1){
                    $('#Abducc-22').val(sintoma.Abducc)
                    $('#Abduc-22').val(sintoma.Abduc)
                    $('#Rexion-22').val(sintoma.Rexion)
                    $('#Extens-22').val(sintoma.Extens)
                    $('#RotExt-22').val(sintoma.RotExt)
                    $('#RotInt-22').val(sintoma.RotInt)
                    $('#Dolor04-22').val(sintoma.Dolor04)
                    $('#Irrad-22').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-22').val(sintoma.AlternacionMasaMuscular)
                }
                if(sintoma.Area == 2){
                    $('#Abducc-23').val(sintoma.Abducc)
                    $('#Abduc-23').val(sintoma.Abduc)
                    $('#Rexion-23').val(sintoma.Rexion)
                    $('#Extens-23').val(sintoma.Extens)
                    $('#RotExt-23').val(sintoma.RotExt)
                    $('#RotInt-23').val(sintoma.RotInt)
                    $('#Dolor04-23').val(sintoma.Dolor04)
                    $('#Irrad-23').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-23').val(sintoma.AlternacionMasaMuscular)
                }
            }

            if(sintoma.SeccionDescripcion == "Cadera"){
                if(sintoma.Area == 1){
                    $('#Abducc-24').val(sintoma.Abducc)
                    $('#Abduc-24').val(sintoma.Abduc)
                    $('#Rexion-24').val(sintoma.Rexion)
                    $('#Extens-24').val(sintoma.Extens)
                    $('#RotExt-24').val(sintoma.RotExt)
                    $('#RotInt-24').val(sintoma.RotInt)
                    $('#Dolor04-24').val(sintoma.Dolor04)
                    $('#Irrad-24').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-24').val(sintoma.AlternacionMasaMuscular)
                }
                if(sintoma.Area == 2){
                    $('#Abducc-25').val(sintoma.Abducc)
                    $('#Abduc-25').val(sintoma.Abduc)
                    $('#Rexion-25').val(sintoma.Rexion)
                    $('#Extens-25').val(sintoma.Extens)
                    $('#RotExt-25').val(sintoma.RotExt)
                    $('#RotInt-25').val(sintoma.RotInt)
                    $('#Dolor04-25').val(sintoma.Dolor04)
                    $('#Irrad-25').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-25').val(sintoma.AlternacionMasaMuscular)
                }
            }

            if(sintoma.SeccionDescripcion == "Rodilla"){
                if(sintoma.Area == 1){
                    $('#Abducc-26').val(sintoma.Abducc)
                    $('#Abduc-26').val(sintoma.Abduc)
                    $('#Rexion-26').val(sintoma.Rexion)
                    $('#Extens-26').val(sintoma.Extens)
                    $('#RotExt-26').val(sintoma.RotExt)
                    $('#RotInt-26').val(sintoma.RotInt)
                    $('#Dolor04-26').val(sintoma.Dolor04)
                    $('#Irrad-26').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-26').val(sintoma.AlternacionMasaMuscular)
                }
                if(sintoma.Area == 2){
                    $('#Abducc-27').val(sintoma.Abducc)
                    $('#Abduc-27').val(sintoma.Abduc)
                    $('#Rexion-27').val(sintoma.Rexion)
                    $('#Extens-27').val(sintoma.Extens)
                    $('#RotExt-27').val(sintoma.RotExt)
                    $('#RotInt-27').val(sintoma.RotInt)
                    $('#Dolor04-27').val(sintoma.Dolor04)
                    $('#Irrad-27').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-27').val(sintoma.AlternacionMasaMuscular)
                }
            }

            if(sintoma.SeccionDescripcion == "Tobillo"){
                if(sintoma.Area == 1){
                    $('#Abducc-28').val(sintoma.Abducc)
                    $('#Abduc-28').val(sintoma.Abduc)
                    $('#Rexion-28').val(sintoma.Rexion)
                    $('#Extens-28').val(sintoma.Extens)
                    $('#RotExt-28').val(sintoma.RotExt)
                    $('#RotInt-28').val(sintoma.RotInt)
                    $('#Dolor04-28').val(sintoma.Dolor04)
                    $('#Irrad-28').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-28').val(sintoma.AlternacionMasaMuscular)
                }
                if(sintoma.Area == 2){
                    $('#Abducc-29').val(sintoma.Abducc)
                    $('#Abduc-29').val(sintoma.Abduc)
                    $('#Rexion-29').val(sintoma.Rexion)
                    $('#Extens-29').val(sintoma.Extens)
                    $('#RotExt-29').val(sintoma.RotExt)
                    $('#RotInt-29').val(sintoma.RotInt)
                    $('#Dolor04-29').val(sintoma.Dolor04)
                    $('#Irrad-29').val(sintoma.Irrad)
                    $('#AlternacionMasaMuscular-29').val(sintoma.AlternacionMasaMuscular)
                }
            }
        }

        if(sintoma.Tipo == 3){
            if(sintoma.SeccionDescripcion == "Columna"){
                if(sintoma.Area == 1){
                    $('#Flexion-30').val(sintoma.Flexion)
                    $('#Extension-30').val(sintoma.Extension)
                    $('#LateralizaIzquierda-30').val(sintoma.LateralizaIzquierda)
                    $('#LateralizaDerecha-30').val(sintoma.LateralizaDerecha)
                    $('#RotacionDerecha-30').val(sintoma.RotacionDerecha)
                    $('#RotacionIzquierda-30').val(sintoma.RotacionIzquierda)
                    $('#Dolor04-30').val(sintoma.Dolor04)
                    $('#Irrad-30').val(sintoma.Irrad)
                }
                if(sintoma.Area == 2){
                    $('#Flexion-31').val(sintoma.Flexion)
                    $('#Extension-31').val(sintoma.Extension)
                    $('#LateralizaIzquierda-31').val(sintoma.LateralizaIzquierda)
                    $('#LateralizaDerecha-31').val(sintoma.LateralizaDerecha)
                    $('#RotacionDerecha-31').val(sintoma.RotacionDerecha)
                    $('#RotacionIzquierda-31').val(sintoma.RotacionIzquierda)
                    $('#Dolor04-31').val(sintoma.Dolor04)
                    $('#Irrad-31').val(sintoma.Irrad)
                }
            }
        }

    })

}

function setTest(Tests){
    Tests.forEach(test => {
        if(test.Tipo == 1){
            if(test.TestDescripcion == "Test Phalen"){
                if(test.Area == 1){
                    if(test.Signos == 1){
                        $('#Signos-Phalen-d-si').addClass('test-active-si')
                        $('#Signos-Phalen-d-no').removeClass('test-active-no')
                    }
                }

                if(test.Area == 2){
                    if(test.Signos == 1){
                        $('#Signos-Phalen-i-si').addClass('test-active-si')
                        $('#Signos-Phalen-i-no').removeClass('test-active-no')
                    }
                }
            }

            if(test.TestDescripcion == "Test Finkelstein"){
                if(test.Area == 1){
                    if(test.Signos == 1){
                        $('#Signos-Finkelstein-d-si').addClass('test-active-si')
                        $('#Signos-Finkelstein-d-no').removeClass('test-active-no')
                    }
                }
                if(test.Area == 2){
                    if(test.Signos == 1){
                        $('#Signos-Finkelstein-i-si').addClass('test-active-si')
                        $('#Signos-Finkelstein-i-no').removeClass('test-active-no')
                    }
                }
            }

            if(test.TestDescripcion == "Test del brazo caido"){
                if(test.Area == 1){
                    if(test.Signos == 1){
                        $('#Signos-brazo-d-si').addClass('test-active-si')
                        $('#Signos-brazo-d-no').removeClass('test-active-no')
                    }
                }
                if(test.Area == 2){
                    if(test.Signos == 1){
                        $('#Signos-brazo-i-si').addClass('test-active-si')
                        $('#Signos-brazo-i-no').removeClass('test-active-no')
                    }
                }
            }

            if(test.TestDescripcion == "Test de Tinel"){
                if(test.Area == 1){
                    if(test.Signos == 1){
                        $('#Signos-Tinel-d-si').addClass('test-active-si')
                        $('#Signos-Tinel-d-no').removeClass('test-active-no')
                    }
                }
                if(test.Area == 2){
                    if(test.Signos == 1){
                        $('#Signos-Tinel-i-si').addClass('test-active-si')
                        $('#Signos-Tinel-i-no').removeClass('test-active-no')
                    }
                }
            }

            if(test.TestDescripcion == "Signo de Neer"){
                if(test.Area == 1){
                    if(test.Signos == 1){
                        $('#Signos-Neer-d-si').addClass('test-active-si')
                        $('#Signos-Neer-d-no').removeClass('test-active-no')
                    }
                }
                if(test.Area == 2){
                    if(test.Signos == 1){
                        $('#Signos-Neer-i-si').addClass('test-active-si')
                        $('#Signos-Neer-i-no').removeClass('test-active-no')
                    }
                }
            }
        }

        if(test.Tipo == 2){
            if(test.TestDescripcion == "Laseegue"){
                if(test.Area == 1){
                    if(test.Signos == 1){
                        $('#Signos-Laseegue-d-si').addClass('test-active-si')
                        $('#Signos-Laseegue-d-no').removeClass('test-active-no')
                    }
                }
                if(test.Area == 2){
                    if(test.Signos == 1){
                        $('#Signos-Laseegue-i-si').addClass('test-active-si')
                        $('#Signos-Laseegue-i-no').removeClass('test-active-no')
                    }
                }
            }

            if(test.TestDescripcion == "Schober"){
                if(test.Area == 1){
                    if(test.Signos == 1){
                        $('#Signos-Schober-d-si').addClass('test-active-si')
                        $('#Signos-Schober-d-no').removeClass('test-active-no')
                    }
                }
                if(test.Area == 2){
                    if(test.Signos == 1){
                        $('#Signos-Schober-i-si').addClass('test-active-si')
                        $('#Signos-Schober-i-no').removeClass('test-active-no')
                    }
                }
            }
        }
    })
}

function generarData(){
    var settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=generaldata",
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response.SeccionesAfectadas);
        general = response
    });
}

function mostrarDatos(){
    $('#modal').modal('show')
}

function modalAntecedentesOcupacionales(){
    var settings = {
        url: apiUrlsho+"/api/hce_Get_002_historia_clinica_antecedente?code=BsULi1Y0aoClfhCk3mSUvEsnbFAyVhtkEXaf8L8REAcYUGZLmv5qaw==&IdHC="+idHC,
        method: "GET",
        dataType: 'json',
        headers: {
            apikey: constantes.apiKey,
            "Content-Type": "application/json"
        },
    };

    $.ajax(settings).done(function (response) {

        $('#lista-body').html('')

        var myModal = new bootstrap.Modal(document.getElementById('historial'))
        myModal.show()

        response.HistoriaAntecedentes.forEach((item, index) => {

            if(item.Empresa_A != null){
                let tipo = response.HistoriaTipoAntec.filter(tipo => {
                    return tipo.IdTipA == item.IdTipA
                })

                let inicio = item.FechaInicio_A.split('T')[0].split('-')
                let final = item.FechaFin_A.split('T')[0].split('-')

                $('#lista-body').append(`<tr>
                    <td>${inicio[0]}/${inicio[2]}/${inicio[1]}</td>
                    <td>${final[0]}/${final[2]}/${final[1]}</td>
                    <td>${item.Empresa_A}</td>
                    <td>${(item.ActividadEmpresa_A == null) ? '--' : item.ActividadEmpresa_A}</td>
                    <td>${(item.AreaTrabajo_A == null) ? '--' : item.AreaTrabajo_A}</td>
                    <td>${(item.Ocupacion_A == null) ? '--' : (item.Ocupacion_A)}</td>
                    <td>${(item.PeligrosAgentesOcupacionales_A == null) ? '--' : item.PeligrosAgentesOcupacionales_A}</td>
                    <td>${tipo[0].Descripcion_TipA}</td>
                </tr>`)
            }
        })

    });
}