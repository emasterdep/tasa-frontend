const idVMO = localStorage.getItem('idVMO');
function _initProgresoTME(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    cargarDatos()
    getProgreso()
}

function getProgreso(){console.log("idVMO------->",idVMO);
    var settings = {
        url: `${apiUrlsho}/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=progresstme&EmoId=${idVMO}`,
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        let cantidad = 0;
        console.log(response);console.log("response Progreso VMO------->",response.ListVmo);
        response.ListVmo.forEach(item => {


            $('#table_hospitalizacion thead tr').append(`<th class="text-center">${item.CreadoFecha}</th>`)

            item.DiagnosticosCIE10.forEach(item2 => {
                if(item2.CIE10Code == 'A000'){
                    $('#A000').removeClass('d-none')
                    $('#A000').append(`<td class="text-center" style="font-size: 18px; background: #8fbb02">
                    <i class="bi bi-check-circle"></i>
                </td>`)
                    cantidad = cantidad + 1
                }else{
                    $('#A000').append(`<td></td>`)
                }

                if(item2.CIE10Code == 'A001'){
                    $('#A001').removeClass('d-none')
                    $('#A001').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
                    cantidad = cantidad + 1
                }else{
                    $('#A001').append(`<td></td>`)
                }

                if(item2.CIE10Code == 'A009'){
                    $('#A009').removeClass('d-none')
                    $('#A009').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
                    cantidad = cantidad + 1
                }else{
                    $('#A009').append(`<td></td>`)
                }

                if(item2.CIE10Code == 'A010'){
                    $('#A010').removeClass('d-none')
                    $('#A010').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
                    cantidad = cantidad + 1
                }else{
                    $('#A010').append(`<td></td>`)
                }

                if(item2.CIE10Code == 'A011'){
                    $('#A011').removeClass('d-none')
                    $('#A011').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
                    cantidad = cantidad + 1
                }else{
                    $('#A011').append(`<td></td>`)
                }

                if(item2.CIE10Code == 'A012'){
                    $('#A012').removeClass('d-none')
                    $('#A012').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
                    cantidad = cantidad + 1
                }else{
                    $('#A012').append(`<td></td>`)
                }

                $('#table_progreso-count-row').html('')
                $('#table_progreso-count-row').html(cantidad)

            })
            
            if(item.DiagnosticosCIE10.length == 0){

                $('#A000').append(`<td></td>`)
                $('#A001').append(`<td></td>`)
                $('#A009').append(`<td></td>`)
                $('#A010').append(`<td></td>`)
                $('#A011').append(`<td></td>`)
                $('#A012').append(`<td></td>`)

            }

        })
    });
}