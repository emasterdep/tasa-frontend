function _initRegistroConservacionAuditiva(){
    setTimeout(function (){
        console.log('local ..... regresar')
        $("#regresar").show();
    },1000)
    cargarDatos()
    const idVMO = localStorage.getItem('idVMO');
    localStorage.setItem('idVmoRegistro',idVMO)
    conservacionAuditiva = new conservasionAuditiva(`${idVMO}`,"","1","1","","1","1","","","", "", "","", "1","1","1","","","","","","","","","","","","","","","","","","","","","","","","","1","","","1","","","","","","");
    getDatosGenerales()
}

function conservasionAuditiva(
    EmoId,
    DiagnosticoAudiometria,
    ExpuestoRuido,
    NivelExposicionId,
    ValorExposicion,
    UsoEppAuditivo,
    TipoEppAuditivoId,
    ABI2000,
    ABI3000,
    ABI4000,
    ABD2000,
    ABD3000,
    ABD4000,
    EstadoOidoIzquierdoId,
    EstadoOidoDerechoId,
    Otro,
    DetalleOtro,
    AAI125,
    AAI250,
    AAI500,
    AAI750,
    AAI1000,
    AAI1500,
    AAI2000,
    AAI3000,
    AAI4000,
    AAI6000,
    AAI8000,
    AAD125,
    AAD250,
    AAD500,
    AAD750,
    AAD1000,
    AAD1500,
    AAD2000,
    AAD3000,
    AAD4000,
    AAD6000,
    AAD8000,
    STSOD,
    InterpretacionStsOD,
    STSOI,
    InterpretacionStsOI,
    DiagnosticoFinal,
    PerdidaMonoauralOD,
    PerdidaMonoauralOI,
    Comentarios,
    MedicoCargo,
    CreadoPor,
    ModificadoPor,
){
    this.Id = "0"
    this.EmoId = EmoId
    this.DiagnosticoAudiometria = DiagnosticoAudiometria
    this.ExpuestoRuido = ExpuestoRuido
    this.NivelExposicionId = NivelExposicionId
    this.ValorExposicion = ValorExposicion
    this.UsoEppAuditivo = UsoEppAuditivo
    this.TipoEppAuditivoId = TipoEppAuditivoId
    this.ABI2000 = ABI2000
    this.ABI3000 = ABI3000
    this.ABI4000 = ABI4000
    this.ABD2000 = ABD2000
    this.ABD3000 = ABD3000
    this.ABD4000 = ABD4000
    this.EstadoOidoIzquierdoId = EstadoOidoIzquierdoId
    this.EstadoOidoDerechoId = EstadoOidoDerechoId
    this.Otro = Otro
    this.DetalleOtro = DetalleOtro
    this.AAI125 = AAI125
    this.AAI250 = AAI250
    this.AAI500 = AAI500
    this.AAI750 = AAI750
    this.AAI1000 = AAI1000
    this.AAI1500 = AAI1500
    this.AAI2000 = AAI2000
    this.AAI3000 = AAI3000
    this.AAI4000 = AAI4000
    this.AAI6000 = AAI6000
    this.AAI8000 = AAI8000
    this.AAD125 = AAD125
    this.AAD250 = AAD250
    this.AAD500 = AAD500
    this.AAD750 = AAD750
    this.AAD1000 = AAD1000
    this.AAD1500 = AAD1500
    this.AAD2000 = AAD2000
    this.AAD3000 = AAD3000
    this.AAD4000 = AAD4000
    this.AAD6000 = AAD6000
    this.AAD8000 = AAD8000
    this.STSOD = STSOD
    this.InterpretacionStsOD = InterpretacionStsOD
    this.STSOI = STSOI
    this.InterpretacionStsOI = InterpretacionStsOI
    this.DiagnosticoFinal = DiagnosticoFinal
    this.PerdidaMonoauralOD = PerdidaMonoauralOD
    this.PerdidaMonoauralOI = PerdidaMonoauralOI
    this.Comentarios = Comentarios
    this.MedicoCargo = MedicoCargo
    this.Activo = "1"
    this.CreadoPor = CreadoPor
    this.ModificadoPor = ModificadoPor
    this.Adjuntos = []
    this.Capacitaciones = []
}

function getDatosGenerales(){
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    $('#valor').val('86-90 dBs')
    conservacionAuditiva["ValorExposicion"] = '86-90 dBs'
    conservacionAuditiva.MedicoCargo = usuario.fullusername
    conservacionAuditiva.CreadoPor = usuario.idhash
    conservacionAuditiva.ModificadoPor = usuario.idhash
    const settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=generaldata",
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {

        const niveleExprecion = response.NivelExposicion.sort((x,y) => {
            return (x.Descripcion < y.Descripcion) ? -1 : 1
        })

        console.log(niveleExprecion)

        niveleExprecion.forEach(item => {
            $('#nivelExposicion').append(`
                <option value="${item.Id}">${item.Descripcion}</option>
            `)
        })

        const TipoEppAuditivo = response.TipoEppAuditivo.sort((x,y) => {
            return (x.Descripcion < y.Descripcion) ? -1 : 1
        })

        TipoEppAuditivo.forEach(item => {
            $('#tipoEppAuditivo').append(`
                <option value="${item.Id}">${item.Descripcion}</option>
            `)
        })

        const EstadoOido = response.EstadoOido.sort((x,y) => {
            return (x.Descripcion < y.Descripcion) ? -1 : 1
        })

        EstadoOido.forEach(item => {
            $('#oidoIzquierdo').append(`
                <option value="${item.Id}">${item.Descripcion}</option>
            `)
            $('#oidoDerecho').append(`
                <option value="${item.Id}">${item.Descripcion}</option>
            `)
        })

        const DiagnosticoOido = response.DiagnosticoOido.sort((x,y) => {
            return (x.Descripcion < y.Descripcion) ? -1 : 1
        })

        DiagnosticoOido.forEach(item => {
            $('#diagnostico_final').append(`
                <option value="${item.Id}">${item.Descripcion}</option>
            `)
        })
    });
}

function Adjuntos(NombreArchivo, Archivo, Tipo){
    this.Id = 0
    this.NombreArchivo = NombreArchivo
    this.Archivo = Archivo
    this.Tipo = Tipo
    this.Activo = 1
}

function AdjuntarArchivoCA(id,tipo){
    let file = $(`#${id}`)[0].files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        conservacionAuditiva.Adjuntos.push(new Adjuntos(file.name,reader.result,tipo))
        listaTablaAdjunto()
    }
    reader.readAsDataURL(file);
}

function listaTablaAdjunto(){
    let adjunto_archivo = conservacionAuditiva.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 1
    })

    let adjunto_firma = conservacionAuditiva.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 2
    })

    $('#table_adjunto_firma-count-row').html(adjunto_firma.length)
    $('#table_adjunto-archivos-count-row').html(adjunto_archivo.length)

    if(adjunto_firma.length > 0){
        $('#lista-adjunto-firma').removeClass('d-none')
    }

    if(adjunto_archivo.length > 0){
        $('#lista-adjunto-archivos').removeClass('d-none')
    }

    $('#adjunto_firma').pagination({
        dataSource: adjunto_firma,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_firma').html(html);
        }
    })

    $('#pagination-adjunto-archivos').pagination({
        dataSource: adjunto_archivo,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto-archivos').html(html);
        }
    })
}

function simpleTemplatingAdjunto(data){
    const date = new Date();
    let CreadoFecha = `${date.getDate()}/${((parseInt(date.getMonth()) + 1) < 10) ? '0'+(parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1}/${date.getFullYear()}`
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.NombreArchivo}</td>
                <td>${CreadoFecha}</td>
                <td>
                    <button onclick="eliminarAdjunto('${item.NombreArchivo}')" id="hc_btn_borrar_evidencia_31" type="button" class="btn btn-link shadow-none float-right">
                      <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                    </button>
                    <a class="btn btn-link shadow-none float-right" href="${item.Archivo}" download="${item.NombreArchivo}">
                      <img class="inject-svg" src="./images/sho/download.svg" fill="#207345" style="width:16px !important">
                    </a>
                </td>
            </tr> 
        `
        });
    }else{
        html += ` <tr>
            <td className="text-center" colSpan="5" style="    text-align: center;">No se encontro resultados</td>
        </tr>`
    }
    return html;
}

function eliminarAdjunto(nombre){
    conservacionAuditiva.Adjuntos.forEach((adjunto, index) => {
        if(adjunto.NombreArchivo == nombre){
            conservacionAuditiva.Adjuntos.splice(index,1)
            listaTablaAdjunto()
        }
    })
}

function setDatosConservasionAuditiva(campo, idCampo){
    let valor = $(`#${idCampo}`).val()
    conservacionAuditiva[campo] = valor

    if(campo == 'NivelExposicionId'){
        let valor1 = '86-90 dBs'
        if(valor == 1){
            valor1 = "80-81 dBs"
        }else if(valor == 2){
            valor1 = "82-85 dBs"
        }else if(valor == 3){
            valor1 = "86-90 dBs"
        }else if(valor == 4){
            valor1 = "90 dBs a más"
        }

        conservacionAuditiva["ValorExposicion"] = valor1
        $('#valor').val(valor1)

    }else if(campo == 'ABI2000' || campo == 'ABI3000' || campo == 'ABI4000'){
        let ABI2000 = ($(`#ABI2000`).val() == '') ? 0 : $(`#ABI2000`).val()
        let ABI3000 = ($(`#ABI3000`).val() == '') ? 0 : $(`#ABI3000`).val()
        let ABI4000 = ($(`#ABI4000`).val() == '') ? 0 : $(`#ABI4000`).val()
        let calculo = ((parseFloat(ABI2000) + parseFloat(ABI3000) + parseFloat(ABI4000)) / 3 ).toFixed(2)
        $('#stsOD').val(calculo)
        conservacionAuditiva["STSOD"] = calculo
        conservacionAuditiva["InterpretacionStsOI"] = (calculo >= 10) ? 'POSITIVO' : 'NEGATIVO'
        $('#InterpretacionStsOI').html((calculo >= 10) ? 'POSITIVO' : 'NEGATIVO')
    }else if(campo == 'ABD2000' || campo == 'ABD3000' || campo == 'ABD4000'){
        let ABD2000 = ($(`#ABD2000`).val() == '') ? 0 : $(`#ABD2000`).val()
        let ABD3000 = ($(`#ABD3000`).val() == '') ? 0 : $(`#ABD3000`).val()
        let ABD4000 = ($(`#ABD4000`).val() == '') ? 0 : $(`#ABD4000`).val()
        let calculo = ((parseFloat(ABD2000) + parseFloat(ABD3000) + parseFloat(ABD4000)) / 3).toFixed(2)
        $('#stsOI').val( calculo )
        conservacionAuditiva["STSOI"] = calculo
        conservacionAuditiva["InterpretacionStsOD"] = (calculo >= 10) ? 'POSITIVO' : 'NEGATIVO'
        $('#InterpretacionStsOD').html((calculo >= 10) ? 'POSITIVO' : 'NEGATIVO')
    }else if(campo == 'AAI500' || campo == 'AAI1000' || campo == 'AAI2000' || campo == 'AAI3000'){
        let AAI500 = ($(`#AAI500`).val() == '') ? 0 : $(`#AAI500`).val()
        let AAI1000 = ($(`#AAI1000`).val() == '') ? 0 : $(`#AAI1000`).val()
        let AAI2000 = ($(`#AAI2000`).val() == '') ? 0 : $(`#AAI2000`).val()
        let AAI3000 = ($(`#AAI3000`).val() == '') ? 0 : $(`#AAI3000`).val()
        $('#PerdidaMonoauralOI').val( ((((parseFloat(AAI500) + parseFloat(AAI1000) + parseFloat(AAI2000) + parseFloat(AAI3000))/ 4) - 25) * 1.5).toFixed(2) )
        conservacionAuditiva["PerdidaMonoauralOI"] = (((parseFloat(AAI500) + parseFloat(AAI1000) + parseFloat(AAI2000) + parseFloat(AAI3000)/ 4) - 25) * 1.5).toFixed(2)
    }else if(campo == 'AAD500' || campo == 'AAD1000' || campo == 'AAD2000' || campo == 'AAD3000'){
        let AAD500 = ($(`#AAD500`).val() == '') ? 0 : $(`#AAD500`).val()
        let AAD1000 = ($(`#AAD1000`).val() == '') ? 0 : $(`#AAD1000`).val()
        let AAD2000 = ($(`#AAD2000`).val() == '') ? 0 : $(`#AAD2000`).val()
        let AAD3000 = ($(`#AAD3000`).val() == '') ? 0 : $(`#AAD3000`).val()
        $('#PerdidaMonoauralOD').val( ((((parseFloat(AAD500) + parseFloat(AAD1000) + parseFloat(AAD2000) + parseFloat(AAD3000))/ 4) - 25) * 1.5).toFixed(2) )
        conservacionAuditiva["PerdidaMonoauralOD"] = (((parseFloat(AAD500) + parseFloat(AAD1000) + parseFloat(AAD2000) + parseFloat(AAD3000)/ 4) - 25) * 1.5).toFixed(2)
    }
}

function agregarListaCapacitacionCA(){
    let tema = $('#tema').val();
    let comentario = $('#comentario').val();
    conservacionAuditiva.Capacitaciones.push(new Capacitacion(tema,comentario))
    tablaCapacitacionCA()
    $('#tema').val('')
    $('#comentario').val('')
}

function Capacitacion(tema, comentario){
    this.Id = 0;
    this.Tema = tema
    this.Comentario = comentario
    this.Activo = 1
}

function tablaCapacitacionCA(){
    $('#table_capacitacion-count-row').html(conservacionAuditiva.Capacitaciones.length)
    console.log(conservacionAuditiva.Capacitaciones.length)
    $('#content_capacitacion').html('')
    conservacionAuditiva.Capacitaciones.forEach((capacitacion, index) => {
        $('#content_capacitacion').append(`
            <tr>
                <td>${index + 1}</td>
                <td>${capacitacion.Tema}</td>
                <td>${capacitacion.Comentario}</td>
                <td>
                    <img src="images/iconos/trash.svg" onclick="eliminarCapacitacion(${index})"/>
                </td>
            </tr>    
        `)
    })
}

function eliminarCapacitacion(index){
    conservacionAuditiva.Capacitaciones.splice(index, 1);
    tablaCapacitacionCA()
}

function guardarConservacionAuditiva(){

    if(conservacionAuditiva.DiagnosticoAudiometria != '' && conservacionAuditiva.AAD125 != '' && conservacionAuditiva.AAD250 != '' && conservacionAuditiva.AAD500 != ''
        && conservacionAuditiva.AAD750 != '' && conservacionAuditiva.AAD1000 != '' && conservacionAuditiva.AAD1500 != '' && conservacionAuditiva.AAD2000 != ''
        && conservacionAuditiva.AAD3000 != '' && conservacionAuditiva.AAD4000 != '' && conservacionAuditiva.AAD6000 != '' && conservacionAuditiva.AAD8000 != ''
        && conservacionAuditiva.AAI125 != '' && conservacionAuditiva.AAI250 != '' && conservacionAuditiva.AAI500 != '' && conservacionAuditiva.AAI750 != ''
        && conservacionAuditiva.AAI1000 != '' && conservacionAuditiva.AAI1500 != '' && conservacionAuditiva.AAI2000 != '' && conservacionAuditiva.AAI3000 != ''
        && conservacionAuditiva.AAI4000 != '' && conservacionAuditiva.AAI6000 != '' && conservacionAuditiva.AAI8000 != '' && conservacionAuditiva.ABD2000 != ''
        && conservacionAuditiva.ABD3000 != '' && conservacionAuditiva.ABD4000 != '' && conservacionAuditiva.ABI2000 != '' && conservacionAuditiva.ABI3000 != ''
        && conservacionAuditiva.ABI4000 != '' && conservacionAuditiva.Capacitaciones.length > 0 && conservacionAuditiva.ABI3000 != '' && conservacionAuditiva.ABI3000 != ''){

        var settings = {
            url: apiUrlsho+"/api/hce_Post_vmo?code=aOqdZIaCcrb03DFAYLAEOuaE4YmZywydiZ8tVakK26za/CWLzZck/w==&httpmethod=postCA",
            method: "POST",
            timeout: 0,
            headers: {
                apikey: constantes.apiKey,
                "Content-Type": "application/json"
            },
            data: JSON.stringify(conservacionAuditiva)
        };
        let datos = paObj_hc[idHC].a;
        Swal.fire({
            title: 'Guardar registro de conservación auditiva',
            html: `<p>Está por guardar el registro conservación auditiva de: ${datos.Nombres_Trabajador_H} ${datos.Apellidos_Trabajador_H}</p> <p class="mt-4">¿Desea confirmar la acción?</p>`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#8fbb02',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar <img src="./images/sho/confirm.svg" />',
            cancelButtonText: 'Cancelar <img src="./images/sho/cancelar.svg" />',
            reverseButtons: true
        }).then((result) => {
            $.ajax(settings).done(function (response) {
                console.log(response);
            });
            if (result.isConfirmed) {
                Swal.fire({
                    title: 'Se guardó el registro con éxito',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 2500
                })

                atrasBack()

            }
        })

    }else{
        Swal.fire({
            title: 'Se ha detectado campos que faltan ser rellenado',
            icon: 'error',
            timer: 2500
        })
    }

}

function ImprimirCAConservacionAuditiva(){

    let UsuarioVMO = localStorage.getItem('UsuarioVMO');

    if(UsuarioVMO){
        // exportListaVMO(UsuarioVMO)
    }else{
        exportConstanciaAutorizacion(paObj_hc[idHC].a)
    }

}

function exportConstanciaAutorizacion(UsuarioVMO){
    const date = new Date()
    const PDF = new jsPDF()
    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,10,200,10)
    PDF.line(10,10,10,285)
    PDF.line(10,285,200,285)
    PDF.line(200,10,200,285)
    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,15,190,15)
    PDF.line(20,15,20,33)
    PDF.line(20,33,190,33)
    PDF.line(190,15,190,33)

    PDF.line(60,15,60,33)
    PDF.setFontSize(9)
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO\n         AL PROGRAMA DE VIGILANCIA MÉDICA',62, 22)
    PDF.line(140,15,140,33)

    PDF.setFontSize(10)
    PDF.text('    Versión',142, 19)
    PDF.text('      N°.00',142, 24)
    PDF.line(165,15,165,26)
    PDF.text('  Página:',170, 19)
    PDF.text('   1 de 1',170, 24)
    PDF.line(140,26,190,26)
    PDF.text('    Código: SSM02-F16',145, 31)

    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,80,200,80)
    PDF.setFontSize(16)
    PDF.setFontType("bold");
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO AL PROGRAMA\n' +
        '                                     DE VIGILANCIA MÉDICA',15, 88)
    PDF.line(10,100,200,100)

    PDF.text('PROGRAMA:',25, 120)
    PDF.text('Conservación auditiva',75, 120)

    PDF.text('PARTICIPANTE:',25, 135)
    PDF.text(`${UsuarioVMO.Nombres_Trabajador_H} ${UsuarioVMO.Apellidos_Trabajador_H}`,75, 135)

    PDF.text('DNI:',135, 135)
    PDF.text(`${UsuarioVMO.NroDocumento_Trabajador_H}`,150, 135)

    PDF.text('UNIDAD:',25, 150)

    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == UsuarioVMO.SedeId_Empresa_H
    });

    PDF.text(`${sede[0].Description}`,70, 150)
    PDF.text('ÁREA/PUESTO:',25, 165)

    let area = sedeAreaGerencia.Area.filter(area => {
        return area.Id == UsuarioVMO.AreaId_Empresa_H
    })

    PDF.text(`${area[0].Description} / ${UsuarioVMO.PuestoTrabajo_Empresa_H}`,70, 165)

    PDF.setFontSize(11)
    PDF.setFontType("itali");
    PDF.text('A través del presente documento dejo constancia que deseo participar del programa de vigilancia\n' +
        'médica ocupacional antes referido, comprometiéndome a asistir y a poner en práctica las diversas\n' +
        'pautas preventivas que nos brinden.',25,180)

    PDF.setFontSize(10)
    PDF.setFontType("bold")
    PDF.text('FIRMA DEL TRABAJADOR',28,250)
    PDF.text('FIRMA DEL MÉDICO OCUPACIONAL',120,250)

    PDF.text('Fecha',28,270)
    PDF.text(`${date.getMonth()}-${date.getDate()}-${date.getFullYear()}`,45,270)

    PDF.save('constancia-de-autorizacion-conservacion-auditiva.pdf')
}

function pdfCapacitacion(){
    let UsuarioVMO = localStorage.getItem('UsuarioVMO');
    if(UsuarioVMO){
        // exportListaVMO(UsuarioVMO)
    }else{
        exportHCCapacitacion(paObj_hc[idHC].a)
    }
}

function exportHCCapacitacion(usuario){
    const PDF = new jsPDF()
    const date = new Date();
    PDF.setFontSize(7)
    PDF.setFontType("bold");
    PDF.text('TECNOLÓGICA DE ALIMENTOS S.A',150.5,15)
    PDF.text('RUC 20100971772 S.A',167,20)
    PDF.text('ACTIVIDAD ECONÓMICA: ELABORACIÓN Y CONSERVACIÓN DE PESCADO, CRUSTÁCEOS Y MOLUSCOS',67,25)

    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,30,190,30)
    PDF.line(20,30,20,40)
    PDF.line(20,40,190,40)
    PDF.line(190,30,190,40)

    PDF.setFontSize(15)
    PDF.setFontType("bold");
    PDF.text('CAPACITACIÓN VMO: CONSERVACIÓN AUDITIVA',50,37)

    PDF.setFontSize(10)
    PDF.text('APELLIDOS Y NOMBRES :',20,50)
    PDF.text('UNIDAD OPERATIVA :',20,60)
    PDF.text('PUESTO DE TRABAJO :',20,70)

    PDF.text(`${usuario.Nombres_Trabajador_H} ${usuario.Apellidos_Trabajador_H}`,70,50)
    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == usuario.SedeId_Empresa_H
    });
    PDF.text(`${sede[0].Description}`,65,60)
    PDF.text(`${usuario.PuestoTrabajo_Empresa_H}`,65,70)

    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,80,190,80)
    PDF.line(20,80,20,85)
    PDF.line(20,85,190,85)
    PDF.line(190,80,190,85)

    PDF.setFontSize(8)
    PDF.line(30,80,30,85)
    PDF.text('N°',23,83.5)
    PDF.text('TEMA',60,83.5)
    PDF.line(110,80,110,85)
    PDF.text('N°DNI',115,83.5)
    PDF.line(130,80,130,85)
    PDF.text('ÁREA',135,83.5)
    PDF.line(150,80,150,85)
    PDF.text('Fecha',155,83.5)
    PDF.line(170,80,170,85)
    PDF.text('FIRMA',175,83.5)

    let datos = paObj_hc[idHC].a


    conservacionAuditiva.Capacitaciones.forEach((capacitacion,index) => {
        PDF.setFontSize(8)
        PDF.line(20,(80 + (index + 1) * 5),20,(85 + (index + 1) * 5))
        PDF.line(20,(85 + (index + 1) * 5),190,(85 + (index + 1) * 5))
        PDF.line(30,(80 + (index + 1) * 5),30,(85 + (index + 1) * 5))
        PDF.text(`${index+1}`,23,(83.5 + (index + 1) * 5))
        PDF.text(`${capacitacion.Comentario}`,33,(83.5 + (index + 1) * 5))
        PDF.line(110,(80 + (index + 1) * 5),110,(85 + (index + 1) * 5))
        PDF.text(`${datos.NroDocumento_Trabajador_H}`,113,(83.5 + (index + 1) * 5))
        PDF.line(130,(80 + (index + 1) * 5),130,(85 + (index + 1) * 5))

        sedeAreaGerencia.Area.forEach((e) => {
            if (e.Id == datos.AreaId_Empresa_H) {
                PDF.setFontSize(6)
                PDF.text(`${e.Description}`,132,(83.5 + (index + 1) * 5))
            }
        });
        PDF.line(150,(80 + (index + 1) * 5),150,(85 + (index + 1) * 5))

        PDF.setFontSize(8)
        PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,152,(83.5 + (index + 1) * 5))
        PDF.line(170,(80 + (index + 1) * 5),170,(85 + (index + 1) * 5))
        PDF.line(190,(80 + (index + 1) * 5),190,(85 + (index + 1) * 5))
    })

    if(conservacionAuditiva.Capacitaciones.length < 20){
        for (let i = conservacionAuditiva.Capacitaciones.length ; i < 20 ; i++){
            PDF.setFontSize(8)
            PDF.line(20,(80 + (i + 1) * 5),20,(85 + (i + 1) * 5))
            PDF.line(20,(85 + (i + 1) * 5),190,(85 + (i + 1) * 5))
            PDF.line(30,(80 + (i + 1) * 5),30,(85 + (i + 1) * 5))
            PDF.text(`${i+1}`,23,(83.5 + (i + 1) * 5))
            PDF.line(110,(80 + (i + 1) * 5),110,(85 + (i + 1) * 5))

            PDF.line(130,(80 + (i + 1) * 5),130,(85 + (i + 1) * 5))


            PDF.line(150,(80 + (i + 1) * 5),150,(85 + (i + 1) * 5))

            PDF.line(170,(80 + (i + 1) * 5),170,(85 + (i + 1) * 5))
            PDF.line(190,(80 + (i + 1) * 5),190,(85 + (i + 1) * 5))
        }
        // let usuario = JSON.parse(localStorage.getItem('usuario'))
        // PDF.setFontSize(10)
        // PDF.text('OBSERVACIONES :',20,(85 + 22 * 5))
        // PDF.line(55,(85 + 22 * 5),190,(85 + 22 * 5))
        // PDF.line(20,(85 + 23 * 5),190,(85 + 23 * 5))
        // PDF.line(20,(85 + 24 * 5),190,(85 + 24 * 5))
        // PDF.text('RESPONSABLE DEL REGISTRO',20,(85 + 26 * 5))
        // PDF.text('Nombre y Apellido :',20,(85 + 28 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${usuario.fullusername}`,60,(84 + 28 * 5))
        // PDF.line(60,(85 + 28 * 5),140,(85 + 28 * 5))
        // PDF.setFontSize(10)
        // PDF.text('Fecha :',145,(85 + 28 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,165,(84 + 28 * 5))
        // PDF.line(160,(85 + 28 * 5),190,(85 + 28 * 5))
        // PDF.setFontSize(10)
        // PDF.text('Cargo :',20,(85 + 30 * 5))
        // PDF.line(60,(85 + 30 * 5),140,(85 + 30 * 5))
        // PDF.text('Fecha :',145,(85 + 30 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,165,(84 + 30 * 5))
        // PDF.line(160,(85 + 30 * 5),190,(85 + 30 * 5))
    }

    let cantidad = (conservacionAuditiva.Capacitaciones.length <= 20) ? 20 : conservacionAuditiva.Capacitaciones.length

    let usuario2 = JSON.parse(localStorage.getItem('usuario'))
    PDF.setFontSize(10)
    PDF.text('OBSERVACIONES :',20,(85 + (cantidad + 2) * 5))
    PDF.line(55,(85 + 22 * 5),190,(85 + (cantidad + 2) * 5))
    PDF.line(20,(85 + 23 * 5),190,(85 + (cantidad + 3) * 5))
    PDF.line(20,(85 + 24 * 5),190,(85 + (cantidad + 4) * 5))
    PDF.text('RESPONSABLE DEL REGISTRO',20,(85 + (cantidad + 6) * 5))
    PDF.text('Nombre y Apellido :',20,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(8)
    PDF.text(`${usuario2.fullusername}`,60,(84 + (cantidad + 8) * 5))
    PDF.line(60,(85 + (cantidad + 8) * 5),140,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(10)
    PDF.text('Fecha :',145,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(8)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,168,(84 + (cantidad + 8) * 5))
    PDF.line(160,(85 + (cantidad + 8) * 5),190,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(10)
    PDF.text('Cargo :',20,(85 + (cantidad + 10) * 5))
    PDF.line(60,(85 + (cantidad + 10) * 5),140,(85 + (cantidad + 10) * 5))
    PDF.text('Fecha :',145,(85 + (cantidad + 10) * 5))
    PDF.setFontSize(8)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,168,(84 + (cantidad + 10) * 5))
    PDF.line(160,(85 + (cantidad + 10) * 5),190,(85 + (cantidad + 10) * 5))

    PDF.save('capacitación-vmo-conservación-auditiva.pdf')
}