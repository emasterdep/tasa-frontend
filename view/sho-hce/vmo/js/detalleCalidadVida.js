function _initDetalleCalidadVida(){

    cargarDatos()

    let idRegistroVMO = localStorage.getItem('idRegistroVMO')
    let settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=objectcv&CalidadVidaId="+idRegistroVMO,
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log('log..... ',response);

       let adjunto1 = 0

        response.Adjuntos.forEach(adjunto => {
            if(adjunto.Tipo == 1){
                console.log('holaaa.....')
                $('#content_archivo-adjunto').append(`<tr>
                    <td>${adjunto.NombreArchivo}</td>
                    <td>${adjunto.CreadoFecha}</td>
                    <td>${adjunto.NombreArchivo}</td>
                </tr>`)
                adjunto1 = adjunto1 + 1
            }
            $('#table_archivo-adjunto-count-row').html(adjunto1)
        })

        $('#reg_cali_vida_datos_sistole').val(response.PresionArterial)
        $('#reg_cali_vida_datos_diastole').val(response.PresionArterialDiastole)
        $('#reg_cali_vida_datos_peso').val(response.Peso)
        $('#reg_cali_vida_datos_glucosa').val(response.Glucosa)

        if(response.HTA == 1){
            $('#select_reg_cali_vida_datos_HTA_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_HTA_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_HTA_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_HTA_no').addClass('active-no')
        }

        if(response.Dislipidemias == 1){
            $('#select_reg_cali_vida_datos_dislipidemias_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_dislipidemias_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_dislipidemias_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_dislipidemias_no').addClass('active-no')
        }

        $('#reg_cali_vida_datos_frecuencia_cardiaca').val(response.FrecuenciaCardiaca)
        $('#reg_cali_vida_datos_temperatura').val(response.Temperatura)

        if(response.Fumador == 1){
            $('#select_reg_cali_vida_datos_fumador_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_fumador_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_fumador_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_fumador_no').addClass('active-no')
        }

        if(response.ConsumoFumadorDescripcion != null){
            $('#reg_cali_vida_datos_fumador_select').append(`<option>${response.ConsumoFumadorDescripcion}</option>`)
        }

        if(response.ConsumoAzucar == 1){
            $('#select_reg_cali_vida_datos_consumo_de_azucar_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_consumo_de_azucar_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_consumo_de_azucar_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_consumo_de_azucar_no').addClass('active-no')
        }

        if(response.ConsumoAzucarDescripcion != null){
            $('#reg_cali_vida_datos_consumo_azucar_select').append(`<option>${response.ConsumoAzucarDescripcion}</option>`)
        }

        if(response.ActividadFisica == 1){
            $('#select_reg_cali_vida_datos_actividad_fisica_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_actividad_fisica_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_actividad_fisica_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_actividad_fisica_no').addClass('active-no')
        }

        if(response.ActividadFisicaDescripcion != null){
            $('#reg_cali_vida_datos_actividad_fisica_select').append(`<option>${response.ActividadFisicaDescripcion}</option>`)
        }

        $('#reg_cali_vida_datos_imc').val(response.IMC)
        $('#reg_cali_vida_datos_talla').val(response.Talla)
        $('#reg_cali_vida_datos_colesterol').val(response.Colesterol)

        if(response.Sobrepeso == 1){
            $('#select_reg_cali_vida_datos_sosbrepeso_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_sosbrepeso_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_sosbrepeso_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_sosbrepeso_no').addClass('active-no')
        }

        $('#reg_cali_vida_datos_frecuencia_respiratoria').val(response.FrecuenciaRespiratorio)
        $('#reg_cali_vida_datos_ldl').val(response.LDL)
        $('#reg_cali_vida_datos_perimetro_abdominal').val(response.PerimetroAbdominal)
        $('#reg_cali_vida_datos_trigliceridos').val(response.Trigliceridos)

        if(response.Obesidad == 1){
            $('#select_reg_cali_vida_datos_obesidad_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_obesidad_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_obesidad_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_obesidad_no').addClass('active-no')
        }

        $('#reg_cali_vida_datos_saturacion_oxigeno').val(response.SaturaciondeOxigeno)

        if(response.Estress == 1){
            $('#select_reg_cali_vida_datos_estress_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_estress_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_estress_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_estress_no').addClass('active-no')
        }

        if(response.RiesgoCardioVascularDescripcion != null){
            $('#reg_cali_vida_datos_riesgo_cardiovascular_select').append(`<option>${response.RiesgoCardioVascularDescripcion}</option>`)
        }

        $('#reg_cali_vida_datos_hdl').val(response.HDL)

        if(response.Diabetes == 1){
            $('#select_reg_cali_vida_datos_diabetes_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_diabetes_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_diabetes_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_diabetes_no').addClass('active-no')
        }

        if(response.Alcohol == 1){
            $('#select_reg_cali_vida_datos_alcohol_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_alcohol_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_alcohol_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_alcohol_no').addClass('active-no')
        }

        if(response.ConsumoAlcoholDescripcion != null){
            $('#reg_cali_vida_datos_alcohol_select').append(`<option>${response.ConsumoAlcoholDescripcion}</option>`)
        }

        if(response.Otros == 1){
            $('#select_reg_cali_vida_datos_otros_si').addClass('active-si')
            $('#select_reg_cali_vida_datos_otros_no').addClass('disabled')
        }else{
            $('#select_reg_cali_vida_datos_otros_si').addClass('disabled')
            $('#select_reg_cali_vida_datos_otros_no').addClass('active-no')
        }

        $('#reg_cali_vida_datos_otros_input').val(response.OtrosDetalle)

        $('#table_hospitalizacion-count-row').html(response.Hospitalizaciones.length)
        response.Hospitalizaciones.forEach(item => {
            $('#content_hospitalizacion').append(`<tr>
                <td>${item.FechaInicio}</td>
                <td>${item.FechaFin}</td>
                <td>${item.TiempoEstadia}</td>
                <td>${item.Motivo}</td>
            </tr>`)
        })

        $('#reg_cali_vida_hosp_otros').val(response.OtrosHospitalizacion)

        response.OtrosSintomas.forEach((sintomas, index) => {
            $('#otrosSintomas').append(`
            <div class="row w-100 align-items-center mb-3">
                    <div class="col-md-2">
                        <div class="d-flex">
                            <img src="./images/clipboard-minus-azul.svg" alt="" style="width: 25px;height: 25px;">
                            <span>${sintomas.Nombre}</span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="d-flex">
                            <div class="">
                                <div class="">
                                    <label 
                                     for="reg_cali_vida_datos_HTA_si"
                                     class="label-radio ${(sintomas.Flag == 1) ? 'active-si' : 'disabled' }"
                                     id="otroSintomas-si-${index}"
                                     onclick="setOtrosSintomasRadio('${index}','Flag','1')">Si</label>
                                    <input 
                                        class="d-none" 
                                        type="radio" 
                                        name="hta" 
                                        value="Si" 
                                        min="0" 
                                        id="reg_cali_vida_otros_creatinina_si"
                                        >
                                </div>
                            </div>
                            <div class="ml-3">
                                <div class="input_hc_sp3">
                                    <label for="reg_cali_vida_datos_HTA_no" class="label-radio ${(sintomas.Flag == 1) ? 'disabled' : 'active-no' }"
                                    id="otroSintomas-no-${index}"
                                    onclick="setOtrosSintomasRadio('${index}','Flag','0')">No</label>
                                    <input class="d-none" type="radio" name="hta" value="No" min="0" id="reg_cali_vida_otros_creatinina_no">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control w-100" type="text" name="hta" min="0" placeholder="Comentarios" i
                            d="reg_cali_vida_otros_creatinina_comentario-${index}" style="
                            width: 100% !important;
                            text-align: start;
                        "
                        disabled
                        value="${sintomas.Comentario}">
                    </div>
                    <div class="col-md-2">
                        ${(sintomas.Archivo) ?
                            '<label for="fileOtrosSintomas-${index}"\n' +
                '                                   class="btn btn-success btn-block btn-lg w-100"\n' +
                '                                   style="width: 200px; padding: 10px;"\n' +
                '                            >Ver archivo\n' +
                '                                <img src="./images/sho/eyeIcon_blanco.svg" fill="#fff" style="position: relative; width: 18px">\n' +
                '                            </label>'
                        :
                            '<label for="fileOtrosSintomas-${index}"\n' +
                '                            class="btn btn-disabled btn-block btn-lg w-100"\n' +
                '                            style="width: 200px; padding: 10px;"\n' +
                '                            >Subir archivo\n' +
                '                            <img src="./images/sho/upload_disabled.svg" fill="#fff" style="position: relative; width: 18px">\n' +
                '                            </label>'
                        }
                        <input type="file" id="fileOtrosSintomas-${index}" class="d-none" onchange="fileOtrosSintomas('${index}')">
                    </div>
                    <div class="col-md-2">
                        <span class="m-0" id="nombrefile-${index}">${(sintomas.NombreArchivo) ? sintomas.NombreArchivo : 'No hay archivo'}</span>
                    </div>
                </div>
        `)
        })

        $('#table_medicamento-count-row').html(response.Medicamentos.length)

        response.Medicamentos.forEach((medicamento,index) => {
            $('#content_medicamento').append(`<tr>
                <td>${index + 1}</td>
                <td>${medicamento.CreadoFecha}</td>
                <td>${medicamento.NombreMedicamento}</td>
                <td>${medicamento.Comentario}</td>
            </tr>`)
        })

        $('#table_capacitacion-count-row').html(response.Capacitaciones.length)

        response.Capacitaciones.forEach(capacitacion => {
            $('#content_capacitacion').append(`<tr>
                <td>${capacitacion.CreadoFecha}</td>
                <td>${capacitacion.Tema}</td>
                <td>${capacitacion.Comentario}</td>
                <td></td>
            </tr>`)
        })

        let adjunto2 = 0
        $('#table_firma-count-row').html(adjunto2)
        response.Adjuntos.forEach(adjunto => {
            if(adjunto.Tipo == 2){
                $('#content_firma').append(`<tr>
                    <td>${adjunto.NombreArchivo}</td>
                    <td>${adjunto.CreadoFecha}</td>
                    <td>${adjunto.NombreArchivo}</td>
                    <td></td>
                </tr>`)
                adjunto2 = adjunto2 + 1
            }

            $('#table_firma-count-row').html(adjunto2)
        })
    });
}