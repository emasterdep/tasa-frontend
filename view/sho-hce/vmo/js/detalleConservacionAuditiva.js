function _initDetalleConservacionAuditiva(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    cargarDatos()
    getDetalle()
}

function getDetalle(){

    let storage = localStorage.getItem('UsuarioVMO')
    let id = 0

    if(storage != ''){
        let registro = JSON.parse(localStorage.getItem('UsuarioVMO'))
        id = registro.Id
    }else{
        id = localStorage.getItem('idVmoDetalle')
    }

    var settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=objectca&ConservacionAuditivaId="+id,
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        $('#diagnostico_audiometria_por_emo').val((response.DiagnosticoAudiometria == null) ? '' : response.DiagnosticoAudiometria )
        $('#expuestoRuido').append(`<option>${(response.ExpuestoRuido == 1) ? 'Si' : 'No'}</option>`)
        $('#nivelExposicion').append(`<option>${response.NivelExposicionDescripcion}</option>`)
        $('#valor').val(response.ValorExposicion)
        $('#UsoEppAuditivo').append(`<option>${(response.UsoEppAuditivo == 1) ? 'Si' : 'No' }</option>`)
        $('#tipoEppAuditivo').append(`<option>${response.TipoEppAuditivoDescripcion}</option>`)

        $('#ABI2000').val(response.ABI2000)
        $('#ABI3000').val(response.ABI3000)
        $('#ABI4000').val(response.ABI4000)

        $('#ABD2000').val(response.ABD2000)
        $('#ABD3000').val(response.ABD3000)
        $('#ABD4000').val(response.ABD4000)

        $('#oidoIzquierdo').append(`<option>${response.EstadoOidoIzquierdoDescripcion}</option>`)
        $('#oidoDerecho').append(`<option>${response.EstadoOidoDerechoDescripcion}</option>`)
        $('#otro').append(`<option>${(response.Otro == 1) ? 'Si' : 'No' }</option>`)
        $('#detalle_otros').val(`${response.DetalleOtro}`)

        $('#AAI125').val(response.AAI125)
        $('#AAI250').val(response.AAI250)
        $('#AAI500').val(response.AAI500)
        $('#AAI750').val(response.AAI750)
        $('#AAI1000').val(response.AAI1000)
        $('#AAI1500').val(response.AAI1500)
        $('#AAI2000').val(response.AAI2000)
        $('#AAI3000').val(response.AAI3000)
        $('#AAI4000').val(response.AAI4000)
        $('#AAI6000').val(response.AAI6000)
        $('#AAI8000').val(response.AAI8000)

        $('#AAD125').val(response.AAD125)
        $('#AAD250').val(response.AAD250)
        $('#AAD500').val(response.AAD500)
        $('#AAD750').val(response.AAD750)
        $('#AAD1000').val(response.AAD1000)
        $('#AAD1500').val(response.AAD1500)
        $('#AAD2000').val(response.AAD2000)
        $('#AAD3000').val(response.AAD3000)
        $('#AAD4000').val(response.AAD4000)
        $('#AAD6000').val(response.AAD6000)
        $('#AAD8000').val(response.AAD8000)

        $('#stsOD').val(response.STSOD)
        $('#InterpretacionStsOD').html(response.InterpretacionStsOD)
        $('#stsOI').val(response.STSOI)
        $('#InterpretacionStsOI').html(response.InterpretacionStsOI)
        $('#diagnostico_final').append(`<option>${response.DiagnosticoFinalDescripcion}</option>`)

        $('#PerdidaMonoauralOI').val(response.PerdidaMonoauralOI)
        $('#PerdidaMonoauralOD').val(response.PerdidaMonoauralOD)
        $('#Comentarios').val(response.Comentarios)

        $('#table_capacitacion-count-row').html(response.Capacitaciones.length)

        response.Capacitaciones.forEach(item => {
            $('#content_capacitacion').append(`<tr>
                <td>${item.CreadoFecha}</td>
                <td>${item.Tema}</td>
                <td>${item.Comentario}</td>
                <td></td>
            </tr>`)
        })

        let adjunto = 0
        let adjunto2 = 0

        if(response.Adjuntos.length == 0){
            $('#table_firma-count-row').html('0')
            $('#table_archivo-adjunto-count-row').html('0')
        }

        response.Adjuntos.forEach(item => {
            if(item.Tipo == 2){
                $('#content_firma').append(`<tr>
                    <td>${item.NombreArchivo}</td>
                    <td>${item.CreadoFecha}</td>
                    <td>${item.NombreArchivo}</td>
                </tr>`)
                adjunto = adjunto + 1
            }

            if(item.Tipo == 1){
                $('#content_archivo-adjunto').append(`<tr>
                    <td>${item.NombreArchivo}</td>
                    <td>${item.CreadoFecha}</td>
                    <td>${item.NombreArchivo}</td>
                </tr>`)
                adjunto2 = adjunto2 + 1
            }
            $('#table_firma-count-row').html(adjunto)
            $('#table_archivo-adjunto-count-row').html(adjunto2)
        })
    });
}