function getSedes() {
    let url = apiUrlssoma+`/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {
        sedeAreaGerencia = response;

        const setObjAreas = new Set(); // creamos pares de clave y array

        const Areas = response.Area.reduce((acc, area) => {
            if (!setObjAreas.has(area.Description.trim())){
                setObjAreas.add(area.Description.trim(), area)
                acc.push(area)
            }
            return acc;
        },[]);

        const setObjGerencia = new Set(); // creamos pares de clave y array

        const Gerencias = response.Gerencia.reduce((acc, gerencia) => {
            if (!setObjGerencia.has(gerencia.Description)){
                setObjGerencia.add(gerencia.Description, gerencia)
                acc.push(gerencia)
            }
            return acc;
        },[]);

        const setObjSede = new Set(); // creamos pares de clave y array

        const Sedes = response.Sedes.reduce((acc, sede) => {
            if (!setObjSede.has(sede.Description)){
                setObjSede.add(sede.Description, sede)
                acc.push(sede)
            }
            return acc;
        },[]);

        Gerencias.forEach(item => {
            $('#gerencia-lista').append(`<option value="${item.Id}">${item.Description}</option>`)
        })

        Areas.forEach(item => {
            $('#area-lista').append(`<option value="${item.Id}">${item.Description}</option>`)
        })

        Sedes.forEach(item => {
            $('#planta-list').append(`<option value="${item.Id}">${item.Description}</option>`)
        })

    })

}

function listarTablaExport(){

    listaVMO.EstadisticoCalidadVida.forEach(item => {
        $('#listaExportCalidadVida').append(`
            <tr>
                <td>${item.Codigo}</td>
                <td>${item.A_DniPaciente}</td>
                <td>${item.A_NombresPaciente}</td>
                <td>${item.A_ApellidosPaciente}</td>
                <td>${item.gerencia}</td>
                <td>${item.ubicacion}</td>
                <td>${item.area}</td>
                <td>${item.B_PuestoTrabajo}</td>
                <td>${item.A_SexoDescripcion}</td>
                <td>${item.IMC}</td>
                <td>${(item.Dislipidemias == 1) ? 'Si' : 'No' }</td>
                <td>${item.CodeEmo}</td>
                <td>${item.FechaEmo}</td>
                <td>${item.TipoEmoDescripcion}</td>
                <td>${item.NivelSeguimientoDescripcion}</td>
            </tr>
        `)
    })

    listaVMO.EstadisticoConservacionAuditiva.forEach(item => {
            $('#listaExportConservacionAuditiva').append(`
                <tr>
                    <td>${item.Codigo}</td>
                    <td>${item.A_DniPaciente}</td>
                    <td>${item.A_NombresPaciente}</td>
                    <td>${item.A_ApellidosPaciente}</td>
                    <td>${item.gerencia}</td>
                    <td>${item.ubicacion}</td>
                    <td>${item.area}</td>
                    <td>${item.B_PuestoTrabajo}</td>
                    <td>${item.FechaEmo}</td>
                    <td>${item.TipoEmoDescripcion}</td>
                    <td>${(item.ExpuestoRuido == 1) ? 'Si' : 'No' }</td>
                    <td>${item.NivelExposicionDescripcion}</td>
                    <td>${item.STSOD}</td>
                    <td>${item.STSOI}</td>
                    <td>${item.DiagnosticoDescripcion}</td>
                </tr>
            `)
        })

    listaVMO.EstadisticoTranstornoMusculo.forEach(item => {
            $('#listaExportTranstornoMusculoEsqueletico').append(`
                <tr>
                    <td>${item.Codigo}</td>
                    <td>${item.A_DniPaciente}</td>
                    <td>${item.A_NombresPaciente}</td>
                    <td>${item.A_ApellidosPaciente}</td>
                    <td>${item.gerencia}</td>
                    <td>${item.ubicacion}</td>
                    <td>${item.area}</td>
                    <td>${item.B_PuestoTrabajo}</td>
                    <td>${item.FechaEmo}</td>
                    <td>${item.TipoEmoDescripcion}</td>
                    <td>${item.AntecedentesOseoMusculares}</td>
                    <td>${item.DiagnosticoFinal}</td>
                </tr>                
            `)
        })

}

function html_table_to_excel(type,nombreFile)
{
    var data = document.getElementById('listaExportCalidadVida');

    var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});

    XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

    XLSX.writeFile(file, nombreFile+'.'+ type);
}

// const export_button = document.getElementById('btn-export');

function exportCalidadVida(){
    html_table_to_excel('xlsx','Calidad-de-vida');
}

function html_table_to_excel2(type,nombreFile)
{
    var data = document.getElementById('listaExportConservacionAuditiva');

    var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});

    XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

    XLSX.writeFile(file, nombreFile+'.'+ type);
}

// const export_button2 = document.getElementById('btn-export2');

function exportConservacionAuditiva(){
    html_table_to_excel2('xlsx','Conservación-auditiva');
}

function html_table_to_excel3(type,nombreFile)
{
    var data = document.getElementById('listaExportTranstornoMusculoEsqueletico');

    var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});

    XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

    XLSX.writeFile(file, nombreFile+'.'+ type);
}

// const export_button3 = document.getElementById('btn-export3');

function exportTranstornoMusculoEsqueletico(){
    html_table_to_excel3('xlsx','Transtorno-músculo-esquelético');
}

function estadisticoCalidadVida(Id,IdHC,Codigo,A_DniPaciente,A_NombresPaciente,A_ApellidosPaciente,gerencia,ubicacion,area,B_PuestoTrabajo,A_SexoDescripcion,IMC,Dislipidemias,CodeEmo,FechaEmo,TipoEmoDescripcion,NivelSeguimientoDescripcion){
    this.Id = Id
    this.IdHC = IdHC
    this.Codigo = Codigo
    this.A_DniPaciente = A_DniPaciente
    this.A_NombresPaciente = A_NombresPaciente
    this.A_ApellidosPaciente = A_ApellidosPaciente
    this.gerencia = gerencia
    this.ubicacion = ubicacion
    this.area = area
    this.B_PuestoTrabajo = B_PuestoTrabajo
    this.A_SexoDescripcion = A_SexoDescripcion
    this.IMC = IMC
    this.Dislipidemias = Dislipidemias
    this.CodeEmo = CodeEmo
    this.FechaEmo = FechaEmo
    this.TipoEmoDescripcion = TipoEmoDescripcion
    this.NivelSeguimientoDescripcion = NivelSeguimientoDescripcion
}

function EstadisticoConservacionAuditiva(Id,IdHC,Codigo,A_DniPaciente,A_NombresPaciente,A_ApellidosPaciente,gerencia,ubicacion,area,B_PuestoTrabajo,FechaEmo,TipoEmoDescripcion,ExpuestoRuido,NivelExposicionDescripcion,STSOD,STSOI,DiagnosticoDescripcion,CodeEmo,NivelSeguimientoDescripcion){
    this.Id = Id
    this.IdHC = IdHC
    this.Codigo = Codigo
    this.A_DniPaciente = A_DniPaciente
    this.A_NombresPaciente = A_NombresPaciente
    this.A_ApellidosPaciente = A_ApellidosPaciente
    this.gerencia = gerencia
    this.ubicacion = ubicacion
    this.area = area
    this.B_PuestoTrabajo = B_PuestoTrabajo
    this.FechaEmo = FechaEmo
    this.ExpuestoRuido = ExpuestoRuido
    this.TipoEmoDescripcion = TipoEmoDescripcion
    this.NivelExposicionDescripcion = NivelExposicionDescripcion
    this.STSOD = STSOD
    this.STSOI = STSOI
    this.DiagnosticoDescripcion = DiagnosticoDescripcion
    this.CodeEmo = CodeEmo
    this.NivelSeguimientoDescripcion = NivelSeguimientoDescripcion
}

function EstadisticoTranstornoMusculo(Id,IdHC,Codigo,A_DniPaciente,A_NombresPaciente,A_ApellidosPaciente,gerencia,ubicacion,area,B_PuestoTrabajo,FechaEmo,TipoEmoDescripcion,AntecedentesOseoMusculares,DiagnosticoFinal,CodeEmo,NivelSeguimientoDescripcion,TipoPuesto){
    this.Id = Id
    this.IdHC = IdHC
    this.Codigo = Codigo
    this.A_DniPaciente = A_DniPaciente
    this.A_NombresPaciente = A_NombresPaciente
    this.A_ApellidosPaciente = A_ApellidosPaciente
    this.gerencia = gerencia
    this.ubicacion = ubicacion
    this.area = area
    this.B_PuestoTrabajo = B_PuestoTrabajo
    this.FechaEmo = FechaEmo
    this.TipoEmoDescripcion = TipoEmoDescripcion
    this.AntecedentesOseoMusculares = AntecedentesOseoMusculares
    this.DiagnosticoFinal = DiagnosticoFinal
    this.CodeEmo = CodeEmo
    this.NivelSeguimientoDescripcion = NivelSeguimientoDescripcion
    this.TipoPuesto = TipoPuesto
}

function _initListaVMO(){
    getSedes().then(res => {
        listaGestionAll()
        localStorage.setItem('UsuarioVMO','')
        localStorage.setItem('idVMO',undefined)
        var settings = {
            url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=registroestadistico&CodigoVmo=&Gerencia=0&Area=0&PuestoTrabajo&Documento&CodigoEmo&Planta=0&Busqueda",
            method: "GET",
            dataType: "json",
            headers: {
                apikey: constantes.apiKey,
            },
        };

        return $.ajax(settings).done(function (response) {

            let lista = {EstadisticoCalidadVida: [], EstadisticoConservacionAuditiva: [], EstadisticoTranstornoMusculo: []}

            response.EstadisticoCalidadVida.forEach(item => {
                const gerencia = sedeAreaGerencia.Gerencia.filter(gerencia => {
                    return gerencia.Id == item.B_GerenciaId
                })

                const sede = sedeAreaGerencia.Sedes.filter(sede => {
                    return sede.Id == item.B_PlantaId
                })

                const area = sedeAreaGerencia.Area.filter(area => {
                    return area.Id == item.B_AreaId
                })

                lista.EstadisticoCalidadVida.push(new estadisticoCalidadVida(item.Id,item.IdHC,item.Codigo,item.A_DniPaciente,item.A_NombresPaciente,item.A_ApellidosPaciente,gerencia[0].Description,sede[0].Description,area[0].Description,item.B_PuestoTrabajo,item.A_SexoDescripcion,item.IMC,item.Dislipidemias,item.CodeEmo,item.FechaEmo,item.TipoEmoDescripcion,item.NivelSeguimientoDescripcion))
            })

            response.EstadisticoConservacionAuditiva.forEach(item => {
                const gerencia = sedeAreaGerencia.Gerencia.filter(gerencia => {
                    return gerencia.Id == item.B_GerenciaId
                })

                const sede = sedeAreaGerencia.Sedes.filter(sede => {
                    return sede.Id == item.B_PlantaId
                })

                const area = sedeAreaGerencia.Area.filter(area => {
                    return area.Id == item.B_AreaId
                })

                lista.EstadisticoConservacionAuditiva.push(new EstadisticoConservacionAuditiva(item.Id,item.IdHC,item.Codigo,item.A_DniPaciente,item.A_NombresPaciente,item.A_ApellidosPaciente,gerencia[0].Description,sede[0].Description,area[0].Description,item.B_PuestoTrabajo,item.FechaEmo,item.TipoEmoDescripcion,item.ExpuestoRuido,item.NivelExposicionDescripcion,item.STSOD,item.STSOI,item.DiagnosticoDescripcion,item.CodeEmo,item.NivelSeguimientoDescripcion))
            })

            response.EstadisticoTranstornoMusculo.forEach(item => {
                const gerencia = sedeAreaGerencia.Gerencia.filter(gerencia => {
                    return gerencia.Id == item.B_GerenciaId
                })

                const sede = sedeAreaGerencia.Sedes.filter(sede => {
                    return sede.Id == item.B_PlantaId
                })

                const area = sedeAreaGerencia.Area.filter(area => {
                    return area.Id == item.B_AreaId
                })

                lista.EstadisticoTranstornoMusculo.push(new EstadisticoTranstornoMusculo(item.Id,item.IdHC,item.Codigo,item.A_DniPaciente,item.A_NombresPaciente,item.A_ApellidosPaciente,gerencia[0].Description,sede[0].Description,area[0].Description,item.B_PuestoTrabajo,item.FechaEmo,item.TipoEmoDescripcion,item.AntecedentesOseoMusculares,item.DiagnosticoFinal,item.CodeEmo,item.NivelSeguimientoDescripcion,item.TipoPuesto))
            })

            listaVMO = lista
            console.log('log listaVMO.EstadisticoCalidadVida----------------->' ,listaVMO.EstadisticoCalidadVida);
            console.log('log listaVMO.EstadisticoConservacionAuditiva----------------->' ,listaVMO.EstadisticoConservacionAuditiva);
            console.log('log listaVMO.EstadisticoTranstornoMusculo----------------->' ,listaVMO.EstadisticoTranstornoMusculo);
            $('#content_calidad_de_vida').html('')
            $('#cantidad').html(listaVMO.EstadisticoCalidadVida.length+' registros')

            simpleTemplating(listaVMO.EstadisticoCalidadVida);

            /*$('#pagination-container').pagination({
                dataSource: listaVMO.EstadisticoCalidadVida,
                pageSize: 5,
                callback: function(data, pagination) {
                    var html = simpleTemplating(data);
                    $('#content_calidad_de_vida').html(html);
                }
            })*/

            $('#content_conservacion_auditiva').html('')
            $('#cantidad2').html(listaVMO.EstadisticoConservacionAuditiva.length+' registros')

            simpleTemplating2(listaVMO.EstadisticoConservacionAuditiva);

            /*$('#pagination-container2').pagination({
                dataSource: listaVMO.EstadisticoConservacionAuditiva,
                pageSize: 5,
                callback: function(data, pagination) {
                    var html = simpleTemplating2(data);
                    $('#content_conservacion_auditiva').html(html);
                }
            })*/

            $('#content_transtorno_musculo_esqueletico').html('')
            $('#cantidad3').html(listaVMO.EstadisticoTranstornoMusculo.length+' registros')

            simpleTemplating3(listaVMO.EstadisticoTranstornoMusculo);

            /*$('#pagination-container3').pagination({
                dataSource: listaVMO.EstadisticoTranstornoMusculo,
                pageSize: 5,
                callback: function(data, pagination) {
                    var html = simpleTemplating3(data);
                    $('#content_transtorno_musculo_esqueletico').html(html);
                }
            })*/
            $('.select2').select2()
            //listarTablaExport()
        });
    })

}

function listaGestionAll(){
    var settings = {
        url: apiUrlsho+"/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaCombosGestion_All",
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);

        const setObj = new Set(); // creamos pares de clave y array

        const puestos = response.lista_Cargos_Puestos_Trabajo.reduce((acc, puesto) => {
            if (!setObj.has(puesto.PuestoTrabajo)){
                setObj.add(puesto.PuestoTrabajo.trim(), puesto)
                acc.push(puesto)
            }
            return acc;
        },[]);

        console.log(puestos)

        puestos.forEach(item => {
            $('#puesto-de-trabajo').append(`<option value="${item.PuestoTrabajo}">${item.PuestoTrabajo}</option>`)
        })
    });
}

var ObjTablaCalidadVida = '';

function simpleTemplating(data) {

    //ESTO ELIMINA CUALQUIER FILTRO DATATABLE ACTIVO DESDE CUALQUIER OTRA VISTA
    $.fn.dataTable.ext.search.pop();
    ///////////////////////////////////////////////////////////////////////////

   var html = '';
    $.each(data, function(index, item){
        html = `<tr>
                <td>${item.Codigo}</td>
                <td>${item.A_DniPaciente}</td>
                <td>${item.A_NombresPaciente}</td>
                <td>${item.A_ApellidosPaciente}</td>
                <td>${item.gerencia}</td>
                <td>${item.ubicacion}</td>
                <td>${item.area}</td>
                <td>${item.B_PuestoTrabajo}</td>
                <td>${item.A_SexoDescripcion}</td>
                <td>${item.IMC}</td>
                <td>${(item.Dislipidemias == 1) ? 'Si' : 'No' }</td>
                <td>${item.CodeEmo}</td>
                <td>${item.FechaEmo}</td>
                <td>${item.TipoEmoDescripcion}</td>
                <td>${item.NivelSeguimientoDescripcion}</td>
                <td>
                    <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroLista(${item.Id})">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="VerProgresoLista(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </td>
        </tr>`;

        $('#content_calidad_de_vida').append(html);

    });

    ObjTablaCalidadVida = $('#table_calidad_de_vida').DataTable({
        "pageLength": 5,
        "ordering": false,
        "scrollX": true,
        "language": {
          "paginate": {
            "next": "»",
            "previous": "«"
          },
          "loadingRecords":"Cargando registros...",
          "processing":"Procesando...",
          "infoPostFix":"",
          "zeroRecords": "No se encontraron registros"
        },
        buttons:[
          {extend:"excel", titleAttr: 'Generar Excel',title:'Listado de registros de VMO (Calidad de Vida)', className:"btn btn-info btn-lg", text:'<i class="fa fa-file-excel fa-1x"></i>',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14/*, 15, 16, 17, 18, 19, 20*/ ]
          }}/*,
          {extend:"print",messageTop: 'Listado de registros de GES', title:'Listado de registros de GES',className:"btn btn-info btn-lg", text:'<i class="fa fa-print fa-1x"></i>',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
          }},
          {extend:"pdf",messageTop: 'Listado de registros de GES', title:'Listado de registros de GES',className:"btn btn-info btn-lg", text:'<i class="fa fa-file-pdf fa-1x"></i>', orientation: 'landscape',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
          }}*/
        ],
        initComplete: function() {
            $('body').find('.dataTables_scrollBody').addClass("scrollbar");
        },
        dom:
          "<'row btn-table'<B>>"+
          "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>"+
          "<'row row-records'<'col-sm-12'tr>>"+
          "<'row row-info'<'col-sm-12 col-md-12'p>>"
      });
      
    $('.scrollbar').removeClass('dataTables_scrollBody'); 

    //ESTO ES NUEVO. PARA EVITAR EL NO ALINEAMIENTO DENTRE HEADER Y BODY
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

    /*var html = '<tr>';
    $.each(data, function(index, item){
        html += `<tr>
                <td>${item.Codigo}</td>
                <td>${item.A_DniPaciente}</td>
                <td>${item.A_NombresPaciente}</td>
                <td>${item.A_ApellidosPaciente}</td>
                <td>${item.gerencia}</td>
                <td>${item.ubicacion}</td>
                <td>${item.area}</td>
                <td>${item.B_PuestoTrabajo}</td>
                <td>${item.A_SexoDescripcion}</td>
                <td>${item.IMC}</td>
                <td>${(item.Dislipidemias == 1) ? 'Si' : 'No' }</td>
                <td>${item.CodeEmo}</td>
                <td>${item.FechaEmo}</td>
                <td>${item.TipoEmoDescripcion}</td>
                <td>${item.NivelSeguimientoDescripcion}</td>
                <td>
                    <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroLista(${item.Id})">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="VerProgresoLista(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </td>
        </tr>`;
    });
    html += '</tr>';
    return html;*/
}

var ObjTablaConservacionAuditiva = '';

function simpleTemplating2(data) {

    //ESTO ELIMINA CUALQUIER FILTRO DATATABLE ACTIVO DESDE CUALQUIER OTRA VISTA
    $.fn.dataTable.ext.search.pop();
    ///////////////////////////////////////////////////////////////////////////

    var html = '';
    $.each(data, function(index, item){
        html = `<tr>
                <td>${item.Codigo}</td>
                <td>${item.A_DniPaciente}</td>
                <td>${item.A_NombresPaciente}</td>
                <td>${item.A_ApellidosPaciente}</td>
                <td>${item.gerencia}</td>
                <td>${item.ubicacion}</td>
                <td>${item.area}</td>
                <td>${item.B_PuestoTrabajo}</td>
                <td>${item.FechaEmo}</td>
                <td>${item.TipoEmoDescripcion}</td>
                <td>${(item.ExpuestoRuido == 1) ? 'Si' : 'No' }</td>
                <td>${item.NivelExposicionDescripcion}</td>
                <td>${item.STSOD}</td>
                <td>${item.STSOI}</td>
                <td>${item.DiagnosticoDescripcion}</td>
                <td>
                    <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroListaAuditiva(${item.Id})">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="VerProgresoListaAuditiva(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </td>
        </tr>`;

        $('#content_conservacion_auditiva').append(html);
    });

    ObjTablaConservacionAuditiva = $('#table_conservacion_auditiva').DataTable({
        "pageLength": 5,
        "ordering": false,
        "scrollX": true,
        "language": {
          "paginate": {
            "next": "»",
            "previous": "«"
          },
          "loadingRecords":"Cargando registros...",
          "processing":"Procesando...",
          "infoPostFix":"",
          "zeroRecords": "No se encontraron registros"
        },
        buttons:[
          {extend:"excel", titleAttr: 'Generar Excel',title:'Listado de registros de VMO (Conservacion Auditiva)', className:"btn btn-info btn-lg", text:'<i class="fa fa-file-excel fa-1x"></i>',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14/*, 15, 16, 17, 18, 19, 20*/ ]
          }}/*,
          {extend:"print",messageTop: 'Listado de registros de GES', title:'Listado de registros de GES',className:"btn btn-info btn-lg", text:'<i class="fa fa-print fa-1x"></i>',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
          }},
          {extend:"pdf",messageTop: 'Listado de registros de GES', title:'Listado de registros de GES',className:"btn btn-info btn-lg", text:'<i class="fa fa-file-pdf fa-1x"></i>', orientation: 'landscape',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
          }}*/
        ],
        initComplete: function() {
            $('body').find('.dataTables_scrollBody').addClass("scrollbar");
        },
        dom:
          "<'row btn-table'<B>>"+
          "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>"+
          "<'row row-records'<'col-sm-12'tr>>"+
          "<'row row-info'<'col-sm-12 col-md-12'p>>"
      });
      
    $('.scrollbar').removeClass('dataTables_scrollBody'); 

    //ESTO ES NUEVO. PARA EVITAR EL NO ALINEAMIENTO DENTRE HEADER Y BODY
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

    /*var html = '<tr>';
    $.each(data, function(index, item){
        html += `<tr>
                <td>${item.Codigo}</td>
                <td>${item.A_DniPaciente}</td>
                <td>${item.A_NombresPaciente}</td>
                <td>${item.A_ApellidosPaciente}</td>
                <td>${item.gerencia}</td>
                <td>${item.ubicacion}</td>
                <td>${item.area}</td>
                <td>${item.B_PuestoTrabajo}</td>
                <td>${item.FechaEmo}</td>
                <td>${item.TipoEmoDescripcion}</td>
                <td>${(item.ExpuestoRuido == 1) ? 'Si' : 'No' }</td>
                <td>${item.NivelExposicionDescripcion}</td>
                <td>${item.STSOD}</td>
                <td>${item.STSOI}</td>
                <td>${item.DiagnosticoDescripcion}</td>
                <td>
                    <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroListaAuditiva(${item.Id})">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="VerProgresoListaAuditiva(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </td>
        </tr>`;
    });
    html += '</tr>';
    return html;*/
}

var ObjTablaTranstorno = '';

function simpleTemplating3(data) {

    //ESTO ELIMINA CUALQUIER FILTRO DATATABLE ACTIVO DESDE CUALQUIER OTRA VISTA
    $.fn.dataTable.ext.search.pop();
    ///////////////////////////////////////////////////////////////////////////

    var html = '';
    $.each(data, function(index, item){
        html = `<tr>
                <td>${item.Codigo}</td>
                <td>${item.A_DniPaciente}</td>
                <td>${item.A_NombresPaciente}</td>
                <td>${item.A_ApellidosPaciente}</td>
                <td>${item.gerencia}</td>
                <td>${item.ubicacion}</td>
                <td>${item.area}</td>
                <td>${item.B_PuestoTrabajo}</td>
                <td>${item.FechaEmo}</td>
                <td>${item.TipoEmoDescripcion}</td>
                <td>${item.AntecedentesOseoMusculares}</td>
                <td>${item.DiagnosticoFinal}</td>
                <td>
                    <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroTranstornoMusculoEsqueletico(${item.Id},${item.TipoPuesto})">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="VerProgresoTranstornoMusculoEsqueletico(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </td>
        </tr>`;

        $('#content_transtorno_musculo_esqueletico').append(html);
    });

    ObjTablaTranstorno = $('#table_transtorno_musculo_esqueletico').DataTable({
        "pageLength": 5,
        "ordering": false,
        "scrollX": true,
        "language": {
          "paginate": {
            "next": "»",
            "previous": "«"
          },
          "loadingRecords":"Cargando registros...",
          "processing":"Procesando...",
          "infoPostFix":"",
          "zeroRecords": "No se encontraron registros"
        },
        buttons:[
          {extend:"excel", titleAttr: 'Generar Excel',title:'Listado de registros de VMO (Transtorno Musculo Esqueletico)', className:"btn btn-info btn-lg", text:'<i class="fa fa-file-excel fa-1x"></i>',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11/*, 12, 13, 14, 15, 16, 17, 18, 19, 20*/ ]
          }}/*,
          {extend:"print",messageTop: 'Listado de registros de GES', title:'Listado de registros de GES',className:"btn btn-info btn-lg", text:'<i class="fa fa-print fa-1x"></i>',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
          }},
          {extend:"pdf",messageTop: 'Listado de registros de GES', title:'Listado de registros de GES',className:"btn btn-info btn-lg", text:'<i class="fa fa-file-pdf fa-1x"></i>', orientation: 'landscape',
              exportOptions: {
              columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
          }}*/
        ],
        initComplete: function() {
            $('body').find('.dataTables_scrollBody').addClass("scrollbar");
        },
        dom:
          "<'row btn-table'<B>>"+
          "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>"+
          "<'row row-records'<'col-sm-12'tr>>"+
          "<'row row-info'<'col-sm-12 col-md-12'p>>"
      });
      
    $('.scrollbar').removeClass('dataTables_scrollBody'); 

    //ESTO ES NUEVO. PARA EVITAR EL NO ALINEAMIENTO DENTRE HEADER Y BODY
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

    /*var html = '<tr>';
    $.each(data, function(index, item){
        html += `<tr>
                <td>${item.Codigo}</td>
                <td>${item.A_DniPaciente}</td>
                <td>${item.A_NombresPaciente}</td>
                <td>${item.A_ApellidosPaciente}</td>
                <td>${item.gerencia}</td>
                <td>${item.ubicacion}</td>
                <td>${item.area}</td>
                <td>${item.B_PuestoTrabajo}</td>
                <td>${item.FechaEmo}</td>
                <td>${item.TipoEmoDescripcion}</td>
                <td>${item.AntecedentesOseoMusculares}</td>
                <td>${item.DiagnosticoFinal}</td>
                <td>
                    <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroTranstornoMusculoEsqueletico(${item.Id},${item.TipoPuesto})">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="VerProgresoTranstornoMusculoEsqueletico(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </td>
        </tr>`;
    });
    html += '</tr>';
    return html;*/
}

function VerProgresoLista(Id){
    actualPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backTitle = "Registro Estadístico de VMO";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/verProgresoVMO.html', 'Progreso calidad de vida');
    $("#regresar").show();
    $("#logoCompany1 b").text('Progreso calidad de vida');
    //localStorage.setItem('UsuarioVMO', item)]
    // localStorage.setItem('idVMO',Id);
    listaVMO.EstadisticoCalidadVida.forEach(item => {
        if(item.Id == Id){
            localStorage.setItem('UsuarioVMO', JSON.stringify(item))
            localStorage.setItem('idVMO',item.IdHC)
        }
    })
}

function VerProgresoListaAuditiva(Id){
    listaVMO.EstadisticoConservacionAuditiva.forEach(item => {
        if(item.Id == Id){
            localStorage.setItem('UsuarioVMO', JSON.stringify(item))
            localStorage.setItem('idVMO',item.IdHC)
        }
    })
    actualPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backTitle = "Registro Estadístico de VMO";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/verProgresoCA.html', 'Progreso conservación auditiva');
    $("#regresar").show();
    $("#logoCompany1 b").text('Progreso conservación auditiva');
}

function VerProgresoTranstornoMusculoEsqueletico(Id){
    listaVMO.EstadisticoTranstornoMusculo.forEach(item => {
        if(item.Id == Id){
            localStorage.setItem('UsuarioVMO', JSON.stringify(item))
            localStorage.setItem('idVMO',item.IdHC)
        }
    })
    actualPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backTitle = "Registro Estadístico de VMO";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/verProgresoTME.html', 'Progreso TME');
    $("#regresar").show();
    $("#logoCompany1 b").text('Progreso TME');
}

function verRegistroLista(Id){
    localStorage.setItem('idRegistroVMO', Id);
    listaVMO.EstadisticoCalidadVida.forEach(item => {
        if(item.Id == Id){
            localStorage.setItem('UsuarioVMO', JSON.stringify(item))
            localStorage.setItem('idVMO',item.IdHC)
        }
    })
    actualPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backTitle = "Registro Estadístico de VMO";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/calidadVida.html', 'Detalle del registro calidad de vida');
    $("#regresar").show();
    $("#logoCompany1 b").text('Detalle del registro calidad de vida');
}

function verRegistroListaAuditiva(Id){
    listaVMO.EstadisticoConservacionAuditiva.forEach(item => {
        if(item.Id == Id){
            localStorage.setItem('UsuarioVMO', JSON.stringify(item))
            localStorage.setItem('idVMO',item.IdHC)
        }
    })
    actualPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backTitle = "Registro Estadístico de VMO";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/registroConservacionAuditiva.html', 'Detalle del registro conservación auditiva');
    $("#regresar").show();
    $("#logoCompany1 b").text('Detalle del registro conservación auditiva');
}

function verRegistroTranstornoMusculoEsqueletico(Id,TipoPuesto){
    localStorage.setItem('idRegistroVMO', Id);

    listaVMO.EstadisticoTranstornoMusculo.forEach(item => {
        if(item.Id == Id){
            localStorage.setItem('UsuarioVMO', JSON.stringify(item))
            localStorage.setItem('idVMO',item.IdHC)
        }
    })

    actualPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backPageSystemSoma = "view/sho-hce/vmo/listaVMO.html";
    backTitle = "Registro Estadístico de VMO";
    $("#regresar").show();

    if(TipoPuesto == 1){
        handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/TME_ADM.html', 'Detalle del registro TME ADM');
        $("#logoCompany1 b").text('Detalle del registro TME ADM');
    }else{
        handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/TME_OPE.html', 'Detalle del registro TME OPE');
        $("#logoCompany1 b").text('Detalle del registro TME OPE');
    }

}

function buscarReguistros(){

    let codigo_vmo = $('#codigo-vmo').val()
    let gerencia_lista = $('#gerencia-lista').val()
    let area_lista = $('#area-lista').val()
    let puesto_de_trabajo = $('#puesto-de-trabajo').val()
    let documento = $('#documento').val()
    let id_emo = $('#id_emo').val()
    let planta_list = $('#planta-list').val()
    let buscar_list = $('#buscar-list').val()


    var settings = {
        url: `${apiUrlsho}/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=registroestadistico&CodigoVmo=${codigo_vmo}&Gerencia=${gerencia_lista}&Area=${area_lista}&PuestoTrabajo=${puesto_de_trabajo}&Documento=${documento}&CodigoEmo=${id_emo}&Planta=${planta_list}&Busqueda=${buscar_list}`,
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {

        let lista = {EstadisticoCalidadVida: [], EstadisticoConservacionAuditiva: [], EstadisticoTranstornoMusculo: []}

        response.EstadisticoCalidadVida.forEach(item => {
            const gerencia = sedeAreaGerencia.Gerencia.filter(gerencia => {
                return gerencia.Id == item.B_GerenciaId
            })

            const sede = sedeAreaGerencia.Sedes.filter(sede => {
                return sede.Id == item.B_PlantaId
            })

            const area = sedeAreaGerencia.Area.filter(area => {
                return area.Id == item.B_AreaId
            })

            lista.EstadisticoCalidadVida.push(new estadisticoCalidadVida(item.Id,item.IdHC,item.Codigo,item.A_DniPaciente,item.A_NombresPaciente,item.A_ApellidosPaciente,gerencia[0].Description,sede[0].Description,area[0].Description,item.B_PuestoTrabajo,item.A_SexoDescripcion,item.IMC,item.Dislipidemias,item.CodeEmo,item.FechaEmo,item.TipoEmoDescripcion,item.NivelSeguimientoDescripcion))
        })

        response.EstadisticoConservacionAuditiva.forEach(item => {
            const gerencia = sedeAreaGerencia.Gerencia.filter(gerencia => {
                return gerencia.Id == item.B_GerenciaId
            })

            const sede = sedeAreaGerencia.Sedes.filter(sede => {
                return sede.Id == item.B_PlantaId
            })

            const area = sedeAreaGerencia.Area.filter(area => {
                return area.Id == item.B_AreaId
            })

            lista.EstadisticoConservacionAuditiva.push(new EstadisticoConservacionAuditiva(item.Id,item.IdHC,item.Codigo,item.A_DniPaciente,item.A_NombresPaciente,item.A_ApellidosPaciente,gerencia[0].Description,sede[0].Description,area[0].Description,item.B_PuestoTrabajo,item.FechaEmo,item.TipoEmoDescripcion,item.ExpuestoRuido,item.NivelExposicionDescripcion,item.STSOD,item.STSOI,item.DiagnosticoDescripcion))
        })

        response.EstadisticoTranstornoMusculo.forEach(item => {
            const gerencia = sedeAreaGerencia.Gerencia.filter(gerencia => {
                return gerencia.Id == item.B_GerenciaId
            })

            const sede = sedeAreaGerencia.Sedes.filter(sede => {
                return sede.Id == item.B_PlantaId
            })

            const area = sedeAreaGerencia.Area.filter(area => {
                return area.Id == item.B_AreaId
            })

            lista.EstadisticoTranstornoMusculo.push(new EstadisticoTranstornoMusculo(item.Id,item.IdHC,item.Codigo,item.A_DniPaciente,item.A_NombresPaciente,item.A_ApellidosPaciente,gerencia[0].Description,sede[0].Description,area[0].Description,item.B_PuestoTrabajo,item.FechaEmo,item.TipoEmoDescripcion,item.AntecedentesOseoMusculares,item.DiagnosticoFinal,item.TipoPuesto))
        })

        listaVMO = lista

        console.log('log '+response);
        $('#content_calidad_de_vida').html('')
        $('#cantidad').html(listaVMO.EstadisticoCalidadVida.length+' registros')

        $('#pagination-container').pagination({
            dataSource: listaVMO.EstadisticoCalidadVida,
            pageSize: 5,
            callback: function(data, pagination) {
                var html = simpleTemplating(data);
                $('#content_calidad_de_vida').html(html);
            }
        })

        $('#content_conservacion_auditiva').html('')
        $('#cantidad2').html(listaVMO.EstadisticoConservacionAuditiva.length+' registros')

        $('#pagination-container2').pagination({
            dataSource: listaVMO.EstadisticoConservacionAuditiva,
            pageSize: 5,
            callback: function(data, pagination) {
                var html = simpleTemplating2(data);
                $('#content_conservacion_auditiva').html(html);
            }
        })

        $('#content_transtorno_musculo_esqueletico').html('')
        $('#cantidad3').html(listaVMO.EstadisticoTranstornoMusculo.length+' registros')

        $('#pagination-container3').pagination({
            dataSource: listaVMO.EstadisticoTranstornoMusculo,
            pageSize: 5,
            callback: function(data, pagination) {
                var html = simpleTemplating3(data);
                $('#content_transtorno_musculo_esqueletico').html(html);
            }
        })

    });
}