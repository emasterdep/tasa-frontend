function _initProgresoCA(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    cargarDatos()
    getProgreso()
}

function getProgreso(){
    let EmoId = localStorage.getItem('idVMO');
    var settings = {
        url: apiUrlsho+`/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=progressca&EmoId=${EmoId}`,
        method: "GET",
        timeout: 0,
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);

        const diagnosticoFinal = response.map(item => {
            return {id: item.DiagnosticoFinal, nombre : item.DiagnosticoFinalDescripcion, CreadoFecha : item.CreadoFecha}
        })

        console.log(diagnosticoFinal)

        diagnosticoFinal.forEach(item => {
            $('#table_progreso-count-row').html(response.length)
            $('#table_hospitalizacion thead tr').append(`<th class="text-center">${item.CreadoFecha}</th>`)

            if(item.id == 1){
                $('#normoacusia').removeClass('d-none')
                $('#normoacusia').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
            }else{
                $('#normoacusia').append(`<td></td>`)
            }

            if(item.id == 17){
                $('#otras-alteraciones-oi').removeClass('d-none')
                $('#otras-alteraciones-oi').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)

            }else{
                $('#otras-alteraciones-oi').append(`<td></td>`)
            }

            if(item.id == 3){
                $('#trauma-acustico-od').removeClass('d-none')
                $('#trauma-acustico-od').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
            }else{
                $('#trauma-acustico-od').append(`<td></td>`)
            }

            if(item.id == 5){
                $('#trauma-acustico-oi').removeClass('d-none')
                $('#trauma-acustico-oi').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
            }else{
                $('#trauma-acustico-oi').append(`<td></td>`)
            }

            if(item.id == 14){
                $('#hipoacusia-por-ruido-od').removeClass('d-none')
                $('#hipoacusia-por-ruido-od').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
            }else{
                $('#hipoacusia-por-ruido-od').append(`<td></td>`)
            }

            if(item.id == 15){
                $('#hipoacusia-por-ruido-oi').removeClass('d-none')
                $('#hipoacusia-por-ruido-oi').append(`<td class="text-center" style="font-size: 18px; color: #8fbb02">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
                    </svg>
                </td>`)
            }else{
                $('#hipoacusia-por-ruido-oi').append(`<td></td>`)
            }


            $('#peso').append(`<td>${item.Peso} Kg</td>`)
            $('#glucosa').append(`<td>${item.Glucosa}</td>`)
            $('#dislipidemia').append(`<td>${item.Dislipidemias}</td>`)
            $('#colesterol').append(`<td>${(item.Colesterol == 1) ? 'Si' : 'No'}</td>`)
            $('#trigliceridos').append(`<td>${(item.Trigliceridos == 1) ? 'Si' : 'No'}</td>`)
            $('#imc').append(`<td>${item.IMC}</td>`)
            $('#perimetro-abdominal').append(`<td>${item.PerimetroAbdominal} Kg</td>`)
            $('#hdl').append(`<td>${item.HDL}</td>`)
            $('#ldl').append(`<td>${item.LDL}</td>`)
            $('#riesgo-cardiaco-vascular').append(`<td>${(item.RiesgoCardioVascularId == 1) ? 'Si' : 'No'}</td>`)
        })

    });
}