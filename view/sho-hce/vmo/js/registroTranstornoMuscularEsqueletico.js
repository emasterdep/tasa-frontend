function _initRegistroTranstornoMuscularEsqueletico(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    cargarDatos()
    generarData()
    cei10()
    const idVMO = localStorage.getItem('idVMO');
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    registroTranstornoMusculoEsqueletico = new dataTranstornoMusculoesqueletico(idVMO,"3","1","","","","1","1","1","1","1","1","1","1","1");
    registroTranstornoMusculoEsqueletico.MedicoCargo = usuario.fullusername
    registroTranstornoMusculoEsqueletico.CreadoPor = usuario.idhash
    registroTranstornoMusculoEsqueletico.ModificadoPor = usuario.idhash

    registroTranstornoMusculoEsqueleticoOperativo = new dataTranstornoMusculoesqueleticoOperativo(idVMO,"4","2","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","","","","","","","","");
    registroTranstornoMusculoEsqueleticoOperativo.MedicoCargo = usuario.fullusername
    registroTranstornoMusculoEsqueleticoOperativo.CreadoPor = usuario.idhash
    registroTranstornoMusculoEsqueleticoOperativo.ModificadoPor = usuario.idhash

    setDatosSintomas()
    setExploracion()
    setTest()
}

function setDatosSintomas(){
    //nuca cuello
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,12,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //hombros
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //codos
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,13,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,13,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //muñecas y manos
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //columna
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,5,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,5,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //caderas
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,14,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,14,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //rodillas
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,7,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,7,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //tobillo
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,9,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(1,9,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
}

function setExploracion(){
    //hombros
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //codos
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,13,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,13,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //muñecas
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,11,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,11,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //Manos y dedos
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //caderas
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,14,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,14,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //rodillas
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,7,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,7,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    //tobillo
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,9,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(2,9,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))

    //examen fisico - columna vertebral
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(3,5,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas.push(new Sintomas(3,5,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
}

function setTest(){
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(1,1,2,0))
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(1,1,1,0))

    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(2,1,2,0))
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(2,1,1,0))

    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(3,1,2,0))
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(3,1,1,0))

    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(4,1,2,0))
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(4,1,1,0))

    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(5,1,2,0))
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(5,1,1,0))

    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(6,2,2,0))
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(6,2,1,0))

    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(7,2,2,0))
    registroTranstornoMusculoEsqueleticoOperativo.Tests.push(new Test(7,2,1,0))
}

function cei10(){
    var settings = {
        url: apiUrlsho+"/api/hce_Get_021_CIE10_busqueda?code=Kr7q88AoJqtcFZLAx3w8cS7kZ8ezNaxCr/YUbbfUMvEQH1zUvDsxjg==&IdCIE10=&Descripcion=",
        method: "GET",
        dataType: "json"
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        datosCEI = response.CEI10
    });
}

function Adjuntos(NombreArchivo, Archivo, Tipo){
    this.Id = 0
    this.NombreArchivo = NombreArchivo
    this.Archivo = Archivo
    this.Tipo = Tipo
    this.Activo = 1
}

function Riesgos(RiesgoId,FlagActivo,Nombre,Repetitivo,Horas,Dias,EvaluacionId){
    this.Id = 0
    this.RiesgoId = RiesgoId
    this.FlagActivo = FlagActivo
    this.Nombre = Nombre
    this.Repetitivo = Repetitivo
    this.Horas = Horas
    this.Dias = Dias
    this.EvaluacionId = EvaluacionId
    this.Activo = 1
}

function Sintomas(Tipo,SeccionId,Area,Problema12,Incapacitado,Problema7,Abducc,Abduc,Rexion,Extens,RotExt,RotInt,Dolor04,Irrad,AlternacionMasaMuscular,Flexion,Extension,LateralizaIzquierda,LateralizaDerecha,RotacionIzquierda,RotacionDerecha,Nombre){
    this.Id = 0
    this.Tipo = Tipo
    this.SeccionId = SeccionId
    this.Area = Area
    this.Problema12 = Problema12
    this.Incapacitado = Incapacitado
    this.Problema7 = Problema7
    this.Abducc = Abducc
    this.Abduc = Abduc
    this.Rexion = Rexion
    this.Extens = Extens
    this.RotExt = RotExt
    this.RotInt = RotInt
    this.Dolor04 = Dolor04
    this.Irrad = Irrad
    this.AlternacionMasaMuscular = AlternacionMasaMuscular
    this.Flexion = Flexion
    this.Extension = Extension
    this.LateralizaIzquierda = LateralizaIzquierda
    this.LateralizaDerecha = LateralizaDerecha
    this.RotacionIzquierda = RotacionIzquierda
    this.RotacionDerecha = RotacionDerecha
    this.Nombre = Nombre
    this.Activo = 1
}

function Test(TestId,Tipo,Area,Signos){
    this.Id = 0
    this.TestId = TestId
    this.Tipo = Tipo
    this.Area = Area
    this.Signos = Signos
    this.Activo = 1
}

function DiagnosticosCIE10(CIE10,CIE10_Nombre,Diagnostico,SistemaAfectado,SeccionAfectada){
        this.Id = 0
        this.CIE10 = CIE10
        this.CIE10_Nombre = CIE10_Nombre
        this.Diagnostico = Diagnostico
        this.SistemaAfectado = SistemaAfectado
        this.SeccionAfectada = SeccionAfectada
        this.Activo = 1
}

function Capacitacion(tema, comentario){
    this.Id = 0;
    this.Tema = tema
    this.Comentario = comentario
    this.Activo = 1
}

function tablaDiagnosticoCIE(){

    let tipo_puesto = $('#tipo_puesto').val();
    let DiagnosticosCIE10 = []

    if(tipo_puesto == 1){
        DiagnosticosCIE10 = registroTranstornoMusculoEsqueletico.DiagnosticosCIE10
    }else{
        DiagnosticosCIE10 = registroTranstornoMusculoEsqueleticoOperativo.DiagnosticosCIE10
    }

    $('#content_diagnostico').html('')
    $('#table_diagnostico-count-row').html(DiagnosticosCIE10.length)
    DiagnosticosCIE10.forEach(item => {

        const diagnostico = datosCEI.filter(dato => {
            return dato.Code == item.CIE10
        })

        console.log("diagnostico => "+diagnostico.length)

        const general1 = general.SistemasAfectados.filter(itemGeneral => {
            return itemGeneral.Id == item.SistemaAfectado
        })

        const general2 = general.SeccionesAfectadas.filter(itemGeneral => {
            return itemGeneral.Id == item.SeccionAfectada
        })
        const date = new Date()
        if(diagnostico.length != 0){
            $('#content_diagnostico').append(`<tr>
                <td>${item.Diagnostico}</td>
                <td>${item.CIE10_Nombre}</td>
                <td>${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}/${(parseInt(date.getMonth() + 1) < 10) ? '0'+parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1)}/${date.getFullYear()}</td>
                <td>${diagnostico[0].Especilidades}</td>
                <td>${general1[0].Descripcion}</td>
                <td>${general2[0].Descripcion}</td>
            </tr>`)
        }else{
            $('#content_diagnostico').append(`<tr>
                <td>${item.Diagnostico}</td>
                <td>${item.CIE10_Nombre}</td>
                <td>${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}/${(parseInt(date.getMonth() + 1) < 10) ? '0'+parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1)}/${date.getFullYear()}</td>
                <td>--</td>
                <td>${general1[0].Descripcion}</td>
                <td>${general2[0].Descripcion}</td>
            </tr>`)
        }

    })
}

function agregarListaCapacitacionTME(){
    let tema = $('#tema').val();
    let comentario = $('#comentario').val();

    let tipo_puesto = $('#tipo_puesto').val();

    if(tipo_puesto == 1){
        registroTranstornoMusculoEsqueletico.Capacitaciones.push(new Capacitacion(tema,comentario))
    }else{
        registroTranstornoMusculoEsqueleticoOperativo.Capacitaciones.push(new Capacitacion(tema,comentario))
    }

    tablaCapacitacionTME()

    $('#tema').val('');
    $('#comentario').val('');
}

function tablaCapacitacionTME(){

    let tipo_puesto = $('#tipo_puesto').val();
    let Capacitaciones = []

    if(tipo_puesto == 1){
        Capacitaciones = registroTranstornoMusculoEsqueletico.Capacitaciones
    }else{
        Capacitaciones = registroTranstornoMusculoEsqueleticoOperativo.Capacitaciones
    }

    $('#table_capacitacion-count-row').html(Capacitaciones.length)
    console.log(Capacitaciones.length)
    $('#content_capacitacion').html('')
    Capacitaciones.forEach((capacitacion, index) => {
        $('#content_capacitacion').append(`
            <tr>
                <td>${index + 1}</td>
                <td>${capacitacion.Tema}</td>
                <td>${capacitacion.Comentario}</td>
                <td>
                    <img src="images/iconos/trash.svg" onclick="eliminarCapacitacionTME(${index})"/>
                </td>
            </tr>    
        `)
    })
}

function eliminarCapacitacionTME(index){
    registroTranstornoMusculoEsqueletico.Capacitaciones.splice(index, 1);
    tablaCapacitacionTME()
}

function setDatosTranstornoMuscular(campo, idCampo){

    let tipo_puesto = $('#tipo_puesto').val();

    if(tipo_puesto == 1){
        let valor = $(`#${idCampo}`).val()
        registroTranstornoMusculoEsqueletico[campo] = valor
    }else{
        let valor = $(`#${idCampo}`).val()
        registroTranstornoMusculoEsqueleticoOperativo[campo] = valor
    }

}

function dataTranstornoMusculoesqueletico(
    EmoId,
    TipoVMO,
    TipoPuesto,
    OtrosDiagnosticos,
    HorasDiariasSentado,
    AntecedentesOseoMusculares,
    ZonaColumnCervical,
    ZonaColumnDorsal,
    ZonaColumnLumbar,
    ZonaArtralgiaHombro,
    ZonaArtralgiaCodo,
    ZonaArtralgiaMano,
    ZonaArtralgiaCadera,
    ZonaArtralgiaRodilla,
    ZonaArtralgiaTobillos,
    MedicoCargo,
    CreadoPor,
    ModificadoPor){
    this.Id = 0
    this.EmoId = EmoId
    this.TipoVMO = TipoVMO
    this.TipoPuesto = TipoPuesto
    this.OtrosDiagnosticos = OtrosDiagnosticos
    this.HorasDiariasSentado = HorasDiariasSentado
    this.AntecedentesOseoMusculares = AntecedentesOseoMusculares
    this.ZonaColumnCervical = ZonaColumnCervical
    this.ZonaColumnDorsal = ZonaColumnDorsal
    this.ZonaColumnLumbar = ZonaColumnLumbar
    this.ZonaArtralgiaHombro = ZonaArtralgiaHombro
    this.ZonaArtralgiaCodo = ZonaArtralgiaCodo
    this.ZonaArtralgiaMano = ZonaArtralgiaMano
    this.ZonaArtralgiaCadera = ZonaArtralgiaCadera
    this.ZonaArtralgiaRodilla = ZonaArtralgiaRodilla
    this.ZonaArtralgiaTobillos = ZonaArtralgiaTobillos
    this.MedicoCargo = MedicoCargo
    this.Activo = 1
    this.CreadoPor = CreadoPor
    this.ModificadoPor = ModificadoPor
    this.Adjuntos = []
    this.Capacitaciones = []
    this.DiagnosticosCIE10 = []
}

function dataTranstornoMusculoesqueleticoOperativo(
    EmoId,
    TipoVMO,
    TipoPuesto,
    ETCadmio,
    ETMercurio,
    ETManganeso,
    EOManganeso,
    EOEquilibrio,
    EOManipulacion,
    EFExige,
    EFExiste,
    EFCuerpo,
    EFAlzar,
    CAEspacio,
    CASueloIrregular,
    CAAltura,
    CAPostulas,
    CADesniveles,
    CASuelo,
    CATemperatura,
    CAVibraciones,
    EAEsfuerzos,
    EAPeriodo,
    EADistancia,
    EARitmo,
    MPFaja,
    MPMedios,
    MPDiseño,
    MPPausa,
    ComentariosExamenFisico,
    SimetriaMMS,
    ObservacionSimetriaMMS,
    SimetriaMMLL,
    ObservacionSimetriaMMLL,
    ComentariosPalpacion,
    ComentariosCAED,
    ComentariosCCM,
    ComentariosCervical,
    ComentariosDAED,
    ComentariosDCM,
    ComentariosDorsal,
    ComentariosLAED,
    ComentariosLCM,
    ComentariosLumbar,
    MedicoCargo,
    CreadoPor,
    ModificadoPor){
    this.Id = 0
    this.EmoId = EmoId
    this.TipoVMO = TipoVMO
    this.ETCadmio = ETCadmio
    this.ETMercurio = ETMercurio
    this.ETManganeso = ETManganeso
    this.EOManganeso = EOManganeso
    this.EOEquilibrio = EOEquilibrio
    this.EOManipulacion = EOManipulacion
    this.EFExige = EFExige
    this.EFExiste = EFExiste
    this.EFCuerpo = EFCuerpo
    this.EFAlzar = EFAlzar
    this.CAEspacio = CAEspacio
    this.CASueloIrregular = CASueloIrregular
    this.CAAltura = CAAltura
    this.CAPostulas = CAPostulas
    this.CADesniveles = CADesniveles
    this.CASuelo = CASuelo
    this.CATemperatura = CATemperatura
    this.CAVibraciones = CAVibraciones
    this.EAEsfuerzos = EAEsfuerzos
    this.EAPeriodo = EAPeriodo
    this.EADistancia = EADistancia
    this.EARitmo = EARitmo
    this.MPFaja = MPFaja
    this.MPMedios = MPMedios
    this.MPDiseño = MPDiseño
    this.MPPausa = MPPausa
    this.ComentariosExamenFisico = ComentariosExamenFisico
    this.SimetriaMMS = SimetriaMMS
    this.ObservacionSimetriaMMS = ObservacionSimetriaMMS
    this.SimetriaMMLL = SimetriaMMLL
    this.ObservacionSimetriaMMLL = ObservacionSimetriaMMLL
    this.ComentariosPalpacion = ComentariosPalpacion
    this.ComentariosCAED = ComentariosCAED
    this.ComentariosCCM = ComentariosCCM
    this.ComentariosCervical = ComentariosCervical
    this.ComentariosDAED = ComentariosDAED
    this.ComentariosDCM = ComentariosDCM
    this.ComentariosDorsal = ComentariosDorsal
    this.ComentariosLAED = ComentariosLAED
    this.ComentariosLCM = ComentariosLCM
    this.ComentariosLumbar = ComentariosLumbar
    this.MedicoCargo = MedicoCargo
    this.Activo = 1
    this.CreadoPor = CreadoPor
    this.ModificadoPor = ModificadoPor
    this.Adjuntos = []
    this.Capacitaciones = []
    this.DiagnosticosCIE10 = []
    this.Riesgos = []
    this.Sintomas = []
    this.Tests = []
}

function riesgosSetDatos(index,campo,tipo,valor){
    // cambio btn activo diseño

    if(valor == 0){
        $(`#${campo}-${index}-0`).addClass('active-no')
        $(`#${campo}-${index}-1`).removeClass('active-si')
    }else{
        $(`#${campo}-${index}-0`).removeClass('active-no')
        $(`#${campo}-${index}-1`).addClass('active-si')
    }

    if(tipo == 1){
        valor = $(`#${campo}-${index}`).val();
    }

    registroTranstornoMusculoEsqueleticoOperativo.Riesgos[index][campo] = valor
}

function generarData(){
    var settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=generaldata",
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response.SeccionesAfectadas);
        general = response
        response.SistemasAfectados.forEach(item => {
            $('#dat_am_sistema_diagnostico').append(`<option value="${item.Id}">${item.Descripcion}</option>`)
        })

        response.SeccionesAfectadas.forEach(item => {
            if(item.Descripcion != ''){
                $('#dat_am_seccion_diagnostico').append(`<option value="${item.Id}">${item.Descripcion}</option>`)
            }
        })

        response.RiesgosEvaluacionTranstosno.forEach(item => {
            registroTranstornoMusculoEsqueleticoOperativo.Riesgos.push(new Riesgos(item.Id,0,item.Descripcion,0,0,0,1))
        })

        registroTranstornoMusculoEsqueleticoOperativo.Riesgos.forEach((riesgo, index) => {
            $('#listaRiesgos').append(`<div class="row mb-2 align-items-center">
                    <div class="col-md-1">
                        <p class="m-0">${riesgo.Nombre}</p>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA"
                                        id="FlagActivo-${index}-1"
                                        onclick="riesgosSetDatos(${index},'FlagActivo',2,1)">Si</label>
                                <input class="d-none" type="radio" name="hta" value="Si" id="reg_cali_vida_datos_HTA_si">
                            </div>
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA ml-2 active-no"
                                        id="FlagActivo-${index}-0"
                                        onclick="riesgosSetDatos(${index},'FlagActivo',2,0)">No</label>
                                <input class="d-none" type="radio" name="hta" value="Si" id="reg_cali_vida_datos_HTA_no">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <p class="m-0">Repetitivo</p>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA"
                                        id="Repetitivo-${index}-1"
                                        onclick="riesgosSetDatos(${index},'Repetitivo',2,1)">Si</label>
                                <input class="d-none" type="radio" name="hta" value="Si" id="reg_cali_vida_datos_HTA_si">
                            </div>
                            <div class="">
                                <label
                                        for="reg_cali_vida_datos_HTA_si"
                                        class="label-radio select_reg_cali_vida_datos_HTA ml-2 active-no"
                                        id="Repetitivo-${index}-0"
                                        onclick="riesgosSetDatos(${index},'Repetitivo',2,0)">No</label>
                                <input class="d-none" type="radio" name="hta" value="Si" id="reg_cali_vida_datos_HTA_no">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control input_form_control" placeholder="Horas" id="Horas-${index}" value="${riesgo.Horas}" onkeyup="riesgosSetDatos(${index},'Horas',1,0)">
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control input_form_control" placeholder="Días" id="Dias-${index}" value="${riesgo.Dias}" onkeyup="riesgosSetDatos(${index},'Dias',1,0)">
                    </div>
                    <div class="col-md-4">
                        <select name="" id="EvaluacionId-${index}" class="w-100 select_form" onchange="riesgosSetDatos(${index},'EvaluacionId',1,0)">
                        </select>
                    </div>
                </div>`)

            response.EvaluacionesRiesgo.forEach(evaluacion => {
                $('#EvaluacionId-'+index).append(`<option value="${evaluacion.Id}">${evaluacion.Descripcion}</option>`)
            })

        })

    });
}

function agregarDiagnosticoTME(){
    let diagnostico = $('#dat_am_diagnostico_diagnostico').val();
    let cie = $('#dat_am_cie_diagnostico2').val();
    let sistemaAfectado = $('#dat_am_sistema_diagnostico').val();
    let seccionAfectada = $('#dat_am_seccion_diagnostico').val();
    let cie_nombre = $('#dat_am_cie_diagnostico1').val();

    let tipo_puesto = $('#tipo_puesto').val();

    if(tipo_puesto == 1){
        registroTranstornoMusculoEsqueletico.DiagnosticosCIE10.push(new DiagnosticosCIE10(cie_nombre,cie,diagnostico,sistemaAfectado,seccionAfectada))
    }else{
        registroTranstornoMusculoEsqueleticoOperativo.DiagnosticosCIE10.push(new DiagnosticosCIE10(cie_nombre,cie,diagnostico,sistemaAfectado,seccionAfectada))
    }

    $('#dat_am_diagnostico_diagnostico').val('');
    $('#dat_am_cie_diagnostico2').val('');
    $('#dat_am_sistema_diagnostico').val('');
    $('#dat_am_seccion_diagnostico').val('');
    $('#dat_am_cie_diagnostico1').val('');

    tablaDiagnosticoCIE();

}

function AdjuntarArchivoME(id,tipo){
    let file = $(`#${id}`)[0].files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        registroTranstornoMusculoEsqueletico.Adjuntos.push(new Adjuntos(file.name,reader.result,tipo))
        registroTranstornoMusculoEsqueleticoOperativo.Adjuntos.push(new Adjuntos(file.name,reader.result,tipo))
        listaTablaAdjunto()
    }
    reader.readAsDataURL(file);
}

function listaTablaAdjunto(){
    const adjunto_archivo = registroTranstornoMusculoEsqueletico.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 1
    })

    const adjunto_firma = registroTranstornoMusculoEsqueletico.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 2
    })

    const adjunto_ficha = registroTranstornoMusculoEsqueletico.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 3
    })

    if(adjunto_firma.length > 0){
        $('#lista-adjunto-firma').removeClass('d-none')
    }

    if(adjunto_archivo.length > 0){
        $('#lista-adjunto-archivos').removeClass('d-none')
    }

    if(adjunto_ficha.length > 0){
        $('#lista-adjunto-fichas').removeClass('d-none')
    }

    $('#table_adjunto_firma-count-row').html(adjunto_firma.length)
    $('#table_adjunto-archivos-count-row').html(adjunto_archivo.length)
    $('#table_adjunto_fichas-count-row').html(adjunto_ficha.length)

    $('#adjunto_firma').pagination({
        dataSource: adjunto_firma,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_firma').html(html);
        }
    })

    $('#pagination-adjunto-archivos').pagination({
        dataSource: adjunto_archivo,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto-archivos').html(html);
        }
    })

    $('#paginacion_adjunto_fichas').pagination({
        dataSource: adjunto_ficha,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_fichas').html(html);
        }
    })
}

function simpleTemplatingAdjunto(data){
    const date = new Date();
    let CreadoFecha = `${date.getDate()}/${((parseInt(date.getMonth()) + 1) < 10) ? '0'+(parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1}/${date.getFullYear()}`
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.NombreArchivo}</td>
                <td>${CreadoFecha}</td>
                <td>
                    <button onclick="eliminarAdjunto('${item.NombreArchivo}')" id="hc_btn_borrar_evidencia_31" type="button" class="btn btn-link shadow-none float-right">
                      <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                    </button>
                    <a class="btn btn-link shadow-none float-right" href="${item.Archivo}" download="${item.NombreArchivo}">
                      <img class="inject-svg" src="./images/sho/download.svg" fill="#207345" style="width:16px !important">
                    </a>
                </td>
            </tr>
        `
        });
    }else{
        html += ` <tr>
            <td className="text-center" colSpan="5" style="text-align: center">No se encontro resultados</td>
        </tr>`
    }
    return html;
}

function eliminarAdjunto(nombre){

    let tipo_puesto = $('#tipo_puesto').val();
    let datosBusqueda = null

    if(tipo_puesto == 1){
        datosBusqueda = registroTranstornoMusculoEsqueletico.Adjuntos
    }else{
        datosBusqueda = registroTranstornoMusculoEsqueleticoOperativo.Adjuntos
    }
    console.log('datosBusqueda',datosBusqueda)
    datosBusqueda.forEach((adjunto, index) => {
        if(adjunto.NombreArchivo == nombre){
            datosBusqueda.splice(index,1)
            listaTablaAdjunto()
        }
    })
}


function guardarTranstornoMusculoEsqueletico(){
    let tipo_puesto = $('#tipo_puesto').val();

    if(tipo_puesto == 1){
        guardarAdministrador()
    }else{
        guardarOperativo()
    }
}

function guardarAdministrador(){
    var settings = {
        url: apiUrlsho+"/api/hce_Post_vmo?code=aOqdZIaCcrb03DFAYLAEOuaE4YmZywydiZ8tVakK26za/CWLzZck/w==&httpmethod=postTME",
        method: "POST",
        headers: {
            apikey: constantes.apiKey,
            "Content-Type": "application/json"
        },
        data: JSON.stringify(registroTranstornoMusculoEsqueletico)
    };

    if(registroTranstornoMusculoEsqueletico.HorasDiariasSentado == '' && registroTranstornoMusculoEsqueletico.AntecedentesOseoMusculares == '' && registroTranstornoMusculoEsqueletico.Capacitaciones.length == 0 && registroTranstornoMusculoEsqueletico.DiagnosticosCIE10.length == 0){
        Swal.fire({
            title: 'Se ha detectado campos que faltan ser rellenado',
            icon: 'error',
            showConfirmButton: false,
            timer: 2500
        })
    }else{
        let datos = paObj_hc[idHC].a;
        Swal.fire({
            title: 'Guardar registro de TME ADM',
            html: `<p>Está por guardar el registro de TME ADM de: ${datos.Nombres_Trabajador_H} ${datos.Apellidos_Trabajador_H}</p> <p class="mt-4">¿Desea confirmar la acción?</p>`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#8fbb02',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar <img src="./images/sho/confirm.svg" />',
            cancelButtonText: 'Cancelar <img src="./images/sho/cancelar.svg" />',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                showLoading();
                $.ajax(settings).done(function (response) {
                    console.log(response)
                    showLoading();
                    Swal.fire({
                        title: 'Se guardó el registro con éxito',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 2500
                    })
                    atrasBack()
                });
            }
        })
    }

}

function guardarOperativo(){
    var settings = {
        url: apiUrlsho+"/api/hce_Post_vmo?code=aOqdZIaCcrb03DFAYLAEOuaE4YmZywydiZ8tVakK26za/CWLzZck/w==&httpmethod=postTMEO",
        method: "POST",
        headers: {
            apikey: constantes.apiKey,
            "Content-Type": "application/json"
        },
        data: JSON.stringify(registroTranstornoMusculoEsqueleticoOperativo)
    };

    if(registroTranstornoMusculoEsqueleticoOperativo.ComentariosCAED == '' && registroTranstornoMusculoEsqueleticoOperativo.ComentariosCCM == '' && registroTranstornoMusculoEsqueleticoOperativo.ComentariosDAED == '' && registroTranstornoMusculoEsqueleticoOperativo.ComentariosDCM == '' && registroTranstornoMusculoEsqueleticoOperativo.ComentariosLAED == '' && registroTranstornoMusculoEsqueleticoOperativo.ComentariosLCM == '' && registroTranstornoMusculoEsqueletico.Capacitaciones.length == 0 && registroTranstornoMusculoEsqueletico.DiagnosticosCIE10.length == 0){
        Swal.fire({
            title: 'Se ha detectado campos que faltan ser rellenado',
            icon: 'error',
            showConfirmButton: false,
            timer: 2500
        })
    }else{
        let datos = paObj_hc[idHC].a;
        Swal.fire({
            title: 'Guardar registro de TME OPE',
            html: `<p>Está por guardar el registro de TME OPE de: ${datos.Nombres_Trabajador_H} ${datos.Apellidos_Trabajador_H}</p> <p class="mt-4">¿Desea confirmar la acción?</p>`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#8fbb02',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar <img src="./images/sho/confirm.svg" />',
            cancelButtonText: 'Cancelar <img src="./images/sho/cancelar.svg" />',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                showLoading();
                $.ajax(settings).done(function (response) {
                    console.log(response)
                    showLoading();
                    Swal.fire({
                        title: 'Se guardó el registro con éxito',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 2500
                    })
                    atrasBack()
                });
            }
        })
    }

}

function tipoDePuesto(){
    setDatosTranstornoMuscular('TipoPuesto','tipo_puesto')
    let opcion = $('#tipo_puesto').val();
    if(opcion == 1){
        $('.operativo').removeClass('d-none')
        $('.administrativo').addClass('d-none')
        $("#logoCompany1 b").text('Registro de TME ADM');
    }else{
        $('.operativo').addClass('d-none')
        $('.administrativo').removeClass('d-none')
        $("#logoCompany1 b").text('Registro de TME OPE');
    }

}

function selectClickTME(campo,valor,idCampoNo,idCampoSi){
    //estilos
    if(valor == 0){
        $(`#${idCampoNo}`).addClass('active-no')
        $(`#${idCampoSi}`).removeClass('active-si')
    }else{
        $(`#${idCampoNo}`).removeClass('active-no')
        $(`#${idCampoSi}`).addClass('active-si')
    }

    registroTranstornoMusculoEsqueleticoOperativo[campo] = valor
}

function selectSintomas(campo,index,valor,idCampoSi,idCampoNo){
    //estilos
    if(valor == 0){
        $(`#${idCampoNo}`).addClass('active-no')
        $(`#${idCampoSi}`).removeClass('active-si')
    }else{
        $(`#${idCampoNo}`).removeClass('active-no')
        $(`#${idCampoSi}`).addClass('active-si')
    }

    registroTranstornoMusculoEsqueleticoOperativo.Sintomas[index][campo] = valor
}

function setInput(campo,index){
    let valor = $(`#${campo}-${index}`).val()
    console.log(valor)
    registroTranstornoMusculoEsqueleticoOperativo.Sintomas[index][campo] = valor
}

function selectTest(campo,index,valor,idCampoSi,idCampoNo){
    //estilos
    if(valor == 0){
        $(`#${idCampoNo}`).addClass('test-active-no')
        $(`#${idCampoSi}`).removeClass('test-active-si')
    }else{
        $(`#${idCampoNo}`).removeClass('test-active-no')
        $(`#${idCampoSi}`).addClass('test-active-si')
    }

    registroTranstornoMusculoEsqueleticoOperativo.Tests[index][campo] = valor
}

function ImprimirCATranstornoMusculoEsqueletico(){

    let UsuarioVMO = localStorage.getItem('UsuarioVMO');
    if(UsuarioVMO){
        // exportListaVMO(UsuarioVMO)
    }else{
        exportConstanciaAutorizacion(paObj_hc[idHC].a)
    }

}

function exportConstanciaAutorizacion(UsuarioVMO){
    const date = new Date()
    const PDF = new jsPDF()
    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,10,200,10)
    PDF.line(10,10,10,285)
    PDF.line(10,285,200,285)
    PDF.line(200,10,200,285)
    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,15,190,15)
    PDF.line(20,15,20,33)
    PDF.line(20,33,190,33)
    PDF.line(190,15,190,33)

    PDF.line(60,15,60,33)
    PDF.setFontSize(9)
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO\n         AL PROGRAMA DE VIGILANCIA MÉDICA',62, 22)
    PDF.line(140,15,140,33)

    PDF.setFontSize(10)
    PDF.text('    Versión',142, 19)
    PDF.text('      N°.00',142, 24)
    PDF.line(165,15,165,26)
    PDF.text('  Página:',170, 19)
    PDF.text('   1 de 1',170, 24)
    PDF.line(140,26,190,26)
    PDF.text('    Código: SSM02-F16',145, 31)

    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,80,200,80)
    PDF.setFontSize(16)
    PDF.setFontType("bold");
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO AL PROGRAMA\n' +
        '                                     DE VIGILANCIA MÉDICA',15, 88)
    PDF.line(10,100,200,100)

    PDF.text('PROGRAMA:',25, 120)
    PDF.text('Trastornos músculo esquéletico',75, 120)

    PDF.text('PARTICIPANTE:',25, 135)
    PDF.text(`${UsuarioVMO.Nombres_Trabajador_H} ${UsuarioVMO.Apellidos_Trabajador_H}`,75, 135)

    PDF.text('DNI:',135, 135)
    PDF.text(`${UsuarioVMO.NroDocumento_Trabajador_H}`,150, 135)

    PDF.text('UNIDAD:',25, 150)
    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == UsuarioVMO.SedeId_Empresa_H
    });

    PDF.text(`${sede[0].Description}`,60, 150)
    PDF.text('ÁREA/PUESTO:',25, 165)

    const area = sedeAreaGerencia.Area.filter(area => {
        return area.Id == UsuarioVMO.AreaId_Empresa_H
    })

    PDF.text(`${area[0].Description} / ${UsuarioVMO.PuestoTrabajo_Empresa_H}`,70, 165)

    PDF.setFontSize(11)
    PDF.setFontType("itali");
    PDF.text('A través del presente documento dejo constancia que deseo participar del programa de vigilancia\n' +
        'médica ocupacional antes referido, comprometiéndome a asistir y a poner en práctica las diversas\n' +
        'pautas preventivas que nos brinden.',25,180)

    PDF.setFontSize(10)
    PDF.setFontType("bold")
    PDF.text('FIRMA DEL TRABAJADOR',28,250)
    PDF.text('FIRMA DEL MÉDICO OCUPACIONAL',120,250)

    PDF.text('Fecha',28,270)
    let fecha = `${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+ (date.getMonth() + 1) : date.getMonth() }-${date.getFullYear()}`
    PDF.text(`${fecha}`,45,270)

    PDF.save('constancia-de-autorizacion-transtorno-musculo-esqueletico.pdf')
}

function pdfCapacitacion(){
    let UsuarioVMO = localStorage.getItem('UsuarioVMO');
    let registro = null

    let tipo_puesto = $('#tipo_puesto').val();

    if(tipo_puesto == 1){
        registro = registroTranstornoMusculoEsqueletico
    }else{
        registro = registroTranstornoMusculoEsqueleticoOperativo
    }

    if(UsuarioVMO){
        // exportListaVMO(UsuarioVMO)
    }else{
        exportHCCapacitacion(paObj_hc[idHC].a,registro)
    }
}

function exportHCCapacitacion(usuario,registro){
    const PDF = new jsPDF()
    const date = new Date();
    PDF.setFontSize(7)
    PDF.setFontType("bold");
    PDF.text('TECNOLÓGICA DE ALIMENTOS S.A',150.5,15)
    PDF.text('RUC 20100971772 S.A',167,20)
    PDF.text('ACTIVIDAD ECONÓMICA: ELABORACIÓN Y CONSERVACIÓN DE PESCADO, CRUSTÁCEOS Y MOLUSCOS',67,25)

    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,30,190,30)
    PDF.line(20,30,20,40)
    PDF.line(20,40,190,40)
    PDF.line(190,30,190,40)

    PDF.setFontSize(14)
    PDF.setFontType("bold");
    PDF.text('CAPACITACIÓN VMO: TRANSTORNOS MÚSCULO ESQUELÉTICO',27,37)

    PDF.setFontSize(10)
    PDF.text('APELLIDOS Y NOMBRES :',20,50)
    PDF.text('UNIDAD OPERATIVA :',20,60)
    PDF.text('PUESTO DE TRABAJO :',20,70)

    PDF.text(`${usuario.Nombres_Trabajador_H} ${usuario.Apellidos_Trabajador_H}`,70,50)
    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == usuario.SedeId_Empresa_H
    });
    PDF.text(`${sede[0].Description}`,65,60)
    PDF.text(`${usuario.PuestoTrabajo_Empresa_H}`,65,70)

    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,80,190,80)
    PDF.line(20,80,20,85)
    PDF.line(20,85,190,85)
    PDF.line(190,80,190,85)

    PDF.setFontSize(8)
    PDF.line(30,80,30,85)
    PDF.text('N°',23,83.5)
    PDF.text('TEMA',60,83.5)
    PDF.line(110,80,110,85)
    PDF.text('N°DNI',115,83.5)
    PDF.line(130,80,130,85)
    PDF.text('ÁREA',135,83.5)
    PDF.line(150,80,150,85)
    PDF.text('Fecha',155,83.5)
    PDF.line(170,80,170,85)
    PDF.text('FIRMA',175,83.5)

    let datos = paObj_hc[idHC].a


    registro.Capacitaciones.forEach((capacitacion,index) => {
        PDF.setFontSize(8)
        PDF.line(20,(80 + (index + 1) * 5),20,(85 + (index + 1) * 5))
        PDF.line(20,(85 + (index + 1) * 5),190,(85 + (index + 1) * 5))
        PDF.line(30,(80 + (index + 1) * 5),30,(85 + (index + 1) * 5))
        PDF.text(`${index+1}`,23,(83.5 + (index + 1) * 5))
        PDF.text(`${capacitacion.Comentario}`,33,(83.5 + (index + 1) * 5))
        PDF.line(110,(80 + (index + 1) * 5),110,(85 + (index + 1) * 5))
        PDF.text(`${datos.NroDocumento_Trabajador_H}`,113,(83.5 + (index + 1) * 5))
        PDF.line(130,(80 + (index + 1) * 5),130,(85 + (index + 1) * 5))

        sedeAreaGerencia.Area.forEach((e) => {
            if (e.Id == datos.AreaId_Empresa_H) {
                PDF.setFontSize(6)
                PDF.text(`${e.Description}`,132,(83.5 + (index + 1) * 5))
            }
        });
        PDF.line(150,(80 + (index + 1) * 5),150,(85 + (index + 1) * 5))

        PDF.setFontSize(8)
        PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,152,(83.5 + (index + 1) * 5))
        PDF.line(170,(80 + (index + 1) * 5),170,(85 + (index + 1) * 5))
        PDF.line(190,(80 + (index + 1) * 5),190,(85 + (index + 1) * 5))
    })

    if(registro.Capacitaciones.length < 20){
        for (let i = registro.Capacitaciones.length ; i < 20 ; i++){
            PDF.setFontSize(8)
            PDF.line(20,(80 + (i + 1) * 5),20,(85 + (i + 1) * 5))
            PDF.line(20,(85 + (i + 1) * 5),190,(85 + (i + 1) * 5))
            PDF.line(30,(80 + (i + 1) * 5),30,(85 + (i + 1) * 5))
            PDF.text(`${i+1}`,23,(83.5 + (i + 1) * 5))
            PDF.line(110,(80 + (i + 1) * 5),110,(85 + (i + 1) * 5))

            PDF.line(130,(80 + (i + 1) * 5),130,(85 + (i + 1) * 5))


            PDF.line(150,(80 + (i + 1) * 5),150,(85 + (i + 1) * 5))

            PDF.line(170,(80 + (i + 1) * 5),170,(85 + (i + 1) * 5))
            PDF.line(190,(80 + (i + 1) * 5),190,(85 + (i + 1) * 5))
        }
        // let usuario = JSON.parse(localStorage.getItem('usuario'))
        // PDF.setFontSize(10)
        // PDF.text('OBSERVACIONES :',20,(85 + 22 * 5))
        // PDF.line(55,(85 + 22 * 5),190,(85 + 22 * 5))
        // PDF.line(20,(85 + 23 * 5),190,(85 + 23 * 5))
        // PDF.line(20,(85 + 24 * 5),190,(85 + 24 * 5))
        // PDF.text('RESPONSABLE DEL REGISTRO',20,(85 + 26 * 5))
        // PDF.text('Nombre y Apellido :',20,(85 + 28 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${usuario.fullusername}`,60,(84 + 28 * 5))
        // PDF.line(60,(85 + 28 * 5),140,(85 + 28 * 5))
        // PDF.setFontSize(10)
        // PDF.text('Fecha :',145,(85 + 28 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,165,(84 + 28 * 5))
        // PDF.line(160,(85 + 28 * 5),190,(85 + 28 * 5))
        // PDF.setFontSize(10)
        // PDF.text('Cargo :',20,(85 + 30 * 5))
        // PDF.line(60,(85 + 30 * 5),140,(85 + 30 * 5))
        // PDF.text('Fecha :',145,(85 + 30 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,165,(84 + 30 * 5))
        // PDF.line(160,(85 + 30 * 5),190,(85 + 30 * 5))
    }

    let cantidad = (registro.Capacitaciones.length <= 20) ? 20 : registro.Capacitaciones.length

    let usuario2 = JSON.parse(localStorage.getItem('usuario'))
    PDF.setFontSize(10)
    PDF.text('OBSERVACIONES :',20,(85 + (cantidad + 2) * 5))
    PDF.line(55,(85 + 22 * 5),190,(85 + (cantidad + 2) * 5))
    PDF.line(20,(85 + 23 * 5),190,(85 + (cantidad + 3) * 5))
    PDF.line(20,(85 + 24 * 5),190,(85 + (cantidad + 4) * 5))
    PDF.text('RESPONSABLE DEL REGISTRO',20,(85 + (cantidad + 6) * 5))
    PDF.text('Nombre y Apellido :',20,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(8)
    PDF.text(`${usuario2.fullusername}`,60,(84 + (cantidad + 8) * 5))
    PDF.line(60,(85 + (cantidad + 8) * 5),140,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(10)
    PDF.text('Fecha :',145,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(8)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,168,(84 + (cantidad + 8) * 5))
    PDF.line(160,(85 + (cantidad + 8) * 5),190,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(10)
    PDF.text('Cargo :',20,(85 + (cantidad + 10) * 5))
    PDF.line(60,(85 + (cantidad + 10) * 5),140,(85 + (cantidad + 10) * 5))
    PDF.text('Fecha :',145,(85 + (cantidad + 10) * 5))
    PDF.setFontSize(8)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,168,(84 + (cantidad + 10) * 5))
    PDF.line(160,(85 + (cantidad + 10) * 5),190,(85 + (cantidad + 10) * 5))

    PDF.save('capacitación-vmo-tme-adm.pdf')
}

function modalAntecedentesOcupacionales(){
    var settings = {
        url: apiUrlsho+"/api/hce_Get_002_historia_clinica_antecedente?code=BsULi1Y0aoClfhCk3mSUvEsnbFAyVhtkEXaf8L8REAcYUGZLmv5qaw==&IdHC="+idHC,
        method: "GET",
        dataType: 'json',
        headers: {
            apikey: constantes.apiKey,
            "Content-Type": "application/json"
        },
    };

    $.ajax(settings).done(function (response) {

        $('#lista-body').html('')

        var myModal = new bootstrap.Modal(document.getElementById('historial'))
        myModal.show()

        response.HistoriaAntecedentes.forEach((item, index) => {

            if(item.Empresa_A != null){
                let tipo = response.HistoriaTipoAntec.filter(tipo => {
                    return tipo.IdTipA == item.IdTipA
                })

                let inicio = item.FechaInicio_A.split('T')[0].split('-')
                let final = item.FechaFin_A.split('T')[0].split('-')

                $('#lista-body').append(`<tr>
                    <td>${inicio[0]}/${inicio[2]}/${inicio[1]}</td>
                    <td>${final[0]}/${final[2]}/${final[1]}</td>
                    <td>${item.Empresa_A}</td>
                    <td>${(item.ActividadEmpresa_A == null) ? '--' : item.ActividadEmpresa_A}</td>
                    <td>${(item.AreaTrabajo_A == null) ? '--' : item.AreaTrabajo_A}</td>
                    <td>${(item.Ocupacion_A == null) ? '--' : (item.Ocupacion_A)}</td>
                    <td>${(item.PeligrosAgentesOcupacionales_A == null) ? '--' : item.PeligrosAgentesOcupacionales_A}</td>
                    <td>${tipo[0].Descripcion_TipA}</td>
                </tr>`)
            }

        })

    });
}

function mostrarDatos(){
    $('#modal').modal('show')
}