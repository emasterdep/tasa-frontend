let calidadVida = null

function setDatosVmoCalidadVida(campo, idCampo){
    let valor = ''
    let valor1 = ''
    if(campo == 'Peso'){
        imc('reg_cali_vida_datos_peso','reg_cali_vida_datos_talla','reg_cali_vida_datos_imc')
        valor = $(`#reg_cali_vida_datos_imc`).val()
        valor1 = $(`#${idCampo}`).val()
        calidadVida.IMC = valor
    }else if(campo == 'Talla'){
        imc('reg_cali_vida_datos_peso','reg_cali_vida_datos_talla','reg_cali_vida_datos_imc')
        valor = $(`#reg_cali_vida_datos_imc`).val()
        valor1 = $(`#${idCampo}`).val()
        calidadVida.IMC = valor
    }else{
        valor1 = $(`#${idCampo}`).val()
    }
    calidadVida[campo] = valor1
}

function selectClick(campo, idCampo, clase){
    $(`.${clase}`).removeClass("select-active")
    $('#select_'+idCampo).addClass("select-active")
    setDatosVmoCalidadVida(campo, idCampo)
    if(campo == 'Fumador'){
        if(idCampo == 'reg_cali_vida_datos_fumador_no'){
            $('#reg_cali_vida_datos_fumador_select').attr('disabled',true)
            calidadVida.ConsumoFumadorId = 0
        }else{
            $('#reg_cali_vida_datos_fumador_select').attr('disabled',false)
            setDatosVmoCalidadVida('ConsumoFumadorId','reg_cali_vida_datos_fumador_select')
        }
    }
    if(campo == 'ConsumoAzucar'){
        if(idCampo == 'reg_cali_vida_datos_consumo_de_azucar_no'){
            $('#reg_cali_vida_datos_consumo_azucar_select').attr('disabled',true)
            calidadVida.ConsumoAzucarId = 0
        }else{
            $('#reg_cali_vida_datos_consumo_azucar_select').attr('disabled',false)
            setDatosVmoCalidadVida('ConsumoAzucarId','reg_cali_vida_datos_consumo_azucar_select')
        }
    }
    if(campo == 'ActividadFisica'){
        if(idCampo == 'reg_cali_vida_datos_actividad_fisica_no'){
            $('#reg_cali_vida_datos_actividad_fisica_select').attr('disabled',true)
            calidadVida.ConsumoActividadFisicaId = 0
        }else{
            $('#reg_cali_vida_datos_actividad_fisica_select').attr('disabled',false)
            setDatosVmoCalidadVida('ConsumoActividadFisicaId','reg_cali_vida_datos_actividad_fisica_select')
        }
    }
    if(campo == 'Alcohol'){
        if(idCampo == 'reg_cali_vida_datos_alcohol_no'){
            $('#reg_cali_vida_datos_alcohol_select').attr('disabled',true)
            calidadVida.ConsumoAlcoholId = 0
        }else{
            $('#reg_cali_vida_datos_alcohol_select').attr('disabled',false)
            setDatosVmoCalidadVida('ConsumoAlcoholId','reg_cali_vida_datos_alcohol_select')
        }
    }

    if(campo == 'Otros'){
        if(idCampo == 'reg_cali_vida_datos_otros_no'){
            $('#reg_cali_vida_datos_otros_input').attr('disabled',true)
            calidadVida.ConsumoAlcoholId = 0
        }else{
            $('#reg_cali_vida_datos_otros_input').attr('disabled',false)
        }
    }
}

function tiempoEstadia(){
    let fecha_inicio = $('#reg_cali_vida_hosp_fecha_inicio').val();
    let fecha_fin = $('#reg_cali_vida_hosp_fecha_fin').val();

    if(fecha_fin != "" && fecha_inicio != "") {
        if (fecha_fin > fecha_inicio) {
            let day1 = new Date(fecha_inicio);
            let day2 = new Date(fecha_fin);
            let difference= Math.abs(day2-day1);
            let days = difference/(1000 * 3600 * 24)
            $('#reg_cali_vida_hosp_tiempo_estadia').val(days)
        } else {
            Swal.fire('La fecha final debe ser mayor a la fecha inicial', '', 'error')
        }
    }
}

function vmoCalidadDeVida(
    Id,
    EmoId,
    PresionArterial,
    PresionArterialDiastole,
    Peso,
    Glucosa,
    HTA,
    Dislipidemias,
    FrecuenciaCardiaca,
    Temperatura,
    Fumador,
    ConsumoFumadorId,
    ConsumoAzucar,
    ConsumoAzucarId,
    ActividadFisica,
    ConsumoActividadFisicaId,
    IMC,
    Talla,
    Colesterol,
    Sobrepeso,
    FrecuenciaRespiratorio,
    LDL,
    PerimetroAbdominal,
    Trigliceridos,
    Obesidad,
    SaturaciondeOxigeno,
    Estress,
    RiesgoCardioVascularId,
    HDL,
    Diabetes,
    Alcohol,
    ConsumoAlcoholId,
    Otros,
    OtrosDetalle,
    OtrosHospitalizacion,
    MedicoCargo,
    Activo,
    CreadoPor,
    ModificadoPor
){
    this.Id = Id
    this.EmoId = EmoId
    this.PresionArterial = PresionArterial
    this.PresionArterialDiastole = PresionArterialDiastole
    this.Peso = Peso
    this.Glucosa = Glucosa
    this.HTA = HTA
    this.Dislipidemias = Dislipidemias
    this.FrecuenciaCardiaca = FrecuenciaCardiaca
    this.Temperatura = Temperatura
    this.Fumador = Fumador
    this.ConsumoFumadorId = ConsumoFumadorId
    this.ConsumoAzucar = ConsumoAzucar
    this.ConsumoAzucarId = ConsumoAzucarId
    this.ActividadFisica = ActividadFisica
    this.ConsumoActividadFisicaId = ConsumoActividadFisicaId
    this.IMC = IMC
    this.Talla = Talla
    this.Colesterol = Colesterol
    this.Sobrepeso =  Sobrepeso
    this.FrecuenciaRespiratorio =  FrecuenciaRespiratorio
    this.LDL =  LDL
    this.PerimetroAbdominal =  PerimetroAbdominal
    this.Trigliceridos =  Trigliceridos
    this.Obesidad =  Obesidad
    this.SaturaciondeOxigeno =  SaturaciondeOxigeno
    this.Estress =  Estress
    this.RiesgoCardioVascularId =  RiesgoCardioVascularId
    this.HDL =  HDL
    this.Diabetes =  Diabetes
    this.Alcohol =  Alcohol
    this.ConsumoAlcoholId =  ConsumoAlcoholId
    this.Otros =  Otros
    this.OtrosDetalle =  OtrosDetalle
    this.OtrosHospitalizacion =  OtrosHospitalizacion
    this.MedicoCargo =  MedicoCargo
    this.Activo =  Activo
    this.CreadoPor =  CreadoPor
    this.ModificadoPor = ModificadoPor
    this.Adjuntos = []
    this.Hospitalizaciones = []
    this.OtrosSintomas = []
    this.Medicamentos = []
    this.Capacitaciones = []
}

function hospitalizacion(Id,FechaInicio, FechaFin, TiempoEstadia, Motivo, Comentario, Activo){
    this.Id = Id
    this.FechaInicio = FechaInicio
    this.FechaFin = FechaFin
    this.TiempoEstadia = TiempoEstadia
    this.Motivo = Motivo
    this.Comentario = Comentario
    this.Activo = Activo
}

function EliminarHospitalizacion(index){
    Swal.fire({
        title: '¿ Estás seguro de eliminar este registro ?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#8fbb02',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.isConfirmed) {
            calidadVida.Hospitalizaciones.splice(index,1)
            Swal.fire(
                'Éxito',
                'Se ha eliminado exitosamente',
                'success'
            )
            cargarDataHospitalizacion()
        }
    })
}

function agregarHospitalizacion(){
    let fecha_inicio = $('#reg_cali_vida_hosp_fecha_inicio').val().split('-');
    let fecha_fin = $('#reg_cali_vida_hosp_fecha_fin').val().split('-');
    let tiempo_estadia = $('#reg_cali_vida_hosp_tiempo_estadia').val();
    let motivo = $('#reg_cali_vida_hosp_motivo').val();

    calidadVida.Hospitalizaciones.push(new hospitalizacion(0,`${fecha_inicio[2]}/${fecha_inicio[1]}/${fecha_inicio[0]}`,`${fecha_fin[2]}/${fecha_fin[1]}/${fecha_fin[0]}`,tiempo_estadia,motivo,"",1))

    cargarDataHospitalizacion()

    $('#reg_cali_vida_hosp_fecha_inicio').val('');
    $('#reg_cali_vida_hosp_fecha_fin').val('');
    $('#reg_cali_vida_hosp_tiempo_estadia').val('');
    $('#reg_cali_vida_hosp_motivo').val('');
}

function cargarDataHospitalizacion(){
    $('#table_hospitalizacion-count-row').html(calidadVida.Hospitalizaciones.length)
    $('#content_hospitalizacion').html('');

    calidadVida.Hospitalizaciones.forEach((hosp, index) => {
        $('#content_hospitalizacion').append(`<tr>
            <td class="" >${hosp.FechaInicio}</td>
            <td class="" >${hosp.FechaFin}</td>
            <td class="" >${hosp.TiempoEstadia} Días</td>
            <td class="" >${hosp.Motivo}</td>
            <td>
                <img src="images/iconos/trash.svg" onclick="EliminarHospitalizacion(${index})"/>
            </td>
        </tr>  `)
    })
}

function Adjuntos(NombreArchivo, Archivo, Tipo){
    this.Id = 0
    this.NombreArchivo = NombreArchivo
    this.Archivo = Archivo
    this.Tipo = Tipo
    this.Activo = 1
}

function OtrosSintomas(Id, Nombre, Flag, Comentario, NombreArchivo, Archivo, Orden){
    this.Id = Id
    this.Nombre = Nombre
    this.Flag = Flag
    this.Comentario = Comentario
    this.NombreArchivo = NombreArchivo
    this.Archivo = Archivo
    this.Orden = Orden
    this.Activo = 1
}

function Medicamentos(NombreMedicamento, Comentario){
    this.Id = 0
    this.NombreMedicamento = NombreMedicamento
    this.Comentario = Comentario
    this.Activo = 1
}

function Capacitacion(tema, comentario){
    this.Id = 0;
    this.Tema = tema
    this.Comentario = comentario
    this.Activo = 1
}

function _initCreateRegristroCalidadVida(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    let usuario = JSON.parse(localStorage.getItem('usuario'));
    let idVMO = localStorage.getItem('idVMO');
    calidadVida = new vmoCalidadDeVida("0",idVMO,"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","");
    calidadVida.MedicoCargo = usuario.fullusername
    calidadVida.CreadoPor = usuario.idhash
    calidadVida.ModificadoPor = usuario.idhash
    calidadVida.Activo = 1
    // setDatosVmoCalidadVida('RiesgoCardioVascularId','reg_cali_vida_datos_riesgo_cardiovascular_select')
    calidadVida.OtrosSintomas.push(new OtrosSintomas("0","Creatinina","0","","","", "1"))
    calidadVida.OtrosSintomas.push(new OtrosSintomas("0","Dep/ creatina","0","","","", "2"))
    calidadVida.OtrosSintomas.push(new OtrosSintomas("0","I/C nutrición","0","","","", "3"))
    calidadVida.OtrosSintomas.push(new OtrosSintomas("0","I/c Cardiología","0","","","", "4"))
    calidadVida.OtrosSintomas.push(new OtrosSintomas("0","I/C M.G o M. Interna","0","","","", "5"))

    calidadVida.OtrosSintomas.forEach((sintomas, index) => {
        $('#otrosSintomas').append(`
            <div class="row w-100 align-items-center mb-3">
                    <div class="col-md-2">
                        <div class="d-flex">
                            <img src="./images/clipboard-minus-azul.svg" alt="" style="width: 25px;height: 25px;">
                            <span>${sintomas.Nombre}</span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="d-flex">
                            <div class="">
                                <div class="">
                                    <label 
                                     for="reg_cali_vida_datos_HTA_si"
                                     class="label-radio"
                                     id="otroSintomas-si-${index}"
                                     onclick="setOtrosSintomasRadio('${index}','Flag','1')">Si</label>
                                    <input 
                                        class="d-none" 
                                        type="radio" 
                                        name="hta" 
                                        value="1" 
                                        min="0" 
                                        id="reg_cali_vida_otros_creatinina_si"
                                        >
                                </div>
                            </div>
                            <div class="ml-3">
                                <div class="input_hc_sp3">
                                    <label for="reg_cali_vida_datos_HTA_no" class="label-radio active-no"
                                    id="otroSintomas-no-${index}"
                                    onclick="setOtrosSintomasRadio('${index}','Flag','0')">No</label>
                                    <input class="d-none" type="radio" name="hta" value="0" min="0" id="reg_cali_vida_otros_creatinina_no">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control w-100" type="text" name="hta" min="0" placeholder="Comentarios" id="reg_cali_vida_otros_creatinina_comentario-${index}" style="
                            width: 100% !important;
                            text-align: start;
                        "
                        onkeyup="setComentario('${index}','Comentario')">
                    </div>
                    <div class="col-md-2">
                        <label for="fileOtrosSintomas-${index}"
                        class="btn btn-success btn-block btn-lg w-100" 
                        style="width: 200px; padding: 10px;"
                        >Agregar a lista
                            <img src="./images/sho/plus.svg" fill="#fff" style="position: relative; width: 18px">
                        </label>
                        <input type="file" id="fileOtrosSintomas-${index}" class="d-none" onchange="fileOtrosSintomas('${index}')">
                    </div>
                    <div class="col-md-2">
                        <span class="m-0" id="nombrefile-${index}">No hay archivo</span>
                    </div>
                </div>
        `)
    })
}

function setOtrosSintomasRadio(index, campo, valor){
    setOtrosSintomas(index, campo, valor);
    if(valor == 1){
        $('#otroSintomas-si-'+index).addClass('select-active')
        $('#otroSintomas-no-'+index).removeClass('select-active')
    }else{
        $('#otroSintomas-si-'+index).removeClass('select-active')
        $('#otroSintomas-no-'+index).addClass('select-active')
    }
}

function setComentario(index, campo){
    let valor = $('#reg_cali_vida_otros_creatinina_comentario-'+index).val();
    setOtrosSintomas(index, campo, valor)
}

function fileOtrosSintomas(index){
    let valor = $('#fileOtrosSintomas-'+index)[0].files[0];
    $('#nombrefile-'+index).html(valor.name)
    var reader = new FileReader();
    reader.onloadend = function () {
        console.log('render '+reader.result)
        setOtrosSintomas(index, 'NombreArchivo',valor.name)
        setOtrosSintomas(index, 'Archivo',reader.result)
    }
    reader.readAsDataURL(valor);
}

function setOtrosSintomas(index, campo, valor){
    calidadVida.OtrosSintomas[index][campo] = valor
}

function AgregarListaMedicamento(){
    let nombre_de_medicamento = $('#reg_cali_vida_hosp_nombre_de_medicamento').val();
    let comentario = $('#reg_cali_vida_hosp_comentarios').val();
    $('#reg_cali_vida_hosp_nombre_de_medicamento').val('');
    $('#reg_cali_vida_hosp_comentarios').val('');
    calidadVida.Medicamentos.push(new Medicamentos(nombre_de_medicamento,comentario))
    tableMedicamento()
}

function tableMedicamento(){
    $('#content_medicamento').html("")
    $('#table_medicamento-count-row').val(calidadVida.Medicamentos.length);
    calidadVida.Medicamentos.forEach((medicamento,index) => {
        $('#content_medicamento').append(`
            <tr>
                <td>${index + 1}</td>
                <td>${medicamento.NombreMedicamento}</td>
                <td>${medicamento.Comentario}</td>
                <td>
                    <img src="images/iconos/trash.svg" onclick="eliminarMedicamento(${index})"/>
                </td>
            </tr>    
        `)
    })
}

function eliminarMedicamento(index){
    calidadVida.Medicamentos.splice(index,1)
    tableMedicamento()
}

function agregarListaCapacitacion(){
    let tema = $('#reg_cali_vida_hosp_capacitacion_tema').val();
    let comentario = $('#reg_cali_vida_hosp_capacitacion_comentario').val();
    $('#reg_cali_vida_hosp_capacitacion_tema').val('');
    $('#reg_cali_vida_hosp_capacitacion_comentario').val('');
    calidadVida.Capacitaciones.push(new Capacitacion(tema,comentario))
    console.log(calidadVida.Capacitaciones)
    tablaCapacitacion()
}

function tablaCapacitacion(){
    $('#table_capacitacion-count-row').html(calidadVida.Capacitaciones.length)
    console.log(calidadVida.Capacitaciones.length)
    $('#content_capacitacion').html('')
    calidadVida.Capacitaciones.forEach((capacitacion, index) => {
        $('#content_capacitacion').append(`
            <tr>
                <td>${index + 1}</td>
                <td>${capacitacion.Tema}</td>
                <td>${capacitacion.Comentario}</td>
                <td>
                    <img src="images/iconos/trash.svg" onclick="eliminarCapacitacion(${index})"/>
                </td>
            </tr>    
        `)
    })
}

function eliminarCapacitacion(index){
    calidadVida.Capacitaciones.splice(index, 1);
    tablaCapacitacion()
}

function AdjuntarArchivo(id,tipo){
    let file = $(`#${id}`)[0].files[0];
    var reader = new FileReader();
    console.log(id)
    console.log(tipo)
    reader.onloadend = function () {
        console.log('render '+reader.result)
        calidadVida.Adjuntos.push(new Adjuntos(file.name,reader.result,tipo))
        listaTablaAdjunto()
    }
    reader.readAsDataURL(file);
}

function listaTablaAdjunto(){
    let adjunto_archivo = calidadVida.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 1
    })

    let adjunto_firma = calidadVida.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 2
    })

    $('#table_adjunto_firma-count-row').html(adjunto_firma.length)
    $('#table_adjunto-archivos-count-row').html(adjunto_archivo.length)

    if(adjunto_firma.length > 0){
        $('#lista-adjunto-firma').removeClass('d-none')
    }

    if(adjunto_archivo.length > 0){
        $('#lista-adjunto-archivos').removeClass('d-none')
    }

    $('#adjunto_firma').pagination({
        dataSource: adjunto_firma,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_firma').html(html);
        }
    })

    $('#pagination-adjunto-archivos').pagination({
        dataSource: adjunto_archivo,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto-archivos').html(html);
        }
    })
}

function registroCalidadDeVida(){

    let usuario = paObj_hc[idHC].a
    const url = apiUrlsho+`/api/hce_Post_vmo?code=aOqdZIaCcrb03DFAYLAEOuaE4YmZywydiZ8tVakK26za/CWLzZck/w==&httpmethod=post`;
    const headers = {
        apikey: constantes.apiKey,
        "Content-Type": "application/json"
    }
    const setting = {
        url: url,
        method: "post",
        dataType: 'json',
        headers: headers,
        data: JSON.stringify(calidadVida)
    }
    console.log(setting)

    if(calidadVida.Hospitalizaciones.length > 0 && calidadVida.Medicamentos.length > 0 && calidadVida.Capacitaciones.length > 0){

        if(calidadVida.PresionArterial == 0 && calidadVida.PresionArterialDiastole == 0 && calidadVida.Peso == 0 && calidadVida.Glucosa == 0 && calidadVida.HTA == 0
            && calidadVida.Dislipidemias == 0 && calidadVida.FrecuenciaCardiaca == 0 && calidadVida.Fumador == 0 && calidadVida.ConsumoAzucar == 0
            && calidadVida.ActividadFisica == 0 && calidadVida.IMC == 0 && calidadVida.Talla == 0 && calidadVida.Colesterol == 0 && calidadVida.Sobrepeso == 0
            && calidadVida.FrecuenciaCardiaca == 0 && calidadVida.LDL == 0 && calidadVida.PerimetroAbdominal == 0 && calidadVida.Trigliceridos == 0
            && calidadVida.Obesidad == 0 && calidadVida.SaturaciondeOxigeno == 0 && calidadVida.Estress == 0 && calidadVida.RiesgoCardioVascularId == 0
            && calidadVida.HDL == 0 && calidadVida.Diabetes == 0 && calidadVida.Alcohol == 0 && calidadVida.Otros == 0){

            Swal.fire({
                title: 'Guardar registro de calidad de vida',
                html: `<p>Está por guardar el registro de calidad de vida de: ${usuario.Nombres_Trabajador_H} ${usuario.Apellidos_Trabajador_H}, con la información por defecto.
                       </p> <p class="mt-4">¿Desea confirmar la acción?</p>`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#8fbb02',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar <img src="./images/sho/confirm.svg" />',
                cancelButtonText: 'Cancelar <img src="./images/sho/cancelar.svg" />',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax(setting).done(response => {
                        Swal.fire({
                            title: 'Se guardó el registro con éxito',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 2500
                        })
                        atrasBack()
                    })
                }
            })

        }else{
            Swal.fire({
                title: 'Guardar registro de calidad de vida',
                html: `<p>Está por guardar el registro de calidad de vida de: ${usuario.Nombres_Trabajador_H} ${usuario.Apellidos_Trabajador_H}</p> <p class="mt-4">¿Desea confirmar la acción?</p>`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#8fbb02',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar <img src="./images/sho/confirm.svg" />',
                cancelButtonText: 'Cancelar <img src="./images/sho/cancelar.svg" />',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax(setting).done(response => {
                        Swal.fire({
                            title: 'Se guardó el registro con éxito',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 2500
                        })
                        atrasBack()
                    })
                }
            })
        }

    }else{
        Swal.fire({
            title: 'Debe realizar registros en Hospitalización, Medicamentos y Capacitación',
            icon: 'error',
            timer: 2500
        })
    }

}

function ImprimirCACalidadVida(){

    let UsuarioVMO = localStorage.getItem('UsuarioVMO');

    if(UsuarioVMO){
        exportListaVMO(JSON.parse(UsuarioVMO))
    }else{
        exportHC(paObj_hc[idHC].a)
    }
}

function simpleTemplatingAdjunto(data){
    const date = new Date();
    let CreadoFecha = `${date.getDate()}/${((parseInt(date.getMonth()) + 1) < 10) ? '0'+(parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1}/${date.getFullYear()}`
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.NombreArchivo}</td>
                <td>${CreadoFecha}</td>
                <td>
                    <button onclick="eliminarAdjunto('${item.NombreArchivo}')" id="hc_btn_borrar_evidencia_31" type="button" class="btn btn-link shadow-none float-right">
                      <img class="inject-svg" src="./images/sho/delete.svg" alt="" fill="#ff3636" width="16px">
                    </button>
                    <a class="btn btn-link shadow-none float-right" href="${item.Archivo}" download="${item.NombreArchivo}">
                      <img class="inject-svg" src="./images/sho/download.svg" fill="#207345" style="width:16px !important">
                    </a>
                </td>
            </tr> 
        `
        });
    }else{
       html += ` <tr>
            <td className="text-center" colSpan="5" style="text-align: center">No se encontro resultados</td>
        </tr>`
    }
    return html;
}

function eliminarAdjunto(nombre){
    calidadVida.Adjuntos.forEach((adjunto, index) => {
        if(adjunto.NombreArchivo == nombre){
            calidadVida.Adjuntos.splice(index,1)
            listaTablaAdjunto()
        }
    })
}

function exportListaVMO(UsuarioVMO){
    const date = new Date()

    const PDF = new jsPDF()
    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,10,200,10)
    PDF.line(10,10,10,285)
    PDF.line(10,285,200,285)
    PDF.line(200,10,200,285)
    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,15,190,15)
    PDF.line(20,15,20,33)
    PDF.line(20,33,190,33)
    PDF.line(190,15,190,33)

    PDF.line(60,15,60,33)
    PDF.setFontSize(9)
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO\n         AL PROGRAMA DE VIGILANCIA MÉDICA',62, 22)
    PDF.line(140,15,140,33)

    PDF.setFontSize(10)
    PDF.text('    Versión',142, 19)
    PDF.text('      N°.00',142, 24)
    PDF.line(165,15,165,26)
    PDF.text('  Página:',170, 19)
    PDF.text('   1 de 1',170, 24)
    PDF.line(140,26,190,26)
    PDF.text('    Código: SSM02-F16',145, 31)

    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,80,200,80)
    PDF.setFontSize(16)
    PDF.setFontType("bold");
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO AL PROGRAMA\n' +
        '                                     DE VIGILANCIA MÉDICA',15, 88)
    PDF.line(10,100,200,100)

    PDF.text('PROGRAMA:',25, 120)
    PDF.text('Calidad de vida',75, 120)

    PDF.text('PARTICIPANTE:',25, 135)
    PDF.text(`${UsuarioVMO.A_NombresPaciente} ${UsuarioVMO.A_ApellidosPaciente}`,75, 135)

    PDF.text('DNI:',135, 135)
    PDF.text(`${UsuarioVMO.A_DniPaciente}`,150, 135)

    PDF.text('UNIDAD:',25, 150)
    // PDF.text(`${UsuarioVMO.A_NombresPaciente} ${UsuarioVMO.A_ApellidosPaciente}`,75, 150)
    PDF.text('ÁREA/PUESTO:',25, 165)

    const area = sedeAreaGerencia.Area.filter(area => {
        return area.Id = UsuarioVMO.B_AreaId
    })

    PDF.text(`${area[0].Description} / ${UsuarioVMO.B_PuestoTrabajo}`,70, 165)

    PDF.setFontSize(11)
    PDF.setFontType("itali");
    PDF.text('A través del presente documento dejo constancia que deseo participar del programa de vigilancia\n' +
        'médica ocupacional antes referido, comprometiéndome a asistir y a poner en práctica las diversas\n' +
        'pautas preventivas que nos brinden',25,180)

    PDF.setFontSize(10)
    PDF.setFontType("bold")
    PDF.text('FIRMA DEL TRABAJADOR',28,250)
    PDF.text('FIRMA DEL MÉDICO OCUPACIONAL',120,250)

    PDF.text('Fecha',28,270)
    PDF.text(`${date.getMonth()}-${date.getDate()}-${date.getFullYear()}`,45,270)

    PDF.save('constancia-de-autorizacion-calidad-vida.pdf')
}

function exportHC(usuario){
    const date = new Date()

    const PDF = new jsPDF()
    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,10,200,10)
    PDF.line(10,10,10,285)
    PDF.line(10,285,200,285)
    PDF.line(200,10,200,285)
    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,15,190,15)
    PDF.line(20,15,20,33)
    PDF.line(20,33,190,33)
    PDF.line(190,15,190,33)

    PDF.line(60,15,60,33)
    PDF.setFontSize(9)
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO\n         AL PROGRAMA DE VIGILANCIA MÉDICA',62, 22)
    PDF.line(140,15,140,33)

    PDF.setFontSize(10)
    PDF.text('    Versión',142, 19)
    PDF.text('      N°.00',142, 24)
    PDF.line(165,15,165,26)
    PDF.text('  Página:',170, 19)
    PDF.text('   1 de 1',170, 24)
    PDF.line(140,26,190,26)
    PDF.text('    Código: SSM02-F16',145, 31)

    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,80,200,80)
    PDF.setFontSize(16)
    PDF.setFontType("bold");
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO AL PROGRAMA\n' +
        '                                     DE VIGILANCIA MÉDICA',15, 88)
    PDF.line(10,100,200,100)

    PDF.text('PROGRAMA:',25, 120)
    PDF.text('Calidad de vida',75, 120)

    PDF.text('PARTICIPANTE:',25, 135)
    PDF.text(`${usuario.Nombres_Trabajador_H} ${usuario.Apellidos_Trabajador_H}`,75, 135)

    PDF.text('DNI:',135, 135)
    PDF.text(`${usuario.NroDocumento_Trabajador_H}`,150, 135)

    PDF.text('UNIDAD:',25, 150)

    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == usuario.SedeId_Empresa_H
    });

    PDF.text(`${sede[0].Description}`,70, 150)
    PDF.text('ÁREA/PUESTO:',25, 165)

    const area = sedeAreaGerencia.Area.filter(area => {
        return area.Id = usuario.AreaId_Empresa_H
    })
    PDF.setFontSize(13)
    PDF.text(`${area[0].Description} / ${usuario.PuestoTrabajo_Empresa_H}`,70, 165)

    PDF.setFontSize(11)
    PDF.setFontType("itali");
    PDF.text('A través del presente documento dejo constancia que deseo participar del programa de vigilancia\n' +
        'médica ocupacional antes referido, comprometiéndome a asistir y a poner en práctica las diversas\n' +
        'pautas preventivas que nos brinden.',25,180)

    PDF.setFontSize(10)
    PDF.setFontType("bold")
    PDF.text('FIRMA DEL TRABAJADOR',28,250)
    PDF.text('FIRMA DEL MÉDICO OCUPACIONAL',120,250)

    PDF.text('Fecha',28,270)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,45,270)

    PDF.save('constancia-de-autorizacion-calidad-vida.pdf')
}

function pdfCapacitacion(){
    let UsuarioVMO = localStorage.getItem('UsuarioVMO');
    if(UsuarioVMO){
        // exportListaVMO(UsuarioVMO)
    }else{
        exportHCCapacitacion(paObj_hc[idHC].a)
    }
}

function exportHCCapacitacion(usuario){
    const PDF = new jsPDF()
    const date = new Date();
    PDF.setFontSize(7)
    PDF.setFontType("bold");
    PDF.text('TECNOLÓGICA DE ALIMENTOS S.A',150.5,15)
    PDF.text('RUC 20100971772 S.A',167,20)
    PDF.text('ACTIVIDAD ECONÓMICA: ELABORACIÓN Y CONSERVACIÓN DE PESCADO, CRUSTÁCEOS Y MOLUSCOS',67,25)

    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,30,190,30)
    PDF.line(20,30,20,40)
    PDF.line(20,40,190,40)
    PDF.line(190,30,190,40)

    PDF.setFontSize(15)
    PDF.setFontType("bold");
    PDF.text('CAPACITACIÓN VMO: CALIDAD DE VIDA',50,37)

    PDF.setFontSize(10)
    PDF.text('APELLIDOS Y NOMBRES :',20,50)
    PDF.text('UNIDAD OPERATIVA :',20,60)
    PDF.text('PUESTO DE TRABAJO :',20,70)

    PDF.text(`${usuario.Nombres_Trabajador_H} ${usuario.Apellidos_Trabajador_H}`,70,50)
    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == usuario.SedeId_Empresa_H
    });
    PDF.text(`${sede[0].Description}`,65,60)
    PDF.text(`${usuario.PuestoTrabajo_Empresa_H}`,65,70)

    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,80,190,80)
    PDF.line(20,80,20,85)
    PDF.line(20,85,190,85)
    PDF.line(190,80,190,85)

    PDF.setFontSize(8)
    PDF.line(30,80,30,85)
    PDF.text('N°',23,83.5)
    PDF.text('TEMA',60,83.5)
    PDF.line(110,80,110,85)
    PDF.text('N°DNI',115,83.5)
    PDF.line(130,80,130,85)
    PDF.text('ÁREA',135,83.5)
    PDF.line(150,80,150,85)
    PDF.text('Fecha',155,83.5)
    PDF.line(170,80,170,85)
    PDF.text('FIRMA',175,83.5)

    let datos = paObj_hc[idHC].a


    calidadVida.Capacitaciones.forEach((capacitacion,index) => {
        PDF.setFontSize(8)
        PDF.line(20,(80 + (index + 1) * 5),20,(85 + (index + 1) * 5))
        PDF.line(20,(85 + (index + 1) * 5),190,(85 + (index + 1) * 5))
        PDF.line(30,(80 + (index + 1) * 5),30,(85 + (index + 1) * 5))
        PDF.text(`${index+1}`,23,(83.5 + (index + 1) * 5))
        PDF.text(`${capacitacion.Comentario}`,33,(83.5 + (index + 1) * 5))
        PDF.line(110,(80 + (index + 1) * 5),110,(85 + (index + 1) * 5))
        PDF.text(`${datos.NroDocumento_Trabajador_H}`,113,(83.5 + (index + 1) * 5))
        PDF.line(130,(80 + (index + 1) * 5),130,(85 + (index + 1) * 5))

        sedeAreaGerencia.Area.forEach((e) => {
            if (e.Id == datos.AreaId_Empresa_H) {
                PDF.setFontSize(6)
                PDF.text(`${e.Description}`,132,(83.5 + (index + 1) * 5))
            }
        });
        PDF.line(150,(80 + (index + 1) * 5),150,(85 + (index + 1) * 5))

        PDF.setFontSize(8)
        PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,152,(83.5 + (index + 1) * 5))
        PDF.line(170,(80 + (index + 1) * 5),170,(85 + (index + 1) * 5))
        PDF.line(190,(80 + (index + 1) * 5),190,(85 + (index + 1) * 5))
    })

    if(calidadVida.Capacitaciones.length < 20){
        for (let i = calidadVida.Capacitaciones.length ; i < 20 ; i++){
            PDF.setFontSize(8)
            PDF.line(20,(80 + (i + 1) * 5),20,(85 + (i + 1) * 5))
            PDF.line(20,(85 + (i + 1) * 5),190,(85 + (i + 1) * 5))
            PDF.line(30,(80 + (i + 1) * 5),30,(85 + (i + 1) * 5))
            PDF.text(`${i+1}`,23,(83.5 + (i + 1) * 5))
            PDF.line(110,(80 + (i + 1) * 5),110,(85 + (i + 1) * 5))

            PDF.line(130,(80 + (i + 1) * 5),130,(85 + (i + 1) * 5))


            PDF.line(150,(80 + (i + 1) * 5),150,(85 + (i + 1) * 5))

            PDF.line(170,(80 + (i + 1) * 5),170,(85 + (i + 1) * 5))
            PDF.line(190,(80 + (i + 1) * 5),190,(85 + (i + 1) * 5))
        }
        // let usuario = JSON.parse(localStorage.getItem('usuario'))
        // PDF.setFontSize(10)
        // PDF.text('OBSERVACIONES :',20,(85 + 22 * 5))
        // PDF.line(55,(85 + 22 * 5),190,(85 + 22 * 5))
        // PDF.line(20,(85 + 23 * 5),190,(85 + 23 * 5))
        // PDF.line(20,(85 + 24 * 5),190,(85 + 24 * 5))
        // PDF.text('RESPONSABLE DEL REGISTRO',20,(85 + 26 * 5))
        // PDF.text('Nombre y Apellido :',20,(85 + 28 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${usuario.fullusername}`,60,(84 + 28 * 5))
        // PDF.line(60,(85 + 28 * 5),140,(85 + 28 * 5))
        // PDF.setFontSize(10)
        // PDF.text('Fecha :',145,(85 + 28 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,165,(84 + 28 * 5))
        // PDF.line(160,(85 + 28 * 5),190,(85 + 28 * 5))
        // PDF.setFontSize(10)
        // PDF.text('Cargo :',20,(85 + 30 * 5))
        // PDF.line(60,(85 + 30 * 5),140,(85 + 30 * 5))
        // PDF.text('Fecha :',145,(85 + 30 * 5))
        // PDF.setFontSize(8)
        // PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,165,(84 + 30 * 5))
        // PDF.line(160,(85 + 30 * 5),190,(85 + 30 * 5))
    }

    let cantidad = (calidadVida.Capacitaciones.length <= 20) ? 20 : calidadVida.Capacitaciones.length

    let usuario2 = JSON.parse(localStorage.getItem('usuario'))
    PDF.setFontSize(10)
    PDF.text('OBSERVACIONES :',20,(85 + (cantidad + 2) * 5))
    PDF.line(55,(85 + 22 * 5),190,(85 + (cantidad + 2) * 5))
    PDF.line(20,(85 + 23 * 5),190,(85 + (cantidad + 3) * 5))
    PDF.line(20,(85 + 24 * 5),190,(85 + (cantidad + 4) * 5))
    PDF.text('RESPONSABLE DEL REGISTRO',20,(85 + (cantidad + 6) * 5))
    PDF.text('Nombre y Apellido :',20,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(8)
    PDF.text(`${usuario2.fullusername}`,60,(84 + (cantidad + 8) * 5))
    PDF.line(60,(85 + (cantidad + 8) * 5),140,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(10)
    PDF.text('Fecha :',145,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(8)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,168,(84 + (cantidad + 8) * 5))
    PDF.line(160,(85 + (cantidad + 8) * 5),190,(85 + (cantidad + 8) * 5))
    PDF.setFontSize(10)
    PDF.text('Cargo :',20,(85 + (cantidad + 10) * 5))
    PDF.line(60,(85 + (cantidad + 10) * 5),140,(85 + (cantidad + 10) * 5))
    PDF.text('Fecha :',145,(85 + (cantidad + 10) * 5))
    PDF.setFontSize(8)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,168,(84 + (cantidad + 10) * 5))
    PDF.line(160,(85 + (cantidad + 10) * 5),190,(85 + (cantidad + 10) * 5))

    PDF.save('capacitación-vmo-calidad-vida.pdf')
}