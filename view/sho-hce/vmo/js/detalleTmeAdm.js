function _init(){
    setTimeout(function (){
        $("#regresar").show();
    },1000)
    cargarDatos()
    generarData()
    cei10()
}

function getDatosConsultado(){
    let id = localStorage.getItem('idRegistroVMO')

    var settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=objecttmea&TranstornoMEAId="+id,
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);

        $('#tipo_puesto').append(`<option>${(response.TipoPuesto == 1) ? 'Administrativo' : 'Operativo' }</option>`)

        $('#HorasDiariasSentado').val(response.HorasDiariasSentado)

        $('#AntecedentesOseoMusculares').val(response.AntecedentesOseoMusculares)

        $('#ZonaColumnCervical').append(`<option>${(response.ZonaColumnCervical == 2) ? 'Alterado' : 'Normal' }</option>`)
        $('#ZonaColumnDorsal').append(`<option>${(response.ZonaColumnDorsal == 2) ? 'Alterado' : 'Normal' }</option>`)
        $('#ZonaColumnLumbar').append(`<option>${(response.ZonaColumnLumbar == 2) ? 'Alterado' : 'Normal' }</option>`)

        $('#ZonaArtralgiaCadera').append(`<option>${(response.ZonaArtralgiaCadera == 2) ? 'Alterado' : 'Normal' }</option>`)
        $('#ZonaArtralgiaCodo').append(`<option>${(response.ZonaArtralgiaCodo == 2) ? 'Alterado' : 'Normal' }</option>`)
        $('#ZonaArtralgiaHombro').append(`<option>${(response.ZonaArtralgiaHombro == 2) ? 'Alterado' : 'Normal' }</option>`)
        $('#ZonaArtralgiaMano').append(`<option>${(response.ZonaArtralgiaMano == 2) ? 'Alterado' : 'Normal' }</option>`)
        $('#ZonaArtralgiaRodilla').append(`<option>${(response.ZonaArtralgiaRodilla == 2) ? 'Alterado' : 'Normal' }</option>`)
        $('#ZonaArtralgiaTobillos').append(`<option>${(response.ZonaArtralgiaTobillos == 2) ? 'Alterado' : 'Normal' }</option>`)

        setDataAdjunto(response)
        setDataDiagnosticoTME(response.DiagnosticosCIE10)
        setDataCapacitacion(response.Capacitaciones)
    });
}

function generarData(){
    var settings = {
        url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=generaldata",
        method: "GET",
        dataType: "json",
        headers: {
            apikey: constantes.apiKey,
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response.SeccionesAfectadas);
        general = response
    });
}

function cei10(){
    var settings = {
        url: apiUrlsho+"/api/hce_Get_021_CIE10_busqueda?code=Kr7q88AoJqtcFZLAx3w8cS7kZ8ezNaxCr/YUbbfUMvEQH1zUvDsxjg==&IdCIE10=&Descripcion=",
        method: "GET",
        dataType: "json"
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        datosCEI = response.CEI10
        getDatosConsultado()
    });
}

function setDataAdjunto(response){
    const adjunto_archivo = response.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 1
    })

    const adjunto_firma = response.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 2
    })

    const adjunto_ficha = response.Adjuntos.filter(adjunto => {
        return adjunto.Tipo == 3
    })

    if(adjunto_firma.length > 0){
        $('#lista-adjunto-firma').removeClass('d-none')
    }else{
        $('#adjuntarFirma').append(`<div class="null-program">
            No hay archivos adjuntos <img src="./images/exclamation-circle.svg" width="25px" height="25px" class="ml-2"/>
        </div>`)
    }

    if(adjunto_archivo.length > 0){
        $('#lista-adjunto-archivos').removeClass('d-none')
    }else{
        $('#adjuntarArchivos').append(`<div class="null-program">
            No hay archivos adjuntos <img src="./images/exclamation-circle.svg" width="25px" height="25px" class="ml-2"/>
        </div>`)
    }

    if(adjunto_ficha.length > 0){
        $('#lista-adjunto-fichas').removeClass('d-none')
    }else{
        $('#adjuntarFicha').append(`<div class="null-program">
            No hay archivos adjuntos <img src="./images/exclamation-circle.svg" width="25px" height="25px" class="ml-2"/>
        </div>`)
    }

    $('#table_adjunto_firma-count-row').html(adjunto_firma.length)
    $('#table_adjunto-archivos-count-row').html(adjunto_archivo.length)
    $('#table_adjunto_fichas-count-row').html(adjunto_ficha.length)

    $('#adjunto_firma').pagination({
        dataSource: adjunto_firma,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_firma').html(html);
        }
    })

    $('#pagination-adjunto-archivos').pagination({
        dataSource: adjunto_archivo,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto-archivos').html(html);
        }
    })

    $('#paginacion_adjunto_fichas').pagination({
        dataSource: adjunto_ficha,
        callback: function(data, pagination) {
            var html = simpleTemplatingAdjunto(data);
            $('#content_adjunto_fichas').html(html);
        }
    })
}

function simpleTemplatingAdjunto(data){
    const date = new Date();
    let CreadoFecha = `${date.getDate()}/${((parseInt(date.getMonth()) + 1) < 10) ? '0'+(parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1}/${date.getFullYear()}`
    var html = '';
    if(data.length > 0){
        $.each(data, function(index, item){
            html += `<tr>
                <td>${item.NombreArchivo}</td>
                <td>${CreadoFecha}</td>
                <td>
                    <a class="btn btn-link shadow-none float-right" href="${item.Archivo}" download="${item.NombreArchivo}">
                      <img class="inject-svg" src="./images/sho/download.svg" fill="#207345" style="width:16px !important">
                    </a>
                </td>
            </tr> 
        `
        });
    }else{
        html += ` <tr>
            <td className="text-center" colSpan="5" style="text-align: center">No se encontro resultados</td>
        </tr>`
    }
    return html;
}

function setDataDiagnosticoTME(DiagnosticosCIE10){

    $('#content_diagnostico').html('')
    $('#table_diagnostico-count-row').html(DiagnosticosCIE10.length)
    DiagnosticosCIE10.forEach(item => {

        const diagnostico = datosCEI.filter(dato => {
            return dato.Code == item.CIE10
        })

        console.log("diagnostico => "+diagnostico.length)

        const general1 = general.SistemasAfectados.filter(itemGeneral => {
            return itemGeneral.Id == item.SistemaAfectado
        })

        const general2 = general.SeccionesAfectadas.filter(itemGeneral => {
            return itemGeneral.Id == item.SeccionAfectada
        })
        const date = new Date()
        if(diagnostico.length != 0){
            $('#content_diagnostico').append(`<tr>
                <td>${item.Diagnostico}</td>
                <td>${item.CIE10Code}</td>
                <td>${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}/${(parseInt(date.getMonth() + 1) < 10) ? '0'+parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1)}/${date.getFullYear()}</td>
                <td>${diagnostico[0].Especilidades}</td>
                <td>${general1[0].Descripcion}</td>
                <td>${general2[0].Descripcion}</td>
            </tr>`)
        }else{
            $('#content_diagnostico').append(`<tr>
                <td>${item.Diagnostico}</td>
                <td>${item.CIE10Code}</td>
                <td>${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}/${(parseInt(date.getMonth() + 1) < 10) ? '0'+parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1)}/${date.getFullYear()}</td>
                <td>--</td>
                <td>${general1[0].Descripcion}</td>
                <td>${general2[0].Descripcion}</td>
            </tr>`)
        }

    })
}

function setDataCapacitacion(Capacitaciones){
    $('#table_capacitacion-count-row').html(Capacitaciones.length)
    console.log(Capacitaciones.length)
    $('#content_capacitacion').html('')
    Capacitaciones.forEach((capacitacion, index) => {
        $('#content_capacitacion').append(`
            <tr>
                <td>${index + 1}</td>
                <td>${capacitacion.Tema}</td>
                <td>${capacitacion.Comentario}</td>
                <td>
                    
                </td>
            </tr>    
        `)
    })
}
