var idHC_EMO = 0;
var ObjTabla='';

$(document).ready(function (){
    $('#anterior_slider').on('click',onAnteriorSliderClick);
    $('#siguiente_slider').on('click',onSiguienteSliderClick);
    setTimeout(function (){
        $('.nav-item').on('click',onElementoSliderClick);
    },1000)
    ecSp4CargarPlucksEMO()
})

function ecSp4CargarPlucksEMO() {

    let url = apiUrlsho+`/api/hce_Get_500_plucks_emo?code=e42XLr4TQ8b819cU9ZQPuLwCprU9oG7CiEheRWvWzFxnRrdVDpBXTA==`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        pluck_EMO['TiposEmo'] = response.TiposEmo;
        pluck_EMO['ResultadosEmo'] = response.ResultadosEmo;
        pluck_EMO['AlturaEstructuralesEmo'] = response.AlturaEstructuralesEmo;
        pluck_EMO['EspaciosEmo'] = response.EspaciosEmo;
        pluck_EMO['MotivosConsultaEmo'] = response.MotivosConsultaEmo;
        pluck_EMO['NivelesSeguimientoVMO'] = response.NivelesSeguimientoVMO;

    });
}

function ecSp4VerDetalleEMO(IdEMO){

    idDetalleEMO = IdEMO;

    handlerUrlhtml('contentGlobal', 'view/sho-hce/emo/verEMO.html', 'Detalle del EMO');

}

function onAtencionMedicaClick(){

    // ControlLateralEmo = true;
    CALIDAD_DE_VIDA_PAGE = true
    setTimeout(function (){
        $("#regresar").show();
    },1000)

}

function onElementoSliderClick(){

    $('#container-slider').find("li.active").removeClass("active");
    $(this).addClass("active");

    ObjTabla.column(0).search($('#container-slider li.active:first').attr("data")).draw();
    $('#table_emo-count-row').text(ObjTabla.page.info().recordsDisplay);

    $('#table-programa-calidad').addClass('d-none');
    $('#null-programa-calidad').removeClass('d-none');

    $('#table-programa-conservacion-auditiva').addClass('d-none');
    $('#null-programa-conservacion-auditiva').removeClass('d-none');

    $('#table-programa-transtorno-muscular-esqueletico').addClass('d-none');
    $('#null-programa-transtorno-muscular-esqueletico').removeClass('d-none');

    let idVMO = localStorage.getItem('idVMO')

    $('#'+idVMO).removeClass('row-active')
}

$('#mostrar_datos_mvmo').on('click',onClickMostrarDatos);
$('#ocultar_datos_mvmo').on('click',onClickOcultarDatos);

function onClickMostrarDatos(){
    $('#mostrar_datos_mvmo').hide()
    $('#ocultar_datos_mvmo').show()
    // $('.datos_hc_general_trabajador').hide()
    $('#datos_hc_general_trabajador').show()
}

function onClickOcultarDatos(){
    $('#ocultar_datos_mvmo').hide()
    $('#mostrar_datos_mvmo').show()
    // $('.show-less').show()
    $('#datos_hc_general_trabajador').hide()
}

function ecSp4VerDatosCompletoTrabajador() {
    //let datos = paObj_ec[IdHC].a;

    //Se carga el modal con los datos completos del trabajador
    // let datos = paObj_HistoriaAtencionTemporal[idDetalleAtencionMedicaHC];

    let url = apiUrlsho+`/api/hce_Get_600_historia_clinica_datos_generales_completo?code=x65sNwDvhbkdVQk0BvFAlVp7TVvemyvLlzYet9HXKgv5H2Fb4xXaUg==`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }
    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": { "IdHC": idHC }
    };

    return $.ajax(settings).done(response => {
        console.log('response',response)
        let datos = response.HistoriaClin[0]

        sedeAreaGerencia.Sedes.forEach((e) => {
            if (e.Id == datos.SedeId_Empresa_H) {
                datos.SedeDescripcion = e.Description
            }
        });

        sedeAreaGerencia.Area.forEach((e) => {
            if (e.Id == datos.AreaId_Empresa_H) {
                datos.AreaDescripcion = e.Description
            }
        });

        Swal.fire({
            title: "Datos del trabajador",
            html: `
      <div class="text-left">
        <div class="row my-3">
          <div class="col-12">
            <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos Principales</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Documento: </b></span>
            <span>${datos.NroDocumento_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Nombres: </b></span>
            <span>${datos.Nombres_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Apellidos: </b></span>
            <span>${datos.Apellidos_Trabajador_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>C.Colaborador: </b></span>
            <span>${datos.CodigoColaborador_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Telefono: </b></span>
            <span>${datos.Telefono_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Sexo: </b></span>
            <span>${(datos.Sexo_Trabajador_H == 1) ? 'Masculino' : 'Femenino'}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-12">
            <span style="color: #254373"><b>Dirección: </b></span>
            <span>${datos.Direccion_Trabajador_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>F.Nacimiento: </b></span>
            <span>${datos.fecha_nacimiento}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Edad: </b></span>
            <span>${datos.Edad_Trabajador_H}</span>
          </div>
        </div>
        <hr class="my-4">
        <div class="row my-3">
          <div class="col-12">
            <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos de la empresa</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Sede: </b></span>
            <span>${datos.SedeDescripcion}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Área: </b></span>
            <span>${datos.AreaDescripcion}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Cargo: </b></span>
            <span>${datos.CargoJefe_Empresa_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Jefe inmediato: </b></span>
            <span>${datos.JefeInmediato_Empresa_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Celular: </b></span>
            <span>${datos.Celular_Empresa_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Telefono: </b></span>
            <span>${datos.Telefono_Empresa_H}</span>
          </div>
        </div>
      </div>            
    `,
            iconHtml: '<img src="./images/sho/perfil.svg">',
            width: 800,
            showCancelButton: true,
            showConfirmButton: false,
            cancelButtonColor: "#ff3636",
            cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
        })
        $('.swal2-cancel').css('width', '200px');
        $('.swal2-html-container').css('overflow', 'visible');
    })

}

function onAnteriorSliderClick(){

    if ( $('#container-slider').find("li.active").prev("li").length > 0 ) {

        // hacer algo aquí si el elemento existe

        if( $('#container-slider').find("li.active").prev("li").hasClass("d-none") ){

            $('#container-slider').find("li.active").prev("li").removeClass("d-none");
            $('#container-slider').find("li.active").removeClass("primer-show-slider");
            $('#container-slider').find("li.active").prev("li").addClass("primer-show-slider");


            $('#container-slider').find("li.ultimo-show-slider").prev("li").addClass("ultimo-show-slider");
            $('#container-slider li.ultimo-show-slider:last').addClass("d-none");
            $('#container-slider li.ultimo-show-slider:last').removeClass("ultimo-show-slider");

        }

        $('#container-slider').find("li.active a").removeClass("active");
        $('#container-slider').find("li.active").prev("li").addClass("active");

        $('#container-slider li.active:last').removeClass("active");
        $('#container-slider').find("li.active a").addClass("active");

        ObjTabla.column(0).search($('#container-slider li.active:first').attr("data")).draw();
        $('#table_emo-count-row').text(ObjTabla.page.info().recordsDisplay);

    }
    else
    {

    }

}

function onSiguienteSliderClick(){

    if ( $('#container-slider').find("li.active").next("li").length > 0 ) {

        // hacer algo aquí si el elemento existe

        if( $('#container-slider').find("li.active").next("li").hasClass("d-none") ){

            $('#container-slider').find("li.active").next("li").removeClass("d-none");
            $('#container-slider').find("li.active").removeClass("ultimo-show-slider");
            $('#container-slider').find("li.active").next("li").addClass("ultimo-show-slider");


            $('#container-slider').find("li.primer-show-slider").next("li").addClass("primer-show-slider");
            $('#container-slider li.primer-show-slider:first').addClass("d-none");
            $('#container-slider li.primer-show-slider:first').removeClass("primer-show-slider");

        }

        $('#container-slider').find("li.active a").removeClass("active");
        $('#container-slider').find("li.active").next("li").addClass("active");

        $('#container-slider li.active:first').removeClass("active");
        $('#container-slider').find("li.active a").addClass("active");

        ObjTabla.column(0).search($('#container-slider li.active:first').attr("data")).draw();
        $('#table_emo-count-row').text(ObjTabla.page.info().recordsDisplay);

    }
    else
    {

    }

}

function ecSp4cargarDatosEMOIdHC(IdHistoria) {

    //Variable a consultar en la BD. Se consulta con el ID de la Historia Clínica
    //idHC_EC = Id_HC;

    //Se especifican los parametros para la consulta
    let url = apiUrlsho+`/api/hce_Get_501_emo_idhc?code=m6YCKLXoPwbe2CKeXot2pFQlrUziWEFV5uJ5vEaUAKznYZ8l22UHJw==&httpmethod=objectlist`;
    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }
    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": { "IdHC": IdHistoria }
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done((response) => {

        console.log("ecSp4cargarDatosEMOIdHC: "+JSON.stringify(response));

        paObj_EMO["EMOS"] = response.EMOS;

        ecSp4CargarTablaEMO();

    });

}

function ecSp4CargarTablaEMO(){
    const date = new Date();
    let contenido = '';
    let html_tbody = '';

    for(let i = 2010; i <= date.getFullYear(); i++){

        if(i >= date.getFullYear() - 7){

            if(i == date.getFullYear() - 1){
                $('#container-slider').append(`<li class="nav-item active " data="${i}">
                    <a class="nav-link tab-slider active" id="${i}-tab" data-toggle="tab" href="#transtorno-musculo" role="tab" aria-controls="transtorno-musculo" aria-selected="true">${i}</a>
                </li>`)
            }else if(i == date.getFullYear() - 7){
                $('#container-slider').append(`<li class="nav-item primer-slider primer-show-slider" data="${i}">
                    <a class="nav-link tab-slider" id="${i}-tab" data-toggle="tab" href="#transtorno-musculo" role="tab" aria-controls="transtorno-musculo" aria-selected="true">${i}</a>
                </li>`)
            }else if(i == date.getFullYear()){
                $('#container-slider').append(`<li class="nav-item ultimo-slider ultimo-show-slider" data="${i}">
                    <a class="nav-link tab-slider" id="${i}-tab" data-toggle="tab" href="#transtorno-musculo" role="tab" aria-controls="transtorno-musculo" aria-selected="true">${i}</a>
                </li>`)
            }else{
                $('#container-slider').append(`<li class="nav-item" data="${i}">
                    <a class="nav-link tab-slider" id="${i}-tab" data-toggle="tab" href="#transtorno-musculo" role="tab" aria-controls="transtorno-musculo" aria-selected="true">${i}</a>
                </li>`)
            }

        }

        if(i >= 2010 && i <= date.getFullYear() - 8){
            $('#container-slider').append(`<li class="nav-item d-none" data="${i}">
                <a class="nav-link tab-slider" id="${i}-tab" data-toggle="tab" href="#transtorno-musculo" role="tab" aria-controls="transtorno-musculo" aria-selected="false">${i}</a>
            </li>`)
        }

    }

    //Cargando Select de Áreas
    contenido = $('#content_emo');
    contenido.empty();

    paObj_EMO["EMOS"].forEach((Item) => {

        //paObj_detalle[Item.Id] = Item;
        let fechavmo = Item.FechaRegistro.split('-')[0]
        console.log('fechavmo',fechavmo)
        html_tbody =  "<tr id='"+Item.Id+"'>";
        html_tbody += "<td>"+new Date(Item.FechaRegistro).toLocaleDateString('en-GB').split('T')[0]+"</td>";
        //html_tbody += "<td>"+(Item.HoraRegistro).substring(0,5);+"</td>";
        //html_tbody += "<td>"+new Date("August 19, 1975 "+Item.HoraRegistro+" GMT+00:00").toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' })+"</td>";
        html_tbody += "<td>"+new Date("August 19, 1975 "+Item.HoraRegistro).toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' })+"</td>";
        html_tbody += "<td>";
        html_tbody += "<div class='dropdown float-right dropleft'>";
        html_tbody += "<div class='more-info' data-toggle='dropdown'>";
        html_tbody += "<img src='images/iconos/menu_responsive.svg' style='width: 18px; margin-right: 12px'/>";
        html_tbody += "</div>";
        html_tbody += "<div class='dropdown-menu' aria-labelledby='item_descanso'>";
        html_tbody += "<ul>";
        html_tbody += "<li onclick='onAtencionMedicaClick();ecSp4VerDetalleEMO("+Item.Id+");'>";
        html_tbody += "<img src='./images/sho/eyeIcon.svg' width='16' height='16' />";
        html_tbody += "<span>Ver EMO</span>";
        html_tbody += "</li>";
        html_tbody += "<li onclick='verMisVMO("+Item.Id+","+fechavmo+");'>";
        html_tbody += "<img src='./images/sho/eyeIcon.svg' width='16' height='16'/>";
        html_tbody += "<span>Ver mis VMO</span>";
        html_tbody += "</li>";
        html_tbody += "</ul>";
        html_tbody += "</div>";
        html_tbody += "</div>";
        html_tbody += "</td>";
        html_tbody += "</tr>";

        contenido.append(html_tbody);

    });

    ObjTabla = $('#table_emo').DataTable({
        "pageLength": 10,
        "ordering": false,
        //"scrollX": true,
        "language": {
            "paginate": {
                "next": "»",
                "previous": "«"
            },
            "loadingRecords":"Cargando registros...",
            "processing":"Procesando...",
            "infoPostFix":"",
            "zeroRecords": "No se encontraron registros"
        },
        dom:
            "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>"+
            "<'row row-records'<'col-sm-12'tr>>"+
            "<'row row-info'<'col-sm-12 col-md-12'p>>"
    });

    ObjTabla.column(0).search($('#container-slider li.active:first').attr("data")).draw();

    $('#table_emo-count-row').text(ObjTabla.page.info().recordsDisplay);

    //$('.scrollbar').removeClass('dataTables_scrollBody');

    //ESTO ES NUEVO. PARA EVITAR EL NO ALINEAMIENTO DENTRE HEADER Y BODY
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

}

async function _init(IdHistoria) {

    //Se inicia el Cargando
    showLoading();
    await ecSp4cargarDatosEMOIdHC(IdHistoria);
    showLoading();
    localStorage.setItem('UsuarioVMO','')

}


async function irPrograma(IdHC,tipo){
    const idVMO = localStorage.getItem('idVMO');

    if(idVMO != ''){
        idHC = IdHC;
        actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
        backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
        backTitle = "Historia clínica";
        $("#regresar").show();
        if(tipo == 1){
            handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/nuevoRegistroVMO.html', 'Registro de calidad de vida');
            $("#logoCompany1 b").text('Registro de calidad de vida');
            CALIDAD_DE_VIDA_PAGE = true
        }else if(tipo == 2){
            handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/nuevoRegistroConservacionAuditivaVMO.html', 'Registro de conservación auditiva');
            $("#logoCompany1 b").text('Registro de conservación auditiva');
            CONSERVACION_AUDITIVA_PAGE = true
        }else if(tipo == 3){
            handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/nuevoRegistroTranstornoMusculoEsqueleticoVMO.html', 'Registro de TME ADM');
            $("#logoCompany1 b").text('Registro de TME ADM');
            TRANSTORNO_MUSCULO_ESQUELETICO_PAGE = true
        }
    }else{
        Swal.fire(
            'Error',
            'No se ha seleccionado un registro EMO',
            'error'
        )
    }

}

function _initNuevaAtencionMedica(){
    cargarDatos()

    cardarDatosGenerales();

    let contenido = $('#content_hospitalizacion');
    contenido.append(`
        <tr>
            <td class="text-center" colspan="5">No se encontro resultados</td>
        </tr>    
    `)

    $('#table_hospitalizacion-count-row').text(0);

    localStorage.setItem('UsuarioVMO','');
}

function cargarDatos(){
    let infodatos = localStorage.getItem('UsuarioVMO');
    let dni = 0

    if(infodatos){
        dni = JSON.parse(infodatos).A_DniPaciente;
    }else{
        dni = paObj_hc[idHC].a.NroDocumento_Trabajador_H;
    }

    var settings = {
        url: `${apiUrlsho}/api/hce_Get_001_historia_clinica?code=5lJWTsRDsoxqo9VoOxIzfQAPyCTxUTqLpvgY5tuHluCZlSodpQ/Y7w==&AreaId_Empresa_H=0&SedeId_Empresa_H=0&NroDocumento_Trabajador_H=${dni}&Buscador=`,
        method: "GET",
        dataType: "json",
        timeout: 0,
        headers: {
            "apikey": constantes.apiKey,
            "Content-Type": "application/json"
        },
    };

    $.ajax(settings).done(function (response) {
        let datos = response.HistoriaClin[0]

        console.log(datos)

        $('#dat_am_dni_trabajador').val(datos.NroDocumento_Trabajador_H);
        $('#dat_am_nombres_trabajador').val(datos.Nombres_Trabajador_H);
        $('#dat_am_apellidos_trabajador').val(datos.Apellidos_Trabajador_H);
        $('#dat_am_edad_trabajador').val(datos.Edad_Trabajador_H);
        $('#dat_am_puesto_trabajo_trabajador').val(datos.PuestoTrabajo_Empresa_H)

        sedeAreaGerencia.Sedes.forEach((e) => {
            if (e.Id == datos.PlantaId_Empresa_H) {
                $('#dat_am_planta_trabajador').append(`<option>${e.Description}</option>`);
            }
        });

        sedeAreaGerencia.Area.forEach((e) => {
            if (e.Id == datos.AreaId_Empresa_H) {
                $('#dat_am_area_trabajador').append(`<option>${e.Description}</option>`);
            }
        });

        sedeAreaGerencia.Gerencia.forEach((e) => {
            if (e.Id == datos.GerenciaId_Empresa_H) {
                $('#dat_am_gerencia_trabajador').append(`<option>${e.Description}</option>`);
            }
        });

        const idVMO = localStorage.getItem('idVMO');

        if(infodatos == '' || infodatos == undefined){
            let damotsEMos = paObj_EMO["EMOS"].filter(emo => {
                return emo.Id == idVMO
            });

            $('#input_codigo_emo').val(damotsEMos[0].CodeEmo);
            $('#input_fecha_emo').val(damotsEMos[0].FechaEmo.split("T")[0])

            var settings = {
                url: apiUrlsho+"/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=generaldata",
                method: "GET",
                timeout: 0,
                dataType: "json",
                headers: {
                    "apikey": constantes.apiKey,
                },
            };

            $.ajax(settings).done(function (response) {
                console.log('lista de response',response)
                response.TipoVmo.forEach(tipo => {
                    if(tipo.Id == damotsEMos[0].TipoEmo){
                        $('#input_tipo_vmo').append(`<option>${tipo.Descripcion}</option>`)
                    }
                })

                response.NivelSeguimiento.forEach(nivel => {
                    if(nivel.Id == damotsEMos[0].VmoNivelSegCalidadVida){
                        $('#input_nivel_seguimiento').append(`<option>${nivel.Descripcion}</option>`)
                    }
                })

            });

        }else{
            let datos2 = JSON.parse(localStorage.getItem('UsuarioVMO'))
            $('#input_codigo_emo').val(datos2.CodeEmo);
            $('#input_fecha_emo').val(datos2.FechaEmo.split("/")[2]+'-'+datos2.FechaEmo.split("/")[1]+'-'+datos2.FechaEmo.split("/")[0])
            // $('#input_tipo_vmo').append(`<option>Calidad de vida</option>`)
            $('#input_nivel_seguimiento').append(`<option>${datos2.NivelSeguimientoDescripcion}</option>`)
        }

    });
}

function verMisVMO(idVMO,fechavmo){

    $('#content_emo tr').removeClass('row-active');
    $('#'+idVMO).addClass('row-active');

    localStorage.setItem('idVMO', idVMO);
    ID_VMO_REGISTRO = idVMO;
    FECHA_TAB = fechavmo;
    const headers = {
        apikey: constantes.apiKey,
    }
    var settings = {
        url: `${apiUrlsho}/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=objectlist&EmoId=${idVMO}`,
        method: "GET",
        headers: headers,
        dataType: 'json',
    };

    $.ajax(settings).done(function (response) {
        console.log(response);

        listaVMO = response

        if(response.CalidadVida.length != 0){
            $('#content_programa_calidad_vida').html('')
            $('#table-programa-calidad').removeClass('d-none')
            $('#null-programa-calidad').addClass('d-none')
            $('#table_hospitalizacion-count-row').html(response.CalidadVida.length)
            response.CalidadVida.forEach(item => {
                $('#content_programa_calidad_vida').append(`
                <tr>
                    <td>${item.CreadoFecha}</td>
                    <td>${item.MedicoCargo}</td>
                    <td>
                        <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroLista(${item.Id});">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="descargarConstanciaDeAutorizacionCalidadDeVida();">
                                        <img src="./images/sho/download.svg" width="16" height="16">
                                        <span>Descargar Registro</span>
                                    </li>
                                    <li onclick="VerProgreso(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            `)
            })
        }else{
            $('#table-programa-calidad').addClass('d-none')
            $('#null-programa-calidad').removeClass('d-none')
        }

        if(response.ConservacionAuditiva.length != 0){
            $('#content_programa_conservacion-auditiva').html('')
            $('#table-programa-conservacion-auditiva').removeClass('d-none')
            $('#null-programa-conservacion-auditiva').addClass('d-none')
            $('#table_conservacion-auditiva-count-row').html(response.ConservacionAuditiva.length)
            response.ConservacionAuditiva.forEach(item => {
                $('#content_programa_conservacion-auditiva').append(`
                <tr>
                    <td>${item.CreadoFecha}</td>
                    <td>${item.MedicoCargo}</td>
                    <td>
                        <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroListaAuditiva(${item.Id});">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="descargarConstanciaDeAutorizacionAuditiva();">
                                        <img src="./images/sho/download.svg" width="16" height="16">
                                        <span>Descargar Registro</span>
                                    </li>
                                    <li onclick="VerProgresoCA(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            `)
            })
        }else{
            $('#table-programa-conservacion-auditiva').addClass('d-none')
            $('#null-programa-conservacion-auditiva').removeClass('d-none')
        }

        if(response.TranstornoMusculoEsqueletico.length != 0){
            $('#content_programa_transtorno-muscular-esqueletico').html('')
            $('#table-programa-transtorno-muscular-esqueletico').removeClass('d-none')
            $('#null-programa-transtorno-muscular-esqueletico').addClass('d-none')
            $('#table_transtorno-muscular-esqueletico-count-row').html(response.TranstornoMusculoEsqueletico.length)
            response.TranstornoMusculoEsqueletico.forEach(item => {
                $('#content_programa_transtorno-muscular-esqueletico').append(`
                <tr>
                    <td>${item.CreadoFecha}</td>
                    <td>${item.MedicoCargo}</td>
                    <td>
                        <div class="dropdown float-right dropleft">
                            <div class="more-info" data-toggle="dropdown" aria-expanded="true">
                                <img src="images/iconos/menu_responsive.svg" style="width: 18px; margin-right: 12px">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="item_descanso" x-placement="left-start" style="position: absolute; top: 0px; left: -257px; will-change: top, left;">
                                <ul>
                                    <li onclick="verRegistroTranstornoMusculoEsqueletico(${item.Id},${item.TipoPuesto});">
                                        <img src="./images/sho/eyeIcon.svg" width="16" height="16">
                                        <span>Ver registro</span>
                                    </li>
                                    <li onclick="descargarConstanciaDeAutorizacionTME();">
                                        <img src="./images/sho/download.svg" width="16" height="16">
                                        <span>Descargar Registro</span>
                                    </li>
                                    <li onclick="VerProgresoTME(${item.Id});">
                                        <img src="./images/sho/ver_progreso.svg" width="16" height="16">
                                        <span>Ver Progreso</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            `)
            })
        }else{
            $('#table-programa-transtorno-muscular-esqueletico').addClass('d-none')
            $('#null-programa-transtorno-muscular-esqueletico').removeClass('d-none')
        }

    });
}

function descargarConstanciaDeAutorizacionAuditiva(){

    let UsuarioVMO = paObj_hc[idHC].a

    const date = new Date()
    const PDF = new jsPDF()
    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,10,200,10)
    PDF.line(10,10,10,285)
    PDF.line(10,285,200,285)
    PDF.line(200,10,200,285)
    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,15,190,15)
    PDF.line(20,15,20,33)
    PDF.line(20,33,190,33)
    PDF.line(190,15,190,33)

    PDF.line(60,15,60,33)
    PDF.setFontSize(9)
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO\n         AL PROGRAMA DE VIGILANCIA MÉDICA',62, 22)
    PDF.line(140,15,140,33)

    PDF.setFontSize(10)
    PDF.text('    Versión',142, 19)
    PDF.text('      N°.00',142, 24)
    PDF.line(165,15,165,26)
    PDF.text('  Página:',170, 19)
    PDF.text('   1 de 1',170, 24)
    PDF.line(140,26,190,26)
    PDF.text('    Código: SSM02-F16',145, 31)

    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,80,200,80)
    PDF.setFontSize(16)
    PDF.setFontType("bold");
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO AL PROGRAMA\n' +
        '                                     DE VIGILANCIA MÉDICA',15, 88)
    PDF.line(10,100,200,100)

    PDF.text('PROGRAMA:',25, 120)
    PDF.text('Conservación auditiva',75, 120)

    PDF.text('PARTICIPANTE:',25, 135)
    PDF.text(`${UsuarioVMO.Nombres_Trabajador_H} ${UsuarioVMO.Apellidos_Trabajador_H}`,75, 135)

    PDF.text('DNI:',135, 135)
    PDF.text(`${UsuarioVMO.NroDocumento_Trabajador_H}`,150, 135)

    PDF.text('UNIDAD:',25, 150)

    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == UsuarioVMO.SedeId_Empresa_H
    });

    PDF.text(`${sede[0].Description}`,70, 150)
    PDF.text('ÁREA/PUESTO:',25, 165)

    let area = sedeAreaGerencia.Area.filter(area => {
        return area.Id == UsuarioVMO.AreaId_Empresa_H
    })

    PDF.text(`${area[0].Description} / ${UsuarioVMO.PuestoTrabajo_Empresa_H}`,70, 165)

    PDF.setFontSize(11)
    PDF.setFontType("itali");
    PDF.text('A través del presente documento dejo constancia que deseo participar del programa de vigilancia\n' +
        'médica ocupacional antes referido, comprometiéndome a asistir y a poner en práctica las diversas\n' +
        'pautas preventivas que nos brinden.',25,180)

    PDF.setFontSize(10)
    PDF.setFontType("bold")
    PDF.text('FIRMA DEL TRABAJADOR',28,250)
    PDF.text('FIRMA DEL MÉDICO OCUPACIONAL',120,250)

    PDF.text('Fecha',28,270)
    PDF.text(`${date.getMonth()}-${date.getDate()}-${date.getFullYear()}`,45,270)

    PDF.save('constancia-de-autorizacion-conservacion-auditiva.pdf')
}

function descargarConstanciaDeAutorizacionTME(){

    let UsuarioVMO = paObj_hc[idHC].a

    const date = new Date()
    const PDF = new jsPDF()
    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,10,200,10)
    PDF.line(10,10,10,285)
    PDF.line(10,285,200,285)
    PDF.line(200,10,200,285)
    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,15,190,15)
    PDF.line(20,15,20,33)
    PDF.line(20,33,190,33)
    PDF.line(190,15,190,33)

    PDF.line(60,15,60,33)
    PDF.setFontSize(9)
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO\n         AL PROGRAMA DE VIGILANCIA MÉDICA',62, 22)
    PDF.line(140,15,140,33)

    PDF.setFontSize(10)
    PDF.text('    Versión',142, 19)
    PDF.text('      N°.00',142, 24)
    PDF.line(165,15,165,26)
    PDF.text('  Página:',170, 19)
    PDF.text('   1 de 1',170, 24)
    PDF.line(140,26,190,26)
    PDF.text('    Código: SSM02-F16',145, 31)

    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,80,200,80)
    PDF.setFontSize(16)
    PDF.setFontType("bold");
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO AL PROGRAMA\n' +
        '                                     DE VIGILANCIA MÉDICA',15, 88)
    PDF.line(10,100,200,100)

    PDF.text('PROGRAMA:',25, 120)
    PDF.text('Trastornos músculo esquéletico',75, 120)

    PDF.text('PARTICIPANTE:',25, 135)
    PDF.text(`${UsuarioVMO.Nombres_Trabajador_H} ${UsuarioVMO.Apellidos_Trabajador_H}`,75, 135)

    PDF.text('DNI:',135, 135)
    PDF.text(`${UsuarioVMO.NroDocumento_Trabajador_H}`,150, 135)

    PDF.text('UNIDAD:',25, 150)
    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == UsuarioVMO.SedeId_Empresa_H
    });

    PDF.text(`${sede[0].Description}`,60, 150)
    PDF.text('ÁREA/PUESTO:',25, 165)

    const area = sedeAreaGerencia.Area.filter(area => {
        return area.Id == UsuarioVMO.AreaId_Empresa_H
    })

    PDF.text(`${area[0].Description} / ${UsuarioVMO.PuestoTrabajo_Empresa_H}`,70, 165)

    PDF.setFontSize(11)
    PDF.setFontType("itali");
    PDF.text('A través del presente documento dejo constancia que deseo participar del programa de vigilancia\n' +
        'médica ocupacional antes referido, comprometiéndome a asistir y a poner en práctica las diversas\n' +
        'pautas preventivas que nos brinden.',25,180)

    PDF.setFontSize(10)
    PDF.setFontType("bold")
    PDF.text('FIRMA DEL TRABAJADOR',28,250)
    PDF.text('FIRMA DEL MÉDICO OCUPACIONAL',120,250)

    PDF.text('Fecha',28,270)
    let fecha = `${(date.getDate() < 10) ? '0'+date.getDate() : date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+ (date.getMonth() + 1) : date.getMonth() }-${date.getFullYear()}`
    PDF.text(`${fecha}`,45,270)

    PDF.save('constancia-de-autorizacion-transtorno-musculo-esqueletico.pdf')
}

function descargarConstanciaDeAutorizacionCalidadDeVida(){

    let usuario = paObj_hc[idHC].a

    const date = new Date()
    const PDF = new jsPDF()
    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,10,200,10)
    PDF.line(10,10,10,285)
    PDF.line(10,285,200,285)
    PDF.line(200,10,200,285)
    PDF.setLineWidth(0.2)
    PDF.setDrawColor(0,0,0)
    PDF.line(20,15,190,15)
    PDF.line(20,15,20,33)
    PDF.line(20,33,190,33)
    PDF.line(190,15,190,33)

    PDF.line(60,15,60,33)
    PDF.setFontSize(9)
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO\n         AL PROGRAMA DE VIGILANCIA MÉDICA',62, 22)
    PDF.line(140,15,140,33)

    PDF.setFontSize(10)
    PDF.text('    Versión',142, 19)
    PDF.text('      N°.00',142, 24)
    PDF.line(165,15,165,26)
    PDF.text('  Página:',170, 19)
    PDF.text('   1 de 1',170, 24)
    PDF.line(140,26,190,26)
    PDF.text('    Código: SSM02-F16',145, 31)

    PDF.setLineWidth(2)
    PDF.setDrawColor(91, 155, 213)
    PDF.line(10,80,200,80)
    PDF.setFontSize(16)
    PDF.setFontType("bold");
    PDF.text('CONSTANCIA DE CONSENTIMIENTO DE INGRESO AL PROGRAMA\n' +
        '                                     DE VIGILANCIA MÉDICA',15, 88)
    PDF.line(10,100,200,100)

    PDF.text('PROGRAMA:',25, 120)
    PDF.text('Calidad de vida',75, 120)

    PDF.text('PARTICIPANTE:',25, 135)
    PDF.text(`${usuario.Nombres_Trabajador_H} ${usuario.Apellidos_Trabajador_H}`,75, 135)

    PDF.text('DNI:',135, 135)
    PDF.text(`${usuario.NroDocumento_Trabajador_H}`,150, 135)

    PDF.text('UNIDAD:',25, 150)

    let sede = sedeAreaGerencia.Sedes.filter((e) => {
        return e.Id == usuario.SedeId_Empresa_H
    });

    PDF.text(`${sede[0].Description}`,70, 150)
    PDF.text('ÁREA/PUESTO:',25, 165)

    const area = sedeAreaGerencia.Area.filter(area => {
        return area.Id = usuario.AreaId_Empresa_H
    })
    PDF.setFontSize(13)
    PDF.text(`${area[0].Description} / ${usuario.PuestoTrabajo_Empresa_H}`,70, 165)

    PDF.setFontSize(11)
    PDF.setFontType("itali");
    PDF.text('A través del presente documento dejo constancia que deseo participar del programa de vigilancia\n' +
        'médica ocupacional antes referido, comprometiéndome a asistir y a poner en práctica las diversas\n' +
        'pautas preventivas que nos brinden.',25,180)

    PDF.setFontSize(10)
    PDF.setFontType("bold")
    PDF.text('FIRMA DEL TRABAJADOR',28,250)
    PDF.text('FIRMA DEL MÉDICO OCUPACIONAL',120,250)

    PDF.text('Fecha',28,270)
    PDF.text(`${date.getDate()}-${(date.getMonth() + 1 < 10) ? '0'+(date.getMonth() + 1 ): date.getMonth() + 1}-${date.getFullYear()}`,45,270)

    PDF.save('constancia-de-autorizacion-calidad-vida.pdf')
}

async function irProgramaDetalle(IdHC,tipo){
    const idVMO = localStorage.getItem('idVMO');

    if(idVMO != ''){
        idHC = IdHC;
        actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
        backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
        backTitle = "Historia clínica";
        $("#regresar").show();
        if(tipo == 1){
            handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/calidadVida.html', 'Detalle del registro calidad de vida');
            $("#logoCompany1 b").text('Detalle del registro calidad de vida');
            CALIDAD_DE_VIDA_PAGE = true
        }else if(tipo == 2){
            handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/nuevoRegistroConservacionAuditivaVMO.html', 'Registro de conservación auditiva');
            $("#logoCompany1 b").text('Registro de conservación auditiva');
            CONSERVACION_AUDITIVA_PAGE = true
        }else if(tipo == 3){
            handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/nuevoRegistroTranstornoMusculoEsqueleticoVMO.html', 'Registro de TME ADM');
            $("#logoCompany1 b").text('Registro de TME ADM');
            TRANSTORNO_MUSCULO_ESQUELETICO_PAGE = true
        }
    }else{
        Swal.fire(
            'Error',
            'No se ha seleccionado un registro EMO',
            'error'
        )
    }

}

function verRegistroLista(Id){

    actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    $("#regresar").show();
    localStorage.setItem('idRegistroVMO',Id)

    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/calidadVida.html', 'Detalle del registro calidad de vida');
    $("#logoCompany1 b").text('Detalle del registro calidad de vida');
    CALIDAD_DE_VIDA_PAGE = true

}

function verRegistroListaAuditiva(Id){

    actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    $("#regresar").show();
    localStorage.setItem('idVmoDetalle',Id)

    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/registroConservacionAuditiva.html', 'Detalle del registro conservación auditiva');
    $("#logoCompany1 b").text('Detalle del registro conservación auditiva');
    CONSERVACION_AUDITIVA_PAGE = true

}

function verRegistroTranstornoMusculoEsqueletico(Id,TipoPuesto){

    localStorage.setItem('idRegistroVMO', Id);

    actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    $("#regresar").show();
    TRANSTORNO_MUSCULO_ESQUELETICO_PAGE = true

    if(TipoPuesto == 1){
        handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/TME_ADM.html', 'Detalle del registro TME ADM');
        $("#logoCompany1 b").text('Detalle del registro TME ADM');
    }else{
        handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/detalle/TME_OPE.html', 'Detalle del registro TME OPE');
        $("#logoCompany1 b").text('Detalle del registro TME OPE');
    }

}


function cardarDatosGenerales(){
    let url = apiUrlsho+`/api/hce_get_vmo_all?code=LLs3D3WWcNzl74sw3vvXyHmFj6ruAb3H6a526if21owTd5q/gsJYiw==&httpmethod=generaldata`;
    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }
    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
    }

    $.ajax(settings).done(response => {
        console.log("response cargarDatosGenerales "+ response);
        response.Consumo.forEach(consumo => {
            $('#reg_cali_vida_datos_fumador_select').append(`<option value="${consumo.Id}">${consumo.Descripcion}</option>`)
            $('#reg_cali_vida_datos_consumo_azucar_select').append(`<option value="${consumo.Id}">${consumo.Descripcion}</option>`)
        })

        response.TipoActividad.forEach(tipo => {
            $('#reg_cali_vida_datos_actividad_fisica_select').append(`<option value="${tipo.Id}">${tipo.Descripcion}</option>`)
        })

        response.ConsumoAlcohol.forEach(consumoAlcohol => {
            $('#reg_cali_vida_datos_alcohol_select').append(`<option value="${consumoAlcohol.Id}">${consumoAlcohol.Descripcion}</option>`)
        })

        response.RiesgoCardiovascular.forEach(riego => {
            $('#reg_cali_vida_datos_riesgo_cardiovascular_select').append(`<option value="${riego.Id}">${riego.Descripcion}</option>`)
        })
    })
}

function VerProgreso(Id){
    actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/verProgresoVMO.html', 'Progreso calidad de vida');
    $("#regresar").show();
    $("#logoCompany1 b").text('Progreso calidad de vida');
    CALIDAD_DE_VIDA_PAGE = true
}

function VerProgresoCA(Id){
    actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/verProgresoCA.html', 'Progreso conservación auditiva');
    $("#regresar").show();
    $("#logoCompany1 b").text('Progreso conservación auditiva');
    CONSERVACION_AUDITIVA_PAGE = true
}

function VerProgresoTME(Id){
    actualPageSystemSoma = "view/sho-hce/atenciones_medicas/nuevaAtencionMedica.html";
    backPageSystemSoma = "view/sho-hce/historia_clinica/datosGenerales.html";
    backTitle = "Historia clínica";
    handlerUrlhtml('contentGlobal', 'view/sho-hce/vmo/verProgresoTME.html', 'Progreso TME');
    $("#regresar").show();
    $("#logoCompany1 b").text('Progreso TME');
    TRANSTORNO_MUSCULO_ESQUELETICO_PAGE = true
}