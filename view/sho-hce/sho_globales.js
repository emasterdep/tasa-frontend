/****************************************************************************************
 * VISUAL SAT - [2021]
 * PROYECTO : SIGTASA
 * ESQUEMA : SSOMA
 * SPRINT  : 3
 * MODULO : HISTORIA CLINICA
 * OPERADORES_________________________________________________________________________
 * | # | PROGRAMADOR     |  |      FECHA   |  |   HORA   |           CORREO           |
 * | 2 | Andy Vàsquez    |  |   06/09/2021 |  | 11:16:00 |     caracas1348@gmail.com  |
 * |___|_________________|__|______________|__|__________|____________________________|
 *
 * DESCRIPCION: ARCHIVO DE VARIABLES GLOBALES
 *
 * ARCHIVOS DE SERVICIOS   _________________________________________
 * | # |     MODULO             |  |         NOMBRE                 |
 * | 1 |   SALUD OCUPACIONAL    |  |                                |
 * |________________________________________________________________|
 *
 *
 * VERSION: 0.1 Beta
 *********************************************************************************************************************************************************************************
 * NOTA IMPORTANTE:  EN FECHA  *** 06/09/2021 Andy  **** Vasquez: Procede a realizar un ajuste general  de funcionamiento y orden del modulo de historia clinica para optimizarlo
 *********************************************************************************************************************************************************************************

 *******************************************************************************************/

//############################################################# CONJUNTO E VARIBLES GLOBALES DE SALUD E HIGIENE ####################################################

//-----------   VARIABLES GLOBALES QUE AFECTAN VARIOS MODULOS   -----------------

var ID_ORIGEN = 0; //debe ser el id de una (hc-5, Am-1, otros sprint4)
var ID_TIPO_ORIGEN = 5; // CONTIENE EL CODIGO DE UNA INTERCONSULTA, TRASNFERENCIA, DESCANSO MEDICO
var REG_DESDE = 0; // INDICA SI EL REGISTRO ES DESDE UNA VENTANA EMERGENTE,  0 PARA SU REGISTRO DESDE SU BANDEJA

//1 MODULO DE NUEVA ATENCION MEDICA
//2 MODULO DE ENFERMEADDES CRONICAS
/*
 1 -Atención médica
 2 -VMO
 3 -Enfermedades_ocupacionales
 4 -Enfermedades_crónicas
 5 -Registro_a_Demanda (Bandeja General)
*/

//###########################################################################################################
//------------------------------------------- VARIABLES GLOBALES HISTORIA CLINICA  --------------------------------------------

var paObj_hc = []; //objeto que se utilizara y se almacenara toda la informacion de la historia clinica
var DA_TX = []; //DATA DE ATENCIONES MEDICAS ASOCIADAS POR EL ID DE LA HISTORIA CLINICA
var HTMLEXCEL = "";
var idSV = 0;
var idHC = 0;
var estadoHC = 0;
var idA = 0;
var menuAnt = "menu_lateral1"; //contiene los valores del menu anteriormente seleccionado en la historia clinica
var flag_hc = 0; //variable global del contenedor de handler_hc

var mnu = [];
var cachePage = [];
var ddgHtml = "";
var objTemp = [];

//----------------------------------------------- VARIABLES GLOBALES HISTORIA CLINICA --------------------------------------------
//###########################################################################################################

var mnu = [];
var cachePage = [];
var ddgHtml = "";
var objTemp = [];
var HistoriaClinBD = [];
var HistoriaClinData = [];

var imgPerson = "";

//----------------------------------------------- VARIABLES GLOBALES mi vmo --------------------------------------------
//###########################################################################################################
let ID_VMO_REGISTRO = null;
let listaVMO;
let FECHA_TAB = null;
let CIE_10 = null;
// - calidad de vida
let CALIDAD_DE_VIDA_PAGE = false;
// - conservacion auditiva
let CONSERVACION_AUDITIVA_PAGE = false;
let conservacionAuditiva = null;
// - transtorno musculo esqueletico
let TRANSTORNO_MUSCULO_ESQUELETICO_PAGE = false;
let transtornoMusculoEsqueletico = null;
let registroTranstornoMusculoEsqueletico = null;
let registroTranstornoMusculoEsqueleticoOperativo = null;
let datosCEI = null;
let general = null;

//----------------------------------------------- VARIABLES GLOBALES Mujeres en edad fértil gestante --------------------------------------------
//###########################################################################################################

let MUJER_EDAD_FERTIL_GESTANTE = false;

//###########################################################################################################
//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO BANDEJA INTERCONSULTAS Y TRANSFERENCIAS  --------------------------------------------
//var paObj_hc = [];//objeto de interconsulta

var paObj_hi = []; //objeto de interconsulta ESTE ES TU OBJETO CARLOS
var paObj_ht = []; //objeto de trasnferencia
var istAud = 0;
var istAud2 = 0; //############################## BLOQUE CODIGO INTEGRACION ANDY 10-09-2021 #####################################
var HTMLEXCEL = "";
var HTMLEXCEL_INTERC = "";
var HTMLEXCEL_TRANSF = "";
var HTMLEXCEL2 = "";
var visible = 0; // 0 interconsulta 1 transferencia
var D_IT = []; //Almacena temporalmente el listado de Disgnostico de las interconsultas
var D_IT2 = []; //Almacena temporalmente el listado de Disgnostico de las transferencias
var D_IT3 = []; //Almacena temporalmente el listado de Signos Vitales de las transferencias
var OBJ_I = []; // Objeto inicial de la bandeja

var paObj_HC = []; //############################## BLOQUE CODIGO INTEGRACION ANDY 10-09-2021 #####################################

var intTransAM = 0; //cual de las bandejas esta seleccionada entre interconsulta o transferencia desde el formulario nuevo o editar de atenciones medicas
//por defecto interconsulta 0, 1 transferencia

//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO BANDEJA INTERCONSULTAS Y TRANSFERENCIA --------------------------------------------
//###########################################################################################################

//###########################################################################################################
//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO TRANSFERENCIA  --------------------------------------------
//var paObj_HC = [];//objeto para los datos de la hisoria clinica
var istAudT = 0;
var isNow = 1;
var FORMATO_EXPORTABLE;
var lt1 = [];
var lt2 = [];
var lt3 = [];
var IdSV = 0;
var sedeAreaGerencia;

//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO TRANSFERENCIA --------------------------------------------
//###########################################################################################################

//###########################################################################################################
//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO DE DESCANSOS MEDICOS  --------------------------------------------
//var paObj_HC = [];//objeto para los datos de la hisoria clinica

var paObj_DM_SHO = []; //objeto para los datos de LOS DESCANSOS MEDICOS
var paObj_DM_SAP = [];
var id_DM = 0; //LO CAMBIA DE DECLARADO A CERO OJO
var id_DM_SAP;
var newD = 0;
var ttemp = []; //mantiene los id del listado de descansos a vincular
var Vinculado = 0;
var ult_DMV = 0; //MATIENE EL Id del ultimo descndo seleccionado
var HTMLEXCEL_DESCANSO_SAP = [];
var HTMLEXCEL_DESCANSO_SHO = [];

//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO DESCANSOS MEDICOS --------------------------------------------
//###########################################################################################################

//###########################################################################################################
//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO DE ATENCIONES MEDICAS  --------------------------------------------
//var paObj_HC = [];//objeto para los datos de la hisoria clinica

var paObj_ATM_SHO = []; //objeto para los datos de LOS DESCANSOS MEDICOS
var paObj_ATM_SAP = [];
var id_ATM;
var id_ATM_SAP;
var HTMLEXCEL_AT = "";
var ult_ATMV = 0; //MATIENE EL Id del ultimo descndo seleccionado
var SAF = []; //SISTEMAS AFECTADOS
var tipoVerAtencion = 1;
var ObjSV_AM;

ObjSV_AM = new SignosVitales_AM();

//si se cancela descanso medico
var Cdmx = 0; // cero estado inicial, y uno es que si proviene de una cancelacion de descanso medico

//------------------------------------------- VARIABLES GLOBALES HISTORIA SUBMODULO ATENCIONES MEDICAS --------------------------------------------
//###########################################################################################################

//--------------------------VARIABLES GLOBALES ENFERMEDADES CRONICAS------------------------------//

var idEC = 0;
var paObj_ec = [];
var pluck_ec = [];
var paObj_detalle = [];
var paObj_detalleDescanso = [];
var paObj_detalleInterconsulta = [];
var paObj_detalleTransferencia = [];
var paObj_ec_hc = [];
var idDetalle = 0;
var controlNavHistoria = true;
var idTemporalEC = 0;
var idTemporalInterconsulta = 0;
var idTemporalTransferencia = 0;
var idTemporalDescanso = 0;
var paObj_ECTemporal = [];
var paObj_DescansoECTemporal = [];
var paObj_InterconsultaECTemporal = [];
var paObj_TransferenciaECTemporal = [];
var ingresoControl = false;
var campoControl = "";
var ControlLateralEnfermedadesCronicas = false;
var ControlDetalleOrigen = 0;

/*
  Para ControlDetalleOrigen
  
  0 No asignado
  1 Bandeja Registro de Enfermedades Crónicas
  2 Bandeja Gestion de Enfermedades Crónicas HC
  3 Bandeja Editar Enfermedades Crónicas HC
*/

//--------------------------VARIABLES GLOBALES ACCIDENTE TRABAJO ------------------------------//

var paObj_at = {};
var pluck_at = {};

var at_SedeAreaGerencia = {};
var accidenteTrabajoListHC = [];
var atDescansoMedicoListHC = [];
var atTransferenciasListHC = [];
var atSeguimientosListHC = [];
var atAltasListHC = [];
var atExamenesListHC = [];
var atPostAltaListHC = [];
var atInformesListHC = [];

var navHistoriaAT = true;
var navDescansoAT = false;
var navEvaluacionAT = false;
var navTransferenciaAT = false;
var dataAccidenteTrabajo = null;
var modeAccidenteTrabajo = "create"; // create, edit, show
var IdAT = 0;

var ControlLateralAccidenteTrabajo = false;

let at_meses = [
  { text: "ENE", ini: "01-01" },
  { text: "FEB", ini: "02-01" },
  { text: "MAR", ini: "03-01" },
  { text: "ABR", ini: "04-01" },
  { text: "MAY", ini: "05-01" },
  { text: "JUN", ini: "06-01" },
  { text: "JUL", ini: "07-01" },
  { text: "AGO", ini: "08-01" },
  { text: "SEP", ini: "09-01" },
  { text: "OCT", ini: "10-01" },
  { text: "NOV", ini: "11-01" },
  { text: "DIC", ini: "12-01" },
];
let at_selectedMes = null;
let at_current_page_mes = 1;
let at_per_page_mes = 5;

let at_years = [];
let at_selectedYear = null;
let at_current_page_year = 1;
let at_per_page_year = 5;

//--------------------------VARIABLES IPERC----------------------------//

var ControlLateralIPERC = false;

//--------------------------VARIABLES GLOBALES EMO------------------------------//

var idEMO = 0;
var idHCEMO = 0;
var paObj_EMO = [];
var pluck_EMO = [];
var paObj_detalle_EMO = [];
var idDetalleEMO = 0;
var idTemporalEMO = 0;
var idTemporalEMOFicha = 0;
var paObj_EMOTemporal = [];
var paObj_descargarAdjunto_EMO = [];
var paObj_descargarAdjunto_Ficha = [];
var ControlLateralEmo = false;
var fechas_ArrayEMO = {};
var fechas_ObjEMO = [];

let DATA_MUJER_FERTIL = null;

//############################################################# CONJUNTO E VARIBLES GLOBALES DE SALUD E HIGIENE ####################################################

var paObj_GES = [];
var pluck_GES = [];
var ControlNavHigiene = 0;
var idDetalleGES = 0;
var paObj_detalle_GES = [];
var paObj_Observaciones_EMO = [];
var idTemporalGES = 0;

var paObj_Monitoreo = [];
var pluck_Monitoreo = [];
var paObj_detalle_Monitoreo = [];
var idDetalleMonitoreo = 0;
var idTemporalMonitoreo = 0;

//#region Formularios
var bForm = false;
var bNewForm = false;
//#endregion

//#region Parametros para Evaluacion de Monitoreo
var IdMonitoreoGlobal = 0;
var IdEvaluacionGlobal = 0;
var IdTipoEvaluacionGlobal = 0;
var IdEstadoEvaluacionGlobal = 0;
var NombreFormulario = "";
var bModoEditar = true;
//#endregion

//#region Parametros para Seguimiento de Controles
var NombreIncidenteGlobal = "";
var IdControlGlobal = "";
var IdEstadoControlGlobal = 0;
var pluck_Seguimiento = [];
//#endregion

//#region Parametros para Proteccion Respiratoria y Auditiva
var IdProteccionGlobal = "";
var IdEstadoProteccionGlobal = 0;
var pluck_Proteccion = [];
var bProteccionRespAudMain = true;
var ControlLateralFitTest = false;
var NavProteccionRespAud = 1;
//#endregion

var paObj_Inspeccion = [];
var pluck_Inspeccion = [];
var paObj_detalle_Inspeccion = [];
var idDetalleInspeccion = 0;
var idTemporalInspeccion = 0;
var paObj_IndicenciasInspeccion = [];
var paObj_IndicenciasDetalles = [];

var paObj_Incidencia = [];
var pluck_Incidencia = [];

var paObj_HistoriaAtencionTemporal = [];
var pluck_HistoriaAtencionTemporal = [];
var idDetalleAtencionMedica = 0;
var idDetalleAtencionMedicaHC = 0;

var pluck_Ergovisor = [];
var idDetalleEvaluacionErgovisor = 0;
var idDetalleIncidenciaErgovisor = 0;
var IdHCErgo = 0;
var FechaDetalleIncidenciaErgovisor = "";
var EstadoDetalleErgovisor = 0;
var paObj_DetalleEvaluacionErgovisorTemporal = [];
var paObj_DetalleIncidenciaErgovisorTemporal = [];

var ControlNavHigieneTexto = "";

var paObj_Difusion = [];
var pluck_Difusion = [];
var ControlNavHigieneModal = false;
var paObj_DetalleControlTemporal = [];
var idTemporalDifusion = 0;
var paObj_DetalleDifusionTemporal = [];
var idDetalleDifusion = 0;

var paObj_Equipo = [];
var pluck_Equipo = [];
var idDetalleEquipo = 0;
var paObj_DetalleEquipoTemporal = [];
var paObj_DetalleEquipo = [];
var idTemporalEquipo = 0;

var paObj_Agente = [];
var paObj_DetalleAgente = [];

//############################################################# CONJUNTO E VARIBLES GLOBALES DE ENFERMEDADES OCUPACIONES ####################################################

/**
 * [idInter variable usada en varios archivos para el Id de la Interconsulta para nunca definida...]
 * @type {Number}
 */
let idInter = 0;

/**
 * [EnfermedadOcupacionalSeleted variable que almacena la enfermedad ocupacional actualmente seleccionada]
 * @type {Number}
 */
let EnfermedadOcupacionalSeleted = 0;

/**
 * [DiagnosticoId Id del diagnostico o enfermedad confirmada para las enfermedades ocupacionales]
 * @type {Number}
 */
let DiagnosticoId = 0;

/**
 * [ID_INTERCONSULTA_EO saber el valor del Id de la Interconsulta activa desde el módulo de EO]
 * @type {Number}
 */
var ID_INTERCONSULTA_EO = 0;

/**
 * [ID_TRANSFERENCIA_EO saber el valor del Id de la transferencia activa desde el modulo de EO]
 * @type {Number}
 */
var ID_TRANSFERENCIA_EO = 0;

/**
 * [ID_DESCANSO_MEDICO_EO description]
 * @type {Number}
 */
let ID_DESCANSO_MEDICO_EO = 0;

/**
 * [ACCION_INTERCONSULTA_EO BANDERA PARA SABER SI ESTAMOS CREANDO O MODIFICANDO UNA INTERCONSULTA DESDE EL MODULO DE
 * ENFERMEDADES OCUPACIONES]
 * @type {String}
 */
let ACCION_INTERCONSULTA_EO = "";

/**
 * [ACCION_TRANSFERENCIA_EO description]
 * @type {String}
 */
let ACCION_TRANSFERENCIA_EO = "";

/**
 * [ACCION_DESCANSOMEDICO_EO description]
 * @type {String}
 */
let ACCION_DESCANSOMEDICO_EO = "";

/**
 * [CODE_TRANSFERENCIA_EO CODIGO DE LA TRANSFERENCIA SELECCIONADA]
 * @type {String}
 */
let CODE_TRANSFERENCIA_EO = "";

/**
 * [CODE_INTERCONSULTA_EO CODIGO DE LA INTERCONSULTA SELECCIONADA]
 * @type {String}
 */
let CODE_INTERCONSULTA_EO = "";

/**
 * [NEW_SEGUIMIENTO_EO description]
 * @type {Number}
 */
let NEW_SEGUIMIENTO_EO = 0;

/**
 * [COUNT_SEGUIMIENTOS_EO description]
 * @type {Number}
 */
let COUNT_SEGUIMIENTOS_EO = 0;

//###########################################################################################################
//------------------------------------------- CLASES -----------------------------------------------------

function HistoriaClinica() {
  //-------------------------------------------Class HistoriaClinica()

  this.a = [];

  HistoriaClinica.prototype.cargarData = function (data) {
    this.a = data;
    this.a.Adjuntos = [];
    // // this.a.II;
    // this.a.II_BD = 0;//estado inicial, se puede ir al servidor a buscar la informacion.
  };
} //-------------------------------------------Class HistoriaClinica()

function HistoriaClinicaTranferencia() {
  this.a = [];

  HistoriaClinicaTranferencia.prototype.cargarData = function (data) {
    this.a = data;
    this.a.DiagnosticoCIE10 = [];
    this.a.SignosVitales = [];
    this.a.Adjuntos = [];
    // // this.a.II;

    this.a.BD = 0; //estado inicial, se puede ir al servidor a buscar la informacion.
  };
}

function Transferencia() {
  this.a = [];

  Transferencia.prototype.cargarData = function (data) {
    this.a = data;
    this.a.DiagnosticoCIE10 = [];
    this.a.SignosVitales = [];
    this.a.Adjuntos = [];
    // // this.a.II;

    this.a.BD = 0; //estado inicial, se puede ir al servidor a buscar la informacion.
  };
}

function Interconsulta() {
  this.a = [];

  Interconsulta.prototype.cargarData = function (data) {
    this.a = data;
    this.a.DiagnosticoCIE10 = [];
    this.a.SignosVitales = [];
    this.a.Adjuntos_Inter = [];
    // // this.a.II;

    // this.a.II_BD = 0;//estado inicial, se puede ir al servidor a buscar la informacion.
    this.a.Adjuntos = [];
    this.a.BD = 0; //estado inicial, se puede ir al servidor a buscar la informacion.
  };
}

function AtencionesMedicas() {
  this.a = [];

  AtencionesMedicas.prototype.cargarData = function (data) {
    this.a = data;
    this.a.DiagnosticoCIE10 = [];
    this.a.SignosVitales = [];
    this.a.Adjuntos = [];
    // // this.a.II;

    this.a.BD = 0; //estado inicial, se puede ir al servidor a buscar la informacion.
  };
}

function DescansoMedico() {
  this.a = [];

  DescansoMedico.prototype.cargarData = function (data) {
    this.a = data;
    this.a.DiagnosticoCIE10 = [];
    this.a.Adjuntos = [];
    // // this.a.II;

    this.a.BD = 0; //estado inicial, se puede ir al servidor a buscar la informacion.
  };
}

function EnfermedadesCronicas() {
  //-------------------------------------------Class EnfermedadesCronicas()

  this.a = [];

  EnfermedadesCronicas.prototype.cargarData = function (data) {
    this.a = data;
  };
} //-------------------------------------------Class EnfermedadesCronicas()

function SignosVitales_AM() {
  this.Id_AM = 0;
  this.Creando_AM = 0; //0 NO   1//SI

  SignosVitales_AM.prototype.cargarData = function () {
    this.pa = $("#dat_am_presion_arterial_sv").val();
    this.fc = $("#dat_am_frecuencia_cardiaca_sv").val();
    this.fr = $("#dat_am_frecuencia_respiratoria_sv").val();
    this.te = $("#dat_am_temperatura_sv").val();
    this.pe = $("#dat_am_peso_sv").val();
    this.ta = $("#dat_am_talla_sv").val();
    this.sa = $("#dat_am_saturacion_sv").val();
    this.imc = $("#dat_am_masa_corporal_sv").val();
    this.pa = $("#dat_am_perimetro_abdominal_sv").val();
  };

  SignosVitales_AM.prototype.MostrarDataTransferencia = function () {
    $("#dat_int_tran_sv1").val(this.pa);
    $("#dat_int_tran_sv2").val(this.fc);
    $("#dat_int_tran_sv3").val(this.fr);
    $("#dat_int_tran_sv4").val(this.te);
    $("#dat_int_tran_sv5").val(this.pe);
    $("#dat_int_tran_sv6").val(this.ta);
    $("#dat_int_tran_sv7").val(this.sa);
    $("#dat_int_tran_sv8").val(this.imc);
    $("#dat_int_tran_sv9").val(this.pa);
  };
}
//------------------------------------------- CLASES -----------------------------------------------------
//###########################################################################################################

function fnNuevaAtencionMedica() {
  //------------------------------------------------------ hay que ver si es interconsulta o trasnferencia y dependiendo llama al evento -------------------------------------------------------------------

  switch (visible) {
    case 0:
      //alert('Interconsulta');

      break;

    case 1:
      //alert('Transferencia = '+idHC);

      fnSp3VerEditarRegistroTransferencia(idHC, 0, 3);

      //(idHistoriaC, idTransferencia, accion

      break;
  }
} //------------------------------------------------------ hay que ver si es interconsulta o trasnferencia y dependiendo llama al evento -------------------------------------------------------------------

/****************************************************************************************
 * VISUAL SAT - [2021]
 * PROYECTO : SIGTASA
 * ESQUEMA : SSOMA
 * SPRINT  : 3
 * MODULO : HISTORIA CLINICA
 * OPERADORES_________________________________________________________________________
 * | # | PROGRAMADOR     |  |      FECHA   |  |   HORA   |           CORREO           |
 * | 2 | Andy Vàsquez    |  |   06/09/2021 |  | 11:16:00 |     caracas1348@gmail.com  |
 * |___|_________________|__|______________|__|__________|____________________________|
 *
 * DESCRIPCION: ARCHIVO DE FUNCIONES GLOBALES O COMUNES DEL MODULO SALUD
 *
 * ARCHIVOS DE SERVICIOS   _________________________________________
 * | # |     MODULO             |  |         NOMBRE                 |
 * | 1 |   SALUD OCUPACIONAL    |  |                                |
 * |________________________________________________________________|
 *
 *
 * VERSION: 0.1 Beta
 *********************************************************************************************************************************************************************************
 * NOTA IMPORTANTE:  EN FECHA  *** 06/09/2021 Andy  **** Vasquez: Procede a realizar un ajuste general  de funcionamiento y orden del modulo de historia clinica para optimizarlo
 *********************************************************************************************************************************************************************************

 *******************************************************************************************/

// //-----------------incorporar en el evento onkeyup="soloNumeros('idtxt')"
//   jQuery("#dat_hc_it_a_doc").on('input', function (evt)
//   {
//         // Allow only numbers.
//         jQuery(this).val(jQuery(this).val().replace(/[^0-9]/g, ''));
//   });

//------------------------------------------------------------------------------ invierte la fecha que viene de la base de datos-----------------------------

function date_AAAA_MM_DD_T_HH_MM_S_to_DD_MM_AAAA(fechaBD) {
  var startDate = moment(fechaBD).format("DD/MM/YYYY"); //dddd
  var year = moment(fechaBD).format("YYYY"); //dddd
  var month = moment(fechaBD).format("MM"); //
  var day = moment(fechaBD).format("DD");
  //var startDate2   = year +"/"+ month +"/"+ day;
  var startDate2 = day + "/" + month + "/" + year;

  return startDate2;
}

function imc(idPeso, IdTalla, IdIMC) {
  var p = $("#" + idPeso).val();
  var t = $("#" + IdTalla).val();

  var div = t * t;
  var imc = p / div;

  $("#" + IdIMC).val(imc);
}

//----------------------------------------------------------- BUSQUEDA POR DESCRIPCION CIE10 ----------------------------------------------------
function _newDiagnosticoCIE10(IdTxt1, contenedor, listaCIE10, listaCIE10_2) {
  if ($("#" + IdTxt1).val().length > 2) {
    //------------------------------------vamos a buscar el servicio --------------------------------------

    let ptr = $("#" + IdTxt1).val();

    let url =
      apiUrlsho +
      `/api/hce_Get_021_CIE10_busqueda?code=Kr7q88AoJqtcFZLAx3w8cS7kZ8ezNaxCr/YUbbfUMvEQH1zUvDsxjg==&IdCIE10=&Descripcion=` +
      ptr;

    var html = "";
    let headers = {
      apikey: constantes.apiKey,
      "Content-Type": "application/json",
    };

    let settings = {
      url: url,
      method: "get",
      timeout: 0,
      crossDomain: true,
      dataType: "json",
      headers: headers,
    };

    $.ajax(settings).done((response) => {
      CIE_10 = response.CEI10;
      if (response.CEI10.length > 0) {
        var i = 1;
        response.CEI10.map(function (Item) {
          if (i < response.CEI10.length) {
            html =
              html +
              ` <a class = 'dropdown-item itemBD'  onclick="  $('#${IdTxt1}').val('${Item.Descripcion}');  $('#${IdTxt1}').prop('title','${Item.Descripcion}');  agregaCIE10('${listaCIE10}', '${listaCIE10_2}', '${Item.Code}','${Item.Id}');  $('#${contenedor}').css('display','none');  "  >${Item.Descripcion}</a>`;
          }
          if (i == response.CEI10.length) {
            html =
              html +
              ` <a class = 'dropdown-item itemBD' style="border-bottom: 1px solid;"   onclick="  $('#${IdTxt1}').val('${Item.Descripcion}');  $('#${IdTxt1}').prop('title','${Item.Descripcion}');  agregaCIE10('${listaCIE10}','${Item.Code}','${Item.Id}');  $('#${contenedor}').css('display','none');  "  >${Item.Descripcion}</a>`;
          }
          // {  html =  html + ` <a class = 'dropdown-item itemBD'  >${Item.Descripcion}</a>`; }

          i++;
        });

        $("#" + contenedor).html(html);
        $("#" + contenedor).css("display", "block");
        $("#" + contenedor).css("z-index", 3000);
      } else {
        $("#" + contenedor).html(" ");
        $("#" + contenedor).css("display", "none");
      }
    });
  } else {
    $("#" + contenedor).html(" ");
    $("#" + contenedor).css("display", "none");
  }
}

function agregaCIE10(id, id2, d_cie10, id_Cie10) {
  $("#" + id2).val(id_Cie10);
  $("#" + id).val(d_cie10);
}
//----------------------------------------------------------- BUSQUEDA POR DESCRIPCION CIE10 ----------------------------------------------------

//----------------------------------------------------------- BUSQUEDA POR CODIGO CIE10 ----------------------------------------------------

function _newDiagnosticoCIE102(IdTxt1, contenedor, listaCIE10, listaCIE10_2) {
  if ($("#" + IdTxt1).val().length > 1) {
    //------------------------------------vamos a buscar el servicio --------------------------------------

    let ptr = $("#" + IdTxt1).val();

    let url =
      apiUrlsho +
      `/api/hce_Get_021_CIE10_busqueda_2?code=du4cYFZ7WFprYBKU0hCISCgM6dy/AnJMzLrfjJtTa0eAZqGFaBwd/w==&IdCIE10&Descripcion=` +
      ptr;

    var html = "";
    let headers = {
      apikey: constantes.apiKey,
      "Content-Type": "application/json",
    };

    let settings = {
      url: url,
      method: "get",
      timeout: 0,
      crossDomain: true,
      dataType: "json",
      headers: headers,
    };

    $.ajax(settings).done((response) => {
      if (response.CEI10.length > 0) {
        var i = 1;
        response.CEI10.map(function (Item) {
          if (i < response.CEI10.length) {
            html =
              html +
              ` <a class = 'dropdown-item itemBD'  onclick="  $('#${listaCIE10}').val('${Item.Descripcion}');  $('#${IdTxt1}').prop('title','${Item.Descripcion}');  agregaCIE10_2('${listaCIE10_2}', '${IdTxt1}', '${Item.Code}','${Item.Id}');  $('#${contenedor}').css('display','none');  "  >[ ${Item.Code} ]-- ${Item.Descripcion}</a>`;
          }
          if (i == response.CEI10.length) {
            html =
              html +
              ` <a class = 'dropdown-item itemBD' style="border-bottom: 1px solid;"  onclick="  $('#${listaCIE10}').val('${Item.Descripcion}');  $('#${IdTxt1}').prop('title','${Item.Descripcion}');  agregaCIE10_2('${listaCIE10_2}', '${IdTxt1}', '${Item.Code}','${Item.Id}');  $('#${contenedor}').css('display','none');  "  >[ ${Item.Code} ]-- ${Item.Descripcion}</a>`;
          }

          i++;
        });

        $("#" + contenedor).html(html);
        $("#" + contenedor).css("display", "block");
        $("#" + contenedor).css("z-index", 10001);
      } else {
        $("#" + contenedor).html(" ");
        $("#" + contenedor).css("display", "none");
      }
    });
  } else {
    $("#" + contenedor).html(" ");
    $("#" + contenedor).css("display", "none");
  }
}

function agregaCIE10_2(id, id2, d_cie10, id_Cie10) {
  $("#" + id2).val(d_cie10);
  $("#" + id).val(id_Cie10);
}
//----------------------------------------------------------- BUSQUEDA POR CODIGO CIE10 ----------------------------------------------------

function maximoZindex(from) {
  var max = 0;
  from.find(">*").each(function (i, e) {
    var z = Number($(e).css("z-index"));
    if (z > max) {
      max = z;
    }
  });
  return max;
}

/**
 * BUSCAR INFORMACION Y LISTADOS BASE DEL SISTEMA
 */
let vw_main = (function () {
  /**
   * [Empresas LISTADO CON LAS EMPRESAS REGISTRADAS EN EL SISTEMA]
   * @type {Array}
   */
  let Empresas = [];

  /**
   * [PuestosTrabajo LISTADO CON LOS PUESTOS DE TRABAJO REGISTRADOS EN EL SISTEMA]
   * @type {Array}
   */
  let PuestosTrabajo = [];

  /**
   * [SistemasAfectados LISTADO DE SISTEMAS AFECTADOS]
   * @type {Array}
   */
  let SistemasAfectados = [];

  /**
   * [SeccionesAfectadas LISTADO DE SISTEMAS AFECTADOS]
   * @type {Array}
   */
  let SeccionesAfectadas = [];

  /**
   * [TiposExamenes LISTADO DE SISTEMAS AFECTADOS]
   * @type {Array}
   */
  let TiposExamenes = [];

  /**
   * [Coberturas LISTADO DE SISTEMAS AFECTADOS]
   * @type {Array}
   */
  let Coberturas = [];

  /**
   * [getDataBase description]
   * @return {[type]} [description]
   */
  let getDataBase = function () {
    let url = `${apiUrlsho}${GetDataBaseAll}&httpmethod=objectlist`;

    let headers = {
      apikey: constantes.apiKey,
      "Content-Type": "application/json",
    };

    let settings = {
      url: url,
      method: "GET",
      timeout: 0,
      crossDomain: true,
      dataType: "json",
      headers: headers,
    };

    return $.ajax(settings)
      .done(function (response) {
        PuestosTrabajo = response.PuestosTrabajo;
        Empresas = response.Empresas;
        SistemasAfectados = response.SistemasAfectados;
        SeccionesAfectadas = response.SeccionesAfectadas;
        TiposExamenes = response.TiposExamenes;
        Coberturas = response.Coberturas;
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        console.warn(".fail");
      })
      .always(function (jqXHR, textStatus, errorThrown) {
        // console.warn(".always")
      }); //*/
  };

  /**
   * [getPuestosTrabajo DEVOLVEMOS UN ARRAY CON LOS PUESTOS DE TRABAJO...]
   * @param  {[type]} Id [Id del puesto de trabajo a retornar si es cero (0) retorno todo el array]
   * @return {[type]}    [description]
   */
  let getPuestosTrabajo = function (Id) {
    if (Id == 0) return PuestosTrabajo;
    else return PuestosTrabajo.find((x) => x.Id == Id);
  };

  /**
   * [llenarSelectPuestosTrabajo AGREGAR DATOS AL SELECT DE PUESTOS DE TRABAJO]
   * @param  {[type]} idSelect [id del select de puestos de trabajos]
   * @return {[type]}          [description]
   */
  let llenarSelectPuestosTrabajo = function (idSelect) {
    let contenido = $(`#${idSelect}`);
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    PuestosTrabajo.forEach((Item) => {
      ControlOptionSelect = false;

      $(`#${idSelect} option`).each(function () {
        if ($.trim($(this).text()) == $.trim(Item.PuestoTrabajo)) {
          ControlOptionSelect = true;
        }
      });

      if (!ControlOptionSelect) {
        contenido.append(
          `<option value="${Item.Id}">${Item.PuestoTrabajo}</option>`
        );
      }
    });
  };

  /**
   * @return {[type]} [description]
   */
  /**
   * [getEmpresas RETORNAMOS EL ARRAY CON LAS EMPRESAS]
   * @param  {[type]} Id [Id de la empresa a buscar si es 0 devuelve todas]
   * @return {[Array]}    [con la empresa a buscar o todas...]
   */
  let getEmpresas = function (Id) {
    if (Id == 0) return Empresas;
    else return Empresas.find((x) => x.Id == Id);
  };

  /**
   * [llenarSelectEmpresas AGREGAMOS LAS EMPRESAS AL SELECT]
   * @param  {[type]} idSelect [description]
   * @return {[type]}          [description]
   */
  let llenarSelectEmpresas = function (idSelect) {
    let contenido = $(`#${idSelect}`);
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    Empresas.forEach((Item) => {
      ControlOptionSelect = false;

      $(`#${idSelect} option`).each(function () {
        if ($.trim($(this).text()) == $.trim(Item.Name)) {
          ControlOptionSelect = true;
        }
      });

      if (!ControlOptionSelect) {
        contenido.append(`<option value="${Item.Id}">${Item.Name}</option>`);
      }
    });
  };

  /**
   * [getSistemasAfectados RETORNAR EL ARRAY DE LOS SISTEMAS AFECTADOS O UNO ESPECIFICO]
   * @param  {[type]} Id [Id DEL SISTEMA A RETORNAR, SI ES 0 SE DEVUELVEN TODOS]
   * @return {[type]}    [description]
   */
  let getSistemasAfectados = function (Id) {
    if (Id == 0) return SistemasAfectados;
    else return SistemasAfectados.find((x) => x.Id == Id);
  };

  /**
   * [getSeccionesAfectadas RETORNAR EL ARRAY DE LAS SECCIONES AFECTADAS O UNO ESPECIFICO]
   * @param  {[type]} Id [Id DE LA SECCION A RETORNAR, SI ES 0 SE DEVUELVEN TODOS]
   * @return {[type]}    [description]
   */
  let getSeccionesAfectadas = function (Id) {
    if (Id == 0) return SeccionesAfectadas;
    else return SeccionesAfectadas.find((x) => x.Id == Id);
  };

  /**
   * [getTiposExamenes RETORNAR EL ARRAY DE LOS TIPOS DE EXAMENES O UNO ESPECIFICO]
   * @param  {[type]} Id [Id DEL TIPO DE EXAMEN A RETORNAR, SI ES 0 SE DEVUELVEN TODOS]
   * @return {[type]}    [description]
   */
  let getTiposExamenes = function (Id) {
    if (Id == 0) return TiposExamenes;
    else return TiposExamenes.find((x) => x.Id == Id);
  };

  /**
   * [getCoberturas RETORNAR EL ARRAY DE LOS TIPOS DE COBERTURAS O UNO ESPECIFICO]
   * @param  {[type]} Id [Id DE LA COBERTURA A RETORNAR, SI ES 0 SE DEVUELVEN TODOS]
   * @return {[type]}    [description]
   */
  let getCoberturas = function (Id) {
    if (Id == 0) return Coberturas;
    else return Coberturas.find((x) => x.Id == Id);
  };

  return {
    init: function () {
      getDataBase();
    },

    getPuestosTrabajo(Id) {
      return getPuestosTrabajo(Id);
    },

    llenarSelectPuestosTrabajo(idSelect) {
      llenarSelectPuestosTrabajo(idSelect);
    },

    getEmpresas(Id) {
      return getEmpresas(Id);
    },

    llenarSelectEmpresas(idSelect) {
      llenarSelectEmpresas(idSelect);
    },

    getSistemasAfectados(Id) {
      return getSistemasAfectados(Id);
    },

    getSeccionesAfectadas(Id) {
      return getSeccionesAfectadas(Id);
    },

    getTiposExamenes(Id) {
      return getTiposExamenes(Id);
    },

    getCoberturas(Id) {
      return getCoberturas(Id);
    },
  };
})();

/**
 * @author Edward Romero
 * variables globales necesarias
 */
window.paginaBack = {};
window.paginaBack.enfermedadOcupacional = false;

function atSp4VerDatosCompletoTrabajador() {
  let datos = paObj_hc[idHC].a;

  datos.Sede = sedeAreaGerencia.Sedes.find(
    (item) => item.Id == datos.PlantaId_Empresa_H
  );
  datos.Area = sedeAreaGerencia.Area.find(
    (item) => item.Id == datos.AreaId_Empresa_H
  );

  Swal.fire({
    title: "Datos del trabajador",
    html: `
    <div class="text-left">
      <div class="row my-3">
        <div class="col-12">
          <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos Principales</span>
        </div>
      </div>
      <div class="row my-3" style="font-size: 15px">
        <div class="col-4">
          <span style="color: #254373"><b>Documento: </b></span>
          <span>${datos.NroDocumento_Trabajador_H}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Nombres: </b></span>
          <span>${datos.Nombres_Trabajador_H}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Apellidos: </b></span>
          <span>${datos.Apellidos_Trabajador_H}</span>
        </div>
      </div>
      <div class="row my-3" style="font-size: 15px">
        <div class="col-4">
          <span style="color: #254373"><b>C.Colaborador: </b></span>
          <span>${datos.CodigoColaborador_Trabajador_H}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Telefono: </b></span>
          <span>${datos.Telefono_Trabajador_H}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Sexo: </b></span>
          <span>${
            datos.Sexo_Trabajador_H == 1 ? "Masculino" : "Femenino"
          }</span>
        </div>
      </div>
      <div class="row my-3" style="font-size: 15px">
        <div class="col-12">
          <span style="color: #254373"><b>Dirección: </b></span>
          <span>${datos.Direccion_Trabajador_H}</span>
        </div>
      </div>
      <div class="row my-3" style="font-size: 15px">
        <div class="col-4">
          <span style="color: #254373"><b>F.Nacimiento: </b></span>
          <span>${datos.A_FechaNacimiento.slice(0, -9)
            .split("-")
            .reverse()
            .join("-")}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Edad: </b></span>
          <span>${datos.Edad_Trabajador_H}</span>
        </div>
      </div>
      <hr class="my-4">
      <div class="row my-3">
        <div class="col-12">
          <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos de la empresa</span>
        </div>
      </div>
      <div class="row my-3" style="font-size: 15px">
        <div class="col-4">
          <span style="color: #254373"><b>Sede: </b></span>
          <span>${datos.Sede.Description}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Área: </b></span>
          <span>${datos.Area.Description}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Cargo: </b></span>
          <span>${datos.CargoJefe_Empresa_H}</span>
        </div>
      </div>
      <div class="row my-3" style="font-size: 15px">
        <div class="col-4">
          <span style="color: #254373"><b>Jefe inmediato: </b></span>
          <span>${datos.JefeInmediato_Empresa_H}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Celular: </b></span>
          <span>${datos.Celular_Empresa_H}</span>
        </div>
        <div class="col-4">
          <span style="color: #254373"><b>Telefono: </b></span>
          <span>${datos.Telefono_Empresa_H}</span>
        </div>
      </div>
    </div>            
  `,
    iconHtml: '<img src="./images/sho/perfil.svg">',
    width: 800,
    showCancelButton: true,
    showConfirmButton: false,
    cancelButtonColor: "#ff3636",
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  });
  $(".swal2-cancel").css("width", "200px");
  $(".swal2-html-container").css("overflow", "visible");
}

/////////////////////////////////////// AJUSTES DE IMPACTO GENERAL //////////////////////////////////////////////

var BackSeccion = 0;

function onAtrasHistoriaClick(Seccion) {
  BackSeccion = Seccion;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var BackSeccionText = '';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var paObj_PlucksGenerales = [];

function buscarSedesAreasGerenciasGeneral() {
  //Se especifican los parametros para la consulta
  var url =
    apiUrlssoma +
    "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0";

  var headers = {
    apikey: constantes.apiKey,
  };

  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers,
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done(function (response) {
    paObj_PlucksGenerales["Areas"] = response.Area;
    paObj_PlucksGenerales["Gerencias"] = response.Gerencia;
    paObj_PlucksGenerales["Sedes"] = response.Sedes;
  });
}

var paObj_HistoriaGeneral = [];

function buscarDatosGeneralesHistoria(IdHistoriaMedica) {
  //Se especifican los parametros para la consulta
  let url =
    apiUrlsho +
    `/api/hce_Get_600_historia_clinica_datos_generales_completo?code=x65sNwDvhbkdVQk0BvFAlVp7TVvemyvLlzYet9HXKgv5H2Fb4xXaUg==`;

  let headers = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json",
  };

  let settings = {
    url: url,
    method: "get",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers,
    data: { IdHC: IdHistoriaMedica },
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done((response) => {
    paObj_HistoriaGeneral[IdHistoriaMedica] = response.HistoriaClin[0];
    console.log(
      "paObj_HistoriaGeneral[IdHistoriaMedica]----------->",
      paObj_HistoriaGeneral[IdHistoriaMedica]
    );
  });
}

function cargarDatosGeneralesHistoria(IdHistoriaMedica) {
  if (paObj_HistoriaGeneral[IdHistoriaMedica].FotoPacienteBase64.length) {
    $("img[name='img_file_perfil']").attr(
      "src",
      paObj_HistoriaGeneral[IdHistoriaMedica].FotoPacienteBase64
    );
  } else {
    $("img[name='img_file_perfil']").attr("src", "images/sho/profile.png");
  }

  if (paObj_HistoriaGeneral[IdHistoriaMedica].FechaRetiroTasa_H === undefined) {
    $("#sp4EnferCron_dat_trab_fr_tasa").val("Sin fecha de registro");
  } else {
    $("#sp4EnferCron_dat_trab_fr_tasa").val(
      new Date(paObj_HistoriaGeneral[IdHistoriaMedica].FechaRetiroTasa_H)
        .toLocaleDateString("en-GB")
        .split("T")[0]
    );
  }

  $("span[name=dat_nombres]").text(
    paObj_HistoriaGeneral[IdHistoriaMedica].Nombres_Trabajador_H +
      " " +
      paObj_HistoriaGeneral[IdHistoriaMedica].Apellidos_Trabajador_H
  );

  $("#dni_trabajador").text(
    paObj_HistoriaGeneral[IdHistoriaMedica].NroDocumento_Trabajador_H
  );
  $("#puesto_trabajador").text(
    paObj_HistoriaGeneral[IdHistoriaMedica].PuestoTrabajo_Empresa_H
  );
  $("#planta_trabajador").text(
    paObj_PlucksGenerales["Sedes"].find(
      (item) =>
        item.Id == paObj_HistoriaGeneral[IdHistoriaMedica].SedeId_Empresa_H
    ).Description
  );
  $("#gerencia_trabajador").text(
    paObj_PlucksGenerales["Gerencias"].find(
      (item) =>
        item.Id == paObj_HistoriaGeneral[IdHistoriaMedica].GerenciaId_Empresa_H
    ).Description
  );
  $("#area_trabajador").text(
    paObj_PlucksGenerales["Areas"].find(
      (item) =>
        item.Id == paObj_HistoriaGeneral[IdHistoriaMedica].AreaId_Empresa_H
    ).Description
  );
  $("#edad_trabajador").text(
    paObj_HistoriaGeneral[IdHistoriaMedica].Edad_Trabajador_H
  );
  $("#sexo_trabajador").text(
    paObj_HistoriaGeneral[IdHistoriaMedica].Sexo_Trabajador_H == 1
      ? "Masculino"
      : "Femenino"
  );
}

function VerDatosCompletoTrabajador(IdHistoriaMedica) {
  //Se carga el modal con los datos completos del trabajador
  let datos = paObj_HistoriaGeneral[IdHistoriaMedica];

  Swal.fire({
    title: "Datos del trabajador",
    html: `
      <div class="text-left">
        <div class="row my-3">
          <div class="col-12">
            <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos Principales</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Documento: </b></span>
            <span>${datos.NroDocumento_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Nombres: </b></span>
            <span>${datos.Nombres_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Apellidos: </b></span>
            <span>${datos.Apellidos_Trabajador_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>C.Colaborador: </b></span>
            <span>${datos.CodigoColaborador_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Telefono: </b></span>
            <span>${datos.Telefono_Trabajador_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Sexo: </b></span>
            <span>${
              datos.Sexo_Trabajador_H == 1 ? "Masculino" : "Femenino"
            }</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-12">
            <span style="color: #254373"><b>Dirección: </b></span>
            <span>${datos.Direccion_Trabajador_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>F.Nacimiento: </b></span>
            <span>${datos.A_FechaNacimiento.slice(0, -9).split("-").reverse().join("-")}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Edad: </b></span>
            <span>${datos.Edad_Trabajador_H}</span>
          </div>
        </div>
        <hr class="my-4">
        <div class="row my-3">
          <div class="col-12">
            <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos de la empresa</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Sede: </b></span>
            <span>${
              paObj_PlucksGenerales["Sedes"].find(
                (item) => item.Id == datos.SedeId_Empresa_H
              ).Description
            }</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Área: </b></span>
            <span>${
              paObj_PlucksGenerales["Areas"].find(
                (item) => item.Id == datos.AreaId_Empresa_H
              ).Description
            }</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Cargo: </b></span>
            <span>${datos.CargoJefe_Empresa_H}</span>
          </div>
        </div>
        <div class="row my-3" style="font-size: 15px">
          <div class="col-4">
            <span style="color: #254373"><b>Jefe inmediato: </b></span>
            <span>${datos.JefeInmediato_Empresa_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Celular: </b></span>
            <span>${datos.Celular_Empresa_H}</span>
          </div>
          <div class="col-4">
            <span style="color: #254373"><b>Telefono: </b></span>
            <span>${datos.Telefono_Empresa_H}</span>
          </div>
        </div>
      </div>            
    `,
    iconHtml: '<img src="./images/sho/perfil.svg">',
    width: 800,
    showCancelButton: true,
    showConfirmButton: false,
    cancelButtonColor: "#ff3636",
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  });
  $(".swal2-cancel").css("width", "200px");
  $(".swal2-html-container").css("overflow", "visible");
}

async function _init_DatosGeneralesHistoria(IdHistoriaMedica) {
  //Se inicia el Cargando
  showLoading();

  await buscarSedesAreasGerenciasGeneral();
  await buscarDatosGeneralesHistoria(IdHistoriaMedica);
  await cargarDatosGeneralesHistoria(IdHistoriaMedica);

  //Se apaga el Cargando
  hideLoading();
}

function onClickMostrarDatosGeneral() {
  $("#MostrarDatos").hide();
  $("#OcultarDatos").show();
  $(".show-less").hide();
  $(".show-more").show();
}

function onClickOcultarDatosGeneral() {
  $("#OcultarDatos").hide();
  $("#MostrarDatos").show();
  $(".show-less").show();
  $(".show-more").hide();
}
//variable global
window.CEI10 = [];

function loadCIEdataGlobal() {
  let url =
    apiUrlsho +
    `/api/hce_Get_021_CIE10_busqueda?code=Kr7q88AoJqtcFZLAx3w8cS7kZ8ezNaxCr/YUbbfUMvEQH1zUvDsxjg==&IdCIE10=&Descripcion=`;

  let myHeaders = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  let settings = {
    url: url,
    method: "get",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: myHeaders
  };
  fetch(url, settings)
    .then(res => res.json())
    .catch(error => console.error("Error:", error))
    .then(response => {
      console.log(response);
      window.CEI10 = response.CEI10;
    });
}

loadCIEdataGlobal();
