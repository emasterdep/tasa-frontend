//#region Variables
var ListaControl;
var IdControl;
var IdEstadoControl;
var NombreEstadoNuevo;
var ArchivoPDF = '';
var paObj_descargarAdjunto_Control = [];
//#endregion


//#region Inicializar
$(document).ready(function () {

    fnSetVariablesGlobales();
    $('#divArchivos').hide();
    onInit();

});
//#endregion


//#region Inicio
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnGetSedesAreasGerencias(),
        fnGetPlucksSeguimiento(),
        fnGetSelectsForm()
    ]);

    fnSetSelectSeguimiento();
    fnGetDataControl();

    document.getElementById('fieldset1').disabled = true;
    if (!bModoEditar || IdEstadoControl == 1) {
        document.getElementById('fieldset2').disabled = true;
        document.getElementById('fieldset3').disabled = true;
    }

    fnOcultarMostrarBotones();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Ocultar Mostrar Botones
async function fnOcultarMostrarBotones() {
    $('#divBtnInvisible').hide();
    $('#divBtnInvisible_2').hide();

    if (IdEstadoControl == 1) {
        $('#divBtnActivar').show();
        $('#divBtnGuardar').hide();
    }
    else {
        $('#divBtnActivar').hide();

        $('#divBtnGuardar').show();
    }

    //Solo leer
    if (!bModoEditar || IdEstadoControl == 1 || IdEstadoControl == 2 || IdEstadoControl == 3) {
        $('#divBtnCancelar').hide();
        $('#divBtnCerrar').hide();
        $('#divBtnAnular').hide();
        $('#divBtnGuardar').hide();
        $('#divBtnActivar').hide();
    }

    //Abierto o Anulado
    if (bModoEditar && (IdEstadoControl == 1 || IdEstadoControl == 3)) {
        $('#divBtnInvisible').show();
        $('#divBtnActivar').show();
    }
    if (IdEstadoControl == 4) {
        $('#divBtnInvisible_2').show();
    }


}
//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Asignar Variables Globales
async function fnSetVariablesGlobales() {
    //Variables Anteriores    
    IdControl = IdControlGlobal
    IdEstadoControl = IdEstadoControlGlobal;
}
//#endregion


//#region Funcionalidad Boton Cancelar
function fnVolverIndexSeguimiento() {
    handlerUrlhtml('contentGlobal', 'view/sho-hce/controlar/datosGenerales.html', 'Controlar');
}
//#endregion


//#region GET Cargar Datos de Seguimientro de Control
function fnGetDataControl() {
    let url = apiUrlsho + `/api/ho_get_seguimiento_controles?code=z3M/GEgWlcW42lvBNU9Yq1Gw9gZIGqYyN070D1gHrGbgxZrBbJLE2A==&AccionBackEnd=ListaDatosControl&IdControl=` + IdControl;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaControl = response.lista_DatosControl

        fnSetDatosControl(ListaControl);

    });
}
//#endregion


//#region SET Asignar Datos de Control
function fnSetDatosControl(lista) {

    $('#sCodigoControl').val(lista[0].CodigoControl);
    $('#sCodigoReunion').val(lista[0].CodigoReunion);

    $("#dat_origen option[value='" + lista[0].IdOrigenIncidencia + "']").prop("selected", true);
    $("#dat_id_incidencia option[value='" + lista[0].IdIncidencia + "']").prop("selected", true);
    $("#dat_estado option[value='" + lista[0].IdEstado + "']").prop("selected", true);

    $("#dat_control_implementar option[value='" + lista[0].IdControlImplementar + "']").prop("selected", true);

    $('#sLugarActividad').val(lista[0].LugarActividad);

    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $('#sResponsable').val(lista[0].Responsable);

    $('#sDescripcionControl').val(lista[0].DescripcionControl);
    $('#sComentario').val(lista[0].Comentario);

    if (lista[0].FechaRegistro != "") { $('#dFechaRegistro').val(lista[0].FechaRegistro); }
    if (lista[0].FechaProgramacion != "") { $('#dFechaProgramacion').val(lista[0].FechaProgramacion); }
    if (lista[0].FechaRegistroIncidencia != "") { $('#dFechaRegIncidencia').val(lista[0].FechaRegistroIncidencia); }
    if (lista[0].FechaPropuesta != "") { $('#dFechaPropuesta').val(lista[0].FechaPropuesta); }
    if (lista[0].FechaAcuerdo != "") { $('#dFechaAcuerdo').val(lista[0].FechaAcuerdo); }
    if (lista[0].FechaRegistroIncidencia != "") { $('#dFechaRegistroIncidencia').val(lista[0].FechaRegistroIncidencia); }
    if (lista[0].FechaCierre != "") { $('#dFechaCierre').val(lista[0].FechaCierre); }

    fnGetTableAdjuntoControl(IdControl);

}
//#endregion


//#region Funcionalidad CRUD
function fnCRUDForm(EstadoNuevo) {

    switch (EstadoNuevo) {
        case 2:
            NombreEstadoNuevo = 'Cerrar'
            if ($("#dFechaCierre").val() == "") {
                return Swal.fire({
                    icon: 'warning',
                    title: 'Advertencia',
                    text: 'Por favor elija una Fecha de cierre.',
                });
            }
            break;
        case 3:
            NombreEstadoNuevo = 'Anular'

            break;
        case 4:
            NombreEstadoNuevo = 'Guardar'

            break;

        default:
            break;
    }

    /* if (fnValidationForm().length > 0) {
        return Swal.fire({
            icon: 'warning',
            title: 'Advertencia',
            text: 'Por favor complete todos los campos',
        });
    } */

    Swal.fire({
        title: `${NombreEstadoNuevo} control`,
        html: `
      <p>Está por ${NombreEstadoNuevo} el control </p>
      <p class="mt-5">¿Desea confirmar la acción?</p>`,
        icon: "info",
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonColor: "#ff3636",
        confirmButtonColor: "#8fbb02",
        confirmButtonText: `Confirmar <img class="inject-svg" src="./images/sho/confirm.svg" alt="" fill="#fff" width="18px">`,
        cancelButtonText: `Cancelar <img class="inject-svg" src="./images/sho/cancelar.svg" alt="" fill="#fff" width="18px">`,
    }).then((result) => {

        if (result.isConfirmed) {
            fnPostFormulario(EstadoNuevo);
        }
    });
}
//#endregion


//#region POST: Programar | Guardar | Cerrar | Anular
function fnPostFormulario(IdEstado) {

    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    //IdEstado => 2:Cerrar | 3: Anular | 4: En progreso 

    data.IdControl = IdControl;
    data.Estado = IdEstado;
    data.LugarActividad = $("#sLugarActividad").val();
    data.DescripcionControl = $("#sDescripcionControl").val();
    data.Comentario = $("#sComentario").val();
    data.FechaCierre = $("#dFechaCierre").val() == "" ? "1900-01-01" : $("#dFechaCierre").val()

    showLoading();

    let url = apiUrlsho + `/api/ho_Post_seguimiento_controles?code=p38w8l/Bor2xXASgH6KOhAadoYaMx6T3G5sHd7sHTpJubhovJQIoNw==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        hideLoading();

        if (response.Id > 0) {

            if (IdEstado == 4) {
                EjecutarModal = true;
                TituloModal = 'Se guardó el control';
            }

            if (IdEstado == 2) {
                EjecutarModal = true;
                TituloModal = 'Se cerró el control';
            }

            if (IdEstado == 3) {
                EjecutarModal = true;
                TituloModal = 'Se anuló el control';
            }

            if (EjecutarModal) {

                Swal.fire({
                    title: TituloModal,
                    iconColor: "#8fbb02",
                    iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                    showConfirmButton: false,
                    padding: "3em 3em 6em 3em ",
                    timer: 1500,
                }).then(() => {

                    fnVolverIndexSeguimiento();

                });

            }
            else {
                Swal.fire({
                    title: 'No se pudo guardar',
                    icon: 'warning',
                    timer: 3500,
                })
            }


        }

    });

}
//#endregion


//#region Capturar Archivo Adjunto
function fnSetFileAdj(element, IdControl) {

    var file = element.files[0];
    var reader = new FileReader();

    reader.onloadend = function () {

        let data = {};

        data.ControlId = IdControl;
        data.accion = 0;
        data.NombreArchivo = element.files[0].name;
        data.ArchivoBase64 = reader.result;
        data.CreadoPor = "IdHashUser";
        data.ModificadoPor = "IdHashUser";
        data.TipoAdjunto_1_2 = 1;

        fnPostAdjuntoControl(data, IdControl);

    }

    reader.readAsDataURL(file);

}
//#endregion


//#region POST Adjuntar Archivo
function fnPostAdjuntoControl(Data, IdControl) {

    let TituloModal = '';

    let url = apiUrlsho + `/api/ho_Post_seguimiento_controles_adjunto?code=X41MwVla/x0/aOG5M8kMFVnVxKuA3Co8LyQoXikaBa7GnVfWmFmqLg==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(Data)
    };

    return $.ajax(settings).done((response) => {

        fnGetTableAdjuntoControl(IdControl);

        TituloModal = 'Adjunto anexado con éxito';

        Swal.fire({
            title: TituloModal,
            iconColor: "#8fbb02",
            iconHtml: '<img src="./images/sho/check.svg" width="28px">',
            showConfirmButton: false,
            padding: "3em 3em 6em 3em ",
            timer: 1500,
        }).then(() => {

        });

    });

}
//#endregion


//#region GET Cargar Tabla de Adjuntos
function fnGetTableAdjuntoControl(IdControl) {

    //Se especifican los parametros para la consulta
    let url = apiUrlsho + `/api/ho_Get_seguimiento_controles_adjunto?code=P7airZXwvWaqiHaqjBThvpg5z4kZJuxUAsrj9aRRz40IUDIjKUlPIw==`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": { "ControlId": IdControl }
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done((response) => {

        //Si es que hay archivos se muestra la lista de archivos
        if (response.AdjuntoInterconsult.length > 0) {
            $('#divArchivos').show();
        }
        //Se cargan los elementos de la vista con la data obtenida

        $('#table_adjunto_Control').find('.table-empty').remove();
        $('#table_adjunto_Control').find('.table-row').remove();

        if (Object.keys(response.AdjuntoInterconsult).length > 0) {

            $.each(response.AdjuntoInterconsult, function (index, accountObj) {

                paObj_descargarAdjunto_Control[index] = accountObj;

                html_tbody = "<tr class='table-row'>";
                html_tbody += "<td>";
                html_tbody += accountObj.NombreArchivo;
                html_tbody += "</td>";
                html_tbody += "<td>";
                html_tbody += new Date(accountObj.CreadoFecha).toLocaleDateString('en-GB').split('T')[0];
                html_tbody += "</td>";
                html_tbody += "<td>";
                html_tbody += "<button type='button' class='btn btn-link shadow-none float-right btn-delete'>";
                html_tbody += "<img class='inject-svg' src='./images/sho/delete.svg' alt='' fill='#ff3636' width='16px' onclick = 'fnEliminarAdjuntoControl(" + IdControl + "," + accountObj.Id + ")' title='Eliminar Adjunto'>";
                html_tbody += "</button>";
                html_tbody += "<button type='button' class='btn btn-link shadow-none float-right'>";
                html_tbody += "<img class='inject-svg' src='./images/sho/download.svg' alt='' fill='#8fbb02' width='16px' onclick = 'fnDescargarAdjuntoControl(" + index + ")' title='Descargar Adjunto'>";
                html_tbody += "</button>";
                html_tbody += "</td>";
                html_tbody += "</tr>";

                $("#table_adjunto_Control tbody").append(html_tbody);

            });

        }
        else {

            html_tbody =
                "<tr class='table-empty'>" +
                "<td colspan='3' class='text-center text-uppercase'>" +
                "No hay informacion registrada" +
                "</td>" +
                "</tr> ";

            $("#table_adjunto_Control tbody").append(html_tbody);

        }

        $("#table_adjunto_Control-count-row").text($("#table_adjunto_Control tr.table-row").length);

    });

}
//#endregion


//#region Eliminar Adjunto
function fnEliminarAdjuntoControl(IdControl, IdAdjunto) {

    let data = {};
    let TituloModal = '';
    let NumeroFilas = 0;

    data.Id1 = IdAdjunto;
    data.ControlId = IdControl;
    data.accion = 2;
    data.NombreArchivo = '';
    data.ArchivoBase64 = '';
    data.CreadoPor = '';
    data.ModificadoPor = '';

    let url = apiUrlsho + `/api/ho_Post_seguimiento_controles_adjunto?code=X41MwVla/x0/aOG5M8kMFVnVxKuA3Co8LyQoXikaBa7GnVfWmFmqLg==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        $("#table_adjunto_Control tbody").find(".table-empty").remove();

        NumeroFilas = $("#table_adjunto_Control tbody tr").length;

        TituloModal = 'Adjunto eliminado con éxito';

        if (NumeroFilas == 0) {

            html_tbody =
                "<tr class='table-empty'>" +
                "<td colspan='3' class='text-center text-uppercase'>" +
                "No hay informacion registrada" +
                "</td>" +
                "</tr> ";

            $("#table_adjunto_Control tbody").append(html_tbody);

        }

        Swal.fire({
            title: TituloModal,
            iconColor: "#8fbb02",
            iconHtml: '<img src="./images/sho/check.svg" width="28px">',
            showConfirmButton: false,
            padding: "3em 3em 6em 3em ",
            timer: 1500,
        });

    });

}
//#endregion


//#region Descargar Adjunto
function fnDescargarAdjuntoControl(indexObj) {

    url = paObj_descargarAdjunto_Control[indexObj].ArchivoBase64;

    fetch(url)
        .then(res => res.blob())
        .then(function (blob) {
            downloadBlob(blob, paObj_descargarAdjunto_Control[indexObj].NombreArchivo);
        });

}

function downloadBlob(blob, filename) {

    if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    } else {
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
    }

}
//#endregion

