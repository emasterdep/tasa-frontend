
//#region GET Cargar Select de Seguimiento
function fnGetPlucksSeguimiento() {

    //Se especifican los parametros para la consulta
    var url = apiUrlsho + "/api/ho_ergo?code=V2asJPyv8pzLX2o5wkWcwFFD386Y3Rmhrea9oa5CEFdvDJ2sPtaMzg==&AccionBackend=CombosDifusionResultadoAll"

    var headers = {
        "apikey": constantes.apiKey
    }

    var settings = {
        "url": url,
        "method": "GET",
        "timeout": 0,
        "crossDomain": true,
        "dataType": "json",
        "headers": headers,
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done(function (response) {

        pluck_Seguimiento["EstadoControl"] = response.lista_StatusControl;
        pluck_Seguimiento["OrigenIncidente"] = response.lista_OrigenIncidente;
        pluck_Seguimiento["ControlImplementar"] = response.lista_ControlImplementar;

    });

}
//#endregion


//#region GET Cargar Select Area Responsable
function fnGetSedesAreasGerencias() {

    //Se especifican los parametros para la consulta
    var url = apiUrlssoma + "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0"

    var headers = {
        "apikey": constantes.apiKey
    }

    var settings = {
        "url": url,
        "method": "GET",
        "timeout": 0,
        "crossDomain": true,
        "dataType": "json",
        "headers": headers,
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done(function (response) {

        pluck_Seguimiento["Areas"] = response.Area;
        pluck_Seguimiento["Gerencias"] = response.Gerencia;
        pluck_Seguimiento["Sedes"] = response.Sedes;
    });

}
//#endregion


//#region GET Cargar Select de Seguimiento
function fnGetSelectsForm() {

    //Se especifican los parametros para la consulta
    let url = apiUrlsho + `/api/ho_get_seguimiento_controles?code=z3M/GEgWlcW42lvBNU9Yq1Gw9gZIGqYyN070D1gHrGbgxZrBbJLE2A==&AccionBackEnd=ListaControlIncidencia`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        pluck_Seguimiento["CodigoControl"] = response.lista_Control;
        pluck_Seguimiento["CodigoIncidencia"] = response.lista_Incidencia;

    });

}
//#endregion


//#region SET Asignar datos a los Selects
function fnSetSelectSeguimiento() {

    let contenido = '';
    let ControlOptionSelect = false;

    //#region Codigo Control
    contenido = $('#dat_id_control');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Seguimiento["CodigoControl"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.CodigoControl}</option>`)
    });
    //#endregion


    //#region Codigo Incidencia
    contenido = $('#dat_id_incidencia');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Seguimiento["CodigoIncidencia"].forEach((Item) => {
        ControlOptionSelect = false;
        $('#dat_id_incidencia option').each(function () {
            if ($.trim($(this).text()) == $.trim(Item.CodigoIncidencia)) {
                ControlOptionSelect = true;
            }
        });
        if (!ControlOptionSelect) {
            contenido.append(`<option value="${Item.Id}">${Item.CodigoIncidencia}</option>`);
        }
    });

    //#endregion


    //#region Origen Incidente
    contenido = $('#dat_origen');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Seguimiento["OrigenIncidente"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Origen_Incidente}</option>`)
    });
    //#endregion


    //#region Estados de Control
    contenido = $('#dat_estado');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Seguimiento["EstadoControl"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Status_Control}</option>`)
    });
    //#endregion


    //#region Control a Implementar
    contenido = $('#dat_control_implementar');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Seguimiento["ControlImplementar"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Control_Implementar}</option>`)
    });
    //#endregion


    //#region Cargando Select de Áreas
    contenido = $('#dat_area');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Seguimiento["Areas"].forEach((Item) => {

        ControlOptionSelect = false;

        $('#dat_area option').each(function () {

            if ($.trim($(this).text()) == $.trim(Item.Description)) {
                ControlOptionSelect = true;
            }

        });

        if (!ControlOptionSelect) {

            contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`);

        }

    });
    //#endregion


}
//#endregion


