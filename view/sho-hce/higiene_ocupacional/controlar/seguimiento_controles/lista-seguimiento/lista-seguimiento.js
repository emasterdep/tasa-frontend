//#region Variables
var valor = 0;
var ObjTabla;
var ObjTablaSeguimiento;
var ObjListaBandeja;
var ListaBandeja;

//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Al iniciar
async function onInit() {

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnGetSedesAreasGerencias(),
        fnGetPlucksSeguimiento(),
        fnGetSelectsForm(),
        fnGetPuestoTrabajo()
    ]);

    fnSetSelectSeguimiento();

    await fnCargarBandeja();
    await fnCargarTablaSeguimiento();

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Inicializar
$(document).ready(function () {

    onInit();

    clearSelect();
    $('.select2-container').css('width', '100%');

    $('.nav-link-ho').on('click', onClickSeccion);

    fnSetChangeFilter();
});
//#endregion


//#region Cambiar de Modulo
function onClickSeccion() {

    if (!$(this).hasClass("active")) {

        switch ($(this).attr("data-seccion")) {

            case "Identificar":

                Sp5controlNavegacionHigiene(100);
                handlerUrlhtml('contentGlobal', 'view/sho-hce/ges/datosGenerales.html', 'Identificar');
                break;

            case "Evaluar":

                Sp5controlNavegacionHigiene(200);
                handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_evaluar/datosGenerales.html', 'Evaluar');
                break;

            case "Controlar":

                Sp5controlNavegacionHigiene(300);
                handlerUrlhtml('contentGlobal', 'view/sho-hce/controlar/datosGenerales.html', 'Controlar');
                break;

        }

    }

}
//#endregion


//#region Cargar Fecha Actual (Hoy)
function cargarFechaHoy() {

    var fecha = new Date();
    var mes = fecha.getMonth() + 1;
    var dia = fecha.getDate();
    var anio = fecha.getFullYear();

    //Agrega cero si el dia es menor de 10
    if (dia < 10) {
        dia = '0' + dia;
    }

    //Agrega cero si el mes es menor de 10
    if (mes < 10) {
        mes = '0' + mes
    }

    return (anio + "-" + mes + "-" + dia);

}
//#endregion


//#region Ver Detalle
function fnVerDetalle(IdControl, NombreIncidente, IdEstado) {

    bModoEditar = false;
    IdControlGlobal = IdControl;
    IdEstadoControlGlobal = IdEstado;
    NombreIncidenteGlobal = 'Control - ' + NombreIncidente;

    handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_ocupacional/controlar/seguimiento_controles/form-seguimiento/form-seguimiento.html', NombreIncidenteGlobal);

}
//#endregion


//#region Editar
function fnEditar(IdControl, NombreIncidente, IdEstado) {

    bModoEditar = true;
    IdControlGlobal = IdControl;
    IdEstadoControlGlobal = IdEstado;
    NombreIncidenteGlobal = 'Control - ' + NombreIncidente;

    handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_ocupacional/controlar/seguimiento_controles/form-seguimiento/form-seguimiento.html', NombreIncidenteGlobal);
}
//#endregion


//#region Navegacion
function Sp5controlNavegacionHigiene(CodControl) {
    ControlNavHigiene = CodControl;
}
//#endregion


//#region Limpiar Select
function clearSelect() {

    $("select").select2({
        allowClear: true
    }).on("select2:unselecting", function (e) {
        $(this).data('state', 'unselected');
    }).on("select2:open", function (e) {
        if ($(this).data('state') === 'unselected') {
            $(this).removeData('state');
            var self = $(this);
            setTimeout(function () {
                self.select2('close');
            }, 1);
        }
    });
}
//#endregion


//#region GET Cargar Puesto Trabajo
async function fnGetPuestoTrabajo() {

    let url = apiUrlsho + `/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaCombosGestion_All`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        pluck_Seguimiento["Puestos"] = response.lista_Cargos_Puestos_Trabajo;

    });
}
//#endregion


//#region GET Cargar Datos de la Tabla/Bandeja
async function fnCargarBandeja() {
    let url = apiUrlsho + `/api/ho_get_seguimiento_controles?code=z3M/GEgWlcW42lvBNU9Yq1Gw9gZIGqYyN070D1gHrGbgxZrBbJLE2A==&AccionBackEnd=BandejaSeguimientoControles`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaBandeja = response.lista_BandejaControles

        fnSetValueStringsTable();

    });
}
//#endregion


//#region SET Asignar Valores en la tabla
async function fnSetValueStringsTable() {

    //#region Asignar Gerencias
    ListaBandeja.forEach((ItemBandeja) => {
        pluck_Seguimiento["Gerencias"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdGerencia) {
                ItemBandeja.GerenciaDescripcion = Item.Description;
            }
        });
    });
    //#endregion

    //#region Asignar Sedes
    ListaBandeja.forEach((ItemBandeja) => {
        pluck_Seguimiento["Sedes"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdUnidad) {
                ItemBandeja.SedeDescripcion = Item.Description;
            }
        });
    });
    //#endregion

    //#region Asignar Areas
    ListaBandeja.forEach((ItemBandeja) => {
        pluck_Seguimiento["Areas"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdArea) {
                ItemBandeja.AreaDescripcion = Item.Description;
            }
        });
    });
    //#endregion

    //#region Asignar Puestos de Trabajo
    ListaBandeja.forEach((ItemBandeja) => {
        pluck_Seguimiento["Puestos"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdArea) {
                ItemBandeja.PuestoTrabajoDescripcion = Item.PuestoTrabajo;
            }
        });
    });
    //#endregion

    //#region Asignar Areas Responsables
    ListaBandeja.forEach((ItemBandeja) => {
        pluck_Seguimiento["Areas"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdAreaResponsable) {
                ItemBandeja.AreaResponsableDescripcion = Item.Description;
            }
        });
    });
    //#endregion

}
//#endregion


//#region Cargar Datos de Tabla
function fnCargarTablaSeguimiento() {

    let contenido = '';
    let datoVacio = '';
    let html_tbody = '';
    let ContadorFila = 0;

    $.fn.dataTable.ext.search.pop();

    contenido = $('#content_seguimiento_controles');
    contenido.empty();

    ListaBandeja.forEach((Item) => {

        paObj_detalle_Monitoreo[Item.Id] = Item;

        html_tbody = "<tr>";
        html_tbody += "<td>" + Item.GerenciaDescripcion + "</td>";
        html_tbody += "<td>" + Item.SedeDescripcion + "</td>";
        html_tbody += "<td>" + Item.AreaDescripcion + "</td>";
        html_tbody += "<td>" + Item.PuestoTrabajoDescripcion + "</td>";
        html_tbody += "<td>" + Item.ControlImplementar + "</td>";

        html_tbody += "<td>" + Item.CodigoControl + "</td>";
        html_tbody += "<td>" + Item.OrigenIncidencia + "</td>";
        html_tbody += "<td>" + Item.CodigoIncidencia + "</td>";
        html_tbody += "<td>" + Item.FechaAcuerdo + "</td>";
        html_tbody += "<td>" + Item.FechaCierre + "</td>";
        html_tbody += "<td>" + Item.EstadoControl + "</td>";
        html_tbody += "<td>" + Item.AreaResponsableDescripcion + "</td>";
        html_tbody += "<td>" + Item.Responsable + "</td>";

        html_tbody += "<td>";
        html_tbody += "<div class='dropdown dropleft'>";
        html_tbody += "<a class='nav-link text-vertical' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        html_tbody += "...";
        html_tbody += "</a>";
        html_tbody += "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";

        html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(301);`;
        html_tbody += `fnVerDetalle(${Item.IdControl} , '${Item.CodigoIncidencia}' , ${Item.IdEstado});"`;
        html_tbody += "' style='cursor: pointer;'>";
        html_tbody += "<img src='./images/sho/eyeIcon.svg' alt='' fill='#01719d'>";
        html_tbody += "&nbsp;&nbsp; Ver registro";
        html_tbody += "</a>";

        if (Item.IdEstado != 2) {
            html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(301);`;
            html_tbody += `fnEditar(${Item.IdControl} , '${Item.CodigoIncidencia}' , ${Item.IdEstado});"`;
            html_tbody += "style='cursor: pointer; '>";
            html_tbody += "<img src='./images/sho/edit.svg' alt='' fill='#5daf57'>";
            html_tbody += "&nbsp;&nbsp; Editar";
            html_tbody += "</a>";
        }

        html_tbody += "</div>";
        html_tbody += "</div>";
        html_tbody += "</td>";

        html_tbody += "</tr>";

        contenido.append(html_tbody);
        ContadorFila++;

    });


    ObjTablaSeguimiento = $('#table_seguimiento_controles').DataTable({
        "pageLength": 5,
        "ordering": false,
        "scrollX": true,
        "destroy": true,
        "language": {
            "paginate": {
                "next": "»",
                "previous": "«"
            },
            "loadingRecords": "Cargando registros...",
            "processing": "Procesando...",
            "infoPostFix": "",
            "zeroRecords": "No se encontraron registros"
        },
        "columnDefs": [
            {
                "targets": [8],
                "visible": true,
                "searchable": true
            },
        ],
        initComplete: function () {
            $('body').find('.dataTables_scrollBody').addClass("scrollbar");
        },
        dom:
            /* "<'row btn-table'<B>>" + */
            "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row row-records'<'col-sm-12'tr>>" +
            "<'row row-info'<'col-sm-12 col-md-12'p>>"
    });



    $('#table_seguimiento_controles-count-row').text(ObjTablaSeguimiento.rows().count());
    $('.scrollbar').removeClass('dataTables_scrollBody');

    if ($.fn.DataTable.isDataTable('#table_seguimiento_controles')) {
        $('#table_seguimiento_controles').find('.nav-link').trigger('click');
    }

    //{ targets: [0, 1,2,3], visible: true, searchable: false},

    /* Inicializa la tabla cuando se hace uso del filtro fecha */
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {

            var iFini = document.getElementById('dFechaDesde').value;
            var iFfin = document.getElementById('dFechaHasta').value;
            var iStartDateCol = 7;
            var iEndDateCol = 7;

            iFini = iFini.replace(/-/g, "");
            iFfin = iFfin.replace(/-/g, "");

            var datofini = data[iStartDateCol].substring(6, 10) + data[iStartDateCol].substring(3, 5) + data[iStartDateCol].substring(0, 2);
            var datoffin = data[iEndDateCol].substring(6, 10) + data[iEndDateCol].substring(3, 5) + data[iEndDateCol].substring(0, 2);

            if (iFini === "" && iFfin === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin === "") {
                return true;
            }
            else if (iFfin >= datoffin && iFini === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin >= datoffin) {
                return true;
            }
            return false;

        }
    );

}
//#endregion


//#region Filtros de Tabla
function fnSetChangeFilter() {


    //#region Filtrar si es Select
    $('.data-filtro-select').on('change', function () {

        valor = ($(this).find('option[value="' + $(this).val().trim() + '"]').text()).trim();

        ObjTablaSeguimiento.column(parseInt($(this).attr('data-filtro-select'))).search(valor ? '^' + valor + '$' : '', true, false).draw();

        $('#table_seguimiento_controles-count-row').text(ObjTablaSeguimiento.page.info().recordsDisplay);

    });
    //#endregion

    //#region Filtrar si es input text
    $('.data-filtro').on('keyup', function () {

        ObjTablaSeguimiento.column(parseInt($(this).attr('data-filtro'))).search($(this).val()).draw();
        $('#table_seguimiento_controles-count-row').text(ObjTablaSeguimiento.page.info().recordsDisplay);

    });
    //#endregion

    //#region Filtrar Fechas   
    $('#dFechaDesde').on('keyup change', function () {

        $('#dFechaHasta').attr("dFechaDesde", $('#dFechaDesde').val());
        ObjTablaSeguimiento.draw();
        $('#table_seguimiento_controles-count-row').text(ObjTablaSeguimiento.page.info().recordsDisplay);

    });

    $('#dFechaHasta').on('keyup change', function () {

        $('#dFechaDesde').attr("dFechaHasta", $('#dFechaHasta').val());
        ObjTablaSeguimiento.draw();
        $('#table_seguimiento_controles-count-row').text(ObjTablaSeguimiento.page.info().recordsDisplay);

    });
    //#endregion

}
  //#endregion


