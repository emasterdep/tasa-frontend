
//#region Cargar Fecha Actual (Hoy)
function cargarFechaHoy() {

    var fecha = new Date();
    var mes = fecha.getMonth() + 1;
    var dia = fecha.getDate();
    var anio = fecha.getFullYear();

    //Agrega cero si el dia es menor de 10
    if (dia < 10) {
        dia = '0' + dia;
    }

    //Agrega cero si el mes es menor de 10
    if (mes < 10) {
        mes = '0' + mes
    }

    return (anio + "-" + mes + "-" + dia);

}
//#endregion


//#region GET Cargar Select Area Responsable
function fnGetSedesAreasGerencias() {

    //Se especifican los parametros para la consulta
    var url = apiUrlssoma + "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0"

    var headers = {
        "apikey": constantes.apiKey
    }

    var settings = {
        "url": url,
        "method": "GET",
        "timeout": 0,
        "crossDomain": true,
        "dataType": "json",
        "headers": headers,
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done(function (response) {

        pluck_Proteccion["Areas"] = response.Area;
        pluck_Proteccion["Gerencias"] = response.Gerencia;
        pluck_Proteccion["Sedes"] = response.Sedes;

    });

}
//#endregion


//#region GET Cargar ActividadMA, Agentes, Riesgos y Puestos
function fnGetPuestoTrabajo() {

    let url = apiUrlsho + `/api/ho_gestion?code=eNaD99TYDYZh45n9DLfBSdy/8UGDAGoZzUUoNRz38haoLyomuVTqoA==&AccionBackend=ListaCombosGestion_All`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        pluck_Proteccion["Puestos"] = response.lista_Cargos_Puestos_Trabajo;

    });
}
//#endregion


//#region GET Cargar Estados
async function fnGetEstados() {
    let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackEnd=ListaEstados`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        pluck_Proteccion["Estados"] = response.lista_Estados

    });
}
//#endregion


//#region SET Asignar datos a los Selects
function fnSetSelectProteccion() {

    let contenido = '';
    let ControlOptionSelect = false;

    //#region Cargando Select de Puesto
    contenido = $('#dat_puesto');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Proteccion["Puestos"].forEach((Item) => {

        ControlOptionSelect = false;

        $('#dat_puesto option').each(function () {
            if ($.trim($(this).text()) == $.trim(Item.PuestoTrabajo)) {
                ControlOptionSelect = true;
            }
        });

        if (!ControlOptionSelect) {
            contenido.append(`<option value="${Item.Id}">${Item.PuestoTrabajo}</option>`);
        }

    });
    //#endregion


    //#region Cargando Select de Gerencias
    contenido = $('#dat_gerencia');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Proteccion["Gerencias"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
    });
    //#endregion


    //#region Cargando Select de Sedes
    contenido = $('#dat_sede');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Proteccion["Sedes"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`)
    });
    //#endregion


    //#region Cargando Select de Áreas
    contenido = $('#dat_area');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Proteccion["Areas"].forEach((Item) => {

        ControlOptionSelect = false;

        $('#dat_area option').each(function () {

            if ($.trim($(this).text()) == $.trim(Item.Description)) {
                ControlOptionSelect = true;
            }

        });

        if (!ControlOptionSelect) {

            contenido.append(`<option value="${Item.Id}">${Item.Description}</option>`);

        }

    });
    //#endregion


    //#region Estados de Evaluacion
    if (!bForm) {
        contenido = $('#dat_estado');
        contenido.empty();
        contenido.append(`<option value=""></option>`);

        pluck_Proteccion["Estados"].forEach((Item) => {
            contenido.append(`<option value="${Item.Id}">${Item.Descripcion}</option>`)
        });
    }

    //#endregion

}
//#endregion


//#region CHANGE Cambiar Numero de Documento
function onChangeDocument() {

    var numDoc = $(this).val()
    var tamanio = $(this).val().length

    if (numDoc != 0 && tamanio >= 8) {
        fnGetDatosTrabajador($("#sNumeroDoc").val())
    }
    else {
        fnClearDatosTrabajador()
    }

}
//#endregion


//#region GET Cargar Datos del Trabajador
async function fnGetDatosTrabajador(NumeroDoc) {

    let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackEnd=ListaDatosEvaluado&NumeroDoc=` + NumeroDoc;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        var DatosEvaluado = response.lista_DatosEvaluado

        if (DatosEvaluado.length > 0) {
            fnSetDatosTrabajador(DatosEvaluado);
        }
        else {
            fnClearDatosTrabajador()
        }

    });

}
//#endregion


//#region SET Asignar Datos de Trabajador
function fnSetDatosTrabajador(lista) {

    $('#sNombres').val(lista[0].Nombres);
    $('#sApellidos').val(lista[0].Apellidos);

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidad + "']").prop("selected", true);
    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $("#dat_puesto option[value='" + lista[0].IdPuestoTrabajo + "']").prop("selected", true);

}
//#endregion


//#region Limpiar Datos de Trrabajador
function fnClearDatosTrabajador() {
    $('#sNombres').val("");
    $('#sApellidos').val("");
    $("#dat_gerencia").val("");
    $("#dat_sede").val("")
    $("#dat_area").val("")
    $("#dat_puesto").val("")
}
//#endregion


//#region GET Cargar Select de Proteccion
function fnGetSelectsForm() {

    //Se especifican los parametros para la consulta
    var url = apiUrlsho + "/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackend=ListaCombosForm"

    var headers = {
        "apikey": constantes.apiKey
    }

    var settings = {
        "url": url,
        "method": "GET",
        "timeout": 0,
        "crossDomain": true,
        "dataType": "json",
        "headers": headers,
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done(function (response) {

        pluck_Proteccion["TipoProteccionAuditiva"] = response.lista_TipoProteccionAuditiva;
        pluck_Proteccion["TipoRespirador"] = response.lista_TipoRespirador;
        pluck_Proteccion["TallaRespirador"] = response.lista_TallaRespirador;

    });

}
//#endregion


//#region SET Asignar datos a los Selects
function fnSetSelectProteccionForm() {

    let contenido = '';
    let ControlOptionSelect = false;

    //#region Tipo Proteccion Auditiva
    contenido = $('#dat_tipo_proteccion');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Proteccion["TipoProteccionAuditiva"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.TipoProteccionAuditiva}</option>`)
    });
    //#endregion

    //#region Tipo Respirador
    contenido = $('#dat_tipo_respirador');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Proteccion["TipoRespirador"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.TipoRespirador}</option>`)
    });
    //#endregion


    //#region Talla Respirador
    contenido = $('#dat_talla_respirador');
    contenido.empty();
    contenido.append(`<option value=""></option>`);

    pluck_Proteccion["TallaRespirador"].forEach((Item) => {
        contenido.append(`<option value="${Item.Id}">${Item.TallaRespirador}</option>`)
    });
    //#endregion

}
//#endregion


//#region Validar Formulario (Todos los Inputs y Selects Required)
function fnValidationForm() {
    let error = [];
    let inputs = $('input');
    let selects = $('select');

    inputs.each((e) => {
        if (!inputs.eq(e).val()) {
            if (inputs[e].required) {
                inputs.eq(e).addClass('is-invalid');
                inputs.eq(e).parent().find('.invalid-feedback').remove();
                inputs.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
                error.push(inputs.eq(e).val())
            }
        }
        if (inputs.eq(e).val()) {
            inputs.eq(e).removeClass('is-invalid');
        }
    })

    selects.each((e) => {
        if (!selects.eq(e).val()) {
            if (selects[e].required) {
                selects.eq(e).addClass('is-invalid');
                selects.eq(e).parent().find('.invalid-feedback').remove();
                selects.eq(e).parent().append(`<div class="invalid-feedback">Requerido!</div>`)
                error.push(selects.eq(e).val())
            }
        }
        if (selects.eq(e).val()) {
            selects.eq(e).removeClass('is-invalid');
        }
    })

    return error;
}
//#endregion