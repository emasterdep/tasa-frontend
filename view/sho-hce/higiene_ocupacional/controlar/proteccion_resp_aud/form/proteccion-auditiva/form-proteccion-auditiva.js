//#region Variables
var ListaProteccion;
var IdProteccion = 0;
var nTipoProteccion = 1

var IdEstadoProteccion;
var NombreEstadoNuevo;
var ArchivoPDF = '';
var paObj_descargarAdjunto_Proteccion = [];
//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Inicializar
$(document).ready(function () {

    $('#divArchivos').hide();
    onInit();

    //$("#sNumeroDoc").on('change', onChangeDocument);
    var numdoc = document.getElementById('sNumeroDoc');
    numdoc.addEventListener('keyup', onChangeDocument, false);
    $('#dFechaRegistro').val(cargarFechaHoy());

});
//#endregion


//#region Inicio
async function onInit() {

    if (!bNewForm) {
        await fnSetVariablesGlobales();
    }

    fnOcultarMostrarBotones();

    //Se inicia el Cargando
    showLoading();

    await Promise.all([
        fnGetSedesAreasGerencias(),
        fnGetPuestoTrabajo(),
        fnGetSelectsForm()
    ]);

    await fnSetSelectProteccion();
    fnSetSelectProteccionForm();

    //Si ya esta registrado
    if (!bNewForm) {
        fnGetDataProteccion();
    }

    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Ocultar Mostrar Botones
async function fnOcultarMostrarBotones() {

    $('#divBtnInvisible').hide();

    //Nuevos
    if (bNewForm) {
        $('#divAdjuntar').hide();
    }
    //Ya Registrados
    else if (!bNewForm) {
        $('#divAdjuntar').show();
        //Solo leer
        if (!bModoEditar || IdEstadoProteccion == 2) {
            $('#divBtnInvisible').show();
            $('#divBtnCancelar').show();
            $('#divBtnCerrar').hide();
            $('#divBtnGuardar').hide();

            document.getElementById('fieldset1').disabled = true;
            document.getElementById('fieldset2').disabled = true;
            document.getElementById('fieldset3').disabled = true;
        }
    }

    if (!bModoEditar) {
        $('#divAdjuntar').hide();
    }

}
//#endregion


//#region Asignar Variables Globales
async function fnSetVariablesGlobales() {
    //Variables Anteriores    
    IdProteccion = IdProteccionGlobal
    IdEstadoProteccion = IdEstadoProteccionGlobal;
}
//#endregion


//#region Funcionalidad Boton Cancelar / Volver
function fnBtnVolver() {
    if (bProteccionRespAudMain) {
        handlerUrlhtml('contentGlobal', 'view/sho-hce/controlar/datosGenerales.html', 'Controlar');
    }
    else {
        ControlLateralFitTest = true;
        handlerUrlhtml('contentGlobal', 'view/sho-hce/historia_clinica/datosGenerales.html', 'Historia Clínica');
    }
}
//#endregion


//#region GET Cargar Datos de Proteccion Auditiva
function fnGetDataProteccion() {
    let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackEnd=ListaDatosProteccionAuditiva&IdProteccion=
                ${IdProteccion}&nTipoProteccion=${nTipoProteccion}`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaProteccion = response.lista_DatosProteccionAuditiva

        fnSetDatosProteccion(ListaProteccion);

    });
}
//#endregion


//#region SET Asignar Datos de Proteccion Auditiva
function fnSetDatosProteccion(lista) {

    $('#sNumeroDoc').val(lista[0].NroDocumento);
    $('#sNombres').val(lista[0].Nombres);
    $('#sApellidos').val(lista[0].Apellidos);
    $('#sNombres').val(lista[0].Nombres);

    $("#dat_gerencia option[value='" + lista[0].IdGerencia + "']").prop("selected", true);
    $("#dat_sede option[value='" + lista[0].IdUnidad + "']").prop("selected", true);
    $("#dat_area option[value='" + lista[0].IdArea + "']").prop("selected", true);
    $("#dat_puesto option[value='" + lista[0].IdPuestoTrabajo + "']").prop("selected", true);

    $('#sCodEvaluacion').val(lista[0].CodigoEvaluacion);
    $("#dat_tipo_proteccion option[value='" + lista[0].IdTipoProteccion + "']").prop("selected", true);
    $('#sMarcaProteccion').val(lista[0].Marca);
    $('#sModeloProteccion').val(lista[0].Modelo);

    $('#sNRRIzquierda').val(lista[0].NRRIzquierda);
    $('#sNRRDerecha').val(lista[0].NRRDerecha);
    $('#sIndice').val(lista[0].IndiceAtenuacion);
    $('#sComentario').val(lista[0].Comentarios);

    if (lista[0].ProteccionMayor) {
        $("#dat_am_relacion_ocupacional_si").prop("checked", true);
    }
    else {
        $("#dat_am_relacion_ocupacional_no").prop("checked", true);
    }

    if (lista[0].FechaRegistro != "") { $('#dFechaRegistro').val(lista[0].FechaRegistro); }
    if (lista[0].FechaEvaluacion != "") { $('#dFechaEvaluacion').val(lista[0].FechaEvaluacion); }
    if (lista[0].FechaVencimiento != "") { $('#dFechaVencimiento').val(lista[0].FechaVencimiento); }

    if (IdProteccion > 0) {
        fnGetTableAdjuntoProteccion(IdProteccion);
    }

}
//#endregion


//#region Funcionalidad CRUD
function fnCRUDForm(EstadoNuevo) {

    switch (EstadoNuevo) {
        case 1:
            NombreEstadoNuevo = 'Guardar'
            break;
        case 2:
            NombreEstadoNuevo = 'Cerrar'
            break;

        default:
            break;
    }

    if (fnValidationForm().length > 0) {
        return Swal.fire({
            icon: 'warning',
            title: 'Advertencia',
            text: 'Por favor complete todos los campos',
        });
    }

    Swal.fire({
        title: `${NombreEstadoNuevo} evaluación auditiva`,
        html: `
      <p>Está por ${NombreEstadoNuevo} la evaluación </p>
      <p class="mt-5">¿Desea confirmar la acción?</p>`,
        icon: "info",
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonColor: "#ff3636",
        confirmButtonColor: "#8fbb02",
        confirmButtonText: `Confirmar <img class="inject-svg" src="./images/sho/confirm.svg" alt="" fill="#fff" width="18px">`,
        cancelButtonText: `Cancelar <img class="inject-svg" src="./images/sho/cancelar.svg" alt="" fill="#fff" width="18px">`,
    }).then((result) => {

        if (result.isConfirmed) {
            fnPostFormulario(EstadoNuevo);
        }
    });
}
//#endregion


//#region POST: Programar | Guardar | Cerrar | Anular
function fnPostFormulario(IdEstado) {


    let data = {};
    let EjecutarModal = false;
    let TituloModal = '';

    var radioBtn = $('input:radio[name=radio_alcanzo_proteccion]:checked').val();


    //IdEstado => 1: Abierto | 2:Cerrar 

    data.accion = bNewForm == true ? 0 : 1
    data.TipoProteccion = nTipoProteccion

    data.IdProteccion = IdProteccion;
    data.Estado = IdEstado;

    data.NroDocumento = $("#sNumeroDoc").val();

    data.FechaRegistro = $("#dFechaRegistro").val() == "" ? "1900-01-01" : $("#dFechaRegistro").val()
    data.FechaEvaluacion = $("#dFechaEvaluacion").val() == "" ? "1900-01-01" : $("#dFechaEvaluacion").val()
    data.FechaVencimiento = $("#dFechaVencimiento").val() == "" ? "1900-01-01" : $("#dFechaVencimiento").val()

    data.IdTipoProteccion = $("#dat_tipo_proteccion option:selected").val();

    data.Marca = $("#sMarcaProteccion").val();
    data.Modelo = $("#sModeloProteccion").val();
    data.NRRIzquierda = $("#sNRRIzquierda").val();
    data.NRRDerecha = $("#sNRRDerecha").val();
    data.IndiceAtenuacion = $("#sIndice").val();
    data.ProteccionMayor = radioBtn == "1" ? true : false;

    data.Comentarios = $("#sComentario").val();


    showLoading();

    let url = apiUrlsho + `/api/ho_Post_proteccion_resp_aud?code=a1SOBJ/ouVnJSB3rdci6SkeYE31wjczR5osdajPU45omgy7Q0aKe2A==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        hideLoading();

        if (response.Id > 0) {

            if (IdEstado == 1) {
                EjecutarModal = true;
                TituloModal = 'Se guardó la evaluación con éxito';
            }

            if (IdEstado == 2) {
                EjecutarModal = true;
                TituloModal = 'Se cerró la evaluación con éxito';
            }

            if (EjecutarModal) {

                Swal.fire({
                    title: TituloModal,
                    iconColor: "#8fbb02",
                    iconHtml: '<img src="./images/sho/check.svg" width="28px">',
                    showConfirmButton: false,
                    padding: "3em 3em 6em 3em ",
                    timer: 1500,
                }).then(() => {

                    fnBtnVolver();

                });

            }
            else {
                Swal.fire({
                    title: 'No se pudo guardar',
                    icon: 'warning',
                    timer: 3500,
                })
            }


        }

    });

}
//#endregion


//#region Capturar Archivo Adjunto
function fnSetFileAdj(element, IdProteccion) {

    var file = element.files[0];
    var reader = new FileReader();

    reader.onloadend = function () {

        let data = {};

        data.ProteccionAuditivaId = IdProteccion;
        data.accion = 0;
        data.NombreArchivo = element.files[0].name;
        data.ArchivoBase64 = reader.result;
        data.CreadoPor = "IdHashUser";
        data.ModificadoPor = "IdHashUser";
        data.TipoAdjunto_1_2 = 1;
        data.TipoProteccion = 1

        fnPostAdjuntoProteccion(data, IdProteccion);

    }

    reader.readAsDataURL(file);

}
//#endregion


//#region POST Adjuntar Archivo
function fnPostAdjuntoProteccion(Data, IdProteccion) {

    let TituloModal = '';

    let url = apiUrlsho + `/api/ho_Post_proteccion_resp_aud_adjunto?code=av97S/HFiE8geAFIs5HgG4zuni33LM3rk8URG8/vEsjH1NhKB/kKcA==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(Data)
    };

    return $.ajax(settings).done((response) => {

        fnGetTableAdjuntoProteccion(IdProteccion);

        TituloModal = 'Adjunto anexado con éxito';

        Swal.fire({
            title: TituloModal,
            iconColor: "#8fbb02",
            iconHtml: '<img src="./images/sho/check.svg" width="28px">',
            showConfirmButton: false,
            padding: "3em 3em 6em 3em ",
            timer: 1500,
        }).then(() => {

        });

    });

}
//#endregion


//#region GET Cargar Tabla de Adjuntos
function fnGetTableAdjuntoProteccion(IdProteccion) {

    //Se especifican los parametros para la consulta
    let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud_adjunto?code=6j2pXrJTSUPUmkPxLWWaHV3S49wHDVVp9nTH6ZoRidS9H6bJMHb/AA==`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": { "ProteccionId": IdProteccion, "TipoProteccion": 1 }
    };

    //Se consulta mediante el metodo Ajax
    return $.ajax(settings).done((response) => {

        //Si es que hay archivos se muestra la lista de archivos
        if (response.AdjuntoInterconsult.length > 0) {
            $('#divArchivos').show();
        }
        else {
            $('#divArchivos').hide();
        }
        //Se cargan los elementos de la vista con la data obtenida

        $('#table_adjunto_Proteccion').find('.table-empty').remove();
        $('#table_adjunto_Proteccion').find('.table-row').remove();

        if (Object.keys(response.AdjuntoInterconsult).length > 0) {

            $.each(response.AdjuntoInterconsult, function (index, accountObj) {

                paObj_descargarAdjunto_Proteccion[index] = accountObj;

                html_tbody = "<tr class='table-row'>";
                html_tbody += "<td>";
                html_tbody += accountObj.NombreArchivo;
                html_tbody += "</td>";
                html_tbody += "<td>";
                html_tbody += new Date(accountObj.CreadoFecha).toLocaleDateString('en-GB').split('T')[0];
                html_tbody += "</td>";
                html_tbody += "<td>";
                if (bModoEditar && IdEstadoProteccion != 2) {
                    html_tbody += "<button type='button' class='btn btn-link shadow-none float-right btn-delete'>";
                    html_tbody += "<img class='inject-svg' src='./images/sho/delete.svg' alt='' fill='#ff3636' width='16px' onclick = 'fnEliminarAdjuntoProteccion(" + IdProteccion + "," + accountObj.Id + ")' title='Eliminar Adjunto'>";
                    html_tbody += "</button>";
                }
                html_tbody += "<button type='button' class='btn btn-link shadow-none float-right'>";
                html_tbody += "<img class='inject-svg' src='./images/sho/download.svg' alt='' fill='#8fbb02' width='16px' onclick = 'fnDescargarAdjuntoProteccion(" + index + ")' title='Descargar Adjunto'>";
                html_tbody += "</button>";
                html_tbody += "</td>";
                html_tbody += "</tr>";

                $("#table_adjunto_Proteccion tbody").append(html_tbody);

            });

        }
        else {

            html_tbody =
                "<tr class='table-empty'>" +
                "<td colspan='3' class='text-center text-uppercase'>" +
                "No hay informacion registrada" +
                "</td>" +
                "</tr> ";

            $("#table_adjunto_Proteccion tbody").append(html_tbody);

        }

        $("#table_adjunto_Proteccion-count-row").text($("#table_adjunto_Proteccion tr.table-row").length);

    });

}
//#endregion


//#region Eliminar Adjunto
function fnEliminarAdjuntoProteccion(IdProteccion, IdAdjunto) {

    let data = {};
    let TituloModal = '';
    let NumeroFilas = 0;

    data.Id1 = IdAdjunto;
    data.ProteccionAuditivaId = IdProteccion;
    data.accion = 2;
    data.NombreArchivo = '';
    data.ArchivoBase64 = '';
    data.CreadoPor = '';
    data.ModificadoPor = '';
    data.TipoProteccion = 1;

    let url = apiUrlsho + `/api/ho_Post_proteccion_resp_aud_adjunto?code=av97S/HFiE8geAFIs5HgG4zuni33LM3rk8URG8/vEsjH1NhKB/kKcA==&httpmethod=post`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "post",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers,
        "data": JSON.stringify(data)
    };

    return $.ajax(settings).done((response) => {

        fnGetTableAdjuntoProteccion(IdProteccion);

        $("#table_adjunto_Proteccion tbody").find(".table-empty").remove();

        NumeroFilas = $("#table_adjunto_Proteccion tbody tr").length;
        TituloModal = 'Adjunto eliminado con éxito';

        if (NumeroFilas == 0) {

            html_tbody =
                "<tr class='table-empty'>" +
                "<td colspan='3' class='text-center text-uppercase'>" +
                "No hay informacion registrada" +
                "</td>" +
                "</tr> ";

            $("#table_adjunto_Proteccion tbody").append(html_tbody);

        }

        Swal.fire({
            title: TituloModal,
            iconColor: "#8fbb02",
            iconHtml: '<img src="./images/sho/check.svg" width="28px">',
            showConfirmButton: false,
            padding: "3em 3em 6em 3em ",
            timer: 1500,
        });

    });

}
//#endregion


//#region Descargar Adjunto
function fnDescargarAdjuntoProteccion(indexObj) {

    url = paObj_descargarAdjunto_Proteccion[indexObj].ArchivoBase64;

    fetch(url)
        .then(res => res.blob())
        .then(function (blob) {
            downloadBlob(blob, paObj_descargarAdjunto_Proteccion[indexObj].NombreArchivo);
        });

}

function downloadBlob(blob, filename) {

    if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    } else {
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
    }

}
//#endregion


