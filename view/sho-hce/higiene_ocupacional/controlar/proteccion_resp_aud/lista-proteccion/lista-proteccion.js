//#region Variables
var valor = 0;
var ObjTablaProteccionAuditiva;
var ObjTablaProteccionRespiratoria;
var ListaBandejaAuditiva;
var ListaBandejaRespiratoria;

//#endregion


//#region Constructor
$(document).ready(() => {
    SVGInject($(".inject-svg"));
});
//#endregion


//#region Al iniciar
async function onInit() {

    //Se inicia el Cargando
    showLoading();
    bForm = false

    await Promise.all([
        fnGetSedesAreasGerencias(),
        fnGetPuestoTrabajo(),
        fnGetEstados()
    ]);

    fnSetSelectProteccion();

    await fnGetBandejaAuditiva()
    await fnCargarTablaProteccionAuditiva();

    await fnGetBandejaRespiratoria();
    await fnCargarTablaProteccionRespiratoria();


    //Se apaga el Cargando
    hideLoading();

}
//#endregion


//#region Inicializar
$(document).ready(function () {

    onInit();

    //$("#proteccion_auditiva-tab").on('click', fnChangeTableProteccion(1));
    //$("#proteccion_respiratoria-tab").on('click', fnChangeTableProteccion(2));

    if (NavProteccionRespAud == 1) {
        $("#proteccion_auditiva-tab").addClass("active");
        $("#proteccion_auditiva").addClass("show active");

        $("#proteccion_auditiva-tab").attr("aria-selected", "true");
        $("#proteccion_respiratoria-tab").attr("aria-selected", "false");

    }
    else {
        $("#proteccion_auditiva-tab").removeClass("active");
        $("#proteccion_auditiva-tab").attr("aria-selected", "false");
        $("#proteccion_respiratoria-tab").addClass("active");
        $("#proteccion_respiratoria").addClass("show active");
        $("#proteccion_respiratoria-tab").attr("aria-selected", "true");
    }

    clearSelect();
    $('.select2-container').css('width', '100%');

    $('.nav-link-ho').on('click', onClickSeccion);

    fnSetChangeFilterAuditiva();
});
//#endregion


//#region Navegacion
function Sp5controlNavegacionHigiene(CodControl) {
    ControlNavHigiene = CodControl;
}
//#endregion


//#region Cambiar de Modulo
function onClickSeccion() {

    if (!$(this).hasClass("active")) {

        switch ($(this).attr("data-seccion")) {

            case "Identificar":

                Sp5controlNavegacionHigiene(100);
                handlerUrlhtml('contentGlobal', 'view/sho-hce/ges/datosGenerales.html', 'Identificar');
                break;

            case "Evaluar":

                Sp5controlNavegacionHigiene(200);
                handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_evaluar/datosGenerales.html', 'Evaluar');
                break;

            case "Controlar":

                Sp5controlNavegacionHigiene(300);
                handlerUrlhtml('contentGlobal', 'view/sho-hce/controlar/datosGenerales.html', 'Controlar');
                break;

        }

    }

}
//#endregion


//#region Ver Detalle
function fnVerDetalle(nTipoProteccion, IdProteccion, IdEstado) {

    bNewForm = false;
    bModoEditar = false;
    IdProteccionGlobal = IdProteccion;
    IdEstadoProteccionGlobal = IdEstado;

    fnRegistrarEva(nTipoProteccion, bNewForm, 'Ver registro de');

}
//#endregion


//#region Editar
function fnEditar(nTipoProteccion, IdProteccion, IdEstado) {

    bNewForm = false;
    bModoEditar = true;
    IdProteccionGlobal = IdProteccion;
    IdEstadoProteccionGlobal = IdEstado;

    fnRegistrarEva(nTipoProteccion, bNewForm, 'Editar');
}
//#endregion


//#region Registrar
function fnRegistrarEva(nTipoProteccion, bNewFormParam, sAccion) {

    bForm = true
    bNewForm = bNewFormParam;
    bProteccionRespAudMain = true;

    if (nTipoProteccion == 1) {
        NavProteccionRespAud = 1
        NombreProteccion = sAccion + ' evaluación auditiva'
        handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_ocupacional/controlar/proteccion_resp_aud/form/proteccion-auditiva/form-proteccion-auditiva.html', NombreProteccion);
    }
    else if (nTipoProteccion == 2) {
        NavProteccionRespAud = 2
        NombreProteccion = sAccion + ' evaluación respiratoria'
        handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_ocupacional/controlar/proteccion_resp_aud/form/proteccion-respiratoria/form-proteccion-respiratoria.html', NombreProteccion);
    }

}
//#endregion


//#region Limpiar Select
function clearSelect() {

    $("select").select2({
        allowClear: true
    }).on("select2:unselecting", function (e) {
        $(this).data('state', 'unselected');
    }).on("select2:open", function (e) {
        if ($(this).data('state') === 'unselected') {
            $(this).removeData('state');
            var self = $(this);
            setTimeout(function () {
                self.select2('close');
            }, 1);
        }
    });
}
//#endregion


//#region Tabla Proteccion Auditiva

//#region GET Cargar Datos de la Tabla/Bandeja Auditiva
async function fnGetBandejaAuditiva() {
    let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackEnd=BandejaProteccionAuditiva`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaBandejaAuditiva = response.lista_BandejaProteccionAuditiva

        fnSetValueStringsTableAuditiva();

    });
}
//#endregion


//#region SET Asignar Valores en la tabla Proteccion Auditiva
async function fnSetValueStringsTableAuditiva() {
    ListaBandejaAuditiva.forEach((ItemBandeja) => {

        //Gerencia
        pluck_Proteccion["Gerencias"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdGerencia) {
                ItemBandeja.GerenciaDescripcion = Item.Description;
            }
        });

        //Sede / Unidad
        pluck_Proteccion["Sedes"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdUnidadSede) {
                ItemBandeja.SedeDescripcion = Item.Description;
            }
        });

        //Se recorre el listado Áreas y se compara con el AreaId_Empresa_H del paObj_ec y se añade la descripción del área al objeto paObj_ec
        pluck_Proteccion["Areas"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdArea) {
                ItemBandeja.AreaDescripcion = Item.Description;
            }
        });



    });
}
//#endregion


//#region Cargar Datos de Tabla Proteccion Auditiva
async function fnCargarTablaProteccionAuditiva() {

    let contenido = '';
    let datoVacio = '';
    let html_tbody = '';
    let ContadorFila = 0;

    $.fn.dataTable.ext.search.pop();

    contenido = $('#content_proteccion_auditiva');
    contenido.empty();

    ListaBandejaAuditiva.forEach((Item) => {

        //paObj_detalle_Monitoreo[Item.Id] = Item;

        html_tbody = "<tr>";
        html_tbody += "<td>" + Item.GerenciaDescripcion + "</td>";
        html_tbody += "<td>" + Item.SedeDescripcion + "</td>";
        html_tbody += "<td>" + Item.AreaDescripcion + "</td>";
        html_tbody += "<td>" + Item.PuestoCargoDescripcion + "</td>";

        html_tbody += "<td>" + Item.NroDocumento + "</td>";
        html_tbody += "<td>" + Item.Nombres + "</td>";
        html_tbody += "<td>" + Item.Apellidos + "</td>";
        html_tbody += "<td>" + Item.CodigoEvaluacion + "</td>";
        //html_tbody += "<td>" + Item.FechaEvaluacion + "</td>";
        html_tbody += "<td>" + new Date(Item.FechaEvaluacion).toLocaleDateString('en-GB').split('T')[0] + "</td>";

        html_tbody += "<td>" + Item.FechaVencimiento + "</td>";
        html_tbody += "<td>" + Item.TipoProteccion + "</td>";
        html_tbody += "<td>" + Item.Modelo + "</td>";
        html_tbody += "<td>" + Item.NRRIzquierda + "</td>";
        html_tbody += "<td>" + Item.NRRDerecha + "</td>";
        html_tbody += "<td>" + Item.IndiceAtenuacion + "</td>";
        html_tbody += "<td>" + Item.EstadoProteccion + "</td>";

        html_tbody += "<td>";
        html_tbody += "<div class='dropdown dropleft'>";
        html_tbody += "<a class='nav-link text-vertical' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        html_tbody += "...";
        html_tbody += "</a>";
        html_tbody += "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";

        html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(303);`;
        html_tbody += `fnVerDetalle(1,${Item.IdProteccion} , ${Item.IdEstadoProteccion});"`;
        html_tbody += "' style='cursor: pointer;'>";
        html_tbody += "<img src='./images/sho/eyeIcon.svg' alt='' fill='#01719d'>";
        html_tbody += "&nbsp;&nbsp; Ver registro";
        html_tbody += "</a>";

        if (Item.IdEstadoProteccion != 2) {
            html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(303);`;
            html_tbody += `fnEditar(1,${Item.IdProteccion} , ${Item.IdEstadoProteccion});"`;
            html_tbody += "style='cursor: pointer; '>";
            html_tbody += "<img src='./images/sho/edit.svg' alt='' fill='#5daf57'>";
            html_tbody += "&nbsp;&nbsp; Editar";
            html_tbody += "</a>";
        }

        html_tbody += "</div>";
        html_tbody += "</div>";
        html_tbody += "</td>";

        html_tbody += "</tr>";

        contenido.append(html_tbody);
        ContadorFila++;

    });


    ObjTablaProteccionAuditiva = $('#table_proteccion_auditiva').DataTable({
        "pageLength": 5,
        "ordering": false,
        "scrollX": true,
        "destroy": true,
        "language": {
            "paginate": {
                "next": "»",
                "previous": "«"
            },
            "loadingRecords": "Cargando registros...",
            "processing": "Procesando...",
            "infoPostFix": "",
            "zeroRecords": "No se encontraron registros"
        },
        buttons: [
            {
                extend: "excel", titleAttr: 'Generar Excel', title: 'Listado de Evaluación de Protección Auditiva', className: "btn btn-info btn-lg", text: '<i class="fa fa-file-excel fa-1x"></i>',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                }
            }
        ],
        "columnDefs": [
            {
                "targets": [16],
                "visible": true,
                "searchable": true
            }
        ],
        initComplete: function () {
            $('body').find('.dataTables_scrollBody').addClass("scrollbar");
        },
        dom:
            "<'row btn-table'<B>>" +
            "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row row-records'<'col-sm-12'tr>>" +
            "<'row row-info'<'col-sm-12 col-md-12'p>>"
    });

    //Contador de registros
    $('#table_proteccion_auditiva-count-row').text(ObjTablaProteccionAuditiva.rows().count());
    $('.scrollbar').removeClass('dataTables_scrollBody');

    if ($.fn.DataTable.isDataTable('#table_proteccion_auditiva')) {
        $('#table_proteccion_auditiva').find('.nav-link').trigger('click');
    }

    //Alinear con Navs
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });

    /* Inicializa la tabla cuando se hace uso del filtro fecha */
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {

            var iFini = document.getElementById('dFechaEvaluacion').value;
            var iFfin = document.getElementById('dFechaVencimiento').value;
            var iStartDateCol = 7;
            var iEndDateCol = 7;

            iFini = iFini.replace(/-/g, "");
            iFfin = iFfin.replace(/-/g, "");

            var datofini = data[iStartDateCol].substring(6, 10) + data[iStartDateCol].substring(3, 5) + data[iStartDateCol].substring(0, 2);
            var datoffin = data[iEndDateCol].substring(6, 10) + data[iEndDateCol].substring(3, 5) + data[iEndDateCol].substring(0, 2);

            if (iFini === "" && iFfin === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin === "") {
                return true;
            }
            else if (iFfin >= datoffin && iFini === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin >= datoffin) {
                return true;
            }
            return false;

        }
    );

}
//#endregion


//#region Filtros de Tabla Proteccion Auditiva
function fnSetChangeFilterAuditiva() {

    //#region Filtrar si es Select
    $('.data-filtro-select').on('change', function () {

        valor = ($(this).find('option[value="' + $(this).val().trim() + '"]').text()).trim();

        ObjTablaProteccionAuditiva.column(parseInt($(this).attr('data-filtro-select'))).search(valor ? '^' + valor + '$' : '', true, false).draw();

        $('#table_proteccion_auditiva-count-row').text(ObjTablaProteccionAuditiva.page.info().recordsDisplay);

    });
    //#endregion

    //#region Filtrar si es input text
    $('.data-filtro').on('keyup', function () {

        ObjTablaProteccionAuditiva.column(parseInt($(this).attr('data-filtro'))).search($(this).val()).draw();
        $('#table_proteccion_auditiva-count-row').text(ObjTablaProteccionAuditiva.page.info().recordsDisplay);

    });
    //#endregion

    //#region Filtrar Fechas   
    $('#dFechaEvaluacion').on('keyup change', function () {

        $('#dFechaVencimiento').attr("dFechaEvaluacion", $('#dFechaEvaluacion').val());
        ObjTablaProteccionAuditiva.draw();
        $('#table_proteccion_auditiva-count-row').text(ObjTablaProteccionAuditiva.page.info().recordsDisplay);

    });

    $('#dFechaVencimiento').on('keyup change', function () {

        $('#dFechaEvaluacion').attr("dFechaVencimiento", $('#dFechaVencimiento').val());
        ObjTablaProteccionAuditiva.draw();
        $('#table_proteccion_auditiva-count-row').text(ObjTablaProteccionAuditiva.page.info().recordsDisplay);

    });
    //#endregion

}
//#endregion

//#endregion


//#region Tabla Proteccion Respiratoria

//#region GET Cargar Datos de la Tabla/Bandeja Respiratoria
async function fnGetBandejaRespiratoria() {
    let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackEnd=BandejaProteccionRespiratoria`;

    let headers = {
        "apikey": constantes.apiKey,
        "Content-Type": "application/json"
    }

    let settings = {
        "url": url,
        "method": "get",
        "timeout": 0,
        "crossDomain": true,
        "dataType": 'json',
        "headers": headers
    };

    return $.ajax(settings).done((response) => {

        ListaBandejaRespiratoria = response.lista_BandejaProteccionRespiratoria

        fnSetValueStringsTableRespiratoria();

    });
}
//#endregion


//#region SET Asignar Valores en la tabla Proteccion Respiratoria
async function fnSetValueStringsTableRespiratoria() {
    ListaBandejaRespiratoria.forEach((ItemBandeja) => {

        //Gerencia
        pluck_Proteccion["Gerencias"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdGerencia) {
                ItemBandeja.GerenciaDescripcion = Item.Description;
            }
        });

        //Sede / Unidad
        pluck_Proteccion["Sedes"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdUnidadSede) {
                ItemBandeja.SedeDescripcion = Item.Description;
            }
        });

        //Se recorre el listado Áreas y se compara con el AreaId_Empresa_H del paObj_ec y se añade la descripción del área al objeto paObj_ec
        pluck_Proteccion["Areas"].forEach((Item) => {
            if (Item.Id == ItemBandeja.IdArea) {
                ItemBandeja.AreaDescripcion = Item.Description;
            }
        });

    });
}
//#endregion


//#region Cargar Datos de Tabla Proteccion Respiratoria
async function fnCargarTablaProteccionRespiratoria() {

    let contenido = '';
    let datoVacio = '';
    let html_tbody = '';
    let ContadorFila = 0;

    $.fn.dataTable.ext.search.pop();

    contenido = $('#content_proteccion_respiratoria');
    contenido.empty();

    ListaBandejaRespiratoria.forEach((Item) => {

        html_tbody = "<tr>";
        html_tbody += "<td>" + Item.GerenciaDescripcion + "</td>";
        html_tbody += "<td>" + Item.SedeDescripcion + "</td>";
        html_tbody += "<td>" + Item.AreaDescripcion + "</td>";
        html_tbody += "<td>" + Item.PuestoCargoDescripcion + "</td>";

        html_tbody += "<td>" + Item.NroDocumento + "</td>";
        html_tbody += "<td>" + Item.Nombres + "</td>";
        html_tbody += "<td>" + Item.Apellidos + "</td>";
        html_tbody += "<td>" + Item.CodigoEvaluacion + "</td>";
        html_tbody += "<td>" + Item.FechaEvaluacion + "</td>";
        //html_tbody += "<td>" + new Date(Item.FechaEvaluacion).toLocaleDateString('en-GB').split('T')[0] + "</td>";

        html_tbody += "<td>" + Item.FechaVencimiento + "</td>";
        html_tbody += "<td>" + Item.TipoRespirador + "</td>";
        html_tbody += "<td>" + Item.Modelo + "</td>";
        html_tbody += "<td>" + Item.TallaRespirador + "</td>";
        html_tbody += "<td>" + Item.Ovarall + "</td>";
        html_tbody += "<td>" + datoVacio + "</td>";
        html_tbody += "<td>" + Item.EstadoProteccion + "</td>";

        html_tbody += "<td>";
        html_tbody += "<div class='dropdown dropleft'>";
        html_tbody += "<a class='nav-link text-vertical' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        html_tbody += "...";
        html_tbody += "</a>";
        html_tbody += "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";

        html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(303);`;
        html_tbody += `fnVerDetalle(2,${Item.IdProteccion} , ${Item.IdEstadoProteccion});"`;
        html_tbody += "' style='cursor: pointer;'>";
        html_tbody += "<img src='./images/sho/eyeIcon.svg' alt='' fill='#01719d'>";
        html_tbody += "&nbsp;&nbsp; Ver registro";
        html_tbody += "</a>";

        if (Item.IdEstadoProteccion != 2) {
            html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(303);`;
            html_tbody += `fnEditar(2,${Item.IdProteccion} , ${Item.IdEstadoProteccion});"`;
            html_tbody += "style='cursor: pointer; '>";
            html_tbody += "<img src='./images/sho/edit.svg' alt='' fill='#5daf57'>";
            html_tbody += "&nbsp;&nbsp; Editar";
            html_tbody += "</a>";
        }

        html_tbody += "</div>";
        html_tbody += "</div>";
        html_tbody += "</td>";

        html_tbody += "</tr>";

        contenido.append(html_tbody);
        ContadorFila++;

    });


    ObjTablaProteccionRespiratoria = $('#table_proteccion_respiratoria').DataTable({
        "pageLength": 5,
        "ordering": false,
        "scrollX": true,
        "destroy": true,
        "language": {
            "paginate": {
                "next": "»",
                "previous": "«"
            },
            "loadingRecords": "Cargando registros...",
            "processing": "Procesando...",
            "infoPostFix": "",
            "zeroRecords": "No se encontraron registros"
        },
        buttons: [
            {
                extend: "excel", titleAttr: 'Generar Excel', title: 'Listado de Evaluación de Protección Respiratoria', className: "btn btn-info btn-lg", text: '<i class="fa fa-file-excel fa-1x"></i>',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                }
            }
        ],
        "columnDefs": [
            {
                "targets": [16],
                "visible": true,
                "searchable": true
            },
        ],
        initComplete: function () {
            $('body').find('.dataTables_scrollBody').addClass("scrollbar");
        },
        dom:
            "<'row btn-table'<B>>" +
            "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row row-records'<'col-sm-12'tr>>" +
            "<'row row-info'<'col-sm-12 col-md-12'p>>"
    });

    $('#table_proteccion_respiratoria-count-row').text(ObjTablaProteccionRespiratoria.rows().count());
    $('.scrollbar').removeClass('dataTables_scrollBody');

    if ($.fn.DataTable.isDataTable('#table_proteccion_respiratoria')) {
        $('#table_proteccion_respiratoria').find('.nav-link').trigger('click');
    }

    //Ajustar Columnas
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });

    /* Inicializa la tabla cuando se hace uso del filtro fecha */
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {

            var iFini = document.getElementById('dFechaEvaluacion').value;
            var iFfin = document.getElementById('dFechaVencimiento').value;
            var iStartDateCol = 7;
            var iEndDateCol = 7;

            iFini = iFini.replace(/-/g, "");
            iFfin = iFfin.replace(/-/g, "");

            var datofini = data[iStartDateCol].substring(6, 10) + data[iStartDateCol].substring(3, 5) + data[iStartDateCol].substring(0, 2);
            var datoffin = data[iEndDateCol].substring(6, 10) + data[iEndDateCol].substring(3, 5) + data[iEndDateCol].substring(0, 2);

            if (iFini === "" && iFfin === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin === "") {
                return true;
            }
            else if (iFfin >= datoffin && iFini === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin >= datoffin) {
                return true;
            }
            return false;

        }
    );

}
//#endregion


//#region Filtros de Tabla Proteccion Respiratoria
function fnSetChangeFilterRespiratoria() {


    //#region Filtrar si es Select
    $('.data-filtro-select').on('change', function () {

        valor = ($(this).find('option[value="' + $(this).val().trim() + '"]').text()).trim();

        ObjTablaProteccionRespiratoria.column(parseInt($(this).attr('data-filtro-select'))).search(valor ? '^' + valor + '$' : '', true, false).draw();

        $('#table_proteccion_respiratoria-count-row').text(ObjTablaProteccionRespiratoria.page.info().recordsDisplay);

    });
    //#endregion

    //#region Filtrar si es input text
    $('.data-filtro').on('keyup', function () {

        ObjTablaProteccionRespiratoria.column(parseInt($(this).attr('data-filtro'))).search($(this).val()).draw();
        $('#table_proteccion_respiratoria-count-row').text(ObjTablaProteccionRespiratoria.page.info().recordsDisplay);

    });
    //#endregion

    //#region Filtrar Fechas   
    $('#dFechaEvaluacion').on('keyup change', function () {

        $('#dFechaVencimiento').attr("dFechaEvaluacion", $('#dFechaEvaluacion').val());
        ObjTablaProteccionRespiratoria.draw();
        $('#table_proteccion_respiratoria-count-row').text(ObjTablaProteccionRespiratoria.page.info().recordsDisplay);

    });

    $('#dFechaVencimiento').on('keyup change', function () {

        $('#dFechaEvaluacion').attr("dFechaVencimiento", $('#dFechaVencimiento').val());
        ObjTablaProteccionRespiratoria.draw();
        $('#table_proteccion_respiratoria-count-row').text(ObjTablaProteccionRespiratoria.page.info().recordsDisplay);

    });
    //#endregion

}
//#endregion

//#endregion


//#region Cambiar Datos de Tabla
async function fnChangeTableProteccion(nTipoProteccion) {

    if (nTipoProteccion == 1) {
        await fnGetBandejaAuditiva();
        await fnCargarTablaProteccionAuditiva();
    }
    else if (nTipoProteccion == 2) {
        await fnGetBandejaRespiratoria()
        await fnCargarTablaProteccionRespiratoria();

    }

}
//#endregion