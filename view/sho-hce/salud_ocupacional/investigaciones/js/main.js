console.log("estoy en listado de investigaciones");
//lógica principal
window.addEventListener("loadGerenciasSedes", () => {
  //cargar Gerencias
  cargarSelect(
    "filtro_gerencia_list_sp5",
    window.areasGerencias.Gerencia,
    "Id",
    "Description"
  );
  //cargar sedes
  cargarSelect(
    "eo_sede_planta_input_sp5",
    window.areasGerencias.Sedes,
    "Id",
    "Description"
  );
  //cargar areas
  cargarSelect(
    "eo_area_input_sp5",
    getOnlyValue(window.areasGerencias.Area, "Description").filter(
      eliminarDuplicados
    ),
    "",
    "",
    "ASC"
  );
  cargarSelect(
    "eo_puesto_trabajo_input_sp5",
    getOnlyValue(vw_main.getPuestosTrabajo(0), "PuestoTrabajo").filter(
      eliminarDuplicados
    ),
    "",
    "",
    "ASC"
  );
});

//mostrar contador respectivo
function mostrarInvestigaciones() {
  document
    .getElementById("cont_list_medico_ocupa_investigaciones")
    .classList.remove("hidden");
  document
    .getElementById("cont_list_medico_ocupa_confirmados")
    .classList.add("hidden");
  document
    .getElementById("cont_list_medico_ocupa_seguimientos")
    .classList.add("hidden");
}

function mostrarConfirmadosSp5() {
  document
    .getElementById("cont_list_medico_ocupa_investigaciones")
    .classList.add("hidden");
  document
    .getElementById("cont_list_medico_ocupa_confirmados")
    .classList.remove("hidden");
  document
    .getElementById("cont_list_medico_ocupa_seguimientos")
    .classList.add("hidden");
}

function mostrarSeguimientosSp5() {
  document
    .getElementById("cont_list_medico_ocupa_investigaciones")
    .classList.add("hidden");
  document
    .getElementById("cont_list_medico_ocupa_confirmados")
    .classList.add("hidden");
  document
    .getElementById("cont_list_medico_ocupa_seguimientos")
    .classList.remove("hidden");
}

//inicia el filtro
function initBusquedadFiltro() {
  console.log("init busquedad.... **");
  let listadoInvestigaciones = JSON.parse(
    sessionStorage.getItem("listado_investigaciones")
  );
  let listadoConfirmados = JSON.parse(
    sessionStorage.getItem("listado_invConfirmados")
  );
  //dato a filtrar
  let gerenciaFiltro = parseInt(
    document.getElementById("filtro_gerencia_list_sp5").value
  );
  if (gerenciaFiltro != 0) {
    let valoresFiltrados = listadoInvestigaciones.filter(
      (element) => element.B_GerenciaId == gerenciaFiltro
    );
    //filtrar los confirmados
    let confirmadosFiltrados = listadoConfirmados.filter(
      (element) => element.B_GerenciaId == gerenciaFiltro
    );
    filtrarSedes(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtrarSedes(listadoInvestigaciones, listadoConfirmados);
  }
}

//filtra por sedes
function filtrarSedes(valores, confirmados) {
  let sedeFiltro = parseInt(
    document.getElementById("eo_sede_planta_input_sp5").value
  );
  if (sedeFiltro != 0) {
    let valoresFiltrados = valores.filter(
      (element) => element.B_SedeId == sedeFiltro
    );
    //filtrar confirmados
    let confirmadosFiltrados = confirmados.filter(
      (element) => element.B_SedeId == sedeFiltro
    );
    filtrarArea(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtrarArea(valores, confirmados);
  }
}

//filtra por areas
function filtrarArea(valores, confirmados) {
  let sedeArea = document.getElementById("eo_area_input_sp5").value;
  if (sedeArea != "0") {
    let valoresFiltrados = [];
    valores.forEach((element) => {
      if (getArea(element.B_AreaId) == sedeArea) {
        valoresFiltrados.push(element);
      }
    });
    //filtrar confirmados
    let confirmadosFiltrados = [];
    confirmados.forEach((element) => {
      if (getArea(element.B_AreaId) == sedeArea) {
        confirmadosFiltrados.push(element);
      }
    });
    filtrarDocumento(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtrarDocumento(valores, confirmados);
  }
}

//filtrar documento
function filtrarDocumento(valores, confirmados) {
  let documentoFiltro = document.getElementById("eo_documento_input_sp5").value;
  if (documentoFiltro != "") {
    let valoresFiltrados = valores.filter(
      (element) =>
        element.A_DniPaciente.toString().indexOf(documentoFiltro) != -1
    );
    //filtrar confirmados
    let confirmadosFiltrados = confirmados.filter(
      (element) =>
        element.A_DniPaciente.toString().indexOf(documentoFiltro) != -1
    );
    filtrarNombre(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtrarNombre(valores, confirmados);
  }
}

//filtrar nombre
function filtrarNombre(valores, confirmados) {
  let nombreFiltro = document.getElementById("eo_nombre_input_sp5").value;
  if (nombreFiltro != "") {
    let valoresFiltrados = valores.filter(
      (element) =>
        element.A_NombresPaciente.toUpperCase().indexOf(
          nombreFiltro.toUpperCase()
        ) != -1
    );
    //filtrar confirmados
    let confirmadosFiltrados = confirmados.filter(
      (element) =>
        element.A_NombresPaciente.toUpperCase().indexOf(
          nombreFiltro.toUpperCase()
        ) != -1
    );
    filtrarApellido(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtrarApellido(valores, confirmados);
  }
}

//filtrar apellido
function filtrarApellido(valores, confirmados) {
  let apellidoFiltro = document.getElementById("eo_apellido_input_sp5").value;
  if (apellidoFiltro != "") {
    let valoresFiltrados = valores.filter(
      (element) =>
        element.A_ApellidosPaciente.toUpperCase().indexOf(
          apellidoFiltro.toUpperCase()
        ) != -1
    );
    //filtrar confirmados
    let confirmadosFiltrados = confirmados.filter(
      (element) =>
        element.A_ApellidosPaciente.toUpperCase().indexOf(
          apellidoFiltro.toUpperCase()
        ) != -1
    );
    filtrarPuestoTrab(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtrarPuestoTrab(valores, confirmados);
  }
}

//filtrar puesto
function filtrarPuestoTrab(valores, confirmados) {
  let puestoFiltro = document.getElementById(
    "eo_puesto_trabajo_input_sp5"
  ).value;
  if (puestoFiltro != 0) {
    let valoresFiltrados = valores.filter(
      (element) => element.B_PuestoTrabajo.indexOf(puestoFiltro) != -1
    );
    //filtrar confirmados
    let confirmadosFiltrados = confirmados.filter(
      (element) => element.B_PuestoTrabajo.indexOf(puestoFiltro) != -1
    );
    filtroIDInvestigacion(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtroIDInvestigacion(valores, confirmados);
  }
}

//filtro de ID investigacion
function filtroIDInvestigacion(valores, confirmados) {
  console.log(valores);
  let idDocumento = document.getElementById(
    "eo_id_investigacion_input_sp5"
  ).value;
  if (idDocumento != 0) {
    let valoresFiltrados = valores.filter(
      (element) => element.Code.indexOf(idDocumento.toUpperCase()) != -1
    );
    //filtrar confirmados
    let confirmadosFiltrados = confirmados.filter(
      (element) => element.Code.indexOf(idDocumento.toUpperCase()) != -1
    );
    filtroRegistroDesdeHasta(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtroRegistroDesdeHasta(valores, confirmados);
  }
}

//filtro de registro desde
function filtroRegistroDesdeHasta(valores, confirmados) {
  let registroDesdeFiltro = document.getElementById(
    "eo_id_f_registro_desde_input_sp5"
  ).value;
  let registroHastaFiltro = document.getElementById(
    "eo_id_f_registro_hasta_input_sp5"
  ).value;
  //validación
  if (registroDesdeFiltro != "") {
    let valoresFiltrados = [];
    valores.forEach((element) => {
      let fechaConfirmada = new Date(
        element.CreatedDateF.split("/").reverse().join("/")
      );
      let fechaConfirmadaFinal = new Date();
      let fechaDesde = new Date(registroDesdeFiltro);
      let fechaHasta = new Date(registroHastaFiltro);
      //evaluacion
      if (fechaConfirmada >= fechaDesde || fechaConfirmada <= fechaHasta) {
        valoresFiltrados.push(element);
      }
    });
    //filtrar confirmados
    let confirmadosFiltrados = [];
    confirmados.forEach((element) => {
      let fechaConfirmada = new Date(
        element.CreatedDateF.split("/").reverse().join("/")
      );
      let fechaConfirmadaFinal = new Date();
      let fechaDesde = new Date(registroDesdeFiltro);
      let fechaHasta = new Date(registroHastaFiltro);
      //evaluacion
      if (fechaConfirmada >= fechaDesde || fechaConfirmada <= fechaHasta) {
        confirmadosFiltrados.push(element);
      }
    });
    filtroInicioInve(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtroInicioInve(valores, confirmados);
  }
}

//filtro de inicio investigacion
function filtroInicioInve(valores, confirmados) {
  let inicioInvFiltro = document.getElementById(
    "eo_id_f_inicio_investigac_input_sp5"
  ).value;
  if (inicioInvFiltro != "") {
    let valoresFiltrados = [];
    valores.forEach((element) => {
      //evaluacion
      if (
        moment(element.CreatedDateF.split("/").reverse().join("/")).format(
          "D/M/Y"
        ) == moment(inicioInvFiltro).format("D/M/Y")
      ) {
        valoresFiltrados.push(element);
      }
    });
    //filtrar confirmados
    let confirmadosFiltrados = [];
    confirmados.forEach((element) => {
      if (
        moment(element.CreatedDateF.split("/").reverse().join("/")).format(
          "D/M/Y"
        ) == moment(inicioInvFiltro).format("D/M/Y")
      ) {
        confirmadosFiltrados.push(element);
      }
    });
    filtroFinInve(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtroFinInve(valores, confirmados);
  }
}

//filtro de fin investigacion
function filtroFinInve(valores, confirmados) {
  let finInvFiltro = document.getElementById(
    "eo_id_f_fin_investigac_input_sp5"
  ).value;
  if (finInvFiltro != "") {
    let valoresFiltrados = [];
    valores.forEach((element) => {
      if (
        moment(
          element.CierreInvestigacionDateF.split("/").reverse().join("/")
        ).isValid()
      ) {
        //condicional para verificar si es la misma fecha final
        if (
          moment(
            element.CierreInvestigacionDateF.split("/").reverse().join("/")
          ).format("D/M/Y") == moment(finInvFiltro).format("D/M/Y")
        ) {
          valoresFiltrados.push(element);
        }
      }
    });
    //filtrar confirmados
    let confirmadosFiltrados = [];
    confirmados.forEach((element) => {
      if (
        moment(
          element.CierreInvestigacionDateF.split("/").reverse().join("/")
        ).isValid()
      ) {
        //condicional para verificar si es la misma fecha final
        if (
          moment(
            element.CierreInvestigacionDateF.split("/").reverse().join("/")
          ).format("D/M/Y") == moment(finInvFiltro).format("D/M/Y")
        ) {
          confirmadosFiltrados.push(element);
        }
      }
    });
    filtroDiagnosticoSp5(valoresFiltrados, confirmadosFiltrados);
  } else {
    filtroDiagnosticoSp5(valores, confirmados);
  }
}

//filtro diagnostico
function filtroDiagnosticoSp5(valores, confirmados) {
  console.log(valores);
  let diagnosticoFiltro = document.getElementById(
    "eo_diagnostico_input_sp5"
  ).value;
  if (diagnosticoFiltro != "") {
    let valoresFiltrados = valores.filter(
      (element) =>
        (element.DiagnosticosIniciales + " " + element.DiagnosticosFinales)
          .toUpperCase()
          .indexOf(diagnosticoFiltro.toUpperCase()) != -1
    );
    filtrarEstado(valoresFiltrados, confirmados);
  } else {
    filtrarEstado(valores, confirmados);
  }
}

//filtro estado de la investigacion
function filtrarEstado(valores, confirmados) {
  console.log(valores);
  let filterEstado = document.getElementById("eo_estado_input_sp5").value;
  if (filterEstado != "0") {
    let valoresFiltrados = valores.filter(
      (element) => element.EstadoEnfermedadOcupacional == filterEstado
    );
    window.listadoInvestigacion = valoresFiltrados;
  } else {
    window.listadoInvestigacion = valores;
  }
  window.listadoConfirmados = confirmados;
  listarInvestigacionesEO(1);
  drawInveConfirmadas(1);
}

//helpers funciones
function cargarSelect(path, contenido, value, name, type = "DES") {
  let content = '<option value="0">Seleccione</option>';
  contenido.forEach((element) => {
    if (type == "DES") {
      content += `<option value="${element[value]}">${element[name]}</option>`;
    } else {
      content += `<option value="${element}">${element}</option>`;
    }
  });
  document.getElementById(path).innerHTML = content;
}

function eliminarDuplicados(value, index, self) {
  return self.indexOf(value) === index;
}

function getOnlyValue(arreglo, propiedad) {
  let nuevoArray = [];
  arreglo.forEach((element) => {
    nuevoArray.push(element[propiedad]);
  });
  return nuevoArray;
}

function eliminarDuplicadosArrays(data, propiedad) {
  let arreglo = getOnlyValue(data, propiedad).filter(eliminarDuplicados);
  let nuevoArreglo = [];
  arreglo.forEach((element) => {
    data.forEach((item) => {
      console.log(element + " == " + item[propiedad]);
      if (element == item[propiedad]) {
        nuevoArreglo.push(item);
        return false;
      }
    });
  });
  return nuevoArreglo;
}
