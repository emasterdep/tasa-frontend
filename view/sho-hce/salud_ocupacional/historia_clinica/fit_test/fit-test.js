//#region Variables
var IdHistoriaClinica = 0;
var paObj_Proteccion = {}
var valor = 0;
var ObjTablaProteccionAuditiva;
var ObjTablaProteccionRespiratoria;
var ListaBandejaAuditiva;
var ListaBandejaRespiratoria;
//#endregion


//#region Abrir Modal Ver Datos
function fnModalVerDatosCompletoTrabajador() {

  //Se carga el modal con los datos completos del trabajador
  let datos = paObj_Proteccion[IdHistoriaClinica];

  Swal.fire({
    title: "Datos del trabajador",
    html: `
        <div class="text-left">
          <div class="row my-3">
            <div class="col-12">
              <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos Principales</span>
            </div>
          </div>
          <div class="row my-3" style="font-size: 15px">
            <div class="col-4">
              <span style="color: #254373"><b>Documento: </b></span>
              <span>${datos.NroDocumento_Trabajador_H}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Nombres: </b></span>
              <span>${datos.Nombres_Trabajador_H}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Apellidos: </b></span>
              <span>${datos.Apellidos_Trabajador_H}</span>
            </div>
          </div>
          <div class="row my-3" style="font-size: 15px">
            <div class="col-4">
              <span style="color: #254373"><b>C.Colaborador: </b></span>
              <span>${datos.CodigoColaborador_Trabajador_H}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Telefono: </b></span>
              <span>${datos.Telefono_Trabajador_H}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Sexo: </b></span>
              <span>${(datos.Sexo_Trabajador_H == 1) ? 'Masculino' : 'Femenino'}</span>
            </div>
          </div>
          <div class="row my-3" style="font-size: 15px">
            <div class="col-12">
              <span style="color: #254373"><b>Dirección: </b></span>
              <span>${datos.Direccion_Trabajador_H}</span>
            </div>
          </div>
          <div class="row my-3" style="font-size: 15px">          
            <div class="col-4">
              <span style="color: #254373"><b>Edad: </b></span>
              <span>${datos.Edad_Trabajador_H}</span>
            </div>
          </div>
          <hr class="my-4">
          <div class="row my-3">
            <div class="col-12">
              <span class="float-left" style="font-size: 20px; font-weight: 600; color: #254373">Datos de la empresa</span>
            </div>
          </div>
          <div class="row my-3" style="font-size: 15px">
            <div class="col-4">
              <span style="color: #254373"><b>Sede: </b></span>
              <span>${datos.SedeDescripcion}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Área: </b></span>
              <span>${datos.AreaDescripcion}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Cargo: </b></span>
              <span>${datos.CargoJefe_Empresa_H}</span>
            </div>
          </div>
          <div class="row my-3" style="font-size: 15px">
            <div class="col-4">
              <span style="color: #254373"><b>Jefe inmediato: </b></span>
              <span>${datos.JefeInmediato_Empresa_H}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Celular: </b></span>
              <span>${datos.Celular_Empresa_H}</span>
            </div>
            <div class="col-4">
              <span style="color: #254373"><b>Telefono: </b></span>
              <span>${datos.Telefono_Empresa_H}</span>
            </div>
          </div>
        </div>            
      `,
    iconHtml: '<img src="./images/sho/perfil.svg">',
    width: 800,
    showCancelButton: true,
    showConfirmButton: false,
    cancelButtonColor: "#ff3636",
    cancelButtonText: `Cancelar <img src="./images/sho/cancelar.svg">`,
  })
  $('.swal2-cancel').css('width', '200px');
  $('.swal2-html-container').css('overflow', 'visible');

}
//#endregion


//#region Inicializar
$(document).ready(function () {
  onInit();
});
//#endregion


//#region Constructor
$(document).ready(function () {

  //Se inicializa la vista  
  //console.log(idHC)
  IdHistoriaClinica = idHC

  onInit()

  $('#mostrar_datos_at').on('click', onClickMostrarDatos);
  $('#ocultar_datos_at').on('click', onClickOcultarDatos);


  if (NavProteccionRespAud == 1) {
    $("#proteccion_auditiva-tab").addClass("active");
    $("#proteccion_auditiva").addClass("show active");
    $("#proteccion_auditiva-tab").attr("aria-selected", "true");
    $("#proteccion_respiratoria-tab").attr("aria-selected", "false");

  }
  else {
    $("#proteccion_auditiva-tab").removeClass("active");
    $("#proteccion_auditiva-tab").attr("aria-selected", "false");
    $("#proteccion_respiratoria-tab").addClass("active");
    $("#proteccion_respiratoria").addClass("show active");
    $("#proteccion_respiratoria-tab").attr("aria-selected", "true");
  }

});
//#endregion


//#region Al iniciar
async function onInit() {

  //Se inicia el Cargando
  showLoading();

  await fnGetSedesAreasGerencias();
  await fnCargarDatosGenerales();

  await fnGetBandejaAuditiva()
  await fnCargarTablaProteccionAuditiva();

  await fnGetBandejaRespiratoria();
  await fnCargarTablaProteccionRespiratoria();

  //Se apaga el Cargando
  hideLoading();

}
//#endregion


//#region GET Cargar Select Sede, Area y Gerencia
function fnGetSedesAreasGerencias() {

  //Se especifican los parametros para la consulta
  var url = apiUrlssoma + "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0"

  var headers = {
    "apikey": constantes.apiKey
  }

  var settings = {
    "url": url,
    "method": "GET",
    "timeout": 0,
    "crossDomain": true,
    "dataType": "json",
    "headers": headers,
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done(function (response) {

    pluck_Proteccion["Areas"] = response.Area;
    pluck_Proteccion["Gerencias"] = response.Gerencia;
    pluck_Proteccion["Sedes"] = response.Sedes;

  });

}
//#endregion


//#region Cargar Datos Generales Por HC
function fnCargarDatosGenerales() {

  //Se especifican los parametros para la consulta
  let url = apiUrlsho + `/api/hce_Get_600_historia_clinica_datos_generales_completo?code=x65sNwDvhbkdVQk0BvFAlVp7TVvemyvLlzYet9HXKgv5H2Fb4xXaUg==`;
  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }
  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers,
    "data": { "IdHC": IdHistoriaClinica }
  };

  //Se consulta mediante el metodo Ajax
  return $.ajax(settings).done((response) => {

    //Se asiga la respuesta de la Historia al Obj paObj_ec
    paObj_Proteccion[IdHistoriaClinica] = response.HistoriaClin[0];

    //#region Asignar Areas
    pluck_Proteccion["Areas"].forEach((Item) => {

      if (Item.Id == paObj_Proteccion[IdHistoriaClinica].AreaId_Empresa_H) {
        paObj_Proteccion[IdHistoriaClinica].AreaDescripcion = Item.Description;
      }

    });
    //#endregion

    //#region Asignar Gerencias
    pluck_Proteccion["Gerencias"].forEach((Item) => {

      if (Item.Id == paObj_Proteccion[IdHistoriaClinica].GerenciaId_Empresa_H) {
        paObj_Proteccion[IdHistoriaClinica].GerenciaDescripcion = Item.Description;
      }

    });
    //#endregion

    //#region Asignar Sedes
    pluck_Proteccion["Sedes"].forEach((Item) => {

      if (Item.Id == paObj_Proteccion[IdHistoriaClinica].SedeId_Empresa_H) {
        paObj_Proteccion[IdHistoriaClinica].SedeDescripcion = Item.Description;
      }

    });
    //#endregion

    fnCargarElementosDatosGenerales();

  });

}
//#endregion


//#region Cargar Datos Generales
function fnCargarElementosDatosGenerales() {


  if (paObj_Proteccion[IdHistoriaClinica].FotoPacienteBase64.length) {
    $("img[name='img_file_perfil']").attr("src", paObj_Proteccion[IdHistoriaClinica].FotoPacienteBase64);
  } else {
    $("img[name='img_file_perfil']").attr("src", 'images/sho/profile.png')
  }


  if (paObj_Proteccion[IdHistoriaClinica].FechaRetiroTasa_H === undefined) {
    $('#sp4EnferCron_dat_trab_fr_tasa').val('Sin fecha de registro');
  }
  else {
    $('#sp4EnferCron_dat_trab_fr_tasa').val(new Date(paObj_Proteccion[IdHistoriaClinica].FechaRetiroTasa_H).toLocaleDateString('en-GB').split('T')[0]);
  }

  $('span[name=dat_nombres]').text(paObj_Proteccion[IdHistoriaClinica].Nombres_Trabajador_H + " " + paObj_Proteccion[IdHistoriaClinica].Apellidos_Trabajador_H);

  $('#dni_trabajador').text(paObj_Proteccion[IdHistoriaClinica].NroDocumento_Trabajador_H);
  $('#puesto_trabajador').text(paObj_Proteccion[IdHistoriaClinica].PuestoTrabajo_Empresa_H);
  $('#planta_trabajador').text(paObj_Proteccion[IdHistoriaClinica].SedeDescripcion);
  $('#gerencia_trabajador').text(paObj_Proteccion[IdHistoriaClinica].GerenciaDescripcion);
  $('#area_trabajador').text(paObj_Proteccion[IdHistoriaClinica].AreaDescripcion);
  $('#edad_trabajador').text(paObj_Proteccion[IdHistoriaClinica].Edad_Trabajador_H);
  $('#sexo_trabajador').text(paObj_Proteccion[IdHistoriaClinica].Sexo_Trabajador_H == 1 ? 'Masculino' : 'Femenino');

}


//#endregion


//#region Mostrar Datos
function onClickMostrarDatos() {
  $('#mostrar_datos_at').hide()
  $('#ocultar_datos_at').show()
  $('.show-less').hide()
  $('.show-more').show()
}
//#endregion


//#region Ocultar Datos
function onClickOcultarDatos() {
  $('#ocultar_datos_at').hide()
  $('#mostrar_datos_at').show()
  $('.show-less').show()
  $('.show-more').hide()
}
//#endregion


//#region Control Lateral Despues de Atencion medica
function onAtencionMedicaClick() {
  ControlLateralFitTest = true;
}
//#endregion


//#region GET Cargar Datos de la Tabla/Bandeja Auditiva
async function fnGetBandejaAuditiva() {
  let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackEnd=FitTestProteccionAuditiva&IdHistoriaClinica=` + IdHistoriaClinica;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {

    ListaBandejaAuditiva = response.lista_BandejaProteccionAuditiva

  });
}
//#endregion


//#region Cargar Datos de Tabla Proteccion Auditiva
async function fnCargarTablaProteccionAuditiva() {

  let contenido = '';
  let datoVacio = '';
  let html_tbody = '';
  let ContadorFila = 0;

  $.fn.dataTable.ext.search.pop();

  contenido = $('#content_proteccion_auditiva');
  contenido.empty();

  ListaBandejaAuditiva.forEach((Item) => {

    //paObj_detalle_Monitoreo[Item.Id] = Item;

    html_tbody = "<tr>";
    html_tbody += "<td>" + Item.CodigoEvaluacion + "</td>";
    html_tbody += "<td>" + Item.FechaEvaluacion + "</td>";
    html_tbody += "<td>" + Item.FechaVencimiento + "</td>";
    html_tbody += "<td>" + Item.TipoProteccion + "</td>";
    html_tbody += "<td>" + Item.Modelo + "</td>";
    html_tbody += "<td>" + Item.NRRIzquierda + "</td>";
    html_tbody += "<td>" + Item.NRRDerecha + "</td>";
    html_tbody += "<td>" + Item.IndiceAtenuacion + "</td>";
    html_tbody += "<td>" + Item.EstadoProteccion + "</td>";

    html_tbody += "<td>";
    html_tbody += "<div class='dropdown dropleft'>";
    html_tbody += "<a class='nav-link text-vertical' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
    html_tbody += "...";
    html_tbody += "</a>";
    html_tbody += "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";

    html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(303);`;
    html_tbody += `fnVerDetalle(1,${Item.IdProteccion} , ${Item.IdEstadoProteccion});"`;
    html_tbody += "' style='cursor: pointer;'>";
    html_tbody += "<img src='./images/sho/eyeIcon.svg' alt='' fill='#01719d'>";
    html_tbody += "&nbsp;&nbsp; Ver registro";
    html_tbody += "</a>";

    html_tbody += "</div>";
    html_tbody += "</div>";
    html_tbody += "</td>";

    html_tbody += "</tr>";

    contenido.append(html_tbody);
    ContadorFila++;

  });


  ObjTablaProteccionAuditiva = $('#table_proteccion_auditiva').DataTable({
    "pageLength": 5,
    "ordering": false,
    "scrollX": true,
    "destroy": true,
    "language": {
      "paginate": {
        "next": "»",
        "previous": "«"
      },
      "loadingRecords": "Cargando registros...",
      "processing": "Procesando...",
      "infoPostFix": "",
      "zeroRecords": "No se encontraron registros"
    },
    initComplete: function () {
      $('body').find('.dataTables_scrollBody').addClass("scrollbar");
    },
    dom:
      /* "<'row btn-table'<B>>" + */
      "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
      "<'row row-records'<'col-sm-12'tr>>" +
      "<'row row-info'<'col-sm-12 col-md-12'p>>"
  });

  //Contador de registros
  $('#table_proteccion_auditiva-count-row').text(ObjTablaProteccionAuditiva.rows().count());
  $('.scrollbar').removeClass('dataTables_scrollBody');

  if ($.fn.DataTable.isDataTable('#table_proteccion_auditiva')) {
    $('#table_proteccion_auditiva').find('.nav-link').trigger('click');
  }

  //Alinear
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $($.fn.dataTable.tables(true)).DataTable()
      .columns.adjust();
  });

  $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

}
//#endregion


//#region GET Cargar Datos de la Tabla/Bandeja Respiratoria
async function fnGetBandejaRespiratoria() {
  let url = apiUrlsho + `/api/ho_Get_proteccion_resp_aud?code=OTgta6WXaV37xZeNyQqyHAK2iaMu3AsKnxpT18Za6d6kk/z5QjJKaQ==&AccionBackEnd=FitTestProteccionRespiratoria&IdHistoriaClinica=` + IdHistoriaClinica;

  let headers = {
    "apikey": constantes.apiKey,
    "Content-Type": "application/json"
  }

  let settings = {
    "url": url,
    "method": "get",
    "timeout": 0,
    "crossDomain": true,
    "dataType": 'json',
    "headers": headers
  };

  return $.ajax(settings).done((response) => {

    ListaBandejaRespiratoria = response.lista_BandejaProteccionRespiratoria

  });
}
//#endregion


//#region Cargar Datos de Tabla Proteccion Respiratoria
async function fnCargarTablaProteccionRespiratoria() {

  let contenido = '';
  let datoVacio = '';
  let html_tbody = '';
  let ContadorFila = 0;

  $.fn.dataTable.ext.search.pop();

  contenido = $('#content_proteccion_respiratoria');
  contenido.empty();

  ListaBandejaRespiratoria.forEach((Item) => {

    html_tbody = "<tr>";
    html_tbody += "<td>" + Item.CodigoEvaluacion + "</td>";
    html_tbody += "<td>" + Item.FechaEvaluacion + "</td>";
    html_tbody += "<td>" + Item.FechaVencimiento + "</td>";
    html_tbody += "<td>" + Item.TipoRespirador + "</td>";
    html_tbody += "<td>" + Item.Modelo + "</td>";
    html_tbody += "<td>" + Item.TallaRespirador + "</td>";
    html_tbody += "<td>" + Item.Ovarall + "</td>";
    html_tbody += "<td>" + "Si" + "</td>";
    html_tbody += "<td>" + Item.EstadoProteccion + "</td>";

    html_tbody += "<td>";
    html_tbody += "<div class='dropdown dropleft'>";
    html_tbody += "<a class='nav-link text-vertical' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
    html_tbody += "...";
    html_tbody += "</a>";
    html_tbody += "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";

    html_tbody += `<a class='dropdown-item' onclick="Sp5controlNavegacionHigiene(303);`;
    html_tbody += `fnVerDetalle(2,${Item.IdProteccion} , ${Item.IdEstadoProteccion});"`;
    html_tbody += "' style='cursor: pointer;'>";
    html_tbody += "<img src='./images/sho/eyeIcon.svg' alt='' fill='#01719d'>";
    html_tbody += "&nbsp;&nbsp; Ver registro";
    html_tbody += "</a>";


    html_tbody += "</div>";
    html_tbody += "</div>";
    html_tbody += "</td>";

    html_tbody += "</tr>";

    contenido.append(html_tbody);
    ContadorFila++;

  });


  ObjTablaProteccionRespiratoria = $('#table_proteccion_respiratoria').DataTable({
    "pageLength": 5,
    "ordering": false,
    "scrollX": false,
    "destroy": true,
    "language": {
      "paginate": {
        "next": "»",
        "previous": "«"
      },
      "loadingRecords": "Cargando registros...",
      "processing": "Procesando...",
      "infoPostFix": "",
      "zeroRecords": "No se encontraron registros"
    },
    initComplete: function () {
      $('body').find('.dataTables_scrollBody').addClass("scrollbar");
    },
    dom:
      /* "<'row btn-table'<B>>" + */
      "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
      "<'row row-records'<'col-sm-12'tr>>" +
      "<'row row-info'<'col-sm-12 col-md-12'p>>"
  });

  $('#table_proteccion_respiratoria-count-row').text(ObjTablaProteccionRespiratoria.rows().count());
  $('.scrollbar').removeClass('dataTables_scrollBody');

  if ($.fn.DataTable.isDataTable('#table_proteccion_respiratoria')) {
    $('#table_proteccion_respiratoria').find('.nav-link').trigger('click');
  }

  //Alinear
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
}
//#endregion


//#region Ver Detalle
function fnVerDetalle(nTipoProteccion, IdProteccion, IdEstado) {

  bForm = true;
  bNewForm = false;
  bModoEditar = false;
  IdProteccionGlobal = IdProteccion;
  IdEstadoProteccionGlobal = IdEstado;
  bProteccionRespAudMain = false;

  if (nTipoProteccion == 1) {
    NavProteccionRespAud = 1
    NombreProteccion = 'Ver registro de evaluación auditiva'
    handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_ocupacional/controlar/proteccion_resp_aud/form/proteccion-auditiva/form-proteccion-auditiva.html', NombreProteccion);
  }
  else if (nTipoProteccion == 2) {
    NavProteccionRespAud = 2
    NombreProteccion = 'Ver registro de evaluación respiratoria'
    handlerUrlhtml('contentGlobal', 'view/sho-hce/higiene_ocupacional/controlar/proteccion_resp_aud/form/proteccion-respiratoria/form-proteccion-respiratoria.html', NombreProteccion);
  }

}
//#endregion

