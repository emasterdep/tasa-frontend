/**
 * editar la investigación
 * @returns
 */
var fecha = new Date();
window.paginaBack.enfermedadOcupacional = true;

function sendInvestigacionToServer() {
  let empleado = paObj_hc[idHC].a;
  Swal.fire({
    title: "Guardar registro de investigación de enfermedades ocupacionales",
    html: `<p>Está por guardar la investigación de enfermedad ocupacional de: ${empleado.Nombres_Trabajador_H} ${empleado.Apellidos_Trabajador_H}</p> <p class="mt-4">¿Desea confirmar la acción?</p>`,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#8fbb02",
    cancelButtonColor: "#d33",
    confirmButtonText: 'Confirmar <img src="./images/sho/confirm.svg" />',
    cancelButtonText: 'Cancelar <img src="./images/sho/cancelar.svg" />',
    reverseButtons: true,
  }).then((result) => {
    // si confirma que desea guardar
    proccessSendInvestigacion();
  });
}

function proccessSendInvestigacion() {
  document.getElementById("am_spin_guardar_investigacion_boton").style.display =
    "block";
  document.getElementById("icon-guardar-investigacion-form").style.display =
    "none";
  var raw = JSON.stringify(prerativeJsonInvestigacion());
  var url =
    apiUrlsho +
    `/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=post`;

  var myHeaders = new Headers();
  myHeaders.append("apikey", constantes.apiKey);
  myHeaders.append("Content-Type", "application/json");
  console.log(raw);
  console.log(prerativeJsonInvestigacion());
  fetch(url, {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  })
    .then((res) => res)
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log("save investigación");
      console.log(response);
      if (response.status == 200) {
        //si se guardo la investigación correctamente
        Swal.fire({
          title: "Se guardó el registro con éxito",
          iconColor: "#8fbb02",
          iconHtml: '<img src="./images/sho/check.svg" width="28px">',
          showConfirmButton: false,
          padding: "3em 3em 6em 3em ",
          timer: 1500,
        });
        document.getElementById(
          "am_spin_guardar_investigacion_boton"
        ).style.display = "none";
        document.getElementById(
          "icon-guardar-investigacion-form"
        ).style.display = "block";
        setTimeout(() => {
          handlerUrlhtml(
            "contentGlobal",
            "view/sho-hce/historia_clinica/datosGenerales.html",
            "Historia Clínica"
          );
          document.getElementById("menu_lateral1").classList.remove("active");
        }, 3000);
      }
      return response;
    });
}

function cancelarFormInvestigacion() {
  switch (window.toWhereBack) {
    case "default":
      window.modeVistaInvestigacion = false;
      handlerUrlhtml(
        "contentGlobal",
        "view/sho-hce/historia_clinica/datosGenerales.html",
        "Historia Clínica"
      );
      document.getElementById("menu_lateral1").classList.remove("active");
      break;
    case "medicoOcupacional":
      window.modeVistaInvestigacion = false;
      handlerUrlhtml(
        "contentGlobal",
        "view/sho-hce/salud_ocupacional/investigaciones/listado-investigaciones.html",
        "Investigaciones de enfermedades ocupacionales"
      );
      break;
  }
}

// funciones para formulario de investigaciones
function createObjInvestigacion(idObj) {
  console.log(idObj);
  let data = {
    Id: idObj,
    HistoriaClinicaId: IdHC,
    EstadoEnfermedadOcupacionalCode: parseInt(
      document.getElementById("codigo_estado_enfermedadOcupacional").value
    ),
    TurnosCode: parseInt(document.getElementById("turno_trabajo").value),
    DiaTurnoEncuentra: parseInt(
      document.getElementById("turno_encuentra").value
    ),
    ActividadRutinariaPuesto: document.getElementById(
      "activida_rutinaria_enfermedad"
    ).value,
    MotivoConsulta: document.getElementById("relato_molestia_consulta").value,
    FlagLesion: window.saludOcupacional.FlagLesion,
    DetalleLesion: document.getElementById("detalleDescansoMedico").value,
    FlagDescansosMedicos: window.saludOcupacional.FlagDescansosMedicos,
    FlagSintomatologia: 1,
    DetalleSintomatologia: document.getElementById("DetalleSintomatologia_id")
      .value,
    FechaInicioPuesto: document.getElementById("inicio_laborares").value,
    CreatedBy: "JAMQ postman",
    Deleted: 0,
  };

  return data;
}

function getDataEmpleado(empleado) {
  document.getElementById("iNombres_Trabajador_H").value =
    empleado.A_NombresPaciente;
  document.getElementById("NroDocumento_Trabajador_H").value =
    empleado.A_DniPaciente;
  document.getElementById("Apellidos_Trabajador_H").value =
    empleado.A_ApellidosPaciente;
  document.getElementById("Edad_Trabajador_H").value = empleado.A_Edad;
  document.getElementById("inicio_laborares").value = setInputDate(
    empleado.FechaInicioPuestoF,
    "latin"
  );
  //inicio de labores en el puesto
  document.getElementById(
    "CargoJefe_Empresa_H"
  ).innerHTML = `<option value="0" selected>${empleado.B_PuestoTrabajo}</option>`;
  document.getElementById("A_FechaIngresoTasa").value =
    empleado.A_FechaIngresoTasaF;
  // para gerencia del empleado
  document.getElementById("gerencia_work_empleado").value = getGerencia(
    empleado.B_GerenciaId
  );

  // para planta del empleado
  document.getElementById("planta_work_empleado").value = getSedePlanta(
    empleado.B_PlantaId
  );

  // para gerencia del empleado
  document.getElementById("area_work_empleado").value = getArea(
    empleado.B_AreaId
  );
}

/**
 * No se para que es esto
 */
function sendDataInvestigacion() {
  var url =
    apiUrlsho +
    `/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=post`;

  fetch(url, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
      data: JSON.stringify(createObjInvestigacion()),
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      renderListInvestigacionesOE(response);
    });
}

function prerativeJsonInvestigacion() {
  let obj_investigacion = JSON.parse(
    sessionStorage.getItem("obj_investigacion")
  );
  console.log(obj_investigacion);
  let evaluacionDSO = preparationDataEvaluacionDSO(
    obj_investigacion.EvaluacionDSO.Id
  );
  let jsonInvestigacion = createObjInvestigacion(obj_investigacion.Id);
  jsonInvestigacion.Insumos = returnInsumosGlobales();
  jsonInvestigacion.EvaluacionDSO = evaluacionDSO;
  jsonInvestigacion.DescansosMedicos = window.saludOcupacional.DescansosMedicos;
  jsonInvestigacion.Conclusiones = [];
  if (!isNaN(preparateJsonConclusion().AptitudId)) {
    jsonInvestigacion.Conclusiones.push(preparateJsonConclusion());
  }
  if (window.saludOcupacional.Medicamentos.length > 0) {
    jsonInvestigacion.Medicamentos = window.saludOcupacional.Medicamentos;
  }
  return jsonInvestigacion;
}

function preparateJsonConclusion() {
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );
  let conclusion = {
    Id: enfermedadStorage.Conclusiones.Id,
    AptitudId: parseInt(document.getElementById("conclu_inp_aptitu_con").value),
    InicioRestrincion: document.getElementById(
      "conclu_inp_fecha_inicio_rest_con"
    ).value,
    FinRestrincion: document.getElementById("conclu_inp_fecha_fin_rest_con")
      .value,
    TipoRestrincion: document.getElementById("conclu_inp_tipo_rest_con").value,
    PersonalSalud: document.getElementById("conclu_inp_personal_salud_con")
      .value,
    ReevaluacionDSO: document.getElementById(
      "conclu_inp_fecha_reevaluacion_dso_con"
    ).value,
    Observaciones: document.getElementById("conclu_inp_observaciones_con")
      .value,
    Deleted: 0,
  };
  return conclusion;
}

/**
 * añade insumos al array global para luego poder guardarlos, haciendo la petición post
 */
function addInsumoToInvestigacion() {
  const tiempoTranscurrido = Date.now();
  const hoy = new Date(tiempoTranscurrido);
  let dato = new Date();
  let insumo = {
    Description: document.getElementById("insumos_materiales").value,
    Id: 0,
    Date: hoy.toLocaleDateString(),
    CreatedDateF: hoy.toLocaleDateString(),
    Deleted: 0,
  };
  document.getElementById("insumos_materiales").value = "";
  window.saludOcupacional.insumo.push(insumo);
  listInsumosCreate(window.saludOcupacional.insumo);
}

/**
 * puestos de trabajo del paciente
 */
function listPuestosTrabajo(dataPuestosTrabajo) {
  let puestosAPintar = "";
  dataPuestosTrabajo.forEach((element, index) => {
    puestosAPintar += `
        <tr>
          <td>${element.B_PuestoTrabajo}</td>
          <td>${getArea(element.B_AreaId)}</td>
          <td>${getGerencia(element.B_GerenciaId)}</td>
          <td></td>
          <td>
            <div class="dropdown dropleft d-flex justify-content-end">

              <a
                class="nav-link textVertical"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ...
              </a>
              <div
                class="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
                x-placement="left-start"
                style="
                  position: absolute;
                  top: 15px;
                  left: -236px;
                  will-change: top, left;
                "
              >
                <a class="dropdown-item" onclick=""
                  ><img src="./images/sho/eyeIcon.svg" alt="" />&nbsp;&nbsp;
                  Exportar matriz IPERC</a
                >
              </div>
            </div>
          </td>
        </tr>`;
  });
  //pintarlo
  if (dataPuestosTrabajo.length == 0) {
    puestosAPintar =
      '<tr><td colspan="5" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById("content_puestos_trabajo_list_sp5").innerHTML =
    puestosAPintar;
}

/**
 *
 * @param {array} data
 * pinta los datos en la parte de insumos del formulario
 */
function listInsumosCreate(data) {
  document.getElementById("total-insumos").innerHTML = data.length;
  let listInsumos = "";
  data.forEach((element, index) => {
    listInsumos += `
        <tr>
          <td>${element.CreatedDateF}</td>
          <td>${element.Description}</td>
          <td>
            <div class="d-flex justify-content-end">
              <a href="javascript:void(0)" class="btn-delete" onclick="deleteInsumo(${index})">
                <img src="images/sho/delete-trash.png" style="width: 15px">
              </a>  
            </div>                    
          </td>
        </tr>`;
  });
  if (data.length == 0) {
    listInsumos +=
      '<tr><td colspan="3" class="text-center">No hay registros para esta tabla</td></tr>';
  }
  document.getElementById("content_insumos_list").innerHTML = listInsumos;
}

function deleteInsumo(index) {
  let insumoDeteted = {
    Description: window.saludOcupacional.insumo[index].Description,
    Id: window.saludOcupacional.insumo[index].Id,
    Date: window.saludOcupacional.insumo[index].Date,
    CreatedDateF: window.saludOcupacional.insumo[index].CreatedDateF,
    Deleted: 1,
  };
  window.arrayBorrado.insumos.push(insumoDeteted);
  //eliminandolo del listado
  window.saludOcupacional.insumo.splice(index, 1);
  listInsumosCreate(window.saludOcupacional.insumo);
}

function drawSelect() {
  document.getElementById("").addEventListener("click");
}

//manejo de array de descansos
function addDescansosMedicosToArray(data) {
  let accidente = {
    Id: 343,
    A_Accidente_TrabajoId: 1,
    A_Fecha_RegistroF: "08/11/2021",
    A_Fecha_AccidenteF: "08/11/2021",
    A_Hora_Accidente: "12:02:00",
    A_Estado_Accidente: 1,
    EstadoAccidente: "Abierto",
    TipoAccidente: "Primeros Auxilios",
    AccidenteTrabajoRelacionadoId: 0,
    MessageError: null,
  };
  window.saludOcupacional.AccidentesTrabajo.push(accidente);
}

/**
 * Edward Romero
 * segundo tab Formulario
 */
function getDataEvaluacionDSO(idObjDSO) {
  console.log(idObjDSO);
  let EvaluacionDSO = {
    Id: idObjDSO,
    Sistole: returnFloat(
      document.getElementById("dat_dg_presion_arterial_sv").value
    ),
    Diastole: returnFloat(
      document.getElementById("dat_dg_diastole_estado").value
    ),
    FrecuenciaCardiaca: returnFloat(
      document.getElementById("dat_dg_frecuencia_cardiaca_sv").value
    ),
    FrecuenciaRespiratoria: returnFloat(
      document.getElementById("dat_dg_frecuencia_respiratoria_sv").value
    ),
    Temperatura: returnFloat(
      document.getElementById("dat_dg_temperatura_sv").value
    ),
    Pesokg: returnFloat(document.getElementById("dat_dg_peso_sv").value),
    Talla: returnFloat(document.getElementById("dat_dg_talla_sv").value),
    Saturacion: returnFloat(
      document.getElementById("dat_dg_saturacion_sv").value
    ),
    IndiceMasaCorporal: returnFloat(
      document.getElementById("dat_dg_masa_corporal_sv").value
    ),
    PerimetroAbdominal: returnFloat(
      document.getElementById("dat_dg_perimetro_abdominal_sv").value
    ),
    EstadoConciencia: document.getElementById("dat_dg_estado_conciencia").value,
    EstadoNutricionId: returnFloat(
      document.getElementById("dat_dg_estado_nutricion").value
    ),
    EstadoGeneralId: returnFloat(
      document.getElementById("dat_dg_estado_general").value
    ),
    EstadoHidratacionId: returnFloat(
      document.getElementById("dat_dg_estado_Hidratacion").value
    ),
    Orofaringe: document.getElementById("data_dg_orofaringe_estado").value,
    ToraxPulmones: document.getElementById("data_dg_torax_pulmones_estado")
      .value,
    CardioVascular: document.getElementById("data_dg_cardio_vascular_estado")
      .value,
    Soma: document.getElementById("data_dg_soma_estado").value,
    FlagExamenAuxiliar: window.saludOcupacional.FlagExamenAuxiliar,
    TratamientoInicial: document.getElementById("dat_dg_tratamiento_inicial")
      .value,
    FlagJustificacion: window.saludOcupacional.FlagJustificacion,
    Justificacion: document.getElementById(
      "tab5_conclusiones_justificacion_enf"
    ).value,
    Deleted: 0,
  };
  return EvaluacionDSO;
}

function createOtraInvestigacion() {
  let otraInvestigacion = {
    Id: 0,
    TipoEvaluacionId: parseInt(
      document.getElementById("dat_dg_tipo_investigacion_otro").value
    ),
    Description: document.getElementById("dat_dg_estado_otra_investigacion")
      .value,
    Deleted: 0,
  };

  document.getElementById("dat_dg_estado_otra_investigacion").value = "";

  window.saludOcupacional.otrasInvestigaciones.push(otraInvestigacion);
  drawOtrasInvestigacionesDSO();
  return false;
}

function createOtraInvestigacion2() {
  let otraInvestigacion = {
    Id: 0,
    TipoEvaluacionId: parseInt(
      document.getElementById("dat_dg_tipo_investigacion_otro2").value
    ),
    Description: document.getElementById("dat_dg_estado_otra_investigacion2")
      .value,
    Deleted: 0,
  };

  document.getElementById("dat_dg_estado_otra_investigacion2").value = "";

  window.saludOcupacional.otrasInvestigaciones.push(otraInvestigacion);
  drawOtrasInvestigacionesDSO();
  return false;
}

function drawOtrasInvestigacionesDSO() {
  let otrasInvestigaciones = "";
  window.saludOcupacional.otrasInvestigaciones.forEach((element, index) => {
    otrasInvestigaciones += `
        <tr>
          <td>${returnTipoInvestigacion(element.TipoEvaluacionId)}</td>
          <td>${element.Description}</td>
          <td>
            <div class="d-flex justify-content-end">
              <a href="javascript:void(0)" class="btn-delete" onclick="deleteOtrasInvestigaciones(${index})">
                <img src="images/sho/delete-trash.png" style="width: 15px">
              </a>  
            </div>                    
          </td>
        </tr>`;
  });
  if (
    document.getElementById("content_otras_investigaciones_estado_general") !=
    undefined
  ) {
    if (window.saludOcupacional.otrasInvestigaciones.length < 1) {
      otrasInvestigaciones +=
        '<tr><td colspan="3" class="text-center">No hay registros para este listado</td></tr>';
    }
    document.getElementById(
      "content_otras_investigaciones_estado_general"
    ).innerHTML = otrasInvestigaciones;
    document.getElementById(
      "content_table_otras_evaluaciones_tabConclusion"
    ).innerHTML = otrasInvestigaciones;
    //contabilizo en el contador
    document.getElementById("cant_investigaciones_otras").innerHTML =
      window.saludOcupacional.otrasInvestigaciones.length;
    document.getElementById("cant_investigaciones_otras2").innerHTML =
      window.saludOcupacional.otrasInvestigaciones.length;
  }
}

function returnTipoInvestigacion(id) {
  let valor = "";
  window.TipoEvaluacion.forEach((element) => {
    if (id == element.Id) {
      valor = element.Descripcion;
    }
  });
  return valor;
}

function deleteOtrasInvestigaciones(index) {
  window.saludOcupacional.otrasInvestigaciones.splice(index, 1);
  drawOtrasInvestigacionesDSO();
}

function preparationDataEvaluacionDSO(id) {
  let evaluacionDSO = getDataEvaluacionDSO(id);
  if (window.saludOcupacional.otrasInvestigaciones.length > 0) {
    evaluacionDSO.OtrasEvaluaciones =
      window.saludOcupacional.otrasInvestigaciones;
  }
  if (window.saludOcupacional.factoresPersonales.length > 0) {
    evaluacionDSO.FactoresPersonales =
      window.saludOcupacional.factoresPersonales;
  }
  if (window.saludOcupacional.ExamenesAuxiliares.length > 0) {
    evaluacionDSO.ExamenesAuxiliares =
      window.saludOcupacional.ExamenesAuxiliares;
  }
  if (window.saludOcupacional.EvaluacionesEspecialistas.length > 0) {
    evaluacionDSO.EvaluacionesEspecialistas =
      returnFormatEvaluacionEspecialista();
  }
  evaluacionDSO.FlagExamenAuxiliar = window.saludOcupacional.FlagExamenAuxiliar;
  evaluacionDSO.Diagnosticos = preparateDiagnosticoCIE10TODSO();
  //rellando con los arrays borrados
  //retornar el objeto creado
  return evaluacionDSO;
}

//funcion para calcular el indice de masa corporal
function calcularIndiceMasaCorporal() {
  if (
    parseFloat(document.getElementById("dat_dg_talla_sv").value) != 0 &&
    parseFloat(document.getElementById("dat_dg_talla_sv").value) != undefined &&
    parseFloat(document.getElementById("dat_dg_peso_sv").value) != 0 &&
    parseFloat(document.getElementById("dat_dg_peso_sv").value) != undefined
  ) {
    console.log("calculando IMC....");
    let altura = parseFloat(document.getElementById("dat_dg_talla_sv").value);
    let peso = parseFloat(document.getElementById("dat_dg_peso_sv").value);
    console.log(peso + " -- " + altura);
    var indice = peso / Math.pow(altura, 2);
    document.getElementById("dat_dg_masa_corporal_sv").value =
      indice.toFixed(2);
  }
}

document
  .getElementById("dat_dg_talla_sv")
  .addEventListener("change", calcularIndiceMasaCorporal);
document
  .getElementById("dat_dg_peso_sv")
  .addEventListener("change", calcularIndiceMasaCorporal);

//factores de riesgos
function addFactoresRiesgos() {
  let factor = {
    Id: 0,
    Description: document.getElementById("dat_gd_detalle_factor_riesgo").value,
    Deleted: 0,
  };

  document.getElementById("dat_gd_detalle_factor_riesgo").value = "";

  window.saludOcupacional.factoresPersonales.push(factor);
  drawFactoresRiesgos();
}

function drawFactoresRiesgos() {
  let otrosFactores = "";
  window.saludOcupacional.factoresPersonales.forEach((element, index) => {
    otrosFactores += `
        <tr>
          <td>${element.Description}</td>
          <td>
            <div class="d-flex justify-content-end">
              <a href="javascript:void(0)" class="btn-delete" onclick="deleteFactorRiesgo(${index})">
                <img src="images/sho/delete-trash.png" style="width: 15px">
              </a>  
            </div>                    
          </td>
        </tr>`;
  });
  if (window.saludOcupacional.factoresPersonales.length < 1) {
    otrosFactores +=
      '<tr><td colspan="2" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById(
    "content_factores_de_riesgo_asociado_table"
  ).innerHTML = otrosFactores;
  document.getElementById("cant_factor_riesgo").innerHTML =
    window.saludOcupacional.factoresPersonales.length;
}

function deleteFactorRiesgo(index) {
  window.saludOcupacional.factoresPersonales.splice(index, 1);
  drawFactoresRiesgos();
}

//examenes auxiliares
function addExamenesAuxiliares() {
  let examenes = {
    Id: 0,
    Description: document.getElementById("dat_dg_tipoexamen_euxiliares")
      .options[
      document.getElementById("dat_dg_tipoexamen_euxiliares").selectedIndex
    ].text,
    Deleted: 0,
  };

  document.getElementById("dat_dg_tipoexamen_euxiliares").value = "";
  window.saludOcupacional.ExamenesAuxiliares.push(examenes);
  drawExamenesAuxiliares();
}

function drawExamenesAuxiliares() {
  let examenesAux = "";
  window.saludOcupacional.ExamenesAuxiliares.forEach((element, index) => {
    examenesAux += `
        <tr>
          <td>${element.Description}</td>
          <td>
            <div class="d-flex justify-content-end">
              <a href="javascript:void(0)" class="btn-delete" onclick="deleteExamenesAuxiliares(${index})">
                <img src="images/sho/delete-trash.png" style="width: 15px">
              </a>  
            </div>                    
          </td>
        </tr>`;
  });
  if (window.saludOcupacional.ExamenesAuxiliares.length < 1) {
    examenesAux +=
      '<tr><td colspan="2" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById("content_tipo_examen_table").innerHTML = examenesAux;
  document.getElementById("cant-tipo-examen-auxiliar").innerHTML =
    window.saludOcupacional.ExamenesAuxiliares.length;
}

function deleteExamenesAuxiliares(index) {
  window.saludOcupacional.ExamenesAuxiliares.splice(index, 1);
  drawExamenesAuxiliares();
}

/**
 * @date 10-01-2022
 * @author Edward
 * EXAMENES ESPECIALISTAS
 */
//examenes especialistas
function returnFormatEvaluacionEspecialista() {
  let formatEvaluacion = [];
  let formatEvaluacionDelete = [];
  window.saludOcupacional.EvaluacionesEspecialistas.forEach((element) => {
    let evaluacion = {
      Id: element.Id,
      TipoExamenId: element.TipoExamenId,
      Conclusion: element.Conclusion,
      ExamenDate: setInputDate(element.ExamenDateF),
      Deleted: 0,
    };
    formatEvaluacion.push(evaluacion);
  });
  window.arrayBorrado.EvaluacionEspecialistas.forEach((element) => {
    let evaluacion = {
      Id: element.Id,
      TipoExamenId: element.TipoExamenId,
      Conclusion: element.Conclusion,
      ExamenDate: setInputDate(element.ExamenDateF),
      Deleted: 1,
    };
    formatEvaluacionDelete.push(evaluacion);
  });
  cargarArray(formatEvaluacionDelete, formatEvaluacion);
  return formatEvaluacion;
}

function returnInsumosGlobales() {
  let insumoExistentes = [];
  let insumosBorrados = [];
  window.arrayBorrado.insumos.forEach((element) => {
    let insumoDeteted = {
      Description: element.Description,
      Id: element.Id,
      Date: element.Date,
      CreatedDateF: element.CreatedDateF,
      Deleted: 1,
    };
    insumosBorrados.push(insumoDeteted);
  });
  window.saludOcupacional.insumo.forEach((element) => {
    insumoExistentes.push(element);
  });
  cargarArray(insumosBorrados, insumoExistentes);
  return insumoExistentes;
}

function addEvaluacionEspecialista() {
  //valida si esta editando porque carga el index
  if (document.getElementById("index_array_examen_especialista").value != "") {
    updateValueEvaluacionEspecialista();
  } else {
    //si no es edición agrega
    let fechaCreacion = moment(
      document.getElementById("date_examen_eval_id").value
    );
    //el objecto a evaluación a crear
    let evaluacion = {
      Id: 0,
      TipoExamenId: parseInt(
        document.getElementById("tipo_examen_eval_id").value
      ),
      Conclusion: document.getElementById("conclusion_examen_eval_id").value,
      CreatedDateF:
        fecha.getDate() +
        "/" +
        fecha.getMonth() +
        1 +
        "/" +
        fecha.getFullYear(),
      Name: " ---- ",
      ExamenDateF: fechaCreacion.format("DD/MM/YYYY"),
      Deleted: 0,
    };
    window.saludOcupacional.EvaluacionesEspecialistas.push(evaluacion);
  }
  document.getElementById("tipo_examen_eval_id").value = "";
  document.getElementById("conclusion_examen_eval_id").value = "";
  document.getElementById("date_examen_eval_id").value = "";
  drawEvaluacionEspecialistas();
}

function drawEvaluacionEspecialistas() {
  let content = "";
  window.saludOcupacional.EvaluacionesEspecialistas.forEach(
    (element, index) => {
      content += `<tr>
      <td>${element.ExamenDateF}</td>
      <td>${cargarExamenAuxiliar(element.TipoExamenId)}</td>
      <td>${element.CreatedDateF}</td>
      <td>${element.Conclusion}</td>
      <td>${element.Name}</td>
      <td colspan="2"></td>
      <td><!-- Botones de opciones-->
        <div class="dropdown float-right dropleft">
          <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
            <img src="images/iconos/menu_responsive.svg" alt="">
          </div>
          <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
              <a class="dropdown-item" onclick="editValueEvaluacionEspecialista(${index})">
                <img src="./images/sho/edit.svg">&nbsp;&nbsp;
                Editar
              </a>
              <a class="dropdown-item" onclick="deleteEvaluacionEspecialistas(${index})">
                <img src="images/sho/delete-trash.png" alt="">&nbsp;&nbsp;
                Eliminar
              </a>
              <a class="dropdown-item ${evaluacionIsCreated(element.Id)}">
                <img src="images/sho/upload-icongreen.png" alt="">&nbsp;&nbsp;
                <label for="file_adjunto_examen_especialistas-${index}">Adjuntar documento</label>
                <input
                  type="file"
                  class="hidden"
                  id="file_adjunto_examen_especialistas-${index}"
                  accept="application/pdf, .pdf, .doc, docx, .odf"
                  onchange="loadAdjuntoExamenesEspecialistas(this,${
                    element.Id
                  },${index})"
                />
              </a>
              <a class="dropdown-item ${evaluacionIsCreated(
                element.Id
              )} ${adjIsLoad(
        element.Name
      )}" onclick="downloadAdjExamenEspecialista(${element.Id})">
                <img src="./images/sho/download-green.png" alt="">&nbsp;&nbsp;
                Descargar documento 
              </a>
          </div>
      </td>
    </tr>`;
    }
  );
  if (window.saludOcupacional.EvaluacionesEspecialistas.length < 1) {
    content +=
      '<tr><td colspan="7" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById("content_table_evaluacion_especialistas").innerHTML =
    content;
  document.getElementById("cant_examen_evaluea_especialista_head").innerHTML =
    window.saludOcupacional.EvaluacionesEspecialistas.length;
}

function deleteEvaluacionEspecialistas(index) {
  window.saludOcupacional.EvaluacionesEspecialistas[index].Deleted = 1;
  window.arrayBorrado.EvaluacionEspecialistas.push(
    window.saludOcupacional.EvaluacionesEspecialistas[index]
  );
  window.saludOcupacional.EvaluacionesEspecialistas.splice(index, 1);
  drawEvaluacionEspecialistas();
}

function evaluacionIsCreated(id) {
  if (id == 0) {
    return "hidden";
  }
}

function addNameToAdjunto(index, name) {
  window.saludOcupacional.EvaluacionesEspecialistas[index].Name = name;
  drawEvaluacionEspecialistas();
}

function adjIsLoad(name) {
  if (name == " ---- ") {
    return "hidden";
  }
}

//añade para editar el dato seleccionado
function editValueEvaluacionEspecialista(index) {
  document.getElementById("index_array_examen_especialista").value = index;
  document.getElementById("tipo_examen_eval_id").value =
    window.saludOcupacional.EvaluacionesEspecialistas[index].TipoExamenId;
  document.getElementById("conclusion_examen_eval_id").value =
    window.saludOcupacional.EvaluacionesEspecialistas[index].Conclusion;
  document.getElementById("date_examen_eval_id").value = setInputDate(
    window.saludOcupacional.EvaluacionesEspecialistas[index].ExamenDateF,
    "latin"
  );
  //seleccionar en el select
  selectedInSelectUpdate(
    "tipo_examen_eval_id",
    window.saludOcupacional.EvaluacionesEspecialistas[index].TipoExamenId
  );
}

function updateValueEvaluacionEspecialista() {
  let index = document.getElementById("index_array_examen_especialista").value;
  let fechaCreacion = moment(
    document.getElementById("date_examen_eval_id").value
  );
  //actualiza el tipo examen id
  window.saludOcupacional.EvaluacionesEspecialistas[index].TipoExamenId =
    document.getElementById("tipo_examen_eval_id").value;
  //actualiza el texto de conclusion
  window.saludOcupacional.EvaluacionesEspecialistas[index].Conclusion =
    document.getElementById("conclusion_examen_eval_id").value;
  //actualiza la fecha del examen
  window.saludOcupacional.EvaluacionesEspecialistas[index].ExamenDateF =
    fechaCreacion.format("DD/MM/YYYY");
  //limpia el index
  document.getElementById("index_array_examen_especialista").value = "";
}
/**
 * @var botones de seleccion
 * funcionalidades para botones de selección
 * ------------------------------------------
 */

//examenes descanso Tab 1
document.querySelectorAll(".radio-lesion-descanso").forEach((element) => {
  element.addEventListener("click", (e) => {
    clearSelectedRadio(".radio-lesion-descanso");
    window.saludOcupacional.descanso1 = element.getAttribute("valor");
    //verifica si se verá
    quitarSeleccionParaMostrar(
      "box-descansos-medicos-tabla-tab-1",
      element.getAttribute("valor")
    );
    if (parseInt(element.getAttribute("valor")) == 1) {
      window.saludOcupacional.FlagDescansosMedicos = 1;
      element.classList.add("c-btn--green");
    } else if (parseInt(element.getAttribute("valor")) == 0) {
      window.saludOcupacional.FlagDescansosMedicos = 0;
      element.classList.add("btn-danger");
    }
  });
});

document.querySelectorAll(".radio-lesion-descanso-2").forEach((element) => {
  element.addEventListener("click", (e) => {
    clearSelectedRadio(".radio-lesion-descanso-2");
    window.saludOcupacional.descanso2 = element.getAttribute("valor");
    //verifica si se verá
    quitarSeleccionParaMostrar(
      "box-descanso-medico-lesion2-tab-1",
      element.getAttribute("valor")
    );
    if (parseInt(element.getAttribute("valor")) == 1) {
      element.classList.add("c-btn--green");
    } else if (parseInt(element.getAttribute("valor")) == 0) {
      element.classList.add("btn-danger");
    }
  });
});

//descansos examen Tab 2
document.querySelectorAll(".radio-examen-auxiliar").forEach((element) => {
  element.addEventListener("click", (e) => {
    clearSelectedRadio(".radio-examen-auxiliar");
    window.saludOcupacional.FlagExamenAuxiliar = element.getAttribute("valor");
    //verifica si se verá
    quitarSeleccionParaMostrar(
      "box-examen-auxiliares-tab-2",
      element.getAttribute("valor")
    );
    if (parseInt(element.getAttribute("valor")) == 1) {
      element.classList.add("c-btn--green");
    } else if (parseInt(element.getAttribute("valor")) == 0) {
      element.classList.add("btn-danger");
    }
  });
});

//evento no habitual lesion Tab 1
document
  .querySelectorAll(".radio-evento-no-habitual-lesion")
  .forEach((element) => {
    element.addEventListener("click", (e) => {
      clearSelectedRadio(".radio-evento-no-habitual-lesion");
      window.saludOcupacional.eventoNoHabitual = element.getAttribute("valor");
      //verifica si se verá
      quitarSeleccionParaMostrar(
        "box-evento-genero-lesion",
        element.getAttribute("valor")
      );
      if (parseInt(element.getAttribute("valor")) == 1) {
        window.saludOcupacional.FlagLesion = 1;
        element.classList.add("c-btn--green");
      } else if (parseInt(element.getAttribute("valor")) == 0) {
        window.saludOcupacional.FlagLesion = 0;
        element.classList.add("btn-danger");
      }
    });
  });
//evento establecer justificacion de la enfermedad ocupacional
document
  .querySelectorAll(".radio-evento-establecimiento-justificacion")
  .forEach((element) => {
    element.addEventListener("click", (e) => {
      clearSelectedRadio(".radio-evento-establecimiento-justificacion");
      window.saludOcupacional.FlagJustificacion = parseInt(
        element.getAttribute("valor")
      );
      //verifica si se verá
      quitarSeleccionParaMostrar(
        "tab5_conclusion_justificacion_enfermedad",
        element.getAttribute("valor")
      );
      if (parseInt(element.getAttribute("valor")) == 1) {
        window.saludOcupacional.FlagDescansosMedicos = 1;
        element.classList.add("c-btn--green");
      } else if (parseInt(element.getAttribute("valor")) == 0) {
        window.saludOcupacional.FlagDescansosMedicos = 0;
        element.classList.add("btn-danger");
      }
    });
  });

// sintomatologia de lesiones
document.querySelectorAll(".radio-sintomatologia-sp").forEach((element) => {
  element.addEventListener("click", (e) => {
    clearSelectedRadio(".radio-sintomatologia-sp");
    window.saludOcupacional.FlagSintomatologia = element.getAttribute("valor");
    //verifica si se verá
    quitarSeleccionParaMostrar(
      "box-sintomatologia-lesion-tab-1",
      element.getAttribute("valor")
    );
    if (parseInt(element.getAttribute("valor")) == 1) {
      element.classList.add("c-btn--green");
    } else if (parseInt(element.getAttribute("valor")) == 0) {
      element.classList.add("btn-danger");
    }
  });
});

//limpia la seleccion del boton
function clearSelectedRadio(path) {
  document.querySelectorAll(path).forEach((element) => {
    let classNames = Object.values(element.classList);
    let btnDanger = classNames.filter((element) => "btn-danger" == element);
    let btnSuccess = classNames.filter((element) => "c-btn--green" == element);
    if (btnDanger.length > 0) {
      element.classList.remove("btn-danger");
    } else if (btnSuccess.length > 0) {
      element.classList.remove("c-btn--green");
    }
  });
}

//quita la seleccion
function quitarSeleccionParaMostrar(path, value) {
  if (parseInt(value) == 1) {
    document.getElementById(path).classList.remove("hidden");
  } else if (parseInt(value) == 0) {
    document.getElementById(path).classList.add("hidden");
  }
}

/**
 * Edward Romero
 * seccion de diagnostico
 */
function addDiagnosticoCIE() {
  const tiempoTranscurrido = Date.now();
  const hoy = new Date(tiempoTranscurrido);
  let diagnostico = {
    Id: 0,
    TipoCie10Id: document.getElementById("dat_am_cie_diagnostico2").value,
    IdTipoCIE: document.getElementById("dat_am_cie_diagnostico1").value,
    SistemaAfectadoId: parseInt(
      document.getElementById("dat_am_sistema_diagnostico").value
    ),
    SeccionAfectadaId: parseInt(
      document.getElementById("dat_am_seccion_diagnostico").value
    ),
    names: {
      sistema: document.getElementById("dat_am_sistema_diagnostico").options[
        document.getElementById("dat_am_sistema_diagnostico").selectedIndex
      ].text,
      section: document.getElementById("dat_am_seccion_diagnostico").options[
        document.getElementById("dat_am_seccion_diagnostico").selectedIndex
      ].text,
      diagnostico: document.getElementById("dat_am_diagnostico_diagnostico")
        .value,
    },
    StatusDiagnostico: "",
    fechaCreacion: hoy.toLocaleDateString(),
    Deleted: 0,
  };
  document.getElementById("dat_am_cie_diagnostico2").value = "";
  document.getElementById("dat_am_diagnostico_diagnostico").value = "";

  window.saludOcupacional.diagnosticos.push(diagnostico);
  //pintar los datos en las tablas correspondientes
  drawDiagnosticoCIE10();
  drawDiagnosticosCIEenConclusiones();
}

function drawDiagnosticoCIE10() {
  let contentTableRow = "";
  window.saludOcupacional.diagnosticos.forEach((element, index) => {
    contentTableRow += `
          <tr>
            <td>${element.names.diagnostico}</td>
            <td>${mostrarCodigoCIE10(element.IdTipoCIE)}</td>
            <td>${element.fechaCreacion}</td>
            <td>especialidad 1</td>
            <td>${element.names.sistema}</td>
            <td>${element.names.section}</td>
            <td>
              <div class="d-flex justify-content-end">
                <a href="javascript:void(0)" class="btn-delete" onclick="deleteDiagnosticoCIE10(${index})">
                  <img src="images/sho/delete-trash.png" style="width: 15px">
                </a>  
              </div>   
            </td>
          </tr>`;
  });

  if (window.saludOcupacional.diagnosticos.length == 0) {
    contentTableRow =
      "<tr><td colspan='7' class='text-center'>Sin diagnostico registrado</td></tr>";
  }

  document.getElementById("total-diagnosticos-table-tab2").innerHTML =
    window.saludOcupacional.diagnosticos.length;
  document.getElementById("content_table_diagnosticos_cie").innerHTML =
    contentTableRow;
}

//buscar el código del tipo CIE10
function mostrarCodigoCIE10(codigo){
    let descriptionReturn = '';
    window.CEI10.forEach(element=>{
        if(element.Id == codigo ){
            descriptionReturn = element.Code;
        }
    });
    return descriptionReturn;
}

function deleteDiagnosticoCIE10(index) {
  window.saludOcupacional.diagnosticos[index].Deleted = 1;
  window.arrayBorrado.diagnosticos.push(
    window.saludOcupacional.diagnosticos[index]
  );
  window.saludOcupacional.diagnosticos.splice(index, 1);
  drawDiagnosticoCIE10();
  drawDiagnosticosCIEenConclusiones();
}

function preparateDiagnosticoCIE10TODSO() {
  let diagnosticos = [];
  let diagnosticoBorrado = [];
  window.saludOcupacional.diagnosticos.forEach((element) => {
    let filterDiagn = {
      Id: element.Id,
      TipoCie10Id: parseInt(element.IdTipoCIE),
      SistemaAfectadoId: element.SistemaAfectadoId,
      SeccionAfectadaId: element.SeccionAfectadaId,
      StatusDiagnostico: element.StatusDiagnostico,
      Deleted: 0,
    };
    diagnosticos.push(filterDiagn);
  });
  //agregar el array borrar
  window.arrayBorrado.diagnosticos.forEach((element) => {
    let valor = {
      Id: element.Id,
      TipoCie10Id: parseInt(element.IdTipoCIE),
      SistemaAfectadoId: element.SistemaAfectadoId,
      SeccionAfectadaId: element.SeccionAfectadaId,
      StatusDiagnostico: element.StatusDiagnostico,
      Deleted: element.Deleted,
    };
    diagnosticoBorrado.push(valor);
  });
  cargarArray(diagnosticoBorrado, diagnosticos);
  return diagnosticos;
}

/**
 * Tab 5 conclusiones
 * ____________________
 * CONCLUSIONES
 */
function drawDiagnosticosCIEenConclusiones() {
  let contentTableRow = "";
  let selectedBtn = [];
  window.saludOcupacional.diagnosticos.forEach((element, index) => {
    contentTableRow += `
          <tr>
            <td><div class="w-200 text-wrap">${mostrarCodigoCIE10(element.IdTipoCIE)}</div></td>
            <td><div class="w-200 text-wrap">${
              element.names.diagnostico
            }</div></td>
            <td>${element.names.sistema}</td>
            <td>${element.names.section}</td>
            <td>
              <div>
              <div class="d-flex justify-content-between" style="width:270px">
                <div>
                  <button onclick="confirmDiagnosticoConclusion(${index})" class="btn ${
      element.StatusDiagnostico == "CO"
        ? selectedBtn.push({
            status: "CO",
            path: "confirm-diagnostico-btn" + index,
          })
        : ""
    } btn-block btn-lg" id="confirm-diagnostico-btn${index}"> <img class="hidden img-hide-diag" src="./images/sho/confirm.svg" alt="" /> <img src="./images/sho/check-negro.svg" alt="" class="check-diag" /> Confirmar</button>
                </div>
                <div>
                  <button onclick="destacarDiagnosticoConclusion(${index})" class="btn ${
      element.StatusDiagnostico == "DE"
        ? selectedBtn.push({
            status: "DE",
            path: "negar-diagnositico-btn" + index,
          })
        : ""
    } btn-block btn-lg" id="negar-diagnositico-btn${index}"> <img class="hidden img-hide-diag" src="./images/sho/cancelar.svg" alt="" /> <img class="check-diag" src="./images/sho/check-negro.svg" alt="" /> Descartar</button>
                </div>
              </div> 
              </div> 
            </td>
          </tr>`;
  });

  if (window.saludOcupacional.diagnosticos.length == 0) {
    contentTableRow =
      "<tr><td colspan='6' class='text-center'>Sin diagnostico registrado</td></tr>";
  }

  document.getElementById("cant_diagnostico_confirmar_tab5").innerHTML =
    window.saludOcupacional.diagnosticos.length;
  document.getElementById(
    "content_diagnosticos_table_confirmar_tab5"
  ).innerHTML = contentTableRow;

  //seleccionar en pre-carga
  if (selectedBtn.length > 0) {
    selectedBtn.forEach((element) => {
      selectButtonConfirm(element.path, element.status);
    });
  }
}

function confirmDiagnosticoConclusion(index) {
  window.saludOcupacional.diagnosticos[index].StatusDiagnostico = "CO";
  clearSeleccionConfirm(index);
  selectButtonConfirm("confirm-diagnostico-btn" + index, "CO");
}

function destacarDiagnosticoConclusion(index) {
  window.saludOcupacional.diagnosticos[index].StatusDiagnostico = "DE";
  clearSeleccionConfirm(index);
  selectButtonConfirm("negar-diagnositico-btn" + index, "DE");
}

function selectButtonConfirm(path, status) {
  console.log(path + " status:" + status);
  let buttonOn = document.querySelector("#" + path);
  let buttonHidden = document.querySelector("#" + path + " .img-hide-diag");
  let buttonNotSelected = document.querySelector("#" + path + " .check-diag");
  //funciones ayuda
  function clearBtn() {
    buttonHidden.classList.add("hidden");
    buttonNotSelected.classList.remove("hidden");
  }
  function markSelected() {
    buttonHidden.classList.remove("hidden");
    buttonNotSelected.classList.add("hidden");
  }
  //limpiando seleccion
  clearBtn();
  if (status == "CO") {
    buttonOn.classList.add("btn-success");
    markSelected();
  } else if (status == "DE") {
    buttonOn.classList.add("btn-danger");
    markSelected();
  }
}

function clearSeleccionConfirm(index) {
  //desde confirmar
  let buttonConfirmar = document.querySelector(
    "#confirm-diagnostico-btn" + index
  );
  let iconHideConfirmar = document.querySelector(
    "#confirm-diagnostico-btn" + index + " .img-hide-diag"
  );
  let iconHideNotSelect = document.querySelector(
    "#confirm-diagnostico-btn" + index + " .check-diag"
  );
  //desde destacar
  let buttonDestacar = document.querySelector(
    "#negar-diagnositico-btn" + index
  );
  let iconHideDestacar = document.querySelector(
    "#negar-diagnositico-btn" + index + " .img-hide-diag"
  );
  let iconHideNotSelectDestacar = document.querySelector(
    "#negar-diagnositico-btn" + index + " .check-diag"
  );
  //limpiar confirmar
  buttonConfirmar.classList.remove("btn-success");
  iconHideConfirmar.classList.add("hidden");
  iconHideNotSelect.classList.remove("hidden");
  //limpiar destacar
  buttonDestacar.classList.remove("btn-danger");
  iconHideDestacar.classList.add("remove");
  iconHideNotSelectDestacar.classList.remove("hidden");
}

function setDiagnosticosJsonPreload(datos) {
  const tiempoTranscurrido = Date.now();
  const hoy = new Date(tiempoTranscurrido);
  window.saludOcupacional.diagnosticos = [];
  datos.forEach((element) => {
    let diagnostico = {
      Id: element.Id,
      TipoCie10Id: element.TipoCie,
      IdTipoCIE: element.TipoCie10Id,
      SistemaAfectadoId: element.SistemaAfectadoId,
      SeccionAfectadaId: element.SeccionAfectadaId,
      names: {
        sistema: element.SistemaAfectado,
        section: element.SeccionAfectada,
        diagnostico: element.TipoCie,
      },
      StatusDiagnostico: element.StatusDiagnostico,
      fechaCreacion: hoy.toLocaleDateString(),
      Deleted: 0,
    };
    window.saludOcupacional.diagnosticos.push(diagnostico);
  });
  drawDiagnosticoCIE10();
  drawDiagnosticosCIEenConclusiones();
}

//medicamentos conclusiones
function addMedicamentoInsumoConclusion() {
  let medicamento = {
    Id: 0,
    CoberturaId: parseInt(document.getElementById("inpu_tab5_cobertura").value),
    MedicamentoId: parseInt(
      document.getElementById("inpu_tab5_medicamento_seleccionado_id").value
    ),
    Presentacion: document.getElementById("inpu_tab5_presentación").value,
    DosisRequerida: document.getElementById("inpu_tab5_dosis").value,
    DosisDSO: document.getElementById("inpu_tab5_dosis_DSO").value,
    Frecuencia: document.getElementById("inpu_tab5_frecuencia").value,
    TiempoUso: document.getElementById("inpu_tab5_tiempo_uso").value,
    FlagReceta: getFlagReceta(),
    Deleted: 0,
  };

  document.getElementById("inpu_tab5_presentación").value = "";
  document.getElementById("inpu_tab5_dosis").value = "";
  document.getElementById("inpu_tab5_dosis_DSO").value = "";
  document.getElementById("inpu_tab5_frecuencia").value = "";
  document.getElementById("emiv_receta_requi_si").value = "";
  document.getElementById("emiv_receta_requi_no").value = "";
  document.getElementById("inpu_tab5_tiempo_uso").value = "";
  document.getElementById("inpu_tab5_busca_medicamento").value = "";
  //agregar al dom
  window.saludOcupacional.Medicamentos.push(medicamento);
  drawMedicamentosInsumoConclusioin();
}

function deleteMedicamentoInsumoConclusion(index) {
  window.saludOcupacional.Medicamentos.splice(index, 1);
  drawMedicamentosInsumoConclusioin();
}

function drawMedicamentosInsumoConclusioin() {
  let contenido = "";
  window.saludOcupacional.Medicamentos.forEach((element, index) => {
    contenido += `
        <tr>
          <td>${returnNombreCobertura(element.CoberturaId)}</td>
          <td>${returnNombreMedicamento(element.MedicamentoId)}</td>
          <td>${element.Presentacion}</td>
          <td>${element.DosisRequerida}</td>
          <td>${element.DosisDSO}</td>
          <td>${element.Frecuencia}</td>
          <td>${element.TiempoUso}</td>
          <td>${convertirInTextSiNo(element.FlagReceta)}</td>
          <td></td>
          <td>
            <div class="d-flex justify-content-end">
              <a href="javascript:void(0)" class="btn-delete" onclick="deleteMedicamentoInsumoConclusion(${index})">
                <img src="images/sho/delete-trash.png" style="width: 15px">
              </a>  
            </div>  
          </td>
        </tr>`;
  });

  if (window.saludOcupacional.Medicamentos.length == 0) {
    contenido += `
      <tr>
        <td class="text-center" colspan="10">Sin registro de medicamentos</td>
      </tr>`;
  }
  document.getElementById("content_table_medicamentos_conclusion").innerHTML =
    contenido;
  document.getElementById("cant_medicamentos_conclusion").innerHTML =
    window.saludOcupacional.Medicamentos.length;
}

function activarDosisAdminDSO() {
  if (parseInt(document.getElementById("inpu_tab5_cobertura").value) == 2) {
    document.getElementById("inpu_tab5_dosis_DSO").removeAttribute("disabled");
    document
      .getElementById("div__inpu_tab5_dosis")
      .classList.remove("form-input--disabled");
  } else {
    document
      .getElementById("inpu_tab5_dosis_DSO")
      .setAttribute("disabled", true);
    document
      .getElementById("div__inpu_tab5_dosis")
      .classList.add("form-input--disabled");
  }
}

//retorna el nombre de la cobertura
function returnNombreCobertura(id) {
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );
  let coberturaResult = "";
  enfermedadStorage.ListadoCoberturas.forEach((element) => {
    if (element.Id == id) {
      coberturaResult = element.Cobertura;
    }
  });
  return coberturaResult;
}

// retorna el nombre del medicamento por id
function returnNombreMedicamento(id) {
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );
  let coberturaResult = "";
  enfermedadStorage.ListadoMedicamentos.forEach((element) => {
    if (element.Id == id) {
      coberturaResult = element.Medicamento;
    }
  });
  return coberturaResult;
}

//logica del formulario para medicamentos
function searchPresentacionMedicamento() {
  let id = parseInt(
    document.getElementById("inpu_tab5_medicamento_seleccionado_id").value
  );

  let url =
    apiUrlsho +
    "/api/hce_Get_051_medicamento_presentacion?code=rQLd0e2U1sRntipTG4t2fH8/knnILRcWnT0QXwkLPgAXgnPr97Xk8Q==";

  let headers = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json",
  };

  let settings = {
    url: url,
    method: "get",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers,
    data: { IdMedicamento: id },
  };

  $.ajax(settings).done((response) => {
    drawSelectPresentacionMedicamento(response.MedicamentoPresentacion);
  });
}

function drawSelectPresentacionMedicamento(presentaciones) {
  let options = "";
  presentaciones.forEach((element) => {
    options += `<option value="${element.Descripcion}">${element.Descripcion}</option>`;
  });
  document.getElementById("inpu_tab5_presentación").innerHTML = options;
}

function drawSelectCoberturas(coberturas) {
  let optionSelect = "";
  coberturas.forEach((element) => {
    optionSelect += `<option value="${element.Id}">${element.Cobertura}</option>`;
  });
  document.getElementById("inpu_tab5_cobertura").innerHTML = optionSelect;
}

function drawResultadosMedicamentos() {
  let medicamentos = resultBusquedaMedicamentos();
  let listaResult = "";
  //verifica que tenga texto
  if (document.getElementById("inpu_tab5_busca_medicamento").value != "") {
    document
      .getElementById("dropdown_resultados_medicamentos")
      .classList.remove("hidden");
  } else {
    document
      .getElementById("dropdown_resultados_medicamentos")
      .classList.add("hidden");
  }
  //pinta los resultados
  medicamentos.forEach((element) => {
    listaResult += `<li onclick="selectMedicamentoList(${element.Id},'${element.Medicamento}')">${element.Medicamento}</li>`;
  });

  document.getElementById("list_resultados_medicamentos").innerHTML =
    listaResult;
  document.getElementById("cant_dropdown_resultados_medicamentos").innerHTML =
    medicamentos.length;
}

//obtiene los resultados de la busqueda de medicamentos
function resultBusquedaMedicamentos() {
  let medicamento = document.getElementById(
    "inpu_tab5_busca_medicamento"
  ).value;
  console.log("buscando medicamento: " + medicamento);
  let result = [];
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );
  result = enfermedadStorage.ListadoMedicamentos.filter(
    (element) =>
      element.Medicamento.toLowerCase().indexOf(medicamento.toLowerCase()) !==
      -1
  );
  return result;
}

function selectMedicamentoList(id, medicamento) {
  document
    .getElementById("dropdown_resultados_medicamentos")
    .classList.add("hidden");
  document.getElementById("inpu_tab5_busca_medicamento").value = medicamento;
  document.getElementById("inpu_tab5_medicamento_seleccionado_id").value = id;
  searchPresentacionMedicamento();
}

/**
 * primer tab
 * Sección descansos medicos
 * @param {array} descansos
 */
function drawDescansosMedicos(paginate) {
  //funcion para mostrar el dato formateado acerca del estado del descanso
  function showOpenToClose(estado) {
    return estado == 1 ? "Cerrado" : "Abierto";
  }
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );
  let pagineInit = 5 * paginate - 5;
  let pagineEnd = 5 * paginate;
  let descansos = enfermedadStorage.DescansosMedicos;
  descansos = descansos.slice(pagineInit, pagineEnd);
  //logica principal de la funcion
  let contentDescansosRow = "";
  descansos.forEach((element) => {
    contentDescansosRow += `
        <tr>
          <td>${element.A_IdDescansoMedico}</td>
          <td>${element.A_DniTrabajador}</td>
          <td>${element.A_FechaRegistroF}</td>
          <td>${element.OrigenDescanso}</td>
          <td>${element.A_IdDescansoMedico}</td>
          <td>${element.FechaInicioF}</td>
          <td>${element.FechaFinF}</td>
          <td>${element.B_CantidadDias}</td>
          <td>${element.B_DiasAcumulados}</td>
          <td>${showOpenToClose(element.Estado)}</td>
          <td>
            <input type="checkbox" onclick="addDescansosMedicosToArray(${
              element.A_IdDescansoMedico
            })" name="selectDescansos" value="${element.A_IdDescansoMedico}">
          </td>
        </tr>`;
  });
  if (descansos.length == 0) {
    contentDescansosRow =
      '<tr><td colspan="10" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById("content_descansos_medicos").innerHTML =
    contentDescansosRow;
  document.getElementById("cant_descan_medicos").innerHTML =
    enfermedadStorage.DescansosMedicos.length;
}

function drawAccidentesTrabajo(paginate) {
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );
  let pagineInit = 5 * paginate - 5;
  let pagineEnd = 5 * paginate;
  let contentAccidentesRow = "";
  let accidentes = enfermedadStorage.AccidentesTrabajo;
  accidentes = accidentes.slice(pagineInit, pagineEnd);
  accidentes.forEach((element) => {
    contentAccidentesRow += `
        <tr>
          <td>${element.A_Fecha_AccidenteF}</td>
          <td>${element.A_Hora_Accidente}</td>
          <td>Tipo ${element.A_Estado_Accidente}</td>
          <td>${element.EstadoAccidente}</td>
          <td colspan="3"></td>
          <td>
            <input type="checkbox">
          </td>
        </tr>`;
  });
  if (accidentes.length == 0) {
    contentAccidentesRow =
      '<tr><td colspan="6" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById("cant_accidente_trabajo").innerHTML =
    enfermedadStorage.AccidentesTrabajo.length;
  document.getElementById("content_Accidentes_medicos").innerHTML =
    contentAccidentesRow;
}

/**
 * Primer tab del formulario
 * sección de dibujar y manipular data antecedentes
 */
function drawAntecedentesOcupacionales(antecedentesOcupacionales) {
  let contentTableRow = "";
  antecedentesOcupacionales.forEach((element) => {
    contentTableRow += `<tr>
              <td>${element.FechaInicioF}</td>
              <td>${element.FechaFinF}</td>
              <td>${element.Empresa}</td>
              <td>${element.ActividadEmpresa}</td>
              <td>${element.AreaTrabajo}</td>
              <td>${element.Ocupacion}</td>
              <td>${element.PeligrosAgentesOcupacionales}</td>
              <td>${element.UsoEpp_TipoEpp}</td>
              <td>-------------</th>
            </tr>`;
  });
  if (antecedentesOcupacionales.length < 0) {
    contentTableRow += `<tr>
          <td colspan="9">Sin registros</td>
        </tr>`;
  }
  document.getElementById("cant_antocupacionales_header").innerHTML =
    antecedentesOcupacionales.length;
  document.getElementById("cant_enfe_ocupacionales_collapse").innerHTML =
    antecedentesOcupacionales.length;
  document.getElementById("content_ant_ocupacionales_table").innerHTML =
    contentTableRow;
}

function drawAntecedentesAlergias(alergias) {
  let contentAlergiasRow = "";
  alergias.forEach((element) => {
    contentAlergiasRow += `
            <div class="row antecedentes-list" style="border-bottom: 1px solid lightgray;">
              <div class="col-md-5 col-sm-6 col-12">
                <h5>Nombre de la alergia: <span class="respuesta">${element.A_NombreAntecedente}</span></h5>
              </div>
              <div class="col-md-7 col-sm-6 col-12">
                <h5>Creación: <span class="respuesta">${element.A_FechaCreacionF}</span></h5>
              </div>
            </div>`;
  });
  if (alergias.length < 0) {
    contentAlergiasRow += `
        <div class="row antecedentes-list text-center" style="border-bottom: 1px solid lightgray;">
          Sin registro
        </div>`;
  }
  document.getElementById("cant_ant_alergias_header").innerHTML =
    alergias.length;
  document.getElementById("content_ant_alergias").innerHTML =
    contentAlergiasRow;
}

function drawAntecedentesPatologicos(patologicos) {
  let contentTablePatologicosRow = "";
  patologicos.forEach((element) => {
    contentTablePatologicosRow += `
        <div class="row antecedentes-list" style="border-bottom: 1px solid lightgray;">
          <div class="col-md-5 col-sm-6 col-12">
            <h5>Nombre patología: <span class="respuesta">${element.A_NombreAntecedente}</span></h5>
          </div>
          <div class="col-md-7 col-sm-6 col-12">
            <h5>Creación: <span class="respuesta">${element.A_FechaCreacionF}</span></h5>
          </div>
        </div>`;
  });
  if (patologicos.length < 0) {
    contentTablePatologicosRow += `
        <div class="row antecedentes-list text-center" style="border-bottom: 1px solid lightgray;">
          Sin registro
        </div>`;
  }
  document.getElementById("content_ant_patologicos").innerHTML =
    contentTablePatologicosRow;
  document.getElementById("cant_ant_patologicos_header").innerHTML =
    patologicos.length;
}

function drawAntecedentesPatologicosFamiliares(patologicosFmlia) {
  let contentTablePatologicosRow = "";
  patologicosFmlia.forEach((element) => {
    contentTablePatologicosRow += `
        <div class="row antecedentes-list" style="border-bottom: 1px solid lightgray;">
          <div class="col-md-5 col-sm-6 col-12">
            <h5>Nombre patología: <span class="respuesta">${element.A_NombreAntecedente}</span></h5>
          </div>
          <div class="col-md-7 col-sm-6 col-12">
            <h5>Creación: <span class="respuesta">${element.A_FechaCreacionF}</span></h5>
          </div>
        </div>`;
  });
  if (patologicosFmlia.length < 0) {
    contentTablePatologicosRow += `
        <div class="row antecedentes-list text-center" style="border-bottom: 1px solid lightgray;">
          Sin registro
        </div>`;
  }
  document.getElementById("content_ant_patologicos_familiares").innerHTML =
    contentTablePatologicosRow;
  document.getElementById("cant_ant_patologicos_familiares_header").innerHTML =
    patologicosFmlia.length;
}

function drawAntecedentesUsoFarmacos(farmacos) {
  let contentFamacosRow = "";
  farmacos.forEach((element) => {
    contentFamacosRow += `
        <div class="row antecedentes-list" style="border-bottom: 1px solid lightgray;">
          <div class="col-md-5 col-sm-6 col-12">
            <h5>Nombre farmaco: <span class="respuesta">${element.A_NombreAntecedente}</span></h5>
          </div>
          <div class="col-md-7 col-sm-6 col-12">
            <h5>Creación: <span class="respuesta">${element.A_FechaCreacionF}</span></h5>
          </div>
        </div>`;
  });
  if (farmacos.length < 0) {
    contentFamacosRow += `
        <div class="row antecedentes-list text-center" style="border-bottom: 1px solid lightgray;">
          Sin registro
        </div>`;
  }
  document.getElementById("content_ant_farmacos").innerHTML = contentFamacosRow;
  document.getElementById("cant_ant_farmacos_header").innerHTML =
    farmacos.length;
}

function drawAntecedenteMalosHabitos(habitosToxicos) {
  let contentHabitosRow = "";
  habitosToxicos.forEach((element) => {
    contentHabitosRow += `
        <div class="row antecedentes-list" style="border-bottom: 1px solid lightgray;">
          <div class="col-md-5 col-sm-6 col-12">
            <h5>Nombre habito: <span class="respuesta">${element.A_NombreAntecedente}</span></h5>
          </div>
          <div class="col-md-7 col-sm-6 col-12">
            <h5>Creación: <span class="respuesta">${element.A_FechaCreacionF}</span></h5>
          </div>
        </div>`;
  });
  if (habitosToxicos.length < 0) {
    contentHabitosRow += `
        <div class="row antecedentes-list text-center" style="border-bottom: 1px solid lightgray;">
          Sin registro
        </div>`;
  }
  document.getElementById("content_ant_malos_habitos").innerHTML =
    contentHabitosRow;
  document.getElementById("cant_ant_malos_habitos_header").innerHTML =
    habitosToxicos.length;
}

/**
 * Edward Romero
 * tercer tab Formulario
 * ______________________
 * EVALUACOIN HIGIENE
 */
function drawInvestigacionPuestosTrabajo(paginate) {
  let contentPuestos = "";
  let pagineInit = 5 * paginate - 5;
  let pagineEnd = 5 * paginate;
  let data = returnDataPuestosTrabajo();
  data = data.slice(pagineInit, pagineEnd);
  data.forEach((element) => {
    contentPuestos += `
        <tr>
          <td>${element.CodigoEvaluacion}</td>
          <td>${element.CodigoMonitoreo}</td>
          <td>${element.CreadoFechaF}</td>
          <td>${element.Riesgo}</td>
          <td>${element.AgenteRiesgo}</td>
          <td>${element.TipoEvaluacion}</td>
          <td>Intensidad baja</td>
          <td>Nivel Medio</td>
          <td><!-- Botones de opciones-->
            <div class="dropdown float-right dropleft">
              <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
                <img src="images/iconos/menu_responsive.svg" alt="">
              </div>
              <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
                <ul>
                  <li >
                    <img src="./images/sho/ojo.svg">
                    <span>Ver detalle</span>
                  </li>
                </ul>
              </div>
            </td>
        </tr>`;
  });
  if (returnDataPuestosTrabajo() == 0) {
    contentPuestos +=
      '<tr><td colspan="9" class="text-center">Sin registro de evaluaciones de higiene</td></tr>';
  }

  document.getElementById(
    "box_tab3_evaluacion_puestos_trabajo_table"
  ).innerHTML = contentPuestos;
  document.getElementById("cant_tab3_evaluacion_puesto_trabajo").innerHTML =
    window.saludOcupacional.evaluacionHigiene.length;
}

function returnDataPuestosTrabajo() {
  return window.saludOcupacional.evaluacionHigiene;
}

/**
 *
 * @param {id} path el id donde esta la <ul></ul>
 * @param {array} data array de los datos a pintar, calcula cuanto será por vista, esta por defecto 5
 * @param {function} callback funcion para llamar despues de presionar el boton
 */
function paginacionDinamica(path, data, callback) {
  let lengthPage = Math.ceil(data.length / 5);
  let pageInit = 0;
  let pageEnd = pageInit + 6;
  let paginateStep = "";
  if (document.getElementById(path).getAttribute("pagina") != undefined) {
    pageInit = parseInt(document.getElementById(path).getAttribute("pagina"));
  }
  paginateStep = `
      <li class="page-items hidden" id="page-item--init">
        <a class="page-link" href="javascript:void(0)" aria-label="Previous" onclick="pintarSelectoresPaginacion('${path}','menos')">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>`;
  for (let i = 0; i < lengthPage; i++) {
    paginateStep += `<li class="page-item ${verificarPaginacion(
      pageInit,
      pageEnd,
      i
    )}" onclick="${callback}(${
      i + 1
    })"><a class="page-link" href="javascript:void(0)">${i + 1}</a></li>`;
  }
  paginateStep += `
    <li class="page-items ${
      lengthPage > 7 ? "" : "hidden"
    }" id="page-item--end">
      <a class="page-link" href="javascript:void(0)" aria-label="Next" onclick="pintarSelectoresPaginacion('${path}','mas')">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>`;
  document.getElementById(path).innerHTML = paginateStep;
  document.getElementById(path).setAttribute("total-paginas", lengthPage - 6);
}

function pintarSelectoresPaginacion(path, action) {
  let pageInit = parseInt(document.getElementById(path).getAttribute("pagina"));
  let totalPaginas = parseInt(
    document.getElementById(path).getAttribute("total-paginas")
  );
  let pageEnd = pageInit + 5;
  if (action == "mas") {
    pageInit++;
    pageEnd++;
  } else if (action == "menos") {
    pageInit--;
    pageEnd--;
  }
  document
    .querySelectorAll("#" + path + " .page-item")
    .forEach((element, index) => {
      if (index < pageInit || index > pageEnd) {
        element.classList.add("hidden");
      } else {
        element.classList.remove("hidden");
      }
    });
  //añade el boton flecha izquierdo o lo oculta
  if (pageInit == 0) {
    document
      .querySelector("#" + path + " #page-item--init")
      .classList.add("hidden");
  } else {
    document
      .querySelector("#" + path + " #page-item--init")
      .classList.remove("hidden");
  }
  //verificacion para flecha derecha
  if (document.querySelector("#" + path + " #page-item--end") != null) {
    if (totalPaginas == pageInit) {
      document
        .querySelector("#" + path + " #page-item--end")
        .classList.add("hidden");
    } else {
      document
        .querySelector("#" + path + " #page-item--end")
        .classList.remove("hidden");
    }
  }
  document.getElementById(path).setAttribute("pagina", pageInit);
}

function verificarPaginacion(init, end, contador) {
  if (contador < init || contador > end) {
    return "hidden";
  } else if (contador == 0) {
    return "";
  } else {
    return "";
  }
}
/**
 * 25-01-2022
 * Medidas preventivas, logica
 */
function drawMedidasPreventivas(paginate) {
  let pagineInit = 5 * paginate - 5;
  let pagineEnd = 5 * paginate;
  let data = window.saludOcupacional.MedidasPreventivas;
  data = data.slice(pagineInit, pagineEnd);
  let contentTable = "";
  data.forEach((element, index) => {
    contentTable += `
      <tr>
        <td>${element.CodeControl}</td>
        <td>${element.CodigoEvaluacion}</td>
        <td>${element.IdTareaMonitoreo}</td>
        <td>${element.ResponsableName}o</td>
        <td>${element.FechaAcuerdoF}</td>
        <td>${element.Status}</td>
        <td>${element.FechaCierreF}</td>
        <td>${element.TipoControl}</td>
        <td><div id="input_edit_comentario_medidas_${index}">${element.ComentarioEO}</div></td>
        <td></td>
        <td><!-- Botones de opciones-->
          <div class="dropdown float-right dropleft">
            <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
              <img src="images/iconos/menu_responsive.svg" alt="">
            </div>
            <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
              <ul>
                <li onclick="editComentarioMedida(${index},${element.Id})">
                  <img src="./images/sho/ojo.svg">
                  <span>Editar</span>
                </li>
              </ul>
            </div>
          </div>
        </td>
      </tr>`;
  });
  if (window.saludOcupacional.MedidasPreventivas.length < 0) {
    contentTable += `<tr><td colspan="11" class="text-center">Sin registro de medidas preventibas</td></tr>`;
  }
  document.getElementById("content_medidas_preventivas_table").innerHTML =
    contentTable;
  document.getElementById("cant_medidas_preventivas_it").innerHTML =
    window.saludOcupacional.MedidasPreventivas.length;
}

//editar comentario de medidas preventibas
function editComentarioMedida(index, id) {
  window.global.indexMedida = index;
  let inputElement = document.createElement("input");
  inputElement.id = "input_child_comentario_" + index;
  inputElement.setAttribute("valor", id);
  inputElement.value = document.getElementById(
    "input_edit_comentario_medidas_" + index
  ).innerHTML;
  document.getElementById("input_edit_comentario_medidas_" + index).innerHTML =
    "";
  document
    .getElementById("input_edit_comentario_medidas_" + index)
    .append(inputElement);
  document.addEventListener("keydown", updateComentarioMedidas);
}

//cambiar de input a div normal
function changeInputToNormal(index) {
  let nuevoValor = document.getElementById(
    "input_child_comentario_" + index
  ).value;
  document.getElementById("input_edit_comentario_medidas_" + index).innerHTML =
    nuevoValor;
}

//actualizar comentario en la base de datos por fetch
function updateComentarioMedidas(e) {
  if (e.keyCode == 13) {
    //las variables necesarias
    let comentario = document.getElementById(
      "input_child_comentario_" + window.global.indexMedida
    ).value;
    let idMedida = parseInt(
      document
        .getElementById("input_child_comentario_" + window.global.indexMedida)
        .getAttribute("valor")
    );
    var raw = JSON.stringify({
      Id: idMedida,
      Comentario: comentario,
    });
    //url para consumir
    var url =
      apiUrlsho +
      `/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=postMedidasPreventivas`;

    var myHeaders = new Headers();
    myHeaders.append("apikey", constantes.apiKey);
    myHeaders.append("Content-Type", "application/json");

    fetch(url, {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    })
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        //actualiza el registro en la variable global
        window.saludOcupacional.MedidasPreventivas.forEach((element, index) => {
          if (element.Id == idMedida) {
            window.saludOcupacional.MedidasPreventivas[index].ComentarioEO =
              comentario;
          }
        });
        changeInputToNormal(window.global.indexMedida);
      })
      .catch((error) => console.log("error", error));
  }
}

/**
 * Escucha el evento de guardado de la enfermedad traida por el id
 * generado o pasado para consultar y pinta todos los datos según corresponda
 * ______________________________
 * dibujar datos en el formulario
 */
window.addEventListener("loadEnfermedadStorage", (event) => {
  $("#regresar").show();
  getSistemaAfectadoDSO();
  getTipoInvestigacionGlobal();
  //cargar selects
  cargarSelect(
    "tipo_examen_eval_id",
    vw_main.getTiposExamenes(0),
    "Id",
    "TipoExamen"
  );
  //examen de DSO
  cargarSelect(
    "dat_dg_tipoexamen_euxiliares",
    vw_main.getTiposExamenes(0),
    "Id",
    "TipoExamen"
  );
  //cargando los selects correspondientes
  //datos cargados
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );

  getDataEmpleado(enfermedadStorage);
  drawSelectCoberturas(enfermedadStorage.ListadoCoberturas);
  document.getElementById("numero-caso-investigacion").innerHTML =
    enfermedadStorage.NroCaso;

  listPuestosTrabajo(enfermedadStorage.PuestoTrabajo);

  //carga los insumos en la respectiva tabla
  if (enfermedadStorage.Insumos.length > 0) {
    listInsumosCreate(enfermedadStorage.Insumos);
    window.saludOcupacional.insumo = enfermedadStorage.Insumos;
  }
  //carga los adjuntos en su respectiva tabla
  if (enfermedadStorage.Adjuntos.length > 0) {
    drawInfoAdjuntos(enfermedadStorage.Adjuntos);
  }
  /**
   * carga los antecedentes del trabajador
   * -------------------------------------
   */
  if (enfermedadStorage.AntecedentesHCE.Alergias.length > 0) {
    drawAntecedentesAlergias(enfermedadStorage.AntecedentesHCE.Alergias);
  }
  //carga antecedentes patologicos
  if (enfermedadStorage.AntecedentesHCE.AntecedentesPatologicos.length > 0) {
    drawAntecedentesPatologicos(
      enfermedadStorage.AntecedentesHCE.AntecedentesPatologicos
    );
  }
  //carga antecendentes patologicos familiraes
  if (
    enfermedadStorage.AntecedentesHCE.AntecedentesOcupacionalesFamiliares
      .length > 0
  ) {
    drawAntecedentesPatologicosFamiliares(
      enfermedadStorage.AntecedentesHCE.AntecedentesOcupacionalesFamiliares
    );
  }
  //carga antecedentes ocupacionales
  if (enfermedadStorage.AntecedentesHCE.AntecedentesOcupacionales.length > 0) {
    drawAntecedentesOcupacionales(
      enfermedadStorage.AntecedentesHCE.AntecedentesOcupacionales
    );
  }
  //carga antecedentes de uso de farmacos
  if (enfermedadStorage.AntecedentesHCE.Farmacos.length > 0) {
    drawAntecedentesUsoFarmacos(enfermedadStorage.AntecedentesHCE.Farmacos);
  }
  //carga antecendentes de malos habitos
  if (enfermedadStorage.AntecedentesHCE.HabitosNocivos.length > 0) {
    drawAntecedenteMalosHabitos(
      enfermedadStorage.AntecedentesHCE.HabitosNocivos
    );
  }
  //cargar los descansos medicos
  if (enfermedadStorage.DescansosMedicos.length > 0) {
    drawDescansosMedicos(1);
    //pinta la paginacion
    paginacionDinamica(
      "pagination_descansos_medicos_tab1",
      enfermedadStorage.DescansosMedicos,
      "drawDescansosMedicos"
    );
  }
  //cargar los accidentes
  if (enfermedadStorage.AccidentesTrabajo.length > 0) {
    drawAccidentesTrabajo(1);
    paginacionDinamica(
      "paginacion_lista_accidentes_trabajo_tab1",
      enfermedadStorage.AccidentesTrabajo,
      "drawAccidentesTrabajo"
    );
  }
  //carga la variable global para poder imprimir el pdf luego
  window.saludOcupacional.AntecedentesHCE = enfermedadStorage.AntecedentesHCE;

  /**
   * preparando el contenido de para el segundo tab
   * ______________________________________________
   * (EVALUACIÓN DSO)
   */
  // otras investigaciones
  if (enfermedadStorage.EvaluacionDSO.OtrasEvaluaciones.length > 0) {
    window.saludOcupacional.otrasInvestigaciones =
      enfermedadStorage.EvaluacionDSO.OtrasEvaluaciones;
  }

  //factores de riesgos
  if (enfermedadStorage.EvaluacionDSO.FactoresPersonales.length > 0) {
    window.saludOcupacional.factoresPersonales =
      enfermedadStorage.EvaluacionDSO.FactoresPersonales;
    drawFactoresRiesgos();
  }

  //examenes auxiliares
  if (enfermedadStorage.EvaluacionDSO.ExamenesAuxiliares.length > 0) {
    window.saludOcupacional.ExamenesAuxiliares =
      enfermedadStorage.EvaluacionDSO.ExamenesAuxiliares;
    drawExamenesAuxiliares();
  }

  //diagnosticos c10
  if (enfermedadStorage.EvaluacionDSO.Diagnosticos.length > 0) {
    setDiagnosticosJsonPreload(enfermedadStorage.EvaluacionDSO.Diagnosticos);
  }

  /**
   * preparando el contenido de para el tercer tab
   * ______________________________________________
   * (EVALUACION HIGIENE)
   */
  if (enfermedadStorage.EvaluacionHigiene.length > 0) {
    document
      .getElementById("box_paginacion_investigacion_puesto_table")
      .classList.remove("hidden");
    //logica fuerte
    window.saludOcupacional.evaluacionHigiene =
      enfermedadStorage.EvaluacionHigiene;
    drawInvestigacionPuestosTrabajo(1);
    paginacionDinamica(
      "paginacion_investigacion_puesto_table",
      enfermedadStorage.EvaluacionHigiene,
      "drawInvestigacionPuestosTrabajo"
    );
  }

  /**
   * preparando el contenido para el cuarto tab
   * ____________________________________________
   * EVALUACION ESPECIALISTAS
   */
  if (enfermedadStorage.EvaluacionDSO.EvaluacionesEspecialistas.length > 0) {
    window.saludOcupacional.EvaluacionesEspecialistas =
      enfermedadStorage.EvaluacionDSO.EvaluacionesEspecialistas;
    drawEvaluacionEspecialistas();
  }
  //llena los inputs con los values
  rellenarInputsInvestigacion(enfermedadStorage);

  /**
   * preparando el entorno para quinto tab
   * ______________________________________
   * CONCLUSIONES
   */
  if (enfermedadStorage.Medicamentos.length > 0) {
    window.saludOcupacional.Medicamentos = enfermedadStorage.Medicamentos;
  }
  drawMedicamentosInsumoConclusioin();

  /**
   * MEDIDAS PREVENTIVAS CARGA
   * cargar los datos
   */
  if (enfermedadStorage.MedidasPreventivas.length > 0) {
    window.saludOcupacional.MedidasPreventivas =
      enfermedadStorage.MedidasPreventivas;
    drawMedidasPreventivas(1);
    paginacionDinamica(
      "pagination_medidas_preventivas_un",
      enfermedadStorage.MedidasPreventivas,
      "drawMedidasPreventivas"
    );
  }
  window.scrollTo(0, 0);
  if (window.modeVistaInvestigacion) {
    verInModoLectura();
  }
});

//valida seleccion para edicion
function validaSeleccionBotonesSi(path, value) {
  document.querySelectorAll(path).forEach((element) => {
    if (parseInt(element.getAttribute("valor")) == value) {
      element.click();
    }
  });
}

function rellenarInputsInvestigacion(data) {
  /**
   * TAB 1
   * __________________________
   * DECLARACION DEL TRABAJADOR
   */
  selectedInSelectUpdate("turno_trabajo", data.TurnosCode);
  document.getElementById("turno_encuentra").value = data.DiaTurnoEncuentra;
  document.getElementById("activida_rutinaria_enfermedad").value =
    data.ActividadRutinariaPuesto;
  document.getElementById("relato_molestia_consulta").value =
    data.MotivoConsulta;
  document.getElementById("detalleDescansoMedico").value = data.DetalleLesion;
  document.getElementById("DetalleSintomatologia_id").value =
    data.DetalleSintomatologia;
  document.getElementById("activida_rutinaria_enfermedad").value =
    data.ActividadRutinariaPuesto;
  document.getElementById("nombre_empresa_trabajo").value = data.Empresa;
  document.getElementById(
    "direction_empresa_trabajo"
  ).value = `${data.Country}, ${data.City}`;
  document.getElementById("tipo_actividad_empresa").value = data.BusinessSector;
  document.getElementById("fecha_registro_enfermedad").value =
    data.CreatedDateF;
  document.getElementById("domicio_puesto").value =
    paObj_hc[idHC].a.Direccion_Trabajador_H;
  document.getElementById("celular_puesto").value =
    paObj_hc[idHC].a.Telefono_Trabajador_H;
  document.getElementById("numero_traba_puesto").value =
    data.CantidadTrabajadoresPuesto;
  document.getElementById("hora_registro_enfermedad").value =
    data.CreatedTime.slice(0, -11);
  validaSeleccionBotonesSi(".radio-lesion-descanso", data.FlagDescansosMedicos);
  validaSeleccionBotonesSi(".radio-lesion-descanso-2", data);
  validaSeleccionBotonesSi(".radio-evento-no-habitual-lesion", data.FlagLesion);
  //del tab 2
  validaSeleccionBotonesSi(".radio-examen-auxiliar", data.FlagExamenAuxiliar);
  validaSeleccionBotonesSi(".radio-sintomatologia-sp", data.FlagSintomatologia);

  /**
   * TAB 2
   * __________________________
   * EVALUACION DSO
   */
  //signos vitales
  document.getElementById("dat_dg_presion_arterial_sv").value =
    data.EvaluacionDSO.Sistole;
  document.getElementById("dat_dg_diastole_estado").value =
    data.EvaluacionDSO.Diastole;
  document.getElementById("dat_dg_frecuencia_cardiaca_sv").value =
    data.EvaluacionDSO.FrecuenciaCardiaca;
  document.getElementById("dat_dg_frecuencia_respiratoria_sv").value =
    data.EvaluacionDSO.FrecuenciaRespiratoria;
  document.getElementById("dat_dg_temperatura_sv").value =
    data.EvaluacionDSO.Temperatura;
  document.getElementById("dat_dg_peso_sv").value = data.EvaluacionDSO.Pesokg;
  document.getElementById("dat_dg_talla_sv").value = data.EvaluacionDSO.Talla;
  document.getElementById("dat_dg_saturacion_sv").value =
    data.EvaluacionDSO.Saturacion;
  document.getElementById("dat_dg_perimetro_abdominal_sv").value =
    data.EvaluacionDSO.PerimetroAbdominal;

  //Exámen físico - Estado General
  document.getElementById("dat_dg_estado_conciencia").value =
    data.EvaluacionDSO.EstadoConciencia;
  selectedInSelectUpdate(
    "dat_dg_estado_nutricion",
    data.EvaluacionDSO.EstadoNutricionId
  );
  selectedInSelectUpdate(
    "dat_dg_estado_general",
    data.EvaluacionDSO.EstadoGeneralId
  );
  selectedInSelectUpdate(
    "dat_dg_estado_Hidratacion",
    data.EvaluacionDSO.EstadoHidratacionId
  );
  document.getElementById("data_dg_orofaringe_estado").value =
    data.EvaluacionDSO.Orofaringe;
  document.getElementById("data_dg_torax_pulmones_estado").value =
    data.EvaluacionDSO.ToraxPulmones;
  document.getElementById("data_dg_cardio_vascular_estado").value =
    data.EvaluacionDSO.CardioVascular;
  document.getElementById("data_dg_soma_estado").value =
    data.EvaluacionDSO.Soma;
  document.getElementById("dat_dg_tratamiento_inicial").value =
    data.EvaluacionDSO.TratamientoInicial;

  calcularIndiceMasaCorporal();
  //llenando el select de sistemas afectados
  let contentSelectSistemaAfectado = "<option value='0'></option>";
  vw_main.getSistemasAfectados(0).forEach((element) => {
    contentSelectSistemaAfectado += `<option value="${element.Id}">${element.SistemaAfectado}</option>`;
  });
  document.getElementById("dat_am_sistema_diagnostico").innerHTML =
    contentSelectSistemaAfectado;
  /**
   * select seccion afectado
   * ____________________
   */
  let contentSeccionAfectada = "<option value='0'></option>";
  vw_main.getSeccionesAfectadas(0).forEach((element) => {
    contentSeccionAfectada += `<option value="${element.Id}">${element.SeccionAfectada}</option>`;
  });
  document.getElementById("dat_am_seccion_diagnostico").innerHTML =
    contentSeccionAfectada;
  /**
   * Tab 5
   * _________
   * CONCLUSIONES
   */
  document.getElementById("conclu_inp_aptitu_con").value =
    data.Conclusiones.AptitudId;
  //evalua la fecha que sea mayor a 1900
  if (
    Date.parse(data.Conclusiones.InicioRestrincion.slice(0, -9)) >
    Date.parse("1900-01-01")
  ) {
    document.getElementById("conclu_inp_fecha_inicio_rest_con").value =
      data.Conclusiones.InicioRestrincion.slice(0, -9);
    document.getElementById("conclu_inp_fecha_fin_rest_con").value =
      data.Conclusiones.FinRestrincion.slice(0, -9);
    document.getElementById("conclu_inp_fecha_reevaluacion_dso_con").value =
      data.Conclusiones.ReevaluacionDSO.slice(0, -9);
  }
  document.getElementById("conclu_inp_tipo_rest_con").value =
    data.Conclusiones.TipoRestrincion;
  document.getElementById("conclu_inp_personal_salud_con").value =
    data.Conclusiones.PersonalSalud;
  document.getElementById("conclu_inp_observaciones_con").value =
    data.Conclusiones.Observaciones;
  document.getElementById("tab5_conclusiones_justificacion_enf").value =
    data.EvaluacionDSO.Justificacion;
  validaSeleccionBotonesSi(
    ".radio-evento-establecimiento-justificacion",
    data.EvaluacionDSO.FlagJustificacion
  );
}

//evento para cargar tipos de investigaciones
window.addEventListener("loadTipoInvestigacion", () => {
  let optionTipoEval = "<option value=''></option>";
  window.TipoEvaluacion.forEach((element) => {
    optionTipoEval += `<option value="${element.Id}">${element.Descripcion}</option>`;
  });
  if (document.getElementById("dat_dg_tipo_investigacion_otro") != undefined) {
    document.getElementById("dat_dg_tipo_investigacion_otro").innerHTML =
      optionTipoEval;
    document.getElementById("dat_dg_tipo_investigacion_otro2").innerHTML =
      optionTipoEval;
  }

  drawOtrasInvestigacionesDSO();
});

/**
 *
 * desabihilitar los input y botones, para ver todo en modo lectura
 */
function verInModoLectura() {
  //tab 1 - Declaración del trabajador
  document.getElementById("turno_trabajo").setAttribute("disabled", true);
  document.getElementById("turno_encuentra").setAttribute("disabled", true);
  document
    .getElementById("file_adjunto_investigacion_evaluacion")
    .setAttribute("disabled", true);
  document
    .getElementById("activida_rutinaria_enfermedad")
    .setAttribute("disabled", true);
  document
    .getElementById("relato_molestia_consulta")
    .setAttribute("disabled", true);
  document.getElementById("insumos_materiales").setAttribute("disabled", true);
  document
    .getElementById("insumos_materiales__button")
    .setAttribute("disabled", true);
  document
    .getElementById("DetalleSintomatologia_id")
    .setAttribute("disabled", true);
  document
    .getElementById("detalleDescansoMedico")
    .setAttribute("disabled", true);
  //tab 2 - Evaluacion DSO
  document
    .getElementById("dat_dg_presion_arterial_sv")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_diastole_estado")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_temperatura_sv")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_saturacion_sv")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_frecuencia_cardiaca_sv")
    .setAttribute("disabled", true);
  document.getElementById("dat_dg_peso_sv").setAttribute("disabled", true);
  document
    .getElementById("dat_dg_frecuencia_respiratoria_sv")
    .setAttribute("disabled", true);
  document.getElementById("dat_dg_talla_sv").setAttribute("disabled", true);
  document
    .getElementById("dat_dg_perimetro_abdominal_sv")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_estado_conciencia")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_estado_nutricion")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_estado_general")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_estado_Hidratacion")
    .setAttribute("disabled", true);
  document
    .getElementById("data_dg_orofaringe_estado")
    .setAttribute("disabled", true);
  document
    .getElementById("data_dg_torax_pulmones_estado")
    .setAttribute("disabled", true);
  document
    .getElementById("data_dg_cardio_vascular_estado")
    .setAttribute("disabled", true);
  document.getElementById("data_dg_soma_estado").setAttribute("disabled", true);
  document
    .getElementById("dat_dg_tipo_investigacion_otro")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_estado_otra_investigacion")
    .setAttribute("disabled", true);
  document.getElementById("fn_hc_guardar").setAttribute("disabled", true);
  document
    .getElementById("dat_gd_detalle_factor_riesgo")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_gd_detalle_factor_riesgo__button")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_am_diagnostico_diagnostico")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_am_cie_diagnostico2")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_am_sistema_diagnostico")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_am_seccion_diagnostico")
    .setAttribute("disabled", true);
  document
    .getElementById("diagnostico_cie10__button")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_tratamiento_inicial")
    .setAttribute("disabled", true);
  // tab 4 - evaluación especialistas
  document.getElementById("date_examen_eval_id").setAttribute("disabled", true);
  document.getElementById("tipo_examen_eval_id").setAttribute("disabled", true);
  document
    .getElementById("conclusion_examen_eval_id")
    .setAttribute("disabled", true);
  document
    .getElementById("especialistas_examen_eval_id__button")
    .setAttribute("disabled", true);
  // tab 5 - conclusiones
  document
    .getElementById("tab5_conclusiones_justificacion_enf")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_tipo_investigacion_otro2")
    .setAttribute("disabled", true);
  document
    .getElementById("dat_dg_estado_otra_investigacion2")
    .setAttribute("disabled", true);
  document.getElementById("fn_hc_guardar").setAttribute("disabled", true);
  document
    .getElementById("conclu_inp_aptitu_con")
    .setAttribute("disabled", true);
  document
    .getElementById("conclu_inp_fecha_inicio_rest_con")
    .setAttribute("disabled", true);
  document
    .getElementById("conclu_inp_fecha_fin_rest_con")
    .setAttribute("disabled", true);
  document
    .getElementById("conclu_inp_tipo_rest_con")
    .setAttribute("disabled", true);
  document
    .getElementById("conclu_inp_personal_salud_con")
    .setAttribute("disabled", true);
  document
    .getElementById("conclu_inp_fecha_reevaluacion_dso_con")
    .setAttribute("disabled", true);
  document
    .getElementById("conclu_inp_observaciones_con")
    .setAttribute("disabled", true);
  document.getElementById("inpu_tab5_cobertura").setAttribute("disabled", true);
  document
    .getElementById("inpu_tab5_busca_medicamento")
    .setAttribute("disabled", true);
  document
    .getElementById("inpu_tab5_presentación")
    .setAttribute("disabled", true);
  document.getElementById("inpu_tab5_dosis").setAttribute("disabled", true);
  document.getElementById("inpu_tab5_dosis_DSO").setAttribute("disabled", true);
  document
    .getElementById("inpu_tab5_frecuencia")
    .setAttribute("disabled", true);
  document
    .getElementById("inpu_tab5_tiempo_uso")
    .setAttribute("disabled", true);
  document
    .getElementById("medicamento_section_button_add")
    .setAttribute("disabled", true);
  //botones de generales
  document
    .getElementById("botton_form_invest_save")
    .setAttribute("disabled", true);
}

//seleccionar en select
function selectedInSelectUpdate(path, value) {
  console.log(path + " --- " + value);
  let options = Object.entries(document.getElementById(path).children);
  options.forEach((element) => {
    if (element[1].value == value) {
      element[1].selected = true;
    }
  });
}

function setInputDate(date, type) {
  switch (type) {
    case "latin":
      let datos = date.split("/").reverse().join("-");
      return datos;
      break;
  }
}

/**
 *
 * @param {String} path
 * @param {String} contenido
 * @param {String} value
 * @param {String} name
 * Helpers, funcion para cargar los valores a los option
 */
function cargarSelect(path, contenido, value, name) {
  let content = '<option value="">Seleccione</option>';
  contenido.forEach((element) => {
    content += `<option value="${element[value]}">${element[name]}</option>`;
  });
  document.getElementById(path).innerHTML = content;
}

function cargarExamenAuxiliar(id) {
  let examenAuxiliar = vw_main.getTiposExamenes(0);
  let seleccionado = "";
  examenAuxiliar.forEach((element) => {
    if (element.Id == id) {
      seleccionado = element.TipoExamen;
    }
  });
  return seleccionado;
}

//función para pintar cuantos caracteres quedan
function LenghtRestanteInput(path, lengthTotal) {
  let divLenght = "";
  if (
    parseInt(document.getElementById(path).value.length) >=
    lengthTotal * 0.7
  ) {
    if (document.getElementById(path + "-lenghtCustom") == undefined) {
      divLenght = document.createElement("div");
      divLenght.classList.add("length-input");
      divLenght.id = path + "-lenghtCustom";
      document.getElementById(path).parentElement.appendChild(divLenght);
    } else {
      divLenght = document.getElementById(path + "-lenghtCustom");
      let internoDiv = document.createElement("div");
      internoDiv.classList.add("restantes");
      let restantePalabras =
        lengthTotal - document.getElementById(path).value.length;
      internoDiv.innerHTML = `<div class="restantes">${restantePalabras} palabras restantes</div>`;
      divLenght.innerHTML = internoDiv.innerHTML;
    }
  } else {
    if (document.getElementById(path + "-lenghtCustom") == undefined) {
      let boxMessage = document.createElement("div");
      boxMessage.classList.add("length-input");
      boxMessage.id = path + "-lenghtCustom";
      document.getElementById(path).parentElement.appendChild(boxMessage);
    }
    divLenght = document.getElementById(path + "-lenghtCustom");
    divLenght.innerHTML = "";
  }
}

//funcion para prevenil comportamiento de envio de formulario
function prevenirEnvio(callback) {
  console.log("deteniendo");
  callback();
  return false;
}

//validar campos texto ingreso de numeros
function itsNumber(path) {
  let inputNumber = document.getElementById(path).value;
  var valoresAceptados = /^[0-9]\d*(\.\d+)?$/;
  if (!inputNumber.match(valoresAceptados)) {
    document.getElementById(path).value = removeNonNumeric(inputNumber);
  }
}

function removeNonNumeric(testString) {
  var NumericString = testString.replace(/^ [0-9]\d*(\.\d+)?$/g, "");
  return NumericString;
}

function getFlagReceta() {
  let valorRetorno = "";
  if (emiv_receta_requi_si.checked) {
    valorRetorno = 1;
  } else {
    valorRetorno = 2;
  }
  return valorRetorno;
}

function convertirInTextSiNo(value) {
  let palabra = "";
  if (value == 1) {
    palabra = "Si";
  } else {
    palabra = "No";
  }
  return palabra;
}

function returnFloat(texto) {
  return parseFloat(texto.replace(",", "."));
}

function cargarArray(datos, principal) {
  if (datos.length > 0) {
    datos.forEach((element) => {
      principal.push(element);
    });
  }
}

function getGerencia(id) {
  let gerencia = "";
  sedeAreaGerencia.Gerencia.forEach((element) => {
    if (element.Id == id) {
      gerencia = element.Description;
    }
  });
  return gerencia;
}

function getSedePlanta(idSede) {
  let sede = "";
  sedeAreaGerencia.Sedes.forEach((element) => {
    if (element.Id == idSede) {
      sede = element.Description;
    }
  });
  return sede;
}

function getArea(id) {
  let area = "";
  sedeAreaGerencia.Area.forEach((element) => {
    if (element.Id == id) {
      area = element.Description;
    }
  });
  return area;
}
