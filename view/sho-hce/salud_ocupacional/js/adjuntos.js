//variables globales
window.adjunto_examen = {};
window.adjunto_examen.id = 0;
window.adjunto_examen.index = 0;
//funciones generales
function sendAdjuntos(name, adjunto) {
  //obteniendo la información de la investigación creada
  let obj_investigacion = JSON.parse(
    sessionStorage.getItem("obj_investigacion")
  );
  //creacion del json a enviar
  var raw = JSON.stringify({
    Id: 0,
    EnfermedadOcupacionalId: obj_investigacion.Id,
    Name: name,
    Adjunto: adjunto,
    CreatedBy: "jamq postman...",
    Deleted: 0,
  });

  console.log(raw);
  var url =
    apiUrlsho +
    `/api/EO-AdjuntoEnfermedadesOcupacionales-All-post?code=wLBde9JxnvwgfyN5/xjExmaA3hE8oG3156W7QmPGdeSPdIBWRAS9FA==&httpmethod=post`;

  var myHeaders = new Headers();
  myHeaders.append("apikey", constantes.apiKey);
  myHeaders.append("Content-Type", "application/json");

  fetch(url, {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  })
    .then((res) => res.text())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log("cargando adjunto...");
      console.log(response);
      Swal.fire({
        title: "Se adjunto el documento de la Enfermedad Ocupacional con exito",
        icon: "success",
        showConfirmButton: false,
        timer: 2500,
      });
      getEnfermedadesOcupacionales(obj_investigacion.Id);
      reiniciarBotonesdeCargarAdjuntos();
      return response;
    });
}

function reiniciarBotonesdeCargarAdjuntos() {
  var textAdjunto = document.querySelectorAll("#text_adjuntar_inv_file");
  document
    .querySelectorAll("#am_spin_guardar_adjunto_descanso")
    .forEach((element) => {
      element.style.display = "none";
    });
  textAdjunto.forEach((Element) => {
    Element.innerHTML = "Subir archivo";
  });
}

//convierte en base64 cualquier archivo y llama a una funcion que la usa
function getBase64(file, callback) {
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    console.log(reader.result);
    callback(reader.result);
    return reader.result;
  };
  reader.onerror = function (error) {
    console.log("Error: ", error);
  };
}

function deleteAdjuntos(id, index) {
  //obteniendo la información de la investigación creada
  let obj_investigacion = JSON.parse(
    sessionStorage.getItem("obj_investigacion")
  );
  let adjuntoGlobal = selectAdjuntoParticular(index);
  var raw = JSON.stringify({
    Id: id,
    EnfermedadOcupacionalId: idHC,
    Name: adjuntoGlobal.Name,
    Adjunto: adjuntoGlobal.Adjunto,
    Deleted: 1,
    CreatedBy: "jamq2 postman...",
  });

  var url =
    apiUrlsho +
    `/api/EO-AdjuntoEnfermedadesOcupacionales-All-post?code=wLBde9JxnvwgfyN5/xjExmaA3hE8oG3156W7QmPGdeSPdIBWRAS9FA==&httpmethod=post`;

  var myHeaders = new Headers();
  myHeaders.append("apikey", constantes.apiKey);
  myHeaders.append("Content-Type", "application/json");

  fetch(url, {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  })
    .then((res) => res.text())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log("Eliminando adjunto...");
      console.log(response);
      Swal.fire({
        title: "Se elimino el adjunto de la Enfermedad Ocupacional con exito",
        icon: "success",
        showConfirmButton: false,
        timer: 2500,
      });
      getEnfermedadesOcupacionales(obj_investigacion.Id);
      return response;
    });
}

function adjuntoLoadInHtml(file) {
  var textAdjunto = document.querySelectorAll("#text_adjuntar_inv_file");
  document
    .querySelectorAll("#am_spin_guardar_adjunto_descanso")
    .forEach((element) => {
      element.style.display = "block";
    });
  textAdjunto.forEach((Element) => {
    Element.innerHTML = document.getElementById(
      "file_adjunto_investigacion_evaluacion"
    ).files[0].name;
  });
  getBase64(
    document.getElementById("file_adjunto_investigacion_evaluacion").files[0],
    preparateDataSend
  );
  //enviando el adjunto
}

function preparateDataSend(base64) {
  sendAdjuntos(
    document.getElementById("file_adjunto_investigacion_evaluacion").files[0]
      .name,
    base64
  );
}

//dibuja los adjuntos que posee la enfermedad
function drawInfoAdjuntos(adjuntosList) {
  let seccionAdjuntoRows = "";
  if (adjuntosList.length > 0) {
    document
      .getElementById("seccion-boton-subir-adjunto-investigacion-top")
      .classList.remove("hidden");
    document
      .getElementById("box-btn-subir-adjunto-investigacion-center")
      .classList.add("hidden");
    document
      .getElementById("table_adjuntar_files_investigacion")
      .classList.remove("hidden");
    //pintar los datos de los adjuntos que posee
    adjuntosList.forEach((element, index) => {
      seccionAdjuntoRows += `<tr>
          <td>${element.Name}</td>
          <td>${element.CreatedDateF}</td>
          <td>
            <div class="dropdown dropleft d-flex justify-content-end">
              <a
                class="nav-link textVertical"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ...
              </a>
              <div
                class="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
                x-placement="left-start"
                style="
                  position: absolute;
                  top: 15px;
                  left: -236px;
                  will-change: top, left;
                "
              >
                <a class="dropdown-item" onclick="deleteAdjuntos(${element.Id},${index})"
                  ><img src="images/sho/delete-trash.png" alt="" />&nbsp;&nbsp;
                  Eliminar
                  </a
                >
                <a class="dropdown-item" onclick="loadAdjuntoEnfermedadOcupacional(${element.Id})"
                  ><img src="./images/sho/eyeIcon.svg" alt="" />&nbsp;&nbsp;
                  Descargar documento </a
                >
              </div>
            </div>
          </td>
        </tr>`;
    });
    //coloca la informacion en su tabla
    document.getElementById(
      "content_adjunto_investigaciones_enfermedadOcupacional"
    ).innerHTML = seccionAdjuntoRows;
  } else {
    console.log("es cero los adjuntos");
  }
}

function DescargableDocumento(data, name) {
  var arrdata = data.split(",");
  var fileBase64 = arrdata[1];
  var mime = arrdata[0].replace("data:", "").replace(";base64", "");
  var arrBuffer = base64ToArrayBuffer(fileBase64);

  // It is necessary to create a new blob object with mime-type explicitly set
  // otherwise only Chrome works like it should
  var newBlob = new Blob([arrBuffer], { type: mime });

  // IE doesn't allow using a blob object directly as link href
  // instead it is necessary to use msSaveOrOpenBlob
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(newBlob);
    return;
  }

  // For other browsers:
  // Create a link pointing to the ObjectURL containing the blob.
  var data = window.URL.createObjectURL(newBlob);

  var link = document.createElement("a");
  document.body.appendChild(link); //required in FF, optional for Chrome
  link.href = data;
  link.download = name;
  link.click();
  window.URL.revokeObjectURL(data);
  link.remove();
}

function selectAdjuntoParticular(index) {
  let enfermedadStorage = JSON.parse(
    sessionStorage.getItem("enfermedad_ocupacional")
  );
  return enfermedadStorage.Adjuntos[index];
}

function loadAdjuntoEnfermedadOcupacional(id) {
  var url =
    apiUrlsho +
    `/api/EO-AdjuntoEnfermedadesOcupacionales-All-get?code=Kmn0CZecPkIDrig3YNQzXpSFTqhjZwq8SUgVkcMf3nKFACcDa4q4qA==&httpmethod=objectlist&Id=${id}`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log(response);
      DescargableDocumento(response.Adjunto, response.Name);
    });
}

//funcion helpers para convertir base64 a buffer
function base64ToArrayBuffer(base64) {
  var binaryString = window.atob(base64);
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
    var ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
}

/**
 * CARGAR EXAMENES AUXILIARES
 * --------------------------|
 */
function loadAdjuntoExamenesEspecialistas(file, id, index) {
  window.adjunto_examen.id = id;
  window.adjunto_examen.index = index;
  getBase64(
    document.getElementById("file_adjunto_examen_especialistas-" + index)
      .files[0],
    preparandoExamenFile
  );
}

function sendDataExamenEspecialista(id, name, index, base64) {
  var url =
    apiUrlsho +
    `/api/EO-AdjuntoEvaluacionesEspecialistas-All-post?code=mfkVdrfjnoIMRNOn8jjHBWmoudo0IBNBWMKw4ITNyome5bRKEiYMMQ==&httpmethod=post`;

  var myHeaders = new Headers();
  myHeaders.append("apikey", constantes.apiKey);
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    Id: id,
    Name: name,
    Adjunto: base64,
    CreatedBy: "jamq postman...",
    Deleted: 0,
  });
  console.log(id, name, index);

  fetch(url, {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log(response);
      console.log("cargando adjunto examen especia...");
      Swal.fire({
        title:
          "Se adjunto el documento de examen especialista cargado correctamente",
        icon: "success",
        showConfirmButton: false,
        timer: 2500,
      });
      addNameToAdjunto(index, name);
    });
}

function preparandoExamenFile(file) {
  let nameExamen = document.getElementById(
    "file_adjunto_examen_especialistas-" + window.adjunto_examen.index
  ).files[0].name;
  sendDataExamenEspecialista(
    window.adjunto_examen.id,
    nameExamen,
    window.adjunto_examen.index,
    file
  );
}

function downloadAdjExamenEspecialista(id) {
  var url =
    apiUrlsho +
    `/api/EO-AdjuntoEvaluacionesEspecialistas-All-get?code=QOWRCoJkwIlENxPm9mOQ1wZWWZo3ntTU2KtI39/nJZwWa3a/FMZpxw==&httpmethod=objectlist&Id=${id}`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log(response);
      DescargableDocumento(response.Adjunto, response.Name);
    });
}
