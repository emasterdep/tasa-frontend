function renderListInvestigacionesOE(data) {
  let columnInformacion = "";
  console.log(data);

  function validateAbiertaInv(status, value) {
    return status == value ? "" : "d-none";
  }

  data.EnfermedadesOcupacionales.forEach((element) => {
    columnInformacion += `
    <tr class="item_ant_oc item_ant_3" investigacion-list="${element.Id}">
    <td><span>${element.Code}</span></td>
    <td><span>----</span></td>
    <td><span>${element.CreatedDateF}</span></td>
    <td><span>----</span></td>
    <td><span>${element.DiagnosticosIniciales}</span></td>
    <td><span>${element.DiagnosticosFinales}</span></td>
    <td><span>${element.EstadoEnfermedadOcupacional}</span></td>
    <td>
      <!-- Botones de opciones-->
      <div class="dropdown float-right dropleft">
        <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
          <img src="images/iconos/menu_responsive.svg" alt="">
        </div>
        <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
          <ul>
            <li class="${validateAbiertaInv(
              element.EstadoEnfermedadOcupacional,
              "Cerrado"
            )}" onclick="viewDataEnfermadadesOcupacionalesConfirmadas(${
      element.Id
    },'${element.NroCaso}')" id="btn_examen_editar">
              <img src="./images/sho/eyeIcon.svg" fill="#5daf57">
              <span>Ver información ocupacional</span>
            </li>
            <li class="${validateAbiertaInv(
              element.EstadoEnfermedadOcupacional,
              "Abierto"
            )}" onclick="onAtrasHistoriaClick(13); shoEditInvestigacionSP5(${element.Id})">
              <img src="./images/sho/edit.svg">
              <span>Editar</span>
            </li>
            <li onclick="verModoEdicionInvestigacion(${element.Id})">
              <img src="./images/sho/eyeIcon.svg">
              <span>Ver detalle</span>
            </li>
          </ul>
        </div>
      </div>
    </td>
    </tr>`;
  });
  if (data.EnfermedadesOcupacionales.length == 0) {
    columnInformacion += `
    <tr class="table-empty">
      <td colspan="8" class="text-center text-uppercase">No hay informacion registrada</td>
    </tr>
  `;
  }
  document.getElementById("list-investigaciones-EO").innerHTML =
    columnInformacion;
}

function listInvestigacionesEO(id) {
  var url =
    apiUrlsho +
    `/api/EO-EnfermedadesOcupacionales-All-get?code=acJSBh7lPrk/REaaCQmangSeJ/qgv063y0UgjpwvyB9zNZaWrrdHgg==&httpmethod=objectlist&Id=0&HistoriaClinicaId=${id}&GerenciaId=0&SedeId=0&AreaId=0&Documento=0&Nombres=&Apellidos=&CodeEO=`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      renderListInvestigacionesOE(response);
    });
}

/**
 * JESUS MILLAN 24/12/2021
 * :-(
 */

/**
 * [viewDataEnfermadadesOcupacionalesConfirmadas description]
 * @param  {[int]} Id [ID de la Enfermedad Ocupacional registrada]
 * @return {[type]}    [description]
 */
function viewDataEnfermadadesOcupacionalesConfirmadas(Id, NroCaso) {
  console.log("aquí cargo esta infomación ", EnfermedadOcupacionalSeleted);
  console.log("NroCaso ", NroCaso);
  /**
   * [columnInformacion LISTADO DE REGISTROS]
   * @type {String}
   */
  let columnInformacion = "";
  /**
   * [EnfermedadOcupacionalSeleted ID DE LA ENFERMEDAD OCUPACIONAL SELECCIONADA]
   * @type {[type]}
   */
  EnfermedadOcupacionalSeleted = Id;

  document.getElementById("NroCasoEO").innerHTML = NroCaso;

  //BUSCAMOS LOS DIAGNOSTICOS CONFIRMADOS PARA ESTA IEO
  var url =
    apiUrlsho +
    `/api/EO-Diagnosticos-All-get?code=4aEkn7Aebc4qe33SyEQfhM5aJTB56RRizPLFHnjfaHnX020qbqeezA==&httpmethod=objectlist&EnfermedadOcupacionalId=${Id}&StatusDiagnostico=CO`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.warn("response -----> ", response.Diagnosticos);

      response.Diagnosticos.forEach((element, index) => {
        console.warn("index, element -> ", index, element);
        columnInformacion += `
          <tr class="item_ant_oc item_ant_3" data-cie10="${element.CIE10}">
            <td><span>${element.CIE10}</span></td>
            <td><span>${element.CreatedDateF}</span></td>
            <td><span>${element.CantidadDescansosMedicos}</span></td>
            <td>
              <!-- Botones de opciones-->
              <div class="dropdown float-right dropleft">
                <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
                  <img src="images/iconos/menu_responsive.svg" alt="">
                </div>
                <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
                  <ul>
                    <li onclick="getListadosMedicos(${element.Id}, ${element.HistoriaClinicaId})">
                      <img src="./images/sho/ojo.svg">
                      <span>Ver detalle</span>
                    </li>
                  </ul>
                </div>
              </div>
            </td>
          </tr>
        `;
      });
      if (response.Diagnosticos.length == 0) {
        columnInformacion += `
          <tr class="table-empty">
            <td colspan="4" class="text-center text-uppercase">No hay informaci&oacute;n registrada</td>
          </tr>
        `;
      }

      document.getElementById("ListConfirmadas").innerHTML = columnInformacion;
    });

  $("#ListadosMedicosEnfermedadesOcupacionalesConfirmada").collapse("hide");
  $("#EnfermedadesOcupacionalesConfirmada").collapse("show");
}

function getListadosMedicos(IdDiagnostico, HistoriaClinicaId) {
  console.warn("getListadosMedicos recibe IdDiagnostico -> ", IdDiagnostico);
  console.warn(
    "getListadosMedicos recibe HistoriaClinicaId -> ",
    HistoriaClinicaId
  );
  // PARA SABER QUE VOY POR EL MODULO DE IEO
  DiagnosticoId = IdDiagnostico;
  ID_ORIGEN = DiagnosticoId;
  ID_TIPO_ORIGEN = 3; //que es una ENFERMEDAD OCUPACIONAL

  //BUSCAMOS LOS DESCANSOS MEDICOS, TRANSFERENCIAS, INTERCONSULTAS Y SEGUIMIENTOS PARA EL DIAGNOSTICO
  var url =
    apiUrlsho +
    `/api/EO-Diagnosticos-All-get?code=4aEkn7Aebc4qe33SyEQfhM5aJTB56RRizPLFHnjfaHnX020qbqeezA==&httpmethod=objectlistByEOC&DiagnosticoId=${IdDiagnostico}`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      //renderListInvestigacionesOE(response);
      console.warn("response -> ", response);

      listarDescansosMedicosSp5EO(response.DescansosMedicos, HistoriaClinicaId);
      listarTransferenciasSp5EO(response.Transferencias);
      listarInterconsultasSp5EO(response.Interconsultas);
      listarSeguimientosSp5EO(response.Seguimientos);
    });

  $("#ListadosMedicosEnfermedadesOcupacionalesConfirmada").collapse("show");
}

/**
 * [listarDescansosMedicosSp5EO description]
 * @param  {[array]} DescansosMedicos [Listado de descansos medicos]
 * @return {[type]}                  [description]
 */
function listarDescansosMedicosSp5EO(DescansosMedicos, HistoriaClinicaId) {
  let columnInformacion = "";

  document.getElementById("IEO_DesMedCount").innerHTML =
    DescansosMedicos.length;

  if (DescansosMedicos.length > 0) {
    //IEO_ListadoDescansosMedicos
    DescansosMedicos.forEach((element, index) => {
      columnInformacion += ` 
        <tr class="item_ant_oc item_ant_3" data-cie10="${element.Id}">
          <td><span>${element.B_FechaIniF}</span></td>
          <td><span>${element.B_FechaFinF}</span></td>
          <td><span>${element.B_CantidadDias}</span></td>
          <td><span>${element.B_DiasAcumulados}</span></td>
          <td><span>---</span></td>
          <td><span>${element.Diagnosticos}</span></td>
          <td colspan="2"><span>${element.TipoContingencia}</span></td>
          <td>
            <!-- Botones de opciones-->
            <div class="dropdown float-right dropleft">
              <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
                <img src="images/iconos/menu_responsive.svg" alt="">
              </div>
              <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
                <ul>
                  <li onclick="VerDescansoMedico(${element.Id}, ${HistoriaClinicaId}, '${element.A_IdDescansoMedico}')"> 
                    <img src="./images/sho/ojo.svg">
                    <span>Ver detalle</span>
                  </li>
                </ul>
              </div>
            </div>
          </td>
        </tr>
      `;
    });

    document.getElementById("IEO_ListadoDescansosMedicos").innerHTML =
      columnInformacion;
  }

  return;
}

/**
 * [VerDescansoMedico METODO PARA VISUALIZAR EL DESCANSO MEDICO]
 * @param {[type]} DescansoMedicoId  [description]
 * @param {[type]} HistoriaClinicaId [description]
 */
function VerDescansoMedico(
  DescansoMedicoId,
  HistoriaClinicaId,
  A_IdDescansoMedico
) {
  console.warn("DescansoMedicoId -> ", DescansoMedicoId);
  console.warn("HistoriaClinicaId -> ", HistoriaClinicaId);
  console.warn("A_IdDescansoMedico -> ", A_IdDescansoMedico);

  //cargamos en la vista por el handler el nuevo formulario
  handlerUrlhtml(
    "contentGlobal",
    "view/sho-hce/descansos_medicos/Ver_DescansoMedico_HC.html",
    "Ver Datos descanso medico "
  );

  id_ATM = DescansoMedicoId;
  ID_DESCANSO_MEDICO_EO = DescansoMedicoId;
  idHC = HistoriaClinicaId;
  newD = 0;

  var headers = {
    apiKey: constantes.apiKey,
    "Content-Type": "application/json",
  };

  var url =
    apiUrlsho +
    "/api/hce_Get_017_descanso-medico_busqueda?code=qiLgD8uqGlqrIjk1Y7P1NgNioYJRj/HQvmwq60UFvqerUH4Uz5Nwcw==&IdDescanso=" +
    A_IdDescansoMedico +
    "&Gerencia=&Planta=&Area=&Documento=&Nombres=&Apellidos=&OrigenDescanso=&FechaDesde=&FechaHasta=&Buscar=";

  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers,
  };

  $.ajax(settings).done(function (response) {
    console.log("**todos interc 269 >>>--> ", response);

    if (response.DiagnosticoCIE10.length > 0) {
      response.DiagnosticoCIE10.map(function (itemx) {
        //D_IT[itemx.InterconsultaId] = D_IT[itemx.InterconsultaId].itemx.Descripcion_TipCie10;
        console.log("diagnostico", itemx.Descripcion_TipCie10);

        if (
          itemx.Descripcion_TipCie10 == "null" ||
          itemx.Descripcion_TipCie10 == null ||
          itemx.Descripcion_TipCie10 == ""
        ) {
          D_IT[itemx.InterconsultaId] = "-----";
        } else {
          D_IT[itemx.InterconsultaId] = itemx.Descripcion_TipCie10;
        }
      });
      console.log("302 D_IT[]", D_IT);
    }

    if (response.DescansoMedico.length > 0) {
      response.DescansoMedico.forEach((Item) => {
        paObj_DM_SHO[Item.Id] = new Interconsulta();
        paObj_DM_SHO[Item.Id].cargarData(Item);
        paObj_ATM_SHO[Item.Id] = new Interconsulta();
        paObj_ATM_SHO[Item.Id].cargarData(Item);
      });
      console.log("paObj_ATM_SHO", paObj_ATM_SHO);
    }
  });

  // en gestionDescansoMedico.js
  fnSp3Carga_Listas_Desplegables_EO(); //DENTRO DE ELLA LLAMAMOS A CARGAR LOS DATOS PERSONALES

  return;
}

/**
 * [listarTransferenciasSp5EO description]
 * @param  {[array]} Transferencias [Listado de transferencias]
 * @return {[type]}                [description]
 */
function listarTransferenciasSp5EO(Transferencias) {
  let columnInformacion = "";

  document.getElementById("IEO_TransfCount").innerHTML = Transferencias.length;

  if (Transferencias.length > 0) {
    //IEO_ListadoTransferencias
    Transferencias.forEach((element, index) => {
      columnInformacion += ` 
        <tr class="item_ant_oc item_ant_3" data-TransferenciaId="${element.Id}">
          <td><span>${element.A_CodeTransferencia}</span></td>
          <td><span>---</span></td>
          <td><span>${element.A_FechaTransferenciaF}</span></td>
          <td><span>---</span></td>
          <td>
            <!-- Botones de opciones-->
            <div class="dropdown float-right dropleft">
              <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
                <img src="images/iconos/menu_responsive.svg" alt="">
              </div>
              <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
                <ul>
                  <li onclick="verEditarTransferenciaSp5EO(${element.Id},'${element.A_CodeTransferencia}', 'Editar')">
                    <img src="./images/sho/edit.svg">
                    <span>Editar</span>
                  </li>
                </ul>
                <ul>
                  <li onclick="verEditarTransferenciaSp5EO(${element.Id},'${element.A_CodeTransferencia}', 'Ver')">
                    <img src="./images/sho/ojo.svg">
                    <span>Ver detalle</span>
                  </li>
                </ul>
              </div>
            </div>
          </td>
        </tr>
      `;
    });

    document.getElementById("IEO_ListadoTransferencias").innerHTML =
      columnInformacion;
  }

  return;
}

/**
 * [verEditarTransferenciaSp5EO ver o editar una transferencia]
 * @param  {[int]} Id                [description]
 * @param  {[string]} CodeTransferencia [description]
 * @param  {[string]} Accion [description]
 * @return {[type]}                   [description]
 */
function verEditarTransferenciaSp5EO(Id, CodeTransferencia, Accion) {
  console.warn("Id -> ", Id);
  console.warn("CodeTransferencia -> ", CodeTransferencia);
  console.warn("Accion -> ", Accion);

  CODE_TRANSFERENCIA_EO = CodeTransferencia;

  /**
   * [VER TRANSFERENCIA]
   */
  if (Accion === "Ver") {
    ACCION_TRANSFERENCIA_EO = "VER";
    getTransferenciasByCode();
  }

  /**
   * [EDITAR TRANSFERENCIA]
   */
  if (Accion === "Editar") {
    ACCION_TRANSFERENCIA_EO = "EDITAR";
    getTransferenciasByCode();
  }
}

/**
 * [listarInterconsultasSp5EO description]
 * @param  {[array]} Interconsultas [listado de interconsultas]
 * @return {[type]}                [description]
 */
function listarInterconsultasSp5EO(Interconsultas) {
  let columnInformacion = "";

  document.getElementById("IEO_InterCount").innerHTML = Interconsultas.length;

  if (Interconsultas.length > 0) {
    Interconsultas.forEach((element, index) => {
      let RecibioRespuesta = element.B_RecibioRespuesta === 1 ? "Si" : "No";

      columnInformacion += ` 
        <tr class="item_ant_oc item_ant_3" data-InterconsultaId="${element.Id}">
          <td><span>${element.A_CodeInterconsulta}</span></td>
          <td><span>${element.A_FechaInterconsultaF}</span></td>
          <td><span>${element.Especialidad}</span></td>
          <td><span>${RecibioRespuesta}</span></td>
          <td colspan="2"><span>${element.Aptitud}</span></td>
          <td>
            <!-- Botones de opciones-->
            <div class="dropdown float-right dropleft">
              <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
                <img src="images/iconos/menu_responsive.svg" alt="">
              </div>
              <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
                <ul>
                  <li onclick="verEditarInterconsultaSp5EO(${element.Id},'${element.A_CodeInterconsulta}', 'Editar')">
                    <img src="./images/sho/edit.svg">
                    <span>Editar</span>
                  </li>
                </ul>
                <ul>
                  <li onclick="verEditarInterconsultaSp5EO(${element.Id},'${element.A_CodeInterconsulta}', 'Ver')">
                    <img src="./images/sho/ojo.svg">
                    <span>Ver detalle</span>
                  </li>
                </ul>
              </div>
            </div>
          </td>
        </tr>
      `;
    });

    document.getElementById("IEO_ListadoInterconsulta").innerHTML =
      columnInformacion;
  }

  return;
}

/**
 * [verEditarInterconsultaSp5EO ver o editar una transferencia]
 * @param  {[int]} Id                   [description]
 * @param  {[string]} CodeInterconsulta [description]
 * @param  {[string]} Accion            [description]
 * @return {[type]}                     [description]
 */
function verEditarInterconsultaSp5EO(Id, CodeInterconsulta, Accion) {
  console.warn("Id -> ", Id);
  console.warn("CodeInterconsulta -> ", CodeInterconsulta);
  console.warn("Accion -> ", Accion);

  CODE_INTERCONSULTA_EO = CodeInterconsulta;

  /**
   * VER INTERCONSULTA
   * IdInterConsulta, IdHistoriaClinica
   */
  if (Accion === "Ver") {
    ACCION_INTERCONSULTA_EO = "VER";
    console.warn("1. ACCION_INTERCONSULTA_EO -> ", ACCION_INTERCONSULTA_EO);
    getInterconsultasByCode();
  }

  /**
   *  EDITAR INTERCONSULTA
   *  IdInterConsulta, IdHistoriaClinica
   */
  if (Accion === "Editar") {
    ACCION_INTERCONSULTA_EO = "EDITAR";
    console.warn("2. ACCION_INTERCONSULTA_EO -> ", ACCION_INTERCONSULTA_EO);
    getInterconsultasByCode();
  }
}

/**
 * [listarSeguimientosSp5EO description]
 * @param  {[array]} Seguimientos [listado de interconsultas]
 * @return {[type]}                [description]
 */
function listarSeguimientosSp5EO(Seguimientos) {
  let columnInformacion = "";

  document.getElementById("IEO_SeguimCount").innerHTML = Seguimientos.length;
  COUNT_SEGUIMIENTOS_EO = Seguimientos.length;

  if (Seguimientos.length > 0) {
    Seguimientos.forEach((element, index) => {
      columnInformacion += ` 
        <tr id="TR_SEGUIMIENTO_EO_${element.Id}" class="item_ant_oc item_ant_3" data-SeguimientoId="${element.Id}">
          <td><span>${element.CreatedDateF}</span></td>
          <td>
            <span id="ver_seguimiento_eo_${element.Id}">${element.Comentario}</span>
            <input name="editar_seguimiento_eo_${element.Id}" id="editar_seguimiento_eo_${element.Id}" value="${element.Comentario}" class="submit d-none">
          </td>
          <td>
            <!-- Botones de opciones-->
            <div class="dropdown float-right dropleft">
              <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
                <img src="images/iconos/menu_responsive.svg" alt="">
              </div>
              <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
                <ul id="seguimiento_eo_${element.Id}" class="d-none">
                  <li onclick="editarEliminarSeguiminetoSp5EO(${element.Id}, 'Guardar')">
                    <img src="./images/sho/checkMenu.svg">
                    <span>Guardar</span>
                  </li>
                </ul>
                <ul>
                  <li onclick="editarEliminarSeguiminetoSp5EO(${element.Id}, 'Editar')">
                    <img src="./images/sho/edit.svg">
                    <span>Editar</span>
                  </li>
                </ul>
                <ul>
                  <li onclick="editarEliminarSeguiminetoSp5EO(${element.Id}, 'Eliminar')">
                    <img src="./images/sho/delete.svg">
                    <span>Eliminar</span>
                  </li>
                </ul>
              </div>
            </div>
          </td>
        </tr>
      `;
    });

    document.getElementById("IEO_ListadoSeguimientos").innerHTML =
      columnInformacion;
  }

  return;
}

/**
 * [editarEliminarSeguiminetoSp5EO METODO PARA ELIMINAR O EDITAR SEGUIMIENTOS]
 * @param  {[type]} Id     [Id DEL SEGUIMIENTO]
 * @param  {[type]} Accion [ACCION A REALIZAR ('EDITAR' O ELIMINAR)]
 * @return {[type]}        [description]
 */
function editarEliminarSeguiminetoSp5EO(Id, Accion) {
  console.warn("Id -> ", Id);
  console.warn("Accion -> ", Accion);

  // ELIMINAMOS UN REGSITRO NUEVO
  if (Accion === "NewEliminar") {
    $(`#TR_SEGUIMIENTO_EO_${Id}`).remove();
    COUNT_SEGUIMIENTOS_EO--;
    document.getElementById("IEO_SeguimCount").innerHTML =
      COUNT_SEGUIMIENTOS_EO;
  }

  // REGISTRAMOS UN NUEVO SEGUIMIENTO
  if (Accion === "NewGuardar") {
    let comment = $(`#NEW_SEGUIMIENTO_EO_${Id}`).val().trim();
    console.warn("NEW_SEGUIMIENTO_EO -> ", NEW_SEGUIMIENTO_EO);
    console.warn("comment -> ", comment);

    // IF EL COMENTARIO ES VACIO NO GUARDA
    if (comment === "") {
      Swal.fire({
        imageUrl: "images/sho/advertencia.svg",
        imageWidth: 48,
        imageHeight: 48,
        title: "Error!, El comentario no puede estar vacío.",
        showConfirmButton: true,
      });
    }
    // GUARDAMOS EL COMENTARIO
    else {
      console.warn("DiagnosticoId -> ", DiagnosticoId);
      // https://5h0-dev-salud.azurewebsites.net/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=postSeguimiento
      var url =
        apiUrlsho +
        `/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=postSeguimiento`;

      let data = {
        Id: 0,
        DiagnosticoId: DiagnosticoId,
        Comentario: comment,
      };

      let headers = {
        apikey: constantes.apiKey,
        "Content-Type": "application/json",
      };

      let settings = {
        url: url,
        method: "post",
        dataType: "json",
        headers: headers,
        data: JSON.stringify(data),
      };

      return $.ajax(settings).done((response) => {
        console.log("Seguimiento post -> ", response);
        if (response.Id > 0) {
          // borramos la linea de nuevo
          $(`#TR_NEW_SEGUIMIENTO_EO_${Id}`).remove();

          // agregamos la nueva linea
          let nowDateF = moment().format("DD/MM/YYYY");
          let columnInformacion = ` 
            <tr id="TR_SEGUIMIENTO_EO_${response.Id}" class="item_ant_oc item_ant_3" data-SeguimientoId="${response.Id}">
              <td><span>${nowDateF}</span></td>
              <td>
                <span id="ver_seguimiento_eo_${response.Id}">${response.Comentario}</span>
                <input name="editar_seguimiento_eo_${response.Id}" id="editar_seguimiento_eo_${response.Id}" value="${response.Comentario}" class="submit d-none">
              </td>
              <td>
                <!-- Botones de opciones-->
                <div class="dropdown float-right dropleft">
                  <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
                    <img src="images/iconos/menu_responsive.svg" alt="">
                  </div>
                  <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
                    <ul id="seguimiento_eo_${response.Id}" class="d-none">
                      <li onclick="editarEliminarSeguiminetoSp5EO(${response.Id}, 'Guardar')">
                        <img src="./images/sho/checkMenu.svg">
                        <span>Guardar</span>
                      </li>
                    </ul>
                    <ul>
                      <li onclick="editarEliminarSeguiminetoSp5EO(${response.Id}, 'Editar')">
                        <img src="./images/sho/edit.svg">
                        <span>Editar</span>
                      </li>
                    </ul>
                    <ul>
                      <li onclick="editarEliminarSeguiminetoSp5EO(${response.Id}, 'Eliminar')">
                        <img src="./images/sho/delete.svg">
                        <span>Eliminar</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </td>
            </tr>
          `;

          // $("#IEO_SeguimCount").html('')

          document.getElementById("IEO_ListadoSeguimientos").innerHTML +=
            columnInformacion;
        }
      }); //*/
    }
  }

  //VAMOS A EDITAR UN SEGUIMIENTO
  if (Accion === "Editar") {
    $(`#seguimiento_eo_${Id}`).removeClass("d-none");
    $(`#editar_seguimiento_eo_${Id}`).removeClass("d-none");
    $(`#ver_seguimiento_eo_${Id}`).addClass("d-none");
  }

  // MODIFICAMOS UN SEGUIMIENTO
  if (Accion === "Guardar") {
    let comment = $(`#editar_seguimiento_eo_${Id}`).val().trim();

    var url =
      apiUrlsho +
      `/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=postSeguimiento`;

    let data = {
      Id: Id,
      DiagnosticoId: DiagnosticoId,
      Comentario: comment,
    };

    let headers = {
      apikey: constantes.apiKey,
      "Content-Type": "application/json",
    };

    let settings = {
      url: url,
      method: "post",
      dataType: "json",
      headers: headers,
      data: JSON.stringify(data),
    };

    return $.ajax(settings).done((response) => {
      console.log("Seguimiento post editar -> ", response);
      if (response.Id > 0) {
        $(`#seguimiento_eo_${Id}`).addClass("d-none");
        $(`#editar_seguimiento_eo_${Id}`)
          .val(response.Comentario)
          .addClass("d-none");
        $(`#ver_seguimiento_eo_${Id}`)
          .html(response.Comentario)
          .removeClass("d-none");
      }
    });
  }

  // ELIMINAMOS UN SEGUIMIENTO
  if (Accion === "Eliminar") {
    let comment = $(`#editar_seguimiento_eo_${Id}`).val().trim();

    var url =
      apiUrlsho +
      `/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=postSeguimiento`;

    let data = {
      Id: Id,
      DiagnosticoId: DiagnosticoId,
      Comentario: comment,
      Deleted: 1,
    };

    let headers = {
      apikey: constantes.apiKey,
      "Content-Type": "application/json",
    };

    let settings = {
      url: url,
      method: "post",
      dataType: "json",
      headers: headers,
      data: JSON.stringify(data),
    };

    return $.ajax(settings).done((response) => {
      console.log("Seguimiento post editar -> ", response);
      if (response.Id > 0) {
        $(`#TR_SEGUIMIENTO_EO_${Id}`).remove();
        COUNT_SEGUIMIENTOS_EO--;
        document.getElementById("IEO_SeguimCount").innerHTML =
          COUNT_SEGUIMIENTOS_EO;
      }
    });
  }
}
