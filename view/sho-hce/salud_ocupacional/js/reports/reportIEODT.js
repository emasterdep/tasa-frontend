const reportDT = jsPDF;

async function pdfIEODT() {
    const doc = new reportDT();

    let i = 1;
    await headerIEODT(doc, i);
    await bodyIEODT(doc, i);

    doc.save(`reporte-investigaciones-declaracion-trabajador-${moment().format('YYYY-MM-DD-h:mm:ss')}.pdf`);
}

async function headerIEODT(doc, i) {
    let enfermedadStorage = JSON.parse(
        sessionStorage.getItem("enfermedad_ocupacional")
    );

    const logoImg = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABoCAYAAABLw827AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAACH0SURBVHgB7Z1bkBTndcdP98xw3RULGCywBbNyJMqQFIsARZVyzFDlJykVQSIsO6mUltIlsf0gcKK8MuQtUVKgqsiKBSpWD6nYRg6LK9aTK4ySSkWxuIwSIRtJ1g7YBhkkWMQuCzsz3Tn/r7+e7Z3t2/R0z/Qu36+qdy49OzM7O/3vc853LkQpwTTNAm95UigUirQDseJthLc+UigUirTDYnWNtxOkUCgUaYfF6phpcYQUCoUizbBQ7TGn2EcKhUKRVhB4r9bGzIuXf6xES6FQpBsE3KFS//d+0fzNJyds0XqCYkYG+AdIoVAo2oGF5AwsrP/530Fz7CYWDUUgPjZx4efawVuRFArFrEOn9PHGXT3rqVYfp5+PPE+3J68gzeFEHDla/BwH+ALbQVIoFLOONApWefHCPGUziyBW9DMWrVr9ZluiJV1ApEvs4W2/pmmjpFAoZh1pFKwSfixb8qC4cXOiQiO/HsLVPFmi1VJiKQL5fHGGRQ+XQyxWQ6RQKGYlqRMsFpQKX4wuYbfQ5srVEv3yo6O4muftWMinsl3AE59cf6uvVvu0wtf3k0KhmLWk0cIC5aVLtk67A4J16crruFoISix1uoCwznrYxVww/+5XpRgqFIpZSloF641sZjEtWrh22p0Qn3F2EZlBrxwtpwv4zgf7Cc8zf96KCotVkRQKxawmrYJVwo8lPRtm7JArh7habBYt2wXEyuLb554Tj7vn7l3YpVxBhWIOoFEKkYH1a9fHztLZD2ZqDVtMtHHd82IlkRnk7Q3e4CYWOF5Fv7jwIlYWafP6F/HYMltXm0ihUMx6UmlhybSDip3e0AwsJ1haEuRUneGtgDjXOSsNQlhWEDZmLykUijlBWl1CIONY/a47P2Xra+TXr+JqH/K0PrjwHXslkYVqpe0Kllj8SqRQKOYEWUovZd6eWM6rhRAnNy5d+TH/NHn/u3YwXnDP3Y/ZV1XsSqGYQ6RZsEr4cZcjH8sNmerQYBG7kSuXFcTvK+tKoZhbpNYlZLGBhTXqFcfy4ov9z9lXlXWlUMwx0hzDAhCtRplOELCsZKC9rKwrhWLukXbBehs/lgS4hTYy0A5eIIVCMedIu2CV8KO5TMcNh3VVUQXOCsXcZFYIlluZTjMO60rFrhSKOUqqBctOIMV1tzIdm7t4n7SuQIkUCsWcJNWCJWsF87gOK8uLlcu22VeHVEcGhWLukto8LPRe54sirqM+0M5ibwZZ7TLvCrxKCtr8x4d36Lr2KM0S3jr65G6KyNZdLw8QZZ712m+aVDn52pOpCRM8tOOlfDWTGzB1rU83jbx1r76EyLhuP8bQ9Arvq9Rq2XJ5eHfquuNueeyVfZpmGRLu1F946+gzZUqAVAqWbIWMzguibhDFzF44storKpXBQtPMAdMqCp8tRBYsg/RnNTIHPR+gEQ3seOnV8vA3KtQFBnYc6dNzxo4M0TaTzB01oj5NvC0T/yj5KJOcfQh0uS+bq7MgH66YGpUN0zhuVHPD3Rawga+9lKe6WTR9HmMaOsR3DyVAWl3Chiv4zgdFUczshSO2lcpUBjVOLDkgBnyY7wh6XDabHaQOg/e25auv7GPRGWEBOmJaotpSe29g4jgwaYdO+hF+rmubd71yhAU4T10iWwv+LDWdnsDfTwmQOsHiA3yQpHUAN1D2vnLFkcoAhillyBhcgRSJAMuFwoiApj2b1AHkxtbHDz8LoSJrnFysrwtrMpvLjbAYHuiGcLEXE2ZOaF82W0vEwmrbJZS9q/K8wZLA9Y1yV15uflR4s1cCz8tL0ZQPwye84lY2y6bys0ppC7bLAbBF3raTIhHYcgk7ZBcH0CAlPN4N7lK2njtiGh04SZnmnlwut4NjeDuTihc1s+WxQwUz+Ji24JMEyRh0nLQkWFKcCrxhWS5PlkjlKTp5rx0/m+p35QqC7Q7BSlWwXYrVkLzZkS/TnYYVS2lBGDSxCJGYYOH95Oq5E2Z7x0NLWK+ln9ny+KG9J7//dKJiLF6PrasWOn72QeBOvvZ0iWLEV7CkQMHshkAVqOmfcePmJI2N3aZz56/S2PgkXbxyQ1zifjDGlzfGJxuPX7WiR1z2LppHPYvn0eoVveK+Xr5+f35543HowODnCoKmcp0SpYQmsap0YwaiwVarJvPX2gQxIk+XxoznNSKRqedce/qzzzSMmI/LnkISBxDohlhNw9AOsGhRkqKFv1Gruy7k4PuNk3Jhxh5Nw/+oRDEyQ7DkEAdboAr2/Zcu36BT735E71U+oUsfj9G5ylVxX1zs++aX6Q8K9wmhuij6XPmzYir3KjXuoC1W127WaOki8dGWqAucfk18cdv+8vIBXpRfuhlopFVOHn2yn7qEjulJbjsM8wV+zwVyEVrT1CBkJYqZjJE7EEas+DGjOmnDddN4m99/OVurVd5sWr1E2kMtm80bmp7XTKPAf8s2LYwQQrQeO1ROQpCBXp9XsFYzmzAhVuZx+Zk3U0DsMM6VTXFUyZUsmMyDJD8ciFHp5AU6/e4lOvXOpYbVlASrVvYIsQKXr5YCrSskkTpWBzHSa7Db9YO2WN24VaeLo1VbsN4gRew8sOuVQV51y7vtwwG7+bFDx92Cw3L1qhjnAYT3olk5g54IS9SgvfV6pnQq4LWlgFXkzaHGa5C5L0i4+G/mFcQjm5JIfcjw67unMphv1LK1YY7dHXDbK4PvRYoJ3eH2FchxVrp4ZUxcrvpMD93fv1y4bUlR2GrVCUKoIFhBLJteDJ3n7Qj/HSNyhbHj8OsiwDh0q2rQ27+6SSt6c/auEilixzPYblqft6npJY9fjX31Cgey1z5YVPzl2Hvq6FP9p374VOQcqtNHnxzCc/Bz+SbAwspLYnUuINheKn/vGxXP8EDMK7RZGWMp2ndIl3Bg84ZV23jD9caLwR2EkJ0+e4ne47jVKb6Mg68//Nvi8pPrPw20roDdhVTE0DhGJmNjebKEC1+g/Z2yuOTrFSFWJ8+P0wRfLlucwa6KKhOKH79gu2la7YiMqj6s5+pH3B4jLa8ixYCfpScwaPfJHz49TDHB1mORxYO83HSBJRAH47Sy/ILtDRfUoON8JnGrOOjTc1UYREMUAzPysJAtzttB3nbytpTvwogsTJ4Z5sD4KKyhbw8+RP+072F66wdPikvcxv1w7VoFrqAdjG9ud+yF7Q6+8dPz9Iff+j7t/85/8O+O2bvzNGVxJZq0aYtVzWBHni0riNXSRRnK6uLfe5wUsZOp5jwtCE0zhTjIg7Xk9hhYCrAYKAZ80yrYGoJVRTED0TIN3yTpvkymXqCYkMm5g647zanP2P7s3dBJD5t+Ekhg4ihaFXsIWAn72QpjC2kDPf/cV+hH//g4/fPf7RABdAhYGDdym3QHw8SuwOKF/Y1k0VPvWhbev5XeF8L1F/tfp/fPf2I/NM/bGYy1l6U+sWKLFa6f/fUEIXYFVvfNtx+SukTWuYCuk1eN5Oi0gLNpep8w/CyU1vA8IdZq2cRW7DgWViRrdc6VOOtIZXKuB1Ofsfzsvd5TIb6TRIs4BGw7bzAlkBgJxcfSpkhPgNUEAfv3I38mLDAImjNtwQYWWaEhWOHi03f1fLFxvdklxe0/eW642eIa5O2EdHVjQU6YLuL6ud/costj1ca+lb0i2K7qGhMARd1esRSTV9+ct1kwhsjnAGo3rjIgiq49Uj7Y8kiy5k88t2l6WlmoWaSY0K34rCu1Wm36SdnvPVkrtO2/H2oT6ULukdOVscyNQtaSvR8WGFxGWF4/evFxYX3hPrFvvXUJy8prlFcztjsIQXKI0jRsi+vQ0TP2XXmyRKvtMyssNpKFnb/4+DZduHq7sW/1knm2O1giRfx4W1dIZ5hmUeGgNn2srHaD01lT8xE8M/HVYV6ZG/LZ3fd7f3R4LbWJEGXN04osNxeU+72nuOoLY60lRJAZwW5YX3wT7iPEC1no4myDWBWsL1hdEK9ndj0gfu96SLECmKIDzlU+CXzsy0dPC+F6b+qxRf4Sn4jiImI1lTco4CBuQ6w+vHJr2mNW9zVWB1OVeT8XEImLHrEUrFC5xYs0v0BvgvWFpq+YxYPvyhxTzRhLqU0ypHtaVwYZL7i9J/I+WceyQptY8TNWH6V4DcrY105qEi872B7WHUQ5TiN+FXKFElbYn/71sNPaKpBlbeUpJPKxeAJxtnETq975GTv3SrmDCWAlLnrh/nkHxFViDU474RjSEuoMZa8d9Tq1veCk+xTuG9V6yXWHj1Wrae3H1jrWrYHf7LBDvGB5iQ+7Vh8P7Q72SOsKvH/+KrUCrC0Il3Qj82SJVuA/VYrVCfk7rmIF1ixvBNv3kyJ2/PKdNNPwtmh94iqa+zJ8KGqa6RejKlAnMNgA4NVIg7Td2Pj69my12o/t9L8+M0RtYKVseOVemce9+ov5xQ75+QbaDb53pYEfLC8pFgMYMx8W5yCK90aCXcJm4Br+efHH9N3iI7Du8mSJ1m6IqdvjZaD+GMngqpdYLczpHL9SyaJJ4Ze4KNxBn3IUrNZlc3UvsYteX1jNVShXJ/f3ZOaTqlt0It3gRFaj/VI2DPJOYUDskBdHXvU8GbRZX9jNfliiDU2U+NV7lauRS4UsF/EYld5CNxshRMdkWc00ZPb6CQoQK3DvigX2VdVTPgFMnx5MJhm+Fq1fTpb4/YirV9ZKnbdLZpXJdK/RXjuI5FwPKxEniNNH/a03v5wsanOFtpuCVcCP8YnzoX9hAcewwI3x29QO6CDx3PM/EauJkiHTUQ8mVxMP2rf9xKrJulIDXGPGL9gOPGMpDvwSLdtbvfKO18AizOVyJ5CKQbMMz04YguD4LCxLvwWBdoLvXREsZ+zo5sRI6N+zXcK4CrGRr+UQLSSYFniDC1i07/QTK+CwrpDe4XnGVUTDP9juHUtxgqJj8gm+y+Z+LSOTQz1jWRAtFsRjW3YdnlXC5Rds16ke6qSsmaZ3XNFq7heJbllYefwYn6j49mt3ggx3GySbIi3C7vDQDg7RwlkWLmDji/XOxQlfsWqyrlSwPQH8EhdNI1y9qMjJMrxTTUK2/XV9Xr+gvoMChGvrrsMj6MkeV9Z3Evgl5zLlsN1NAzL9+6IKeLcES1hYYUpxbDKZRdNuIyUCSagQLrcs+laAaDlytQi1gShkvnTd35LbeE/jPZVUKkP8+CUueuVeeeEXV2ln9UoemKEOYmFxYRiFpp1gq+valq8ePvbAY4f2WKPKUgK7yF673HKvvAiKHUZdoe2WYImAOyyssCyePqrezukSwmXXL0YpvrZBHaKdOY+6QDTh8wNZ7ci9kijrKgH8EhfDxFKcyBU779+JWF+IA7OWqe6M0H21T0zD0bQDJtocQ8DYdUTTRIhnJ4dm2Mh4oaflEyZeOA3/djiR6gu7JVjin4EcrLA4Jz/bOV1klQIJ4YJ7+N19j9DXHt5AUUAgHikPiI8hAfQLK+Z7Phau4L1T+4eUdZUMfrGUenWy9ZOEX0F0G6tXyPCuZ6rbI4iWE2teAoSTLTCM9IKAddIC84sXmqQNtTrbEYNgyS/GF2GFtqsxrFZcQsc4r8YHIEuBBslKRK3A2vrLwYeEmxjF2oKF9TfsHoJ7P7OAli52T1NDoB2iJVHWVQL4Jy7OrGMLQ0BBdFurVxAtNNkLaP3SKgXbAutE/Ctycq4HgbHDCCu0XRWssAF34BCsSvM+2awPxddCPCBcaHXz9K5N1Cqln56n771u5YZtWLWQcvr01mVwBZ2BdpV3lQz+iYtGJFGQcRXveFMbq1c2HFfbwy5iv7NXVBw441+2eMWZ5xWUnBs1CTYgJ6vlFdquDlK9PXmZIuB6hpS1i0Wy3MQK7kNxdRRr6x+G3hRBeOH6rZxyDZtcwQolPOfuTsUvcRG0HEtx4h9X6YvDgoG1dfK1p7ZrZGwy/Zb3I2KLlxyoui+OeJfpu1IaPeQRInbYUn1hxwVL9pBvmVwmnOhINxGiJc7CIij/tztbjm3t/85/iss1S+c3XMP7757mCu7uxgivOwG/xMUosRQnAQXRcTb3I6QAnHrt6UFYXFatXwJlW6ZZzOXqZ9qJcwUl50aKFzoJiB22cpLohoUVSbAcaQ2VMI9Hjy6yOqOKzqeIbX37iYcoLLCwUDAN4Bqu++wCWtkzldGuAu3J4RdsjxJLmYF/7lQh7iA3LC4MkoDVVatmlqJIWcS6YhIwWFxipZGtLYqAb3KuaEYY/QQBgmKHpId3xbvqEiaJLFye9kF8/ZENLbmIiGUhEA+ras2yaa5gkRSJ4BdsbyeW4iSofbFh6IOUEIij4W9ArAsCdvLoUxoEzDDNvRAH0+/ADoKtrSii5RdsN7T2TxBBzRSR3hHWre1Kt4YoOILuvsh2MGhh3FgyRU7VxKQpGuzBRUT6w189/5NpyaJuINUBSaVoOCjBl2m7cgWTg4/eR733UQWCRm1jiOfyEsYk5hf64YjzCCGVLhJvHN/RWuxrBdF6/ND1sFOgA0Z4IRduSRyfuabRNYw18iLs/MJZI1gOPKulZYeFIkm3E6O3zn001XP9xq35tI7jUHayKYLr9oqgF2gUiM1u68w8y6+DjoNltUIYL3Icul9uToFXDwsUA6b/bnv1qiuLKg4BK+IzgcsmSpTCipehcSD+peEwrpzfCC+xnz8DPejTCkPQU1grtMWAR3XeJXQe5M3lNlGRRcvoCIovWB9Ka35x5Ta9+eHYtAERF67xfSNjQsgA4lphUh8OvdboVgohxJkABdIjcpQY2tPsSXqk2J1AtpYdpLSgxTd5ph0c8a9NLaw69vEK4pGgB8kRXi0nbyZEqBXarsaw7HYxUZFChYJlbEIwLt+oCqH68ONbVDVmyjrKblAnaIsWUh+CRAsWFlzDf2FrDNcd3SLyZP3D4YJipNg12TO+KN9bx8srZjNRi5ATopC2ImXnqmOIrPrAzH05wis939EQK7TdcgkrvOXDxqWakXEq/HGD9n2IU8GqCqoBBBNySvOWtYtpAQfUIVqrV/QKUfLC6ujQaEUjCq5Xc/D+vrXLrOvsZvKlVV5hbfvke0WiIjbbjSyTYgZBsZSuYK1elShlwOpiMdqUzdUbJ2o3gtxa30Gw3UGs0Pp1hOiWYIlgZkQLC1+ihhK3IlROIFr/ze4hRAtFzHarGj/RcoKAPTZkxjuxheyB9avofohZ//KB3kXz8KUaxH4WMDvbGgJWIkvE7vggflAspSuYlpXSqeB7K+A98XvbzqI1Ql5WkiZOnK6CJZJz6x3qPd8Csr4wdYL1Nm8DES0s8c+JKlROanWzYWlFES033ISMBQvCJQRs1crePr4s8O0C3++0wkpkiVjlTrPCZLB90PMBVnZ6iRIAB4hPq5O+5tUrCFguV80bhp43da1PN4082ypLeBWszzDM40mMp/cCosWW6QterpRG+kav3/VPzuXvoGnupqTQtMacBJd9GL920Osk0VULa7FjCk5YEIPCtOV2hMqJLVoP9feIfKs4RKsZxLzs1UaARFZYYs9w7Ow+vpQWGLhO7eThzFKsxEX3ZSQ5ZKJICcEHRzmr1+EaeR5A5BAsuFkm6QdY5JCCIdbrxdgJazf+fx0TLIDhpVkP8cEwDK/fQ3Ku18Id+uSfeu2ZEiWEn8hSwAptt4LuwoKAhZUNuVJoF0pDqOISK4Di5oHPL3KW3AjRilI47QfSIvCcyOk6/uLj4pLdxhKLFbLx+zE5G7WQd2IGvV/iYjt1bGEI6ihATatXhqZ7nlA0vfUBve0ih5e2dJIL6ITRXq1mCIISd/1WaLtlYTVcnkUL+0PNJazXx4W4ZfX4Ih0Qqc3sDjrECiUb23gbsKdSOwawtgQsqM3r76ZtW9cKdxBuIVlfLJyB4foNq9hVcLA9llKcAKyOAj7lIY7RVAbVy7rneV7bRl0A2fFaC6t9vsH2GEpxghCu7K7DJfIuwfIcv9ZxwZIrfI3cD3QS/bSFUV+5TDyCtWbZPPrCigW2AEI40CrmoExFEKsvEC2U5jgGVXjCsSn+lNfQA2xJwZqSAgUqvKEsYVjVH87EL9geNHMwLnBg8AGEk6jXilsBrVzEgewzj5CkNXayA+/ZSStiFRRsj6MUJwyopWS3uuC5X9MhqqXm+xMXLJlQWSCrLTIu8879PSHjWLcmL0sXsj3Bggu4/nMLnYXMFd522oFuWD38nreTNZo+j4JpzEFsLuNBHArW02ZeDdz24FqnQIESTVlRKo3BAytxsb7Da78WbsBDPKDWTdN8UgREUmsx0DrocCqEFCAvwZrx3RPJud6H0GjQzMG4wCQjjh3CUHB97xwfRH3h3ubge2wxLFgmECfeBnk7IBMor5F14COxcpDsxn2GKTawdMnWUM9vdyddkIv+ltF870v39TrFCu7ZpmZRka4aRKsCYfr7574yo2AaQoY+8oh3sVjZDfcRj1rKv79dxqOUWPkQlLhYq9U6FsAOEVd5tpGIaZVmuWPSjk4mnAaMQZtRxqb5DqXVOrrCGRQ7dOsA29LRD3fOIUp7pDChNAW5ILY4oSQAL1QgR00fMtCxuocVuRPnPqXyL60gOnq1L5o+YMKVugy65yLEsJYuyojUhQ2rFzpdQPSz2ukVR5IlRBCtUTGh5xtfnrb/1LuX7KuwAvqlSB1UcanwdDuW4iRoygs5umPW/Lto4stzoFNDJHw7LTSNlA8Y4dWReOG01wv+HGfEFbMyplQgK9gM+mjqrNfXtPkCYUI5zI2JOt24bYjkzNHxmnuJzO2pOMDyJQ/SzYAJ0Ldkd9JWXEIIFeJUGCrhAAJTDCMsEC3+fDCh54S9ymcH4e0UBWaJEqnWSUssZRrI99K0gud+a/XqYPnoM+WAoPFAJmfAq0gul4lBKxl5/LoyY7VPJ8/Vt07FC53I2GGJvD/HGTHBrLQkhvgPh9rBRMcfVUB+Ety2q+N1IUQTNWPGs01MGuIxVX6sXZsXFjw/0hMgJnf1rA98fN3R/x1uod/reQhViazAeolaAI830auI3VoE4U+/+5EQKwTjL125QatW9BZI0TL+49BxsOU65p7YyAPIM65CztWrAHFDC2N+roFatbozCUtR9L0yzaLX/ubOrIHJuQb5dQVNDit2WPDc71ihBfrU/aIn+hDcJL65lC2Z3SwMJfSQQh/zNUvn0YKsxlZTnS6OTooNgoNEzlbFygZuIljSsyEwH2vcYYG5rRTCVcRoru3r7mL3r8cpViWyelhtj7pKB1ePZMtlxK3sAPupsx/hIq+KnFvHr6uodbB1qRwmINBvj6YSoqUFJokO5HK5Ew/senmQYgKu5uavHj7mJ1agua2xf6wLBkHVP4aXEIHdSJuKuF1jWA7xQgynn7fdvQsyZVgtW9b00Jd+q1fEg1b2ZiPFlGwuXp9q/bJiWcH3sc6BFb3zrbe9QHQCnSfiUwUWKozmcuRp4cvUllA5kS2XS4hnPS1ztBxxrAFShCYocbHTsRQnQcF352iq2mRmd1DXBPydOulHMOkGwhV10g1eE1YVagc1M6AlDFt/zVadb3Juh+OFTkIE36eNXwtMa7BdRrLcxjxf7lmY0x9duGReHqtuAJbWhauTDYspLE63cBmvFl668rrPY8dFtjsssVV982jd3QvdkkhLZKUTJBX8hvV55uuPbMi/cfK8M441QCms6k8rfsH2bsRSnIRIamzUF+KxW3e9vNMkHXl7vla2LVw6n2TF85taGY0gOfA8mq3VKm86BOMhFrV6LtNXp8yAbtJGVnD+ftULIfvolU82lTIFJed2JV7oIETibqO+sKVVQjmRZo+cSgPrC39oBYLTMz/cUyG+tHRxhnoX6MJCcrqFd/X4T7aZlKkNy/j1HGJVImse4XZHOkEi7oR8XoiWcA3tOBZR+qre00rQCK+kS3FC4T8KbNrqFVqhGGTspdYosAjtYWvtGOYM1jCua9dh095wG0MlWNiP4HEU8vsFsUfMbMb9AX3Gki7FCSJwFBifDPRcVViVkZOa4GbJqcsV3L42UQ/8nS98Zr6IL8GtfKi/l36fXct1n13Y2L88ICdrbGLEvlohawXGmfNUog4gc6v2wjXE6LDSWyK2tpEUoQgKtrc9UioGAkeBNdUXItmS3Zqd1N3C9XK9Wt3e7NoFjfBqd2xabPiPAmOhEpnv7SWOymBzAUH3a+P+BcmYlowR736sWLbNN/g+PlFpXJcxtq58QWQQfhirhhcvj+EuFXgPie5vLZRTcfCAoCz7pm4DaCtTy1Q3hegEGjsoc6lVM9vdPrugYDsZZndWB5sIE3zHSaLd0pwCflwNECsO2HOQviFEOIPigHce4Lh+BkmkCL57xbImJz+2r+ap++zuXTxvYPXKnry8XaAOtxZJGsMqqq247jTNCrUIvnByPbni+pQRR9AnAZJDM+TrSuWbm/vJzgn9/HcWZY1knhLE7lt16ofeMT9MIfISUQ7ej3ayf5cf+Bw3P3YIKQ6eBeSoPWyrMI+tCgjPs2//6qZnwL2pI8J+OU7e7bmG+OIJlOCcevdbrs+FWsLN61+0b+b5ufyzTRMGfdtvjE+eQBzr/vzyF+RKokJB9rQbFox9sQsXr+ohUN6pur800a5gIe17oHTuU9ds9rBiJZ+rQFaXBPr5yPN09fpbro978HeGbLcRZTVdPzvYos1bSaaBKBTTQJ/yuqkVdNIeNTUaaKW7QgOTeFWRsKo4fLKLq6jdJrJgyRSHEaQloD6wGeRn/e69PaHEyvGcEKzC9bGzdPYD99jrwLrn7drDvTKW1FVk7ArC3cfvZykpFAHAlcxmawOGpuc5ANWnuwgY3HH2gUYzVC9Xq7lKGvvKd4N2YlgiWdLLFdx4T6OL5wthxEqClb8RpDis5FjW5aulGQ+AmEnBylMKkO1o8L7RnWJtt91URfoJUWit8KCdVUJRSHnlxsyA+7rPNur4Xm0lriOTVEXg9Z67d7k+xrFSmJpUAplSgfetXEKFIkHaEawBURTdVEeIXKs1y+bj6qsyT6tViryNIsDuJlqOmsK0lcMUSZXoKBSJEkmwZPxqoDmdAXV9MteqHFGs7GxyEcBateLhGXlZ4xMj9kCKvjTlPsn3PafSGhSKtBHVwirgx8VPp+JX6OIps9aRCd6WaySD6SXkZX3e1cpqZLynyqLRVM92hSJRogrWNlG4LC0sBNc3fE6IVYWsmr44VjREfdbqFY/MqDG8mV63UKFQJEhkC8ueDWjnWmV1q6VwXOUysmZPuIb3rfnmNNcwjYF3hUKRPC0LlpyCk7/Mq4OOxNAKWWJVoRiR6RBlBOCdruH1qbFgysJSKO4golhYQiTQk32jNTG5QgmIlQNRBe90DVG+I6fo5EmhUNwxRBGsR8dYrNbyimDvgkyFkhUrOzdrhmsorSysFAaP3FEoFHcmmDV4c7JuYrSX38SOBF4XU5nN0RvvmP91Zpf54a+OmJJBUigUimbkTEJwTcayOvnasKbO4MUvXPqBefLsN23B6no9oUKhSCFyeGrHxcrx+nlp2Znvn3/RvHX7sikLphUKhWI6cspzV1fmpJV3rVobM8duCu26RgqFQtEMi8MOSgHonWVORwXeFQpFekGw3SFYg6RQKBRpxiFaKvCuUCjSjxStM6RQKBSzgbTE1hQKRbL8P6aib+ZZ6a6pAAAAAElFTkSuQmCC`;

    doc.addImage(logoImg, 'PNG', 20, 16, 30, 10)
        .setFont('times new roman', 'open sanc', 'normal')
        .setFontSize(12)
        .text("FORMATO DE INVESTIGACIÓN DE", 56, 20)
        .text("ENFERMEDADES OCUPACIONALES", 55, 26.5)
        .text('Version Nº: 00', 131, 19)
        .text(`Pagina: ${i}`, 164, 19)
        .text(`Código: ${enfermedadStorage.NroCaso}`, 140, 28)
        .rect(53, 13, 75, 18)
        .rect(17, 13, 36, 18)
        .rect(128, 13, 32, 9)
        .rect(160, 13, 32, 9)
        .rect(128, 22, 64, 9);
}

async function bodyIEODT(doc, i) {
    let enfermedadStorage = JSON.parse(
        sessionStorage.getItem("enfermedad_ocupacional")
    );
    
    let gender = (paObj_hc[idHC].a.Sexo_Trabajador_H == 1) ? 'Masculino' : 'Femenino';

    doc.setFont('times new roman', 'open sanc', 'normal')
        .setFontSize(12)
        .text(`CÓDIGO DEL CASO: ${moment().format('YYYY/MM/DD')} Nro: ${enfermedadStorage.NroCaso}`, 95, 43)
        .text("I.  A SER LLENADO POR EL MÉDICO OCUPACIONAL", 25, 55)
        .text('1.  DATOS DE LA EMPRESA ', 25, 65)
        .setFontSize(11)
        .text(`Empresa (Razón Social): ${enfermedadStorage.Empresa}`, 25, 75)
        .text(`Dirección: ${paObj_hc[idHC].a.Country}, ${paObj_hc[idHC].a.StreetAddress}`, 25, 80)
        .text(`Tipo de actividad de la empresa: ${paObj_hc[idHC].a.BusinessSector}`, 25, 85)
        .text(`2.  DATOS DEL TRABAJADOR Y SU ACTIVIDAD`, 25, 95)
        .text(`Apellidos y Nombres:`, 25, 105)
        .text(`${enfermedadStorage.A_ApellidosPaciente} ${enfermedadStorage.A_NombresPaciente}.   Edad: ${enfermedadStorage.A_Edad}.   Sexo: ${gender}`, 25, 112)
        .text(`Puesto de Trabajo: ${paObj_hc[idHC].a.PuestoTrabajo_Empresa_H}  Turno: ${enfermedadStorage.Turno}`, 25, 121)
        .text(`Jefatura: ${await getArea(enfermedadStorage.B_AreaId)}  Gerencia: ${await getGerencia(paObj_hc[idHC].a.GerenciaId_Empresa_H)}`, 25, 130)
        .text(`Fecha de inicio de labores en el puesto: ${moment(enfermedadStorage.FechaInicioPuestoF, 'DD/MM/YYYY').format('DD/MM/YYYY')}  Turno: ${enfermedadStorage.Turno}`, 25, 139)
        .text(`Dia del Turno en que se encuentra: ${enfermedadStorage.DiaTurnoEncuentra}`, 25, 148)
        .text(`Fecha de inicio de labores en la empresa: ${moment(paObj_hc[idHC].a.A_FechaIngresoTasa).format('DD/MM/YYYY')}`, 25, 157)
        .text(`Puestos anteriores en la empresa:`, 25, 166);
        let jobs = enfermedadStorage.PuestoTrabajo;
        let positionY = 169,
            positionX = 30;
        for (const material of jobs) {
            let positionTable = positionY + 3;
            if(positionTable<173) {
                positionY += 3;
                doc.text(`• ${material.B_PuestoTrabajo}`, positionX, positionTable);    
            } else {
                positionY = 175;
                positionTable = 172;
                positionX += 53;
                doc.text(`• ${material.B_PuestoTrabajo}`, positionX, positionTable);
            }
        }

    doc.text(`Domicilio: ${enfermedadStorage.A_DireccionPaciente}  Teléfonos: ${enfermedadStorage.A_TelefonoPaciente}`, 25, 178)
        .text(`Nº de trabajadores en el puesto de trabajo: ${enfermedadStorage.CantidadTrabajadoresPuesto}`, 25, 187)
        .text(`3.  DATOS DE LA ENFERMEDAD`, 25, 197)
        .text(`Fecha: ${moment(enfermedadStorage.CreatedDateF, 'DD/MM/YYYY').format('DD/MM/YYYY')}  Hora de la atención: ${moment(enfermedadStorage.CreatedTime, 'HHmmss').format('HH:mm A')}`, 25, 204)
        .text(`Actividad rutinaria en el puesto (Delaracion del trabajador)`, 25, 216)
        .text(`${enfermedadStorage.ActividadRutinariaPuesto}`, 25, 222)
        .text(``, 25, 225)
        .text(`4.  MOTIVO DE CONSULTA (Declaración del trabajador)`, 25, 235)
        .text(`•  Relato de las molestias o síntomas`, 25, 242)
        .text(`${enfermedadStorage.MotivoConsulta}`, 25, 249)
        .text(`Insumos, materiales o equipos empleados en su puesto actual:`, 25, 263)
        let insumos = enfermedadStorage.Insumos;
        let positionY2 = 263,
            positionX2 = 30;
        for(const material of insumos) {
            let positionTable = positionY2 + 6;
            if(positionTable<282) {
                positionY2 += 6;
                doc.text(`•  ${material.Description}`, positionX2, positionTable);    
            } else {
                positionY2 = 269;
                positionTable = 269;
                positionX2 += 35;
                doc.text(`•  ${material.Description}`, positionX2, positionTable);
            }
            
        }

    doc.addPage();
    i++;
    await headerIEODT(doc, i);

    doc.setFont(`times new roman`, `open sanc`, `normal`)
        .setFontSize(11)
        .text(`•  ¿En el trabajo se ha presentado algún evento no habitual durante su labor que haya generado la lesión?`, 25, 45)
        .text(`Si          No`, 30, 55);
        if (enfermedadStorage.FlagLesion == 1) {
            doc.setDrawColor(0)
                .setFillColor(116, 255, 0)
                .rect(35, 52, 3, 3, 'F')
                .rect(49, 52, 3, 3)
                .text(`Detallar en caso positivo`, 30, 65)
                .text(`${enfermedadStorage.DetalleSintomatologia}`, 30, 72);
            } else {
            doc.setDrawColor(0)
                .setFillColor(116, 255, 0)
                .rect(35, 52, 3, 3)
                .rect(49, 52, 3, 3, 'F')
                .text(`Detallar en caso positivo`, 30, 65);
            }

    doc.text(`5.  ANTECEDENTES (Declaración del trabajador)`, 25, 93)
        .text(`Si el trabajador tuvo puestos diferentes anteriores en la empresa, describa las actividades de riesgo`, 25, 100)
        .text(`relacionadas a la enfermedad actual:`, 25, 107)
        .text(`...................................................................................................................................................................`, 25, 114)
        .text(`...................................................................................................................................................................`, 25, 121)
        .text(`...................................................................................................................................................................`, 25, 128)
        .text(`...................................................................................................................................................................`, 25, 136)
        .text(`...................................................................................................................................................................`, 25, 143);
        let a = i,
            y = a,
            y2 = a,
            y3 = a,
            y4 = a,
            position;
        if ((enfermedadStorage.AntecedentesHCE.AntecedentesOcupacionales).length > 0) {
            doc.addPage();
            a++;
            await headerIEODT(doc, a);
            await generateTableIEODT3(doc);
            let i = 0;
            y = a;
            const invoiceTableTop = 60;
            const antecedentes = enfermedadStorage.AntecedentesHCE.AntecedentesOcupacionales
            doc.text(`Describa antecedentes de riesgo relacionado a la enfermedad en trabajos anteriores (empiece del`, 25, 45)
                .text(`más reciente):`, 25, 50);
            for await (const ocupacional of antecedentes.reverse()) {
                position = invoiceTableTop + (i + 1) * 10;
                    await generateTableRowIEODT3(
                        doc,
                        position,
                        moment(ocupacional.FechaFinF, 'DD/MM/YYYY').format('YYYY-MM'),
                        ocupacional.Empresa,
                        ocupacional.Ocupacion,
                        ocupacional.PeligrosAgentesOcupacionales
                    );
                    i++;
                    if (i == 13) {
                        doc.addPage();
                        i = 0;
                        y++;
                        await headerIEODT(doc, y);
                        await generateTableIEODT3(doc);
                    }
            }
        } else {
            doc.text(`Describa antecedentes de riesgo relacionado a la enfermedad en trabajos anteriores (empiece del`, 25, 164)
                .text(`más reciente):`, 25, 171)
                .text(`Año / mes	       Empresa		Puesto desempeñado	     Actividad de riesgo relacionado`, 25, 178);

            if ((enfermedadStorage.FlagDescansosMedicos == 1 && enfermedadStorage.DescansosMedicos.length == 0) || enfermedadStorage.FlagDescansosMedicos == 0) {
                doc.text('¿Actividades relacionadas a la lesión durante el descanso (días libres)?', 25, 206)
                    .text('Si          No', 30, 213)
                    .setFillColor(116, 255, 0)
                    .rect(35, 210, 3, 3)
                    .rect(51, 210, 3, 3, 'F')
                    .text('En caso positivo, describa:', 30, 220)
            }
        }
    let bandera;
    let ori = 'portrait';
    let pap = 'a4'
    if (enfermedadStorage.FlagDescansosMedicos == 1 && enfermedadStorage.DescansosMedicos.length > 0) {
        let papper = 'a4',
            orientation = 'landscape';
        doc.addPage(papper, orientation);
        a = y;
        a++;
        await header2(doc, a);
        
        doc.text(`¿Actividades relacionadas a la lesión durante el descanso (días libres)?`, 30, 45)
            .text(`Si          No`, 30, 52)
            .setDrawColor(0)
            .setFillColor(116, 255, 0)
            .rect(35, 49, 3, 3, 'F')
            .rect(51, 49, 3, 3)
            .text(`En caso positivo, describa:`, 30, 59);
            await generateTableIEODT(doc);
            let i2 = 0;
            y2 = a;
            const invoiceTableTop2 = 68;
            const table2 = enfermedadStorage.DescansosMedicos;

            for await (const item of table2) {
                const position = invoiceTableTop2 + (i2 + 1) * 10;
                let cantidadDias = (item.B_CantidadDias).toString(),
                    acumuladosDias = (item.B_DiasAcumulados).toString();
                await generateTableRowIEODT(
                    doc,
                    position,
                    item.A_IdDescansoMedico,
                    item.A_DniTrabajador,
                    item.A_FechaRegistroF,
                    item.OrigenDescanso == null ? '' : item.OrigenDescanso,
                    item.FechaInicioF,
                    item.FechaFinF,
                    `${cantidadDias}`,
                    `${acumuladosDias}`,
                    item.A_Estado,
                );
                i2++;
                if (i2 == 12) {
                    doc.addPage();
                    i2 = 0;
                    y2++;
                    await header2(doc, y2);
                    await generateTableIEODT(doc);
                }
        }
    a = y2;
    } else {
        if((enfermedadStorage.AntecedentesHCE.AntecedentesOcupacionales).length > 0) {
            if(position <= 230) {
                bandera = 1;
                position = 110
                doc.text('¿Actividades relacionadas a la lesión durante el descanso (días libres)?', 25, position += 46)
                .text('Si          No', 30, position += 7)
                .setFillColor(116, 255, 0)
                .rect(35, position -= 3, 3, 3)
                .rect(51, position, 3, 3, 'F')
                .text('En caso positivo, describa:', 30, position += 10)
            } else {
                bandera = 0;
                doc.addPage(papper, ori);
                a++;
                await headerIEODT(doc, a);
                doc.text('¿Actividades relacionadas a la lesión durante el descanso (días libres)?', 25, 45)
                    .text('Si          No', 30, 52)
                    .setFillColor(116, 255, 0)
                    .rect(35, 49, 3, 3)
                    .rect(51, 49, 3, 3, 'F')
                    .text('En caso positivo, describa:', 30, 59)
            }
        }
    }
        
        if (bandera == 0) {
            doc.text(`¿El Trabajador ha presentado sintomatología relacionado en ocasiones anteriores, dentro o fuera`, 25, 73)
                .text(`del trabajo?`, 25, 80)
                .text(`Si          No`, 30, 87);
            doc.setDrawColor(0)
                .setFillColor(116, 255, 0)
                .rect(35, 84, 3, 3, 'F')
                .rect(51, 84, 3, 3)
                .text(`En caso positivo, detallar las situaciones y fechas`, 30, 94)
                .text(`${enfermedadStorage.DetalleLesion}`, 30, 99);
        } else if (bandera == 1) {
            doc.text(`¿El Trabajador ha presentado sintomatología relacionado en ocasiones anteriores, dentro o fuera`, 25, position += 46)
                .text(`del trabajo?`, 25, position += 7)
                .text(`Si          No`, 30, position += 7);
            doc.setDrawColor(0)
                .setFillColor(116, 255, 0)
                .rect(35, position -= 3, 3, 3, 'F')
                .rect(51, position, 3, 3)
                .text(`En caso positivo, detallar las situaciones y fechas`, 30, position += 10)
                .text(`${enfermedadStorage.DetalleLesion}`, 30, position += 7);
        }
        let position2;
        if (enfermedadStorage.AccidentesTrabajo.length > 0) {
            doc.addPage(pap, ori);
            a++
            await headerIEODT(doc, a);
            doc.text(`Otros datos de interés:`, 25, 45)
            await this.generateTableIEODT2(doc);
                let i = 0;
                    y3 = a;
                const invoiceTableTop = 55;
                const table = enfermedadStorage.AccidentesTrabajo;
                for await (const item of table) {
                    position2 = invoiceTableTop + (i + 1) * 10;
                    await generateTableRowIEODT2(
                        doc,
                        position2,
                        item.A_Fecha_AccidenteF,
                        item.A_Hora_Accidente,
                        item.TipoAccidente,
                        item.EstadoAccidente == null ? '' : item.A_Hora_Accidente
                    );
                    i++;
                    if (i == 21) {
                        doc.addPage();
                        i = 0;
                        y3++;
                        await headerIEODT(doc, y3);
                        await generateTableIEODT2(doc);
                    }
                }
        } else {
            if (bandera == 0) {
                doc.text(`Otros datos de interés:`, 25, 130)
            } else if (bandera == 1) {
                doc.addPage(pap, ori);
                a++
                await headerIEODT(doc, a);
                doc.text(`Otros datos de interés:`, 25, 45)

                doc.setFont(`times new roman`, `open sanc`, `normal`)
                    .setFontSize(11)
                    .text(`La información brindada por el trabajador hasta este punto es real, es de su conocimiento y esta`, 30, 85)
                    .text(`conforme`, 30, 92)
                    .text(`DNI del trabajador: .................................................  Firma: .........................................`, 30, 102)
                    .rect(25, 80, 160, 25)
                    .text(`Conformidad del Jefe del Area`, 30, 122)
                    .text(`Nombre: .............................................................  Firma: .........................................`, 30, 139)
                    .rect(25, 117, 160, 25);
            }
        }
        if (enfermedadStorage.AccidentesTrabajo.length > 0) {
            doc.addPage(pap, ori);
            a = y3;
            a++;
            await headerIEODT(doc, a);
        
            doc.setFont(`times new roman`, `open sanc`, `normal`)
                .setFontSize(11)
                .text(`La información brindada por el trabajador hasta este punto es real, es de su conocimiento y esta`, 30, 50)
                .text(`conforme`, 30, 57)
                .text(`DNI del trabajador: .................................................  Firma: .........................................`, 30, 67)
                .rect(25, 45, 160, 25)
                .text(`Conformidad del Jefe del Area`, 30, 87)
                .text(`Nombre: .............................................................  Firma: .........................................`, 30, 104)
                .rect(25, 82, 160, 25);
        }
    }


async function generateTableIEODT(doc) {
    doc.setFontSize(12)
        .text('ID Código', 18, 68, { maxWidth: 90, align: 'center' })
        .text('Documento', 52, 68, { maxWidth: 90, align: 'center' })
        .text('Fecha registro', 83, 68, { maxWidth: 90, align: 'center' })
        .text('Origen descanso', 115, 68, { maxWidth: 90, align: 'center' })
        .text('Fecha Inicio', 150, 68, { maxWidth: 70, align: 'center' })
        .text('Fecha Fin', 183, 68, { maxWidth: 90, align: 'center' })
        .text('Dias', 225, 63, { align: 'center' })
        .text('descanso / acumulados', 210, 68, { maxWidth: 10, align: 'center' })
        .text('Estado', 265, 68, { maxWidth: 90, align: 'center' });
}

async function generateTableIEODT2(doc) {
    doc.setFontSize(12)
        .text('Fecha de accidente', 30, 55, { maxWidth: 90, align: 'center' })
        .text('Hora de accidente', 75, 55, { maxWidth: 90, align: 'center' })
        .text('Tipo de accidente', 120, 55, { maxWidth: 80, align: 'center' })
        .text('Estado', 165, 55, { maxWidth: 90, align: 'center' });
}

async function generateTableIEODT3(doc) {
    doc.setFontSize(12)
        .text('Año / Mes', 25, 60, { maxWidth: 90, align: 'center' })
        .text('Empresa', 55, 60, { maxWidth: 90, align: 'center' })
        .text('Puesto desempeñado', 85, 60, { maxWidth: 80, align: 'center' })
        .text('Actividad de riesgo relacionado', 135, 60, { maxWidth: 90, align: 'center' });
}

async function generateTableRowIEODT(doc, y, c1, c2, c3, c4, c6, c7, c8, c9, c10) {
    doc.setFontSize(12)
        .text(`${c1}`, 27, y, 'center')
        .text(`${c2}`, 61, y, 'center')
        .text(`${c3}`, 95, y, 'center')
        .text(`${c4}`, 129, y, 'center')
        .text(`${c6}`, 160, y, 'center')
        .text(`${c7}`, 190, y, 'center')
        .text(`${c8} / ${c9}`, 230, y, 'center')
        .text(`${c10}`, 270, y, 'center');
}

async function generateTableRowIEODT2(doc, y, c1, c2, c3, c4) {
    doc.setFontSize(12)
        .text(`${c1}`, 45, y, 'center')
        .text(`${c2}`, 90, y, 'center')
        .text(`${c3}`, 135, y, 'center')
        .text(`${c4}`, 170, y, 'center');
}

async function generateTableRowIEODT3(doc, y, c1, c2, c3, c4) {
    doc.setFontSize(12)
        .text(`${c1}`, 33, y, 'center')
        .text(`${c2}`, 62, y, 'center')
        .text(`${c3}`, 102, y, 'center')
        .text(`${c4}`, 162, y, 'center');
}

function getGerencia(id) {
    let gerencia = "";
    sedeAreaGerencia.Gerencia.forEach((element) => {
      if (element.Id == id) {
        gerencia = element.Description;
      }
    });
    return gerencia;
}

function getArea(id) {
    let area = "";
    sedeAreaGerencia.Area.forEach((element) => {
      if (element.Id == id) {
        area = element.Description;
      }
    });
    return area;
}

async function header2(doc, i) {
    let enfermedadStorage = JSON.parse(
        sessionStorage.getItem("enfermedad_ocupacional")
    );

    const logoImg = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABoCAYAAABLw827AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAACH0SURBVHgB7Z1bkBTndcdP98xw3RULGCywBbNyJMqQFIsARZVyzFDlJykVQSIsO6mUltIlsf0gcKK8MuQtUVKgqsiKBSpWD6nYRg6LK9aTK4ySSkWxuIwSIRtJ1g7YBhkkWMQuCzsz3Tn/r7+e7Z3t2/R0z/Qu36+qdy49OzM7O/3vc853LkQpwTTNAm95UigUirQDseJthLc+UigUirTDYnWNtxOkUCgUaYfF6phpcYQUCoUizbBQ7TGn2EcKhUKRVhB4r9bGzIuXf6xES6FQpBsE3KFS//d+0fzNJyds0XqCYkYG+AdIoVAo2oGF5AwsrP/530Fz7CYWDUUgPjZx4efawVuRFArFrEOn9PHGXT3rqVYfp5+PPE+3J68gzeFEHDla/BwH+ALbQVIoFLOONApWefHCPGUziyBW9DMWrVr9ZluiJV1ApEvs4W2/pmmjpFAoZh1pFKwSfixb8qC4cXOiQiO/HsLVPFmi1VJiKQL5fHGGRQ+XQyxWQ6RQKGYlqRMsFpQKX4wuYbfQ5srVEv3yo6O4muftWMinsl3AE59cf6uvVvu0wtf3k0KhmLWk0cIC5aVLtk67A4J16crruFoISix1uoCwznrYxVww/+5XpRgqFIpZSloF641sZjEtWrh22p0Qn3F2EZlBrxwtpwv4zgf7Cc8zf96KCotVkRQKxawmrYJVwo8lPRtm7JArh7habBYt2wXEyuLb554Tj7vn7l3YpVxBhWIOoFEKkYH1a9fHztLZD2ZqDVtMtHHd82IlkRnk7Q3e4CYWOF5Fv7jwIlYWafP6F/HYMltXm0ihUMx6UmlhybSDip3e0AwsJ1haEuRUneGtgDjXOSsNQlhWEDZmLykUijlBWl1CIONY/a47P2Xra+TXr+JqH/K0PrjwHXslkYVqpe0Kllj8SqRQKOYEWUovZd6eWM6rhRAnNy5d+TH/NHn/u3YwXnDP3Y/ZV1XsSqGYQ6RZsEr4cZcjH8sNmerQYBG7kSuXFcTvK+tKoZhbpNYlZLGBhTXqFcfy4ov9z9lXlXWlUMwx0hzDAhCtRplOELCsZKC9rKwrhWLukXbBehs/lgS4hTYy0A5eIIVCMedIu2CV8KO5TMcNh3VVUQXOCsXcZFYIlluZTjMO60rFrhSKOUqqBctOIMV1tzIdm7t4n7SuQIkUCsWcJNWCJWsF87gOK8uLlcu22VeHVEcGhWLukto8LPRe54sirqM+0M5ibwZZ7TLvCrxKCtr8x4d36Lr2KM0S3jr65G6KyNZdLw8QZZ712m+aVDn52pOpCRM8tOOlfDWTGzB1rU83jbx1r76EyLhuP8bQ9Arvq9Rq2XJ5eHfquuNueeyVfZpmGRLu1F946+gzZUqAVAqWbIWMzguibhDFzF44storKpXBQtPMAdMqCp8tRBYsg/RnNTIHPR+gEQ3seOnV8vA3KtQFBnYc6dNzxo4M0TaTzB01oj5NvC0T/yj5KJOcfQh0uS+bq7MgH66YGpUN0zhuVHPD3Rawga+9lKe6WTR9HmMaOsR3DyVAWl3Chiv4zgdFUczshSO2lcpUBjVOLDkgBnyY7wh6XDabHaQOg/e25auv7GPRGWEBOmJaotpSe29g4jgwaYdO+hF+rmubd71yhAU4T10iWwv+LDWdnsDfTwmQOsHiA3yQpHUAN1D2vnLFkcoAhillyBhcgRSJAMuFwoiApj2b1AHkxtbHDz8LoSJrnFysrwtrMpvLjbAYHuiGcLEXE2ZOaF82W0vEwmrbJZS9q/K8wZLA9Y1yV15uflR4s1cCz8tL0ZQPwye84lY2y6bys0ppC7bLAbBF3raTIhHYcgk7ZBcH0CAlPN4N7lK2njtiGh04SZnmnlwut4NjeDuTihc1s+WxQwUz+Ji24JMEyRh0nLQkWFKcCrxhWS5PlkjlKTp5rx0/m+p35QqC7Q7BSlWwXYrVkLzZkS/TnYYVS2lBGDSxCJGYYOH95Oq5E2Z7x0NLWK+ln9ny+KG9J7//dKJiLF6PrasWOn72QeBOvvZ0iWLEV7CkQMHshkAVqOmfcePmJI2N3aZz56/S2PgkXbxyQ1zifjDGlzfGJxuPX7WiR1z2LppHPYvn0eoVveK+Xr5+f35543HowODnCoKmcp0SpYQmsap0YwaiwVarJvPX2gQxIk+XxoznNSKRqedce/qzzzSMmI/LnkISBxDohlhNw9AOsGhRkqKFv1Gruy7k4PuNk3Jhxh5Nw/+oRDEyQ7DkEAdboAr2/Zcu36BT735E71U+oUsfj9G5ylVxX1zs++aX6Q8K9wmhuij6XPmzYir3KjXuoC1W127WaOki8dGWqAucfk18cdv+8vIBXpRfuhlopFVOHn2yn7qEjulJbjsM8wV+zwVyEVrT1CBkJYqZjJE7EEas+DGjOmnDddN4m99/OVurVd5sWr1E2kMtm80bmp7XTKPAf8s2LYwQQrQeO1ROQpCBXp9XsFYzmzAhVuZx+Zk3U0DsMM6VTXFUyZUsmMyDJD8ciFHp5AU6/e4lOvXOpYbVlASrVvYIsQKXr5YCrSskkTpWBzHSa7Db9YO2WN24VaeLo1VbsN4gRew8sOuVQV51y7vtwwG7+bFDx92Cw3L1qhjnAYT3olk5g54IS9SgvfV6pnQq4LWlgFXkzaHGa5C5L0i4+G/mFcQjm5JIfcjw67unMphv1LK1YY7dHXDbK4PvRYoJ3eH2FchxVrp4ZUxcrvpMD93fv1y4bUlR2GrVCUKoIFhBLJteDJ3n7Qj/HSNyhbHj8OsiwDh0q2rQ27+6SSt6c/auEilixzPYblqft6npJY9fjX31Cgey1z5YVPzl2Hvq6FP9p374VOQcqtNHnxzCc/Bz+SbAwspLYnUuINheKn/vGxXP8EDMK7RZGWMp2ndIl3Bg84ZV23jD9caLwR2EkJ0+e4ne47jVKb6Mg68//Nvi8pPrPw20roDdhVTE0DhGJmNjebKEC1+g/Z2yuOTrFSFWJ8+P0wRfLlucwa6KKhOKH79gu2la7YiMqj6s5+pH3B4jLa8ixYCfpScwaPfJHz49TDHB1mORxYO83HSBJRAH47Sy/ILtDRfUoON8JnGrOOjTc1UYREMUAzPysJAtzttB3nbytpTvwogsTJ4Z5sD4KKyhbw8+RP+072F66wdPikvcxv1w7VoFrqAdjG9ud+yF7Q6+8dPz9Iff+j7t/85/8O+O2bvzNGVxJZq0aYtVzWBHni0riNXSRRnK6uLfe5wUsZOp5jwtCE0zhTjIg7Xk9hhYCrAYKAZ80yrYGoJVRTED0TIN3yTpvkymXqCYkMm5g647zanP2P7s3dBJD5t+Ekhg4ihaFXsIWAn72QpjC2kDPf/cV+hH//g4/fPf7RABdAhYGDdym3QHw8SuwOKF/Y1k0VPvWhbev5XeF8L1F/tfp/fPf2I/NM/bGYy1l6U+sWKLFa6f/fUEIXYFVvfNtx+SukTWuYCuk1eN5Oi0gLNpep8w/CyU1vA8IdZq2cRW7DgWViRrdc6VOOtIZXKuB1Ofsfzsvd5TIb6TRIs4BGw7bzAlkBgJxcfSpkhPgNUEAfv3I38mLDAImjNtwQYWWaEhWOHi03f1fLFxvdklxe0/eW642eIa5O2EdHVjQU6YLuL6ud/costj1ca+lb0i2K7qGhMARd1esRSTV9+ct1kwhsjnAGo3rjIgiq49Uj7Y8kiy5k88t2l6WlmoWaSY0K34rCu1Wm36SdnvPVkrtO2/H2oT6ULukdOVscyNQtaSvR8WGFxGWF4/evFxYX3hPrFvvXUJy8prlFcztjsIQXKI0jRsi+vQ0TP2XXmyRKvtMyssNpKFnb/4+DZduHq7sW/1knm2O1giRfx4W1dIZ5hmUeGgNn2srHaD01lT8xE8M/HVYV6ZG/LZ3fd7f3R4LbWJEGXN04osNxeU+72nuOoLY60lRJAZwW5YX3wT7iPEC1no4myDWBWsL1hdEK9ndj0gfu96SLECmKIDzlU+CXzsy0dPC+F6b+qxRf4Sn4jiImI1lTco4CBuQ6w+vHJr2mNW9zVWB1OVeT8XEImLHrEUrFC5xYs0v0BvgvWFpq+YxYPvyhxTzRhLqU0ypHtaVwYZL7i9J/I+WceyQptY8TNWH6V4DcrY105qEi872B7WHUQ5TiN+FXKFElbYn/71sNPaKpBlbeUpJPKxeAJxtnETq975GTv3SrmDCWAlLnrh/nkHxFViDU474RjSEuoMZa8d9Tq1veCk+xTuG9V6yXWHj1Wrae3H1jrWrYHf7LBDvGB5iQ+7Vh8P7Q72SOsKvH/+KrUCrC0Il3Qj82SJVuA/VYrVCfk7rmIF1ixvBNv3kyJ2/PKdNNPwtmh94iqa+zJ8KGqa6RejKlAnMNgA4NVIg7Td2Pj69my12o/t9L8+M0RtYKVseOVemce9+ov5xQ75+QbaDb53pYEfLC8pFgMYMx8W5yCK90aCXcJm4Br+efHH9N3iI7Du8mSJ1m6IqdvjZaD+GMngqpdYLczpHL9SyaJJ4Ze4KNxBn3IUrNZlc3UvsYteX1jNVShXJ/f3ZOaTqlt0It3gRFaj/VI2DPJOYUDskBdHXvU8GbRZX9jNfliiDU2U+NV7lauRS4UsF/EYld5CNxshRMdkWc00ZPb6CQoQK3DvigX2VdVTPgFMnx5MJhm+Fq1fTpb4/YirV9ZKnbdLZpXJdK/RXjuI5FwPKxEniNNH/a03v5wsanOFtpuCVcCP8YnzoX9hAcewwI3x29QO6CDx3PM/EauJkiHTUQ8mVxMP2rf9xKrJulIDXGPGL9gOPGMpDvwSLdtbvfKO18AizOVyJ5CKQbMMz04YguD4LCxLvwWBdoLvXREsZ+zo5sRI6N+zXcK4CrGRr+UQLSSYFniDC1i07/QTK+CwrpDe4XnGVUTDP9juHUtxgqJj8gm+y+Z+LSOTQz1jWRAtFsRjW3YdnlXC5Rds16ke6qSsmaZ3XNFq7heJbllYefwYn6j49mt3ggx3GySbIi3C7vDQDg7RwlkWLmDji/XOxQlfsWqyrlSwPQH8EhdNI1y9qMjJMrxTTUK2/XV9Xr+gvoMChGvrrsMj6MkeV9Z3Evgl5zLlsN1NAzL9+6IKeLcES1hYYUpxbDKZRdNuIyUCSagQLrcs+laAaDlytQi1gShkvnTd35LbeE/jPZVUKkP8+CUueuVeeeEXV2ln9UoemKEOYmFxYRiFpp1gq+valq8ePvbAY4f2WKPKUgK7yF673HKvvAiKHUZdoe2WYImAOyyssCyePqrezukSwmXXL0YpvrZBHaKdOY+6QDTh8wNZ7ci9kijrKgH8EhfDxFKcyBU779+JWF+IA7OWqe6M0H21T0zD0bQDJtocQ8DYdUTTRIhnJ4dm2Mh4oaflEyZeOA3/djiR6gu7JVjin4EcrLA4Jz/bOV1klQIJ4YJ7+N19j9DXHt5AUUAgHikPiI8hAfQLK+Z7Phau4L1T+4eUdZUMfrGUenWy9ZOEX0F0G6tXyPCuZ6rbI4iWE2teAoSTLTCM9IKAddIC84sXmqQNtTrbEYNgyS/GF2GFtqsxrFZcQsc4r8YHIEuBBslKRK3A2vrLwYeEmxjF2oKF9TfsHoJ7P7OAli52T1NDoB2iJVHWVQL4Jy7OrGMLQ0BBdFurVxAtNNkLaP3SKgXbAutE/Ctycq4HgbHDCCu0XRWssAF34BCsSvM+2awPxddCPCBcaHXz9K5N1Cqln56n771u5YZtWLWQcvr01mVwBZ2BdpV3lQz+iYtGJFGQcRXveFMbq1c2HFfbwy5iv7NXVBw441+2eMWZ5xWUnBs1CTYgJ6vlFdquDlK9PXmZIuB6hpS1i0Wy3MQK7kNxdRRr6x+G3hRBeOH6rZxyDZtcwQolPOfuTsUvcRG0HEtx4h9X6YvDgoG1dfK1p7ZrZGwy/Zb3I2KLlxyoui+OeJfpu1IaPeQRInbYUn1hxwVL9pBvmVwmnOhINxGiJc7CIij/tztbjm3t/85/iss1S+c3XMP7757mCu7uxgivOwG/xMUosRQnAQXRcTb3I6QAnHrt6UFYXFatXwJlW6ZZzOXqZ9qJcwUl50aKFzoJiB22cpLohoUVSbAcaQ2VMI9Hjy6yOqOKzqeIbX37iYcoLLCwUDAN4Bqu++wCWtkzldGuAu3J4RdsjxJLmYF/7lQh7iA3LC4MkoDVVatmlqJIWcS6YhIwWFxipZGtLYqAb3KuaEYY/QQBgmKHpId3xbvqEiaJLFye9kF8/ZENLbmIiGUhEA+ras2yaa5gkRSJ4BdsbyeW4iSofbFh6IOUEIij4W9ArAsCdvLoUxoEzDDNvRAH0+/ADoKtrSii5RdsN7T2TxBBzRSR3hHWre1Kt4YoOILuvsh2MGhh3FgyRU7VxKQpGuzBRUT6w189/5NpyaJuINUBSaVoOCjBl2m7cgWTg4/eR733UQWCRm1jiOfyEsYk5hf64YjzCCGVLhJvHN/RWuxrBdF6/ND1sFOgA0Z4IRduSRyfuabRNYw18iLs/MJZI1gOPKulZYeFIkm3E6O3zn001XP9xq35tI7jUHayKYLr9oqgF2gUiM1u68w8y6+DjoNltUIYL3Icul9uToFXDwsUA6b/bnv1qiuLKg4BK+IzgcsmSpTCipehcSD+peEwrpzfCC+xnz8DPejTCkPQU1grtMWAR3XeJXQe5M3lNlGRRcvoCIovWB9Ka35x5Ta9+eHYtAERF67xfSNjQsgA4lphUh8OvdboVgohxJkABdIjcpQY2tPsSXqk2J1AtpYdpLSgxTd5ph0c8a9NLaw69vEK4pGgB8kRXi0nbyZEqBXarsaw7HYxUZFChYJlbEIwLt+oCqH68ONbVDVmyjrKblAnaIsWUh+CRAsWFlzDf2FrDNcd3SLyZP3D4YJipNg12TO+KN9bx8srZjNRi5ATopC2ImXnqmOIrPrAzH05wis939EQK7TdcgkrvOXDxqWakXEq/HGD9n2IU8GqCqoBBBNySvOWtYtpAQfUIVqrV/QKUfLC6ujQaEUjCq5Xc/D+vrXLrOvsZvKlVV5hbfvke0WiIjbbjSyTYgZBsZSuYK1elShlwOpiMdqUzdUbJ2o3gtxa30Gw3UGs0Pp1hOiWYIlgZkQLC1+ihhK3IlROIFr/ze4hRAtFzHarGj/RcoKAPTZkxjuxheyB9avofohZ//KB3kXz8KUaxH4WMDvbGgJWIkvE7vggflAspSuYlpXSqeB7K+A98XvbzqI1Ql5WkiZOnK6CJZJz6x3qPd8Csr4wdYL1Nm8DES0s8c+JKlROanWzYWlFES033ISMBQvCJQRs1crePr4s8O0C3++0wkpkiVjlTrPCZLB90PMBVnZ6iRIAB4hPq5O+5tUrCFguV80bhp43da1PN4082ypLeBWszzDM40mMp/cCosWW6QterpRG+kav3/VPzuXvoGnupqTQtMacBJd9GL920Osk0VULa7FjCk5YEIPCtOV2hMqJLVoP9feIfKs4RKsZxLzs1UaARFZYYs9w7Ow+vpQWGLhO7eThzFKsxEX3ZSQ5ZKJICcEHRzmr1+EaeR5A5BAsuFkm6QdY5JCCIdbrxdgJazf+fx0TLIDhpVkP8cEwDK/fQ3Ku18Id+uSfeu2ZEiWEn8hSwAptt4LuwoKAhZUNuVJoF0pDqOISK4Di5oHPL3KW3AjRilI47QfSIvCcyOk6/uLj4pLdxhKLFbLx+zE5G7WQd2IGvV/iYjt1bGEI6ihATatXhqZ7nlA0vfUBve0ih5e2dJIL6ITRXq1mCIISd/1WaLtlYTVcnkUL+0PNJazXx4W4ZfX4Ih0Qqc3sDjrECiUb23gbsKdSOwawtgQsqM3r76ZtW9cKdxBuIVlfLJyB4foNq9hVcLA9llKcAKyOAj7lIY7RVAbVy7rneV7bRl0A2fFaC6t9vsH2GEpxghCu7K7DJfIuwfIcv9ZxwZIrfI3cD3QS/bSFUV+5TDyCtWbZPPrCigW2AEI40CrmoExFEKsvEC2U5jgGVXjCsSn+lNfQA2xJwZqSAgUqvKEsYVjVH87EL9geNHMwLnBg8AGEk6jXilsBrVzEgewzj5CkNXayA+/ZSStiFRRsj6MUJwyopWS3uuC5X9MhqqXm+xMXLJlQWSCrLTIu8879PSHjWLcmL0sXsj3Bggu4/nMLnYXMFd522oFuWD38nreTNZo+j4JpzEFsLuNBHArW02ZeDdz24FqnQIESTVlRKo3BAytxsb7Da78WbsBDPKDWTdN8UgREUmsx0DrocCqEFCAvwZrx3RPJud6H0GjQzMG4wCQjjh3CUHB97xwfRH3h3ubge2wxLFgmECfeBnk7IBMor5F14COxcpDsxn2GKTawdMnWUM9vdyddkIv+ltF870v39TrFCu7ZpmZRka4aRKsCYfr7574yo2AaQoY+8oh3sVjZDfcRj1rKv79dxqOUWPkQlLhYq9U6FsAOEVd5tpGIaZVmuWPSjk4mnAaMQZtRxqb5DqXVOrrCGRQ7dOsA29LRD3fOIUp7pDChNAW5ILY4oSQAL1QgR00fMtCxuocVuRPnPqXyL60gOnq1L5o+YMKVugy65yLEsJYuyojUhQ2rFzpdQPSz2ukVR5IlRBCtUTGh5xtfnrb/1LuX7KuwAvqlSB1UcanwdDuW4iRoygs5umPW/Lto4stzoFNDJHw7LTSNlA8Y4dWReOG01wv+HGfEFbMyplQgK9gM+mjqrNfXtPkCYUI5zI2JOt24bYjkzNHxmnuJzO2pOMDyJQ/SzYAJ0Ldkd9JWXEIIFeJUGCrhAAJTDCMsEC3+fDCh54S9ymcH4e0UBWaJEqnWSUssZRrI99K0gud+a/XqYPnoM+WAoPFAJmfAq0gul4lBKxl5/LoyY7VPJ8/Vt07FC53I2GGJvD/HGTHBrLQkhvgPh9rBRMcfVUB+Ety2q+N1IUQTNWPGs01MGuIxVX6sXZsXFjw/0hMgJnf1rA98fN3R/x1uod/reQhViazAeolaAI830auI3VoE4U+/+5EQKwTjL125QatW9BZI0TL+49BxsOU65p7YyAPIM65CztWrAHFDC2N+roFatbozCUtR9L0yzaLX/ubOrIHJuQb5dQVNDit2WPDc71ihBfrU/aIn+hDcJL65lC2Z3SwMJfSQQh/zNUvn0YKsxlZTnS6OTooNgoNEzlbFygZuIljSsyEwH2vcYYG5rRTCVcRoru3r7mL3r8cpViWyelhtj7pKB1ePZMtlxK3sAPupsx/hIq+KnFvHr6uodbB1qRwmINBvj6YSoqUFJokO5HK5Ew/senmQYgKu5uavHj7mJ1agua2xf6wLBkHVP4aXEIHdSJuKuF1jWA7xQgynn7fdvQsyZVgtW9b00Jd+q1fEg1b2ZiPFlGwuXp9q/bJiWcH3sc6BFb3zrbe9QHQCnSfiUwUWKozmcuRp4cvUllA5kS2XS4hnPS1ztBxxrAFShCYocbHTsRQnQcF352iq2mRmd1DXBPydOulHMOkGwhV10g1eE1YVagc1M6AlDFt/zVadb3Juh+OFTkIE36eNXwtMa7BdRrLcxjxf7lmY0x9duGReHqtuAJbWhauTDYspLE63cBmvFl668rrPY8dFtjsssVV982jd3QvdkkhLZKUTJBX8hvV55uuPbMi/cfK8M441QCms6k8rfsH2bsRSnIRIamzUF+KxW3e9vNMkHXl7vla2LVw6n2TF85taGY0gOfA8mq3VKm86BOMhFrV6LtNXp8yAbtJGVnD+ftULIfvolU82lTIFJed2JV7oIETibqO+sKVVQjmRZo+cSgPrC39oBYLTMz/cUyG+tHRxhnoX6MJCcrqFd/X4T7aZlKkNy/j1HGJVImse4XZHOkEi7oR8XoiWcA3tOBZR+qre00rQCK+kS3FC4T8KbNrqFVqhGGTspdYosAjtYWvtGOYM1jCua9dh095wG0MlWNiP4HEU8vsFsUfMbMb9AX3Gki7FCSJwFBifDPRcVViVkZOa4GbJqcsV3L42UQ/8nS98Zr6IL8GtfKi/l36fXct1n13Y2L88ICdrbGLEvlohawXGmfNUog4gc6v2wjXE6LDSWyK2tpEUoQgKtrc9UioGAkeBNdUXItmS3Zqd1N3C9XK9Wt3e7NoFjfBqd2xabPiPAmOhEpnv7SWOymBzAUH3a+P+BcmYlowR736sWLbNN/g+PlFpXJcxtq58QWQQfhirhhcvj+EuFXgPie5vLZRTcfCAoCz7pm4DaCtTy1Q3hegEGjsoc6lVM9vdPrugYDsZZndWB5sIE3zHSaLd0pwCflwNECsO2HOQviFEOIPigHce4Lh+BkmkCL57xbImJz+2r+ap++zuXTxvYPXKnry8XaAOtxZJGsMqqq247jTNCrUIvnByPbni+pQRR9AnAZJDM+TrSuWbm/vJzgn9/HcWZY1knhLE7lt16ofeMT9MIfISUQ7ej3ayf5cf+Bw3P3YIKQ6eBeSoPWyrMI+tCgjPs2//6qZnwL2pI8J+OU7e7bmG+OIJlOCcevdbrs+FWsLN61+0b+b5ufyzTRMGfdtvjE+eQBzr/vzyF+RKokJB9rQbFox9sQsXr+ohUN6pur800a5gIe17oHTuU9ds9rBiJZ+rQFaXBPr5yPN09fpbro978HeGbLcRZTVdPzvYos1bSaaBKBTTQJ/yuqkVdNIeNTUaaKW7QgOTeFWRsKo4fLKLq6jdJrJgyRSHEaQloD6wGeRn/e69PaHEyvGcEKzC9bGzdPYD99jrwLrn7drDvTKW1FVk7ArC3cfvZykpFAHAlcxmawOGpuc5ANWnuwgY3HH2gUYzVC9Xq7lKGvvKd4N2YlgiWdLLFdx4T6OL5wthxEqClb8RpDis5FjW5aulGQ+AmEnBylMKkO1o8L7RnWJtt91URfoJUWit8KCdVUJRSHnlxsyA+7rPNur4Xm0lriOTVEXg9Z67d7k+xrFSmJpUAplSgfetXEKFIkHaEawBURTdVEeIXKs1y+bj6qsyT6tViryNIsDuJlqOmsK0lcMUSZXoKBSJEkmwZPxqoDmdAXV9MteqHFGs7GxyEcBateLhGXlZ4xMj9kCKvjTlPsn3PafSGhSKtBHVwirgx8VPp+JX6OIps9aRCd6WaySD6SXkZX3e1cpqZLynyqLRVM92hSJRogrWNlG4LC0sBNc3fE6IVYWsmr44VjREfdbqFY/MqDG8mV63UKFQJEhkC8ueDWjnWmV1q6VwXOUysmZPuIb3rfnmNNcwjYF3hUKRPC0LlpyCk7/Mq4OOxNAKWWJVoRiR6RBlBOCdruH1qbFgysJSKO4golhYQiTQk32jNTG5QgmIlQNRBe90DVG+I6fo5EmhUNwxRBGsR8dYrNbyimDvgkyFkhUrOzdrhmsorSysFAaP3FEoFHcmmDV4c7JuYrSX38SOBF4XU5nN0RvvmP91Zpf54a+OmJJBUigUimbkTEJwTcayOvnasKbO4MUvXPqBefLsN23B6no9oUKhSCFyeGrHxcrx+nlp2Znvn3/RvHX7sikLphUKhWI6cspzV1fmpJV3rVobM8duCu26RgqFQtEMi8MOSgHonWVORwXeFQpFekGw3SFYg6RQKBRpxiFaKvCuUCjSjxStM6RQKBSzgbTE1hQKRbL8P6aib+ZZ6a6pAAAAAElFTkSuQmCC`;

    doc.addImage(logoImg, 'PNG', 65, 16, 30, 10)
        .setFont('times new roman', 'open sanc', 'normal')
        .setFontSize(12)
        .text("FORMATO DE INVESTIGACIÓN DE", 101, 20)
        .text("ENFERMEDADES OCUPACIONALES", 100, 26.5)
        .text('Version Nº: 00', 176, 19)
        .text(`Pagina: ${i}`, 213, 19)
        .text(`Código: ${enfermedadStorage.NroCaso}`, 185, 28)
        .rect(98, 13, 75, 18)
        .rect(62, 13, 36, 18)
        .rect(173, 13, 32, 9)
        .rect(205, 13, 32, 9)
        .rect(173, 22, 64, 9);
}