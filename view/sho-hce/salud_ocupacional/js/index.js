/**
 * var globales
 * Edward Romero
 * Fecha: 16-12-2021
 */
//var globals
window.obj_investigacion = {};
//salud ocupacional
window.saludOcupacional = {};
window.saludOcupacional.insumo = [];
window.saludOcupacional.FlagDescansosMedicos = 0;
window.saludOcupacional.FlagLesion = 0;
window.saludOcupacional.FlagSintomatologia = 0;
window.saludOcupacional.eventoNoHabitual = 0;
window.saludOcupacional.descanso1 = 0;
window.saludOcupacional.descanso2 = 0;
window.saludOcupacional.eventoNoHabitual = 0;
window.saludOcupacional.otrasInvestigaciones = [];
window.saludOcupacional.factoresPersonales = [];
window.saludOcupacional.ExamenesAuxiliares = [];
window.saludOcupacional.FlagExamenAuxiliar = 0;
window.saludOcupacional.diagnosticos = [];
window.saludOcupacional.Conclusiones = {};
window.saludOcupacional.AntecedentesHCE = {};
window.saludOcupacional.evaluacionHigiene = {};
window.saludOcupacional.DescansosMedicos = [];
window.saludOcupacional.AccidentesTrabajo = [];
window.saludOcupacional.EvaluacionesEspecialistas = [];
window.saludOcupacional.FlagJustificacion;
window.saludOcupacional.Medicamentos = [];
window.saludOcupacional.MedidasPreventivas = [];
window.TipoEvaluacion = [];
window.sistemaAfectado = [];
//variables globales
window.global = [];
window.global.indexMedida = 0;
window.global.CIE = [];
window.global.CIE.CEI10 = [];

//variables globales a eliminar
window.arrayBorrado = [];
window.arrayBorrado.EvaluacionEspecialistas = [];
window.arrayBorrado.diagnosticos = [];
window.arrayBorrado.insumos = [];

//carga de CIE10
loadCIEdataGlobal()
/**
 *
 * @param {int} id
 * @returns void
 * carga la foto del trabajador
 */
function cargarImgEmpleado_SaludOcupacional(id) {
  IdHC = id;
  let url =
    apiUrlsho +
    `/api/hce_Get_020_datos_generales_foto_hc?code=LFR1oB7IQuhuxYcjZCqc5/x2hCuqqjpgPi/hNafJIhgfQi3lkwoL7Q==`;

  let headers = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  let settings = {
    url: url,
    method: "get",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers,
    data: {
      IdHC: idHC
    }
  };

  return $.ajax(settings).done(response => {
    //Esto lo comente (Javier Galindo)
    //loadImgEmpleado_saludOcupacional(response.Foto_HC, id);
    //cargarDataEmpleado_saludOcupacional(id);
  });
}

/**
 *
 * @param {int} id
 * carga los datos de un empleado en especifico
 */
function cargarDataEmpleado_saludOcupacional(idHC) {
  let obj = paObj_hc[idHC].a;
  sedeAreaGerencia.Sedes.forEach(e => {
    if (e.Id == obj.PlantaId_Empresa_H) {
      $("#dat_dg_planta_trabajador").text(e.Description);
    }
  });
  sedeAreaGerencia.Area.forEach(e => {
    if (e.Id == obj.AreaId_Empresa_H) {
      $("#dat_dg_area_trabajador").text(e.Description);
    }
  });
  sedeAreaGerencia.Gerencia.forEach(e => {
    if (e.Id == obj.GerenciaId_Empresa_H) {
      $("#dat_dg_gerencia_trabajador").text(e.Description);
    }
  });
  $("#dat_dg_dni_trabajador").text(obj.NroDocumento_Trabajador_H);
  $("#dat_dg_edad_trabajador").text(`${obj.Edad_Trabajador_H} años`);
  $("#dat_dg_nombres").text(obj.Nombres_Trabajador_H);
  $("#dat_dg_apellidos").text(obj.Apellidos_Trabajador_H);
  $("#dat_dg_sexo_trabajador").text(
    obj.Sexo_Trabajador_H == 1 ? "Masculino" : "Femenino"
  );
  $("#dat_dg_puesto_trabajador").text(obj.PuestoTrabajo_Empresa_H);
}

/**
 *
 * @param {object} data
 * @param {int} idHC
 * carga la imagen del empleado en su div
 */
function loadImgEmpleado_saludOcupacional(data, idHC) {
  if (data) {
    paObj_hc[idHC].a.FotoPerfil = data;
    $("#img_file_perfil").attr(
      "src",
      paObj_hc[idHC].a.FotoPerfil[0].FotoPacienteBase64
    );
  } else {
    $("#img_file_perfil").attr("src", "images/sho/profile.png");
  }
}

/**
 * para la parte de ocultar y mostrar botones
 */
$("#ocultar_datos_dg").click(() => {
  $("#datos_hc_general_trabajador")
    .children()
    .hide();
  $(".img-perfil").css("height", "75px");
  $(".img-perfil").css("width", "75px");
  $("#content_img_datos_generales").removeClass("col-md-2");
  $("#botones_datos_generales").removeClass("col-md-3");
  $("#content_img_datos_generales").addClass("col-md-1");
  $("#botones_datos_generales").addClass("col-md-4");
  $(".btn_interacciones_datos_generales").removeClass("col-md-12");
  $(".btn_interacciones_datos_generales").addClass("col-md-6");
  $("#botones_datos_generales .row").addClass("flex-row-reverse");
  $("#ocultar_datos_dg").hide();
  $("#mostrar_datos_dg").show();
});
$("#mostrar_datos_dg").click(() => {
  $("#datos_hc_general_trabajador")
    .children()
    .show();
  $(".img-perfil").css("height", "148px");
  $(".img-perfil").css("width", "148px");
  $("#content_img_datos_generales").removeClass("col-md-1");
  $("#botones_datos_generales").removeClass("col-md-4");
  $("#content_img_datos_generales").addClass("col-md-2");
  $("#botones_datos_generales").addClass("col-md-3");
  $(".btn_interacciones_datos_generales").removeClass("col-md-6");
  $(".btn_interacciones_datos_generales").addClass("col-md-12");
  $("#botones_datos_generales .row").removeClass("flex-row-reverse");
  $("#ocultar_datos_dg").show();
  $("#mostrar_datos_dg").hide();
});

function getEnfermedadesOcupacionales(id, deleted = 0) {
  var url =
    apiUrlsho +
    `/api/EO-EnfermedadesOcupacionales-All-get?code=acJSBh7lPrk/REaaCQmangSeJ/qgv063y0UgjpwvyB9zNZaWrrdHgg==&httpmethod=objectlist&Id=${id}&HistoriaClinicaId=0&GerenciaId=0&SedeId=0&AreaId=0&Documento=0&Nombres=&Apellidos=&CodeEO=&Deleted=${deleted}`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*"
    }
  })
    .then(res => res.json())
    .catch(error => console.error("Error:", error))
    .then(response => {
      if (response.EnfermedadesOcupacionales.length > 0) {
        sessionStorage.setItem(
          "enfermedad_ocupacional",
          JSON.stringify(response.EnfermedadesOcupacionales[0])
        );
      } else {
        sessionStorage.setItem(
          "enfermedad_ocupacional",
          sessionStorage.getItem("obj_investigacion")
        );
      }

      window.dispatchEvent(
        new CustomEvent("loadEnfermedadStorage", {
          detail: "load",
          bubbles: true
        })
      );
    });
}

function getTipoInvestigacionGlobal() {
  let url =
    apiUrlsho +
    `/api/hce_Get_038_tipo_evaluacion?code=pUx9Oqqpa38aZUsMFjCWKtWdHRye9xt09iP96l1I8cr74brX02Wdow==`;

  let myHeaders = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  let settings = {
    url: url,
    method: "get",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: myHeaders
  };

  fetch(url, settings)
    .then(res => res.json())
    .catch(error => console.error("Error:", error))
    .then(response => {
      //console.log("tipos de evaluacion globales");
      window.TipoEvaluacion = response.TipoEvaluacion;
      //creando evento de carga de tipo investigacions
      window.dispatchEvent(
        new CustomEvent("loadTipoInvestigacion", {
          detail: "load",
          bubbles: true
        })
      );
    });
}

getTipoInvestigacionGlobal();
// cargar investigacion creada, solo editar
function shoEditInvestigacionSP5(id) {
  getEnfermedadesOcupacionales(id);
  //actua cuando este cargado en el storage
  window.addEventListener("loadEnfermedadStorage", () => {
    let responseObj_investigacion = JSON.parse(
      sessionStorage.getItem("enfermedad_ocupacional")
    );
    //establece en el sessionStorage 'obj_investigacion' para poder usar y editar en la investigación a ver
    sessionStorage.setItem(
      "obj_investigacion",
      JSON.stringify(responseObj_investigacion)
    );
  });
  handlerUrlhtml(
    "contentGlobal",
    "view/sho-hce/salud_ocupacional/formularioInvestigaciones.html",
    "Formulario de investigación"
  );
}

function verModoEdicionInvestigacion(id) {
  var url =
    apiUrlsho +
    `/api/EO-EnfermedadesOcupacionales-All-get?code=acJSBh7lPrk/REaaCQmangSeJ/qgv063y0UgjpwvyB9zNZaWrrdHgg==&httpmethod=objectlist&Id=${id}&HistoriaClinicaId=0&GerenciaId=0&SedeId=0&AreaId=0&Documento=0&Nombres=&Apellidos=&CodeEO=&Deleted=0`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*"
    }
  })
    .then(res => res.json())
    .catch(error => console.error("Error:", error))
    .then(response => {
      console.log(response);
      if (response.EnfermedadesOcupacionales.length > 0) {
        sessionStorage.setItem(
          "enfermedad_ocupacional",
          JSON.stringify(response.EnfermedadesOcupacionales[0])
        );
      } else {
        sessionStorage.setItem(
          "enfermedad_ocupacional",
          sessionStorage.getItem("obj_investigacion")
        );
      }

      window.dispatchEvent(
        new CustomEvent("loadEnfermedadStorage", {
          detail: "load",
          bubbles: true
        })
      );
    });
  //mandar a pintar
  window.toWhereBack = "default";
  window.modeVistaInvestigacion = true;
  handlerUrlhtml(
    "contentGlobal",
    "view/sho-hce/salud_ocupacional/formularioInvestigaciones.html",
    "Formulario de investigación"
  );
}

/**
 * JESUS MILLAN
 * 2021-12-20
 */

/**
 * [sp5ViewDescansosTransferenciasInterconsultas description]
 * @return {[type]} [description]
 */
function sp5ViewDescansosTransferenciasInterconsultas() {
  /**
   * [REGISTRO DE UN NUEVO DESCANSO MEDICO]
   */
  if ($("#des_med-tab-2").hasClass("active")) {
    ID_ORIGEN = DiagnosticoId;
    ID_TIPO_ORIGEN = 3; //que es una ENFERMEDAD OCUPACIONAL
    ACCION_DESCANSOMEDICO_EO = "NUEVA";

    /**
     * Ir a la Ventana de Descansos Medicos...
     */
    fnNuevoDescanzoMedico_HC();
  }

  /**
   * [REGISTRAR O MODIFICAR UNA TRANSFERENCIA]
   */
  if ($("#transf-tab-2").hasClass("active")) {
    ID_ORIGEN = DiagnosticoId;
    ID_TIPO_ORIGEN = 3; //que es una ENFERMEDAD OCUPACIONAL

    /**
     * IR AL FORMULARIO DE CREAR NUEVA TRANSFERENCIA...
     */
    ACCION_TRANSFERENCIA_EO = "NUEVA";
    handlerUrlhtml(
      "contentGlobal",
      "view/sho-hce/interconsultas_transferencias/formularioTransferenciaEditar.html",
      "Registro de Transferencias"
    );
  }

  /**
   * [if description]
   */
  if ($("#inter_consul-tab-2").hasClass("active")) {
    ID_ORIGEN = DiagnosticoId;
    ID_TIPO_ORIGEN = 3; //que es una ENFERMEDAD OCUPACIONAL

    /**
     * [REGISTRAR, EDITAR O VER UNA INTERCONSULTA]
     */
    ACCION_INTERCONSULTA_EO = "NUEVA";
    handlerUrlhtml(
      "contentGlobal",
      "view/sho-hce/interconsultas_transferencias/formularioInterconsultasEditar.html",
      "Registro de Interconsulta"
    );
  }

  if ($("#hist_coment-tab-2").hasClass("active")) {
    let columnInformacion = "";

    ACCION_SEGUIMIENTO_EO = "NUEVA";
    NEW_SEGUIMIENTO_EO++;

    // let nowdate = (this.mode == 'created') ? moment().format("DD/MM/YYYY") : moment(this.inspectionObject.ReceptionDate).format("DD/MM/YYYY")  ;
    let nowDateF = moment().format("DD/MM/YYYY");
    let nowDate = moment().format("YYYY-MM-DD");

    columnInformacion = `
      <tr id="TR_NEW_SEGUIMIENTO_EO_${NEW_SEGUIMIENTO_EO}" class="item_ant_oc item_ant_3" data-seguimientoid="0">
        <td><span>${nowDateF}</span></td>
        <td><span><input name="NEW_SEGUIMIENTO_EO_${NEW_SEGUIMIENTO_EO}" id="NEW_SEGUIMIENTO_EO_${NEW_SEGUIMIENTO_EO}" class="submit"></span></td>
        <td>
          <!-- Botones de opciones-->
          <div class="dropdown float-right dropleft">
            <div class="more-info" id="item_am_diagnostico_1" data-toggle="dropdown">
              <img src="images/iconos/menu_responsive.svg" alt="">
            </div>
            <div class="dropdown-menu" aria-labelledby="item_am_diagnostico_1">
              <ul>
                <li onclick="editarEliminarSeguiminetoSp5EO(${NEW_SEGUIMIENTO_EO}, 'NewGuardar')">
                  <img src="./images/sho/checkMenu.svg">
                  <span>Guardar</span>
                </li>
              </ul>
              <ul class="d-none">
                <li onclick="editarEliminarSeguiminetoSp5EO(${NEW_SEGUIMIENTO_EO}, 'Editar')">
                  <img src="./images/sho/edit.svg">
                  <span>Editar</span>
                </li>
              </ul>
              <ul>
                <li onclick="editarEliminarSeguiminetoSp5EO(${NEW_SEGUIMIENTO_EO}, 'NewEliminar')">
                  <img src="./images/sho/delete.svg">
                  <span>Eliminar</span>
                </li>
              </ul>
            </div>
          </div>
        </td>
      </tr>
    `;
    // id="seguimiento_eo_${element.Id}" class="d-none">
    // document.getElementById("IEO_ListadoSeguimientos").innerHTML = columnInformacion;
    document.getElementById(
      "IEO_ListadoSeguimientos"
    ).innerHTML += columnInformacion;
    COUNT_SEGUIMIENTOS_EO++;
    document.getElementById(
      "IEO_SeguimCount"
    ).innerHTML = COUNT_SEGUIMIENTOS_EO;
  }
}

/**
 * [getTransferenciasByCode OBTENER LOS DATOS DE LAS TRASNFERENCIAS POR SU CODIGO]
 * @return {[type]} [description]
 */
function getTransferenciasByCode() {
  /**
   * [IdTransferencia Id de la transferencia buscada.]
   * @type {Number}
   */
  let IdTransferencia = 0;

  var headers = {
    apiKey: constantes.apiKey,
    "Content-Type": "application/json"
  };
  var url =
    apiUrlsho +
    "/api/hce_Get_010_transferencia_busqueda?code=Ma57zVWN/hVKiVbpbiaBFuwa3iEiVrtFLv6X0qATPa1taStnyUvlPw==&Gerencia=&Planta=&Area=&Documento=&Nombres=&Apellidos=&CodeInterconsultaTraferencia=" +
    CODE_TRANSFERENCIA_EO +
    "&FechaDesde=&FechaHasta=&Buscar=";

  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers
    // "data": { "NroDocumento_Trabajador_H": a, "AreaId_Empresa_H": c, "SedeId_Empresa_H": b, "Buscador": d } //JSON.stringify(body)
  };
  paObj_ht = [];
  $.ajax(settings).done(function(response) {
    response.transferencias.forEach(Item => {
      paObj_ht[Item.Id] = new Transferencia();
      paObj_ht[Item.Id].cargarData(Item);

      IdTransferencia = Item.Id;

      // let d = response.SignosVitales.find(x => x.TransferenciaId == Item.Id )
      paObj_ht[Item.Id].a.DiagnosticoCIE10 = response.DiagnosticoCIE10;
      response.DiagnosticoCIE10.map(item => {
        lt1[item.CIE10] = item;
      });

      let sv = response.SignosVitales.find(x => x.TransferenciaId == Item.Id);
      paObj_ht[Item.Id].a.SignosVitales = sv;
    });

    /**
     * IdHistoriaClinica, IdTransferencia
     */
    fnSp5VerRegistroTransferenciaEO(IdHC, IdTransferencia);
  });
}

function fnSp5VerRegistroTransferenciaEO(idHistoriaC, idTransferencia) {
  istAud = idTransferencia;
  istAud2 = idHistoriaC;
  OBJ_I = sedeAreaGerencia;

  if (ACCION_TRANSFERENCIA_EO === "VER") {
    handlerUrlhtml(
      "contentGlobal",
      "view/sho-hce/interconsultas_transferencias/formularioTransferenciasVer.html",
      "Ver Datos de la Transferencia "
    );
  }

  if (ACCION_TRANSFERENCIA_EO === "EDITAR") {
    ID_TRANSFERENCIA_EO = idTransferencia;
    istAudT = idTransferencia;
    handlerUrlhtml(
      "contentGlobal",
      "view/sho-hce/interconsultas_transferencias/formularioTransferenciaEditar.html",
      "Modificar Datos de la Transferencia "
    );
  }
}

/**
 * [getInterconsultasByCode OBTENER LOS DATOS DE UNA INTERCONSULTA POR SU CODIGO]
 * @param  {[string]} accion [ver o editar]
 * @return {[type]}        [description]
 */
function getInterconsultasByCode() {
  var url =
    apiUrlsho +
    "/api/hce_Get_009_interconsulta_busqueda?code=mlLAeNuPAvTC057apNoiFLG/H/j36XcKY8vYElXN1st3CaLy2eeQ1A==&Gerencia=&Planta=&Area=&Documento=&Nombres=&Apellidos=&CodeInterconsultaTraferencia=Int000314&FechaDesde=&FechaHasta=&Buscar=";

  var headers = {
    apiKey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers
    // "data": { "NroDocumento_Trabajador_H": a, "AreaId_Empresa_H": c, "SedeId_Empresa_H": b, "Buscador": d } //JSON.stringify(body)
  };

  $.ajax(settings).done(function(response) {
    paObj_hi[response.Interconsult[0].IdInter] = new Interconsulta();
    paObj_hi[response.Interconsult[0].IdInter].cargarData(
      response.Interconsult[0]
    );

    // paObj_hi[response.Interconsult[0].IdInter] = response[0]

    idInter = response.Interconsult[0].IdInter;

    if (ACCION_INTERCONSULTA_EO === "VER") {
      ID_INTERCONSULTA_EO = idInter;
      // archivo -> gestionAtencionesMedicasBandeja.js
      interSp3VerInterconsulta(314, 262);
    } else if (ACCION_INTERCONSULTA_EO === "EDITAR") {
      ID_INTERCONSULTA_EO = idInter;
      // archivo -> gestionAtencionesMedicasBandeja.js
      interSp3EditarInterconsulta(314, 262);
    }
  });
}

/**
 * [EOSp5GuardarDescansoMedico GUARDAR UN DESCANSO MEDICO CON ORIGEN DESDE ENFERMEDADES OCUPACIONALES]
 */
async function EOSp5GuardarDescansoMedico() {
  let data = {};

  data.IdHC = idHC;
  data.IdDescM = id_DM;
  data.OrigenId = DiagnosticoId;
  data.TipoOrigen = 3;
  data.A_IdAtencionMedica = 0;

  (data.A_DniTrabajador = $("#dat_des_dni_trabajador_v").val()),
    (data.A_NombreTrabjador = $("#dat_des_nombres_trabajador_v").val()),
    (data.A_ApellidosTrabajador = $("#dat_des_apellidos_trabajador_v").val()),
    (data.A_Empresa = $("#dat_des_empresa_v").val()),
    (data.A_Origen = $("#dat_des_origen_v").val()),
    (data.A_Gerencia = $("#dat_des_gerencia_v").val()),
    (data.A_Planta = $("#dat_des_planta_v").val()),
    (data.A_Area = $("#dat_des_area_v").val()),
    (data.A_PuestoTrabajo = $("#dat_des_id_atencion_med_v").val()),
    (data.B_PersonalSolicitud = $("#dat_des_personal_salud_v").val());
  data.B_PersonalIdHash = "";
  data.B_TipoContingencia = $("#dat_des_tipo_contingencia_v").val();
  data.B_DescansoPorEnfermedad = $("#dat_des_descanso_enfermedad_v").val();

  (data.B_CMP = $("#dat_des_cmp_v").val()),
    (data.B_CantidadDias = $("#dat_des_cant_dias_v").val());
  data.B_DiasAcumulados = $("#dat_des_dias_ac_v").val();
  data.B_FechaIni = $("#dat_des_fi_v2").val();
  data.B_FechaFin = $("#dat_des_ff_v").val();
  data.B_HuboAltaMedica = $("#dat_des_alta_med_v").val();
  data.B_EstableceDescanso = $("#dat_des_establecimiento_emisión_v").val();
  data.B_Particular = $("#dat_des_particular_v").val();

  data.CreadoPor = "EO";
  data.ModificadoPor = "EO";

  let url = `${apiUrlsho}/api/hce_Post_023_descanso_medico?code=ZMMXyYQzh2QaROc92eMe55BMlkkckJVD9r9mxqTmHFpCUXm75EvSsQ==&httpmethod=post`;

  let headers = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  let settings = {
    url: url,
    method: "post",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers,
    data: JSON.stringify(data)
  };

  return $.ajax(settings)
    .done(response => {
      // para usar al agergar un diagnostico en descansos medicos...
      id_DM = response.Id;
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      Swal.fire({
        icon: "error",
        title: "Descanso Medico",
        text: "Error al crear el registro dell Descanso Medico",
        footer: '<a href="">Why do I have this issue?</a>'
      });
    })
    .always(function(jqXHR, textStatus, errorThrown) {
      hideLoading();
    }); //*/
}

async function EOSp5CargarDataTransferencias() {
  await EOGuardarNewTransferenciaSp5();

  OBJ_I = sedeAreaGerencia;

  /**
   * [CARGAR SELECT DE GERENCIAS]
   */
  $("#dat_int_tran_gerencia").html('<option value=""></option>');
  sedeAreaGerencia.Gerencia.map((item, index) => {
    $("#dat_int_tran_gerencia").append(
      `<option value="${item.Id}">${item.Description}</option>`
    );
  });

  /**
   * [CARGAR SELECT DE PLANTAS]
   */
  $("#dat_int_tran_planta").html('<option value=""></option>');
  sedeAreaGerencia.Sedes.map((item, index) => {
    $("#dat_int_tran_planta").append(
      `<option value="${item.Id}">${item.Description}</option>`
    );
  });

  /**
   * [CARGAR SELECT DE AREAS]
   */
  $("#dat_int_tran_area").html('<option value=""></option>');
  sedeAreaGerencia.Area.map((item, index) => {
    $("#dat_int_tran_area").append(
      `<option value="${item.Id}">${item.Description}</option>`
    );
  });

  /**
   * DATOS DE LA HISTORIA CLINICA
   */
  $("#dat_int_tran_codigo").val(paObj_hc[idHC].a.NroDocumento_Trabajador_H);
  $("#dat_int_tran_nombre").val(paObj_hc[idHC].a.Nombres_Trabajador_H);
  $("#dat_int_tran_apellido").val(paObj_hc[idHC].a.Apellidos_Trabajador_H);
  $("#dat_int_tran_edad").val(paObj_hc[idHC].a.Edad_Trabajador_H);
  $("#dat_int_tran_gerencia").val(paObj_hc[idHC].a.GerenciaId_Empresa_H);
  $("#dat_int_tran_planta").val(paObj_hc[idHC].a.SedeId_Empresa_H);
  $("#dat_int_tran_area").val(paObj_hc[idHC].a.AreaId_Empresa_H);
  $("#dat_int_tran_puesto").val(paObj_hc[idHC].a.PuestoTrabajo_Empresa_H);

  await EOcargarDatosInter();

  /**
   * [CARGANDO DATOS DE LOS SELECTS DE SECCION Y AREA AFECTADAS EN LOS DOS DIAGNOSTICOS]
   */
  $("#dat_int_tran_seccion_af").html(`<option value=""></option>`);
  $("#dat_int_tran_seccion_af_2").html(`<option value=""></option>`);
  datosInter.SeccionAfectada.forEach(e => {
    $("#dat_int_tran_seccion_af").append(`
        <option value="${e.Id}">${e.Descripcion}</option>
    `);
    $("#dat_int_tran_seccion_af_2").append(`
        <option value="${e.Id}">${e.Descripcion}</option>
    `);
  });

  $("#dat_int_tran_sis_af").html(`<option value=""></option>`);
  $("#dat_int_tran_sist_af_2").html(`<option value=""></option>`);
  datosInter.SistemasAfectados.forEach(e => {
    $("#dat_int_tran_sis_af").append(`
        <option value="${e.Id}">${e.Descripcion}</option>
      `);
    $("#dat_int_tran_sist_af_2").append(`
        <option value="${e.Id}">${e.Descripcion}</option>
      `);
  });
}

function EOGuardarNewTransferenciaSp5() {
  // primeramente vamos a registrar la nueva transferencia, ojo si dice cancelar se debe mandar a boerrar asi como a sus hijos

  var hoy = new Date();
  var mes = hoy.getMonth() + 1;
  var dia = hoy.getDate();
  if (mes < 10) {
    mes = "0" + mes;
  }
  if (dia < 10) {
    dia = "0" + dia;
  }

  var f1 = hoy.getFullYear() + "-" + mes + "-" + dia;

  //alert(f1);
  $("#dat_int_tran_fe_transferencia").val(f1);

  //aqui debemos mandar a crear el nuevo registro para tener el Id para las listas
  //alert("gestonTransferencia 535  ID_ORIGEN = "+ID_ORIGEN);
  var body = {
    IdHC: idHC, //Id Historia clinica
    IdHashUser: getCookie("vtas_id_hash" + sessionStorage.tabVisitasa),
    IdTransf: 0,
    A_Empresa: ".",
    A_DireccionTrabajador: ".",
    A_Telefono: "0",
    A_Anamnesis: ".",
    B_Tratamiento: ".",
    B_MotivoTrasferencia: ".",
    B_HoraAtencionDSO: "16:21",
    B_HoraEvaluacion: "16:21",

    C_Tratamiento: ".",
    C_RecibidoPor: "CMP",
    C_IdHashRecibido: ".",
    C_Lugar: ".",
    C_Fecha: "2021-01-01",
    CodigoOrigen: "",
    IdOrigen: ID_ORIGEN,
    IdTipoOrigen: ID_TIPO_ORIGEN,

    IdSV: 0, //Id de signos vitales
    PresionArterial_SV: 0,
    FrecuenciaCardiaca_SV: 0,
    FrecuenciaRespiratoria_SV: 0,
    Temperatura_SV: 0,
    PesoKg_SV: 0,
    Talla_SV: 0,
    Saturacion_SV: 0,
    IndiceMasaCorporal_SV: 0,
    PerimetroAbdominal_SV: 0
  };

  //var url = apiUrlssoma+ "/api/Post-LeccionAprendida?code=3SZLZvJm1g4qEdObbrPAWiDaiGa6EFKE9RFyIN1Q5osMz6ozxWX/xQ==&httpmethod=post";
  var url =
    apiUrlsho +
    "/api/hce_Post_017_transferencias?code=x69HnqaaZgWZQTcc61KfkEokA8TYLE6bRUfIxQIwrhOkrrFiYcDVbQ==&httpmethod=post";

  var headers = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  return $.ajax({
    method: "POST",
    url: url,
    headers: headers,
    data: JSON.stringify(body),
    crossDomain: true,
    dataType: "json"
  })
    .done(function(data) {
      if (data.Id > 0) {
        isNow = 0;
        //alert('el reguistro de la transferencia tiene el Id = '+data.Id );
        hideLoading();
        paObj_ht[data.Id] = new Transferencia();
        paObj_ht[data.Id].cargarData(data);

        //istAud2 //histria clinica
        ID_TRANSFERENCIA_EO = data.Id;
        istAudT = data.Id;
        istAud = data.Id;
        istAud2 = idHC;
        // fnSp3CargaListaFormTransferencia4(data.Id)
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      verModalError(
        "Al Crear el registro de Transferencia",
        "Intente Nuevamente"
      );
    })
    .always(function(jqXHR, textStatus, errorThrown) {
      console.warn("always -> ", jqXHR);
    });
}

async function fnEOCargaDataInterconsulta() {
  $("#dat_int_area_trabajador_editar").html('<option value=""></option>');
  sedeAreaGerencia.Gerencia.map((item, index) => {
    $("#dat_int_area_trabajador_editar").append(
      `<option value="${item.Id}">${item.Description}</option>`
    );
  });

  $("#dat_int_planta_trabajador_editar").html('<option value=""></option>');
  sedeAreaGerencia.Sedes.map((item, index) => {
    $("#dat_int_planta_trabajador_editar").append(
      `<option value="${item.Id}">${item.Description}</option>`
    );
  });

  $("#dat_int_planta_trabajador_editar").html('<option value=""></option>');
  sedeAreaGerencia.Sedes.map((item, index) => {
    $("#dat_int_planta_trabajador_editar").append(
      `<option value="${item.Id}">${item.Description}</option>`
    );
  });

  $("#dat_int_gerencia_trabajador_editar").html('<option value=""></option>');
  sedeAreaGerencia.Area.map((item, index) => {
    $("#dat_int_gerencia_trabajador_editar").append(
      `<option value="${item.Id}">${item.Description}</option>`
    );
  });

  await EOcargarDatosInter();

  // $('#dat_int_especialidad_interconsulta').append(`<option value=""></option>`)
  datosInter.Especialidades.forEach(e => {
    $("#dat_int_especialidad_interconsulta").append(`
            <option value="${e.Id}">${e.Descripcion}</option>
        `);
  });
  $("#dat_int_seccion_diagnostico").html(`<option value=""></option>`);
  datosInter.SeccionAfectada.forEach(e => {
    $("#dat_int_seccion_diagnostico").append(`
            <option value="${e.Id}">${e.Descripcion}</option>
        `);
  });
  $("#dat_int_sistema_diagnostico").html(`<option value=""></option>`);
  datosInter.SistemasAfectados.forEach(e => {
    $("#dat_int_sistema_diagnostico").append(`
            <option value="${e.Id}">${e.Descripcion}</option>
        `);
  });

  /**
   * DATOS DE LA HISTORIA CLINICA
   */

  $("#dat_int_dni_trabajador_editar").val(
    paObj_hc[idHC].a.NroDocumento_Trabajador_H
  );
  $("#dat_int_nombres_trabajador_editar").val(
    paObj_hc[idHC].a.Nombres_Trabajador_H
  );
  $("#dat_int_apellidos_trabajador_editar").val(
    paObj_hc[idHC].a.Apellidos_Trabajador_H
  );
  $("#dat_int_edad_trabajador_editar").val(paObj_hc[idHC].a.Edad_Trabajador_H);
  $("#dat_int_area_trabajador_editar").val(
    paObj_hc[idHC].a.GerenciaId_Empresa_H
  );
  $("#dat_int_planta_trabajador_editar").val(paObj_hc[idHC].a.SedeId_Empresa_H);
  $("#dat_int_gerencia_trabajador_editar").val(
    paObj_hc[idHC].a.AreaId_Empresa_H
  );
  $("#dat_int_puesto_trabajo_trabajador_editar").val(
    paObj_hc[idHC].a.PuestoTrabajo_Empresa_H
  );

  if (ACCION_INTERCONSULTA_EO === "NUEVA") {
    await EOGuardarNewInterconsultaSp5(0);
  }
}

function EOcargarDatosInter() {
  let url =
    apiUrlsho +
    `/api/hce_Get_005_interconsulta?code=5apeYFqxMjc7U4gRbZFs/mBepNwlDkPcUymNTlP7kzOsv5AN3DYecg==`;

  let headers = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  let settings = {
    url: url,
    method: "get",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers
  };

  return $.ajax(settings).done(response => {
    datosInter["Especialidades"] = response.IntercExpecialidad;
    datosInter["SeccionAfectada"] = response.SeccionAfectada;
    datosInter["SistemasAfectados"] = response.SistemasAfectados;
    datosInter["TipoOrigen"] = response.TipoOrigen;
    let cie10 = response.TipoCIE10.slice(0, 100);
    datosInter["TipoCIE10"] = cie10;
  });
}

function EOGuardarNewInterconsultaSp5(id) {
  //alert('3677 gestionInterconsultasTransferencia.js');
  let data = {};
  let interNew = 0;

  if (id) {
    data.IdInter = id;
  } else {
    data.IdInter = 0;
    interNew = 1;
  }

  ID_INTERCONSULTA_EO = data.IdInter;

  data.IdHC = idHC;
  data.A_Empresa = $("#dat_int_empresa_interconsulta").val()
    ? $("#dat_int_empresa_interconsulta").val()
    : "vacio";
  data.A_InterEspecialidad = $("#dat_int_especialidad_interconsulta").val()
    ? $("#dat_int_especialidad_interconsulta").val()
    : 0;
  data.A_InterMotivo = $("#dat_int_motivo_interconsulta").val()
    ? $("#dat_int_motivo_interconsulta").val()
    : "vacio";
  data.A_SeSolicita = $("#dat_int_solicita_interconsulta").val()
    ? $("#dat_int_solicita_interconsulta").val()
    : "vacio";
  data.A_Otros = $("#dat_int_otros_interconsulta").val()
    ? $("#dat_int_otros_interconsulta").val()
    : "vacio";
  data.A_MedicoCargo = $("#dat_int_medico_cargo_interconsulta").val()
    ? $("#dat_int_medico_cargo_interconsulta").val()
    : "vacio";
  data.A_CMP = $("#dat_int_cmp_interconsulta").val()
    ? $("#dat_int_cmp_interconsulta").val()
    : "vacio";
  data.A_IdHashMedico = "A_IdHashMedico";

  data.B_RecibioRespuesta = $(
    "input:radio[name=recibio_respuesta_int]:checked"
  ).val()
    ? $("input:radio[name=recibio_respuesta_int]:checked").val()
    : 0;
  data.B_InstitucionSalud = $("#dat_int_institucion_respuesta").val()
    ? $("#dat_int_institucion_respuesta").val()
    : "vacio";
  data.B_FechaRespuesta = $("#dat_int_fecha_respuesta").val()
    ? $("#dat_int_fecha_respuesta").val()
    : "2000-01-01";
  data.B_HallazgoEval = $("#dat_int_hallazgos_respuesta").val()
    ? $("#dat_int_hallazgos_respuesta").val()
    : "vacio";
  data.B_ExamenesAuxiliarers = $("#dat_int_examenes_respuesta").val()
    ? $("#dat_int_examenes_respuesta").val()
    : "vacio";

  data.C_Recomendaciones = $("#dat_int_recomendaciones_recomendaciones").val()
    ? $("#dat_int_recomendaciones_recomendaciones").val()
    : "vacio";
  data.C_RequiereControles = $(
    "input:radio[name=posteriores_int]:checked"
  ).val()
    ? $("input:radio[name=posteriores_int]:checked").val()
    : 0;
  data.C_ActitupParaLaboral = $("input:radio[name=aptitud_int]:checked").val()
    ? $("input:radio[name=aptitud_int]:checked").val()
    : 0;
  data.C_FechaProximoControl = $("#dat_int_fecha_proximo_recomendaciones").val()
    ? $("#dat_int_fecha_proximo_recomendaciones").val()
    : "2000-01-01";
  data.C_MedicoEvaluador = $("#dat_int_medico_evaluador_recomendaciones").val()
    ? $("#dat_int_medico_evaluador_recomendaciones").val()
    : "vacio";
  data.C_IdHashMedico = "IdHashMedico";
  data.C_CMP = $("#dat_int_cmp_recomendaciones").val()
    ? $("#dat_int_cmp_recomendaciones").val()
    : "vacio";
  data.C_RNE = $("#dat_int_rne_recomendaciones").val()
    ? $("#dat_int_rne_recomendaciones").val()
    : "vacio";
  data.C_Otros = $("#dat_int_otros_recomendaciones").val()
    ? $("#dat_int_otros_recomendaciones").val()
    : "vacio";

  data.Tratamiento = $("#dat_int_tratamiento_diagnostico").val()
    ? $("#dat_int_tratamiento_diagnostico").val()
    : "vacio";
  data.CodigoOrigen = "0000-IC-" + idHC;
  data.IdOrigen = ID_ORIGEN;
  data.IdTipoOrigen = ID_TIPO_ORIGEN;

  let url =
    apiUrlsho +
    `/api/hce_Post_008_interconsulta?code=p6WGKSBcccNRnMdgJ5xr5uWwAn/FAYyhahe4DRpyNTTZevUnTfPL0w==&httpmethod=post`;

  let headers = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json"
  };

  let settings = {
    url: url,
    method: "post",
    dataType: "json",
    headers: headers,
    data: JSON.stringify(data)
  };

  $.ajax(settings).done(response => {
    if (data.IdInter != 0) {
      Swal.fire({
        title: "Se terminó con éxito el registro",
        iconColor: "#8fbb02",
        iconHtml: '<img src="./images/sho/check.svg" width="28px">',
        showConfirmButton: false,
        padding: "3em 3em 6em 3em ",
        timer: 1500
      }).then(() => {
        /*if (REG_DESDE == 0) {
                  handlerUrlhtml('contentGlobal', 'view/sho-hce/interconsultas_transferencias/gestionInterconsultasTransferencia.html', 'Registro de interconsultas y transferencias');
                }
                if (REG_DESDE == 1) {
                  $('#modalAlertaIncidentePDF').modal('hide').removeClass('fade');
                  fnSp3CargaFiltroEstadoInicialSaludOcupacional_inc_HC2();
                }*/
      });
    }
    ID_INTERCONSULTA_EO = response.Id;
    idInter = response.Id;
    // if (interNew == 1) {
    //     paObj_hi[ID_INTERCONSULTA_EO] = new Interconsulta();
    // }
  }); //*/
}

/**
 * [interSp3ValidarDatos VALIDAR DATOS DE INTERCONSULTAS]
 * @param  {[type]} idClass [description]
 * @return {[type]}         [description]
 */
function interSp3ValidarDatos(idClass) {
  let error = [];
  let inputs = $(`.${idClass}`);

  inputs.each(e => {
    if (!inputs.eq(e).val()) {
      inputs.eq(e).addClass("is-invalid");
      error.push(inputs.eq(e).val());
    }
    if (inputs.eq(e).val()) {
      inputs.eq(e).removeClass("is-invalid");
    }
  });

  if (error.length > 0) {
    inputs.focus();
  }
  // console.log(error);
  return error;
}

/**
 * [fnSp3Carga_Listas_Desplegables_EO description]
 * @return {[type]} [description]
 */
function fnSp3Carga_Listas_Desplegables_EO() {
  showLoading();

  $("#" + "dat_hc_it_a_gerencia").html("Cargando ...");
  $("#" + "dat_des_planta_v").html("Cargando ...");
  $("#" + "dat_des_area_v").html("Cargando ...");

  var url =
    apiUrlssoma +
    "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0";

  var headers = {
    apikey: constantes.apiKey
  };

  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers
  };

  $.ajax(settings).done(function(response1) {
    OBJ_I = response1;

    //ahora vamos a recibire el servicio

    var l = response1.Gerencia.length;
    var lk = 0;

    var n = response1.Sedes.length;
    var nk = 0;

    var m = response1.Area.length;
    var mk = 0;

    //------------------------------------------------------------------------------------------------------------------------------------------------------------
    $("#" + "dat_des_gerencia_v").html(" ");
    $("#" + "dat_des_gerencia_v").css("font-size", "13px");
    $("#" + "dat_des_gerencia_v").html(
      "<option selected value='0'>          </option>"
    );

    response1.Gerencia.map(function(item) {
      $("#" + "dat_des_gerencia_v").append(
        `<option value='${item.Id}' title='${
          item.Description
        }' style='font-weight: bold;'>${item.Description}</option>`
      );

      lk++;
      if (l == lk) {
        //----------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("#" + "dat_des_planta_v").html(" ");
        $("#" + "dat_des_planta_v").css("font-size", "13px");
        $("#" + "dat_des_planta_v").html(
          "<option selected value='0'>          </option>"
        );

        response1.Sedes.map(function(item) {
          $("#" + "dat_des_planta_v").append(
            `<option value='${item.Id}' title='${
              item.Description
            }' style='font-weight: bold;'>${item.Description}</option>`
          );

          nk++;
          if (n == nk) {
            //-----------------------------------------------------------------------

            $("#" + "dat_des_area_v").html(" ");
            $("#" + "dat_des_area_v").css("font-size", "13px");
            $("#" + "dat_des_area_v").html(
              "<option selected value='0'>          </option>"
            );

            response1.Area.map(function(item) {
              $("#" + "dat_des_area_v").append(
                `<option value='${item.Id}' title='${
                  item.Description
                }' style='font-weight: bold;'>${item.Description}</option>`
              );

              mk++;
              if (m == mk) {
                //--------------------------------------------------------------------------------------------------------------------------------------------
                // fnSp3CargaFiltroEstadoInicialSaludOcupacional_inc();
                // fnSp3CargaFiltroEstadoInicialSaludOcupacional_trans();
                // fnSp3CargarHistoriasClinicas();

                // alert(' 4308   newd = '+newD);
                if (newD == 0) {
                  fnSp3Carga_Combos_Listas_cie10_EO(
                    idHC,
                    "dat_des_cie10_v",
                    "dat_des_sis_afectado_v",
                    "dat_des_sec_afectada_v"
                  );
                } else {
                  if (newD == 1) {
                    ///alert('a punto de llamar fnSp3Carga_Combos_Listas_cie10 4308');
                    fnSp3Carga_Combos_Listas_cie10_EO(
                      idHC,
                      "dat_des_cie10_v",
                      "dat_des_sis_afectado_v",
                      "dat_des_sec_afectada_v"
                    );
                  }
                }
              }
            });
          }
        });
      }
    });
  });
}

/**
 * [fnSp3Carga_Combos_Listas_cie10_EO description]
 * @param  {[type]} idHistoriaClinica [description]
 * @param  {[type]} id1               [description]
 * @param  {[type]} id2               [description]
 * @param  {[type]} id3               [description]
 * @return {[type]}                   [description]
 */
function fnSp3Carga_Combos_Listas_cie10_EO(idHistoriaClinica, id1, id2, id3) {
  // alert('dentro de fnSp3Carga_Combos_Listas_cie10');

  showLoading();
  $("#" + id1).html("Cargando...");
  $("#" + id2).html("Cargando..."); //sistema afectado
  $("#" + id3).html("Cargando..."); //sistema afectado

  // $("#" + 'dat_des_cie10_v_2').html("Cargando...");
  // $("#" + 'dat_int_tran_sist_af_2').html("Cargando...");
  // $("#" + 'dat_int_tran_seccion_af_2').html("Cargando...");

  var url =
    apiUrlsho +
    "/api/hce_Get_006_transferencia?code=bdFhdqqPWPq62GBhPORONSYngrPBCDmZ6QnQdfIid4NdqCddRk7r3Q==&IdHC=" +
    idHistoriaClinica;

  var headers = {
    apikey: constantes.apiKey
  };

  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers
  };

  $.ajax(settings).done(function(response1) {
    // TipoCIE10
    // SistemasAfectados
    // SeccionAfectada

    if (response1.TipoCIE10.length > 0) {
      $("#" + id1).html(" ");
      $("#" + id1).css("font-size", "13px");
      $("#" + id1).html("<option selected value='0'>          </option>");
      // $("#" + 'dat_des_cie10_v_2').html(" "); $("#" + 'dat_des_cie10_v_2').css('font-size', '13px'); $("#" + 'dat_des_cie10_v_2').html("<option selected value='0'>          </option>");
      for (let i = 0; i < response1.TipoCIE10.length; i++) {
        let item = response1.TipoCIE10[i];
        lt1[item.Id] = response1.TipoCIE10[i];
      }

      if (response1.SistemasAfectados.length > 0) {
        n = response1.SistemasAfectados.length;
        $("#" + id2).html(" ");
        $("#" + id2).css(
          "font-size",
          "13px"
        ); /*$("#" + id2).html("<option selected value='0'>          </option>");*/
        //$("#" + 'dat_int_tran_sist_af_2').html(" "); $("#" + 'dat_int_tran_sist_af_2').css('font-size', '13px'); $("#" + 'dat_int_tran_sist_af_2').html("<option selected value='0'>          </option>");
        for (let i = 0; i < n; i++) {
          let item = response1.SistemasAfectados[i];
          lt2[item.Id] = response1.SistemasAfectados[i];

          $("#" + id2).append(
            `<option value='${item.Id}' title='${
              item.Especilidades
            }' style='font-weight: bold;'>${item.Descripcion}</option>`
          );
          //$("#" + 'dat_int_tran_sist_af_2').append(`<option value='${item.Id}' title='${item.Especilidades}' style='font-weight: bold;'>${item.Descripcion}</option>`);
        }

        if (response1.SeccionAfectada.length > 0) {
          n = response1.SeccionAfectada.length;
          $("#" + id3).html(" ");
          $("#" + id3).css(
            "font-size",
            "13px"
          ); /*$("#" + id3).html("<option selected value='0'>          </option>");*/
          //$("#" + 'dat_int_tran_seccion_af_2').html(" "); $("#" + 'dat_int_tran_seccion_af_2').css('font-size', '13px'); $("#" + 'dat_int_tran_seccion_af_2').html("<option selected value='0'>          </option>");
          for (let i = 0; i < n; i++) {
            let item = response1.SeccionAfectada[i];
            lt3[item.Id] = response1.SeccionAfectada[i];

            $("#" + id3).append(
              `<option value='${item.Id}' title='${
                item.Especilidades
              }' style='font-weight: bold;'>${item.Descripcion}</option>`
            );
            //$("#" + 'dat_int_tran_seccion_af_2').append(`<option value='${item.Id}' title='${item.Especilidades}' style='font-weight: bold;'>${item.Descripcion}</option>`);
          }

          //alert(' 4308   newd = '+newD);
          if (newD == 0) {
            fnCargarDataPersonal_DM(idHC, ID_DESCANSO_MEDICO_EO);
          } else {
            if (newD == 1) {
              //si se esta creando un nuevo registro que debe hacer busca la data vacía ya que siempre es editar
              fnSp3CrearNuevo_DM_en_Blanco();
            }
          }
        }
      }
    }
  });
}

function sp3CargaLosCie10_1_Ver_EO(DescansoMedicoId) {
  //DescansoMedicoId = 1; //esta forzado

  var ikk = 0;

  var headers = {
    apiKey: constantes.apiKey,
    "Content-Type": "application/json"
  };
  showLoading();

  var url =
    apiUrlsho +
    "/api/hce_Get_014_descanso_medico_diagnosticoCIE10?code=/J44W6Q4iAAgb6cxLiqZKMLhKa6QAOFNoNsOlVU0KHWXrK3n053ELQ==&IdDescansoMedico=" +
    DescansoMedicoId;
  var tabla;
  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: headers
    // "data": { "NroDocumento_Trabajador_H": a, "AreaId_Empresa_H": c, "SedeId_Empresa_H": b, "Buscador": d } //JSON.stringify(body)
  };

  $.ajax(settings).done(function(response) {
    $("#lb_diag_1_tabla").html(" ");
    $("#lb_diag_1_int").html("" + ikk + " registros");
    response.DiagnosticoCIE.map(function(Item) {
      if (Item.DescansoMedicoId > 0) {
        var f1a = date_AAAA_MM_DD_T_HH_MM_S_to_DD_MM_AAAA(Item.CreadoFecha);

        let b = Item.Code;
        let c = Item.SistemaAfectado_D;
        let d = Item.SeccionAfectada_D;
        let e = Item.Especilidades;

        diag_DN = diag_DN + " " + Item.Diagnostico;

        tabla += `
            <tr>
              <td>${Item.Diagnostico != null ? Item.Diagnostico : "---"}</td>
              <td>${b != null ? b : "---"}</td>
              <td>${f1a != null ? f1a : "---"}</td>
              <td>${e != null ? e : "---"}</td>
              <td>${c != null ? c : "---"}</td>
              <td>${d != null ? d : "---"}</td>
            </tr>

          `;

        $("#lb_diag_1_tabla").append(tabla);
        ikk = ikk + 1;
        $("#lb_diag_1_int_t").html("" + ikk + " registros");
        tabla = "";

        $("#dat_des_diagnostico_v").val("");
        $("#dat_des_cie10_v").val(0);
        $("#dat_des_sis_afectado_v").val(15);
        $("#dat_des_sec_afectada_v").val(11);
      } //---------------------------------------------------------------------------------
    });

    hideLoading();
  });
}
