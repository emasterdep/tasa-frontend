//funciones
function createObjInvestigacionNueva() {
  var raw = {
    Id: 0,
    HistoriaClinicaId: idHC,
    EstadoEnfermedadOcupacionalCode: 1,
    TurnosCode: 1,
    DiaTurnoEncuentra: 0,
    ActividadRutinariaPuesto: "",
    MotivoConsulta: "",
    FlagLesion: 0,
    DetalleLesion: "",
    FlagDescansosMedicos: 0,
    FlagSintomatologia: 0,
    DetalleSintomatologia: "",
    Deleted: 0,
    CreatedBy: "",
    Insumos: [],
    DescansosMedicos: [
      {
        DescansoMedicoRelacionadoId: 0,
        DescansoMedicoId: 691,
        Deleted: 0,
      },
    ],
    AccidentesTrabajo: [
      {
        AccidenteTrabajoRelacionadoId: 0,
        AccidenteTrabajoId: 508,
        Deleted: 0,
      },
    ],
    EvaluacionDSO: {},
  };

  raw = JSON.stringify(raw);
  console.log(raw);

  var url =
    apiUrlsho +
    `/api/EO-Post-EnfermedadesOcupacionales-All?code=LHNA4wGKVuAM/lV8UPvSs7SHyERgJaOrYpRrBjz3GwHTGz6Zl60bMA==&httpmethod=post`;

  var myHeaders = new Headers();
  myHeaders.append("apikey", constantes.apiKey);
  myHeaders.append("Content-Type", "application/json");

  fetch(url, {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log("creando datos");
      console.log(response);
      //guarda los datos de la nueva investigacion en el sessionStorage para poder acceder en todas partes
      sessionStorage.setItem("obj_investigacion", JSON.stringify(response));

      // obtiene la data de enfermedad ocupacional
      getEnfermedadesOcupacionales(response.Id, 1);

      //ejecuta el evento de carga de nueva investigacion en el sessionStorage
      window.dispatchEvent(
        new CustomEvent("loadInvestigacionStorage", {
          detail: "load",
          bubbles: true,
        })
      );
      return response;
    });
}

function objCreatedInvestigacion() {
  window.obj_investigacion = createObjInvestigacionNueva();
}

function initNewInvestigacion() {
  console.log("***** ----- crear nueva investigacion ----- ***** ");
  objCreatedInvestigacion();
  getTipoInvestigacionGlobal();
  getSistemaAfectadoDSO();
}

//get data transferencia
function getSistemaAfectadoDSO() {
  var url =
    apiUrlsho +
    "/api/hce_Get_006_transferencia?code=bdFhdqqPWPq62GBhPORONSYngrPBCDmZ6QnQdfIid4NdqCddRk7r3Q==&IdHC=" +
    idHC;

  let myHeaders = {
    apikey: constantes.apiKey,
    "Content-Type": "application/json",
  };

  let settings = {
    url: url,
    method: "get",
    timeout: 0,
    crossDomain: true,
    dataType: "json",
    headers: myHeaders,
  };

  fetch(url, settings)
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log("sistema afectado");
      console.log(response);
      //creando evento de carga de tipo investigacions
      window.sistemaAfectado = response;
      //event dispatch for load data complement
      window.dispatchEvent(
        new CustomEvent("loadDataTransferenciaHistoria", {
          detail: "load",
          bubbles: true,
        })
      );
    });
}
