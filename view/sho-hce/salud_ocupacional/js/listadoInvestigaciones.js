    window.areasGerencias = [];
window.listadoInvestigacion = [];
window.listadoConfirmados = [];
//-------- iniciando con las funciones
function getEnfermedadesOcupacionales() {
  var url =
    apiUrlsho +
    `/api/EO-EnfermedadesOcupacionales-All-get?code=acJSBh7lPrk/REaaCQmangSeJ/qgv063y0UgjpwvyB9zNZaWrrdHgg==&httpmethod=objectlist&Id=0&HistoriaClinicaId=0&GerenciaId=0&SedeId=0&AreaId=0&Documento=0&Nombres=&Apellidos=&CodeEO=`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log(response);
      sessionStorage.setItem(
        "listado_investigaciones",
        JSON.stringify(response.EnfermedadesOcupacionales)
      );
      cargarAreasGerencias();
    });
}

function getCasosConfirmado() {
  var url =
    apiUrlsho +
    `/api/EO-Diagnosticos-All-get?code=4aEkn7Aebc4qe33SyEQfhM5aJTB56RRizPLFHnjfaHnX020qbqeezA==&httpmethod=objectlist&EnfermedadOcupacionalId=0&StatusDiagnostico=CO`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log(response);
      sessionStorage.setItem(
        "listado_invConfirmados",
        JSON.stringify(response.Diagnosticos)
      );
      window.listadoConfirmados = response.Diagnosticos;
      drawInveConfirmadas(1);
    });
}

//listar investigaciones confirmadas
function drawInveConfirmadas(paginate) {
  console.log("listando las confirmadas...");
  let listadoConfirmados = window.listadoConfirmados;
  let dataTr = "";
  let pagineInit = 5 * paginate - 5;
  let pagineEnd = 5 * paginate;
  let dataListados = listadoConfirmados.slice(pagineInit, pagineEnd);
  //listados
  dataListados.forEach((element) => {
    dataTr += `
    <tr>
        <td>${element.Code}</td>
        <td>${element.A_NombresPaciente}</td>
        <td>${element.A_ApellidosPaciente}</td>
        <td>${getGerencia(element.B_GerenciaId)}</td>
        <td>${getSedePlanta(element.B_SedeId)}</td>
        <td>${getArea(element.B_AreaId)}</td>
        <td>${element.B_PuestoTrabajo}</td>
        <td>${element.Id}</td>
        <td>${element.CreatedDateF}</td>
        <td>${element.CreatedDateF}</td>
        <td>${element.CierreInvestigacionDateF}</td>
        <td>${element.CIE10}</td>
        <td>${element.CantidadDescansosMedicos}</td>
        <td>${element.CantidadInterconsultas}</td>
        <td>${element.CantidadTransferencias}</td>
        <td>${element.Seguimiento}</td>
        <td></td>
        <td>
          <div class="dropdown float-right dropleft">
            <div
              class="more-info"
              id="item_am_diagnostico_1"
              data-toggle="dropdown"
            >
              <img src="images/iconos/menu_responsive.svg" alt="" />
            </div>
            <div
              class="dropdown-menu"
              aria-labelledby="item_am_diagnostico_1"
            >
              <ul>
                <li onclick="verModoEdicionInvestigacion(${element.Id})">
                    <svg xmlns="http://www.w3.org/2000/svg" width="22.639" height="16.189" viewBox="0 0 22.639 16.189">
                      <defs>
                        <style>
                          .cls-1 {
                            fill: #007bff;
                            fill-rule: evenodd;
                          }
                        </style>
                      </defs>
                      <path id="Trazado_204" data-name="Trazado 204" class="cls-1" d="M12.132,4c2.76,0,5.4,1.448,7.858,3.826a23.586,23.586,0,0,1,2.266,2.544c.4.523.689.934.846,1.179l.349.545-.349.545c-.157.245-.443.656-.846,1.179a23.59,23.59,0,0,1-2.266,2.544c-2.459,2.378-5.1,3.826-7.858,3.826s-5.4-1.448-7.858-3.826a23.588,23.588,0,0,1-2.266-2.544c-.4-.523-.69-.934-.846-1.179l-.349-.545.349-.545c.157-.245.443-.656.846-1.179A23.584,23.584,0,0,1,4.274,7.826C6.734,5.448,9.373,4,12.132,4Zm8.522,7.606a21.583,21.583,0,0,0-2.071-2.325c-2.118-2.048-4.321-3.257-6.451-3.257S7.8,7.232,5.681,9.281A21.576,21.576,0,0,0,3.61,11.606c-.132.171-.254.335-.366.489.112.154.234.318.366.489a21.571,21.571,0,0,0,2.071,2.325C7.8,16.957,10,18.166,12.132,18.166s4.333-1.209,6.451-3.257a21.578,21.578,0,0,0,2.071-2.325c.132-.171.254-.335.366-.489C20.909,11.941,20.786,11.777,20.654,11.606Zm-12.569.489a4.047,4.047,0,1,0,4.047-4.047A4.047,4.047,0,0,0,8.085,12.095Zm6.071,0a2.024,2.024,0,1,1-2.024-2.024A2.024,2.024,0,0,1,14.156,12.095Z" transform="translate(-0.813 -4)"/>
                    </svg>
                  <span>Ver detalle</span>
                </li>
              </ul>
            </div>
          </div>
        </td>
    </tr>`;
  });
  if (listadoConfirmados.length == 0) {
    dataTr +=
      '<tr><td colspan="18" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById("content_list_inves_confirmados_sp5").innerHTML =
    dataTr;
  document.getElementById("cant_invest_list_confirmadas_sp5").innerHTML =
    listadoConfirmados.length;
  //pintar la paginacion
  drawPaginacionConfirmadasPag(
    "content_inv_confirmados_paginacion",
    listadoConfirmados
  );
}

function drawPaginacionConfirmadasPag(path, data) {
  let paginateStep = "";
  let lengthPage = Math.ceil(data.length / 5);
  for (let i = 0; i < lengthPage; i++) {
    paginateStep += `<li class="page-item" id="btn-paginate-investigacion-${i}" onclick="drawInveConfirmadas(${
      i + 1
    });selectPaginateInvEO(${i},'#content_inv_confirmados_paginacion')"><a class="page-link" href="javascript:void(0)">${
      i + 1
    }</a></li>`;
  }
  document.getElementById(path).innerHTML = paginateStep;
}

//cargar las variables necesarias para esta ocasion
function cargarAreasGerencias() {
  var url =
    apiUrlssoma +
    "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0";

  let headers = {
    "Content-Type": "application/json",
    apikey: constantes.apiKey,
    "Access-Control-Allow-Origin": "*",
  };

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers,
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log(response);
      window.areasGerencias = response;
      //evento para cargar los datos de las tablas
      window.dispatchEvent(
        new CustomEvent("loadGerenciasSedes", {
          detail: "load",
          bubbles: true,
        })
      );
    });
}

window.addEventListener("loadGerenciasSedes", () => {
  let listadoInvestigaciones = JSON.parse(
    sessionStorage.getItem("listado_investigaciones")
  );
  window.listadoInvestigacion = listadoInvestigaciones;
  listarInvestigacionesEO(1);
  getCasosConfirmado();
  console.log("cargando gerencias");
});

//dibujar datos en las tablas
function listarInvestigacionesEO(paginate) {
  console.log("pintando datos... de la tabla investigacion ocupacional");
  let listadoInvestigaciones = window.listadoInvestigacion;
  let dataTr = "";
  let pagineInit = 5 * paginate - 5;
  let pagineEnd = 5 * paginate;
  let dataListados = listadoInvestigaciones.slice(pagineInit, pagineEnd);

  dataListados.forEach((element) => {
    dataTr += `
    <tr>
        <td>${element.Code}</td>
        <td>${element.A_NombresPaciente}</td>
        <td>${element.A_ApellidosPaciente}</td>
        <td>${getGerencia(element.B_GerenciaId)}</td>
        <td>${getSedePlanta(element.B_SedeId)}</td>
        <td>${getArea(element.B_AreaId)}</td>
        <td>${element.B_PuestoTrabajo}</td>
        <td>${element.Id}</td>
        <td>${element.CreatedDateF}</td>
        <td>${element.CreatedDateF}</td>
        <td>${element.CierreInvestigacionDateF}</td>
        <td>${element.DiagnosticosIniciales}</td>
        <td>${element.EstadoEnfermedadOcupacional}</td>
        <td></td>
        <td>
            <div class="dropdown float-right dropleft">
                <div
                class="more-info"
                id="item_am_diagnostico_1"
                data-toggle="dropdown"
                >
                <img src="images/iconos/menu_responsive.svg" alt="" />
                </div>
                <div
                class="dropdown-menu"
                aria-labelledby="item_am_diagnostico_1"
                >
                    <ul>
                        <li onclick="verModoEdicionInvestigacion(${
                          element.Id
                        })">
                        <img src="./images/sho/eyeIcon.svg" />
                            <span>Ver detalle</span>
                        </li>
                    </ul>
                </div>
            </div>
        </td>
    </tr>`;
  });
  if (listadoInvestigaciones.length < 1) {
    dataTr +=
      '<tr><td colspan="15" class="text-center">No hay registros para este listado</td></tr>';
  }
  document.getElementById("content_table_investigaciones_eo_sp5").innerHTML =
    dataTr;
  document.getElementById(
    "cant_investigacion_eo_data_trabajador_sp5"
  ).innerHTML = listadoInvestigaciones.length;
  drawPaginacionListadosEO(
    "paginacion_listado_investigaciones_eo_sp5",
    listadoInvestigaciones
  );
}

function drawPaginacionListadosEO(path, data) {
  let paginateStep = "";
  let lengthPage = Math.ceil(data.length / 5);
  for (let i = 0; i < lengthPage; i++) {
    paginateStep += `<li class="page-item" id="btn-paginate-investigacion-${i}" onclick="listarInvestigacionesEO(${
      i + 1
    });selectPaginateInvEO(${i},'#paginacion_listado_investigaciones_eo_sp5')"><a class="page-link" href="javascript:void(0)">${
      i + 1
    }</a></li>`;
  }
  document.getElementById(path).innerHTML = paginateStep;
}

function selectPaginateInvEO(index, path) {
  document.querySelectorAll(path + " li").forEach((element) => {
    element.classList.remove("active");
  });
  document
    .querySelector(path + " #btn-paginate-investigacion-" + index)
    .classList.add("active");
}

function verModoEdicionInvestigacion(id) {
  var url =
    apiUrlsho +
    `/api/EO-EnfermedadesOcupacionales-All-get?code=acJSBh7lPrk/REaaCQmangSeJ/qgv063y0UgjpwvyB9zNZaWrrdHgg==&httpmethod=objectlist&Id=${id}&HistoriaClinicaId=0&GerenciaId=0&SedeId=0&AreaId=0&Documento=0&Nombres=&Apellidos=&CodeEO=&Deleted=0`;

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log(response);
      if (response.EnfermedadesOcupacionales.length > 0) {
        sessionStorage.setItem(
          "enfermedad_ocupacional",
          JSON.stringify(response.EnfermedadesOcupacionales[0])
        );
      } else {
        sessionStorage.setItem(
          "enfermedad_ocupacional",
          sessionStorage.getItem("obj_investigacion")
        );
      }

      window.dispatchEvent(
        new CustomEvent("loadEnfermedadStorage", {
          detail: "load",
          bubbles: true,
        })
      );
    });
  //mandar a pintar
  window.toWhereBack = "medicoOcupacional";
  window.modeVistaInvestigacion = true;
  handlerUrlhtml(
    "contentGlobal",
    "view/sho-hce/salud_ocupacional/formularioInvestigaciones.html",
    "Formulario de investigación"
  );
}

function getSedesGerenciasAreas() {
  var url =
    apiUrlssoma +
    "/api/a_get_lista_sedes_areas_gerencia?code=5sz/nIVDXtW7t97kzgfOyzuN4yYq6FEfuJoZSlk3DzGQ9AMLu7k6WQ==&IdSede=0&IdArea=0&IdGerencia=0";

  fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      apikey: constantes.apiKey,
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      console.log("cargando sedes areas gerencia...");
      console.log(response);
      sedeAreaGerencia = response;
    });
}

/**
 * funciones helpers
 */
function getGerencia(id) {
  let gerencia = "";
  window.areasGerencias.Gerencia.forEach((element) => {
    if (element.Id == id) {
      gerencia = element.Description;
    }
  });
  return gerencia;
}

function getSedePlanta(idSede) {
  let sede = "";
  window.areasGerencias.Sedes.forEach((element) => {
    if (element.Id == idSede) {
      sede = element.Description;
    }
  });
  return sede;
}

function getArea(id) {
  let area = "";
  window.areasGerencias.Area.forEach((element) => {
    if (element.Id == id) {
      area = element.Description;
    }
  });
  return area;
}
