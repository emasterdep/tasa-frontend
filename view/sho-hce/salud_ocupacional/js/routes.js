function sp5ViewFormInvestigacion() {
  handlerUrlhtml(
    "contentGlobal",
    "view/sho-hce/salud_ocupacional/formularioInvestigaciones.html",
    "Formulario de investigación"
  );
  window.toWhereBack = "default";
}

function showRegistroCalidad() {
  handlerUrlhtml(
    "contentGlobal",
    "view/sho-hce/salud_ocupacional/Calidad_vida/registroCalidadVida.html",
    "Registro de calidad de vida"
  );
}
